  $(function(){

    $("#treeUK").dynatree({

      fx: { height: "toggle", duration: 200 },
      autoCollapse: true,
      onActivate: function(node) {

		var valData = (node.data.key).split("|");

		$("[name=RoleParentId]").attr("value", valData[0]);
		$("[name=GRoleId]").attr("value", valData[7]);
		$("#RoleName").val(valData[2]);
		$("#RoleCode").val(valData[6]);
		var x= $("#GRoleId").val(valData[7]);
		var xyz= $("#gjabatanId").val(valData[5]);
		$("#RoleDesc").val(valData[3]);
		$("[name=gjabatanId]").attr("data-val", valData[5]);
		$("[name=RoleStatus]").attr("value",valData[4]);
		$("[name=RoleId]").attr("value", valData[1]);	

		if (valData[4] == 1) {
			$('#RoleStatus').prop('checked',true);
	    }else{
	    	$('#RoleStatus').prop('checked',false);
	    }
	    
      },

      onDeactivate: function(node) {

        $("[name=RoleParentId]").attr("value", "");
		$("[name=RoleCode]").attr("value", "");
		$("[name=RoleName]").attr("value", "");

		$("#RoleDesc").val('');

		$("[name=gjabatanId]").attr("data-val", '');
		$("[name=GRoleId]").attr("data-val", '');

		$("[name=RoleStatus]").attr("checked", "false");
		$('.btn-edit').removeClass('hidden').removeAttr('disabled');
		$('.btn-edit').removeClass('hidden').removeAttr('disabled');
      }

    });

    	

	$("#treeUK").dynatree("option", "autoCollapse", 0);
	$("#treeUK").dynatree("option", "fx", { height: "toggle", duration: 200 });
	// $("#treeUK").dynatree("getTree").activateKey(($("[name=RoleId]").attr("value") + '|' + $("[name=RoleId]").attr("value")));
	

  });

  

	function setChecked(str){

	  	var RoleStatus = document.forms.formUK.RoleStatus;

		var btn1 = document.forms.formUK.btnTambah;

		var btn2 = document.forms.formUK.btnUbah;

		

	  	if(str == '0'){

			RoleStatus.checked = false;

			btn1.disabled = true;

			btn2.disabled = true;

		}else{

			RoleStatus.checked = true;

			btn1.disabled = false;

			btn2.disabled = false;

		}

  	}

  

  function setNew(){

  	var RoleParentId = document.forms.formUK.RoleParentId;

	var RoleCode = document.forms.formUK.RoleCode;

	var RoleName = document.forms.formUK.RoleName;

	var RoleDesc = document.forms.formUK.RoleDesc;

	var RoleStatus = document.forms.formUK.RoleStatus;

	var gjabatanId = document.forms.formUK.gjabatanId;

	var btn1 = document.forms.formUK.btnSimpan;

	var btn2 = document.forms.formUK.btnUbah;

	var btn3 = document.forms.formUK.btnBatal;

	var task = document.forms.formUK.task;

	

	if(RoleParentId.value == ''){

		alert('Pilih data awal untuk Menambah Sub Klasifikasi !');

		return false;

	}

	

	RoleName.value='';

	RoleDesc.value='';

	gjabatanId.value='';

	RoleStatus.checked = true;

	RoleName.disabled=false;

	RoleDesc.disabled=false;

	RoleStatus.disabled=false;

	gjabatanId.disabled=false;

	btn1.disabled=false;

	btn2.style.display = 'none';

	btn3.disabled=false;

	task.value = 'new';

	RoleName.focus();

  }

  

  function setReset(){

  	var RoleName = document.forms.formUK.RoleName;

	var RoleDesc = document.forms.formUK.RoleDesc;

	var RoleStatus = document.forms.formUK.RoleStatus;

	var gjabatanId = document.forms.formUK.gjabatanId;

	var btn1 = document.forms.formUK.btnSimpan;

	var btn2 = document.forms.formUK.btnUbah;

	var btn3 = document.forms.formUK.btnBatal;

	var task = document.forms.formUK.task;

	

	RoleName.disabled=true;

	RoleDesc.disabled=true;

	RoleStatus.disabled=true;

	gjabatanId.disabled=true;

	btn1.disabled=true;

	btn2.style.display = 'block';

	btn3.disabled=true;

	task.value = 'new';

  }

  

  function setEdit(){

  	var RoleParentId = document.forms.formUK.RoleParentId;

	var RoleCode = document.forms.formUK.RoleCode;

	var RoleName = document.forms.formUK.RoleName;

	var RoleDesc = document.forms.formUK.RoleDesc;

	var RoleStatus = document.forms.formUK.RoleStatus;

	var gjabatanId = document.forms.formUK.gjabatanId;

	var btn1 = document.forms.formUK.btnSimpan;

	var btn2 = document.forms.formUK.btnUbah;

	var btn3 = document.forms.formUK.btnBatal;

	var task = document.forms.formUK.task;

	

	if(RoleName.value == ''){

		alert('Pilih data yang akan diubah !');

		return false;

	}

	RoleName.disabled=false;

	RoleDesc.disabled=false;

	RoleStatus.disabled=false;

	gjabatanId.disabled=false;

	btn1.disabled=false;

	btn2.disabled=true;

	btn3.disabled=false;

	task.value = 'edit';

	RoleName.focus();

  }

  

  function getSave(){

  	var RoleName = document.forms.formUK.RoleName;

	var RoleDesc = document.forms.formUK.RoleDesc;

	var RoleStatus = document.forms.formUK.RoleStatus;

	var gjabatanId = document.forms.formUK.gjabatanId;

	if(RoleName.value == ""){

		document.getElementById("req").style.display = "inline";

		return false;

	}

	

	if(RoleDesc.value == ""){

		document.getElementById("req2").style.display = "inline";

		return false;

	}

	

	if(RoleStatus.checked == false){

		var conf = window.confirm('Apakah Anda Akan Me-nonAktif kan Unit Kerja Ini ?');

		if(conf == false){

			return false;

		}

	}

	

	if(gjabatanId.value == ""){

		document.getElementById("req3").style.display = "inline";

		return false;

	}



	document.forms.formUK.submit();

  }

  

  function setDelete(){

  

  	var RoleParentId = document.forms.formUK.RoleParentId;

	var RoleCode = document.forms.formUK.RoleCode;

	

	if(RoleParentId.value == ''){

		alert('Pilih data yang akan dihapus !');

		return false;

	}

	

	if(RoleCode.value == 'uk'){

		alert('Data Unit Kerja Tidak Boleh Dihapus !');

		return false;

	}

	

  	var conf = confirm('Apakah Anda Yakin Akan Menghapus Data ini ?');

	if (conf == false){

		return false;

	}

	

  	var task = document.forms.formUK.task;

	task.value = 'delete';

	document.forms.formUK.submit();

  }