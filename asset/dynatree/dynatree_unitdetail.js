  $(function() {
      $("#treeUK").dynatree({

          fx: {
              height: "toggle",
              duration: 200
          },

          autoCollapse: true,
          onActivate: function(node) {
              var setId = (node.data.key).split('|');
              $("#RoleId").val(setId[1]);
              $("#RoleDesc").val(setId[2]);

              if (setId[4] == 1) {
                $('#RoleStatus').prop('checked',true);
                }else{
                  $('#RoleStatus').prop('checked',false);
              }
          },

          onDeactivate: function(node) {
              $("[name=RoleId]").attr("value", "");
          }
      });

      $("#treeUK").dynatree("option", "autoCollapse", 1);
      $("#treeUK").dynatree("option", "fx", {
          height: "toggle",
          duration: 200
      });

      $("#treeUK").dynatree("getTree").activateKey(($("[name=RoleId]").attr("value") + '|' + $("[name=RoleName]").attr("value")));
  });