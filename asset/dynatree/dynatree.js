  $(function(){

    $("#treeClassfication").dynatree({

      fx: { height: "toggle", duration: 200 },
      autoCollapse: true,
      onActivate: function(node) { 

        var valData = (node.data.key).split("|");

        $("[name=ClId]").attr("value", valData[2]);
        $("#ClParentId").val(valData[1]);
        $("#ClCode").val(valData[3]);
        $("#ClName").val(valData[4]);
        $("#ClDesc").val(valData[5]);
        $("#RetensiThn_Active").val(valData[6]);
        $("#RetensiThn_InActive").val(valData[7]);      
        $("#Ket_Active").val(valData[11]);      
        $("#Ket_InActive").val(valData[10]);
        $("#SusutId").val(valData[9]).change();
        
        if (valData[8] == 1) {
          $('#CIStatus').prop('checked',true);
        }else{
          $('#CIStatus').prop('checked',false);
        }

      },
    
    onDeactivate: function(node) {
      $("[name=ClCode]").attr("value", "");
      $("[name=ClName]").attr("value", "");
      $("[name=ClDesc]").val();
      $("[name=txt4]").attr("value", "");
      $("[name=CIStatus]").attr("checked", "false");
      $('.btn-edit').removeClass('hidden');
      }
    });
    $("#treeClassfication").dynatree("option", "autoCollapse", 0);
    $("#treeClassfication").dynatree("option", "fx", { height: "toggle", duration: 200 });
  
  });

  function setChecked(str){
    
      var CIStatus = document.forms.formClassification.CIStatus;
      var btn1 = document.forms.formClassification.btnTambahx;
      var btn2 = document.forms.formClassification.btnUbah;
    //alert(btn1);
    if(str == 0){
      CIStatus.checked = false;  
      btn1.disabled = true;
      btn2.disabled = true;
      //alert(CIStatus);
    }else{
      CIStatus.checked = true;
      btn1.disabled = false;
      btn2.disabled = false;
    }
  }

  function setButton(int){
    var btn = document.forms.formClassification.btnUbah;
    if(int == 0){
      btn.disabled = true;
    }else{
      btn.disabled = false;
    }
  }
 
  function setNew(){
    var parent = document.forms.formClassification.ClParentId;
  //var dparent = document.getElementById("dotParent");
  
    var ClCode = document.forms.formClassification.ClCode;
    var ClName = document.forms.formClassification.ClName;
    var ClDesc = document.forms.formClassification.ClDesc;
    var RetensiThn_Active = document.forms.formClassification.RetensiThn_Active;
    var Ket_Active = document.forms.formClassification.Ket_Active;
    var CIStatus = document.forms.formClassification.CIStatus;
    var SusutId = document.forms.formClassification.SusutId;
    var ClId = document.forms.formClassification.ClId;
  
    var btn1 = document.forms.formClassification.btnSimpan;
    var btn2 = document.forms.formClassification.btnUbah;
    var btn3 = document.forms.formClassification.btnBatal;
    var task = document.forms.formClassification.task;
  
  // parent.innerHTML  = ClCode.value;
  
  if(ClCode.value == ''){
    alert('Pilih data awal untuk Menambah Sub Klasifikasi !');
    return false;
  }
  
  //if(parent.innerHTML != ""){ 
  //  if(parent.innerHTML != "SK"){
  //    parent.style.display = "inline";
  //    dparent.style.display = "inline";
  //  }else{
  //    parent.style.display = "none";
  //  }
  //} 
  
    ClCode.value='';
    ClName.value='';
    ClDesc.value='';
    ClId.value='';
    RetensiThn_Active.value='0';
    Ket_Active.value='0';
    CIStatus.checked = true;
    
    ClCode.disabled=false;
    ClName.disabled=false;
    ClDesc.disabled=false;
    RetensiThn_Active.disabled=false;
    Ket_Active.disabled=false;
    CIStatus.disabled=false;
    SusutId.disabled=false;
    btn1.disabled=false;
    btn2.disabled=true;
    btn3.disabled=false;
    task.value = 'new';
    ClCode.focus();
  }
  
  function setReset(){
    var parent = document.forms.formClassification.ClParentId;
//  var dparent = document.getElementById("dotParent");
    var ClCode = document.forms.formClassification.ClCode;
    var ClName = document.forms.formClassification.ClName;
    var ClDesc = document.forms.formClassification.ClDesc;
    var RetensiThn_Active = document.forms.formClassification.RetensiThn_Active;
    var Ket_Active = document.forms.formClassification.Ket_Active;
    var CIStatus = document.forms.formClassification.CIStatus;
    var SusutId = document.forms.formClassification.SusutId;
  
    var btn1 = document.forms.formClassification.btnSimpan;
    var btn2 = document.forms.formClassification.btnUbah;
    var btn3 = document.forms.formClassification.btnBatal;
    var task = document.forms.formClassification.task;
  
//  parent.style.display = "none";
//  dparent.style.display = "none"; 
//  if(parent.innerHTML != 'SK'){
//    txt1.value = parent.innerHTML + dparent.innerHTML + txt1.value;
//  }
    ClCode.disabled=true;
    ClName.disabled=true;
    ClDesc.disabled=true;
    ClId.disabled=true;
    RetensiThn_Active.disabled=true;
    Ket_Active.disabled=true;
    CIStatus.disabled=true;
    SusutId.disabled=true;
  
    btn1.disabled=true;
    btn2.disabled=false;
    btn3.disabled=true;
    task.value = 'new';
  }
  
  function setEdit(){
    var parent = document.forms.formClassification.ClParentId;
//  var dparent = document.getElementById("dotParent");
    var ClCode = document.forms.formClassification.ClCode;
    var ClName = document.forms.formClassification.ClName;
    var ClDesc = document.forms.formClassification.ClDesc;
    var RetensiThn_Active = document.forms.formClassification.RetensiThn_Active;
    var Ket_Active = document.forms.formClassification.Ket_Active;
    var CIStatus = document.forms.formClassification.CIStatus;
    var txt9 = document.forms.formClassification.txt9;
    var SusutId = document.forms.formClassification.SusutId;
    var btn1 = document.forms.formClassification.btnSimpan;
    var btn2 = document.forms.formClassification.btnUbah;
    var btn3 = document.forms.formClassification.btnBatal;
    var task = document.forms.formClassification.task;
  
  if(ClCode.value == ''){
    alert('Pilih data yang akan diubah !');
    return false;
  }
  
  if(ClCode.value == "SK"){
    alert('Sistem tidak mengijinkan untuk Perubahan Klasifikasi ini !');
    return false;
  }
  
//  if(parent.innerHTML != ""){ 
//    if(parent.innerHTML != "SK"){
//      txt1.value = txt1.value.replace(parent.innerHTML + '.','');
//      parent.style.display = "inline";
//      dparent.style.display = "inline";
//    }else{
//      parent.style.display = "none";
//    } 
//  }
    ClCode.disabled=parseBool(txt9.value);
    ClName.disabled=false;
    ClDesc.disabled=false;
    RetensiThn_Active.disabled=false;
    Ket_Active.disabled=false;
    CIStatus.disabled=false;
    SusutId.disabled=false;

    btn1.disabled=false;
    btn2.disabled=true;
    btn3.disabled=false;
  
    task.value = 'edit';
    ClCode.focus();
  }
  
  function parseBool(val){
    if(val == 'true') return true;
    if(val == 'false') return false;
  }
  
  function setDelete(){
  
    var ClCode = document.forms.formClassification.ClCode;
    if(ClCode.value == ''){
      alert('Pilih data yang akan dihapus !');
      return false;
    }
  
      var conf = confirm('Apakah Anda Yakin Akan Menghapus Data ini ?');
    if (conf == false){
      return false;
    }
  
    var task = document.forms.formClassification.task;
    task.value = 'delete';
    document.forms.formClassification.submit();
  }
  
  function setSave(){
    var ClCode = document.forms.formClassification.ClCode;
    var ClName = document.forms.formClassification.ClName;
    if(ClCode.value == ""){
      document.getElementById("req").style.display = "inline";
      return false;
    }
    if(ClName.value == ""){
      document.getElementById("req2").style.display = "inline";
      return false;
    }
    document.forms.formClassification.submit();
  }