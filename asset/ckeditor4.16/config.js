/**
 * @license Copyright (c) 2003-2021, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	// config.tabIndex = 3;
	// CKEDITOR.config.tabIndex;
	// 
	config.tabSpaces = 7;
	config.enterMode = CKEDITOR.ENTER_BR;
	config.extraPlugins = 'tableresize';
	/*	Toolbar definition	*/
	config.removeButtons = 'Save,NewPage,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Blockquote,CreateDiv,BidiLtr,BidiRtl,Anchor,Flash,Smiley,Iframe,Styles,Format,Font,FontSize,ShowBlocks,TextColor,BGColor,About';

};

