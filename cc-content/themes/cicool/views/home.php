<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Login</title>
    <link rel="shortcut icon" href="<?= BASE_ASSET; ?>/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.min.css">
</head>

<style type="text/css">
.captcha-box {
    padding: 5px 0;
}

.captcha-box input {
    width: 30%;
    border: 1px solid #E5E2E2;
    padding: 5px;
}

.captcha-box img {
    width: 55%;
    float: left;
}

.required {
    color: #D02727
}
</style>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-box-body">
     <?php
       if (!empty($this->session->flashdata('error'))) {
       ?>
       <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Perhatian!</strong> Username Atau Password Anda Salah / Kode Captcha Salah.
       </div>
       <?php
       }
     ?>
     <?=form_open(BASE_URL('administrator/anri_check'),['autocomplete'=>'off'])?>	
      <div class="form-group text-center" style="font-size: 20px">
         <img src="<?=BASE_URL('uploads/depan.png')?>" height="80px">
         &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
         <img src="<?=BASE_URL('uploads/bsre.jpeg')?>" height="60px"><br>
       <!--  <hr>Sistem Informasi Kearsipan Dinamis -->
      </div>

       <div class="row">
        <div class="col-xs-12"> 
          <a > <img src="<?=BASE_URL('uploads/design_simanis2.png')?>" height="110px"> </a>
        </div>
       
       </div>
       <br>



      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username" autofocus required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <?php $cap = get_captcha(); ?>
      <div class="form-group">
          <div class="captcha-box" data-captcha-time="<?= $cap['time']; ?>">
              <a class="btn btn-flat  refresh-captcha"><i class="fa fa-refresh text-danger"></i></a>
              <span class="box-image captcha_img"><?= $cap['image']; ?></span>
          </div>
      </div>
      <div class="form-group has-feedback">
          <input type="text" class="form-control" placeholder="Captcha Kode" name="captcha" required>
          <span class="fa fa-key form-control-feedback"></span>
      </div>
      <div class="row">
          <div class="col-xs-8">
              <input type="hidden" id="captchaword" name="captchaword" value="<?= $cap['word']; ?>">
          </div>
      </div>
      <div class="row">
          <div class="col-xs-8"> 
			  <a href="https://siap.jabarprov.go.id/autologin/sikd" class="btn btn-primary btn-flat" > Login dengan TRK </a>
          </div>
          <div class="col-xs-4">
              <input type="submit" class="btn btn-primary btn-block btn-flat" value="Login">
          </div>
      </div>

    <?= form_close(); ?>
    <div class="social-auth-links text-center">
      <hr>Copyright &copy; 2016 - 2021 <br>Arsip Nasional Republik Indonesia.<br>Developed by Dispusipda Jabar<br>
    </div>
  </div>
</div>
</body>
</html>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-ZW6ZCJNSMG"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-ZW6ZCJNSMG');
</script>
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>

<script>
$('.refresh-captcha').on('click', function() {
    $.ajax({
        url: "<?= base_url('administrator/anri_captcha/reload_captcha') ?>",
        dataType: 'JSON',
        success: function(data) {
            $(".captcha_img").html(data.image);
            $("#captchaword").val(data.word);
        }
    });
});
$("document").ready(function(){
//$.ajax('https://siap.jabarprov.go.id/autologin/sikd/is-logged-in',{xhrFields: {withCredentials: true}, success: function() {window.location = 'https://siap.jabarprov.go.id/autologin/sikd'}});
});
</script>
