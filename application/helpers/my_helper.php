<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('penulisan_dinas'))
{
    function penulisan_dinas($kalimat = '')
    {
        $uc  = ucwords(strtolower($kalimat));
        $cari_text = ["Dan", "Uptd", "Ii", "Iii", "Iv", "Vi", "Vii", "Viii", "Ix", "Xi", "Xii", "Xiii", "Dprd", "Llij", "Llaj"];
        $replace   = ["dan", "UPTD", "II", "III", "IV", "VI", "VII", "VIII", "IX", "XI", "XII", "XIII", "DPRD", "LLIJ", "LLaj"];
        $newPhrase = str_replace($cari_text, $replace, $uc);
        return $newPhrase;
    }   
}