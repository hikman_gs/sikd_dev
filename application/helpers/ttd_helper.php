<?php



 
	function verifikasifile($file) {

		$ci =& get_instance(); 
		$signed_file = FCPATH . 'FilesUploaded/ttd/sudah_ttd/'.$file;
		if(file_exists($signed_file)) {
			$signed_file = FCPATH . 'FilesUploaded/ttd/sudah_ttd/'.$file;
		} else {
			$signed_file = FCPATH . 'FilesUploaded/ttd/blm_ttd/'.$file;
		}
		$namafile = basename($file);
		
		$curl = curl_init();
		if (ENVIRONMENT == 'development') {
			$nik   = '0803202100007062';
			$url   = 'http://103.122.5.60';
			$auth  = 'ZXNpZ246cXdlcnR5';
			$kukis = '21196E81CA1819E8D34FB087F6F535B2';
		} else {
			$nik   = $ci->session->userdata('nik');
			$url   = 'http://103.122.5.59';
			$auth  = 'U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=';
			$kukis = '';
		}
		curl_setopt_array($curl, array( 
			CURLOPT_URL => $url.'/api/sign/verify',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => array('signed_file' => new CURLFILE($signed_file, 'application/pdf', $namafile)),
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic ".$auth,
				"Cookie: JSESSIONID=".$kukis
			)
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$html = '';
		$result = json_decode($response); 
		if (isset($result->details)) {
			foreach ($result->details as $detail) {
				$d = $detail->info_signer;
				$html .= $d->signer_name;
				$html .='<br>';
			}
		}  else {
			$html .='--';
		}

		return $html;
	}
 