<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH.'/third_party/tcpdf_min/tcpdf.php';

class Pdf extends TCPDF
{
    public $qrcode;
    function __construct()
    {
        parent::__construct();
    }

    public function setData($qrcode, $dokumen_id){
    $this->qrcode = $qrcode;
    $this->dokumen_id = $dokumen_id;
    }

     // Page footer 
    public function Footer($img = NULL) {
        // Position at 15 mm from bottom
  
    

    // $this->SetXY(20, -30);
    $this->Image($this->qrcode, 15, 300, 15, 15, '', '', 'M', false, 300, '', false, false, 0, false, false, false);
    $this->Ln(15);
    $this->SetXY(15,315);
    $this->SetFont('arial','',8);
    $this->Cell(0,5,(!empty($this->dokumen_id) ?  $this->dokumen_id : ''),0,0,'L');
    // Arial italic 8
    $this->SetY(-30);
    $this->SetFont('arial','',9);
    // // Page number
    $this->Ln(5);                
    $this->Cell(160,5,'Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh',0,0,'C');
    $this->Ln(5);
    $this->Cell(160,5,'Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara',0,0,'C');


    }
}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */
?>