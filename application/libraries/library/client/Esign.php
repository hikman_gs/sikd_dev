<?php

require_once 'OAuthRest.php';

class Esign
{
    /**
     * 
     */
    private $error;

    /**
     * 
     */
    public function getError()
    {
        return $this->error;
    }

    public function signHash($nik = '', $passphrase = '', $hash = ''){

        $data = array(
            'passphrase' => $passphrase,
            'approved_info' => 'ok',
            'nik' => $nik,
            'hash' => $hash
        );

        $rest = new OAuthRest();
        $response = $rest->send('/esign/v2/api/entity/sign/hash', 'POST', $data);

        if(!$response) {
            $this->error = $rest->getError();
            return false;
        }
        else return $response;
    }
}