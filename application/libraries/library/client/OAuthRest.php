<?php

class OAuthRest
{
    /**
     * 
     */
    private $error;

    /**
     * @var
     */
    private $curl;


    /**
     *
     */
    private $config;

    /**
     * 
     */
    private $token;

    /**
     * OAuthRest constructor.
     */
    function __construct()
    {
        $this->config = include('config.php');
    }

    /**
     * 
     */
    private function createToken()
    {
        $queryData = array(
            'client_id' => $this->config['client_id'],
            'client_secret' => $this->config['client_secret'],
            'grant_type' => 'client_credentials'
        );

        $response = $this->sendCurl('/token', 'POST', $queryData);

        if(!$response) $this->error = 'Gagal Membuat Token';
        $this->token = $response->{'access_token'};
        //$this->token = $response;

    }

    /**
     * 
     */
    public function getError()
    {
        return $this->error;
    }
    
    /**
     * 
     */
    public function send($url, $method, $queryData = null, array $files = null)
    {
        $this->createToken();
        $response = $this->sendCurl($url, $method, $queryData, $files);
        if(!$response) return false;
        else return $response;
    }

    /**
     * 
     */
    private function sendCurl($url, $method, $queryData = null, array $files = null)
    {
        if(!is_null($this->error)) return false;

        $curl = curl_init();

        if(!is_null($this->token)) $auth = 'Authorization: Bearer ' . $this->token;
        else $auth = '';

        if(!is_null($queryData)) $content = http_build_query($queryData);
        else $content = '';

        if(!is_null($files)) {
            $postfields = $this->curl_custom_postfields($files);
            $header = $postfields[0];
            $body = $postfields[1];
        }
        else {
            $header = '';
            $body = '';
        }

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->config['host'] . $url . '?' . $content,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                'cache-control: no-cache',
                $auth,
                $header
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        $this->curl = $curl;
        
        curl_close($curl);

        if ($err) {
            $this->error = $err;
            return false;
        } else {
            $res = json_decode($response);
            if(json_last_error() == JSON_ERROR_NONE) {
                if(isset($res->error)){
                    $this->error = $res->error;
                    return false;
                }else return $res;
            }
            else return $response;
        }
    }

    /**
     * 
     */
    private function curl_custom_postfields(array $files = array()) {
   
        // invalid characters for "name" and "filename"
        static $disallow = array("\0", "\"", "\r", "\n");
       
        // build file parameters
        foreach ($files as $k => $v) {
            switch (true) {
                case false === $v = realpath(filter_var($v)):
                case !is_file($v):
                case !is_readable($v):
                    break; // or return false, throw new InvalidArgumentException
            }
            $data = file_get_contents($v);
            //$v = call_user_func("end", explode(DIRECTORY_SEPARATOR, $v));
            $k = str_replace($disallow, "_", $k);
            $v = str_replace($disallow, "_", $v);
            $name = basename($v);
            $type = mime_content_type($v);
            $body[] = implode("\r\n", array(
                "Content-Disposition: form-data; name=\"{$k}\"; filename=\"{$name}\"",
                "Content-Type: " . $type,
                "",
                $data,
            ));
        }
       
        // generate safe boundary
        do {
            $boundary = "---------------------" . md5(mt_rand() . microtime());
        } while (preg_grep("/{$boundary}/", $body));
       
        // add boundary for each parameters
        array_walk($body, function (&$part) use ($boundary) {
            $part = "--{$boundary}\r\n{$part}";
        });
       
        // add final boundary
        $body[] = "--{$boundary}--";
        $body[] = "";
       
        $header = "Content-Type: multipart/form-data; boundary={$boundary}";
        // set options
        return array($header, implode("\r\n", $body));
        
    }

}