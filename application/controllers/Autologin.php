<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Autologin extends Admin	
{
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{

		$username = $this->input->get('username');
		$authenticatorKey = $this->input->get('authenticatorKey');
		$anotationKey = $this->input->get('anotationKey');
		
		$results =  file_get_contents('https://siap.jabarprov.go.id/autologin/sikd/verify-token?username='.$username.'&authenticatorKey='.$authenticatorKey.'&anotationKey='.$anotationKey);
		$cek   =  json_decode($results);
		$cek1  =  $cek->success; 
		if($cek1) {

			$username = $username; 
			$where = array(
				'PeopleUsername' => $username, 
				'PeopleIsActive' => '1',
			);

			$ceklogin = $this->db->get_where("people", $where)->num_rows();
			
			if($ceklogin > 0){

				$datax = $this->db->query("SELECT * From v_login WHERE PeopleUsername='" . $username . "'")->result();

				foreach ($datax as $row) {
					//create session pople id
					$data_session = array(
						'peopleid' => $row->PeopleId,
						'groupid' => $row->GroupId,
						'groupname' => $row->GroupName,
						'peopleusername' => $row->PeopleUsername,
						'peoplename' => $row->PeopleName,
						'peopleposition' => $row->PeoplePosition,
						'roleid' => $row->RoleId,
						'namabagian' => $row->RoleDesc,
						'gjabatanid' => $row->gjabatanId,
						'roleatasan' => $row->RoleAtasan,
						'rolecode' => $row->RoleCode,
						'nik' => $row->NIK,
						'groleid' => $row->GRoleId,
						'approvelname' => $row->ApprovelName,
						'primaryroleid' => $row->PrimaryRoleId,
						'status' => "anri_ok_dong"
					);

					$this->session->set_userdata($data_session);
					redirect(base_url('administrator/anri_dashboard'));
	 			}
	 
			} else {
				//$this->session->set_flashdata('error', 'Username dan password salah !');
				//redirect(base_url());
			}
		} else {
			redirect('https://siap.jabarprov.go.id/autologin/sikd');
		}

        	
	}
	
	
	
}