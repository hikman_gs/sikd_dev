<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_klasifikasi extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
	}

	// Fungsi List Klasifikasi
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
				
		$this->template->title('Klasifikasi');
		//model_custome di running di autoload -> application/config/autoload.php ->autoload model
		$this->data['clasification'] = $this->model_custome->classification();
		$this->tempanri('backend/standart/administrator/classification/classification_list', $this->data);
	}
	// Akhir Fungsi List Klasifikasi

	// Fungsi Simpan Klasifikasi
	public function add_save()
	{
	    $this->form_validation->set_rules('ClCode', 'ClCode', 'trim|required|max_length[25]');
		$this->form_validation->set_rules('ClName', 'ClName', 'trim|max_length[255]|required');
		$this->form_validation->set_rules('ClDesc', 'ClDesc', 'trim');
		$this->form_validation->set_rules('RetensiThn_Active', 'RetensiThn Active', 'trim|max_length[2]');
		$this->form_validation->set_rules('Ket_Active', 'Ket Active', 'trim|max_length[250]');
		$this->form_validation->set_rules('RetensiThn_InActive', 'RetensiThn InActive', 'trim|max_length[2]');
		$this->form_validation->set_rules('Ket_InActive', 'Ket InActive', 'trim|max_length[250]');
		$this->form_validation->set_rules('SusutId', 'SusutId', 'trim|max_length[14]');
		

		if ($this->form_validation->run()) {
			$table = 'classification';
			$save_data = [
				'ClKey' => tb_key(),
				'ClId' => max_value($table),
				'ClParentId' => $this->input->post('ClId'),
				'ClCode' => $this->input->post('ClCode'),
				'ClName' => $this->input->post('ClName'),
				'ClDesc' => $this->input->post('ClDesc'),
				'RetensiThn_Active' => $this->input->post('RetensiThn_Active'),
				'Ket_Active' => $this->input->post('Ket_Active'),
				'RetensiThn_InActive' => $this->input->post('RetensiThn_InActive'),
				'Ket_InActive' => $this->input->post('Ket_InActive'),
				'SusutId' => $this->input->post('SusutId'),
				'CIStatus' => (empty($this->input->post('CIStatus')) ? '0' : '1'),
				'ClStatusParent' => '1',
			];

			$save_classification = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_klasifikasi'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_klasifikasi'));
		}
	}
	// Akhir Fungsi Simpan Klasifikasi
	
	// Buka edit data klasifikasi
	public function update()
	{

		$id 	= $this->input->post('ClId');
		$data 	= array(
			'ClCode'                  => $this->input->post('ClCode'),
	        'ClName'                  => $this->input->post('ClName'),
	        'ClDesc'                  => $this->input->post('ClDesc'),
	        'RetensiThn_Active'       => $this->input->post('RetensiThn_Active'),
	        'Ket_Active'              => $this->input->post('Ket_Active'),
	        'RetensiThn_InActive'     => $this->input->post('RetensiThn_InActive'),
	        'Ket_InActive'            => $this->input->post('Ket_InActive'),
	        'SusutId'                 => $this->input->post('SusutId'),
	        'CIStatus' 				  => (empty($this->input->post('CIStatus')) ? '0' : '1'),
		);

		$this->db->where('ClId', $id);
		$this->db->update('classification', $data);

	}
	// Tutup edit data klasifikasi

	// Buka hapus klasifikasi
	public function delete()
	{
		if ($this->db->query("SELECT ClParentId FROM classification WHERE ClParentId = ".$this->input->post('data_id'))->num_rows() > 0) {
			$data = array('status' => 'error_check_parent',);
			echo json_encode($data);
		}elseif($this->db->query("SELECT ClId FROM berkas WHERE ClId = ".$this->input->post('data_id'))->num_rows() > 0){
			$data = array('status' => 'error_berkas',);
			echo json_encode($data);
		}else{
			if ($this->input->post('data_id') == 1) {
				$data = array('status' => 'error_check_parent',);
				echo json_encode($data);
			}else{
				$data = array('status' => 'success');
				$this->db->where('ClId',$this->input->post('data_id'))->delete('classification');
				echo json_encode($data);
			}
		}
	}
	// Tutup hapus klasifikasi

}