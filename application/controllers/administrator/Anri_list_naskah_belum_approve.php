<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Anri_list_naskah_belum_approve extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_list_naskah_ba');
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_konsepnaskah_koreksi');
		$this->load->model('model_inboxfile');
		$this->load->model('model_log');
		$this->load->model('model_inbox');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_ttd');
	}

	//List NASKAH MASUK BELUM APPROVE
	public function index()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['title'] = 'Naskah Belum Disetujui';
		$this->data['tipe'] = 0;
		$this->data['NId'] = 0;
		$this->tempanri('backend/standart/administrator/mail_tl/naskah_belum_approve', $this->data);
	}
	//List NASKAH MASUK BELUM APPROVE

	//Ambil Data Seluruh Naskah Belum Disetujui
	public function get_data_naskah_ba()
	{
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_ba->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = date('d-m-Y', strtotime($field->TglReg));
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $field->JenisId . "'")->row()->JenisName;
			$row[] = $field->Hal;
			$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $field->NId_Temp . "' AND GIR_Id = '" . $field->GIR_Id . "' AND Keterangan != 'outbox'")->result();

			$files = BASE_URL . '/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {
				$z21 .= "<tr>";
				$z21 .= "<td><a href='" . $files . $value1->FileName_fake . "' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
				$z21 .= "</tr>";
			}
			$z21 .= "</table>";

			$row[] = $z21;
			//Tambahan kemsos
			//Cek no surat dulu, jika nomor surat beri ada maka tolah keluarkan pesan
			$konsep123 = $this->db->query("SELECT Number FROM konsep_naskah WHERE NId_Temp = '" . $field->NId_Temp . "' AND GIR_Id = '" . $field->GIR_Id . "'")->row();

			$ceknosurat = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '" . $field->NId_Temp . "' AND GIR_Id = '" . $field->GIR_Id . "'")->row()->nosurat;

			$jenis_naskah = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $field->JenisId . "'")->row()->JenisName;

			if ($konsep123->Number == '0') {

				if($this->session->userdata('groupid')==7) {

					$row[] = '<a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';

				}else if (!$ceknosurat) {

					if ($jenis_naskah == 'Nota Dinas') {

						$this->data['NId'] 				= $NId;

						$cari_asal_nota = $this->db->query("SELECT RoleId_From FROM konsep_naskah WHERE NId_Temp = '" . $field->NId_Temp . "' AND GIR_Id = '" . $field->GIR_Id . "'")->row()->RoleId_From;

						$cari_approve_people = $this->db->query("SELECT Approve_People FROM konsep_naskah WHERE NId_Temp = '" . $field->NId_Temp . "' AND GIR_Id = '" . $field->GIR_Id . "'")->row()->Approve_People;

						$cari_roleid_approve_people =$this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '" . $cari_approve_people. "'")->row()->PrimaryRoleId;

						$cari_GroupId =$this->db->query("SELECT GroupId FROM people WHERE PrimaryRoleId = '" . $cari_asal_nota. "' AND GroupId !=8")->row()->GroupId;

						if ($cari_GroupId==7){

							$row[] = '<a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_tu_ttdnotapimpinan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Nota Dinas Untuk Dinomeri" class="btn btn-warning btn-sm"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></button> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';

						}else if(($cari_GroupId==3) && ($cari_asal_nota !=$cari_roleid_approve_people  )) {

							$row[] = '<a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_teruskan_tu(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Nota Dinas Untuk Dinomeri" class="btn btn-warning btn-sm"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></button> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';	

						}else if(($cari_GroupId==3) && ($cari_asal_nota ==$cari_roleid_approve_people  )) {

							$row[] = '<a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_tu_ttdnotapimpinan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Nota Dinas Untuk Dinomeri" class="btn btn-warning btn-sm"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></button> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';	

						}else{

							$row[] = '<a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_teruskan_uk(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Nota Dinas Untuk Dinomeri" class="btn btn-warning btn-sm"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></button> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';
						}						

					}else if($jenis_naskah == 'Surat Dinas') {

						$row[] = '<a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_teruskan_surat_dinas(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Surat Surat Dinas Untuk Dinomeri" class="btn btn-warning btn-sm"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></button> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';

					}else{

						$row[] = '<a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_teruskan_uk(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Dinomeri" class="btn btn-warning btn-sm"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></button> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';
					}

				}else{

					$row[] = '<button type="button" onclick=ko_verifikasi(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Masukkan Kode Verifikasi" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button> <a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_teruskan_tu(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Dinomeri" class="btn btn-warning btn-sm"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></button> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';
				}	
				
			} else {

				$row[] = '<button type="button" onclick=ko_verifikasi(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Masukkan Kode Verifikasi" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button></a>  <a target="_blank" href="' . site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/' . $field->GIR_Id) . '" title="Lihat Naskah Sudah di Nomori" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
			}


			$jenis_naskah = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $field->JenisId . "'")->row()->JenisName;
			if ($jenis_naskah == 'Nota Dinas') {
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_notadinas/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Surat Dinas') {
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_naskahdinas/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Surat Edaran') {
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_suratedaran/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Surat Perintah') {
				$row[] = '<a href="' . site_url('administrator/surat_perintah/edit/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Instruksi Gubernur') {
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_suratinstruksi/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Surat Pernyataan Melaksanakan Tugas') {
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_superpelaksanaantugas/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Surat Keterangan') {
				$row[] = '<a href="' . site_url('administrator/surat_keterangan/edit/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Surat Izin') {
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_surat_izin/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Pengumuman') {
				$row[] = '<a href="' . site_url('administrator/surat_pengumuman/edit/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			} elseif ($jenis_naskah == 'Rekomendasi') {
				$row[] = '<a href="' . site_url('administrator/surat_rekomendasi/edit/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			}
			$data[] = $row;
		}


		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_ba->count_all(),
			"recordsFiltered" => $this->model_list_naskah_ba->count_filtered(),
			"data" => $data,
		);

		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Belum Disetujui

	//PDF naskah dinas tindaklanjut
	public function naskahdinas_tindaklanjut_pdf($NId)
	{
		$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();
		// var_dump($naskah);
		// die();

		$this->data['NId'] 				= $NId;
		$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
		$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_To_2'] 		= $naskah->RoleId_To_2;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['lampiran'] 		= $naskah->lampiran;
		$this->data['lampiran2'] 		= $naskah->lampiran2;
		$this->data['lampiran3'] 		= $naskah->lampiran3;
		$this->data['lampiran4'] 		= $naskah->lampiran4;
		$this->data['type_naskah'] 		= $naskah->type_naskah;
		$this->data['Alamat']			= $naskah->Alamat;
		$this->data['RoleCode'] 		= $naskah->RoleCode;
		$this->data['Number']			= $naskah->Number;
		$this->data['Hal'] 				= $naskah->Hal;
		$this->data['Hal_pengantar'] 	= $naskah->Hal_pengantar;
		$this->data['Nama_ttd_pengantar'] 	= $naskah->Nama_ttd_pengantar;

		if (!empty($naskah->SifatId)) {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
		}

		if (!empty($naskah->Jumlah)) {
			$this->data['Jumlah'] 			= $naskah->Jumlah;
		}

		if (!empty($naskah->MeasureUnitId)) {
			$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}
		// $this->load->library('Pdf');
		// $sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;
		$this->data['TglReg'] 				= $naskah->TglReg;
		$this->data['Approve_People'] 		= $naskah->Approve_People;
		$this->data['TtdText'] 				= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 		= $naskah->Nama_ttd_konsep;
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Approve_People3'] 		= $naskah->Approve_People3;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		// 	$data_perintah = $this->db->get('v_login')->result();
		// // var_dump($data_perintah);
		// // die();
		// 	$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

		// //tcpdf
		// 	$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama. "'")->row()->Pangkat;
		// 	$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

		// 	$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
		// 	$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

		// 	$age = $this->session->userdata('roleid') == 'uk.1';
		// 	$xx = $this->session->userdata('roleid');		
		// 	$r_atasan =  $this->session->userdata('roleatasan'); 

		// 	if($this->data['TtdText'] == 'PLT') {
		// 		$tte = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
		// 	}elseif($this->data['TtdText'] == 'PLH') {
		// 		$tte = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);
		// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
		// 		$tte =get_data_people('RoleName', $this->data['Approve_People']);
		// 	// .'<br>'.get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']);
		// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
		// 		$tte = 'a.n'.'. '.get_data_people('RoleName', $this->data['Approve_People3']).'<br>'.get_data_people('RoleName', $$r_atasan).','.'<br>'.'u.b.<br>'.get_data_people('RoleName',$this->data['Approve_People']).',';
		// 	}else{

		// 		$tte =get_data_people('RoleName', $this->data['Approve_People']);
		// 	}

		// 	$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		// 	<tr nobr="true">
		// 	<td rowspan="7" width="60px" style="valign:bottom;">
		// 	<br><br>
		// 	<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
		// 	<td width="180px">Ditandatangani secara elekronik oleh:</td>
		// 	</tr>
		// 	<tr nobr="true">
		// 	<td width="180px" style="text-align: left; font-size:9;">'.$tte.',</td>
		// 	</tr>
		// 	<tr nobr="true">
		// 	<td><br></td>
		// 	</tr>
		// 	<tr nobr="true">
		// 	<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
		// 	</td>
		// 	</tr>
		// 	<tr nobr="true">
		// 	<td>'.$pangkat_ttd. '
		// 	</td>
		// 	</tr>
		// 	</table>';

		// 	$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		// 	$pdf->SetPrintHeader(false);
		// 	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
		// 	$pdf->SetMargins(30, 30, 30);
		// 	$pdf->SetAutoPageBreak(TRUE, 30);
		// // 
		// 	$pdf->AddPage('P', 'F4');

		// 	$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
		// 	$pdf->Ln(10);
		// 	$pdf->SetX(120);
		// 	$pdf->SetFont('Arial','',11);

		// 	$pdf->Cell(30,5,'Tempat / Tanggal / Bulan / Tahun ',0,1,'L');
		// 	$pdf->Ln(2);
		// 	$pdf->SetX(120);
		// // $pdf->SetFont('Arial','',10);
		// 	$pdf->Cell(30,5,'Kepada',0,1,'L');


		// 	$pdf->Cell(20,5,'Nomor',0,0,'L');
		// 	$pdf->Cell(5,5,':',0,0,'L');
		// 	$pdf->Cell(5,5,'.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'],0,0,'L');
		// 	$pdf->SetX(120);
		// 	$pdf->Cell(10,5,'Yth. ',0,0,'L');
		// 	$pdf->SetX(130);
		// 	$pdf->writeHTMLCell(60, 5, '', '', $this->data['RoleId_To'], '', 1, 0, true, 'L', true);
		// 	$pdf->SetX(126);
		// 	$pdf->Cell(10,5,'di ',0,0,'L');
		// // $pdf->SetX(130); ini dimatikan
		// 	$pdf->Ln(5);
		// 	$pdf->SetX(132);
		// 	$pdf->MultiCell(56,5,$this->data['Alamat'],0,'L');
		// $y_kanan = $pdf->GetY(); // ini tambahan
		// $pdf->Ln(5);
		// $pdf->SetY(58);
		// $pdf->Cell(20,5,'Sifat',0,0,'L');
		// $pdf->Cell(5,5,':',0,0,'L');
		// $pdf->Cell(60,5,$this->data['SifatId'],0,0,'L');
		// $pdf->SetX(130);
		// // $pdf->Cell(10,5,'di',0,0,'L');
		// $pdf->Ln(5);
		// $pdf->Cell(20,5,'Lampiran',0,0,'L');
		// $pdf->Cell(5,5,':',0,0,'L');
		// $pdf->Cell(60,5,$this->data['Jumlah'].' '.$this->data['MeasureUnitId'],0,0,'L');
		// $pdf->SetX(133);
		// $pdf->MultiCell(56,5,'',0,'L');
		// $pdf->Cell(20,5,'Hal',0,0,'L');
		// $pdf->Cell(5,5,':',0,0,'L');
		// $pdf->writeHTMLCell(60, 5, '', '', $this->data['Hal'], 0, 1, 0, true, 'L', true); // ini 0 jadi 1
		// $y_kiri = $pdf->GetY(); // ini tambahan
		// $pdf->Ln(25);	
		// $pdf->SetX(50);
		// // ini tambahan
		// if ($y_kiri > $y_kanan) {
		// 	$pdf->SetY($y_kiri + 10);
		// }else if ($y_kiri < $y_kanan){
		// 	$pdf->SetY($y_kanan + 10);
		// }
		// $pdf->SetX(50);
		// $pdf->writeHTMLCell(140, 0, '', '', $this->data['Konten'], 0, 1, 0, true, 'J', true);


		// // $pdf->SetFont('Arial','',10);
		// $r_atasan =  $this->session->userdata('roleatasan'); 
		// $r_biro =  $this->session->userdata('groleid');
		// $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
		// $atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

		// $pdf->Ln(6);
		// if($this->data['TtdText'] == 'PLT') {
		// 	$pdf->SetX(100);
		// 	$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// }elseif($this->data['TtdText'] == 'PLH') {
		// 	$pdf->SetX(100);
		// 	$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// }elseif($this->data['TtdText2'] == 'Atas_Nama') {
		// 	$pdf->SetX(100);
		// 	$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 	$pdf->SetX(100);
		// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

		// }elseif($this->data['TtdText2'] == 'untuk_beliau') {
		// 	$pdf->SetX(100);
		// 	$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 	$pdf->SetX(100);
		// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
		// 	$pdf->SetX(100);
		// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
		// }else{
		// 	$pdf->SetX(100);
		// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
		// }
		// $pdf->Ln(2);
		// $pdf->SetX(130);
		// // $pdf->SetFont('Arial','',10);
		// $pdf->Cell(32,5,'PEMERIKSA',0,1,'L');
		// $pdf->Ln(5);		
		// $pdf->SetX(100);
		// $pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

		// if (!empty($this->data['RoleId_Cc'] )) {


		// 	$pdf->Ln(12);
		// 	$pdf->Cell(30,5,'Tembusan',0,0,'L');
		// 	$pdf->Cell(5,5,':',0,0,'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30,5,'',0,0,'L');
		// 	$pdf->Cell(5,5,'',0,0,'L');

		// 	$exp = explode(',', $this->data['RoleId_Cc']);
		// 	$hitung = count($exp);
		// 	$i = 0;
		// 	foreach( $exp as $v){
		// 		$y = 'Yth';
		// 		$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;			
		// 		$uc  = ucwords(strtolower($xx1));
		// 		$str = str_replace('Dan', 'dan', $uc);
		// 		$str = str_replace('Uptd', 'UPTD', $str);
		// 		$str = str_replace('Dprd', 'DPRD', $str);
		// 		$i++;

		// 	if (!empty($hitung)) {

		// 			if ($hitung > 1) {
		// 				# code...
		// 				if ($i == $hitung - 1) {
		// 					# code...
		// 					$pdf->Ln(1);
		// 					$pdf->SetX(30);
		// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
		// 					$pdf->SetX(34);
		// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
		// 					$pdf->SetX(42);
		// 					$pdf->MultiCell(150, 5, $str."; dan\n", 0, 'J', 0, 2, '' ,'', true);
		// 				} elseif($i == $hitung){
		// 					$pdf->Ln(1);
		// 					$pdf->SetX(30);
		// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
		// 					$pdf->SetX(34);
		// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
		// 					$pdf->SetX(42);
		// 					$pdf->MultiCell(150, 5, $str.".\n", 0, 'J', 0, 2, '' ,'', true);
		// 				}else{
		// 					$pdf->Ln(1);
		// 					$pdf->SetX(30);
		// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
		// 					$pdf->SetX(34);
		// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
		// 					$pdf->SetX(42);
		// 					$pdf->MultiCell(150, 5, $str.";\n", 0, 'J', 0, 2, '' ,'', true);
		// 				}

		// 			}else{
		// 				$$pdf->SetX(34);
		// 				$pdf->Cell(30,5,$y.'. ',0,0,'L');
		// 				$pdf->SetX(42);
		// 				$pdf->MultiCell(150, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
		// 			}


		// 		}	
		// 	}
		// }




		// if (!empty($this->data['lampiran2'])) {
		// 	$lamp_romawi1= "I";
		// }

		// else{
		// 	$lamp_romawi1= "";
		// }

		// if  ($this->data['type_naskah'] == 'XxJyPn38Yh.2') {
		// 	$type_surat ="SURAT UNDANGAN ";          
		// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
		// 	$type_surat ="SURAT PANGGILAN ";  
		// }else { 
		// 	$type_surat ="SURAT ";
		// }

		// $header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
		// <tr>
		// <td width="65px">Lampiran '.$lamp_romawi1.' : </td>
		// <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName',$naskah->Nama_ttd_atas_nama).'</td>
		// </tr>
		// <tr>
		// <td>&nbsp;</td>
		// <td width="50px">NOMOR</td>
		// <td width="10px">:</td>
		// <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
		// </tr>

		// <tr>

		// <td>&nbsp;</td>
		// <td>TANGGAL</td>
		// <td>:</td>
		// <td>Tanggal/Bulan/Tahun</td>
		// </tr>
		// <tr>
		// <td>&nbsp;</td>
		// <td>PERIHAL</td>
		// <td>:</td>
		// <td>'. $this->data['Hal'].'</td>
		// </tr>
		// </table>';

		// $jum_lamp2 = $this->data['lampiran2'];
		// $no_lamp2 = 1;
		// if (!empty($this->data['lampiran2'])) {
		// 	switch ($no_lamp2){
		// 		case 1: 
		// 		$lamp_romawi2 = "II";

		// 		break;
		// 	}                                                         
		// }

		// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
		// 	$type_surat ="SURAT UNDANGAN ";          
		// }elseif ($this->data['type_naskah']  == 'XxJyPn38Yh.3') {
		// 	$type_surat ="SURAT PANGGILAN ";  
		// }else { 
		// 	$type_surat ="SURAT ";
		// }

		// $header_lampiran2 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
		// <tr>
		// <td width="65px">Lampiran '.$lamp_romawi2.' : </td>
		// <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName',$naskah->Nama_ttd_atas_nama).'</td>
		// </tr>
		// <tr>
		// <td>&nbsp;</td>
		// <td width="50px">NOMOR</td>
		// <td width="10px">:</td>
		// <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
		// </tr>   
		// <tr>

		// <td>&nbsp;</td>
		// <td>TANGGAL</td>
		// <td>:</td>
		// <td>Tanggal/Bulan/Tahun</td>
		// </tr>
		// <tr>
		// <td>&nbsp;</td>
		// <td>PERIHAL</td>
		// <td>:</td>
		// <td>'. $this->data['Hal'].'</td>
		// </tr>
		// </table>';

		// $jum_lamp3 = $this->data['lampiran3'];
		// $no_lamp3 = 1;
		// if (!empty($this->data['lampiran3'])) {
		// 	switch ($no_lamp3){
		// 		case 1: 
		// 		$lamp_romawi3 = "III";

		// 		break;
		// 	}                         

		// }

		// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
		// 	$type_surat ="SURAT UNDANGAN ";          
		// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
		// 	$type_surat ="SURAT PANGGILAN ";  
		// }else { 
		// 	$type_surat ="SURAT ";
		// }

		// $header_lampiran3 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
		// <tr>
		// <td width="65px">Lampiran '.$lamp_romawi3.' : </td>
		// <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName',$naskah->Nama_ttd_atas_nama).'</td>
		// </tr>
		// <tr>
		// <td>&nbsp;</td>
		// <td width="50px">NOMOR</td>
		// <td width="10px">:</td>
		// <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
		// </tr>

		// <tr>

		// <td>&nbsp;</td>
		// <td>TANGGAL</td>
		// <td>:</td>
		// <td>Tanggal/Bulan/Tahun</td>
		// </tr>
		// <tr>
		// <td>&nbsp;</td>
		// <td>PERIHAL</td>
		// <td>:</td>
		// <td>'. $this->data['Hal'].'</td>
		// </tr>
		// </table>';


		// $jum_lamp4 =$this->data['lampiran4'];
		// $no_lamp4 = 1;

		// if (!empty($this->data['lampiran4'])) {
		// 	switch ($no_lamp4){
		// 		case 1: 
		// 		$lamp_romawi4 ="IV";

		// 		break;
		// 	}                            

		// }

		// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
		// 	$type_surat ="SURAT UNDANGAN ";          
		// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
		// 	$type_surat ="SURAT PANGGILAN ";  
		// }else { 
		// 	$type_surat ="SURAT ";
		// }

		// $header_lampiran4 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
		// <tr>
		// <td width="65px">Lampiran '.$lamp_romawi4.' : </td>
		// <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName',$naskah->Nama_ttd_atas_nama).'</td>
		// </tr>
		// <tr>
		// <td>&nbsp;</td>
		// <td width="50px">NOMOR</td>
		// <td width="10px">:</td>
		// <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
		// </tr>

		// <tr>

		// <td>&nbsp;</td>
		// <td>TANGGAL</td>
		// <td>:</td>
		// <td>Tanggal/Bulan/Tahun</td>
		// </tr>
		// <tr>
		// <td>&nbsp;</td>
		// <td>PERIHAL</td>
		// <td>:</td>
		// <td>'. $this->data['Hal'].'</td>
		// </tr>
		// </table>';


		// if (!empty($this->data['lampiran'] )) {
		// 	$pdf->AddPage('P', 'F4');
		// 	$pdf->SetX(95);
		// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);

		// 	$pdf->Ln(10);
		// 	$pdf->SetX(30);
		// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran'] , 0, 1, 0, true, 'J', true);
		// 	$r_atasan =  $this->session->userdata('roleatasan'); 
		// 	$r_biro =  $this->session->userdata('groleid');
		// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
		// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

		// 	$pdf->Ln(6);
		// 	if($this->data['TtdText'] == 'PLT') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// 	}elseif($this->data['TtdText'] == 'PLH') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

		// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
		// 	}else{
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
		// 	}
		// 	$pdf->Ln(2);
		// 	$pdf->SetX(130);
		// // $pdf->SetFont('Arial','',10);
		// 	$pdf->Cell(32,5,'PEMERIKSA',0,1,'L');

		// 	$pdf->Ln(5);

		// 	$pdf->SetX(100);
		// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
		// }

		// if (!empty($this->data['lampiran2'] )) {
		// 	$pdf->AddPage('P', 'F4');
		// 	$pdf->SetX(95);
		// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran2, '', 1, 0, false, 'J', false);
		// 	$pdf->Ln(10);
		// 	$pdf->SetX(30);
		// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran2'] , 0, 1, 0, true, 'J', true);
		// 	$r_atasan =  $this->session->userdata('roleatasan'); 
		// 	$r_biro =  $this->session->userdata('groleid');
		// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
		// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

		// 	$pdf->Ln(6);
		// 	if($this->data['TtdText'] == 'PLT') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// 	}elseif($this->data['TtdText'] == 'PLH') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

		// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
		// 	}else{
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
		// 	}
		// 	$pdf->Ln(2);
		// 	$pdf->SetX(130);
		// // $pdf->SetFont('Arial','',10);
		// 	$pdf->Cell(32,5,'PEMERIKSA',0,1,'L');

		// 	$pdf->Ln(5);

		// 	$pdf->SetX(100);
		// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

		// }

		// if (!empty($this->data['lampiran3'])) {

		// 	$pdf->AddPage('P', 'F4');
		// 	$pdf->SetX(95);
		// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran3, '', 1, 0, false, 'J', false);
		// 	$pdf->Ln(10);
		// 	$pdf->SetX(30);
		// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran3'] , 0, 1, 0, true, 'J', true);

		// 	$r_atasan =  $this->session->userdata('roleatasan'); 
		// 	$r_biro =  $this->session->userdata('groleid');
		// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
		// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

		// 	$pdf->Ln(6);
		// 	if($this->data['TtdText'] == 'PLT') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// 	}elseif($this->data['TtdText'] == 'PLH') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

		// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
		// 	}else{
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
		// 	}
		// 	$pdf->Ln(2);
		// 	$pdf->SetX(130);
		// // $pdf->SetFont('Arial','',10);
		// 	$pdf->Cell(32,5,'PEMERIKSA',0,1,'L');

		// 	$pdf->Ln(5);

		// 	$pdf->SetX(100);
		// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

		// }

		// if (!empty($this->data['lampiran4'])) {

		// 	$pdf->AddPage('P', 'F4');
		// 	$pdf->SetX(95);
		// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran4, '', 1, 0, false, 'J', false);
		// 	$pdf->Ln(10);
		// 	$pdf->SetX(30);
		// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran4'] , 0, 1, 0, true, 'J', true);


		// 	$r_atasan =  $this->session->userdata('roleatasan'); 
		// 	$r_biro =  $this->session->userdata('groleid');
		// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
		// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

		// 	$pdf->Ln(6);
		// 	if($this->data['TtdText'] == 'PLT') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// 	}elseif($this->data['TtdText'] == 'PLH') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

		// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
		// 	}else{
		// 		$pdf->SetX(100);
		// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
		// 	}
		// 	$pdf->Ln(2);
		// 	$pdf->SetX(130);
		// // $pdf->SetFont('Arial','',10);
		// 	$pdf->Cell(32,5,'PEMERIKSA',0,1,'L');

		// 	$pdf->Ln(5);

		// 	$pdf->SetX(100);
		// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);



		// }

		// $pdf->Output('surat_dinas_view.pdf','I');		


		$this->data['TglReg'] 			= $naskah->TglReg;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Approve_People3'] 	= $naskah->Approve_People3;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		// if ($naskah->Ket == 'outboxnotadinas') {
		// 	$this->load->library('Pdf');
		// 	$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

		// 	$this->db->select("*");
		// 	$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
		// 	$this->db->order_by('GroupId', 'ASC');
		// 	$this->db->order_by('Golongan', 'DESC');
		// 	$this->db->order_by('Eselon ', 'ASC');
		// 	$data_nota_dinas = $this->db->get('v_login')->result();

		// 	$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
		// 	$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
		// 	//tcpdf

		// 	$r_atasan =  $this->session->userdata('roleatasan');
		// 	$r_biro =  $this->session->userdata('groleid');
		// 	$age = $this->session->userdata('roleid');
		// 	if ($this->data['TtdText'] == 'PLT') {
		// 		$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} elseif ($this->data['TtdText'] == 'PLH') {
		// 		$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
		// 		$tte = 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',' . '<br>' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} else {
		// 		$tte = get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	}
		// 	$dari = penulisan_dinas(get_data_people('RoleName', $this->data['Approve_People']));

		// 	$table = '<table style="font-size:10px;" cellpadding="1px" nobr="true">
		// <tr nobr="true">
		// <td width="240px" style="text-align:center; line-height:15px;">' . $tte . '</td>
		// </tr>
		// </table>
		// <br>
		// <table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		// <tr nobr="true">
		// <td rowspan="7" width="60px" style="valign:bottom;">
		// <br><br>
		// <img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
		// <td width="180px">Ditandatangani secara elekronik oleh:</td>
		// </tr>
		// <tr nobr="true">
		// <td width="180px" style="text-align: left; font-size:9;">' . $tte . '</td>
		// </tr>
		// <tr nobr="true">
		// <td><br></td>
		// </tr>
		// <tr nobr="true">
		// <td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '</td>
		// </tr>
		// <tr nobr="true">
		// 		<td>' . $tujuan . '
		// 		</td>
		// 	</tr>
		// </table>';

		// 	$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		// 	$pdf->SetPrintHeader(false);
		// 	$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
		// 	$pdf->SetMargins(30, 30, 20);
		// 	$pdf->SetAutoPageBreak(TRUE, 25);
		// 	// 
		// 	$pdf->AddPage('P', 'F4');
		// 	if ($this->session->userdata('roleid') == 'uk.1') {
		// 		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 10, 160, 0, 'PNG', '', 'N');
		// 		$pdf->Ln(10);
		// 	} else {
		// 		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
		// 	}
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(0, 10, 'NOTA DINAS', 0, 0, 'C');
		// 	$pdf->Ln(15);
		// 	// $pdf->Cell(0,10,'NOMOR : .........../'.$data['ClCode'].'/'.$data['RoleCode'],0,0,'C');
		// 	$pdf->SetFont('Arial', '', 10);
		// 	$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$n = 1;
		// 	$RoleId_To_decode = explode(',', $this->data['RoleId_To']);
		// 	$jum_roleid_to = count($RoleId_To_decode);

		// 	foreach ($RoleId_To_decode as $r => $v) {
		// 		$xx1 =  $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleID = '" . $v . "'AND GroupId  IN('3','4','7') ")->row();
		// 		$str = penulisan_dinas($xx1->PeoplePosition);
				
		// 		if ($jum_roleid_to > 1) {
		// 			$pdf->SetX(63);
		// 			$pdf->Cell(5, 0, $n . ".", 0, 0, 'L');
		// 			if ($xx1->GroupId = 7) {
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 		} else {
		// 			$pdf->SetX(63);
		// 			if ($xx1->GroupId = 7) {
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 		}
		// 		$n++;
		// 	}

		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Dari', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->MultiCell(125, 5, $dari, 0, 'L', 0, 2, '', '', true);
		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
		// 	if (!empty($this->data['RoleId_Cc'])) {
		// 		$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 		$k = 1;
		// 		$RoleId_Cc_decode = explode(',', $this->data['RoleId_Cc']);
		// 		$jum_roleid_cc = count($RoleId_Cc_decode);
		// 		foreach ($RoleId_Cc_decode as $c => $cc) {
		// 			$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $cc . "'")->row()->RoleName;
		// 			$str = penulisan_dinas($xx1);
					
		// 			if ($jum_roleid_cc > 1) {
		// 				$pdf->SetX(63);
		// 				$pdf->Cell(5, 0, $k . ".", 0, 0, 'L');
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->SetX(63);
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 			$k++;
		// 		}
		// 	} else {
		// 		$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 		$pdf->SetX(63);
		// 		$pdf->MultiCell(125, 0, '-', 0, 'L', 0, 2, '', '', true);
		// 	}

		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Tanggal', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, get_bulan($this->data['TglReg']), 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Nomor', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, '.........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Sifat', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, $this->data['SifatId'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Lampiran', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, $this->data['Jumlah'] . ' ' . $this->data['MeasureUnitId'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Hal', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->MultiCell(125, 5, $this->data['Hal'], 0, '', 0, 1, '', '', true);
		// 	$complex_cell_border = array(
		// 		'B' => array('width' => 0.8, 'color' => array(0, 0, 0), 'dash' => '1', 'cap' => 'round'),
		// 	);
		// 	$pdf->Ln(0);
		// 	$pdf->Cell(160, 5, '', $complex_cell_border, 1, 'C', 0, '', 0);
		// 	$pdf->Ln(3);
		// 	// $pdf->MultiCell(160, 5, '', 'T', 'J', 0, 1, '' ,'', true);
		// 	// $pdf->writeHTML(, true, 0, true, true);
		// 	// $pdf->writeHTML($this->data['Konten'], true, 0, true, true, 'J');
		// 	$pdf->writeHTML($this->data['Konten'], true, false, true, false, '');
		// 	$pdf->Ln(10);
		// 	$pdf->SetX(100);
			
		// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);


		// 	if (!empty($naskah->lampiran)) {

		// 		$lampiran_decode = array_filter(json_decode($naskah->lampiran));

		// 		$no_lamp = 1;

		// 		$no_lamp = 1;
		// 		$jum_lamp = count($lampiran_decode);

		// 		foreach ($lampiran_decode as $isilamp) {
		// 			switch ($no_lamp) {
		// 				case 1:
		// 					if ($jum_lamp > 1) {
		// 						$lamp_romawi = "I";
		// 					} else {
		// 						$lamp_romawi = "";
		// 					}
		// 					break;
		// 				case 2:
		// 					$lamp_romawi = "II";
		// 					break;
		// 				case 3:
		// 					$lamp_romawi = "III";
		// 					break;
		// 				case 4:
		// 					$lamp_romawi = "IV";
		// 					break;
		// 				case 5:
		// 					$lamp_romawi = "V";
		// 					break;
		// 				case 6:
		// 					$lamp_romawi = "VI";
		// 					break;
		// 				case 7:
		// 					$lamp_romawi = "VII";
		// 					break;
		// 				case 8:
		// 					$lamp_romawi = "VIII";
		// 					break;
		// 				case 9:
		// 					$lamp_romawi = "IX";
		// 					break;
		// 				case 10:
		// 					$lamp_romawi = "X";
		// 					break;
		// 				default:
		// 					$lamp_romawi = "";
		// 			}
		// 			$header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
		// 				<tr>
		// 			        <td width="65px">Lampiran ' . $lamp_romawi . ' : </td>
		// 			        <td colspan="2" width="200px">Nota Dinas ' . $dari . '</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td width="50px">NOMOR</td>
		// 			        <td width="10px">:</td>
		// 			        <td width="140px">.../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'] . '</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td>TANGGAL</td>
		// 			        <td>:</td>
		// 			        <td>Tanggal/Bulan/Tahun</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td>PERIHAL</td>
		// 			        <td>:</td>
		// 			        <td>' . $this->data['Hal'] . '</td>
		// 			    </tr>
		// 			</table>';
		// 			# code...
		// 			$pdf->AddPage('P', 'F4');
		// 			$pdf->SetX(95);
		// 			$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);
		// 			$pdf->Ln(10);
		// 			$pdf->SetX(30);
		// 			$pdf->writeHTML($isilamp, true, 0, true, 0);
		// 			// $pdf->writeHTMLCell(161, 0, '', '', $isilamp, 0, 1, 0, true, '', true);
		// 			$pdf->SetX(100);
		// 			$pdf->MultiCell(90, 5, '', 0, 'C');

		// 			$pdf->Ln(2);
		// 			$pdf->Ln(5);
		// 			$pdf->SetX(100);
		// 			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
		// 			$no_lamp++;
		// 		}
		// 	}
		// 	$pdf->Output('surat_nota_dinas_view.pdf', 'I');
		// 	die();
		 if ($naskah->Ket == 'outboxsprint') {
			// $this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);
			$this->load->library('Pdf');
			// $this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_perintah = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf

			$r_atasan =  $this->session->userdata('roleatasan');
			$r_biro =  $this->session->userdata('groleid');
			$age = $this->session->userdata('roleid');
			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',' . '<br>' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']) . ',';
			}

			$table = '<table style="font-size:12px; line-height:10px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<br><br>
		<td width="240px" style="text-align:center;">Ditetapkan di ..............</td>
		</tr>
		<tr nobr="true">
		<td width="240px" style="text-align:center;">Pada tanggal ..................<br></td>
		</tr>
		<tr nobr="true">
		<td width="240px" style="text-align:center; line-height:15px;">' . $tte . '</td>
		</tr>
		</table>
		<br>
		<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td rowspan="7" width="60px" style="valign:bottom;">
		<br><br>
		<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
		<td width="180px">Ditandatangani secara elekronik oleh:</td>
		</tr>
		<tr nobr="true">
		<td width="180px" style="text-align: left; font-size:9;">' . $tte . '</td>
		</tr>
		<tr nobr="true">
		<td><br></td>
		</tr>
		<tr nobr="true">
		<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
		</td>
		</tr>
		<tr nobr="true">
		<td>' . $pangkat_ttd . '
		</td>
		</tr>
		</table>';

			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 25);

			$pdf->AddPage('P', 'F4');

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			$pdf->Ln(7);
			$pdf->Cell(0, 10, 'SURAT PERINTAH', 0, 0, 'C');
			$pdf->Ln(5);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(12);
			$pdf->Cell(25, 5, 'DASAR', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(130, 3, '', '', $this->data['Hal'], '', 1, 0, true, 'J', true);
			$pdf->Ln(3); // dari 5 ke 3
			$pdf->Cell(0, 5, 'MEMERINTAHKAN:', 0, 1, 'C');
			$pdf->Ln(3);
			$pdf->Cell(25, 5, 'Kepada', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$i = 0;
			foreach ($data_perintah as $row) {
				$i++;
				$pdf->SetX(60);
				$pdf->Cell(5, 5, $i . '. ', 0, 0, 'L');
				$pdf->SetX(65);
				$pdf->Cell(40, 5, 'Nama', 0, 0, 'L');
				$pdf->Cell(3, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(76, 5, $row->PeopleName, 0, 'L');
				$pdf->SetX(65);
				$pdf->Cell(40, 5, 'Pangkat/Golongan', 0, 0, 'L');
				$pdf->Cell(3, 5, ':', 0, 0, 'L');
				$pdf->Cell(76, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
				$pdf->SetX(65);
				$pdf->Cell(40, 5, 'NIP', 0, 0, 'L');
				$pdf->Cell(3, 5, ':', 0, 0, 'L');
				$pdf->Cell(76, 5, $row->NIP, 0, 1, 'L');
				$pdf->SetX(65);
				$pdf->Cell(40, 5, 'Jabatan', 0, 0, 'L');
				$pdf->Cell(3, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				$pdf->MultiCell(76, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			}
			$pdf->Ln(5);
			$pdf->Cell(25, 5, 'Untuk', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(130, 0, '', '', $this->data['Konten'], '', 1, 0, false, 'J', false);
			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			$pdf->Output('surat_perintah_view.pdf', 'I');
			die();


		//APPROVE
		} else if ($naskah->Ket == 'outboxinstruksigub') {

			$this->load->library('Pdf');
			// ob_start();

			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = 'uk.1' ")->row()->Header;
			$sql_gub = $this->db->query("SELECT PeopleName FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeopleName;
			$sql_namagub = $this->db->query("SELECT PeoplePosition FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeoplePosition;

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');

			$table = '<table style="border: 1px solid black;fonts-ize:9px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="60" height="60"></td>
			<td width="180px" style="text-align: left; font-size:9px;">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9px;">' . $sql_namagub . ',</td>
			</tr>
			<tr nobr="true">
			<td></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:9px;">' . $sql_gub . '
			</td>
			</tr>
			<tr nobr="true">

			</tr>
			</table>';


			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 168, 0, 'PNG');
			$pdf->Ln(13);
			$pdf->SetX(38);
			$pdf->Cell(0, 10, 'INSTRUKSI GUBERNUR JAWA BARAT', 0, 0, 'C');
			$pdf->Ln(7);
			$pdf->SetX(38);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(38);
			$pdf->Cell(0, 5, 'TENTANG', 0, 0, 'C');
			$pdf->Ln(7);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal'], 0, 1, 0, true, 'C', true);
			$pdf->Ln(2);
			$pdf->SetX(38);
			$pdf->Cell(0, 10, 'GUBERNUR JAWA BARAT', 0, 1, 'C');
			$pdf->Ln(3);
			$pdf->SetX(30);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal_pengantar'], 0, 1, 0, true, 'J', true);
			$pdf->Ln(5);
			$pdf->Cell(55, 5, 'Dengan ini meninstruksikan', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Ln(7);
			$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$i = 0;
			if (!empty($this->data['RoleId_To'])) {
				$RoleId_To_decode = explode(',', $this->data['RoleId_To']);


				foreach ($RoleId_To_decode as $lamp => $v) {
					$xx1 =  $this->db->query("SELECT PeoplePosition FROM v_login WHERE PeopleId = '" . $v . "'")->row()->PeoplePosition;

					$uc  = ucwords(strtolower($xx1));
					$str = str_replace('Dan', 'dan', $uc);
					$str = str_replace('Uptd', 'UPTD', $str);
					$str = str_replace('Dprd', 'DPRD', $str);

					$i++;
					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
					$pdf->SetX(70);
					$pdf->MultiCell(117, 5,  $str . "\n", 0, 'J', 0, 2, '', '', true);
				}
			}

			if (!empty($this->data['RoleId_To_2'])) {
				$RoleId_To_decode2 = json_decode($this->data['RoleId_To_2']);
				foreach ($RoleId_To_decode2 as $lamp => $v) {
					$i++;
					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
					$pdf->SetX(70);
					$pdf->MultiCell(117, 5,  $v . "\n", 0, 'J', 0, 2, '', '', true);
				}
			}


			$pdf->Ln(2);
			$pdf->Cell(30, 5, 'Untuk', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Ln(8);


			if (!empty($this->data['Konten'])) {
				$Konten_decode = json_decode($this->data['Konten']);
				$jum_lamp = count($Konten_decode);
				$no_lamp = 1;

				foreach ($Konten_decode as $isi_konten) {

					switch ($no_lamp) {
						case 1:
							$lamp_romawi = "KESATU";
							break;
						case 2:
							$lamp_romawi =  "KEDUA";
							break;
						case 3:
							$lamp_romawi =  "KETIGA";
							break;
						case 4:
							$lamp_romawi =  "KEEMPAT";
							break;
						case 5:
							$lamp_romawi =  "KELIMA";
							break;
						case 6:
							$lamp_romawi =  "KEENAM";
							break;
						case 7:
							$lamp_romawi =  "KETUJUH";
							break;

						case 8:
							return "KEDELAPAN";
							break;
					}

					$pdf->Cell(30, 5, $lamp_romawi, 0, 0, 'L');
					$pdf->Cell(5, 5, ':', 0, 0, 'L');
					$pdf->SetX(65);
					$pdf->writeHTMLCell(125, 0, '', '', $isi_konten, 0, 1, 0, true, 'J', true);
					$pdf->Ln(3);
					$no_lamp++;
				}
			}
			$pdf->Ln(3);
			$pdf->Cell(30, 5, 'Instruksi ini mulai berlaku pada tanggal ditetapkan.', 0, 1, 'L');
			$pdf->Ln(10);
			$pdf->SetX(125);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Ditetapkan di ..............', 0, 1, 'L');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Pada tanggal ..................', 0, 1, 'L');
			$pdf->Ln(2);
			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, $sql_namagub . ',', 0, 'C');
			}
			$pdf->Ln(5);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			$pdf->Output('surat_instruksi_view.pdf', 'I');
			die();

			//surat izin
		} else if ($naskah->Ket == 'outboxsuratizin') {
			// $this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);
			$this->load->library('Pdf');

			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', $this->data['RoleId_To']);
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_perintah = $this->db->get('v_login')->result();
			// var_dump($data_perintah);
			// die();
			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4','7') ")->row()->Pangkat;
			$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
			$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');

			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}

			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $tujuan . '
			</td>
			</tr>
			</table>';

			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 30);
			$pdf->AddPage('P', 'F4');
			if ($this->session->userdata('roleid') == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 10, 160, 0, 'PNG', '', 'N');
				$pdf->Ln(10);
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(3);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'SURAT IZIN', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'TENTANG', 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(48);
			$pdf->writeHTMLCell(130, 0, '', '', $this->data['Hal'] , 0, 1, 0, true, 'C', true);

			$pdf->Ln(5);
			$pdf->Cell(30, 9, 'Dasar', 0, 0, 'L');
			$pdf->Cell(5, 9, ':', 0, 0, 'L');

			if (!empty($this->data['Konten'])) {
				$Konten_decode = json_decode($this->data['Konten']);
				$i = a;
				foreach ($Konten_decode as $row) {
					$pdf->Ln(2);
					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
					$pdf->SetX(70);
					$pdf->MultiCell(117, 5, $row . "\n", 0, 'J', 0, 2, '', '', true);
					$i++;
				}
			}

			$pdf->Ln(3);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'MEMBERI IZIN :', 0, 0, 'C');
			$pdf->Ln(12);
			$pdf->Cell(30, 5, 'Kepada :', 0, 0, 'L');
			$pdf->Ln(3);
			$i = 0;
			foreach ($data_perintah as $row) {
				$i++;
				$pdf->Ln(6);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(110, 6, $row->PeopleName, 0, 'L');
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->NIP, 0, 1, 'L');
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				$pdf->MultiCell(110, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);

				$instansi = $this->db->query("SELECT GroleId FROM role WHERE RoleId = '" . $this->session->userdata('primaryroleid') . "'  ")->row()->GroleId;

				$instansi_nama = $this->db->query("SELECT GroleName FROM master_grole WHERE GroleId = '" . $instansi . "'  ")->row()->GroleName;

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Instansi', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($instansi_nama));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				$pdf->MultiCell(110, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Untuk', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->writeHTMLCell(110, 5, '', '', $this->data['Hal_pengantar'] , 0, 1, 0, true, 'J', true);
			}
			$pdf->Ln(12);
			$pdf->SetX(125);
			$pdf->Cell(30, 5, 'Ditetapkan di ..................', 0, 1, 'L');
			$pdf->SetX(120);
			$pdf->Cell(30, 5, 'Pada tanggal .....................', 0, 1, 'L');
			$pdf->Ln(4);
			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			}
			$pdf->Ln(2);
			$pdf->SetX(130);
			$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');
			$pdf->Ln(3);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('surat_izin_view.pdf', 'I');
			die();


			// Akhir Fungsi Surat izin

			//naskah belum approve
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;
			$this->db->select("*");
			$this->db->where_in('PeopleId', $this->data['RoleId_To']);
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');

			$data_perintah = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4','7') ")->row()->Pangkat;

			$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;

			$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');

			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}

			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $tujuan . '
			</td>
			</tr>
			</table>';

			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 25);
			// 
			$pdf->AddPage('P', 'F4');
			if ($this->session->userdata('roleid') == 'uk.1') {

				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 174, 0, 'PNG', '', 'N');
				$pdf->Ln(-3);
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(3);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'SURAT PERNYATAAN MELAKSANAKAN TUGAS', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(30, 5, 'Yang bertandatangan di bawah ini :', 0, 0, 'L');

			$pdf->Ln(5);
			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->MultiCell(85, 6, get_data_people('PeopleName', $this->data['Nama_ttd_pengantar']) . ',', 0, 'L');
			// if($this->data['TtdText'] == 'PLT') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }elseif($this->data['TtdText'] == 'PLH') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }elseif($this->data['TtdText2'] == 'Atas_Nama') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');

			// }else{
			// 	$pdf->SetX(97);
			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }
			$variable_nip	 		= $this->data['Approve_People'];
			$variable_nip3 			= $this->data['Approve_People3'];
			$variable_nip_ttd 		= $this->data['Nama_ttd_pengantar'];
			$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;
			// if($data['TtdText'] == 'PLT') {

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

			// }elseif($data['TtdText'] == 'PLH') {

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {

			// $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;
			// }else{

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;
			// }

			// $gol =$this->data['Approve_People'];
			$gol_ttd = $this->data['Nama_ttd_pengantar'];
			// $gol3 =$this->data['Approve_People3'];

			$golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			$pangkat_ttd_pengantar = $this->db->query("SELECT Pangkat FROM people WHERE PeopleId = '" .  $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			$jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			// if($data['TtdText'] == 'PLT') {

			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

			// }elseif($data['TtdText'] == 'PLH') {
			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {
			// $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;
			// }else{

			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;
			// }

			// $jab_ttd =$this->data['Approve_People'];
			// $jab_ttd3 =$this->data['Approve_People3'];

			// if($data['TtdText'] == 'PLT') {			


			// $jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

			// }elseif($data['TtdText'] == 'PLH') {
			// $jabatan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {
			// $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;
			// }else{

			// $jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;
			// }


			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');

			if($nipttd =='170592' ){
				$nipttd=' - ';				
			}else{				
				$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;
			}	

			$pdf->MultiCell(85, 6, $nipttd, 0, 'L');

			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Pangkat/Golongan', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');

			if(!empty($pangkat_ttd_pengantar )) {

				$pangkat_kosong=' / ';
			}else{
				$pangkat_kosong=' - ';
			}

			$pdf->Cell(85,5,$pangkat_ttd_pengantar.$pangkat_kosong.$golongan_ttd,0,1,'L');

			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$uc  = ucwords(strtolower($jabatan_ttd));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			// $pdf->MultiCell(85,5,$str,0,'J');
			$pdf->MultiCell(84, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);

			$pdf->Ln(3);
			$pdf->Cell(30, 5, 'Dengan ini menerangkan dengan sesungguhnya bahwa :', 0, 0, 'L');
			$pdf->Ln(6);
			$i = 0;
			foreach ($data_perintah as $row) {
				$i++;


				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');

				$pdf->MultiCell(85, 6, $row->PeopleName, 0, 'L');


				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->NIP, 0, 1, 'L');

				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Pangkat/Golongan', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');

				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(84, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			}
			$pdf->Ln(6);
			// $pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan'.' '.$this->data['Hal'].' '.'Nomor'.' '.$row->NIP.'' .' '.'terhitung'.' '.$row->NIP.''.' '.'telah nyata menjalankan tugas sebagai'.' '.$row->NIP.''.' '.'di'.' '.$row->NIP.'' ,0, 1, 0, true, 'J', true);
			$pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan' . ' ' . $this->data['Hal'] . '.', 0, 1, 0, true, 'J', true);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Konten'] . '', 0, 1, 0, true, 'J', true);
			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
			$pdf->Ln(5);

			$pdf->writeHTMLCell(160, 0, '', '', '&nbsp;&nbsp;&nbsp;&nbsp;Demikian Surat Pernyataan Melaksanakan Tugas ini dibuat dengan sesungguhnya dengan mengingat sumpah jabatan/pegawai negeri sipil dan apabila dikemudian hari isi surat pernyataan ini ternyata tidak benar yang berakibat kerugian bagi negara, maka saya bersedia menanggung kerugian tersebut.', 0, 1, 0, true, 'J', true);
			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
			$pdf->Ln(10);
			$pdf->SetX(128);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Tempat, tanggal, bulan dan tahun', 0, 1, 'C');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Ln(5);
			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			}
			$pdf->Ln(2);
			$pdf->SetX(130);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');
			$pdf->Ln(4);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			if (!empty($this->data['RoleId_Cc'])) {
				$pdf->Ln(12);
				$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Ln(5);
				$pdf->Cell(30, 5, '', 0, 0, 'L');
				$pdf->Cell(5, 5, '', 0, 0, 'L');
				$filter_RoleId_To = array_filter($this->data['RoleId_Cc']);
				$hitung = count($filter_RoleId_To);
				$i = 0;
				foreach ($filter_RoleId_To as $v) {
					$y = 'Yth';
					$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;
					$uc  = ucwords(strtolower($xx1));
					$str = str_replace('Dan', 'dan', $uc);
					$str = str_replace('Uptd', 'UPTD', $str);
					$str = str_replace('Dprd', 'DPRD', $str);
					$i++;

					if (!empty($hitung)) {
						if ($hitung >= 1) {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . "; \n", 0, 'J', 0, 2, '', '', true);
						} elseif ($i == $hitung) {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . ".\n", 0, 'J', 0, 2, '', '', true);
						} else {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . ";\n", 0, 'J', 0, 2, '', '', true);
						}
					} else {
						$$pdf->SetX(34);
						$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
						$pdf->SetX(42);
						$pdf->MultiCell(150, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
					}
				}
			}



			$pdf->Output('surat_pernyataan_tugas_view.pdf', 'I');
			die;
		} else if ($naskah->Ket == 'outboxsprintgub') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_supergub_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxundangan') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_undangan_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxedaran') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_edaran_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxkeluar') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_dinas_tindaklanjut_pdf', $this->data);

			//baru notadinas
		} else if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_tindaklanjut_pdf', $this->data);

			//akhir baru notadinas

		} else if ($naskah->Ket == 'outboxsket') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_ket = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf
			$age = $this->session->userdata('roleid');

			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}
			$PEMERIKSA='PEMERIKSA';
			$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td width="240px" style="text-align:center;">Tempat, tanggal, bulan dan tahun</td>
			</tr>
			<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $tte . ',</td>				
			</tr>
				<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $PEMERIKSA . '</td>				
			</tr>
			</table>
			<br>
			<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $pangkat_ttd . '
			</td>
			</tr>
			</table>';



			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 25);
			// 
			$pdf->AddPage('P', 'F4');

			if ($age == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
				$pdf->Ln(10);
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(5);
			$pdf->Cell(0, 10, 'SURAT KETERANGAN', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(70, 5, 'Yang bertandatangan di bawah ini : ', 0, 0, 'L');
			$pdf->Ln(7);
			$pdf->Cell(45, 5, 'a. Nama', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Cell(108, 5, $this->data['Nama_ttd_konsep'], 0, 0, 'L');
			$pdf->Ln(6);
			$pdf->Cell(45, 5, 'b. Jabatan', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->MultiCell(108, 5, ucwords(strtolower(get_data_people('RoleName', $this->data['Approve_People']))) . "\n", 0, 'J', 0, 2, '', '', true);
			$pdf->Ln(7);
			$pdf->Cell(67, 5, 'dengan ini menerangkan bahwa : ', 0, 0, 'L');
			$pdf->Ln(7);
			$i = 0;
			foreach ($data_ket as $row) {
				$i++;
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'a. Nama/NIP', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(108, 5, $row->PeopleName . ' / NIP.' . $row->NIP, 0, 'L');
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'b. Pangkat/Golongan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(108, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'c. Jabatan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(108, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
				$pdf->Ln(3);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'd. Maksud', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				// $pdf->MultiCell(108, 5, $this->data['Hal']."\n", 0, 'J', 0, 2, '' ,'', true);
				$pdf->writeHTMLCell(108, 0, '', '', $this->data['Hal'] . "\n", 0, 1, 0, false, 'J', false);
			}
			$pdf->Ln(7);
			$pdf->Cell(30, 5, 'Demikian Surat Keterangan ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
			$pdf->Ln(15);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('surat_keterangan_view.pdf', 'I');
			die();
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_ket = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf
			$age = $this->session->userdata('roleid');

			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}

			$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td width="240px" style="text-align:center;">Tempat, tanggal, bulan dan tahun</td>
			</tr>
			<tr nobr="true">
			<td width="240px" style="text-align:center;">' . $tte . ',</td>
			</tr>
			</table>
			<br>
			<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $tujuan . '
			</td>
			</tr>
			</table>';
			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 30);
			$pdf->AddPage('P', 'F4');

			if ($age == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 160, 0, 'PNG', '', 'N');
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(10);
			$pdf->Cell(0, 10, 'PENGUMUMAN', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->Cell(0, 10, 'TENTANG', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal'] . "\n", 0, 1, false, true, 'C', false);
			$pdf->Ln(10);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Konten'] . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(13);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('Pengumuman_view.pdf', 'I');
			die();
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_ket = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf
			$age = $this->session->userdata('roleid');

			//tcpdf
			$konten = json_decode($this->data['Konten']);

			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}
			$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td width="240px" style="text-align:center;">Tempat, tanggal, bulan dan tahun</td>
			</tr>
			<tr nobr="true">
			<td width="240px" style="text-align:center;">' . $tte . ',</td>
			</tr>
			</table>
			<br><br>
			<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $tujuan . '
			</td>
			</tr>
			</table>';


			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');
			if ($this->session->userdata('roleid') == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 160, 0, 'PNG', '', 'N');
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(10);
			$pdf->Cell(0, 10, 'REKOMENDASI', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(30, 5, 'Dasar', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(125, 0, '', '',  $this->data['Hal'] . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(5);
			$pdf->Cell(30, 0, 'Menimbang', 0, 0, 'L');
			$pdf->Cell(5, 0, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(125, 0, '', '', $konten->menimbang . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(5);
			$pdf->MultiCell(160, 5, $tte . ', memberikan rekomendasi kepada :', 0, 'L');
			$pdf->Ln(0);
			$i = 0;
			foreach ($data_ket as $row) {
				$i++;
				$pdf->SetX(30);
				$pdf->Cell(55, 5, 'a. Nama/obyek', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(95, 5, $row->PeopleName, 0, 'L');
				$pdf->Ln(0);
				$pdf->SetX(30);
				$pdf->Cell(55, 5, 'b. Jabatan/Tempat/Identitas', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(95, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			}
			$pdf->Ln(5);
			$pdf->Cell(30, 5, 'Untuk', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(125, 0, '', '', $konten->untuk . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(0);
			$pdf->Cell(30, 5, 'Demikian rekomendasi ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
			$pdf->Ln(10);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('surat_rekomendasi_view.pdf', 'I');
			die();
		} else {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_naskah_dinas_tindaklanjut_pdf', $this->data);
		}

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' 	=> 'p',
			'format' 		=> 'F4',
			'marges' 		=> array(30, 30, 20, 10),
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	//Akhir PDF naskah dinas tindaklanjut	


	public function view_naskah_koreksi_pdf($NId)
	{

		$naskah = $this->db->query("SELECT * FROM konsep_naskah_koreksi WHERE id_koreksi = '" . $NId . "' LIMIT 1")->row();
		$this->data['NId'] 				= $NId;
		$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
		$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_To_2'] 		= $naskah->RoleId_To_2;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['lampiran'] 		= $naskah->lampiran;
		$this->data['lampiran2'] 		= $naskah->lampiran2;
		$this->data['lampiran3'] 		= $naskah->lampiran3;
		$this->data['lampiran4'] 		= $naskah->lampiran4;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Alamat']			= $naskah->Alamat;
		$this->data['RoleCode'] 		= $naskah->RoleCode;
		$this->data['Number']			= $naskah->Number;
		$this->data['Hal_pengantar'] 	= $naskah->Hal_pengantar;
		$this->data['Hal'] 				= $naskah->Hal;
		$this->data['type_naskah'] 		= $naskah->type_naskah;
		$this->data['Nama_ttd_pengantar'] 		= $naskah->Nama_ttd_pengantar;


		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
			$this->data['Jumlah'] 			= $naskah->Jumlah;
			$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}

		$this->data['TglReg'] 			= $naskah->TglReg;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Approve_People3'] 	= $naskah->Approve_People3;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		// if ($naskah->Ket == 'outboxnotadinas') {
		// 	$this->load->library('Pdf');
		// 	$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

		// 	$this->db->select("*");
		// 	$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
		// 	$this->db->order_by('GroupId', 'ASC');
		// 	$this->db->order_by('Golongan', 'DESC');
		// 	$this->db->order_by('Eselon ', 'ASC');
		// 	$data_nota_dinas = $this->db->get('v_login')->result();

		// 	$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
		// 	$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
		// 	//tcpdf

		// 	$r_atasan =  $this->session->userdata('roleatasan');
		// 	$r_biro =  $this->session->userdata('groleid');
		// 	$age = $this->session->userdata('roleid');
		// 	if ($this->data['TtdText'] == 'PLT') {
		// 		$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} elseif ($this->data['TtdText'] == 'PLH') {
		// 		$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
		// 		$tte = 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',' . '<br>' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} else {
		// 		$tte = get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	}
		// 	$dari = penulisan_dinas(get_data_people('RoleName', $this->data['Approve_People']));

		// 	$table = '<table style="font-size:10px;" cellpadding="1px" nobr="true">
		// <tr nobr="true">
		// <td width="240px" style="text-align:center; line-height:15px;">' . $tte . '</td>
		// </tr>
		// </table>
		// <br>
		// <table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		// <tr nobr="true">
		// <td rowspan="7" width="60px" style="valign:bottom;">
		// <br><br>
		// <img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
		// <td width="180px">Ditandatangani secara elekronik oleh:</td>
		// </tr>
		// <tr nobr="true">
		// <td width="180px" style="text-align: left; font-size:9;">' . $tte . '</td>
		// </tr>
		// <tr nobr="true">
		// <td><br></td>
		// </tr>
		// <tr nobr="true">
		// <td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '</td>
		// </tr>
		// <tr nobr="true">
		// 		<td>' . $tujuan . '
		// 		</td>
		// 	</tr>
		// </table>';

		// 	$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		// 	$pdf->SetPrintHeader(false);
		// 	$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
		// 	$pdf->SetMargins(30, 30, 20);
		// 	$pdf->SetAutoPageBreak(TRUE, 25);
		// 	// 
		// 	$pdf->AddPage('P', 'F4');
		// 	if ($this->session->userdata('roleid') == 'uk.1') {
		// 		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 10, 160, 0, 'PNG', '', 'N');
		// 		$pdf->Ln(10);
		// 	} else {
		// 		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
		// 	}
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(0, 10, 'NOTA DINAS', 0, 0, 'C');
		// 	$pdf->Ln(15);
		// 	// $pdf->Cell(0,10,'NOMOR : .........../'.$data['ClCode'].'/'.$data['RoleCode'],0,0,'C');
		// 	$pdf->SetFont('Arial', '', 10);
		// 	$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$n = 1;
		// 	$RoleId_To_decode = explode(',', $this->data['RoleId_To']);
		// 	$jum_roleid_to = count($RoleId_To_decode);

		// 	foreach ($RoleId_To_decode as $r => $v) {
		// 		$xx1 =  $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleID = '" . $v . "'AND GroupId  IN('3','4','7') ")->row();
		// 		$str = penulisan_dinas($xx1->PeoplePosition);
				
		// 		if ($jum_roleid_to > 1) {
		// 			$pdf->SetX(63);
		// 			$pdf->Cell(5, 0, $n . ".", 0, 0, 'L');
		// 			if ($xx1->GroupId = 7) {
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 		} else {
		// 			$pdf->SetX(63);
		// 			if ($xx1->GroupId = 7) {
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 		}
		// 		$n++;
		// 	}

		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Dari', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->MultiCell(125, 5, $dari, 0, 'L', 0, 2, '', '', true);
		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
		// 	if (!empty($this->data['RoleId_Cc'])) {
		// 		$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 		$k = 1;
		// 		$RoleId_Cc_decode = explode(',', $this->data['RoleId_Cc']);
		// 		$jum_roleid_cc = count($RoleId_Cc_decode);
		// 		foreach ($RoleId_Cc_decode as $c => $cc) {
		// 			$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $cc . "'")->row()->RoleName;
		// 			$str = penulisan_dinas($xx1);
					
		// 			if ($jum_roleid_cc > 1) {
		// 				$pdf->SetX(63);
		// 				$pdf->Cell(5, 0, $k . ".", 0, 0, 'L');
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->SetX(63);
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 			$k++;
		// 		}
		// 	} else {
		// 		$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 		$pdf->SetX(63);
		// 		$pdf->MultiCell(125, 0, '-', 0, 'L', 0, 2, '', '', true);
		// 	}

		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Tanggal', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, get_bulan($this->data['TglReg']), 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Nomor', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, '.........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Sifat', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, $this->data['SifatId'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Lampiran', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, $this->data['Jumlah'] . ' ' . $this->data['MeasureUnitId'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Hal', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->MultiCell(125, 5, $this->data['Hal'], 0, '', 0, 1, '', '', true);
		// 	$complex_cell_border = array(
		// 		'B' => array('width' => 0.8, 'color' => array(0, 0, 0), 'dash' => '1', 'cap' => 'round'),
		// 	);
		// 	$pdf->Ln(0);
		// 	$pdf->Cell(160, 5, '', $complex_cell_border, 1, 'C', 0, '', 0);
		// 	$pdf->Ln(3);
		// 	// $pdf->MultiCell(160, 5, '', 'T', 'J', 0, 1, '' ,'', true);
		// 	$pdf->writeHTML($this->data['Konten'], true, 0, true, 0);
		// 	$pdf->Ln(10);
		// 	$pdf->SetX(100);
		// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);


		// 	if (!empty($naskah->lampiran)) {

		// 		$lampiran_decode = array_filter(json_decode($naskah->lampiran));

		// 		$no_lamp = 1;

		// 		$no_lamp = 1;
		// 		$jum_lamp = count($lampiran_decode);

		// 		foreach ($lampiran_decode as $isilamp) {
		// 			switch ($no_lamp) {
		// 				case 1:
		// 					if ($jum_lamp > 1) {
		// 						$lamp_romawi = "I";
		// 					} else {
		// 						$lamp_romawi = "";
		// 					}
		// 					break;
		// 				case 2:
		// 					$lamp_romawi = "II";
		// 					break;
		// 				case 3:
		// 					$lamp_romawi = "III";
		// 					break;
		// 				case 4:
		// 					$lamp_romawi = "IV";
		// 					break;
		// 				case 5:
		// 					$lamp_romawi = "V";
		// 					break;
		// 				case 6:
		// 					$lamp_romawi = "VI";
		// 					break;
		// 				case 7:
		// 					$lamp_romawi = "VII";
		// 					break;
		// 				case 8:
		// 					$lamp_romawi = "VIII";
		// 					break;
		// 				case 9:
		// 					$lamp_romawi = "IX";
		// 					break;
		// 				case 10:
		// 					$lamp_romawi = "X";
		// 					break;
		// 				default:
		// 					$lamp_romawi = "";
		// 			}
		// 			$header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
		// 				<tr>
		// 			        <td width="65px">Lampiran ' . $lamp_romawi . ' : </td>
		// 			        <td colspan="2" width="200px">Nota Dinas ' . $dari . '</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td width="50px">NOMOR</td>
		// 			        <td width="10px">:</td>
		// 			        <td width="140px">.../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'] . '</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td>TANGGAL</td>
		// 			        <td>:</td>
		// 			        <td>Tanggal/Bulan/Tahun</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td>PERIHAL</td>
		// 			        <td>:</td>
		// 			        <td>' . $this->data['Hal'] . '</td>
		// 			    </tr>
		// 			</table>';
		// 			# code...
		// 			$pdf->AddPage('P', 'F4');
		// 			$pdf->SetX(95);
		// 			$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);
		// 			$pdf->Ln(10);
		// 			$pdf->SetX(30);
		// 			$pdf->writeHTML($isilamp, true, 0, true, 0);
		// 			// $pdf->writeHTMLCell(161, 0, '', '', $isilamp, 0, 1, 0, true, '', true);
		// 			$pdf->SetX(100);
		// 			$pdf->MultiCell(90, 5, '', 0, 'C');

		// 			$pdf->Ln(2);
		// 			$pdf->Ln(5);
		// 			$pdf->SetX(100);
		// 			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
		// 			$no_lamp++;
		// 		}
		// 	}
		// 	$pdf->Output('surat_nota_dinas_view.pdf', 'I');
			
	 if ($naskah->Ket == 'outboxsprint') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxsprintgub') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_supergub_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxundangan') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_undangan_tindaklanjut_pdf', $this->data);



			// 	$this->load->library('Pdf');
			// 	$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;
			// 	$this->data['TglReg'] 				= $naskah->TglReg;
			// 	$this->data['Approve_People'] 		= $naskah->Approve_People;
			// 	$this->data['TtdText'] 				= $naskah->TtdText;
			// 	$this->data['Nama_ttd_konsep'] 		= $naskah->Nama_ttd_konsep;
			// 	$this->data['TtdText2'] 			= $naskah->TtdText2;
			// 	$this->data['Approve_People3'] 		= $naskah->Approve_People3;
			// 	$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
			// 	$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;		

			// 	$data_perintah = $this->db->get('v_login')->result();
			// // var_dump($data_perintah);
			// // die();
			// 	$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			// //tcpdf
			// 	$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $this->data['Nama_ttd_atas_nama']. "'")->row()->Pangkat;
			// 	$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			// 	$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
			// 	$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			// 	$age = $this->session->userdata('roleid') == 'uk.1';
			// 	$xx = $this->session->userdata('roleid');		
			// 	$r_atasan =  $this->session->userdata('roleatasan'); 

			// 	if($this->data['TtdText'] == 'PLT') {
			// 		$tte = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
			// 	}elseif($this->data['TtdText'] == 'PLH') {
			// 		$tte = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);
			// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			// 		$tte =get_data_people('RoleName', $this->data['Approve_People']);
			// 	// .'<br>'.get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']);
			// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			// 		$tte = 'a.n'.'. '.get_data_people('RoleName', $this->data['Approve_People3']).'<br>'.get_data_people('RoleName', $$r_atasan).','.'<br>'.'u.b.<br>'.get_data_people('RoleName',$this->data['Approve_People']).',';
			// 	}else{

			// 		$tte =get_data_people('RoleName', $this->data['Approve_People']);
			// 	}

			// 	$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			// 	<tr nobr="true">
			// 	<td rowspan="7" width="60px" style="valign:bottom;">
			// 	<br><br>
			// 	<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
			// 	<td width="180px">Ditandatangani secara elekronik oleh:</td>
			// 	</tr>
			// 	<tr nobr="true">
			// 	<td width="180px" style="text-align: left; font-size:9;">'.$tte.',</td>
			// 	</tr>
			// 	<tr nobr="true">
			// 	<td><br></td>
			// 	</tr>
			// 	<tr nobr="true">
			// 	<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
			// 	</td>
			// 	</tr>
			// 	<tr nobr="true">
			// 	<td>'.$pangkat_ttd. '
			// 	</td>
			// 	</tr>
			// 	</table>';

			// 	$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			// 	$pdf->SetPrintHeader(false);
			// 	$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
			// 	$pdf->SetMargins(30, 30, 30);
			// 	$pdf->SetAutoPageBreak(TRUE, 30);
			// // 
			// 	$pdf->AddPage('P', 'F4');

			// 	$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
			// 	$pdf->Ln(10);
			// 	$pdf->SetX(120);
			// 	$pdf->SetFont('Arial','',11);

			// 	$pdf->Cell(30,5,'Tempat / Tanggal / Bulan / Tahun ',0,1,'L');
			// 	$pdf->Ln(2);
			// 	$pdf->SetX(120);
			// // $pdf->SetFont('Arial','',10);
			// 	$pdf->Cell(30,5,'Kepada',0,1,'L');


			// 	$pdf->Cell(20,5,'Nomor',0,0,'L');
			// 	$pdf->Cell(5,5,':',0,0,'L');
			// 	$pdf->Cell(5,5,'.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'],0,0,'L');
			// 	$pdf->SetX(120);
			// 	$pdf->Cell(10,5,'Yth. ',0,0,'L');
			// 	$pdf->SetX(130);
			// 	$pdf->writeHTMLCell(60, 5, '', '', $this->data['RoleId_To'], '', 1, 0, true, 'L', true);
			// 	$pdf->SetX(126);
			// 	$pdf->Cell(10,5,'di ',0,0,'L');
			// // $pdf->SetX(130); ini dimatikan
			// 	$pdf->Ln(5);
			// 	$pdf->SetX(132);
			// 	$pdf->MultiCell(56,5,$this->data['Alamat'],0,'L');
			// $y_kanan = $pdf->GetY(); // ini tambahan
			// $pdf->Ln(5);
			// $pdf->SetY(58);
			// $pdf->Cell(20,5,'Sifat',0,0,'L');
			// $pdf->Cell(5,5,':',0,0,'L');
			// $pdf->Cell(60,5,$this->data['SifatId'],0,0,'L');
			// $pdf->SetX(130);
			// // $pdf->Cell(10,5,'di',0,0,'L');
			// $pdf->Ln(5);
			// $pdf->Cell(20,5,'Lampiran',0,0,'L');
			// $pdf->Cell(5,5,':',0,0,'L');
			// $pdf->Cell(60,5,$this->data['Jumlah'].' '.$this->data['MeasureUnitId'],0,0,'L');
			// $pdf->SetX(133);
			// $pdf->MultiCell(56,5,'',0,'L');
			// $pdf->Cell(20,5,'Hal',0,0,'L');
			// $pdf->Cell(5,5,':',0,0,'L');
			// $pdf->writeHTMLCell(60, 5, '', '', $this->data['Hal'], 0, 1, 0, true, 'L', true); // ini 0 jadi 1
			// $y_kiri = $pdf->GetY(); // ini tambahan
			// $pdf->Ln(25);	
			// $pdf->SetX(50);
			// // ini tambahan
			// if ($y_kiri > $y_kanan) {
			// 	$pdf->SetY($y_kiri + 10);
			// }else if ($y_kiri < $y_kanan){
			// 	$pdf->SetY($y_kanan + 10);
			// }
			// $pdf->SetX(50);
			// $pdf->writeHTMLCell(140, 0, '', '', $this->data['Konten'], 0, 1, 0, true, 'J', true);	
			// // $pdf->SetFont('Arial','',10);
			// $r_atasan =  $this->session->userdata('roleatasan'); 
			// $r_biro =  $this->session->userdata('groleid');
			// $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
			// $atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

			// $pdf->Ln(6);
			// if($this->data['TtdText'] == 'PLT') {
			// 	$pdf->SetX(100);
			// 	$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// }elseif($this->data['TtdText'] == 'PLH') {
			// 	$pdf->SetX(100);
			// 	$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// }elseif($this->data['TtdText2'] == 'Atas_Nama') {
			// 	$pdf->SetX(100);
			// 	$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 	$pdf->SetX(100);
			// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

			// }elseif($this->data['TtdText2'] == 'untuk_beliau') {
			// 	$pdf->SetX(100);
			// 	$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 	$pdf->SetX(100);
			// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
			// 	$pdf->SetX(100);
			// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
			// }else{
			// 	$pdf->SetX(100);
			// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
			// }
			// $pdf->Ln(2);
			// $pdf->SetX(130);
			// // $pdf->SetFont('Arial','',10);
			// $pdf->Cell(32,5,'PEMERIKSA',0,1,'L');
			// $pdf->Ln(5);		
			// $pdf->SetX(100);
			// $pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			// if (!empty($this->data['RoleId_Cc'] )) {


			// 	$pdf->Ln(12);
			// 	$pdf->Cell(30,5,'Tembusan',0,0,'L');
			// 	$pdf->Cell(5,5,':',0,0,'L');
			// 	$pdf->Ln(5);
			// 	$pdf->Cell(30,5,'',0,0,'L');
			// 	$pdf->Cell(5,5,'',0,0,'L');

			// 	$i = 0;
			// 	$exp = explode(',', $this->data['RoleId_Cc']);
			// 	$hitung = count($exp);

			// 	foreach( $exp as $v){
			// 		$y = 'Yth';
			// 		$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;			
			// 		$uc  = ucwords(strtolower($xx1));
			// 		$str = str_replace('Dan', 'dan', $uc);
			// 		$str = str_replace('Uptd', 'UPTD', $str);
			// 		$str = str_replace('Dprd', 'DPRD', $str);
			// 		$i++;
			// if (!empty($hitung)) {

			// 			if ($hitung > 1) {
			// 				# code...
			// 				if ($i == $hitung - 1) {
			// 					# code...
			// 					$pdf->Ln(1);
			// 					$pdf->SetX(30);
			// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
			// 					$pdf->SetX(34);
			// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
			// 					$pdf->SetX(42);
			// 					$pdf->MultiCell(150, 5, $str."; dan\n", 0, 'J', 0, 2, '' ,'', true);
			// 				} elseif($i == $hitung){
			// 					$pdf->Ln(1);
			// 					$pdf->SetX(30);
			// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
			// 					$pdf->SetX(34);
			// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
			// 					$pdf->SetX(42);
			// 					$pdf->MultiCell(150, 5, $str.".\n", 0, 'J', 0, 2, '' ,'', true);
			// 				}else{
			// 					$pdf->Ln(1);
			// 					$pdf->SetX(30);
			// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
			// 					$pdf->SetX(34);
			// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
			// 					$pdf->SetX(42);
			// 					$pdf->MultiCell(150, 5, $str.";\n", 0, 'J', 0, 2, '' ,'', true);
			// 				}

			// 			}else{
			// 				$$pdf->SetX(34);
			// 				$pdf->Cell(30,5,$y.'. ',0,0,'L');
			// 				$pdf->SetX(42);
			// 				$pdf->MultiCell(150, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
			// 			}


			// 		}	

			// 	}			
			// }

			// if (!empty($this->data['lampiran2'])) {
			// 	$lamp_romawi1= "I";
			// }

			// else{
			// 	$lamp_romawi1= "";
			// }

			// if  ($this->data['type_naskah'] == 'XxJyPn38Yh.2') {
			// 	$type_surat ="SURAT UNDANGAN ";          
			// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
			// 	$type_surat ="SURAT PANGGILAN ";  
			// }else { 
			// 	$type_surat ="SURAT ";
			// }

			// $header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
			// <tr>
			// <td width="65px">Lampiran '.$lamp_romawi1.' : </td>
			// <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName',$naskah->Nama_ttd_atas_nama).'</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td width="50px">NOMOR</td>
			// <td width="10px">:</td>
			// <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
			// </tr>

			// <tr>

			// <td>&nbsp;</td>
			// <td>TANGGAL</td>
			// <td>:</td>
			// <td>Tanggal/Bulan/Tahun</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td>PERIHAL</td>
			// <td>:</td>
			// <td>'. $this->data['Hal'].'</td>
			// </tr>
			// </table>';

			// $jum_lamp2 = $this->data['lampiran2'];
			// $no_lamp2 = 1;
			// if (!empty($this->data['lampiran2'])) {
			// 	switch ($no_lamp2){
			// 		case 1: 
			// 		$lamp_romawi2 = "II";

			// 		break;
			// 	}                                                         
			// }

			// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
			// 	$type_surat ="SURAT UNDANGAN ";          
			// }elseif ($this->data['type_naskah']  == 'XxJyPn38Yh.3') {
			// 	$type_surat ="SURAT PANGGILAN ";  
			// }else { 
			// 	$type_surat ="SURAT ";
			// }

			// $header_lampiran2 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
			// <tr>
			// <td width="65px">Lampiran '.$lamp_romawi2.' : </td>
			// <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName',$naskah->Nama_ttd_atas_nama).'</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td width="50px">NOMOR</td>
			// <td width="10px">:</td>
			// <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
			// </tr>   
			// <tr>

			// <td>&nbsp;</td>
			// <td>TANGGAL</td>
			// <td>:</td>
			// <td>Tanggal/Bulan/Tahun</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td>PERIHAL</td>
			// <td>:</td>
			// <td>'. $this->data['Hal'].'</td>
			// </tr>
			// </table>';

			// $jum_lamp3 = $this->data['lampiran3'];
			// $no_lamp3 = 1;
			// if (!empty($this->data['lampiran3'])) {
			// 	switch ($no_lamp3){
			// 		case 1: 
			// 		$lamp_romawi3 = "III";

			// 		break;
			// 	}                         

			// }

			// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
			// 	$type_surat ="SURAT UNDANGAN ";          
			// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
			// 	$type_surat ="SURAT PANGGILAN ";  
			// }else { 
			// 	$type_surat ="SURAT ";
			// }

			// $header_lampiran3 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
			// <tr>
			// <td width="65px">Lampiran '.$lamp_romawi3.' : </td>
			// <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName',$naskah->Nama_ttd_atas_nama).'</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td width="50px">NOMOR</td>
			// <td width="10px">:</td>
			// <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
			// </tr>

			// <tr>

			// <td>&nbsp;</td>
			// <td>TANGGAL</td>
			// <td>:</td>
			// <td>Tanggal/Bulan/Tahun</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td>PERIHAL</td>
			// <td>:</td>
			// <td>'. $this->data['Hal'].'</td>
			// </tr>
			// </table>';


			// $jum_lamp4 =$this->data['lampiran4'];
			// $no_lamp4 = 1;

			// if (!empty($this->data['lampiran4'])) {
			// 	switch ($no_lamp4){
			// 		case 1: 
			// 		$lamp_romawi4 ="IV";

			// 		break;
			// 	}                            

			// }

			// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
			// 	$type_surat ="SURAT UNDANGAN ";          
			// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
			// 	$type_surat ="SURAT PANGGILAN ";  
			// }else { 
			// 	$type_surat ="SURAT ";
			// }

			// $header_lampiran4 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
			// <tr>
			// <td width="65px">Lampiran '.$lamp_romawi4.' : </td>
			// <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName',$naskah->Nama_ttd_atas_nama).'</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td width="50px">NOMOR</td>
			// <td width="10px">:</td>
			// <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
			// </tr>

			// <tr>

			// <td>&nbsp;</td>
			// <td>TANGGAL</td>
			// <td>:</td>
			// <td>Tanggal/Bulan/Tahun</td>
			// </tr>
			// <tr>
			// <td>&nbsp;</td>
			// <td>PERIHAL</td>
			// <td>:</td>
			// <td>'. $this->data['Hal'].'</td>
			// </tr>
			// </table>';


			// if (!empty($this->data['lampiran'] )) {
			// 	$pdf->AddPage('P', 'F4');
			// 	$pdf->SetX(95);
			// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);

			// 	$pdf->Ln(10);
			// 	$pdf->SetX(30);
			// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran'] , 0, 1, 0, true, 'J', true);
			// 	$r_atasan =  $this->session->userdata('roleatasan'); 
			// 	$r_biro =  $this->session->userdata('groleid');
			// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
			// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

			// 	$pdf->Ln(6);
			// 	if($this->data['TtdText'] == 'PLT') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// 	}elseif($this->data['TtdText'] == 'PLH') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

			// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
			// 	}else{
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
			// 	}
			// 	$pdf->Ln(2);
			// 	$pdf->SetX(130);
			// // $pdf->SetFont('Arial','',10);
			// 	$pdf->Cell(32,5,'PEMERIKSA',0,1,'L');

			// 	$pdf->Ln(5);

			// 	$pdf->SetX(100);
			// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			// }

			// if (!empty($this->data['lampiran2'] )) {
			// 	$pdf->AddPage('P', 'F4');
			// 	$pdf->SetX(95);
			// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran2, '', 1, 0, false, 'J', false);
			// 	$pdf->Ln(10);
			// 	$pdf->SetX(30);
			// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran2'] , 0, 1, 0, true, 'J', true);
			// 	$r_atasan =  $this->session->userdata('roleatasan'); 
			// 	$r_biro =  $this->session->userdata('groleid');
			// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
			// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

			// 	$pdf->Ln(6);
			// 	if($this->data['TtdText'] == 'PLT') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// 	}elseif($this->data['TtdText'] == 'PLH') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

			// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
			// 	}else{
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
			// 	}
			// 	$pdf->Ln(2);
			// 	$pdf->SetX(130);
			// // $pdf->SetFont('Arial','',10);
			// 	$pdf->Cell(32,5,'PEMERIKSA',0,1,'L');

			// 	$pdf->Ln(5);

			// 	$pdf->SetX(100);
			// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			// }

			// if (!empty($this->data['lampiran3'])) {

			// 	$pdf->AddPage('P', 'F4');
			// 	$pdf->SetX(95);
			// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran3, '', 1, 0, false, 'J', false);
			// 	$pdf->Ln(10);
			// 	$pdf->SetX(30);
			// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran3'] , 0, 1, 0, true, 'J', true);

			// 	$r_atasan =  $this->session->userdata('roleatasan'); 
			// 	$r_biro =  $this->session->userdata('groleid');
			// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
			// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

			// 	$pdf->Ln(6);
			// 	if($this->data['TtdText'] == 'PLT') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// 	}elseif($this->data['TtdText'] == 'PLH') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

			// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
			// 	}else{
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
			// 	}
			// 	$pdf->Ln(2);
			// 	$pdf->SetX(130);
			// // $pdf->SetFont('Arial','',10);
			// 	$pdf->Cell(32,5,'PEMERIKSA',0,1,'L');

			// 	$pdf->Ln(5);

			// 	$pdf->SetX(100);
			// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			// }

			// if (!empty($this->data['lampiran4'])) {

			// 	$pdf->AddPage('P', 'F4');
			// 	$pdf->SetX(95);
			// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran4, '', 1, 0, false, 'J', false);
			// 	$pdf->Ln(10);
			// 	$pdf->SetX(30);
			// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran4'] , 0, 1, 0, true, 'J', true);

			// 	$r_atasan =  $this->session->userdata('roleatasan'); 
			// 	$r_biro =  $this->session->userdata('groleid');
			// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
			// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

			// 	$pdf->Ln(6);
			// 	if($this->data['TtdText'] == 'PLT') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// 	}elseif($this->data['TtdText'] == 'PLH') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

			// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$r_atasan).',',0,'C');
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');		
			// 	}else{
			// 		$pdf->SetX(100);
			// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
			// 	}
			// 	$pdf->Ln(2);
			// 	$pdf->SetX(130);
			// // $pdf->SetFont('Arial','',10);
			// 	$pdf->Cell(32,5,'PEMERIKSA',0,1,'L');

			// 	$pdf->Ln(5);

			// 	$pdf->SetX(100);
			// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			// }

			// $pdf->Output('surat_dinas_koreksi.pdf','I');	
		} else if ($naskah->Ket == 'outboxkeluar') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_dinas_koreksi_pdf', $this->data);

		} else if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_koreksi_pdf', $this->data);

		} else if ($naskah->Ket == 'outboxedaran') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_edaran_tindaklanjut_pdf', $this->data);

			//instruksi gubernur

		} else if ($naskah->Ket == 'outboxinstruksigub') {


			$this->load->library('Pdf');


			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = 'uk.1' ")->row()->Header;
			$sql_gub = $this->db->query("SELECT PeopleName FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeopleName;
			$sql_namagub = $this->db->query("SELECT PeoplePosition FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeoplePosition;

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');


			$table = '<table style="border: 1px solid black;fonts-ize:9px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td rowspan="7" width="60px" style="valign:bottom;">


		<img src="' . base_url('/uploads/kosong.jpg') . '" widht="60" height="60"></td>

		<td width="180px" style="text-align: left; font-size:9px;">Ditandatangani secara elekronik oleh:</td>
		</tr>
		<tr nobr="true">
		<td width="180px" style="text-align: left; font-size:9px;">' . $sql_namagub . ',</td>
		</tr>
		<tr nobr="true">
		<td></td>
		</tr>
		<tr nobr="true">
		<td style="font-size:9px;">' . $sql_gub . '
		</td>
		</tr>
		<tr nobr="true">

		</tr>
		</table>';


			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 168, 0, 'PNG');
			$pdf->Ln(13);
			$pdf->SetX(38);
			$pdf->Cell(0, 10, 'INSTRUKSI GUBERNUR JAWA BARAT', 0, 0, 'C');
			$pdf->Ln(7);
			$pdf->SetX(38);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(38);
			$pdf->Cell(0, 5, 'TENTANG', 0, 0, 'C');
			$pdf->Ln(7);
			$pdf->Cell(0, 5, $this->data['Hal'], 0, 0, 'C');
			$pdf->Ln(2);
			$pdf->SetX(38);
			$pdf->Cell(0, 10, 'GUBERNUR JAWA BARAT', 0, 1, 'C');
			$pdf->Ln(3);
			$pdf->SetX(30);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal_pengantar'], 0, 1, 0, true, 'J', true);
			$pdf->Ln(5);
			$pdf->Cell(55, 5, 'Dengan ini meninstruksikan', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Ln(7);
			$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');

			$i = 0;
			if (!empty($this->data['RoleId_To'])) {
				$RoleId_To_decode = explode(',', $this->data['RoleId_To']);


				foreach ($RoleId_To_decode as $lamp => $v) {
					$xx1 =  $this->db->query("SELECT PeoplePosition FROM v_login WHERE PeopleId = '" . $v . "'")->row()->PeoplePosition;

					$uc  = ucwords(strtolower($xx1));
					$str = str_replace('Dan', 'dan', $uc);
					$str = str_replace('Uptd', 'UPTD', $str);
					$str = str_replace('Dprd', 'DPRD', $str);

					$i++;
					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
					$pdf->SetX(70);
					$pdf->MultiCell(117, 5,  $str . "\n", 0, 'J', 0, 2, '', '', true);
				}
			}

			if (!empty($this->data['RoleId_To_2'])) {
				$RoleId_To_decode2 = json_decode($this->data['RoleId_To_2']);
				foreach ($RoleId_To_decode2 as $lamp => $v) {
					$i++;
					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
					$pdf->SetX(70);
					$pdf->MultiCell(117, 5,  $v . "\n", 0, 'J', 0, 2, '', '', true);
				}
			}

			$pdf->Ln(2);
			$pdf->Cell(30, 5, 'Untuk', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Ln(8);

			if (!empty($this->data['Konten'])) {
				$Konten_decode = json_decode($this->data['Konten']);
				$jum_lamp = count($Konten_decode);
				$no_lamp = 1;

				foreach ($Konten_decode as $isi_konten) {


					switch ($no_lamp) {
						case 1:
							$lamp_romawi = "KESATU";
							break;
						case 2:
							$lamp_romawi =  "KEDUA";
							break;
						case 3:
							$lamp_romawi =  "KETIGA";
							break;
						case 4:
							$lamp_romawi =  "KEEMPAT";
							break;
						case 5:
							$lamp_romawi =  "KELIMA";
							break;
						case 6:
							$lamp_romawi =  "KEENAM";
							break;
						case 7:
							$lamp_romawi =  "KETUJUH";
							break;

						case 8:
							return "KEDELAPAN";
							break;
					}

					$pdf->Cell(30, 5, $lamp_romawi, 0, 0, 'L');
					$pdf->Cell(5, 5, ':', 0, 0, 'L');
					$pdf->SetX(65);
					$pdf->writeHTMLCell(125, 0, '', '', $isi_konten, 0, 1, 0, true, 'J', true);
					$pdf->Ln(3);
					$no_lamp++;
				}
			}
			$pdf->Ln(3);
			$pdf->Cell(30, 5, 'Instruksi ini mulai berlaku pada tanggal ditetapkan.', 0, 1, 'L');
			$pdf->Ln(10);
			$pdf->SetX(125);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Ditetapkan di ..............', 0, 1, 'L');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Pada tanggal ..................', 0, 1, 'L');
			$pdf->Ln(2);
			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, $sql_namagub . ',', 0, 'C');
			}
			$pdf->Ln(5);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			$pdf->Output('surat_instruksi_koreksi_view.pdf', 'I');
			die();
			//koreksi
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;
			$this->db->select("*");
			$this->db->where_in('PeopleId', $this->data['RoleId_To']);
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');

			$data_perintah = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;

			$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');

			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}

			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $tujuan . '
			</td>
			</tr>
			</table>';

			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 25);
			// 
			$pdf->AddPage('P', 'F4');
			if ($this->session->userdata('roleid') == 'uk.1') {

				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 174, 0, 'PNG', '', 'N');
				$pdf->Ln(-3);
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(3);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'SURAT PERNYATAAN MELAKSANAKAN TUGAS', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(30, 5, 'Yang bertandatangan di bawah ini :', 0, 0, 'L');

			$pdf->Ln(5);
			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->MultiCell(85, 6, get_data_people('PeopleName', $this->data['Nama_ttd_pengantar']) . ',', 0, 'L');
			// if($this->data['TtdText'] == 'PLT') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }elseif($this->data['TtdText'] == 'PLH') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }elseif($this->data['TtdText2'] == 'Atas_Nama') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');

			// }else{
			// 	$pdf->SetX(97);
			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }



			$variable_nip	 		= $this->data['Approve_People'];
			$variable_nip3 			= $this->data['Approve_People3'];
			$variable_nip_ttd 		= $this->data['Nama_ttd_pengantar'];

			$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;

			// if($data['TtdText'] == 'PLT') {




			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

			// }elseif($data['TtdText'] == 'PLH') {

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {

			// $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;
			// }else{

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;
			// }

			// $gol =$this->data['Approve_People'];
			$gol_ttd = $this->data['Nama_ttd_pengantar'];
			// $gol3 =$this->data['Approve_People3'];

			$golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			$pangkat_ttd_pengantar = $this->db->query("SELECT Pangkat FROM people WHERE PeopleId = '" .  $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			$jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->PeoplePosition;


			// if($data['TtdText'] == 'PLT') {




			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

			// }elseif($data['TtdText'] == 'PLH') {
			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {
			// $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;
			// }else{

			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;
			// }

			// $jab_ttd =$this->data['Approve_People'];
			// $jab_ttd3 =$this->data['Approve_People3'];

			// if($data['TtdText'] == 'PLT') {			


			// $jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

			// }elseif($data['TtdText'] == 'PLH') {
			// $jabatan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {
			// $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;
			// }else{

			// $jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;
			// }


			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');

			if($nipttd =='170592' ){
				$nipttd=' - ';				
			}else{				
				$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;
			}	

			$pdf->MultiCell(85, 6, $nipttd, 0, 'L');

			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Pangkat/Golongan', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');

			if(!empty($pangkat_ttd_pengantar )) {

				$pangkat_kosong=' / ';
			}else{
				$pangkat_kosong=' - ';
			}

			$pdf->Cell(85,5,$pangkat_ttd_pengantar.$pangkat_kosong.$golongan_ttd,0,1,'L');

			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$uc  = ucwords(strtolower($jabatan_ttd));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			// $pdf->MultiCell(85,5,$str,0,'J');
			$pdf->MultiCell(84, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			$pdf->Ln(3);
			$pdf->Cell(30, 5, 'Dengan ini menerangkan dengan sesungguhnya bahwa :', 0, 0, 'L');
			$pdf->Ln(6);


			$i = 0;
			foreach ($data_perintah as $row) {
				$i++;


				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');

				$pdf->MultiCell(85, 6, $row->PeopleName, 0, 'L');


				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->NIP, 0, 1, 'L');

				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Pangkat/Golongan', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');

				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(84, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			}
			$pdf->Ln(6);

			// $pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan'.' '.$this->data['Hal'].' '.'Nomor'.' '.$row->NIP.'' .' '.'terhitung'.' '.$row->NIP.''.' '.'telah nyata menjalankan tugas sebagai'.' '.$row->NIP.''.' '.'di'.' '.$row->NIP.'' ,0, 1, 0, true, 'J', true);
			$pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan' . ' ' . $this->data['Hal'] . '.', 0, 1, 0, true, 'J', true);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Konten'] . '', 0, 1, 0, true, 'J', true);

			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
			$pdf->Ln(5);

			$pdf->writeHTMLCell(160, 0, '', '', '&nbsp;&nbsp;&nbsp;&nbsp;Demikian Surat Pernyataan Melaksanakan Tugas ini dibuat dengan sesungguhnya dengan mengingat sumpah jabatan/pegawai negeri sipil dan apabila dikemudian hari isi surat pernyataan ini ternyata tidak benar yang berakibat kerugian bagi negara, maka saya bersedia menanggung kerugian tersebut.', 0, 1, 0, true, 'J', true);
			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');


			$pdf->Ln(10);
			$pdf->SetX(128);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Tempat, tanggal, bulan dan tahun', 0, 1, 'C');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Ln(5);
			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			}
			$pdf->Ln(2);
			$pdf->SetX(130);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');
			$pdf->Ln(4);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			if (!empty($this->data['RoleId_Cc'])) {

				$pdf->Ln(12);
				$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Ln(5);
				$pdf->Cell(30, 5, '', 0, 0, 'L');
				$pdf->Cell(5, 5, '', 0, 0, 'L');
				$filter_RoleId_To = array_filter($this->data['RoleId_Cc']);
				$hitung = count($filter_RoleId_To);
				$i = 0;

				foreach ($filter_RoleId_To as $v) {

					$y = 'Yth';
					$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;
					$uc  = ucwords(strtolower($xx1));
					$str = str_replace('Dan', 'dan', $uc);
					$str = str_replace('Uptd', 'UPTD', $str);
					$str = str_replace('Dprd', 'DPRD', $str);
					$i++;

					if (!empty($hitung)) {
						if ($hitung >= 1) {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . "; \n", 0, 'J', 0, 2, '', '', true);
						} elseif ($i == $hitung) {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . ".\n", 0, 'J', 0, 2, '', '', true);
						} else {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . ";\n", 0, 'J', 0, 2, '', '', true);
						}
					} else {
						$$pdf->SetX(34);
						$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
						$pdf->SetX(42);
						$pdf->MultiCell(150, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
					}
				}
			}

			$pdf->Output('surat_pernyataan_tugas_view.pdf', 'I');

			// surat izin

		} else if ($naskah->Ket == 'outboxsuratizin') {

			// $this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', $this->data['RoleId_To']);
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_perintah = $this->db->get('v_login')->result();

			// var_dump($data_perintah);
			// die();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4','7') ")->row()->Pangkat;
			$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;


			$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
			$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');

			if ($data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->input->post('TtdText2') == 'Atas_Nama') {
				$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}

			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td rowspan="7" width="60px" style="valign:bottom;">
		<br><br>
		<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
		<td width="180px">Ditandatangani secara elekronik oleh:</td>
		</tr>
		<tr nobr="true">
		<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
		</tr>
		<tr nobr="true">
		<td><br></td>
		</tr>
		<tr nobr="true">
		<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
		</td>
		</tr>
		<tr nobr="true">
		<td>' . $tujuan . '
		</td>
		</tr>
		</table>';


			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');
			if ($this->session->userdata('roleid') == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 10, 160, 0, 'PNG', '', 'N');
				$pdf->Ln(10);
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(3);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'SURAT IZIN', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'TENTANG', 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(48);
			$pdf->writeHTMLCell(130, 0, '', '', $this->data['Hal'] , 0, 1, 0, true, 'C', true);

			$pdf->Ln(5);
			$pdf->Cell(30, 9, 'Dasar', 0, 0, 'L');
			$pdf->Cell(5, 9, ':', 0, 0, 'L');

			if (!empty($this->data['Konten'])) {
				$Konten_decode = json_decode($this->data['Konten']);
				$i = a;
				foreach ($Konten_decode as $row) {
					$pdf->Ln(2);

					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');

					$pdf->SetX(70);
					$pdf->MultiCell(117, 5, $row . "\n", 0, 'J', 0, 2, '', '', true);
					$i++;
				}
			}

			$pdf->Ln(3);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'MEMBERI IZIN :', 0, 0, 'C');
			$pdf->Ln(12);
			$pdf->Cell(30, 5, 'Kepada :', 0, 0, 'L');

			$pdf->Ln(3);
			$i = 0;
			foreach ($data_perintah as $row) {
				$i++;
				$pdf->Ln(6);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(110, 6, $row->PeopleName, 0, 'L');

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->NIP, 0, 1, 'L');

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(110, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);


				$instansi = $this->db->query("SELECT GroleId FROM role WHERE RoleId = '" . $this->session->userdata('primaryroleid') . "'  ")->row()->GroleId;

				$instansi_nama = $this->db->query("SELECT GroleName FROM master_grole WHERE GroleId = '" . $instansi . "'  ")->row()->GroleName;

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Instansi', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($instansi_nama));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(110, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Untuk', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				// $pdf->MultiCell(85,5,$str,0,'J');

				$pdf->writeHTMLCell(110, 5, '', '', $this->data['Hal_pengantar'] , 0, 1, 0, true, 'J', true);
			}
			$pdf->Ln(12);

			$pdf->SetX(125);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Ditetapkan di ..................', 0, 1, 'L');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Pada tanggal .....................', 0, 1, 'L');


			// $pdf->SetFont('Arial','',10);
			$pdf->Ln(4);
			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			}
			$pdf->Ln(2);
			$pdf->SetX(130);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');
			$pdf->Ln(3);

			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			$pdf->Output('surat_izin_view_koreksi.pdf', 'I');


} else if ($naskah->Ket == 'outboxsket') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_ket = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf
			$age = $this->session->userdata('roleid');

			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}
			$PEMERIKSA='PEMERIKSA';
			$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td width="240px" style="text-align:center;">Tempat, tanggal, bulan dan tahun</td>
			</tr>
			<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $tte . ',</td>				
			</tr>
				<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $PEMERIKSA . '</td>				
			</tr>
			</table>
			<br>
			<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $pangkat_ttd . '
			</td>
			</tr>
			</table>';



			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 25);
			// 
			$pdf->AddPage('P', 'F4');

			if ($age == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
				$pdf->Ln(10);
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(5);
			$pdf->Cell(0, 10, 'SURAT KETERANGAN', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(70, 5, 'Yang bertandatangan di bawah ini : ', 0, 0, 'L');
			$pdf->Ln(7);
			$pdf->Cell(45, 5, 'a. Nama', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Cell(108, 5, $this->data['Nama_ttd_konsep'], 0, 0, 'L');
			$pdf->Ln(6);
			$pdf->Cell(45, 5, 'b. Jabatan', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->MultiCell(108, 5, ucwords(strtolower(get_data_people('RoleName', $this->data['Approve_People']))) . "\n", 0, 'J', 0, 2, '', '', true);
			$pdf->Ln(7);
			$pdf->Cell(67, 5, 'dengan ini menerangkan bahwa : ', 0, 0, 'L');
			$pdf->Ln(7);
			$i = 0;
			foreach ($data_ket as $row) {
				$i++;
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'a. Nama/NIP', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(108, 5, $row->PeopleName . ' / NIP.' . $row->NIP, 0, 'L');
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'b. Pangkat/Golongan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(108, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'c. Jabatan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(108, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
				$pdf->Ln(3);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'd. Maksud', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				// $pdf->MultiCell(108, 5, $this->data['Hal']."\n", 0, 'J', 0, 2, '' ,'', true);
				$pdf->writeHTMLCell(108, 0, '', '', $this->data['Hal'] . "\n", 0, 1, 0, false, 'J', false);
			}
			$pdf->Ln(7);
			$pdf->Cell(30, 5, 'Demikian Surat Keterangan ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
			$pdf->Ln(15);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('surat_keterangan_view.pdf', 'I');
			

		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_ket = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf
			$age = $this->session->userdata('roleid');
			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}
			$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td width="240px" style="text-align:center;">Tempat, tanggal, bulan dan tahun</td>
		</tr>
		</table>
		<br><br>
		<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td rowspan="7" width="60px" style="valign:bottom;">
		<br><br>
		<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
		<td width="180px">Ditandatangani secara elekronik oleh:</td>
		</tr>
		<tr nobr="true">
		<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
		</tr>
		<tr nobr="true">
		<td><br></td>
		</tr>
		<tr nobr="true">
		<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
		</td>
		</tr>
		<tr nobr="true">
		<td>' . $pangkat_ttd . '
		</td>
		</tr>
		</table>';


			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');
			if ($this->session->userdata('roleid') == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 160, 0, 'PNG', '', 'N');
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(10);
			$pdf->Cell(0, 10, 'REKOMENDASI', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->Cell(0, 10, 'NOMOR : .........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'], 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(30, 5, 'Dasar', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(123, 0, '', '', $this->data['Hal'] . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(5);
			$pdf->Cell(30, 5, 'Menimbang', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(123, 0, '', '', $this->data['menimbang'] . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(5);
			$pdf->MultiCell(160, 5, $tte . ', memberikan rekomendasi kepada :', 0, 'L');
			$pdf->Ln(5);
			$i = 0;
			foreach ($data_ket as $row) {
				$i++;
				$pdf->SetX(30);
				$pdf->Cell(55, 5, 'a. Nama/obyek', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(95, 5, $row->PeopleName, 0, 'L');
				$pdf->Ln(5);
				$pdf->SetX(30);
				$pdf->Cell(55, 5, 'b. Jabatan/Tempat/Identitas', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(95, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			}
			$pdf->Ln(5);
			$pdf->Cell(30, 5, 'Untuk', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(123, 0, '', '', $this->data['untuk'] . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(10);
			$pdf->Cell(30, 5, 'Demikian rekomendasi ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
			$pdf->Ln(18);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('surat_rekomendasi_view.pdf', 'I');
		} else {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_naskah_dinas_tindaklanjut_pdf', $this->data);
		}

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' 	=> 'p',
			'format' 		=> 'F4',
			'marges' 		=> array(30, 30, 20, 10),
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	//Akhir PDF view koreksi	


	public function edit_naskahdinas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_dinas/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Naskah Dinas Edit
	public function edit_notadinas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/nota_dinas/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Naskah Dinas Edit
	public function edit_surattugas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_tugas/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Naskah Dinas Edit
	public function edit_suratedaran($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_edaran/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Naskah Dinas Edit
	public function edit_suratinstruksi($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_instruksi/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit



	//Naskah Dinas Edit
	public function edit_superpelaksanaantugas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Surat Pernyataan Melaksanakan Tugas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_m_tugas/edit', $this->data);
	}

	public function edit_surat_izin($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Surat Izin';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_izin/edit', $this->data);
	}
	//Edit naskah dinas
	public function postedit_notadinas($NId)
	{
		$gir = $NId;
		$tanggal = date('Y-m-d H:i:s');

		$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

		if ($naskah1->Ket != 'outboxsprint') {
			$sifat =  $this->input->post('SifatId');
		} else {
			$sifat =  NULL;
		}

		if ($naskah1->Ket == 'outboxkeluar') {
			$alamat =  $this->input->post('Alamat');

			$role = $this->input->post('RoleId_To');
			$konten = $this->input->post('Konten');
			//penambahan
			$lampiran =$this->input->post('lampiran');
			//akhir penambahan

		} elseif ($naskah1->Ket == 'outboxedaran') {
			$alamat =  $this->input->post('Alamat');
			$role = $this->input->post('RoleId_To');
			$konten = $this->input->post('Konten');
		} elseif ($naskah1->Ket == 'outboxinstruksigub') {
			$alamat =  $this->input->post('Alamat');
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$role2 = (!empty($this->input->post('RoleId_To_2')) ?  json_encode(array_filter($this->input->post('RoleId_To_2'))) : '');
			$konten = (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : '');
		} elseif ($naskah1->Ket == 'outboxsuratizin') {
			$alamat =  $this->input->post('Alamat');
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$konten = (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : '');

		} else {
			$alamat =  NULL;
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$konten = $this->input->post('Konten');
			$lampiran =(!empty($this->input->post('isi_lampiran')) ?  json_encode($this->input->post('isi_lampiran')) : '');
		}


		// } else {
		// 	$alamat =  '';
		// 	$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
		// 	$konten = $this->input->post('Konten');
		// }

		if ($naskah1->Ket == 'outboxkeluar') {
			$ket =  'outboxkeluar';
		} elseif ($naskah1->Ket == 'outboxedaran') {
			$ket =  'outboxedaran';
		} elseif ($naskah1->Ket == 'outboxnotadinas') {
			$ket =  'outboxnotadinas';
		} elseif ($naskah1->Ket == 'outboxsprint') {
			$ket =  'outboxsprint';
		} elseif ($naskah1->Ket == 'outboxinstruksigub') {
			$ket =  'outboxinstruksigub';
		} elseif ($naskah1->Ket == 'outboxsupertugas') {
			$ket =  'outboxsupertugas';
		} elseif ($naskah1->Ket == 'outboxsuratizin') {
			$ket =  'outboxsuratizin';
		} else {
		}


		try {
			$CI = &get_instance();
			$this->load->helper('string');

			$data_konsep = [
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'SifatId'       	=> $sifat,
				'Alamat'       		=> $alamat,
				'ClId'       		=> $this->input->post('ClId'),
				'RoleId_To'       	=> $role,
				'RoleId_To_2'       => $role2,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $konten,
				'lampiran'       	=> $lampiran,
			
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'       		=> $this->input->post('Hal_pengantar'),
				'Nama_ttd_pengantar' 		=> $this->input->post('Nama_ttd_pengantar'),
				'RoleCode' 					=> $this->input->post('RoleCode'),

			];

			//menyimpan record koreksi pada konsep naskah koreksi
			$type_naskah = $this->db->query("SELECT type_naskah FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->type_naskah;
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$data_konsep2 = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'id_koreksi'		=> $girid_ir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $role,
				'RoleId_To_2'       => $role2,
				'Alamat'       		=> $alamat,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $konten,
				'lampiran'       	=> $lampiran,
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Approve_People'       	=> $this->db->query("SELECT Approve_People FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People,
				'Approve_People3'    =>  $this->db->query("SELECT Approve_People3 FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People3,
				'TtdText'       	=> $this->db->query("SELECT TtdText FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText,
				'TtdText2'       	=>  $this->db->query("SELECT TtdText2 FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText2,
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'     => $this->input->post('Hal_pengantar'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> $ket,
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep'   => $this->db->query("SELECT Nama_ttd_konsep FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_konsep,

				'Nama_ttd_atas_nama'       	=> $this->db->query("SELECT Nama_ttd_atas_nama FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_atas_nama,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'type_naskah'			=> $type_naskah,
				'RoleCode' 			=> $this->input->post('RoleCode'),
				'Nama_ttd_pengantar' 	=> $this->input->post('Nama_ttd_pengantar'),

			];

			$this->db->where('NId_Temp', $NId);
			$this->db->update('konsep_naskah', $data_konsep);

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'RoleCode'       	=> $this->input->post('rolecode'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outboxkeluar',
					];
					$save_file = $this->model_inboxfile->store($save_files);
				}

				//penambahan koreksi
				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploadedkoreksi/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);
					}
				}
				//akhir penambahan koreksi
			}


			//penambahan koreksi2
			$save_konsep2 	= $this->model_konsepnaskah_koreksi->store($data_konsep2);
			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$save_recievers = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),

				'To_Id' 		=> $this->db->query("SELECT To_Id FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->To_Id,
				'RoleId_To'       	=> $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->RoleId_To,
				'id_koreksi'		=>	$girid_ir,
				'ReceiverAs' 	=> 'to_koreksi',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'read',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];
			$save_reciever2 = $this->model_inboxreciever_koreksi->store($save_recievers);
			//akhir penambahan koreksi2

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup Edit naskah dinas

	// public function postedit_notadinas($NId)
	// {
	// 	$gir = $NId;
	// 	$tanggal = date('Y-m-d H:i:s');

	// 	$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

	// 	if ($naskah1->Ket != 'outboxsprint') {
	// 		$sifat =  $this->input->post('SifatId');
	// 	} else {
	// 		$sifat =  NULL;
	// 	}

	// 	if ($naskah1->Ket == 'outboxkeluar') {
	// 		$alamat =  $this->input->post('Alamat');

	// 		$role = $this->input->post('RoleId_To');
	// 	} elseif ($naskah1->Ket == 'outboxedaran') {
	// 		$alamat =  $this->input->post('Alamat');
	// 		$role = $this->input->post('RoleId_To');
	// 	} else {
	// 		$alamat =  NULL;
	// 		$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
	// 	}

	// 	try {
	// 		$CI = &get_instance();
	// 		$this->load->helper('string');

	// 		$data_konsep = [
	// 			'Jumlah'       		=> $this->input->post('Jumlah'),
	// 			'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
	// 			'UrgensiId'       	=> $this->input->post('UrgensiId'),
	// 			'SifatId'       	=> $sifat,
	// 			'Alamat'       		=> $alamat,
	// 			'ClId'       		=> $this->input->post('ClId'),
	// 			'RoleId_To'       	=> $role,
	// 			'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
	// 			'Konten'       		=> $this->input->post('Konten'),
	// 			'lampiran'       	=> $this->input->post('lampiran'),
	// 			'Hal'       		=> $this->input->post('Hal'),
	// 		];

	// 		$this->db->where('NId_Temp', $NId);
	// 		$this->db->update('konsep_naskah', $data_konsep);

	// 		if (!empty($_POST['test_title_name'])) {

	// 			foreach ($_POST['test_title_name'] as $idx => $file_name) {
	// 				$test_title_name_copy = date('Ymd') . '-' . $file_name;

	// 				//untuk merename nama file 
	// 				rename(
	// 					FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
	// 					FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
	// 				);

	// 				//untuk menghapus folder upload sementara setelah simpan data dieksekusi
	// 				rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

	// 				$listed_image[] = $test_title_name_copy;

	// 				$save_files = [
	// 					'FileKey' 			=> tb_key(),
	// 					'GIR_Id' 			=> $gir,
	// 					'NId' 				=> $NId,
	// 					'PeopleID' 			=> $this->session->userdata('peopleid'),
	// 					'PeopleRoleID' 		=> $this->session->userdata('roleid'),
	// 					'RoleCode'       	=> $this->input->post('rolecode'),
	// 					'FileName_real' 	=> $file_name,
	// 					'FileName_fake' 	=> $test_title_name_copy,
	// 					'FileStatus' 		=> 'available',
	// 					'EditedDate'        => $tanggal,
	// 					'Keterangan' 		=> 'outboxkeluar',
	// 				];
	// 				$save_file = $this->model_inboxfile->store($save_files);
	// 			}
	// 		}

	// 		set_message('Data Berhasil Disimpan', 'success');
	// 		redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
	// 	} catch (\Exception $e) {
	// 		set_message('Gagal Menyimpan Data', 'error');
	// 		$this->load->library('user_agent');
	// 		redirect($this->agent->referrer());
	// 	}
	// }
	//Tutup Edit naskah dinas

	//hapus file notadinas registrasi
	public function hapus_file5($NId, $name_file)
	{

		$files = glob('./FilesUploaded/naskah/' . $name_file);
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file);
		}

		$this->db->query("DELETE FROM inbox_files WHERE NId = '" . $NId . "' AND FileName_fake = '" . $name_file . "'");

		set_message('Data Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//tutup hapus file notadinas registrasi

	//Hapus nota dinas registrasi
	public function hapus_nota_dinas_tindaklanjut($NId)
	{
		$query1 	= $this->db->query("SELECT NFileDir FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'");
		foreach ($query1->result_array() as $row1) :
			$fileDir = $row1['NFileDir'];
		endforeach;

		if ($fileDir != '') {

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $NId . "'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach ($query->result_array() as $row) :
				if (is_file($files . $row['FileName_fake']))
					unlink($files . $row['FileName_fake']);
			endforeach;
		}

		$this->db->query("DELETE FROM `konsep_naskah` WHERE NId_Temp = '" . $NId . "'");
		$this->db->query("DELETE FROM `inbox_files` WHERE NId = '" . $NId . "'");

		redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
	}
	//Akhir Hapus nota dinas registrasi

	/*// Approve Naskah
	
	// //Tutup Approve Naskah */
	public function approve_naskah()
	{
		$NId = $this->input->post('NId_Temp');
		$GIR_Id = $this->input->post('GIR_Id');

		//user number identification
		$nik = $this->session->userdata('nik');
		// $nik = '3273280809690005';

		// var_dump($NId);
		// die();


		//Tambahan kemsos
		//Cek no surat dulu, jika nomor surat beri ada maka tolah keluarkan pesan
		$konsep123 = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row();

		// var_dump($konsep123);
		// die();

		if ($konsep123->nosurat == '') {

			set_message('Nomor naskah wajib diisi, Silahkan berikan nomor naskah dahulu', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} else {

			$tanggal = date('Y-m-d H:i:s');

			$this->load->library('user_agent');
			get_data_konsepnaskah('NId_Temp', $NId);

			$password = $this->input->post('password');

			$kalimat = str_replace(' ', '', $password);

			if ($kalimat == '') {
				set_message('Passphrase Wajib Diisi', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			} else {

				//Proses cek NIK pada API Cloud BSRE
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => "http://103.122.5.59/api/user/status/" . $nik,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => array(
						"Authorization: Basic U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=",
						"Cookie: JSESSIONID="
					),
				));

				$response = curl_exec($curl);
				curl_close($curl);

				$result = json_decode($response);
				// var_dump($response);
				// die();


				if ($result->status_code == 1111) {

					//Jika NIK terdaftar, Cek Passphrase
					//Proses cek Passphrase pada API Cloud BSRE

					//url api sertifikat elektronik
					$url_signed = 'http://103.122.5.59/api/sign/pdf';

					//passphrase for the certificate
					$passphrase = $this->input->post('password');

					//curl initialization
					$curl = curl_init();

					$asal = FCPATH . 'FilesUploaded/example/example.pdf';

					curl_setopt_array($curl, array(
						CURLOPT_URL => $url_signed,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => array('file' => new CURLFILE($asal, 'application/pdf'), 'nik' => $nik, 'passphrase' => $passphrase, 'tampilan' => 'invisible', 'page' => '1', 'image' => 'false', 'linkQR' => 'https://google.com', 'xAxis' => '0', 'yAxis' => '0', 'width' => '200', 'height' => '100'),
						CURLOPT_HTTPHEADER => array(
							"Authorization: Basic U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=",
							"Cookie: JSESSIONID="
						),
					));

					$response = curl_exec($curl);
					//Fungsi untuk mengambil nilai http request
					$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
					//===
					curl_close($curl);

					$result = json_decode($response);

					// $x = $httpCode;
					// echo $x;
					// die();

					if ($httpCode == '200') {

						//Jika Passphrase Benar
						// $logopath = './uploads/anri.png'; //Logo yang akan di insert

						$CI = &get_instance();
						$this->load->library('ciqrcode');
						$this->load->helper('string');

						$config['cacheable']    = true; //boolean, the default is true
						$config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
						$config['errorlog']     = './uploads/'; //string, the default is application/logs/
						$config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
						$config['quality']      = true; //boolean, the default is true
						$config['size']         = '1024'; //interger, the default is 1024
						$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
						$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
						$CI->ciqrcode->initialize($config);

						$image_name				= $NId . '-' . date('Ymd') . '.png'; //buat name dari qr code sesuai dengan

						// $params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/').$NId; //data yang akan di jadikan QR CODE
						$params['data'] 		= base_url('verifikasi_dokumen');
						$params['level'] 		= 'H'; //H=High
						$params['size'] 		= 10;
						$params['savename'] 	= FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
						$CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

						$fullpath = FCPATH . $config['imagedir'] . $image_name;

						$QR = imagecreatefrompng($fullpath);
						// memulai menggambar logo dalam file qrcode
						$logo = imagecreatefromstring(file_get_contents($logopath));
						imagecolortransparent($logo, imagecolorallocatealpha($logo, 0, 0, 0, 127));

						imagealphablending($logo, false);
						imagesavealpha($logo, true);

						$QR_width = imagesx($QR); //get logo width
						$QR_height = imagesy($QR); //get logo width

						$logo_width = imagesx($logo);
						$logo_height = imagesy($logo);

						// Scale logo to fit in the QR Code
						$logo_qr_width = $QR_width / 4;
						$scale = $logo_width / $logo_qr_width;
						$logo_qr_height = $logo_height / $scale;

						//220 -> left,  210 -> top
						imagecopyresampled($QR, $logo, 220, 210, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
						// imagecopyresampled($QR, $logo, $QR_width/4, $QR_height/3.5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
						// 
						// Simpan kode QR lagi, dengan logo di atasnya

						imagepng($QR, $fullpath);

						$kode_verifikasi = random_string('sha1', 10);

						//Simpan QRCode
						$data_ttd = [
							'NId' 				=> $NId,
							'PeopleId' 			=> $this->session->userdata('peopleid'),
							'RoleId' 			=> $this->session->userdata('roleid'),
							'Verifikasi'  		=> $kode_verifikasi,
							'Identitas_Akses' 	=> '',
							'QRCode' 			=> $image_name,
							'TglProses' 		=> date('Y-m-d H:i:s'),
						];

						$save_ttd 		= $this->model_ttd->store($data_ttd);

						//Update Konsep = 0
						//Konsep 0 untuk sudah approve, 1 baru registrasi, 2 sudah dikirim

						//PERBAIKAN TGL NASKAH SESUAI PAK HIKMAN
						$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id', $GIR_Id)->update('konsep_naskah', ['Konsep' => 0, 'TglNaskah' => $tanggal]);
						//AKHIR SESUAI

						// $save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id', $GIR_Id)->update('konsep_naskah', ['Konsep' => 0]);

						//Create PDF File


						$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

						$this->data['NId'] 				= $NId;
						$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
						$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
						$this->data['RoleId_To'] 		= $naskah->RoleId_To;
						$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
						$this->data['Konten'] 			= $naskah->Konten;
						$this->data['RoleCode'] 		= $naskah->RoleCode;
						$this->data['Hal'] 				= $naskah->Hal;
						$this->data['Alamat'] 			= $naskah->Alamat;
						$this->data['Hal_pengantar'] 	= $naskah->Hal_pengantar;

						if ($naskah->Ket != 'outboxsprint') {
							$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
							$this->data['Jumlah'] 			= $naskah->Jumlah;
							$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
						}

						if ($naskah->Ket == 'outboxkeluar') {
							$this->data['Alamat'] 			= $naskah->Alamat;
						}

						$this->data['TglNaskah'] 		= $naskah->TglNaskah;
						$this->data['Approve_People'] 	= $naskah->Approve_People;
						$this->data['Approve_People3'] 	= $naskah->Approve_People3;
						$this->data['lampiran'] 		= $naskah->lampiran;
						$this->data['lampiran2'] 		= $naskah->lampiran2;
						$this->data['lampiran3'] 		= $naskah->lampiran3;
						$this->data['lampiran4'] 		= $naskah->lampiran4;
						$this->data['type_naskah'] 		= $naskah->type_naskah;

						$this->data['TtdText'] 			= $naskah->TtdText;
						$this->data['TtdText2'] 			= $naskah->TtdText2;
						$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
						$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
						$this->data['Number'] 			= $naskah->Number;
						$this->data['nosurat'] 			= $naskah->nosurat;
						$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;
						$this->data['lokasi'] 			= $naskah->lokasi;
						$this->data['id_dokumen'] 		= $kode_verifikasi;

						// if ($naskah->Ket == 'outboxnotadinas') {
						// 	$this->load->library('Pdf');
						// 	$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

						// 	$this->db->select("*");
						// 	$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
						// 	$this->db->order_by('GroupId', 'ASC');
						// 	$this->db->order_by('Golongan', 'DESC');
						// 	$this->db->order_by('Eselon ', 'ASC');
						// 	$data_nota_dinas = $this->db->get('v_login')->result();

						// 	$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
						// 	$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
						// 	//tcpdf

						// 	$r_atasan =  $this->session->userdata('roleatasan');
						// 	$r_biro =  $this->session->userdata('groleid');
						// 	$age = $this->session->userdata('roleid');
						// 	if ($this->data['TtdText'] == 'PLT') {
						// 		$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
						// 	} elseif ($this->data['TtdText'] == 'PLH') {
						// 		$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
						// 	} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
						// 		$tte = 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',' . '<br>' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
						// 	} else {
						// 		$tte = get_data_people('RoleName', $this->data['Approve_People']) . ',';
						// 	}
						// 	$dari = penulisan_dinas(get_data_people('RoleName', $this->data['Approve_People']));

						// 	if ($age1 == 'uk.1') {
						// 		$logottd = '/uploads/garuda.png';
						// 	} else {
						// 		$logottd = '/uploads/logoanri.jpg';
						// 	}

						// 	$table = '<table style="font-size:10px;" cellpadding="1px" nobr="true">
						// 	<tr nobr="true">
						// 	<td width="240px" style="text-align:center; line-height:15px;">' . $tte . '</td>
						// 	</tr>
						// 	<td width="240px" style="text-align:center; line-height:15px;"></td>
						// 	</tr>

						// 	</table>
						// 	<br>
						// 	<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
						// 	<tr nobr="true">
						// 	<td rowspan="7" width="60px" style="valign:bottom;">
						// 	<br><br>
						// 	<img src="" widht="55" height="60"></td>
						// 	<td width="180px">Ditandatangani secara elekronik oleh:</td>
						// 	</tr>
						// 	<tr nobr="true">
						// 	<td width="180px" style="text-align: left; font-size:9;">' . $tte . '</td>
						// 	</tr>
						// 	<tr nobr="true">
						// 	<td><br></td>
						// 	</tr>
						// 	<tr nobr="true">
						// 	<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '</td>
						// 	</tr>
						// 	<tr nobr="true">
						// 		<td>' . $tujuan . '
						// 		</td>
						// 	</tr>
						// 	</table>';

						// 	$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
						// 	$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
						// 	$pdf->SetPrintHeader(false);
						// 	$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
						// 	$pdf->SetMargins(30, 30, 20);
						// 	$pdf->SetAutoPageBreak(TRUE, 25);
						// 	// 
						// 	$pdf->AddPage('P', 'F4');
						// 	if ($this->session->userdata('roleid') == 'uk.1') {
						// 		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 10, 160, 0, 'PNG', '', 'N');
						// 		$pdf->Ln(10);
						// 	} else {
						// 		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
						// 	}
						// 	$pdf->Ln(5);
						// 	$pdf->Cell(0, 10, 'NOTA DINAS', 0, 0, 'C');
						// 	$pdf->Ln(15);
						// 	// $pdf->Cell(0,10,'NOMOR : .........../'.$data['ClCode'].'/'.$data['RoleCode'],0,0,'C');
						// 	$pdf->SetFont('Arial', '', 10);
						// 	$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
						// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 	$n = 1;
						// 	$RoleId_To_decode = explode(',', $this->data['RoleId_To']);
						// 	$jum_roleid_to = count($RoleId_To_decode);


						// 	foreach ($RoleId_To_decode as $r => $v) {
						// 		$xx1 =  $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleID = '" . $v . "'AND GroupId  IN('3','4','7') ")->row();
						// 		$str = penulisan_dinas($xx1->PeoplePosition);
						// 		if ($jum_roleid_to > 1) {
						// 			$pdf->SetX(63);
						// 			$pdf->Cell(5, 0, $n . ".", 0, 0, 'L');
						// 			if ($xx1->GroupId = 7) {
						// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
						// 			} else {
						// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
						// 			}
						// 		} else {
						// 			$pdf->SetX(63);
						// 			if ($xx1->GroupId = 7) {
						// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
						// 			} else {
						// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
						// 			}
						// 		}
						// 		$n++;
						// 	}
						// 	$pdf->Ln(0);
						// 	$pdf->Cell(30, 5, 'Dari', 0, 0, 'L');
						// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 	$pdf->MultiCell(125, 5, $dari, 0, 'L', 0, 2, '', '', true);
						// 	$pdf->Ln(0);
						// 	$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
						// 	if (!empty($this->data['RoleId_Cc'])) {
						// 		$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 		$k = 1;
						// 		$RoleId_Cc_decode = explode(',', $this->data['RoleId_Cc']);
						// 		$jum_roleid_cc = count($RoleId_Cc_decode);
						// 		foreach ($RoleId_Cc_decode as $c => $cc) {
						// 			$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $cc . "'")->row()->RoleName;
						// 			$str = penulisan_dinas($xx1);
									
						// 			if ($jum_roleid_cc > 1) {
						// 				$pdf->SetX(63);
						// 				$pdf->Cell(5, 0, $k . ".", 0, 0, 'L');
						// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
						// 			} else {
						// 				$pdf->SetX(63);
						// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
						// 			}
						// 			$k++;
						// 		}
						// 	} else {
						// 		$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 		$pdf->SetX(63);
						// 		$pdf->MultiCell(125, 0, '-', 0, 'L', 0, 2, '', '', true);
						// 	}

						// 	$pdf->Ln(0);
						// 	$pdf->Cell(30, 5, 'Tanggal', 0, 0, 'L');
						// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 	$pdf->Cell(125, 5, get_bulan($this->data['TglNaskah']), 0, 0, 'L');
						// 	$pdf->Ln(5);
						// 	$pdf->Cell(30, 5, 'Nomor', 0, 0, 'L');
						// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 	$pdf->Cell(125, 5, $this->data['nosurat'], 0, 0, 'L');
						// 	$pdf->Ln(5);
						// 	$pdf->Cell(30, 5, 'Sifat', 0, 0, 'L');
						// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 	$pdf->Cell(125, 5, $this->data['SifatId'], 0, 0, 'L');
						// 	$pdf->Ln(5);
						// 	$pdf->Cell(30, 5, 'Lampiran', 0, 0, 'L');
						// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 	$pdf->Cell(125, 5, $this->data['Jumlah'] . ' ' . $this->data['MeasureUnitId'], 0, 0, 'L');
						// 	$pdf->Ln(5);
						// 	$pdf->Cell(30, 5, 'Hal', 0, 0, 'L');
						// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
						// 	$pdf->MultiCell(125, 5, $this->data['Hal'], 0, '', 0, 1, '', '', true);
						// 	$complex_cell_border = array(
						// 		'B' => array('width' => 0.8, 'color' => array(0, 0, 0), 'dash' => '1', 'cap' => 'round'),
						// 	);
						// 	$pdf->Ln(0);
						// 	$pdf->Cell(160, 5, '', $complex_cell_border, 1, 'C', 0, '', 0);
						// 	$pdf->Ln(3);
						// 	// $pdf->MultiCell(160, 5, '', 'T', 'J', 0, 1, '' ,'', true);
						// 	$pdf->writeHTML($this->data['Konten'], true, 0, true, 0);
						// 	$pdf->Ln(10);
						// 	$pdf->SetX(100);
						// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

						// 	if (!empty($naskah->lampiran)) {

						// 		$lampiran_decode = array_filter(json_decode($naskah->lampiran));

						// 		$no_lamp = 1;
						// 		$jum_lamp = count($lampiran_decode);

						// 		foreach ($lampiran_decode as $isilamp) {
						// 			switch ($no_lamp) {
						// 				case 1:
						// 					if ($jum_lamp > 1) {
						// 						$lamp_romawi = "I";
						// 					} else {
						// 						$lamp_romawi = "";
						// 					}
						// 					break;
						// 				case 2:
						// 					$lamp_romawi = "II";
						// 					break;
						// 				case 3:
						// 					$lamp_romawi = "III";
						// 					break;
						// 				case 4:
						// 					$lamp_romawi = "IV";
						// 					break;
						// 				case 5:
						// 					$lamp_romawi = "V";
						// 					break;
						// 				case 6:
						// 					$lamp_romawi = "VI";
						// 					break;
						// 				case 7:
						// 					$lamp_romawi = "VII";
						// 					break;
						// 				case 8:
						// 					$lamp_romawi = "VIII";
						// 					break;
						// 				case 9:
						// 					$lamp_romawi = "IX";
						// 					break;
						// 				case 10:
						// 					$lamp_romawi = "X";
						// 					break;
						// 				default:
						// 					$lamp_romawi = "";
						// 			}
						// 			$header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
						// 		<tr>
						// 	        <td width="65px">Lampiran ' . $lamp_romawi . ' : </td>
						// 	        <td colspan="2" width="200px">Nota Dinas ' . $dari . '</td>
						// 	    </tr>
						// 	    <tr>
						// 	        <td>&nbsp;</td>
						// 	         <td width="50px">NOMOR</td>
						// 	        <td width="10px">:</td>
						// 	        <td width="140px">' . $this->data['nosurat'] . '</td>
						// 	    </tr>
							   
						// 	    <tr>
							       
						// 	        <td>&nbsp;</td>
						// 	        <td>TANGGAL</td>
						// 	        <td>:</td>
						// 	        <td>' . get_bulan($this->data['TglNaskah']) . '</td>
						// 	    </tr>
						// 	    <tr>
						// 	        <td>&nbsp;</td>
						// 	        <td>PERIHAL</td>
						// 	        <td>:</td>
						// 	        <td>' . $this->data['Hal'] . '</td>
						// 	    </tr>
						// 	</table>';
						// 			# code...
						// 			$pdf->AddPage('P', 'F4');
						// 			$pdf->SetX(95);
						// 			$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);
						// 			$pdf->Ln(10);
						// 			$pdf->SetX(30);
						// 			$pdf->writeHTML($isilamp, true, 0, true, 0);
						// 			// $pdf->writeHTMLCell(161, 0, '', '', $isilamp, 0, 1, 0, true, '', true);
						// 			$pdf->SetX(100);
						// 			$pdf->MultiCell(90, 5, '', 0, 'C');

						// 			$pdf->Ln(2);
						// 			$pdf->Ln(5);
						// 			$pdf->SetX(100);
						// 			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
						// 			$no_lamp++;
						// 		}
						// 	}
						if ($naskah->Ket == 'outboxsprint') {
							// $this->load->view('backend/standart/administrator/mail_tl/surattugas_inisiatif_pdf', $this->data);
							$this->load->library('Pdf');
							$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

							$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

							$this->db->select("*");
							$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
							$this->db->order_by('GroupId', 'ASC');
							$this->db->order_by('Golongan', 'DESC');
							$this->db->order_by('Eselon ', 'ASC');
							$data_perintah = $this->db->get('v_login')->result();

							//tcpdf
							$r_atasan =  $this->session->userdata('roleatasan');
							$r_biro =  $this->session->userdata('groleid');

							if ($age1 == 'uk.1') {
								$logottd = '/uploads/garuda.png';
							} else {
								$logottd = '/uploads/logoanri.jpg';
							}

							$age = $this->session->userdata('roleid');
							if ($this->data['TtdText'] == 'PLT') {
								$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
							} elseif ($this->data['TtdText'] == 'PLH') {
								$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
							} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
								$tte = 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',' . '<br>' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
							} else {
								$tte = get_data_people('RoleName', $this->data['Approve_People']) . ',';
							}

							$table = '<table style="font-size:12px; line-height:10px;" cellpadding="1px" nobr="true">
							<tr nobr="true">
							<br>
							<td width="240px" style="text-align:center;">Ditetapkan di ' . $this->data['lokasi'] . '</td>
							</tr>
							<tr nobr="true">
							<td width="240px" style="text-align:center;">Pada tanggal ' . get_bulan($this->data['TglNaskah']) . '<br></td>
							</tr>
							<tr nobr="true">
							<td width="240px" style="text-align:center; line-height:15px;">' . $tte . '</td>
							</tr>
							</table>
							<br>
							<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
							<tr nobr="true">
							<td rowspan="7" width="60px" style="valign:bottom;">
							<br><br>
							<img src="' . base_url($logottd) . '" widht="55" height="60"></td>
							<td width="180px">Ditandatangani secara elekronik oleh:</td>
							</tr>
							<tr nobr="true">
							<td width="180px" style="text-align: left; font-size:9;">' . $tte . '</td>
							</tr>
							<tr nobr="true">
							<td><br></td>
							</tr>
							<tr nobr="true">
							<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
							</td>
							</tr>
							<tr nobr="true">
							<td>' . $tujuan . '
							</td>
							</tr>
							</table>';

							$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
							$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
							$pdf->SetPrintHeader(false);
							$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
							$pdf->SetMargins(30, 30, 30);
							$pdf->SetAutoPageBreak(TRUE, 25);
							// 
							$pdf->AddPage('P', 'F4');

							$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
							$pdf->Ln(7); //
							$pdf->Cell(0, 10, 'SURAT PERINTAH', 0, 0, 'C');
							$pdf->Ln(5);
							$pdf->Cell(0, 10, 'NOMOR : ' . $this->data['nosurat'], 0, 0, 'C');
							$pdf->Ln(12);
							$pdf->Cell(25, 5, 'DASAR', 0, 0, 'L');
							$pdf->Cell(3, 5, ':', 0, 0, 'L');
							$pdf->writeHTMLCell(130, 5, '', '', $naskah->Hal, '', 1, 0, true, 'J', true);
							$pdf->Ln(3);
							$pdf->Cell(0, 10, 'MEMERINTAHKAN:', 0, 1, 'C');
							$pdf->Ln(3);
							$pdf->Cell(25, 5, 'Kepada', 0, 0, 'L');
							$pdf->Cell(3, 5, ':', 0, 0, 'L');
							$i = 0;

							foreach ($data_perintah as $row) {
								$i++;
								$pdf->SetX(60);
								$pdf->Cell(5, 5, $i . '. ', 0, 0, 'L');
								$pdf->SetX(65);
								$pdf->Cell(40, 5, 'Nama', 0, 0, 'L');
								$pdf->Cell(3, 5, ':', 0, 0, 'L');
								$pdf->MultiCell(76, 5, $row->PeopleName, 0, 'L');
								$pdf->SetX(65);
								$pdf->Cell(40, 5, 'Pangkat/Golongan', 0, 0, 'L');
								$pdf->Cell(3, 5, ':', 0, 0, 'L');
								$pdf->Cell(76, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
								$pdf->SetX(65);
								$pdf->Cell(40, 5, 'NIP', 0, 0, 'L');
								$pdf->Cell(3, 5, ':', 0, 0, 'L');
								$pdf->Cell(76, 5, $row->NIP, 0, 1, 'L');
								$pdf->SetX(65);
								$pdf->Cell(40, 5, 'Jabatan', 0, 0, 'L');
								$pdf->Cell(3, 5, ':', 0, 0, 'L');
								$uc  = ucwords(strtolower($row->PeoplePosition));
								$str = str_replace('Dan', 'dan', $uc);
								$str = str_replace('Uptd', 'UPTD', $str);
								$pdf->MultiCell(76, 3, $str . "\n", 0, 'J', 0, 2, '', '', true);
							}
							$pdf->Ln(5);
							$pdf->Cell(25, 5, 'Untuk', 0, 0, 'L');
							$pdf->Cell(3, 5, ':', 0, 0, 'L');
							$pdf->writeHTMLCell(130, 0, '', '', $this->data['Konten'], '', 1, 0, false, 'J', false);
							// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
							$pdf->Ln(3);
							$pdf->SetX(100);
							$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
						} else if ($naskah->Ket == 'outboxinstruksigub') {

							$this->load->library('Pdf');
							// ob_start();

							$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = 'uk.1' ")->row()->Header;
							$sql_gub = $this->db->query("SELECT PeopleName FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeopleName;
							$sql_namagub = $this->db->query("SELECT PeoplePosition FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeoplePosition;

							$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

							//tcpdf
							$age = $this->session->userdata('roleid') == 'uk.1';
							$xx = $this->session->userdata('roleid');

							if ($age == 'uk.1') {
								$logottd = '/uploads/garuda.png';
							} else {
								$logottd = '/uploads/logoanri.jpg';
							}
							$table = '<table style="border: 1px solid black;fonts-ize:9px;" cellpadding="1px" nobr="true">
							<tr nobr="true">
							<td rowspan="7" width="60px" style="valign:bottom;">

							<img src="' . base_url($logottd) . '" widht="60" height="65"></td>
							<td width="180px" style="text-align: left; font-size:9px;">Ditandatangani secara elekronik oleh:</td>
							</tr>
							<tr nobr="true">
							<td width="180px" style="text-align: left; font-size:9px;">' . $sql_namagub . ',</td>
							</tr>
							<tr nobr="true">
							<td></td>
							</tr>
							<tr nobr="true">
							<td style="font-size:9px;">' . $sql_gub . '
							</td>
							</tr>
							<tr nobr="true">

							</tr>
							</table>';


							$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
							$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
							$pdf->SetPrintHeader(false);
							$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
							$pdf->SetMargins(30, 30, 30);
							$pdf->SetAutoPageBreak(TRUE, 30);
							// 
							$pdf->AddPage('P', 'F4');

							$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 168, 0, 'PNG');
							$pdf->Ln(13);
							$pdf->SetX(38);
							$pdf->Cell(0, 10, 'INSTRUKSI GUBERNUR JAWA BARAT', 0, 0, 'C');
							$pdf->Ln(7);
							$pdf->SetX(38);
							$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
							$pdf->Ln(10);
							$pdf->SetX(38);
							$pdf->Cell(0, 5, 'TENTANG', 0, 0, 'C');
							$pdf->Ln(7);
							$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal'], 0, 1, 0, true, 'C', true);

							$pdf->Ln(2);
							$pdf->SetX(38);
							$pdf->Cell(0, 10, 'GUBERNUR JAWA BARAT', 0, 1, 'C');
							$pdf->Ln(3);
							$pdf->SetX(30);
							$pdf->writeHTMLCell(155, 0, '', '', $this->data['Hal_pengantar'], 0, 1, 0, true, 'C', true);
							$pdf->Ln(5);
							$pdf->Cell(55, 5, 'Dengan ini meninstruksikan', 0, 0, 'L');
							$pdf->Cell(5, 5, ':', 0, 0, 'L');
							$pdf->Ln(7);
							$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
							$pdf->Cell(5, 5, ':', 0, 0, 'L');

							$i = 0;
							if (!empty($this->data['RoleId_To'])) {
								$RoleId_To_decode = explode(',', $this->data['RoleId_To']);
								foreach ($RoleId_To_decode as $lamp => $v) {
									$xx1 =  $this->db->query("SELECT PeoplePosition FROM v_login WHERE PeopleId = '" . $v . "'")->row()->PeoplePosition;

									$uc  = ucwords(strtolower($xx1));
									$str = str_replace('Dan', 'dan', $uc);
									$str = str_replace('Uptd', 'UPTD', $str);
									$str = str_replace('Dprd', 'DPRD', $str);

									$i++;
									$pdf->SetX(65);
									$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
									$pdf->SetX(70);
									$pdf->MultiCell(117, 5,  $str . "\n", 0, 'J', 0, 2, '', '', true);
								}
							}

							if (!empty($this->data['RoleId_To_2'])) {
								$RoleId_To_decode2 = json_decode($this->data['RoleId_To_2']);
								foreach ($RoleId_To_decode2 as $lamp => $v) {
									$i++;
									$pdf->SetX(65);
									$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
									$pdf->SetX(70);
									$pdf->MultiCell(117, 5,  $v . "\n", 0, 'J', 0, 2, '', '', true);
								}
							}

							$pdf->Ln(2);
							$pdf->Cell(30, 5, 'Untuk', 0, 0, 'L');
							$pdf->Cell(5, 5, ':', 0, 0, 'L');
							$pdf->Ln(8);


							if (!empty($this->data['Konten'])) {
								$Konten_decode = json_decode($this->data['Konten']);
								$jum_lamp = count($Konten_decode);
								$no_lamp = 1;

								foreach ($Konten_decode as $isi_konten) {




									switch ($no_lamp) {
										case 1:
											$lamp_romawi = "KESATU";
											break;
										case 2:
											$lamp_romawi =  "KEDUA";
											break;
										case 3:
											$lamp_romawi =  "KETIGA";
											break;
										case 4:
											$lamp_romawi =  "KEEMPAT";
											break;
										case 5:
											$lamp_romawi =  "KELIMA";
											break;
										case 6:
											$lamp_romawi =  "KEENAM";
											break;
										case 7:
											$lamp_romawi =  "KETUJUH";
											break;

										case 8:
											return "KEDELAPAN";
											break;
									}

									$pdf->Cell(30, 5, $lamp_romawi, 0, 0, 'L');
									$pdf->Cell(5, 5, ':', 0, 0, 'L');
									$pdf->SetX(65);
									$pdf->writeHTMLCell(125, 0, '', '', $isi_konten, 0, 1, 0, true, 'J', true);
									$pdf->Ln(3);
									$no_lamp++;
								}
							}
							$pdf->Ln(3);
							$pdf->Cell(30, 5, 'Instruksi ini mulai berlaku pada tanggal ditetapkan.', 0, 1, 'L');
							$pdf->Ln(10);
							$pdf->SetX(125);
							// $pdf->SetFont('Arial','',10);
							$pdf->Cell(30, 5, 'Ditetapkan di ' . $this->data['lokasi'], 0, 1, 'L');
							$pdf->SetX(120);
							// $pdf->SetFont('Arial','',10);
							$pdf->Cell(30, 5, 'Pada tanggal ' . get_bulan($this->data['TglNaskah']), 0, 1, 'L');
							$pdf->Ln(2);
							if ($this->data['TtdText'] == 'PLT') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} elseif ($this->data['TtdText'] == 'PLH') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} else {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, $sql_namagub . ',', 0, 'C');
							}
							$pdf->Ln(5);
							$pdf->SetX(100);
							$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

							//approve

						} else if ($naskah->Ket == 'outboxsupertugas') {
							$this->data['Nama_ttd_pengantar'] 	= $naskah->Nama_ttd_pengantar;
							$this->load->library('Pdf');

							$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

							$this->db->select("*");
							$this->db->where_in('PeopleId', $this->data['RoleId_To']);
							$this->db->order_by('GroupId', 'ASC');
							$this->db->order_by('Golongan', 'DESC');
							$this->db->order_by('Eselon ', 'ASC');
							$data_perintah = $this->db->get('v_login')->result();

							// var_dump($data_perintah);
							// die();

							$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
							$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

							$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
							$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;
							//tcpdf
							$age = $this->session->userdata('roleid') == 'uk.1';
							$xx = $this->session->userdata('roleid');

							if ($age == 'uk.1') {
								$logottd = '/uploads/garuda.png';
							} else {
								$logottd = '/uploads/logoanri.jpg';
								if ($this->data['TtdText'] == 'PLT') {
									$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
								} elseif ($this->data['TtdText'] == 'PLH') {
									$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
								} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
									$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
								} else {
									$tte = get_data_people('RoleName', $this->data['Approve_People']);
								}
							}

							$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
								<tr nobr="true">
								<td rowspan="7" width="60px" style="valign:bottom;">
								<br><br>
								<img src="' . base_url($logottd) . '" widht="60" height="65"></td>
								<td width="180px">Ditandatangani secara elekronik oleh:</td>
								</tr>
								<tr nobr="true">
								<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
								</tr>
								<tr nobr="true">
								<td><br></td>
								</tr>
								<tr nobr="true">
								<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
								</td>
								</tr>
								<tr nobr="true">
								<td>' . $tujuan . '
								</td>
								</tr>
								</table>';

							$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);

							$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
							$pdf->SetPrintHeader(false);
							$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
							$pdf->SetMargins(30, 30, 30);
							$pdf->SetAutoPageBreak(TRUE, 30);		// 
							$pdf->AddPage('P', 'F4');
							$pdf->AddPage('P', 'F4');
							if ($this->session->userdata('roleid') == 'uk.1') {

								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 174, 0, 'PNG', '', 'N');
								$pdf->Ln(-3);
							} else {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
							}
							$pdf->Ln(3);
							$pdf->SetX(45);
							$pdf->Cell(0, 10, 'SURAT PERNYATAAN MELAKSANAKAN TUGAS', 0, 0, 'C');
							$pdf->Ln(6);
							$pdf->SetX(45);
							$pdf->Cell(0, 10, 'NOMOR :' . $naskah->nosurat, 0, 0, 'C');
							$pdf->Ln(15);
							$pdf->Cell(30, 5, 'Yang bertandatangan di bawah ini :', 0, 0, 'L');
							$pdf->Ln(5);
							$pdf->SetX(46);
							$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
							$pdf->SetX(92);
							$pdf->Cell(5, 5, ':', 0, 0, 'L');
							$pdf->MultiCell(85, 6, get_data_people('PeopleName', $this->data['Nama_ttd_pengantar']) . ',', 0, 'L');

							// if($this->data['TtdText'] == 'PLT') {

							// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
							// }elseif($this->data['TtdText'] == 'PLH') {

							// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
							// }elseif($this->data['TtdText2'] == 'Atas_Nama') {

							// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');

							// }else{
							// 	$pdf->SetX(97);
							// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
							// }
							$variable_nip_ttd 		= $this->data['Nama_ttd_pengantar'];
							$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;

							$gol_ttd = $this->data['Nama_ttd_pengantar'];
							// $gol3 =$this->data['Approve_People3'];
							$golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Golongan;
							$pangkat_ttd_pengantar = $this->db->query("SELECT Pangkat FROM people WHERE PeopleId = '" .  $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
							$jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->PeoplePosition;

							$pdf->SetX(46);
							$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
							$pdf->SetX(92);
							$pdf->Cell(5, 5, ':', 0, 0, 'L');
							
							if($nipttd =='170592' ){
								$nipttd=' - ';				
							}else{				
								$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;
							}	

							$pdf->MultiCell(85, 6, $nipttd, 0, 'L');
							$pdf->SetX(46);
							$pdf->Cell(25, 5, 'Pangkat/Golongan', 0, 0, 'L');
							$pdf->SetX(92);
							$pdf->Cell(5, 5, ':', 0, 0, 'L');

							if(!empty($pangkat_ttd_pengantar )) {

								$pangkat_kosong=' / ';
							}else{
								$pangkat_kosong=' - ';
							}

							$pdf->Cell(85,5,$pangkat_ttd_pengantar.$pangkat_kosong.$golongan_ttd,0,1,'L');
						
							$pdf->SetX(46);
							$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
							$pdf->SetX(92);
							$pdf->Cell(5, 5, ':', 0, 0, 'L');
							$uc  = ucwords(strtolower($jabatan_ttd));
							$str = str_replace('Dan', 'dan', $uc);
							$str = str_replace('Uptd', 'UPTD', $str);
							// $pdf->MultiCell(85,5,$str,0,'J');
							$pdf->MultiCell(84, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);

							$pdf->Ln(3);
							$pdf->Cell(30, 5, 'Dengan ini menerangkan dengan sesungguhnya bahwa :', 0, 0, 'L');
							$pdf->Ln(6);
							$i = 0;
							foreach ($data_perintah as $row) {
								$i++;
								$pdf->SetX(46);
								$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
								$pdf->SetX(92);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->MultiCell(85, 6, $row->PeopleName, 0, 'L');

								$pdf->SetX(46);
								$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
								$pdf->SetX(92);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->Cell(85, 5, $row->NIP, 0, 1, 'L');

								$pdf->SetX(46);
								$pdf->Cell(25, 5, 'Pangkat/Golongan', 0, 0, 'L');
								$pdf->SetX(92);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->Cell(85, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');

								$pdf->SetX(46);
								$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
								$pdf->SetX(92);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$uc  = ucwords(strtolower($row->PeoplePosition));
								$str = str_replace('Dan', 'dan', $uc);
								$str = str_replace('Uptd', 'UPTD', $str);
								// $pdf->MultiCell(85,5,$str,0,'J');
								$pdf->MultiCell(84, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
							}
							$pdf->Ln(6);

							// $pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan'.' '.$this->data['Hal'].' '.'Nomor'.' '.$row->NIP.'' .' '.'terhitung'.' '.$row->NIP.''.' '.'telah nyata menjalankan tugas sebagai'.' '.$row->NIP.''.' '.'di'.' '.$row->NIP.'' ,0, 1, 0, true, 'J', true);
							$pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan' . ' ' . $this->data['Hal'] . '.', 0, 1, 0, true, 'J', true);
							$pdf->writeHTMLCell(160, 0, '', '', $this->data['Konten'] . '.', 0, 1, 0, true, 'J', true);

							// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
							$pdf->Ln(5);
							$pdf->writeHTMLCell(160, 0, '', '', '&nbsp;&nbsp;&nbsp;&nbsp;Demikian Surat Pernyataan Melaksanakan Tugas ini dibuat dengan sesungguhnya dengan mengingat sumpah jabatan/pegawai negeri sipil dan apabila dikemudian hari isi surat pernyataan ini ternyata tidak benar yang berakibat kerugian bagi negara, maka saya bersedia menanggung kerugian tersebut.', 0, 1, 0, true, 'J', true);
							// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
							$pdf->Ln(10);
							$pdf->SetX(128);
							// $pdf->SetFont('Arial','',10);
							$pdf->Cell(30, 5, '' . $this->data['lokasi'] . ',' . ' ' . get_bulan($this->data['TglNaskah']), 0, 1, 'C');
							$pdf->SetX(120);

							// $pdf->SetFont('Arial','',10);
							$pdf->Ln(5);
							if ($this->data['TtdText'] == 'PLT') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} elseif ($this->data['TtdText'] == 'PLH') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} else {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							}

							$pdf->Ln(4);
							$pdf->SetX(100);
							$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
							if (!empty($this->data['RoleId_Cc'])) {

								$pdf->Ln(12);
								$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->Ln(5);
								$pdf->Cell(30, 5, '', 0, 0, 'L');
								$pdf->Cell(5, 5, '', 0, 0, 'L');
								$filter_RoleId_To = array_filter($this->data['RoleId_Cc']);
								$hitung = count($filter_RoleId_To);
								$i = 0;

								foreach ($filter_RoleId_To as $v) {

									$y = 'Yth';
									$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;
									$uc  = ucwords(strtolower($xx1));
									$str = str_replace('Dan', 'dan', $uc);
									$str = str_replace('Uptd', 'UPTD', $str);
									$str = str_replace('Dprd', 'DPRD', $str);
									$i++;

									if (!empty($hitung)) {
										if ($hitung >= 1) {
											$pdf->Ln(1);
											$pdf->SetX(30);
											$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
											$pdf->SetX(34);
											$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
											$pdf->SetX(42);
											$pdf->MultiCell(150, 5, $str . "; \n", 0, 'J', 0, 2, '', '', true);
										} elseif ($i == $hitung) {
											$pdf->Ln(1);
											$pdf->SetX(30);
											$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
											$pdf->SetX(34);
											$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
											$pdf->SetX(42);
											$pdf->MultiCell(150, 5, $str . ".\n", 0, 'J', 0, 2, '', '', true);
										} else {
											$pdf->Ln(1);
											$pdf->SetX(30);
											$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
											$pdf->SetX(34);
											$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
											$pdf->SetX(42);
											$pdf->MultiCell(150, 5, $str . ";\n", 0, 'J', 0, 2, '', '', true);
										}
									} else {
										$$pdf->SetX(34);
										$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
										$pdf->SetX(42);
										$pdf->MultiCell(150, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
									}
								}
							}
						} else if ($naskah->Ket == 'outboxsket') {
							$this->load->library('Pdf');
							$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

							$this->db->select("*");
							$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
							$this->db->order_by('GroupId', 'ASC');
							$this->db->order_by('Golongan', 'DESC');
							$this->db->order_by('Eselon ', 'ASC');
							$data_ket = $this->db->get('v_login')->result();

							$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
							$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
							//tcpdf
							$age = $this->session->userdata('roleid');
							if ($age == 'uk.1') {
								$logottd = '/uploads/garuda.png';
							} else {
								$logottd = '/uploads/logoanri.jpg';
							}

							if ($this->data['TtdText'] == 'PLT') {
								$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
							} elseif ($this->data['TtdText'] == 'PLH') {
								$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
							} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
								$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
							} else {
								$tte = get_data_people('RoleName', $this->data['Approve_People']);
							}

							$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
								<tr nobr="true">
								<td width="240px" style="text-align:center;">' . $this->data['lokasi'] . ', ' . get_bulan($this->data['TglNaskah']) . '</td>
								</tr>
								<tr nobr="true">
								<td width="240px" style="text-align:center;">' . $tte . ',</td>
								</tr>
								</table>
								<br>
								<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
								<tr nobr="true">
								<td rowspan="7" width="60px" style="valign:bottom;">
								<br><br>
								<img src="' . base_url($logottd) . '" widht="55" height="60"></td>
								<td width="180px">Ditandatangani secara elekronik oleh:</td>
								</tr>
								<tr nobr="true">
								<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
								</tr>
								<tr nobr="true">
								<td><br></td>
								</tr>
								<tr nobr="true">
								<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
								</td>
								</tr>
								<tr nobr="true">
								<td>' . $pangkat_ttd . '
								</td>
								</tr>
								</table>';


							$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
							$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
							$pdf->SetPrintHeader(false);
							$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
							$pdf->SetMargins(30, 30, 20);
							$pdf->SetAutoPageBreak(TRUE, 25);
							// 
							$pdf->AddPage('P', 'F4');

							if ($age == 'uk.1') {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 10, 160, 0, 'PNG', '', 'N');
								$pdf->Ln(10);
							} else {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
							}

							$pdf->Ln(5);
							$pdf->Cell(0, 10, 'SURAT KETERANGAN', 0, 0, 'C');
							$pdf->Ln(6);
							$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
							$pdf->Ln(15);
							$pdf->Cell(70, 5, 'Yang bertandatangan di bawah ini : ', 0, 0, 'L');
							$pdf->Ln(7);
							$pdf->Cell(45, 5, 'a. Nama', 0, 0, 'L');
							$pdf->Cell(5, 5, ':', 0, 0, 'L');
							$pdf->Cell(108, 5, $this->data['Nama_ttd_konsep'], 0, 0, 'L');
							$pdf->Ln(6);
							$pdf->Cell(45, 5, 'b. Jabatan', 0, 0, 'L');
							$pdf->Cell(5, 5, ':', 0, 0, 'L');
							$pdf->MultiCell(108, 5, ucwords(strtolower(get_data_people('RoleName', $this->data['Approve_People']))) . "\n", 0, 'J', 0, 2, '', '', true);
							$pdf->Ln(7);
							$pdf->Cell(67, 5, 'dengan ini menerangkan bahwa : ', 0, 0, 'L');
							$pdf->Ln(7);
							$i = 0;
							foreach ($data_ket as $row) {
								$i++;
								$pdf->SetX(30);
								$pdf->Cell(45, 5, 'a. Nama/NIP', 0, 0, 'L');
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->MultiCell(108, 5, $row->PeopleName . ' / NIP.' . $row->NIP, 0, 'L');
								$pdf->Ln(2);
								$pdf->SetX(30);
								$pdf->Cell(45, 5, 'b. Pangkat/Golongan', 0, 0, 'L');
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->Cell(108, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
								$pdf->Ln(2);
								$pdf->SetX(30);
								$pdf->Cell(45, 5, 'c. Jabatan', 0, 0, 'L');
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$uc  = ucwords(strtolower($row->PeoplePosition));
								$str = str_replace('Dan', 'dan', $uc);
								$str = str_replace('Uptd', 'UPTD', $str);
								// $pdf->MultiCell(85,5,$str,0,'J');
								$pdf->MultiCell(108, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
								$pdf->Ln(3);
								$pdf->SetX(30);
								$pdf->Cell(45, 5, 'd. Maksud', 0, 0, 'L');
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->writeHTMLCell(108, 0, '', '', $this->data['Hal'] . "\n", 0, 1, 0, false, 'J', false);
							}
							$pdf->Ln(7);
							$pdf->Cell(30, 5, 'Demikian Surat Keterangan ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
							$pdf->Ln(15);
							$pdf->SetX(100);
							$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
							// $pdf->Output('surat_keterangan_view.pdf','I');

						} else if ($naskah->Ket == 'outboxsuratizin') {

							// $this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);


							$this->load->library('Pdf');

							$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

							$this->db->select("*");
							$this->db->where_in('PeopleId', $this->data['RoleId_To']);
							$this->db->order_by('GroupId', 'ASC');
							$this->db->order_by('Golongan', 'DESC');
							$this->db->order_by('Eselon ', 'ASC');
							$data_perintah = $this->db->get('v_login')->result();

							// var_dump($data_perintah);
							// die();

							$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
							$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;



							$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
							$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

							//tcpdf
							$age = $this->session->userdata('roleid') == 'uk.1';
							$xx = $this->session->userdata('roleid');

							if ($age == 'uk.1') {
								$logottd = '/uploads/garuda.png';
							} else {
								$logottd = '/uploads/logoanri.jpg';
							}
							if ($this->data['TtdText'] == 'PLT') {
								$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
							} elseif ($this->data['TtdText'] == 'PLH') {
								$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
							} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
								$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
							} else {
								$tte = get_data_people('RoleName', $this->data['Approve_People']);
							}


							$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
								<tr nobr="true">
								<td rowspan="7" width="60px" style="valign:bottom;">
								<br><br>
								<img src="' . base_url($logottd) . '" widht="60" height="65"></td>
								<td width="180px">Ditandatangani secara elekronik oleh:</td>
								</tr>
								<tr nobr="true">
								<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
								</tr>
								<tr nobr="true">
								<td><br></td>
								</tr>
								<tr nobr="true">
								<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
								</td>
								</tr>
								<tr nobr="true">
								<td>' . $tujuan . '
								</td>
								</tr>
								</table>';


							$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
							$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
							$pdf->SetPrintHeader(false);
							$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
							$pdf->SetMargins(30, 30, 30);
							$pdf->SetAutoPageBreak(TRUE, 30);

							$pdf->AddPage('P', 'F4');
							if ($this->session->userdata('roleid') == 'uk.1') {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 10, 160, 0, 'PNG', '', 'N');
								$pdf->Ln(10);
							} else {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
							}
							$pdf->Ln(3);
							$pdf->SetX(45);
							$pdf->Cell(0, 10, 'SURAT IZIN', 0, 0, 'C');
							$pdf->Ln(6);
							$pdf->SetX(45);
							$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
							$pdf->Ln(10);
							$pdf->SetX(45);
							$pdf->Cell(0, 10, 'TENTANG', 0, 0, 'C');
							$pdf->Ln(10);
							$pdf->SetX(48);
							$pdf->writeHTMLCell(130, 0, '', '', $this->data['Hal'] , 0, 1, 0, true, 'C', true);

							$pdf->Ln(5);
							$pdf->Cell(30, 9, 'Dasar', 0, 0, 'L');
							$pdf->Cell(5, 9, ':', 0, 0, 'L');

							if (!empty($this->data['Konten'])) {
								$Konten_decode = json_decode($this->data['Konten']);
								$i = a;
								foreach ($Konten_decode as $row) {
									$pdf->Ln(2);

									$pdf->SetX(65);
									$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');

									$pdf->SetX(70);
									$pdf->MultiCell(117, 5, $row . "\n", 0, 'J', 0, 2, '', '', true);
									$i++;
								}
							}

							$pdf->Ln(3);
							$pdf->SetX(45);
							$pdf->Cell(0, 10, 'MEMBERI IZIN :', 0, 0, 'C');
							$pdf->Ln(12);
							$pdf->Cell(30, 5, 'Kepada :', 0, 0, 'L');

							$pdf->Ln(3);
							$i = 0;
							foreach ($data_perintah as $row) {
								$i++;
								$pdf->Ln(6);
								$pdf->SetX(30);
								$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
								$pdf->SetX(60);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->MultiCell(110, 6, $row->PeopleName, 0, 'L');

								$pdf->Ln(2);
								$pdf->SetX(30);
								$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
								$pdf->SetX(60);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->Cell(85, 5, $row->NIP, 0, 1, 'L');

								$pdf->Ln(2);
								$pdf->SetX(30);
								$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
								$pdf->SetX(60);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$uc  = ucwords(strtolower($row->PeoplePosition));
								$str = str_replace('Dan', 'dan', $uc);
								$str = str_replace('Uptd', 'UPTD', $str);
								// $pdf->MultiCell(85,5,$str,0,'J');
								$pdf->MultiCell(110, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);


								$instansi = $this->db->query("SELECT GroleId FROM role WHERE RoleId = '" . $this->session->userdata('primaryroleid') . "'  ")->row()->GroleId;

								$instansi_nama = $this->db->query("SELECT GroleName FROM master_grole WHERE GroleId = '" . $instansi . "'  ")->row()->GroleName;

								$pdf->Ln(2);
								$pdf->SetX(30);
								$pdf->Cell(25, 5, 'Instansi', 0, 0, 'L');
								$pdf->SetX(60);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$uc  = ucwords(strtolower($instansi_nama));
								$str = str_replace('Dan', 'dan', $uc);
								$str = str_replace('Uptd', 'UPTD', $str);
								// $pdf->MultiCell(85,5,$str,0,'J');
								$pdf->MultiCell(110, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);

								$pdf->Ln(2);
								$pdf->SetX(30);
								$pdf->Cell(25, 5, 'Untuk', 0, 0, 'L');
								$pdf->SetX(60);
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								// $pdf->MultiCell(85,5,$str,0,'J');

								$pdf->writeHTMLCell(110, 5, '', '', $this->data['Hal_pengantar'] , 0, 1, 0, true, 'J', true);
							}
							$pdf->Ln(12);

							$pdf->SetX(125);
							// $pdf->SetFont('Arial','',10);
							$pdf->Cell(30, 5, 'Ditetapkan di ' . $this->data['lokasi'], 0, 1, 'L');
							$pdf->SetX(120);
							// $pdf->SetFont('Arial','',10);
							$pdf->Cell(30, 5, 'Pada tanggal ' . get_bulan($this->data['TglNaskah']), 0, 1, 'L');


							// $pdf->SetFont('Arial','',10);
							$pdf->Ln(4);
							if ($this->data['TtdText'] == 'PLT') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} elseif ($this->data['TtdText'] == 'PLH') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							} else {
								$pdf->SetX(100);
								$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
							}

							$pdf->Ln(3);

							$pdf->SetX(100);
							$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);




							// Akhir Fungsi Surat izin




						} else if ($naskah->Ket == 'outboxsprintgub') {
							$this->load->library('HtmlPdf');
							ob_start();
							$this->load->view('backend/standart/administrator/mail_tl/supergub_inisiatif_pdf', $this->data);
							$content = ob_get_contents();
							ob_end_clean();

							$config = array(
								'orientation' 	=> 'p',
								'format' 		=> 'F4',
								'marges' 		=> array(30, 30, 20, 10),
							);
							//(_,_,KANAN,_)//


							$this->pdf = new HtmlPdf($config);
							$this->pdf->initialize($config);
							$this->pdf->pdf->SetDisplayMode('fullpage');
							$this->pdf->writeHTML($content);
						} else if ($naskah->Ket == 'outboxundangan') {
							$this->load->library('HtmlPdf');
							ob_start();
							$this->load->view('backend/standart/administrator/mail_tl/undangan_inisiatif_pdf', $this->data);
							$content = ob_get_contents();
							ob_end_clean();

							$config = array(
								'orientation' 	=> 'p',
								'format' 		=> 'F4',
								'marges' 		=> array(30, 30, 20, 10),
							);
							//(_,_,KANAN,_)//


							$this->pdf = new HtmlPdf($config);
							$this->pdf->initialize($config);
							$this->pdf->pdf->SetDisplayMode('fullpage');
							$this->pdf->writeHTML($content);
						} else if ($naskah->Ket == 'outboxedaran') {
							$this->load->library('HtmlPdf');
							ob_start();
							$this->load->view('backend/standart/administrator/mail_tl/suratedaran_inisiatif_pdf', $this->data);
							$content = ob_get_contents();
							ob_end_clean();

							$config = array(
								'orientation' 	=> 'p',
								'format' 		=> 'F4',
								'marges' 		=> array(30, 30, 20, 10),
							);
							//(_,_,KANAN,_)//


							$this->pdf = new HtmlPdf($config);
							$this->pdf->initialize($config);
							$this->pdf->pdf->SetDisplayMode('fullpage');
							$this->pdf->writeHTML($content);

						} else if ($naskah->Ket == 'outboxnotadinas') {
							$this->load->library('HtmlPdf');
							ob_start();
							$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf', $this->data);
							$content = ob_get_contents();
							ob_end_clean();

							$config = array(
								'orientation' 	=> 'p',
								'format' 		=> 'F4',
								'marges' 		=> array(30, 30, 20, 10),
							);
							//(_,_,KANAN,_)//


							$this->pdf = new HtmlPdf($config);
							$this->pdf->initialize($config);
							$this->pdf->pdf->SetDisplayMode('fullpage');
							$this->pdf->writeHTML($content);	

						} else if ($naskah->Ket == 'outboxkeluar') {
							$this->load->library('HtmlPdf');
							ob_start();
							$this->load->view('backend/standart/administrator/mail_tl/suratdinas_inisiatif_pdf', $this->data);
							$content = ob_get_contents();
							ob_end_clean();

							$config = array(
								'orientation' 	=> 'p',
								'format' 		=> 'F4',
								'marges' 		=> array(30, 30, 20, 10),
							);
							//(_,_,KANAN,_)//


							$this->pdf = new HtmlPdf($config);
							$this->pdf->initialize($config);
							$this->pdf->pdf->SetDisplayMode('fullpage');
							$this->pdf->writeHTML($content);

							// 						$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;
							// 						$this->data['TglReg'] 				= $naskah->TglReg;
							// 						$this->data['Approve_People'] 		= $naskah->Approve_People;
							// 						$this->data['TtdText'] 				= $naskah->TtdText;
							// 						$this->data['Nama_ttd_konsep'] 		= $naskah->Nama_ttd_konsep;
							// 						$this->data['TtdText2'] 			= $naskah->TtdText2;
							// 						$this->data['Approve_People3'] 		= $naskah->Approve_People3;
							// 						$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
							// 						$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;		

							// 						$data_perintah = $this->db->get('v_login')->result();
							// // var_dump($data_perintah);
							// // die();
							// 						$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

							// //tcpdf
							// 						$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama. "'")->row()->Pangkat;
							// 						$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

							// 						$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
							// 						$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;
							// 						$atasanub = get_data_people('RoleAtasan', $Nama_ttd_atas_nama);

							// 						$age = $this->session->userdata('roleid') == 'uk.1';
							// 						$xx = $this->session->userdata('roleid');		
							// 						$r_atasan =  $this->session->userdata('roleatasan'); 
							// 						if ($age == 'uk.1') {
							// 							$logottd = '/uploads/garuda.png';
							// 						} else {
							// 							$logottd = '/uploads/logoanri.jpg';
							// 						}

							// 						if($this->data['TtdText'] == 'PLT') {
							// 							$tte = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
							// 						}elseif($this->data['TtdText'] == 'PLH') {
							// 							$tte = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);
							// 						}elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 							$tte =get_data_people('RoleName', $this->data['Approve_People']);
							// 	// .'<br>'.get_data_people('RoleName', $this->data['Approve_People']);
							// 						}elseif($this->data['TtdText2'] == 'untuk_beliau') {
							// 							$tte = 'a.n'.'. '.get_data_people('RoleName', $this->data['Approve_People3']).'<br>'.get_data_people('RoleName', $atasanub).','.'<br>'.'u.b.<br>'.get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',';
							// 						}else{

							// 							$tte =get_data_people('RoleName', $this->data['Approve_People']);
							// 						}

							// 						$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
							// 						<tr nobr="true">
							// 						<td rowspan="7" width="60px" style="valign:bottom;">
							// 						<br><br>
							// 						<img src="'.base_url($logottd).'" widht="55" height="60"></td>
							// 						<td width="180px">Ditandatangani secara elekronik oleh:</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td width="180px" style="text-align: left; font-size:9;">'.$tte.',</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td><br></td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
							// 						</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td>'.$pangkat_ttd. '
							// 						</td>
							// 						</tr>
							// 						</table>';

							// 						if ($age == 'uk.1') {
							// 							$logottd1 = '/uploads/garuda_1.png';
							// 						} else {
							// 							$logottd1 = '/uploads/logoanri_1.jpg';
							// 						}


							// 						$table1 = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
							// 						<tr nobr="true">
							// 						<td rowspan="7" width="60px" style="valign:bottom;">
							// 						<br><br>
							// 						<img src="'.base_url($logottd1).'" widht="55" height="60"></td>
							// 						<td width="180px">Ditandatangani secara elekronik oleh:</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td width="180px" style="text-align: left; font-size:9;">'.$tte.',</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td><br></td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
							// 						</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td>'.$pangkat_ttd. '
							// 						</td>
							// 						</tr>
							// 						</table>';

							// 						if ($age == 'uk.1') {
							// 							$logottd2 = '/uploads/garuda_2.png';
							// 						} else {
							// 							$logottd2 = '/uploads/logoanri_2.jpg';
							// 						}


							// 						$table2 = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
							// 						<tr nobr="true">
							// 						<td rowspan="7" width="60px" style="valign:bottom;">
							// 						<br><br>
							// 						<img src="'.base_url($logottd2).'" widht="55" height="60"></td>
							// 						<td width="180px">Ditandatangani secara elekronik oleh:</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td width="180px" style="text-align: left; font-size:9;">'.$tte.',</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td><br></td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
							// 						</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td>'.$pangkat_ttd. '
							// 						</td>
							// 						</tr>
							// 						</table>';

							// 						if ($age == 'uk.1') {
							// 							$logottd3 = '/uploads/garuda_3.png';
							// 						} else {
							// 							$logottd3 = '/uploads/logoanri_3.jpg';
							// 						}


							// 						$table3 = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
							// 						<tr nobr="true">
							// 						<td rowspan="7" width="60px" style="valign:bottom;">
							// 						<br><br>
							// 						<img src="'.base_url($logottd3).'" widht="55" height="60"></td>
							// 						<td width="180px">Ditandatangani secara elekronik oleh:</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td width="180px" style="text-align: left; font-size:9;">'.$tte.',</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td><br></td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
							// 						</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td>'.$pangkat_ttd. '
							// 						</td>
							// 						</tr>
							// 						</table>';

							// 						if ($age == 'uk.1') {
							// 							$logottd4 = '/uploads/garuda_4.png';
							// 						} else {
							// 							$logottd4 = '/uploads/logoanri_4.jpg';
							// 						}


							// 						$table4 = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
							// 						<tr nobr="true">
							// 						<td rowspan="7" width="60px" style="valign:bottom;">
							// 						<br><br>
							// 						<img src="'.base_url($logottd4).'" widht="55" height="60"></td>
							// 						<td width="180px">Ditandatangani secara elekronik oleh:</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td width="180px" style="text-align: left; font-size:9;">'.$tte.',</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td><br></td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
							// 						</td>
							// 						</tr>
							// 						<tr nobr="true">
							// 						<td>'.$pangkat_ttd. '
							// 						</td>
							// 						</tr>
							// 						</table>';


							// 						$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
							// 						$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
							// 						$pdf->SetPrintHeader(false);
							// 						$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
							// 						$pdf->SetMargins(30, 30, 30);
							// 						$pdf->SetAutoPageBreak(TRUE, 25);
							// // 
							// 						$pdf->AddPage('P', 'F4');

							// 						$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG');
							// 						$pdf->Ln(10);
							// 						$pdf->SetX(120);
							// 						$pdf->SetFont('Arial','',11);

							// 						$pdf->Cell(30,5,$this->data['lokasi'].', '.get_bulan($this->data['TglNaskah']),0,1,'L');
							// 						$pdf->Ln(2);
							// 						$pdf->SetX(120);
							// // $pdf->SetFont('Arial','',10);
							// 						$pdf->Cell(30,5,'Kepada',0,1,'L');


							// 						$pdf->Cell(20,5,'Nomor',0,0,'L');
							// 						$pdf->Cell(5,5,':',0,0,'L');
							// 						$pdf->Cell(5,5,$this->data['nosurat'],0,0,'L');
							// 						$pdf->SetX(120);
							// 						$pdf->Cell(10,5,'Yth. ',0,0,'L');
							// 						$pdf->SetX(130);
							// 						$pdf->writeHTMLCell(60, 5, '', '', $this->data['RoleId_To'], '', 1, 0, true, 'L', true);
							// 						$pdf->SetX(126);
							// 						$pdf->Cell(10,5,'di ',0,0,'L');
							// // $pdf->SetX(130); ini dimatikan
							// 						$pdf->Ln(5);
							// 						$pdf->SetX(132);
							// 						$pdf->MultiCell(56,5,$this->data['Alamat'],0,'L');
							// 						$y_kanan = $pdf->GetY(); // ini tambahan
							// 						$pdf->Ln(5);
							// 						$pdf->SetY(58);
							// 						$pdf->Cell(20,5,'Sifat',0,0,'L');
							// 						$pdf->Cell(5,5,':',0,0,'L');
							// 						$pdf->Cell(60,5,$this->data['SifatId'],0,0,'L');
							// 						$pdf->SetX(130);
							// 						// $pdf->Cell(10,5,'di',0,0,'L');
							// 						$pdf->Ln(5);
							// 						$pdf->Cell(20,5,'Lampiran',0,0,'L');
							// 						$pdf->Cell(5,5,':',0,0,'L');
							// 						$pdf->Cell(60,5,$this->data['Jumlah'].' '.$this->data['MeasureUnitId'],0,0,'L');
							// 						$pdf->SetX(133);
							// 						$pdf->MultiCell(56,5,'',0,'L');
							// 						$pdf->Cell(20,5,'Hal',0,0,'L');
							// 						$pdf->Cell(5,5,':',0,0,'L');
							// 						$pdf->writeHTMLCell(60, 5, '', '', $this->data['Hal'], 0, 1, 0, true, 'L', true); // ini 0 jadi 1
							// 						$y_kiri = $pdf->GetY(); // ini tambahan
							// 						$pdf->Ln(25);	
							// 						$pdf->SetX(50);
							// 						// ini tambahan
							// 						if ($y_kiri > $y_kanan) {
							// 							$pdf->SetY($y_kiri + 10);
							// 						}else if ($y_kiri < $y_kanan){
							// 							$pdf->SetY($y_kanan + 10);
							// 						}
							// // $pdf->setData(($src), substr($kode_verifikasi, 0, 10));
							// // $pdf->SetPrintHeader(false);
							// // $pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
							// // $pdf->SetMargins(30, 30, 30);
							// // $pdf->SetAutoPageBreak(TRUE, 30);
							// // // 
							// // $pdf->AddPage('P', 'F4');
							// // $pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
							// // $pdf->Ln(10);
							// // $pdf->SetX(120);
							// // $pdf->SetFont('Arial','',11);
							// // // $pdf->SetFont('Arial','',10);
							// // $pdf->Cell(30,5,$this->data['lokasi'].', '.get_bulan($this->data['TglNaskah']),0,1,'L');
							// // $pdf->Ln(2);
							// // $pdf->SetX(120);
							// // // $pdf->SetFont('Arial','',10);
							// // $pdf->Cell(30,5,'Kepada',0,1,'L');	
							// // $pdf->Cell(20,5,'Nomor',0,0,'L');
							// // $pdf->Cell(5,5,':',0,0,'L');
							// // $pdf->Cell(5,5,$this->data['nosurat'],0,0,'L');
							// // $pdf->SetX(120);
							// // $pdf->Cell(10,5,'Yth. ',0,0,'L');
							// // $pdf->SetX(130);	
							// // $pdf->writeHTMLCell(60, 5, '', '', $this->data['RoleId_To'], '', 1, 0, true, 'L', true);
							// // $pdf->SetX(126);
							// // $pdf->Cell(10,5,'di. ',0,0,'L');
							// // // $pdf->SetX(130);
							// // $pdf->Ln(5);
							// // $pdf->SetX(132);
							// // $pdf->MultiCell(56,5,$this->data['Alamat'],0,'L');
							// // $y_kanan = $pdf->GetY(); // ini tambahan
							// // $pdf->Ln(5);
							// // $pdf->SetY(58);
							// // $pdf->Cell(20,5,'Sifat',0,0,'L');
							// // $pdf->Cell(5,5,':',0,0,'L');
							// // $pdf->Cell(60,5,$this->data['SifatId'],0,0,'L');
							// // $pdf->SetX(130);
							// // // $pdf->Cell(10,5,'di',0,0,'L');
							// // $pdf->Ln(5);
							// // $pdf->Cell(20,5,'Lampiran',0,0,'L');
							// // $pdf->Cell(5,5,':',0,0,'L');
							// // $pdf->Cell(60,5,$this->data['Jumlah'].' '.$this->data['MeasureUnitId'],0,0,'L');
							// // $pdf->SetX(133);
							// // $pdf->MultiCell(56,5,'',0,'L');
							// // $pdf->Cell(20,5,'Hal',0,0,'L');
							// // $pdf->Cell(5,5,':',0,0,'L');
							// // $pdf->writeHTMLCell(60, 5, '', '', $this->data['Hal'], 0, 0, 0, true, 'L', true);
							// // $y_kiri = $pdf->GetY(); // ini tambahan
							// // $pdf->Ln(25);	
							// // $pdf->SetX(50);
							// // // ini tambahan
							// // if ($y_kiri > $y_kanan) {
							// // 	$pdf->SetY($y_kiri + 10);
							// // }else if ($y_kiri < $y_kanan){
							// // 	$pdf->SetY($y_kanan + 10);
							// // }
							// $pdf->SetX(50);	
							// $pdf->writeHTMLCell(140, 0, '', '', $this->data['Konten'], 0, 1, 0, true, 'J', true);	
							// // $pdf->SetFont('Arial','',10);
							// $r_atasan =  $this->session->userdata('roleatasan'); 
							// $r_biro =  $this->session->userdata('groleid');
							// $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
							// $atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

							// $pdf->Ln(6);
							// if($this->data['TtdText'] == 'PLT') {
							// 	$pdf->SetX(100);
							// 	$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// }elseif($this->data['TtdText'] == 'PLH') {
							// 	$pdf->SetX(100);
							// 	$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// }elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 	$pdf->SetX(100);
							// 	$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 	// $pdf->SetX(100);
							// 	// $pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

							// }elseif($this->data['TtdText2'] == 'untuk_beliau') {
							// 	$pdf->SetX(100);
							// 	$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 	$pdf->SetX(100);
							// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
							// 	$pdf->SetX(100);
							// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');		
							// }else{
							// 	$pdf->SetX(100);
							// 	$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
							// }

							// $pdf->Ln(5);		
							// $pdf->SetX(100);
							// $pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
							// if (!empty($this->data['RoleId_Cc'] )) {
							// 	$exp = explode(',', $this->data['RoleId_Cc']);
							// 	$z = count($exp);

							// 	$pdf->Ln(12);
							// 	$pdf->Cell(30,5,'Tembusan',0,0,'L');
							// 	$pdf->Cell(5,5,':',0,0,'L');
							// 	$pdf->Ln(5);
							// 	$pdf->Cell(30,5,'',0,0,'L');
							// 	$pdf->Cell(5,5,'',0,0,'L');

							// 	$exp = explode(',', $this->data['RoleId_Cc']);
							// 	$hitung = count($exp);
							// 	$i = 0;

							// 	foreach( $exp as $v){
							// 		$y = 'Yth';
							// 		$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;			
							// 		$uc  = ucwords(strtolower($xx1));
							// 		$str = str_replace('Dan', 'dan', $uc);
							// 		$str = str_replace('Uptd', 'UPTD', $str);
							// 		$str = str_replace('Dprd', 'DPRD', $str);
							// 		$i++;


							// 		if (!empty($hitung)) {

							// 			if ($hitung > 1) {
							// 				# code...
							// 				if ($i == $hitung - 1) {
							// 					# code...
							// 					$pdf->Ln(1);
							// 					$pdf->SetX(30);
							// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
							// 					$pdf->SetX(34);
							// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
							// 					$pdf->SetX(42);
							// 					$pdf->MultiCell(150, 5, $str."; dan\n", 0, 'J', 0, 2, '' ,'', true);
							// 				} elseif($i == $hitung){
							// 					$pdf->Ln(1);
							// 					$pdf->SetX(30);
							// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
							// 					$pdf->SetX(34);
							// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
							// 					$pdf->SetX(42);
							// 					$pdf->MultiCell(150, 5, $str.".\n", 0, 'J', 0, 2, '' ,'', true);
							// 				}else{
							// 					$pdf->Ln(1);
							// 					$pdf->SetX(30);
							// 					$pdf->Cell(30,5,$i.'. ',0,0,'L');
							// 					$pdf->SetX(34);
							// 					$pdf->Cell(30,5,$y.'. ',0,0,'L');
							// 					$pdf->SetX(42);
							// 					$pdf->MultiCell(150, 5, $str.";\n", 0, 'J', 0, 2, '' ,'', true);
							// 				}

							// 			}else{
							// 				$$pdf->SetX(34);
							// 				$pdf->Cell(30,5,$y.'. ',0,0,'L');
							// 				$pdf->SetX(42);
							// 				$pdf->MultiCell(150, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
							// 			}


							// 		}	

							// 	}			
							// }

							// if (!empty($this->data['lampiran2'])) {
							// 	$lamp_romawi1= "I";
							// }

							// else{
							// 	$lamp_romawi1= "";
							// }

							// if  ($this->data['type_naskah'] == 'XxJyPn38Yh.2') {
							// 	$type_surat ="SURAT UNDANGAN ";          
							// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
							// 	$type_surat ="SURAT PANGGILAN ";  
							// }else { 
							// 	$type_surat ="SURAT ";
							// }
							// if($this->data['TtdText'] == 'PLT') {
							// 	$tte_lamp = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
							// }elseif($this->data['TtdText'] == 'PLH') {
							// 	$tte_lamp = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);
							// }elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 	$tte_lamp =get_data_people('RoleName', $this->data['Approve_People3']);
							// 	// .'<br>'.get_data_people('RoleName', $this->data['Approve_People']);

							// }else{

							// 	$tte_lamp =get_data_people('RoleName', $this->data['Approve_People']);
							// }

							// $header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
							// <tr>
							// <td width="65px">Lampiran '.$lamp_romawi1.' : </td>
							// <td colspan="2" width="200px">'. $type_surat.''.$tte_lamp.'</td>
							// </tr>
							// <tr>
							// <td>&nbsp;</td>
							// <td width="50px">NOMOR</td>
							// <td width="10px">:</td>
							// <td width="140px">'.$this->data['nosurat'].'</td>
							// </tr>

							// <tr>

							// <td>&nbsp;</td>
							// <td>TANGGAL</td>
							// <td>:</td>
							// <td>'.get_bulan($this->data['TglNaskah']).'</td>
							// </tr>
							// <tr>
							// <td>&nbsp;</td>
							// <td>PERIHAL</td>
							// <td>:</td>
							// <td>'. $this->data['Hal'].'</td>
							// </tr>
							// </table>';

							// $jum_lamp2 = $this->data['lampiran2'];
							// $no_lamp2 = 1;
							// if (!empty($this->data['lampiran2'])) {
							// 	switch ($no_lamp2){
							// 		case 1: 
							// 		$lamp_romawi2 = "II";

							// 		break;
							// 	}                                                         
							// }

							// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
							// 	$type_surat ="SURAT UNDANGAN ";          
							// }elseif ($this->data['type_naskah']  == 'XxJyPn38Yh.3') {
							// 	$type_surat ="SURAT PANGGILAN ";  
							// }else { 
							// 	$type_surat ="SURAT ";
							// }
							// if($this->data['TtdText'] == 'PLT') {
							// 	$tte_lamp = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
							// }elseif($this->data['TtdText'] == 'PLH') {
							// 	$tte_lamp = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);
							// }elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 	$tte_lamp =get_data_people('RoleName', $this->data['Approve_People3']);
							// 	// .'<br>'.get_data_people('RoleName', $this->data['Approve_People']);

							// }else{

							// 	$tte_lamp =get_data_people('RoleName', $this->data['Approve_People']);
							// }
							// $header_lampiran2 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
							// <tr>
							// <td width="65px">Lampiran '.$lamp_romawi2.' : </td>
							// <td colspan="2" width="200px">'. $type_surat.''.$tte_lamp.'</td>
							// </tr>
							// <tr>
							// <td>&nbsp;</td>
							// <td width="50px">NOMOR</td>
							// <td width="10px">:</td>
							// <td width="140px">'.$this->data['nosurat'].'</td>
							// </tr>   
							// <tr>

							// <td>&nbsp;</td>
							// <td>TANGGAL</td>
							// <td>:</td>
							// <td>'.get_bulan($this->data['TglNaskah']).'</td>
							// </tr>
							// <tr>
							// <td>&nbsp;</td>
							// <td>PERIHAL</td>
							// <td>:</td>
							// <td>'. $this->data['Hal'].'</td>
							// </tr>
							// </table>';

							// $jum_lamp3 = $this->data['lampiran3'];
							// $no_lamp3 = 1;
							// if (!empty($this->data['lampiran3'])) {
							// 	switch ($no_lamp3){
							// 		case 1: 
							// 		$lamp_romawi3 = "III";

							// 		break;
							// 	}                         

							// }

							// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
							// 	$type_surat ="SURAT UNDANGAN ";          
							// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
							// 	$type_surat ="SURAT PANGGILAN ";  
							// }else { 
							// 	$type_surat ="SURAT ";
							// }
							// if($this->data['TtdText'] == 'PLT') {
							// 	$tte_lamp = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
							// }elseif($this->data['TtdText'] == 'PLH') {
							// 	$tte_lamp = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);
							// }elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 	$tte_lamp =get_data_people('RoleName', $this->data['Approve_People3']);
							// 	// .'<br>'.get_data_people('RoleName', $this->data['Approve_People']);

							// }else{

							// 	$tte_lamp =get_data_people('RoleName', $this->data['Approve_People']);
							// }
							// $header_lampiran3 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
							// <tr>
							// <td width="65px">Lampiran '.$lamp_romawi3.' : </td>
							// <td colspan="2" width="200px">'. $type_surat.''.$tte_lamp.'</td>
							// </tr>
							// <tr>
							// <td>&nbsp;</td>
							// <td width="50px">NOMOR</td>
							// <td width="10px">:</td>
							// <td width="140px">'.$this->data['nosurat'].'</td>
							// </tr>

							// <tr>

							// <td>&nbsp;</td>
							// <td>TANGGAL</td>
							// <td>:</td>
							// <td>'.get_bulan($this->data['TglNaskah']).'</td>
							// </tr>
							// <tr>
							// <td>&nbsp;</td>
							// <td>PERIHAL</td>
							// <td>:</td>
							// <td>'. $this->data['Hal'].'</td>
							// </tr>
							// </table>';


							// $jum_lamp4 =$this->data['lampiran4'];
							// $no_lamp4 = 1;

							// if (!empty($this->data['lampiran4'])) {
							// 	switch ($no_lamp4){
							// 		case 1: 
							// 		$lamp_romawi4 ="IV";

							// 		break;
							// 	}                            

							// }

							// if  ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
							// 	$type_surat ="SURAT UNDANGAN ";          
							// }elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
							// 	$type_surat ="SURAT PANGGILAN ";  
							// }else { 
							// 	$type_surat ="SURAT ";
							// }
							// if($this->data['TtdText'] == 'PLT') {
							// 	$tte_lamp = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
							// }elseif($this->data['TtdText'] == 'PLH') {
							// 	$tte_lamp = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);
							// }elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 	$tte_lamp =get_data_people('RoleName', $this->data['Approve_People3']);
							// 	// .'<br>'.get_data_people('RoleName', $this->data['Approve_People']);

							// }else{

							// 	$tte_lamp =get_data_people('RoleName', $this->data['Approve_People']);
							// }
							// $header_lampiran4 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
							// <tr>
							// <td width="65px">Lampiran '.$lamp_romawi4.' : </td>
							// <td colspan="2" width="200px">'. $type_surat.''.$tte_lamp.'</td>
							// </tr>
							// <tr>
							// <td>&nbsp;</td>
							// <td width="50px">NOMOR</td>
							// <td width="10px">:</td>
							// <td width="140px">'.$this->data['nosurat'].'</td>
							// </tr>

							// <tr>

							// <td>&nbsp;</td>
							// <td>TANGGAL</td>
							// <td>:</td>
							// <td>'.get_bulan($this->data['TglNaskah']).'</td>
							// </tr>
							// <tr>
							// <td>&nbsp;</td>
							// <td>PERIHAL</td>
							// <td>:</td>
							// <td>'. $this->data['Hal'].'</td>
							// </tr>
							// </table>';


							// if (!empty($this->data['lampiran'] )) {
							// 	$pdf->AddPage('P', 'F4');
							// 	$pdf->SetX(95);
							// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);

							// 	$pdf->Ln(10);
							// 	$pdf->SetX(30);
							// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran'] , 0, 1, 0, true, 'J', true);
							// 	$r_atasan =  $this->session->userdata('roleatasan'); 
							// 	$r_biro =  $this->session->userdata('groleid');
							// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
							// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );


							// 	$pdf->Ln(6);
							// 	if($this->data['TtdText'] == 'PLT') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// 	}elseif($this->data['TtdText'] == 'PLH') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 	// $pdf->SetX(100);
							// 	// $pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

							// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');		
							// 	}else{
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
							// 	}

							// 	$pdf->Ln(5);

							// 	$pdf->SetX(100);
							// 	$pdf->writeHTMLCell(90, 0, '', '', $table1, '', 1, 0, false, 'L', false);
							// }

							// if (!empty($this->data['lampiran2'] )) {
							// 	$pdf->AddPage('P', 'F4');
							// 	$pdf->SetX(95);
							// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran2, '', 1, 0, false, 'J', false);
							// 	$pdf->Ln(10);
							// 	$pdf->SetX(30);
							// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran2'] , 0, 1, 0, true, 'J', true);
							// 	$r_atasan =  $this->session->userdata('roleatasan'); 
							// 	$r_biro =  $this->session->userdata('groleid');
							// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
							// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

							// 	$pdf->Ln(6);
							// 	if($this->data['TtdText'] == 'PLT') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// 	}elseif($this->data['TtdText'] == 'PLH') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 	// $pdf->SetX(100);
							// 	// $pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

							// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');				
							// 	}else{
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
							// 	}



							// 	$pdf->Ln(5);

							// 	$pdf->SetX(100);
							// 	$pdf->writeHTMLCell(90, 0, '', '', $table2, '', 1, 0, false, 'L', false);

							// }

							// if (!empty($this->data['lampiran3'])) {

							// 	$pdf->AddPage('P', 'F4');
							// 	$pdf->SetX(95);
							// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran3, '', 1, 0, false, 'J', false);
							// 	$pdf->Ln(10);
							// 	$pdf->SetX(30);
							// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran3'] , 0, 1, 0, true, 'J', true);

							// 	$r_atasan =  $this->session->userdata('roleatasan'); 
							// 	$r_biro =  $this->session->userdata('groleid');
							// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
							// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

							// 	$pdf->Ln(6);
							// 	if($this->data['TtdText'] == 'PLT') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// 	}elseif($this->data['TtdText'] == 'PLH') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 	// $pdf->SetX(100);
							// 	// $pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

							// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');				
							// 	}else{
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
							// 	}


							// 	$pdf->Ln(5);

							// 	$pdf->SetX(100);
							// 	$pdf->writeHTMLCell(90, 0, '', '', $table3, '', 1, 0, false, 'L', false);

							// }

							// if (!empty($this->data['lampiran4'])) {

							// 	$pdf->AddPage('P', 'F4');
							// 	$pdf->SetX(95);
							// 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran4, '', 1, 0, false, 'J', false);
							// 	$pdf->Ln(10);
							// 	$pdf->SetX(30);
							// 	$pdf->writeHTMLCell(160, 0, '', '',$this->data['lampiran4'] , 0, 1, 0, true, 'J', true);


							// 	$r_atasan =  $this->session->userdata('roleatasan'); 
							// 	$r_biro =  $this->session->userdata('groleid');
							// 	$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
							// 	$atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );

							// 	$pdf->Ln(6);
							// 	if($this->data['TtdText'] == 'PLT') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// 	}elseif($this->data['TtdText'] == 'PLH') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
							// 	}elseif($this->data['TtdText2'] == 'Atas_Nama') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 	// $pdf->SetX(100);
							// 	// $pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');

							// 	}elseif($this->data['TtdText2'] == 'untuk_beliau') {
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,'u.b. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Nama_ttd_atas_nama']).',',0,'C');				
							// 	}else{
							// 		$pdf->SetX(100);
							// 		$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C'); 
							// 	}


							// 	$pdf->Ln(5);

							// 	$pdf->SetX(100);
							// 	$pdf->writeHTMLCell(90, 0, '', '', $table4, '', 1, 0, false, 'L', false);



							// }	
							// } else if ($naskah->Ket == 'outboxkeluar') {
							// 	$this->load->library('HtmlPdf');
							// 	ob_start();
							// 	$this->load->view('backend/standart/administrator/mail_tl/suratdinas_inisiatif_pdf', $this->data);
							// 	$content = ob_get_contents();
							// 	ob_end_clean();

							// 	$config = array(
							// 		'orientation' 	=> 'p',
							// 		'format' 		=> 'F4',
							// 		'marges' 		=> array(30, 30, 20, 15),
							// 	);
							// 	//(_,_,KANAN,_)//


							// 	$this->pdf = new HtmlPdf($config);
							// 	$this->pdf->initialize($config);
							// 	$this->pdf->pdf->SetDisplayMode('fullpage');
							// 	$this->pdf->writeHTML($content);
						} else if ($naskah->Ket == 'outboxpengumuman') {
							$this->load->library('Pdf');
							$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

							$this->db->select("*");
							$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
							$this->db->order_by('GroupId', 'ASC');
							$this->db->order_by('Golongan', 'DESC');
							$this->db->order_by('Eselon ', 'ASC');
							$data_ket = $this->db->get('v_login')->result();

							$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
							$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
							//tcpdf
							$age = $this->session->userdata('roleid');
							if ($age == 'uk.1') {
								$logottd = '/uploads/garuda.png';
							} else {
								$logottd = '/uploads/logoanri.jpg';
							}

							if ($this->data['TtdText'] == 'PLT') {
								$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
							} elseif ($this->data['TtdText'] == 'PLH') {
								$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
							} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
								$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
							} else {
								$tte = get_data_people('RoleName', $this->data['Approve_People']);
							}

							$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td width="240px" style="text-align:center;">' . $this->data['lokasi'] . ', ' . get_bulan($this->data['TglNaskah']) . '</td>
		</tr>
		<tr nobr="true">
		<td width="240px" style="text-align:center;">' . $tte . ',</td>
		</tr>
		</table>
		<br>
		<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td rowspan="7" width="60px" style="valign:bottom;">
		<br><br>
		<img src="' . base_url($logottd) . '" widht="55" height="60"></td>
		<td width="180px">Ditandatangani secara elekronik oleh:</td>
		</tr>
		<tr nobr="true">
		<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
		</tr>
		<tr nobr="true">
		<td><br></td>
		</tr>
		<tr nobr="true">
		<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
		</td>
		</tr>
		<tr nobr="true">
		<td>' . $pangkat_ttd . '
		</td>
		</tr>
		</table>';


							$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
							$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
							$pdf->SetPrintHeader(false);
							$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
							$pdf->SetMargins(30, 30, 20);
							$pdf->SetAutoPageBreak(TRUE, 30);
							// 
							$pdf->AddPage('P', 'F4');

							if ($age == 'uk.1') {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 160, 0, 'PNG', '', 'N');
							} else {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
							}
							$pdf->Ln(10);
							$pdf->Cell(0, 10, 'PENGUMUMAN', 0, 0, 'C');
							$pdf->Ln(6);
							$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
							$pdf->Ln(10);
							$pdf->Cell(0, 10, 'TENTANG', 0, 0, 'C');
							$pdf->Ln(6);
							$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal'] . "\n", 0, 1, false, true, 'C', false);
							$pdf->Ln(10);
							$pdf->writeHTMLCell(160, 0, '', '', $this->data['Konten'] . "\n", 0, 1, false, true, 'J', false);
							$pdf->Ln(13);
							$pdf->SetX(100);
							$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
							// $pdf->Output('surat_keterangan_view.pdf','I');
						} else if ($naskah->Ket == 'outboxrekomendasi') {

							$this->load->library('Pdf');
							$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

							$this->db->select("*");
							$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
							$this->db->order_by('GroupId', 'ASC');
							$this->db->order_by('Golongan', 'DESC');
							$this->db->order_by('Eselon ', 'ASC');
							$data_ket = $this->db->get('v_login')->result();
							$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
							//tcpdf
							$age = $this->session->userdata('roleid');
							if ($age == 'uk.1') {
								$logottd = '/uploads/garuda.png';
							} else {
								$logottd = '/uploads/logoanri.jpg';
							}


							//tcpdf
							$konten = json_decode($this->data['Konten']);

							if ($this->data['TtdText'] == 'PLT') {
								$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
							} elseif ($this->data['TtdText'] == 'PLH') {
								$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
							} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
								$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
							} else {
								$tte = get_data_people('RoleName', $this->data['Approve_People']);
							}
							$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td width="240px" style="text-align:center;">' . $this->data['lokasi'] . ', ' . get_bulan($this->data['TglNaskah']) . '</td>
		</tr>
		<tr nobr="true">
		<td width="240px" style="text-align:center;">' . $tte . ',</td>
		</tr>
		</table>
		<br>
		<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		<tr nobr="true">
		<td rowspan="7" width="60px" style="valign:bottom;">
		<br><br>
		<img src="' . base_url($logottd) . '" widht="55" height="60"></td>
		<td width="180px">Ditandatangani secara elekronik oleh:</td>
		</tr>
		<tr nobr="true">
		<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
		</tr>
		<tr nobr="true">
		<td><br></td>
		</tr>
		<tr nobr="true">
		<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
		</td>
		</tr>
		<tr nobr="true">
		<td>' . $pangkat_ttd . '
		</td>
		</tr>
		</table>';


							$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
							$pdf->setData(base_url('FilesUploaded/qrcode/' . $image_name), substr($kode_verifikasi, 0, 10));
							$pdf->SetPrintHeader(false);
							$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
							$pdf->SetMargins(30, 30, 20);
							$pdf->SetAutoPageBreak(TRUE, 30);
							// 
							$pdf->AddPage('P', 'F4');
							if ($this->session->userdata('roleid') == 'uk.1') {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 160, 0, 'PNG', '', 'N');
							} else {
								$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
							}
							$pdf->Ln(10);
							$pdf->Cell(0, 10, 'REKOMENDASI', 0, 0, 'C');
							$pdf->Ln(6);
							$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
							$pdf->Ln(15);
							$pdf->Cell(30, 0, 'Dasar', 0, 0, 'L');
							$pdf->Cell(5, 0, ':', 0, 0, 'L');
							$pdf->writeHTMLCell(125, 0, '', '',  $this->data['Hal'] . "\n", 0, 1, false, true, 'J', false);
							$pdf->Ln(5);
							$pdf->Cell(30, 0, 'Menimbang', 0, 0, 'L');
							$pdf->Cell(5, 0, ':', 0, 0, 'L');
							$pdf->writeHTMLCell(125, 0, '', '', $konten->menimbang . "\n", 0, 1, false, true, 'J', false);
							$pdf->Ln(5);
							$pdf->MultiCell(160, 5, $tte . ', memberikan rekomendasi kepada :', 0, 'L');
							$pdf->Ln(0);
							$i = 0;
							foreach ($data_ket as $row) {
								$i++;
								$pdf->SetX(30);
								$pdf->Cell(55, 5, 'a. Nama/obyek', 0, 0, 'L');
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$pdf->MultiCell(95, 5, $row->PeopleName, 0, 'L');
								$pdf->Ln(5);
								$pdf->SetX(30);
								$pdf->Cell(55, 5, 'b. Jabatan/Tempat/Identitas', 0, 0, 'L');
								$pdf->Cell(5, 5, ':', 0, 0, 'L');
								$uc  = ucwords(strtolower($row->PeoplePosition));
								$str = str_replace('Dan', 'dan', $uc);
								$str = str_replace('Uptd', 'UPTD', $str);
								// $pdf->MultiCell(85,5,$str,0,'J');
								$pdf->MultiCell(95, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
							}
							$pdf->Ln(5);
							$pdf->Cell(30, 5, 'Untuk', 0, 0, 'L');
							$pdf->Cell(5, 5, ':', 0, 0, 'L');
							$pdf->writeHTMLCell(125, 0, '', '', $konten->untuk . "\n", 0, 1, false, true, 'J', false);
							$pdf->Ln(0);
							$pdf->Cell(30, 5, 'Demikian rekomendasi ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
							$pdf->Ln(10);
							$pdf->SetX(100);
							$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
						} else {
							$this->load->library('HtmlPdf');
							ob_start();
							$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf', $this->data);
							$content = ob_get_contents();
							ob_end_clean();
							$config = array(
								'orientation' 	=> 'p',
								'format' 		=> 'F4',
								'marges' 		=> array(30, 30, 20, 10),
							);

							$this->pdf = new HtmlPdf($config);
							$this->pdf->initialize($config);
							$this->pdf->pdf->SetDisplayMode('fullpage');
							$this->pdf->writeHTML($content);
						}


						// if ($naskah->Ket == 'outboxnotadinas') {
						// 	$a = 'nota_dinas-' . $NId . '.pdf';
						// 	// $this->pdf->Output('FilesUploaded/naskah/sprint-' . $NId . '.pdf', 'F');
						// 	$pdf->Output(FCPATH . 'FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf', 'F');
						// 	$a = 'nota_dinas-' . $NId . '.pdf';
						// 	// $this->pdf->Output('FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf', 'F');
						if ($naskah->Ket == 'outboxsprint') {
							$a = 'sprint-' . $NId . '.pdf';
							// $this->pdf->Output('FilesUploaded/naskah/sprint-' . $NId . '.pdf', 'F');
							$pdf->Output(FCPATH . 'FilesUploaded/naskah/sprint-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxsprintgub') {
							$a = 'sprint-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/sprintgub-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxundangan') {
							$a = 'undangan-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/undangan-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxedaran') {
							$a = 'surat_edaran-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/surat_edaran-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxinstruksigub') {
							$a = 'surat_instruksi-' . $NId . '.pdf';
							//$this->pdf->Output('FilesUploaded/naskah/surat_instruksi-' . $NId . '.pdf', 'F');
							$pdf->Output(FCPATH . 'FilesUploaded/naskah/surat_instruksi-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxsupertugas') {
							$a = 'surat_supertugas-' . $NId . '.pdf';
							//$this->pdf->Output('FilesUploaded/naskah/surat_instruksi-' . $NId . '.pdf', 'F');
							$pdf->Output(FCPATH . 'FilesUploaded/naskah/surat_supertugas-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxkeluar') {
							$a = 'surat_dinas-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/surat_dinas-' . $NId . '.pdf', 'F');

						} else if ($naskah->Ket == 'outboxnotadinas') {
							$a = 'nota_dinas-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf', 'F');

						} else if ($naskah->Ket == 'outboxsket') {
							$a = 'surat_keterangan-' . $NId . '.pdf';
							$pdf->Output(FCPATH . 'FilesUploaded/naskah/surat_keterangan-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxpengumuman') {
							$a = 'pengumuman-' . $NId . '.pdf';
							$pdf->Output(FCPATH . 'FilesUploaded/naskah/pengumuman-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxsuratizin') {
							$a = 'surat_izin-' . $NId . '.pdf';
							//$this->pdf->Output('FilesUploaded/naskah/surat_instruksi-' . $NId . '.pdf', 'F');
							$pdf->Output(FCPATH . 'FilesUploaded/naskah/surat_izin-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxrekomendasi') {
							$a = 'rekomendasi-' . $NId . '.pdf';
							$pdf->Output(FCPATH . 'FilesUploaded/naskah/rekomendasi-' . $NId . '.pdf', 'F');
						} else {
							$a = 'nadin_lain-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/nadin_lain-' . $NId . '.pdf', 'F');
						}

						//Simpan Inbox File
						$save_files = [
							'FileKey' 			=> tb_key(),
							'GIR_Id' 			=> $GIR_Id,
							'NId' 				=> $NId,
							'PeopleID' 			=> $this->session->userdata('peopleid'),
							'PeopleRoleID' 		=> $this->session->userdata('roleid'),
							'FileName_real' 	=> $a,
							'FileName_fake' 	=> $a,
							'FileStatus' 		=> 'available',
							'EditedDate'        => $tanggal,
							'Keterangan' 		=> 'outbox',
							'Id_dokumen' 		=> $kode_verifikasi,
						];

						$save_file = $this->model_inboxfile->store($save_files);

						//Update Status
						$this->db->where('NId', $NId);
						$this->db->where('RoleId_To', $this->session->userdata('roleid'));
						$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);

						//============= Sertifikat Elektronik Menggunakan Esign-Client-Gateway PHP
						//url api sertifikat elektronik
						$url_signed = 'http://103.122.5.59/api/sign/pdf';

						//loads a PDF file 
						// if ($naskah->Ket == 'outboxnotadinas') {
						// 	$asal = FCPATH . 'FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf';
					 	if ($naskah->Ket == 'outboxsprint') {
							$asal = FCPATH . 'FilesUploaded/naskah/sprint-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsprintgub') {
							$asal = FCPATH . 'FilesUploaded/naskah/sprintgub-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxundangan') {
							$asal = FCPATH . 'FilesUploaded/naskah/undangan-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxedaran') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_edaran-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxkeluar') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_dinas-' . $NId . '.pdf';


						} else if ($naskah->Ket == 'outboxnotadinas') {
							$asal = FCPATH . 'FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf';


						} else if ($naskah->Ket == 'outboxinstruksigub') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_instruksi_gub-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsupertugas') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_pernyataan_tugas-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsket') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_keterangan-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxpengumuman') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_pengumuman-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsuratizin') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_izin-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxrekomendasi') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_rekomendasi-' . $NId . '.pdf';
						} else {
							$asal = FCPATH . 'FilesUploaded/naskah/nadin_lain-' . $NId . '.pdf';
						}

						//passphrase for the certificate
						$passphrase = $this->input->post('password');

						//save pdf
						// if ($naskah->Ket == 'outboxnotadinas') {
						// 	$tujuan = FCPATH . 'FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf';
						if ($naskah->Ket == 'outboxsprint') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/sprint-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsprintgub') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/sprintgub-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxundangan') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/undangan-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxedaran') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_edaran-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxkeluar') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_dinas-' . $NId . '.pdf';

						} else if ($naskah->Ket == 'outboxnotadinas') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf';

						} else if ($naskah->Ket == 'outboxinstruksigub') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_instruksi_gub-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsupertugas') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_pernyataan_tugas-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsket') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_keterangan-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxpengumuman') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_pengumuman-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsuratizin') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_izin-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxrekomendasi') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_rekomendasi-' . $NId . '.pdf';
						} else {
							$tujuan = FCPATH . 'FilesUploaded/naskah/nadin_lain-' . $NId . '.pdf';
						}

						//curl initialization
						$curl = curl_init();

						curl_setopt_array($curl, array(
							CURLOPT_URL => $url_signed,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 0,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "POST",
							CURLOPT_POSTFIELDS => array('file' => new CURLFILE($asal, 'application/pdf'), 'nik' => $nik, 'passphrase' => $passphrase, 'tampilan' => 'invisible', 'page' => '1', 'image' => 'false', 'linkQR' => 'https://google.com', 'xAxis' => '0', 'yAxis' => '0', 'width' => '200', 'height' => '100'),
							CURLOPT_HTTPHEADER => array(
								"Authorization: Basic U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=",
								"Cookie: JSESSIONID="
							),
						));

						$response = curl_exec($curl);
						file_put_contents($tujuan, $response);
						curl_close($curl);
						//============= Akhir Sertifikat Elektronik Menggunakan Esign-Client-Gateway PHP

						//penambahan log dispu
						$this->load->model('model_log');
						$this->model_log->simpan_user();
						$data['notifikasi'] = 'Data berhasil disimpan';
						$data['judul'] = 'Insert Data Berhasil';
						$this->load->view('notifikasi', $data);
						//akhir penambahan dispu

						//PENAMBAHAN JEJAK APPROVE KE INBOX RECEIVER KOREKSI

						$nid = $NId;
						$girid = $GIR_Id;

						$gir = $this->session->userdata('peopleid') . date('dmyhis');
						$tanggal = date('Y-m-d H:i:s');

						if ($this->input->post('TtdText') == 'none') {
							$z = $this->session->userdata('approvelname');
						} elseif ($this->input->post('TtdText') == 'AL') {
							$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
						} else {
							$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
						}

						if ($this->input->post('TtdText') == 'none') {
							$x =  $this->session->userdata('peopleid');
						} elseif ($this->input->post('TtdText') == 'AL') {
							$x = $this->input->post('tmpid_atasan');
						} else {
							$x = $this->input->post('Approve_People');
						}
						$save_recievers_approve = [
							'NId' 			=> $nid,
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $gir,
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $x,
							'RoleId_To' 	=> get_data_people('RoleId', $x),
							'ReceiverAs' 	=> 'approvenaskah',
							'Msg' 			=> $this->input->post('pesan'),
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
						];

						$save_reciever_approve = $this->model_inboxreciever_koreksi->store($save_recievers_approve);

						//akhir PENAMBAHAN JEJAK APPROVE KE INBOX RECEIVER KOREKSI

						set_message('Data Berhasil Disimpan', 'success');
						$this->load->library('user_agent');
						redirect($this->agent->referrer());
					} else {

						$this->load->model('model_log');
						$this->model_log->gagal_user();
						$data['notifikasi'] = 'Data berhasil disimpan';
						$data['judul'] = 'Insert Data Berhasil';
						$this->load->view('notifikasi', $data);
						//akhir penambahan dispu


						//Jika Passphrase Salah
						set_message('Passphrase Anda Salah, Silahkan Coba Lagi', 'error');
						redirect($this->agent->referrer());
					}
				} else {

					//Jika NIK Tidak Terdaftar
					set_message('NIK Anda Belum Terdaftar', 'error');
					redirect($this->agent->referrer());
				}
			}
		}
	}

	//Fungsi Teruskan Draft Naskah Dinas
	public function k_teruskan()
	{

		$nid = $this->input->post('NId_Temp2');
		$girid = $this->input->post('GIR_Id2');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_sekdis') . "' AND GroupId= '4'")->row()->ApprovelName;
		} elseif ($this->input->post('TtdText') == 'UK') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_lainya') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$y = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$y = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_sekdis') . "'")->row()->ApprovelName;
		} else {
			$y = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$v =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$v = $this->input->post('tmpid_sekdis');
		} else {
			$v = $this->input->post('Approve_People');
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		// if ($naskah->Ket == 'outboxnotadinas') {
		// 	$xxz = 'to_draft_notadinas';
		if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_keluar';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxinstruksigub') {
			$xxz = 'to_draft_instruksi_gub';
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$xxz = 'to_draft_super_tugas';
		} else if ($naskah->Ket == 'outboxsket') {
			$xxz = 'to_draft_sket';
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$xxz = 'to_draft_pengumuman';
		} else if ($naskah->Ket == 'outboxsuratizin') {
			$xxz = 'to_draft_surat_izin';
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$xxz = 'to_draft_rekomendasi';
		} else if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';

		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			$save_recievers_jejak = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'meneruskan',
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];


			$save_reciever_jejak = $this->model_inboxreciever_koreksi->store($save_recievers_jejak);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x, 'TtdText' => $this->input->post('TtdText'), 'Nama_ttd_konsep' => $z]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Fungsi Teruskan Draft Naskah Dinas

	// Beri Nomor
	public function nomor_dong()
	{

		$NId = $this->input->post('NId_Temp3');
		$GIR_Id = $this->input->post('GIR_Id3');

		$nomor = $this->input->post('nonaskah');

		$tahun 	= date('Y');
		$bulan = date('m');

		$cari = $this->db->query("SELECT ClId FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->ClId;

		$klas = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $cari . "'")->row()->ClCode;

		$unit = $this->db->query("SELECT RoleCode FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->RoleCode;

		$nosurat = $nomor . '/' . $klas . '/' . $unit;

		$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id', $GIR_Id)->update('konsep_naskah', ['Number' => $nomor, 'nosurat' => $nosurat]);

		set_message('Nomor Berhasil Disimpan', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//Tutup Beri Nomor	


public function k_teruskan_surat_dinas()
	{
		$nid = $this->input->post('NId_Temp6');
		$girid = $this->input->post('GIR_Id6');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		} else if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_keluar';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		} else if ($naskah->Ket == 'outboxinstruksigub') {
			$xxz = 'to_draft_instruksi_gub';
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$xxz = 'to_draft_super_tugas';
		} else if ($naskah->Ket == 'outboxsket') {
			$xxz = 'to_draft_sket';
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$xxz = 'to_draft_pengumuman';
		} else if ($naskah->Ket == 'outboxsuratizin') {
			$xxz = 'to_draft_surat_izin';
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$xxz = 'to_draft_rekomendasi';
		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			$save_recievers_jejak = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'Meminta Nomber Surat',
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever_jejak = $this->model_inboxreciever_koreksi->store($save_recievers_jejak);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}

public function k_teruskan_uk()
	{
		$nid = $this->input->post('NId_Temp8');
		$girid = $this->input->post('GIR_Id8');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		} else if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_keluar';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		} else if ($naskah->Ket == 'outboxinstruksigub') {
			$xxz = 'to_draft_instruksi_gub';
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$xxz = 'to_draft_super_tugas';
		} else if ($naskah->Ket == 'outboxsket') {
			$xxz = 'to_draft_sket';
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$xxz = 'to_draft_pengumuman';
		} else if ($naskah->Ket == 'outboxsuratizin') {
			$xxz = 'to_draft_surat_izin';
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$xxz = 'to_draft_rekomendasi';
		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			$save_recievers_jejak = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'Meminta Nomber Surat',
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever_jejak = $this->model_inboxreciever_koreksi->store($save_recievers_jejak);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}

	//kirim ke tu
	public function k_teruskan_tu()
	{
		$nid = $this->input->post('NId_Temp5');
		$girid = $this->input->post('GIR_Id5');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		// if ($naskah->Ket == 'outboxnotadinas') {
		// 	$xxz = 'to_draft_notadinas';
		if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_keluar';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxinstruksigub') {
			$xxz = 'to_draft_instruksi_gub';
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$xxz = 'to_draft_super_tugas';
		} else if ($naskah->Ket == 'outboxsket') {
			$xxz = 'to_draft_sket';
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$xxz = 'to_draft_pengumuman';
		} else if ($naskah->Ket == 'outboxsuratizin') {
			$xxz = 'to_draft_surat_izin';
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$xxz = 'to_draft_rekomendasi';

		} else if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';

		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			$save_recievers_nomor = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'Meminta Nomber Surat',
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever_nomor = $this->model_inboxreciever_koreksi->store($save_recievers_nomor);
			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}

//kirim ke tu
	public function k_tu_ttdnotapimpinan()
	{
		$nid = $this->input->post('NId_Temp9');
		$girid = $this->input->post('GIR_Id9');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		// if ($naskah->Ket == 'outboxnotadinas') {
		// 	$xxz = 'to_draft_notadinas';
		if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_keluar';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		} else if ($naskah->Ket == 'outboxinstruksigub') {
			$xxz = 'to_draft_instruksi_gub';
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$xxz = 'to_draft_super_tugas';
		} else if ($naskah->Ket == 'outboxsket') {
			$xxz = 'to_draft_sket';
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$xxz = 'to_draft_pengumuman';
		} else if ($naskah->Ket == 'outboxsuratizin') {
			$xxz = 'to_draft_surat_izin';
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$xxz = 'to_draft_rekomendasi';
		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			$save_recievers_jejak = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'Meminta Nomber Surat',
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever_jejak = $this->model_inboxreciever_koreksi->store($save_recievers_jejak);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}


	public function filter_uk_tu () {

			$nid = $this->input->post('nid');
			$gird = $this->input->post('gird');

			$cari_asal_nota = $this->db->query("SELECT RoleId_From FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' AND GIR_Id = '" . $gird . "'")->row()->RoleId_From;

			$sekdispusipda = $this->session->userdata('roleid');
			$xx = $this->session->userdata('roleid');

			$konsep123 = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND GroupId = 8 AND PrimaryRoleId ='".$cari_asal_nota."' ORDER BY PrimaryRoleId ASC")->result();

			// if ($this->session->userdata('roleid') == 'uk.1'){

			// foreach ($konsep123  as $atasan) {

			// 		$isiuk = '<option value="'.$atasan->PeopleId.'">'.$atasan->PeoplePosition.'</option>';
			// }

			// }else if ($sekdispusipda == 'uk.1.19.1.1'){
			// 	foreach ($konsepsekdis  as $atasan) {
			// 			$isiuk +='<option value="'.$atasan->PeopleId.'">'.$atasan->PeoplePosition.'</option>';
			// 		}
			// }else {

			// 	foreach ($konsep123  as $atasan) {
			// 			$isiuk +='<option value="'.$atasan->PeopleId.'">'.$atasan->PeoplePosition.'</option>';
			// 			}
			// }
					

			echo json_encode($konsep123);

		}

}
