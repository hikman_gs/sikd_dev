<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_reg_video extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
	}
	

	// List pengguna
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		//model_custome di running di autoload -> application/config/autoload.php ->autoload model
		$this->data['people'] = $this->model_custome->get_people();
		$this->tempanri('backend/standart/administrator/reg_video/video1', $this->data);
	}
	

}