<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_temp_doc extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
	}

	//Daftar Template Dokumen
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_doc_template/list_temp_doc', $this->data);
	}
	//Tutup Daftar Template Dokumen			

}