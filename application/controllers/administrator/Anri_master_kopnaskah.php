<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_kopnaskah extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_kopnaskah');
	}

	// List kop naskah
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_kopnaskah/master_kopnaskah_list', $this->data);
	}
	// Tutup list kop naskah

	// Tambah data kop naskah
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_kopnaskah/master_kopnaskah_add', $this->data);
	}
	// Tutup tambah data kop naskah

	// Proses simpan data kop naskah
	public function add_save()
	{

		$this->form_validation->set_rules('GRoleId', 'GRoleId', 'trim|required|max_length[14]');
		$this->form_validation->set_rules('Lokasi', 'Lokasi', 'trim|max_length[20]');
		$config['upload_path'] = './FilesUploaded/kop/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['max_size'] = '15000';
		$config['overwrite'] = TRUE;
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);
		
		$this->upload->do_upload('image_header');
		$file1 = $this->upload->data();
	    $file_name1 = $file1['file_name'];
	    
	    if ($this->form_validation->run()) {
			$table = 'master_kopnaskah';

			$query = $this->db->query("SELECT (max(convert(substr(KopId, 12), UNSIGNED)) + 1) as id FROM master_kopnaskah")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

			if (!empty($file_name1)) {
				$save_data = [
					'KopId' => tb_key().'.'.$id,
					'GRoleId' => $this->input->post('GRoleId'),
					'Header' => $file_name1,
					'Lokasi' => $this->input->post('Lokasi'),
				];
			} else {
				$save_data = [
					'KopId' => tb_key().'.'.$id,
					'GRoleId' => $this->input->post('GRoleId'),
					'Lokasi' => $this->input->post('Lokasi'),
				];
			}

			
			$save_master_kopnaskah = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_kopnaskah'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_kopnaskah'));
		}

	}
	// Tutup proses simpan data kop naskah

	// Edit data kop naskah
	public function update($id)
	{
		$this->data['master_kopnaskah'] = $this->model_master_kopnaskah->find($id);
		$this->tempanri('backend/standart/administrator/master_kopnaskah/master_kopnaskah_update', $this->data);
	}
	// Tutup edit data kop naskah

	// Proses update data kop naskah
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('GRoleId', 'GRoleId', 'trim|required|max_length[14]');
		$this->form_validation->set_rules('Lokasi', 'Lokasi', 'trim|max_length[20]');
		
		$config['upload_path'] = './FilesUploaded/kop/';
		$config['allowed_types'] = 'jpg|jpeg|png';
		$config['max_size'] = '15000';
		$config['overwrite'] = TRUE;
		$config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);
		
		$this->upload->do_upload('image_header');
		$file1 = $this->upload->data();
	    $file_name1 = $file1['file_name'];
	    
	    if ($this->form_validation->run()) {
			$table = 'master_kopnaskah';
			if (!empty($file_name1)) {
				$save_data = [
					'GRoleId' => $this->input->post('GRoleId'),
					'Header' => $file_name1,
					'Lokasi' => $this->input->post('Lokasi'),
				];
			} else {
				$save_data = [
					'GRoleId' => $this->input->post('GRoleId'),
					'Lokasi' => $this->input->post('Lokasi'),
				];
			}

			
			$save_master_kopnaskah = $this->model_master_kopnaskah->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_kopnaskah'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_kopnaskah'));
		}

	}
	// Tutup proses update data kop naskah

	// Hapus data kop naskah
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$lokasi = FCPATH . 'FilesUploaded/kop/';
		    $kop = $this->model_master_kopnaskah->find($id);
			$files = glob($lokasi.$kop->Header);
			foreach($files as $file){
				if(is_file($file))
					unlink($file);
			}
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}


		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_kopnaskah'));						
	}
	// Tutup hapus data kop naskah

	// Proses hapus data kop naskah
	private function remove($id)
	{
		$master_kopnaskah = $this->db->where('KopId',$id)->delete('master_kopnaskah');
		return $master_kopnaskah;
	}
	// Tutup proses hapus data kop naskah

	// Buka Hapus file kopnaskah
	public function hapus_file3($NId,$name_file)
	{

		$this->load->helper('file');

		if (!empty($NId)) {
			$lokasi = FCPATH . 'FilesUploaded/kop/';
		    $kop = $this->model_master_kopnaskah->find($NId);
			$files = glob($lokasi.$kop->Header);
			foreach($files as $file){
				if(is_file($file))
					unlink($file);
			}
		}
		
		$this->db->where('KopId',$NId);
		$this->db->update('master_kopnaskah',['Header' => '']);


		set_message('Data Digital Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	// Tutup Hapus file kopnaskah	
}