<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Anri_regis_suratdinastcpdf extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_konsepnaskah_koreksi');
	}

	//Registrasi Surat Tugas
	public function index()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		$this->data1['people'] = $this->db->query("SELECT * FROM people WHERE PeopleId != 'root' ")->result();
		$this->data['title'] = 'Pencatatan Surat Tugas';
		$this->tempanri('backend/standart/administrator/surat_dinas_tcpdf/regis', $this->data);
	}
	// Tutup Registrasi Surat Tugas

	//Fungsi Lihat Surat Tugas
	public function lihat_naskah_dinas2()
	{

		$tanggal = date('Y-m-d H:i:s');
		// $this->load->library('HtmlPdf');
		$this->load->library('Pdf');
		// ob_start();

		$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');

		$this->data['Hal'] 			= $this->input->post('Hal');
		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['Alamat'] 		= $this->input->post('alamat');
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['RoleCode'] 	= $this->input->post('rolecode');
		$this->data['SifatId'] 		= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$this->input->post('SifatId')."'")->row()->SifatName;

		$this->data['type_naskah'] 		= $this->db->query("SELECT type_name FROM master_type_surat WHERE type_naskah = '".$this->input->post('type_naskah')."'")->row()->Type_Name;
		$this->data['Jumlah'] 		= $this->input->post('Jumlah');
		$this->data['MeasureUnitId'] = $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$this->input->post('MeasureUnitId')."'")->row()->MeasureUnitName;			

		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['lampiran'] 	= $this->input->post('lampiran');

		$this->data['lampiran2'] 	= $this->input->post('lampiran2');
		$this->data['lampiran3'] 	= $this->input->post('lampiran3');
		$this->data['lampiran4'] 	= $this->input->post('lampiran4');

		if ($this->input->post('TtdText') == 'none') {
			$this->data['Approve_People']	= $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$this->data['Approve_People'] 	= $this->input->post('tmpid_atasan');
		} else {
			$this->data['Approve_People'] 	= $this->input->post('Approve_People');
		}
		//penambahan atas nama

		if($this->input->post('TtdText2') == 'AL') {
			$this->data['Approve_People3'] 	= $this->input->post('tmpid_atas_nama');	
		} else {
			$this->data['Approve_People3'] 	= $this->input->post('Approve_People3');
		}

		//
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');
		//atas nama
		$this->data['TtdText2']     = $this->input->post('TtdText2');


		if ($this->input->post('TtdText') == 'none') {
			$this->data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;		
		$data_perintah = $this->db->get('v_login')->result();

		// var_dump($data_perintah);
		// die();

		$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4','7') ")->row()->Pangkat; 

		//tcpdf
		$r_atasan 	=  $this->session->userdata('roleatasan'); 
        $r_biro 	=  $this->session->userdata('groleid');
        $atasanub 	= get_data_people('RoleAtasan', $this->data['Approve_People']);
		$age 		= $this->session->userdata('roleid') == 'uk.1';
		$xx 		= $this->session->userdata('roleid');
		
		
		if($this->data['TtdText'] == 'PLT') {
			$tte = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
		}elseif($this->data['TtdText'] == 'PLH') {
			$tte = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);

		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$tte =get_data_people('RoleName', $this->data['Approve_People3']);
			// .'<br>'.get_data_people('RoleName', $this->data['Approve_People']);
			
		}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			$tte = 'a.n'.'. '.get_data_people('RoleName', $this->data['Approve_People3']).'<br>'.get_data_people('RoleName', $atasanub).','.'<br>'.'u.b.<br>'.get_data_people('RoleName',$this->data['Approve_People']).',';
		}else{

			$tte =get_data_people('RoleName', $this->data['Approve_People']);
		}

		
		$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td rowspan="7" width="60px" style="valign:bottom;">
				<br><br>
				<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
				<td width="180px" style="text-align: left; font-size:9;">'.$tte.'</td>
			</tr>
			<tr nobr="true">
				<td><br></td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
				</td>
			</tr>
			<tr nobr="true">
				<td>'.$tujuan. '
				</td>
			</tr>
		</table>';

		$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
		$pdf->SetMargins(30, 30, 30);
		$pdf->SetAutoPageBreak(TRUE, 30);
		// 
		$pdf->AddPage('P', 'F4');

		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
		$pdf->Ln(10);
		$pdf->SetX(120);
		$pdf->SetFont('Arial','',11);
	
		$pdf->Cell(30,5,'Tempat / Tanggal / Bulan / Tahun ',0,1,'L');
		$pdf->Ln(2);
		$pdf->SetX(120);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Kepada',0,1,'L');
			
		
		$pdf->Cell(20,5,'Nomor',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->Cell(5,5,'.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'],0,0,'L');
		$pdf->SetX(120);
		$pdf->Cell(10,5,'Yth. ',0,0,'L');
		$pdf->SetX(130);
		$pdf->writeHTMLCell(60, 5, '', '', $this->data['RoleId_To'], '', 1, 0, true, 'L', true);
		$pdf->SetX(126);
		$pdf->Cell(10,5,'di ',0,0,'L');
		// $pdf->SetX(130); ini dimatikan
		$pdf->Ln(5);
		$pdf->SetX(132);
		$pdf->MultiCell(56,5,$this->data['Alamat'],0,'L');
		$y_kanan = $pdf->GetY(); // ini tambahan
		$pdf->Ln(5);
		$pdf->SetY(58);
		$pdf->Cell(20,5,'Sifat',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->Cell(60,5,$this->data['SifatId'],0,0,'L');
		$pdf->SetX(130);
		// $pdf->Cell(10,5,'di',0,0,'L');
		$pdf->Ln(5);
		$pdf->Cell(20,5,'Lampiran',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->Cell(60,5,$this->data['Jumlah'].' '.$this->data['MeasureUnitId'],0,0,'L');
		$pdf->SetX(133);
		$pdf->MultiCell(56,5,'',0,'L');
		$pdf->Cell(20,5,'Hal',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->writeHTMLCell(60, 5, '', '', $this->data['Hal'], 0, 1, 0, true, 'L', true); // ini 0 jadi 1
		$y_kiri = $pdf->GetY(); // ini tambahan
		$pdf->Ln(25);	
		$pdf->SetX(50);
		// ini tambahan
		if ($y_kiri > $y_kanan) {
			$pdf->SetY($y_kiri + 10);
		}else if ($y_kiri < $y_kanan){
			$pdf->SetY($y_kanan + 10);
		}
	 	$pdf->SetX(50);
		$pdf->writeHTMLCell(140, 0, '', '', $this->data['Konten'], 0, 1, 0, true, 'J', true);

		
		// $pdf->SetFont('Arial','',10);
		$r_atasan =  $this->session->userdata('roleatasan'); 
        $r_biro =  $this->session->userdata('groleid');
        $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
        $atasanub = get_data_people('RoleAtasan', $this->data['Approve_People']);
		$pdf->Ln(10);
		if($this->data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'u.b. ',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}
		$pdf->Ln(2);
			$pdf->SetX(132);
		
		$pdf->Cell(30,5,'PEMERIKSA',0,1,'L');
		$pdf->Ln(5);		
		$pdf->SetX(100);

		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
		
		if (!empty($this->data['RoleId_Cc'] )) {

			$pdf->Ln(12);
			$pdf->Cell(30,5,'Tembusan',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->Ln(5);
			$pdf->Cell(30,5,'',0,0,'L');
			$pdf->Cell(5,5,'',0,0,'L');
			$filter_RoleId_To=array_filter($this->data['RoleId_Cc']);
			$hitung = count($filter_RoleId_To);

			$i = 0;
			foreach( $filter_RoleId_To as $v){

			$y = 'Yth';
			$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;			
			$uc  = ucwords(strtolower($xx1));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			$str = str_replace('Dprd', 'DPRD', $str);
			$i++;
			
				if (!empty($hitung)) {

					if ($hitung > 1) {
						# code...
						if ($i == $hitung - 1) {
							# code...
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30,5,$i.'. ',0,0,'L');
							$pdf->SetX(34);
							$pdf->Cell(30,5,$y.'. ',0,0,'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str."; dan\n", 0, 'J', 0, 2, '' ,'', true);
						} elseif($i == $hitung){
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30,5,$i.'. ',0,0,'L');
							$pdf->SetX(34);
							$pdf->Cell(30,5,$y.'. ',0,0,'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str.".\n", 0, 'J', 0, 2, '' ,'', true);
						}else{
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30,5,$i.'. ',0,0,'L');
							$pdf->SetX(34);
							$pdf->Cell(30,5,$y.'. ',0,0,'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str.";\n", 0, 'J', 0, 2, '' ,'', true);
						}
						
					}else{
						$$pdf->SetX(34);
						$pdf->Cell(30,5,$y.'. ',0,0,'L');
						$pdf->SetX(42);
						$pdf->MultiCell(150, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
					}
					

				}	
					
			}			
		}

	  		$jum_lamp1 =$this->input->post('lampiran') ;
        	$no_lamp1 = 1;
       		if (!empty($this->input->post('lampiran'))) {
       			switch ($no_lamp1){
       				case 1: 
       				$lamp_romawi1 = "I";

       				break;
       			}
       		}

    if  ($this->input->post('type_naskah') == 'XxJyPn38Yh.2') {
                                $type_surat ="SURAT UNDANGAN ";          
                            }elseif ($this->input->post('type_naskah') == 'XxJyPn38Yh.3') {
                            	$type_surat ="SURAT PANGGILAN ";  
                            }else { 
                            	$type_surat ="SURAT ";
                            }

		if($this->data['TtdText'] == 'PLT') {
			$tte = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']).',';
		}elseif($this->data['TtdText'] == 'PLH') {
			$tte = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']).',';
		
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$tte =get_data_people('RoleName', $this->data['Approve_People']);

		}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			$tte =get_data_people('RoleName', $this->data['Approve_People']);
		}else{

			$tte =get_data_people('RoleName', $this->data['Approve_People']).',';
		}




		$header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
	<tr>
        <td width="65px">Lampiran '.$lamp_romawi1.' : </td>
        <td colspan="2" width="200px">'. $type_surat.''.$tte.'</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
         <td width="50px">NOMOR</td>
        <td width="10px">:</td>
        <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
    </tr>
   
    <tr>
       
        <td>&nbsp;</td>
        <td>TANGGAL</td>
        <td>:</td>
        <td>Tanggal/Bulan/Tahun</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>PERIHAL</td>
        <td>:</td>
        <td>'. $this->data['Hal'].'</td>
    </tr>
</table>';


	$jum_lamp2 = $this->input->post('lampiran2');
        $no_lamp2 = 1;
         if (!empty($this->input->post('lampiran2'))) {
                                    switch ($no_lamp2){
                                    case 1: 
                                        $lamp_romawi2 = "II";
                                       
                                        break;
                                    }
                            
                                   
                                }

  if  ($this->input->post('type_naskah') == 'XxJyPn38Yh.2') {
                                $type_surat ="SURAT UNDANGAN ";          
                            }elseif ($this->input->post('type_naskah') == 'XxJyPn38Yh.3') {
                                 $type_surat ="SURAT PANGGILAN ";  
                            }else { 
                                $type_surat ="SURAT ";
                            }

		$header_lampiran2 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
	<tr>
        <td width="65px">Lampiran '.$lamp_romawi2.' : </td>
        <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName', $this->data['Approve_People']).'</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
         <td width="50px">NOMOR</td>
        <td width="10px">:</td>
        <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
    </tr>
   
    <tr>
       
        <td>&nbsp;</td>
        <td>TANGGAL</td>
        <td>:</td>
        <td>Tanggal/Bulan/Tahun</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>PERIHAL</td>
        <td>:</td>
        <td>'. $this->data['Hal'].'</td>
    </tr>
</table>';

		$jum_lamp3 = $this->input->post('lampiran3');
        $no_lamp3 = 1;
         if (!empty($this->input->post('lampiran3'))) {
                                    switch ($no_lamp3){
                                    case 1: 
                                        $lamp_romawi3 = "III";
                                       
                                        break;
                                    }                                                             
                                }

if  ($this->input->post('type_naskah') == 'XxJyPn38Yh.2') {
                                $type_surat ="SURAT UNDANGAN ";          
                            }elseif ($this->input->post('type_naskah') == 'XxJyPn38Yh.3') {
                                 $type_surat ="SURAT PANGGILAN ";  
                            }else { 
                                $type_surat ="SURAT ";
                            }

		$header_lampiran3 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
	<tr>
        <td width="65px">Lampiran '.$lamp_romawi3.' : </td>
        <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName', $this->data['Approve_People']).'</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
         <td width="50px">NOMOR</td>
        <td width="10px">:</td>
        <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
    </tr>
   
    <tr>
       
        <td>&nbsp;</td>
        <td>TANGGAL</td>
        <td>:</td>
        <td>Tanggal/Bulan/Tahun</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>PERIHAL</td>
        <td>:</td>
        <td>'. $this->data['Hal'].'</td>
    </tr>
</table>';


$jum_lamp4 = $this->input->post('lampiran4');
        $no_lamp4 = 1;
         if (!empty($this->input->post('lampiran4'))) {
                                    switch ($no_lamp4){
                                    case 1: 
                                        $lamp_romawi4 ="IV";
                                       
                                        break;
                                    }
                            
                                   
                                }

if  ($this->input->post('type_naskah') == 'XxJyPn38Yh.2') {
                                $type_surat ="SURAT UNDANGAN ";          
                            }elseif ($this->input->post('type_naskah') == 'XxJyPn38Yh.3') {
                                 $type_surat ="SURAT PANGGILAN ";  
                            }else { 
                                $type_surat ="SURAT ";
                            }

		$header_lampiran4 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
	<tr>
        <td width="65px">Lampiran '.$lamp_romawi4.' : </td>
        <td colspan="2" width="200px">'. $type_surat.''.get_data_people('RoleName', $this->data['Approve_People']).'</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
         <td width="50px">NOMOR</td>
        <td width="10px">:</td>
        <td width="140px">.../'.$this->data['ClCode'].'/'.$this->data['RoleCode'].'</td>
    </tr>
   
    <tr>
       
        <td>&nbsp;</td>
        <td>TANGGAL</td>
        <td>:</td>
        <td>Tanggal/Bulan/Tahun</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>PERIHAL</td>
        <td>:</td>
        <td>'. $this->data['Hal'].'</td>
    </tr>
</table>';

  		if (!empty($this->input->post('lampiran'))) {
		 $pdf->AddPage('P', 'F4');
		 $pdf->SetX(95);
		 $pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);

		 $pdf->Ln(10);
		 $pdf->SetX(30);
	     $pdf->writeHTMLCell(161, 0, '', '',$this->data['lampiran'] , 0, 1, 0, true, 'J', true);
      

		$r_atasan =  $this->session->userdata('roleatasan'); 
        $r_biro =  $this->session->userdata('groleid');
        $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
        $atasanub = get_data_people('RoleAtasan', $this->data['Approve_People']);
		$pdf->Ln(10);
		if($this->data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'u.b. ',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}
		$pdf->Ln(2);
			$pdf->SetX(132);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'PEMERIKSA',0,1,'L');
		$pdf->Ln(5);		
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
  }
       	if (!empty($this->input->post('lampiran2') )) {
			$pdf->AddPage('P', 'F4');
			$pdf->SetX(95);
		 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran2, '', 1, 0, false, 'J', false);
		 $pdf->Ln(10);
		 $pdf->SetX(30);
	     $pdf->writeHTMLCell(161, 0, '', '',$this->data['lampiran2'] , 0, 1, 0, true, 'J', true);
      

      $r_atasan =  $this->session->userdata('roleatasan'); 
        $r_biro =  $this->session->userdata('groleid');
        $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
        $atasanub = get_data_people('RoleAtasan', $this->data['Approve_People']);
		$pdf->Ln(10);
		if($this->data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'u.b. ',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}
		$pdf->Ln(2);
			$pdf->SetX(132);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'PEMERIKSA',0,1,'L');
		$pdf->Ln(5);		
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
  }
         if (!empty($this->input->post('lampiran3') )) {
         	
			$pdf->AddPage('P', 'F4');
			$pdf->SetX(95);
		 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran3, '', 1, 0, false, 'J', false);
		$pdf->Ln(10);
		 $pdf->SetX(30);
	     $pdf->writeHTMLCell(161, 0, '', '',$this->data['lampiran3'] , 0, 1, 0, true, 'J', true);
       

		$r_atasan =  $this->session->userdata('roleatasan'); 
        $r_biro =  $this->session->userdata('groleid');
        $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
        $atasanub = get_data_people('RoleAtasan', $this->data['Approve_People']);
		$pdf->Ln(10);
		if($this->data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'u.b. ',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}
		$pdf->Ln(2);
			$pdf->SetX(132);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'PEMERIKSA',0,1,'L');
		$pdf->Ln(5);		
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

		 }

		if (!empty($this->input->post('lampiran4') )) {

			$pdf->AddPage('P', 'F4');
			$pdf->SetX(95);
		 	$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran4, '', 1, 0, false, 'J', false);
		 $pdf->Ln(10);
		 $pdf->SetX(30);
	     $pdf->writeHTMLCell(161, 0, '', '',$this->data['lampiran4'] , 0, 1, 0, true, 'J', true);
        

$r_atasan =  $this->session->userdata('roleatasan'); 
        $r_biro =  $this->session->userdata('groleid');
        $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
        $atasanub = get_data_people('RoleAtasan', $this->data['Approve_People']);
		$pdf->Ln(10);
		if($this->data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'untuk_beliau') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$atasanub).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'u.b. ',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}
		$pdf->Ln(2);
			$pdf->SetX(132);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'PEMERIKSA',0,1,'L');
		$pdf->Ln(5);		
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
        }
		$pdf->Output('surat_dinas_view.pdf','I');

		// $this->load->view('backend/standart/administrator/pdf/view_surattugas_pdf', $this->data);

		// $content = ob_get_contents();
		// ob_end_clean();

		// $config = array(
		// 	'orientation' 	=> 'p',
		// 	'format' 		=> 'a4',
		// 	'marges' 		=> array(30, 30, 20, 30),
		// );


		// $this->pdf = new HtmlPdf($config);
		// $this->pdf->initialize($config);
		// $this->pdf->pdf->SetDisplayMode('fullpage');
		// $this->pdf->writeHTML($content);
		// $this->pdf->Output('naskah_dinas_tindaklanjut-' . $tanggal . '.pdf', 'H');
	}
	// Akhir Fungsi Surat Tugas

	//Simpan Registrasi Surat Tugas
	public function post_suratdinas_keluar()
	{

		$gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Lokasi;

		if($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;	
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}	

		if($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');		
		} else {
			$x = $this->input->post('Approve_People');
		}


	if($this->input->post('TtdText2') == 'none') {
		$a =('');
		$a = ('');
		} elseif($this->input->post('TtdText2') == 'AL') {		
		$a = $this->input->post('tmpid_atas_nama');	
		} else {
		$a = $this->input->post('Approve_People3');
		}	

		if($this->input->post('TtdText2') == 'none') {
		$b =('');
		$b = ('');
		} elseif($this->input->post('TtdText2') == 'AL') {		
		$b = $this->input->post('tmpid_atas_nama');	
		} else {
		$b = $this->input->post('Approve_People3');
		}	



		try{

			$data_konsep = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'type_naskah'       => $this->input->post('type_naskah'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $this->input->post('RoleId_To'),
				'Alamat'       		=> $this->input->post('alamat'),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'lampiran'       	=> $this->input->post('lampiran'),
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),

				'Approve_People'    => $x,
				'Approve_People3'   => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxkeluar',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 		=> $z,
				'Nama_ttd_atas_nama' 	=> $x,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];
			//memunculkan konsep awal
			$data_konsep_awal = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'type_naskah'       => $this->input->post('type_naskah'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $this->input->post('RoleId_To'),
				'Alamat'       		=> $this->input->post('alamat'),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'lampiran'       	=> $this->input->post('lampiran'),
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),

				'Approve_People'    => $x,
				'Approve_People3'    => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxkeluar',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 		=> $z,
				'Nama_ttd_atas_nama' 	=> $x,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'id_koreksi'    	=> $gir,
			];

			//akhir memunculkan konsep awal

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
			            'GIR_Id' 			=> $gir,
			            'NId' 				=> $data_konsep['NId_Temp'],
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);

				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);
			//memunculkan konsep awal
			$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep_awal);
			//akhir memunculkan konsep awal


			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid').date('dmyhis');
			
			$save_recievers = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_keluar',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];


			//memunculkan konsep awal
			$save_reciever = $this->model_inboxreciever->store($save_recievers);

				$save_recievers_koreksi = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'id_koreksi'	=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_keluar',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_reciever_koreksi = $this->model_inboxreciever_koreksi->store($save_recievers_koreksi);
			//akhir memunculkan konsep awal

			
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
		
	}
	//Fungsi Simpan Regis Surat Dinas Keluar

}