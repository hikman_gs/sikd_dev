<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Integrasi_masterpd extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		$this->load->model('Model_integrasi', 'integrasi');
	}


	public function index($id=0)
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		// $this->db->group_by('satuan_kerja_id');
		// $this->db->order_by('satuan_kerja_id');
		// $this->db->where('satuan_kerja_id !=', ' ');
		// $this->db->from('siap_unit_kerja');
		$data['map'] = $this->integrasi->get_mappd();
		$data['roleCode'] = $this->integrasi->get_rolecode();

		// $SukRole = $this->db->get();
		// $SukRole = $SukRole->result();

		// $this->data['SukRole'] = $SukRole;
		$this->data['title'] = 'Mapping Data PD ke GRole';
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/masterdata_pd',$data);
	}

	// public function update_pdd(){

	// 	$mappd  = $this->db->query("SELECT * FROM mapping_pd");
	// 	// $result = $this->db->get();
	// 	$x = $mappd->result();

	// 	//dd($x);exit();

	// 	foreach ($x as $y) {
			
	// 		$satker = $y->satuan_kerja_id;
	// 		$rolecode  = $this->db->query("SELECT * FROM mapping_per_pd where unit_kerja_id = '".$satker."'");
	// 		// dd($rolecode);
	// 		// // dd($y);

	// 		//$getMapp = $this->db->get();
	// 		$z = $rolecode->row()->rolecode_id;

	// 		$data = array(
	// 			'rolecode_id' => $z
	// 			);
	// 		$this->db->where('satuan_kerja_id', $satker);
	// 		$this->db->update('mapping_pd', $data);

	// 	}

	// 	echo 'DONE WISS ';
	// }

	public function update() {
		$id = $this->input->post('id');
		$roleid = $this->input->post('role');
		$unit_kerja_id = $this->input->post('unit_kerja');
		$data= array();
		if($roleid==''){
			$data=array(
				'GRoleId' => '',
				'GRoleName' => '',
				'eselon' => '',
				'rolecode_id' => '',
			);
		}else{
			$data=array(
				// 'GRoleId' => $roleid,
				//'GRoleName' => $this->integrasi->get_grolename($roleid),
				'eselon' => $this->integrasi->get_eselon($unit_kerja_id),
				'rolecode_id' => $roleid
			);
		}
		$update = $this->integrasi->update_mapping_pd($id, $data);
		if($update) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
		echo json_encode(array("status" => TRUE, "data" => $data));
	}
	
	// public function insert() {
		
	// 	$mgrole  = $this->db->query("SELECT * FROM siap_unit_kerja WHERE satuan_kerja_id = '".$this->input->post('id')."'");
	// 	$s = $mgrole->row();
		
	// 	$mgrolex  = $this->db->query("SELECT * FROM master_grole WHERE GRoleId = '".$this->input->post('role')."'");
	// 	$mastergrolex = $mgrolex->row();

	// 	$data = array( 
	// 		'satuan_kerja_id' => $s->satuan_kerja_id,
	// 		'unit_kerja_nama' => $s->unit_kerja_nama,
	// 		'eselon' => $s->eselon,
	// 		'GRoleId' => $this->input->post('role'),
	// 		'GRoleName' => $mastergrolex->GRoleName
	// 	);
	// 	$insert = $this->db->insert('mapping_pd', $data);
		
	// 	if($insert) {
	// 		$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil ditambahkan.</div>');
	// 	}
	// 	echo json_encode(array("status" => TRUE, "data" => $data, "mastergrolex" => $mastergrolex));
	// }
}