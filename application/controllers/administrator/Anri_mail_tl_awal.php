<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Anri_mail_tl extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inbox');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inbox_disposisi');
		$this->load->model('model_inbox_koreksi');
		$this->load->model('model_ttd');
		$this->load->model('model_inboxreciever_koreksi');
	}

	//Buka iframe history
	public function iframe_histori($NId)
	{
		$this->data['NId'] = $NId;
		$this->load->view('backend/standart/administrator/mail_tl/table_histori', $this->data);
	}
	//Tutup iframe history

	//Buka View Naskah Masuk
	public function view_naskah_masuk($tipe,$NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '".$NId."' AND To_Id = '".$this->session->userdata('peopleid')."' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		
		//UPDATE READ
		$this->db->where('NId',$NId);
		$this->db->where('To_Id',$this->session->userdata('peopleid'));
		$this->db->update('inbox_receiver',['StatusReceive' => 'read']);
		
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori', $this->data);
	}
	// public function view_naskah_masuk($tipe, $NId)
	// {

	// 	$this->data['NId'] 		= $NId;
	// 	$this->data['tipe'] 	= $tipe;

	// 	$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

	// 	//UPDATE READ
	// 	$this->db->where('NId', $NId);
	// 	$this->db->where('RoleId_To', $this->session->userdata('roleid'));
	// 	$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

	// 	$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori', $this->data);
	// }
	//Tutup View Naskah Masuk

	//Buka iframe KOREKSI
	public function iframe_koreksi($NId,$GIR_Id)
	{
		$this->data['NId'] 			= $NId;
		$this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['Koreksi'] 	= $this->db->query("SELECT Koreksi FROM inbox_koreksi WHERE NId = '".$NId."' AND GIR_Id = '".$GIR_Id."'")->row('Koreksi');
		$this->data['Msg'] 			= $this->db->query("SELECT Msg FROM inbox_receiver_koreksi WHERE NId = '".$NId."' AND GIR_Id = '".$GIR_Id."'")->row()->Msg;

		$this->load->view('backend/standart/administrator/mail_tl/table_koreksi',$this->data);
	}
	//Tutup iframe KOREKSI




	// //Buka iframe disposisi
	// public function iframe_koreksi($NId, $GIR_Id)
	// {
	// 	$this->data['NId'] 			= $NId;
	// 	$this->data['GIR_Id'] 		= $GIR_Id;
	// 	$this->data['Koreksi'] 	= $this->db->query("SELECT Koreksi FROM inbox_koreksi WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row('Koreksi');
	// 	$this->data['Msg'] 			= $this->db->query("SELECT Msg FROM inbox_receiver WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row()->Msg;

	// 	$this->load->view('backend/standart/administrator/mail_tl/table_koreksi', $this->data);
	// }
	// //Tutup iframe disposisi


	//Buka view naskah histori KOREKSI
	public function view_naskah_histori_koreksi($tipe,$NId)
	{
		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		$this->data['id_koreksi'] 		= $id_koreksi;
		$this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['data_koreksi']	= $this->db->query("SELECT id_koreksi FROM konsep_naskah_koreksi WHERE NId_Temp = '".$NId."' AND RoleId_From = '".$this->session->userdata('roleid')."'LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '".$NId."' AND RoleId_To = '".$this->session->userdata('roleid')."' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		$this->db->where('RoleId_To',$this->session->userdata('roleid'));
		$this->db->update('inbox_receiver_koreksi',['StatusReceive' => 'read']);
		
		// $this->data['data_koreksi'] = $this->db->query("SELECT * FROM konsep_naskah_koreksi WHERE NId = '".$NId)->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_ukdispu', $this->data);
		// var_dump($this->session->userdata('roleid'));
		// die();
	}
	//Tutup view naskah histori KOREKSI

	// //Buka view naskah histori
	// public function view_naskah_histori_koreksi($tipe, $NId)
	// {
	// 	$this->data['NId'] 			= $NId;
	// 	$this->data['tipe'] 		= $tipe;
	// 	$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
	// 	$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_ukdispu', $this->data);
	// }
	// //Tutup view naskah histori

	//PENAMBAHAN KOREKSI
	public function view_naskah_uk($tipe,$NId)
	{

		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		$this->data['id_koreksi'] 		= $id_koreksi;
		$this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['data_koreksi']	= $this->db->query("SELECT id_koreksi FROM konsep_naskah_koreksi WHERE NId_Temp = '".$NId."' AND RoleId_From = '".$this->session->userdata('roleid')."'LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver_koreksi WHERE NId = '".$NId."' AND RoleId_To = '".$this->session->userdata('roleid')."' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		//UPDATE READ
		$this->db->where('NId',$NId);
		$this->db->where('RoleId_To',$this->session->userdata('roleid'));
		$this->db->update('inbox_receiver_koreksi',['StatusReceive' => 'read']);
		
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_ukdispu', $this->data);
	}
	//AKHIR PENAMBAHAN KOREKSI
	// public function view_naskah_uk($tipe, $NId)
	// {

	// 	$this->data['NId'] 		= $NId;
	// 	$this->data['tipe'] 	= $tipe;

	// 	$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

	// 	//UPDATE READ
	// 	$this->db->where('NId', $NId);
	// 	$this->db->where('RoleId_To', $this->session->userdata('roleid'));
	// 	$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

	// 	$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_ukdispu', $this->data);
	// }


	//tambahan dispu
	public function view_naskah_koreksi($tipe, $NId)
	{

		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_koreksi', $this->data);
	}
	//Tutup View Naskah disposisi

	//Buka selesai tindaklanut
	public function selesai_tindaklanjut($tipe, $NId)
	{
		$x = $_GET['dt'];

		//UPDATE STATUS
		$this->db->where('NId', $NId);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['Status' => 1]);

		set_message('Data Berhasil Disimpan', 'success');
		if ($tipe == 'log') {
			redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
		} else {
			redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId . '?dt=' . $x));
		}
	}
	//Tutup selesai tindaklanut

	//Buka view naskah histori
	public function view_naskah_histori($tipe, $NId)
	{
		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori', $this->data);
	}
	//Tutup view naskah histori

	//Buka view metadata
	public function view_naskah_metadata($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox']	 = $this->db->query("SELECT NTglReg, JenisId, Tgl, Nomor, NAgenda, Hal, Instansipengirim, UrgensiId, SifatId FROM inbox WHERE NId = '" . $NId . "' LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_metadata', $this->data);
	}
	//Tutup view metadata

	public function view_naskah_metadata_koreksi($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox']	 = $this->db->query("SELECT NTglReg, JenisId, Tgl, Nomor, NAgenda, Hal, Instansipengirim, UrgensiId, SifatId FROM inbox WHERE NId = '" . $NId . "' LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_metadata_koreksi', $this->data);
	}

	//Buka view naskah dinas
	public function view_naskah_dinas($tipe, $NId)
	{
		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_naskah', $this->data);
	}
	//Buka view naskah dinas

	//Buka iframe disposisi
	public function iframe_disposisi($NId, $GIR_Id)
	{
		$this->data['NId'] 			= $NId;
		$this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['Disposisi'] 	= $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row('Disposisi');
		$this->data['Msg'] 			= $this->db->query("SELECT Msg FROM inbox_receiver WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row()->Msg;

		$this->load->view('backend/standart/administrator/mail_tl/table_disposisi', $this->data);
	}
	//Tutup iframe disposisi


	//tambahan dispu
	public function view_naskah_disposisi($tipe, $NId)
	{

		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_disposisi', $this->data);
	}
	//Tutup View Naskah disposisi

	//Buka hapus histori
	public function hapus_histori($tipe, $NId, $GIR_Id)
	{
		$x = $_GET['dt'];

		$query1	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '" . $NId . "'");

		foreach ($query1->result_array() as $row1) :
			$fileDir = $row1['NFileDir'];
		endforeach;

		if ($fileDir != '') {

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach ($query->result_array() as $row) :
				if (is_file($files . $row['FileName_fake']))
					unlink($files . $row['FileName_fake']);
			endforeach;
		}

		$this->db->query("DELETE FROM inbox_receiver WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'");

		$this->db->query("DELETE FROM inbox_disposisi WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'");

		$this->db->query("DELETE FROM inbox_files WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'");


		$cari = $this->db->query("SELECT GIR_Id FROM inbox_receiver WHERE NId = '" . $NId . "' AND ReceiverAs in ('to','to_notadinas','to_forward','cc1') ORDER BY ReceiveDate DESC LIMIT 0,1")->row();

		//UPDATE STATUS
		$this->db->where('NId', $NId);
		$this->db->where('GIR_Id', $cari->GIR_Id);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['Status' => 0]);

		set_message('Data Berhasil Dihapus', 'success');
		if ($tipe == 'log') {
			redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
		} else {
			redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId . '?dt=' . $x));
		}
	}
	//Tutup hapus histori

	//View PDF Naskah Masuk
	public function log_naskah_masuk_pdf($id)
	{
		$this->load->library('HtmlPdf');
		ob_start();
		$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $id . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		//penambahan dispu
		$id_approve = $this->db->query("SELECT * FROM inbox_receiver WHERE NId = '" . $id . "' ORDER BY From_Id DESC LIMIT 1")->row();
		//akhir penambahan dispu
		$this->data['NId'] 				= $id;
		$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
		$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Jumlah'] 			= $naskah->Jumlah;
		$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
		$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		$this->data['TglNaskah'] 		= $naskah->TglNaskah;
		$this->data['Hal'] 				= $naskah->Hal;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['Approve_People3'] 	= $naskah->Approve_People3;

		//penambahan dispu
		$this->data['nama_approve'] 			= $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $id_approve->Nama_ttd_konsep . "'")->row()->RoleName;
		//akhir penambahan dispu

		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['Number'] 			= $naskah->Number;
		$this->data['nosurat'] 			= $naskah->nosurat;
		$this->data['lokasi'] 			= $naskah->lokasi;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->view('backend/standart/administrator/mail_tl/inbox_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxsprint') {
			$this->load->view('backend/standart/administrator/mail_tl/surattugas_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxundangan') {
			$this->load->view('backend/standart/administrator/mail_tl/undangan_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxkeluar') {
			$this->load->view('backend/standart/administrator/mail_tl/suratdinas_pdf', $this->data);
		} else {
			$this->load->view('backend/standart/administrator/mail_tl/inbox_pdf', $this->data);
		}

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 20),
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('konsep_naskah.pdf', 'H');
	}
	//Akhir View PDF Naskah Masuk						

	//View PDF Naskah Masuk
	public function log_naskah_masuk_pdf_log($id)
	{
		
		$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $id . "' ORDER BY TglNaskah DESC LIMIT 1")->row();
		//penambahan dispu
		$id_approve = $this->db->query("SELECT * FROM inbox_receiver WHERE NId = '" . $id . "' ORDER BY From_Id DESC LIMIT 1")->row();
		//akhir penambahan
		$this->data['NId'] 				= $id;
		$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
		$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Alamat'] 			= $naskah->Alamat;
		$this->data['Hal'] 				= $naskah->Hal;

		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
			$this->data['Jumlah'] 			= $naskah->Jumlah;
			$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}

		$this->data['TglNaskah'] 		= $naskah->TglNaskah;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['Approve_People3'] 	= $naskah->Approve_People3;
		//penambahan dispu
		$this->data['nama_approve'] 			= $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $id_approve->RoleId_From . "'")->row()->RoleName;
		//akhir penambahan
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['Number'] 			= $naskah->Number;
		$this->data['nosurat'] 			= $naskah->nosurat;
		$this->data['lokasi'] 			= $naskah->lokasi;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/nodin_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxsprint') {
			$this->load->library('Pdf');
			// $this->load->view('backend/standart/administrator/mail_tl/surattugas_pdf', $this->data);
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',',$this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Pangkat', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_perintah = $this->db->get('people')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			//tcpdf
			$r_atasan =  $this->session->userdata('roleatasan'); 
			$r_biro =  $this->session->userdata('groleid');
			$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 


			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px">
				<tr nobr="true">
					<td rowspan="7" width="60px" style="valign:bottom;">
					<br><br>
					<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
					<td width="180px">Ditandatangani secara elekronik oleh:</td>
				</tr>
				<tr nobr="true">
					<td width="180px" style="text-align: left; font-size:9;">'.get_data_people('RoleName', $this->data['Approve_People']).',</td>
				</tr>
				<tr nobr="true">
					<td><br></td>
				</tr>
				<tr nobr="true">
					<td>
						<p style="float: right!important;">'.$this->data['Nama_ttd_konsep'].'</p>
					</td>
				</tr>
				<tr nobr="true">
					<td>'.$tujuan. '
					</td>
				</tr>
			</table>';

			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
			$pdf->Ln(10);
			$pdf->Cell(0,10,'SURAT PERINTAH',0,0,'C');
			$pdf->Ln(5);
			$pdf->Cell(0,10,'NOMOR : '.$this->data['nosurat'],0,0,'C');
			$pdf->Ln(15);
			$pdf->Cell(30,5,'DASAR',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->writeHTMLCell(125, 5, '', '', $this->data['Hal'], '', 1, 0, true, 'J', true);
			$pdf->Cell(0,10,'MEMERINTAHKAN:',0,1,'C');
			$pdf->Ln(5);
			$pdf->Cell(30,5,'Kepada',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$i=0;
			foreach($data_perintah as $row){
				$i++;
				$pdf->SetX(65);
				$pdf->Cell(10,5,$i.'. ',0,0,'L');
				$pdf->SetX(75);
				$pdf->Cell(25,5,'Nama',0,0,'L');
				$pdf->Cell(5,5,':',0,0,'L');
				$pdf->MultiCell(95,5,$row->PeopleName,0,'L');
				$pdf->SetX(75);
				$pdf->Cell(25,5,'Pangkat',0,0,'L');
				$pdf->Cell(5,5,':',0,0,'L');
				$pdf->Cell(95,5,$row->Pangkat,0,1,'L');
				$pdf->SetX(75);
				$pdf->Cell(25,5,'NIP',0,0,'L');
				$pdf->Cell(5,5,':',0,0,'L');
				$pdf->Cell(95,5,$row->NIP,0,1,'L');
				$pdf->SetX(75);
				$pdf->Cell(25,5,'Jabatan',0,0,'L');
				$pdf->Cell(5,5,':',0,0,'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				$pdf->MultiCell(85,5,$str,0,'L');
			}
			$pdf->Ln(10);
			$pdf->Cell(30,5,'Untuk',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->writeHTMLCell(90, 0, '', '', $this->data['Konten'], '', 1, 0, false, 'J', false);
			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
			$pdf->Ln(10);
			$pdf->SetX(125);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30,5,'Ditetapkan di '.$this->data['lokasi'],0,1,'L');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30,5,'Pada tanggal '.get_bulan($this->data['TglNaskah']),0,1,'L');

			if($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
			}elseif($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
			}elseif($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,'a.n '.get_data_people('RoleName',$this->data['Approve_People3'].','),0,'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
			}else{
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
			}
			$pdf->Ln(5);
			$pdf->SetX(100);
			$pdf->MultiCell(90,10,'PEMERIKSA',0,'C');
			
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			$pdf->Output('konsep_naskah_surat_perintah.pdf','I');
			die();

			// $this->load->view('backend/standart/administrator/mail_tl/surattugas_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxundangan') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/undangan_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxkeluar') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/suratdinas_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxedaran') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/suratedaran_pdf', $this->data);
		} else {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/inbox_pdf_log', $this->data);
		}

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 20),
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('konsep_naskah.pdf', 'H');
	}
	//Akhir View PDF Naskah Masuk

	//PDF nota dinas tindaklanjut
	public function nota_dinas_tindaklanjut_pdf($NId)
	{
		$this->load->library('HtmlPdf');
		ob_start();

		$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

		$this->data['NId'] 				= $NId;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Jumlah'] 			= $naskah->Jumlah;
		$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		$this->data['TglReg'] 			= $naskah->TglReg;
		$this->data['Hal'] 				= $naskah->Hal;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_tindaklanjut_pdf', $this->data);

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 20),
		);


		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	//Akhir PDF nota dinas tindaklanjut

	//Buka view naskah dinas tindaklanjut edit
	public function view_naskah_dinas_tindaklanjut_edit($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['naskah'] 	= $this->model_konsepnaskah->find($NId);

		$this->tempanri('backend/standart/administrator/mail_tl/view_naskah_dinas_tindaklanjut_edit', $this->data);
	}
	//Tutup view naskah dinas tindaklanjut edit

	//Buka verifikasi
	public function verifikasi($NId)
	{
		$tanggal = date('Y-m-d H:i:s');
		$GIR_Id = $this->db->query("SELECT GIR_Id FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' ")->row()->GIR_Id;

		try {
			$this->load->library('user_agent');
			get_data_konsepnaskah('NId_Temp', $NId);
			$password = $this->input->post('password');
			$where 	  = array('PeoplePassword' => sha1($password),);
			$cek 	  = $this->db->get_where("people", $where)->num_rows();

			$kalimat = str_replace(' ', '', $password);
			if ($kalimat == '') {
				set_message('Password Wajib Diisi', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			} else {

				if ($cek > 0) {

					$logopath = './uploads/anri.png'; //Logo yang akan di insert

					//Untuk menghitung nomor nota dinas
					$tahun 	= date('Y');
					$query = $this->db->query("SELECT (max(Number) + 1) as id FROM konsep_naskah WHERE ReceiverAs = 'to_notadinas' AND YEAR(TglNaskah) = '" . $tahun . "' AND RoleId_From = '" . $this->session->userdata('roleid') . "'")->row()->id;

					if (!empty($query[0])) {
						$id = $query;
					} else {
						$id = 1;
					}

					$cari = $this->db->query("SELECT ClId FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->ClId;

					$klas = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $cari . "'")->row()->ClCode;

					$nosurat = $id . '/' . $klas . '/' . $tahun;


					//KONSEP NASKAH
					$this->db->where('NId_Temp', $NId);
					$this->db->update('konsep_naskah', ['Konsep' => 0, 'Number' => $id, 'nosurat' => $nosurat, 'TglNaskah' => date('Y-m-d H:i:s')]);


					$CI = &get_instance();
					$this->load->library('ciqrcode'); //pemanggilan library QR CODE
					$this->load->helper('string');

					$config['cacheable']    = true; //boolean, the default is true
					$config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
					$config['errorlog']     = './uploads/'; //string, the default is application/logs/
					$config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
					$config['quality']      = true; //boolean, the default is true
					$config['size']         = '1024'; //interger, the default is 1024
					$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
					$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
					$CI->ciqrcode->initialize($config);

					$image_name				= $NId . '-' . date('Ymd') . '.png'; //buat name dari qr code sesuai dengan

					$params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/') . $NId; //data yang akan di jadikan QR CODE
					$params['level'] 		= 'H'; //H=High
					$params['size'] 		= 10;
					$params['savename'] 	= FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
					$CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

					//Memasukkan logo di qrcode
					$fullpath = FCPATH . $config['imagedir'] . $image_name;

					$QR = imagecreatefrompng($fullpath);
					// memulai menggambar logo dalam file qrcode
					$logo = imagecreatefromstring(file_get_contents($logopath));
					imagecolortransparent($logo, imagecolorallocatealpha($logo, 0, 0, 0, 127));

					imagealphablending($logo, false);
					imagesavealpha($logo, true);

					$QR_width = imagesx($QR); //get logo width
					$QR_height = imagesy($QR); //get logo width

					$logo_width = imagesx($logo);
					$logo_height = imagesy($logo);

					// Scale logo to fit in the QR Code
					$logo_qr_width = $QR_width / 4;
					$scale = $logo_width / $logo_qr_width;
					$logo_qr_height = $logo_height / $scale;

					//220 -> left,  210 -> top
					imagecopyresampled($QR, $logo, 220, 210, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
					// imagecopyresampled($QR, $logo, $QR_width/4, $QR_height/3.5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
					// 
					// Simpan kode QR lagi, dengan logo di atasnya

					imagepng($QR, $fullpath);

					$data_ttd = [
						'NId' 				=> $NId,
						'TglProses' 		=> '',
						'PeopleId' 			=> $this->session->userdata('peopleid'),
						'RoleId' 			=> $this->session->userdata('roleid'),
						'Verifikasi'  		=> random_string('sha1', 10),
						'Identitas_Akses' 	=> '',
						'QRCode' 			=> $image_name,
						'TglProses' 		=> date('Y-m-d H:i:s'),
					];

					$save_ttd 		= $this->model_ttd->store($data_ttd);

					//Create PDF File
					$this->load->library('HtmlPdf');
					ob_start();
					$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' ORDER BY TglNaskah DESC LIMIT 1")->row();
					$this->data['NId'] 				= $NId;
					$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
					$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
					$this->data['RoleId_To'] 		= $naskah->RoleId_To;
					$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
					$this->data['Konten'] 			= $naskah->Konten;
					$this->data['Jumlah'] 			= $naskah->Jumlah;
					$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
					$this->data['TglNaskah'] 		= $naskah->TglNaskah;
					$this->data['Hal'] 				= $naskah->Hal;
					$this->data['Approve_People'] 	= $naskah->Approve_People;
					$this->data['TtdText'] 			= $naskah->TtdText;
					$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
					$this->data['Number'] 			= $naskah->Number;
					$this->data['nosurat'] 			= $naskah->nosurat;
					$this->data['lokasi'] 			= $naskah->lokasi;
					$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

					$this->load->view('backend/standart/administrator/mail_tl/inbox_pdf', $this->data);
					$content = ob_get_contents();
					ob_end_clean();

					$config = array(
						'orientation' => 'p',
						'format' => 'f4',
						'marges' 		=> array(30, 30, 20, 20),
					);

					$this->pdf = new HtmlPdf($config);
					$this->pdf->initialize($config);
					$this->pdf->pdf->SetDisplayMode('fullpage');
					$this->pdf->writeHTML($content);

					$a = 'nota_dinas_tindaklanjut-' . $NId . '.pdf';
					$this->pdf->Output('FilesUploaded/naskah/nota_dinas_tindaklanjut-' . $NId . '.pdf', 'F');

					//Simpan Inbox File
					//Karena naskah nya merupakan tindaklanjut, maka nilai gir_id ditukar dengan nid
					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $NId,
						'NId' 				=> $GIR_Id,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $a,
						'FileName_fake' 	=> $a,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outbox',
					];

					$save_file = $this->model_inboxfile->store($save_files);

					set_message('Data Berhasil Disimpan', 'success');
					redirect(BASE_URL('administrator/anri_list_notadinas_sdh_ap/'));
					redirect($this->agent->referrer());
				} else {
					set_message('Kode Verifikasi Salah', 'error');
					redirect($this->agent->referrer());
				}
			}
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_dinas/' . $NId));
		}
	}
	//Tutup verifikasi

	//Buka kirim naskah dinas
	public function naskah_dinas_kirim()
	{
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$nid = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			$this->load->helper('string');

			if (empty($this->input->post('nid'))) {

				$kepada 	= explode(',', get_data_konsepnaskah('RoleId_To', $this->input->post('temp')));
				$tembusan 	= explode(',', get_data_konsepnaskah('RoleId_Cc', $this->input->post('temp')));

				$save_data = [
					'NKey'         		=> tb_key(),
					'NId'				=> $nid,
					'CreatedBy'			=> $this->session->userdata('peopleid'),
					'CreationRoleId'	=> $this->session->userdata('roleid'),
					'NTglReg' 			=> $tanggal,
					'Tgl' 				=> date('Y-m-d', strtotime($this->input->post('Tgl'))),
					'JenisId' 			=> get_data_konsepnaskah('JenisId', $this->input->post('temp')),
					'Nomor' 			=> get_data_konsepnaskah('Number', $this->input->post('temp')),
					'Hal' 				=> get_data_konsepnaskah('Hal', $this->input->post('temp')),
					'Pengirim'			=> 'internal',
					'NTipe' 			=> get_data_konsepnaskah('Ket', $this->input->post('temp')),
					'NFileDir'			=> 'naskah',
					'BerkasId'			=> '1',
				];
				$save_inbox = $this->model_inbox->store($save_data);

				if (count((array) $kepada)) {
					foreach ($kepada as $data_kepada) {
						$datakepada = [
							'NId' 			=> $this->input->post('temp'),
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $gir,
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $data_kepada,
							'RoleId_To' 	=> get_data_people('RoleId', $data_kepada),
							'ReceiverAs' 	=> get_data_konsepnaskah('ReceiverAs ', $this->input->post('temp')),
							'Msg' 			=> get_data_konsepnaskah('Konten', $this->input->post('temp')),
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> get_data_people('RoleDesc', $data_kepada),
						];

						$save_kepada = $this->model_inboxreciever->store($datakepada);
					}
				}

				if (count((array) $tembusan)) {
					foreach ($tembusan as $data_tembusan) {
						$datatembusan = [
							'NId' 			=> $this->input->post('temp'),
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $gir,
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $data_tembusan,
							'RoleId_To' 	=> get_data_people('RoleId', $data_tembusan),
							'ReceiverAs' 	=> 'bcc',
							'Msg' 			=> get_data_konsepnaskah('Konten', $this->input->post('temp')),
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> get_data_people('RoleDesc', $data_tembusan),
						];

						$save_tembusan = $this->model_inboxreciever->store($datatembusan);
					}
				}
			} else {

				$kepada 	= explode(',', get_data_konsepnaskah('RoleId_To', $this->input->post('nid')));
				$tembusan 	= explode(',', get_data_konsepnaskah('RoleId_Cc', $this->input->post('nid')));

				if (count((array) $kepada)) {
					foreach ($kepada as $data_kepada) {
						$datakepada = [
							'NId' 			=> $this->input->post('girid'),
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $this->input->post('nid'),
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $this->db->query("SELECT PeopleId FROM people WHERE PrimaryRoleId = '" . $data_kepada . "'")->row()->PeopleId,
							'RoleId_To' 	=> $data_kepada,
							'ReceiverAs' 	=> 'to_notadinas',
							'Msg' 			=> get_data_konsepnaskah('Konten', $this->input->post('nid')),
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> $this->db->query("SELECT RoleDesc FROM role WHERE RoleId = '" . $data_kepada . "'")->row()->RoleDesc,
						];

						$save_kepada = $this->model_inboxreciever->store($datakepada);
					}
				}
				if (count((array) $tembusan)) {

					foreach ($tembusan as $data_tembusan) {
						if ($data_tembusan != '') {
							$datatembusan = [
								'NId' 			=> $this->input->post('girid'),
								'NKey' 			=> tb_key(),
								'GIR_Id' 		=> $this->input->post('nid'),
								'From_Id' 		=> $this->session->userdata('peopleid'),
								'RoleId_From' 	=> $this->session->userdata('roleid'),
								'To_Id' 		=> $this->db->query("SELECT PeopleId FROM people WHERE PrimaryRoleId = '" . $data_tembusan . "'")->row()->PeopleId,
								'RoleId_To' 	=> $data_tembusan,
								'ReceiverAs' 	=> 'bcc',
								'Msg' 			=> get_data_konsepnaskah('Konten', $this->input->post('nid')),
								'StatusReceive' => 'unread',
								'ReceiveDate' 	=> $tanggal,
								'To_Id_Desc' 	=> $this->db->query("SELECT RoleDesc FROM role WHERE RoleId = '" . $data_tembusan . "'")->row()->RoleDesc,
							];

							$save_tembusan = $this->model_inboxreciever->store($datatembusan);
						}
					}
				}
			}

			$this->db->where('NId_Temp', $this->input->post('nid'));
			$this->db->update('konsep_naskah', ['Konsep' => 2]);

			$data = array('status' => 'success',);
			echo json_encode($data);
		} catch (\Exception $e) {
			$data = array('status' => 'error',);
			echo json_encode($data);
		}
	}
	//Tutup kirim naskah dinas

	//Hapus naskah dinas tindaklanjut
	public function hapus_naskah_dinas_tindaklanjut($tipe, $NId)
	{
		try {
			$cari = $this->db->query("SELECT GIR_Id FROM inbox_receiver WHERE NId = '" . $NId . "' ORDER BY ReceiveDate DESC LIMIT 0,1")->row();

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('GIR_Id', $cari->GIR_Id);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 0]);

			$query1 	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '" . $NId . "'");
			foreach ($query1->result_array() as $row1) :
				$fileDir = $row1['NFileDir'];
			endforeach;

			if ($fileDir != '') {

				$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $this->input->post('temp') . "'");

				$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

				foreach ($query->result_array() as $row) :
					if (is_file($files . $row['FileName_fake']))
						unlink($files . $row['FileName_fake']);
				endforeach;
			}

			$query21 	= $this->db->query("SELECT QRCode FROM ttd WHERE NId = '" . $this->input->post('temp') . "'");

			$files21 = FCPATH . 'FilesUploaded/qrcode/';

			foreach ($query21->result_array() as $row21) :
				if (is_file($files21 . $row21['QRCode']))
					unlink($files21 . $row21['QRCode']);
			endforeach;

			$this->db->query("DELETE FROM `konsep_naskah` WHERE NId_Temp = '" . $this->input->post('temp') . "'");
			$this->db->query("DELETE FROM `ttd` WHERE NId = '" . $this->input->post('temp') . "'");
			$this->db->query("DELETE FROM `inbox_files` WHERE NId = '" . $this->input->post('temp') . "'");

			$data = array('status' => 'success',);
			echo json_encode($data);
		} catch (\Exception $e) {
			$data = array('status' => 'error',);
			echo json_encode($data);
		}
	}
	//Akhir Hapus naskah dinas tindaklanjut

	//Buka inbox teruskan
	public function inbox_teruskan($tipe, $NId)
	{
		$x = $this->input->post('xdt');
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			if (count((array) $this->input->post('tujuan'))) {
				foreach ($_POST['tujuan'] as $receiver) {
					$save_recievers = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $receiver,
						'RoleId_To' 	=> get_data_people('RoleId', $receiver),
						'ReceiverAs' 	=> 'to_forward',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $receiver),
					];

					$save_reciever = $this->model_inboxreciever->store($save_recievers);
				}
			}

			if (count((array) $this->input->post('tembusan'))) {
				foreach ($_POST['tembusan'] as $tembusan) {
					$data_tembusan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tembusan,
						'RoleId_To' 	=> get_data_people('RoleId', $tembusan),
						'ReceiverAs' 	=> 'bcc',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tembusan),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tembusan);
				}
			}

			if (!empty($_POST['upload_teruskan_name'])) {

				foreach ($_POST['upload_teruskan_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_teruskan_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_teruskan_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup inbox teruskan

	//Buka inbox nota dinas
	public function inbox_nota_dinas($NId)
	{
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			if (count((array) $this->input->post('tujuan'))) {
				foreach ($_POST['tujuan'] as $receiver) {
					$save_recievers = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $receiver,
						'RoleId_To' 	=> get_data_people('RoleId', $receiver),
						'ReceiverAs' 	=> 'cc',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $receiver),
					];

					$save_reciever = $this->model_inboxreciever->store($save_recievers);
				}
			}

			if (count((array) $this->input->post('tembusan'))) {
				foreach ($_POST['tembusan'] as $tembusan) {
					$data_tembusan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tembusan,
						'RoleId_To' 	=> get_data_people('RoleId', $tembusan),
						'ReceiverAs' 	=> 'bcc',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tembusan),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tembusan);
				}
			}

			if (!empty($_POST['upload_notadinas_name'])) {

				foreach ($_POST['upload_notadinas_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_notadinas_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_notadinas_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $data_konsep['NId_Temp'],
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup inbox nota dinas

	//Buka inbox disposisi
	public function post_inbox_disposisi($tipe, $NId)
	{
		$x = $this->input->post('xdt');
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			$CI = &get_instance();
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
			$this->load->helper('string');
			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
			$config['errorlog']     = './uploads/'; //string, the default is application/logs/
			$config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
			$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
			$CI->ciqrcode->initialize($config);
			$image_name				= $NId . random_string('sha1', 10) . '-' . date('Ymd') . '.png'; //buat name dari qr code sesuai dengan


			$data_dispo = [
				'NId' 		=> $NId,
				'GIR_Id' 	=> $gir,
				'NoIndex' 	=> $this->input->post('NoIndex'),
				'Sifat' 	=> $this->input->post('Sifat'),
				'Disposisi' => implode('|', $this->input->post('Disposisi')),
				'RoleId' 	=> $this->session->userdata('roleid'),
				'QRCode' 	=> $image_name,
			];

			$params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/') . $data_dispo['GIR_Id']; //data yang akan di jadikan QR CODE
			$params['level'] 		= 'H'; //H=High
			$params['size'] 		= 10;
			$params['savename'] 	= FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
			$CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

			$save_dispo = $this->model_inbox_disposisi->store($data_dispo);

			if (count((array) $this->input->post('RoleId'))) {
				foreach ($_POST['RoleId'] as $tujuan) {
					$data_tujuan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tujuan,
						'RoleId_To' 	=> get_data_people('RoleId', $tujuan),
						'ReceiverAs' 	=> 'cc1',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tujuan),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tujuan);
				}
			}

			if (count((array) $this->input->post('tujuan_lainnya'))) {
				foreach ($_POST['tujuan_lainnya'] as $tujuanlain) {
					$data_tujuanlain = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tujuanlain,
						'RoleId_To' 	=> get_data_people('RoleId', $tujuanlain),
						'ReceiverAs' 	=> 'cc1',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tujuanlain),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tujuanlain);
				}
			}


			if (!empty($_POST['upload_disposisi_name'])) {

				foreach ($_POST['upload_disposisi_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_disposisi_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_disposisi_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
			} else {
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId . '?dt=' . $x));
			}
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
			} else {
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId . '?dt=' . $x));
			}
		}
	}
	//Tutup inbox disposisi

//inbox KOREKSI
public function post_inbox_koreksi($tipe,$NId)
	{
		$x = $this->input->post('xdt');
		$gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try{
			$CI =& get_instance();
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
			$this->load->helper('string');
	        $config['cacheable']    = true; //boolean, the default is true
	        $config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
	        $config['errorlog']     = './uploads/'; //string, the default is application/logs/
	        $config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
	        $config['quality']      = true; //boolean, the default is true
	        $config['size']         = '1024'; //interger, the default is 1024
	        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
	        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
	        $CI->ciqrcode->initialize($config);
	        $image_name				= $NId.random_string('sha1',10).'-'.date('Ymd').'.png'; //buat name dari qr code sesuai dengan
	        

	        $data_dispo = [
						'NId' 		=> $NId,
						'GIR_Id' 	=> $gir,
						'NoIndex' 	=> $this->input->post('NoIndex'),
						'Koreksi' => implode('|',$this->input->post('Koreksi')),
						'RoleId' 	=> $this->session->userdata('roleid'),
						'QRCode' 	=> $image_name,
					];

			$params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/').$data_dispo['GIR_Id']; //data yang akan di jadikan QR CODE
	        $params['level'] 		= 'H'; //H=High
	        $params['size'] 		= 10;
	        $params['savename'] 	= FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
	        $CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

			$save_dispo = $this->model_inbox_koreksi->store($data_dispo);

			if (count((array) $this->input->post('RoleId'))) {
				foreach ($_POST['RoleId'] as $tujuan) {
					$data_tujuan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tujuan,
						'RoleId_To' 	=> get_data_people('RoleId',$tujuan),
						'ReceiverAs' 	=> 'koreksi',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc',$tujuan),
					];

					$simpan_reciever = $this->model_inboxreciever_koreksi->store($data_tujuan);
				}
			}

			if (count((array) $this->input->post('tujuan_lainnya'))) {
				foreach ($_POST['tujuan_lainnya'] as $tujuanlain) {
					$data_tujuanlain = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tujuanlain,
						'RoleId_To' 	=> get_data_people('RoleId',$tujuanlain),
						'ReceiverAs' 	=> 'cc1',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc',$tujuanlain),
					];

					$simpan_reciever = $this->model_inboxreciever_koreksi->store($data_tujuanlain);
				}
			}

			
			if (!empty($_POST['upload_disposisi_name'])) {

				foreach ($_POST['upload_disposisi_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(FCPATH.'uploads/tmp/'.$_POST['upload_disposisi_uuid'][$idx].'/'.$file_name,
							FCPATH.'FilesUploadedkoreksi/naskah/'.$test_title_name_copy);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH.'uploads/tmp/'.$_POST['upload_disposisi_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
			            'GIR_Id' 			=> $gir,
			            'NId' 				=> $NId,
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile_koreksi->store($save_files);

				}
			}

			//UPDATE STATUS
			$this->db->where('NId',$NId);
			$this->db->where('RoleId_To',$this->session->userdata('roleid'));
			$this->db->update('inbox_receiver_koreksi',['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/'.$NId.'?dt='.$x));
			}else{
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_uk/view/'.$NId.'?dt='.$x));
			}

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/'.$NId.'?dt='.$x));
			}else{
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_uk/view/'.$NId.'?dt='.$x));
			}
		}
	}
	//Tutup inbox KOREKSI

	//Fungsi Lihat Pengatar Nota Dinas Tindaklanjut
	public function lihat_pengantar_naskah_dinas_tindaklanjut()
	{
		$this->load->library('HtmlPdf');
		ob_start();

		$tipe = $this->input->post('xdt2');
		$NId = $this->input->post('xdt1');

		$this->data['NId'] 			= $NId;
		$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$this->data['Jumlah'] 		= $this->input->post('Jumlah');
		$this->data['MeasureUnitId'] = $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $this->input->post('MeasureUnitId') . "'")->row()->MeasureUnitName;
		$this->data['Pesan'] 		= $this->input->post('Pesan');
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['Hal_pengantar'] 			= $this->input->post('Hal_pengantar');
		$this->data['Approve_People'] 			= (($this->input->post('TtdText') == 'none') ? $this->session->userdata('peopleid') : $this->input->post('Approve_People'));
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');

		$this->load->view('backend/standart/administrator/pdf/view_pengantar_nota_dinas_tindaklanjut_pdf', $this->data);

		$content = ob_get_contents();
		ob_end_clean();
		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 20),
		);


		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('pengantar_naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	// Akhir Fungsi Pengantar Nota Dinas Tindaklanjut

	//Fungsi Lihat Nota Dinas Tindaklanjut
	public function lihat_naskah_dinas_tindaklanjut()
	{
		$this->load->library('HtmlPdf');
		ob_start();

		$tipe = $this->input->post('xdt2');
		$NId = $this->input->post('xdt1');

		$this->data['NId'] 			= $NId;
		$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$this->data['Jumlah'] 		= $this->input->post('Jumlah');
		$this->data['MeasureUnitId'] = $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $this->input->post('MeasureUnitId') . "'")->row()->MeasureUnitName;
		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['Hal'] 			= $this->input->post('Hal');
		$this->data['Approve_People'] 			= (($this->input->post('TtdText') == 'none') ? $this->session->userdata('peopleid') : $this->input->post('Approve_People'));
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');

		if ($this->input->post('TtdText') == 'none') {
			$this->data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} else {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		$this->load->view('backend/standart/administrator/pdf/view_nota_dinas_tindaklanjut_pdf', $this->data);

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 20),
		);


		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	// Akhir Fungsi Nota Dinas Tindaklanjut

	//Fungsi Nota Dinas Tindaklanjut
	public function post_naskah_dinas_tindaklanjut()
	{
		$x = $this->input->post('xdt');
		$tipe = $this->input->post('xdt2');
		$NId = $this->input->post('xdt1');
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

		if ($this->input->post('TtdText') == 'none') {
			$xz = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$xz = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$xz = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		try {

			$data_konsep = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $NId,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Nota Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'ReceiverAs'       	=> 'to_notadinas',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '', //get_nomor_surat('to_nadin'),
				// 'Number'       		=> $data,
				'RoleCode'       	=> $this->session->userdata('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> ($this->input->post('RoleId_To') == NULL ? $this->input->post('RoleId_To') : implode(',', $this->input->post('RoleId_To'))),
				'RoleId_Cc'       	=> ($this->input->post('RoleId_Cc') == NULL ? $this->input->post('RoleId_Cc') : implode(',', $this->input->post('RoleId_Cc'))),
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'TtdText'       	=> $this->input->post('TtdText'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				// 'Hal_pengantar'     => $this->input->post('Hal_pengantar'),
				'NFileDir'     		=> 'naskah',
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'nadin',
				// 'Pesan'       		=> $this->input->post('Pesan'),
				// 'Nama_ttd_pengantar'=> $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".get_field_people('PeopleId')."'")->row()->ApprovelName,		
				'Nama_ttd_konsep'  	=> $xz,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];

			if (!empty($_POST['upload_naskahdinas_name'])) {
				foreach ($_POST['upload_naskahdinas_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_naskahdinas_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_naskahdinas_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;
					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

			// 	UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_dinas/' . $tipe . '/' . $NId . '?dt=' . $x));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	// Akhir Fungsi Nota Dinas Tindaklanjut

	//Fungsi Ubah Nota Dinas Tindaklanjut
	public function post_naskah_dinas_tindaklanjut_edit($tipe, $NId)
	{
		$x = $this->input->post('xdt');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

		if ($this->input->post('TtdText') == 'none') {
			$xz = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->session->userdata('peopleid') . "'")->row()->ApprovelName;
		} else {
			$xz = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('pdf') == '1') {
			$this->load->library('HtmlPdf');
			ob_start();

			$this->data['NId'] 				= $NId;
			$this->data['RoleId_To'] 		= $this->input->post('RoleId_To');
			$this->data['RoleId_Cc'] 		= $this->input->post('RoleId_Cc');
			$this->data['Konten'] 			= $this->input->post('Konten');
			$this->data['TglReg'] 			= $tanggal;
			$this->data['Hal'] 				= $this->input->post('Hal');
			$this->data['Approve_People'] 	= (($this->input->post('TtdText') == 'none') ? $this->session->userdata('peopleid') : $this->input->post('Approve_People'));
			$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;

			$this->load->view('backend/standart/administrator/pdf/view_nota_dinas_tindaklanjut_pdf', $this->data);

			$content = ob_get_contents();
			ob_end_clean();

			$config = array(
				'orientation' 	=> 'p',
				'format' 		=> 'f4',
				'marges' 		=> array(30, 30, 20, 20),
			);


			$this->pdf = new HtmlPdf($config);
			$this->pdf->initialize($config);
			$this->pdf->pdf->SetDisplayMode('fullpage');
			$this->pdf->writeHTML($content);
			$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
		}
		if ($this->input->post('pdf') == '2') {

			try {
				$naskah = $this->model_konsepnaskah->find($NId);

				$data_konsep = [
					'Jumlah'       		=> $this->input->post('Jumlah'),
					'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
					'SifatId'       	=> $this->input->post('SifatId'),
					'ClId'       		=> $this->input->post('ClId'),
					'RoleId_To'       	=> ($this->input->post('RoleId_To') == NULL ? $this->input->post('RoleId_To') : implode(',', $this->input->post('RoleId_To'))),
					'RoleId_Cc'       	=> ($this->input->post('RoleId_Cc') == NULL ? $this->input->post('RoleId_Cc') : implode(',', $this->input->post('RoleId_Cc'))),
					'Konten'       		=> $this->input->post('Konten'),
					'Approve_People'    => (($this->input->post('TtdText') == 'none') ? $this->session->userdata('peopleid') : $this->input->post('Approve_People')),
					'TtdText'       	=> $this->input->post('TtdText'),
					'Hal'       		=> $this->input->post('Hal'),
					'Nama_ttd_konsep'  	=> $xz,
					'lokasi'			=> $lok,
				];

				$this->db->where('NId_Temp', $NId);
				$this->db->update('konsep_naskah', $data_konsep);

				if (!empty($_POST['upload_naskahdinas_name'])) {

					foreach ($_POST['upload_naskahdinas_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['upload_naskahdinas_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_naskahdinas_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;
						$save_files = [
							'FileKey' 			=> tb_key(),
							'GIR_Id' 			=> $NId,
							'NId' 				=> $naskah->GIR_Id,
							'PeopleID' 			=> $this->session->userdata('peopleid'),
							'PeopleRoleID' 		=> $this->session->userdata('roleid'),
							'FileName_real' 	=> $file_name,
							'FileName_fake' 	=> $test_title_name_copy,
							'FileStatus' 		=> 'available',
							'EditedDate'        => $tanggal,
							'Keterangan' 		=> '',
						];

						$save_file = $this->model_inboxfile->store($save_files);
					}
				}

				set_message('Data Berhasil Diubah', 'success');


				//xdt 4 --> Daftar Semua Disposisi
				if ($this->input->post('xdt') == '4') {
					redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_dinas/' . $tipe . '/' . $naskah->GIR_Id . '?dt=' . $x));
				} else {
					redirect(BASE_URL('administrator/anri_list_notadinas_btl_ap'));
				}
			} catch (\Exception $e) {
				set_message('Gagal Merubah Data', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}
	}
	//Tutup Ubah Nota Dinas Tindaklanjut

	//Fungsi hapus file Ubah Nota Dinas Tindaklanjut
	public function hapus_file($NId, $name_file)
	{

		$files = glob('./FilesUploaded/naskah/' . $name_file);
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file);
		}

		$this->db->query("DELETE FROM inbox_files WHERE NId = '" . $NId . "' AND FileName_fake = '" . $name_file . "'");

		set_message('Data Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//Tutup hapus file Ubah Nota Dinas Tindaklanjut

	//Fungsi Hapus Nota Dinas Tindaklanjut
	public function hapus_naskah_dinas_tindaklanjut2($NId)
	{
		try {

			$x = $this->input->post('girid');

			$cari = $this->db->query("SELECT GIR_Id FROM inbox_receiver WHERE NId = '" . $this->input->post('girid') . "' ORDER BY ReceiveDate DESC LIMIT 0,1")->row()->GIR_Id;

			//UPDATE STATUS
			$this->db->where('NId', $x);
			$this->db->where('GIR_Id', $cari);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 0]);

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $this->input->post('temp') . "'");

			$files = FCPATH . 'FilesUploaded/naskah/';

			foreach ($query->result_array() as $row) :
				if (is_file($files . $row['FileName_fake']))
					unlink($files . $row['FileName_fake']);
			endforeach;

			$this->db->query("DELETE FROM `konsep_naskah` WHERE NId_Temp = '" . $this->input->post('temp') . "'");
			$this->db->query("DELETE FROM `inbox_files` WHERE NId = '" . $this->input->post('temp') . "'");

			$data = array('status' => 'success',);
			echo json_encode($data);
		} catch (\Exception $e) {
			$data = array('status' => 'error',);
			echo json_encode($data);
		}
	}
	//Tutup Hapus Nota Dinas Tindaklanjut

}
