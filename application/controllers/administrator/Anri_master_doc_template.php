<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_doc_template extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_doc_template');
	}

	// List doc template
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_doc_template/master_doc_template_list', $this->data);
	}
	// Tutup list doc template

	// Tambah data doc template
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_doc_template/master_doc_template_add', $this->data);
	}
	// Tutup tambah data doc template

	// Proses simpan data doc template
	public function add_save()
	{

		$tanggal = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('doc_desc', 'doc_desc', 'trim|required|max_length[250]');

		$config['upload_path'] = './FilesUploaded/TemplateDoc/';
		$config['allowed_types'] = 'doc|docx|pdf|rtf|jpg|jpeg|png';
		$config['max_size'] = '15000';
		$config['overwrite'] = TRUE;
		// $config['encrypt_name'] = TRUE;

		$this->load->library('upload', $config);
		
		$this->upload->do_upload('image_header');
		$file1 = $this->upload->data();
	    $file_name1 = $file1['file_name'];
	    
	    if ($this->form_validation->run()) {
			$table = 'master_doc_template';

			$query = $this->db->query("SELECT (max(convert(substr(doc_id, 12), UNSIGNED)) + 1) as id FROM master_doc_template")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

			if (!empty($file_name1)) {
				$save_data = [
					'doc_id' => tb_key().'.'.$id,
					'doc_desc' => $this->input->post('doc_desc'),
					'doc_file' => $file_name1,
					'upload_date' => $tanggal,
				];
			} else {
				$save_data = [
					'doc_id' => tb_key().'.'.$id,
					'doc_desc' => $this->input->post('GRoleId'),
					'upload_date' => $tanggal,
				];
			}

			
			$save_master_doc_template = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_doc_template'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_doc_template'));
		}

	}
	// Tutup proses simpan data doc template

	// Edit data doc template
	public function update($id)
	{
		$this->data['master_doc_template'] = $this->model_master_doc_template->find($id);
		$this->tempanri('backend/standart/administrator/master_doc_template/master_doc_template_update', $this->data);
	}
	// Tutup edit data doc template

	// Proses update data doc template
	public function update_save($id)
	{
		
		$tanggal = date('Y-m-d H:i:s');
		$this->form_validation->set_rules('doc_desc', 'doc_desc', 'trim|required|max_length[250]');
		
		$config['upload_path'] = './FilesUploaded/TemplateDoc/';
		$config['allowed_types'] = 'doc|docx|pdf|rtf|jpg|jpeg|png';
		$config['max_size'] = '15000';
		$config['overwrite'] = TRUE;
		// $config['encrypt_name'] = TRUE;
	
		$this->load->library('upload', $config);
		
		$this->upload->do_upload('image_header');
		$file1 = $this->upload->data();
	    $file_name1 = $file1['file_name'];
	    
	    if ($this->form_validation->run()) {
			$table = 'master_doc_template';
			if (!empty($file_name1)) {
				$save_data = [
					'doc_desc' => $this->input->post('doc_desc'),
					'doc_file' => $file_name1,
					'upload_date' => $tanggal,
				];
			} else {
				$save_data = [
					'doc_desc' => $this->input->post('doc_desc'),
					'upload_date' => $tanggal,
				];
			}

			
			$save_master_doc_template = $this->model_master_doc_template->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_doc_template'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_doc_template'));
		}

	}
	// Tutup proses update data doc template

	// Hapus data doc template
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$lokasi = FCPATH . 'FilesUploaded/TemplateDoc/';
		    $kop = $this->model_master_doc_template->find($id);
			$files = glob($lokasi.$kop->doc_file);
			foreach($files as $file){
				if(is_file($file))
					unlink($file);
			}
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}


		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_doc_template'));		
	}
	// Tutup hapus data doc template

	// Proses hapus data doc template
	private function remove($id)
	{
		$master_doc_template = $this->db->where('doc_id',$id)->delete('master_doc_template');
		return $master_doc_template;
	}
	// Tutup proses hapus data doc template

	// Buka Hapus file kopnaskah
	public function hapus_file4($NId,$name_file)
	{

		$this->load->helper('file');

		if (!empty($NId)) {
			$lokasi = FCPATH . 'FilesUploaded/TemplateDoc/';
		    $kop = $this->model_master_doc_template->find($NId);
			$files = glob($lokasi.$kop->doc_file);
			foreach($files as $file){
				if(is_file($file))
					unlink($file);
			}
		}
		
		$this->db->where('doc_id',$NId);
		$this->db->update('master_doc_template',['doc_file' => '']);


		set_message('Data Digital Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	// Tutup Hapus file kopnaskah
	
}