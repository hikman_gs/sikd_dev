<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_daftar_arsip_aktif extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_arsip_aktif');
	}

	//Buka Daftar arsip aktif
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/berkas/daftar_arsip_aktif', $this->data);
	}
	//Tutup daftar arsip aktif
					
	//Buka Ambil Data Seluruh arsip aktif
	public function get_data_arsip_aktif()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_arsip_aktif->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $field->Klasifikasi;
			$row[] = $field->BerkasName;

			$z = $this->db->query("SELECT NId FROM inbox WHERE BerkasId='".$field->BerkasId."'")->num_rows();
			if($z > 0) {
				$xx1 = $this->db->query("SELECT NId FROM inbox WHERE BerkasId='".$field->BerkasId."'")->result();
				foreach($xx1 as $xx2) {
					$xx = $xx2->NId;
				}				
			}

			if($z > 0) {
				$jumlah = $this->db->query("SELECT NId FROM v_jumlah_item WHERE NId = '".$xx."'")->num_rows();			
				$row[] = '<font color = "red"><b>'.$jumlah." Item".'</font></b>';	
			} else {
				$row[] = '<font color = "red"><b> 0 Item</font></b>';
			}

			$row[] = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId."'")->row()->RoleName;
			$row[] = date('d-m-Y',strtotime($field->RetensiValue_Active));

			if($z > 0) {
				$row[] = '<center><a href="'.site_url('administrator/anri_daftar_arsip_aktif/view_isi_arsip_aktif/view/'.$xx).'" title="Detail Isi Berkas" class="btn btn-danger btn-sm"><i class="fa fa-search"></i></a></center>';
			} else {
				$row[] = '';
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_arsip_aktif->count_all(),
			"recordsFiltered" => $this->model_list_arsip_aktif->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh arsip aktif

	//Buka View isi arsip aktif
	public function view_isi_arsip_aktif($tipe,$NId)
	{

		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		
		$this->tempanri('backend/standart/administrator/berkas/daftar_isi_berkas_aktif', $this->data);
	}
	//Tutup View isi arsip aktif
	

}
