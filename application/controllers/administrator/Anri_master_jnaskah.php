<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_jnaskah extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
				
		$this->load->model('model_master_jnaskah');
	}

	// List jenis naskah
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_jnaskah/master_jnaskah_list', $this->data);
	}
	// Tutup list jenis naskah

	// Tambah data jenis naskah
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_jnaskah/master_jnaskah_add', $this->data);
	}
	// Tutup tambah data jenis naskah

	// Proses simpan data jenis naskah
	public function add_save()
	{

		$this->form_validation->set_rules('JenisName', 'JenisName', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('Isi', 'Isi', 'trim|required');
		

		if ($this->form_validation->run()) {
			$table = 'master_jnaskah';

			$query = $this->db->query("SELECT (max(convert(substr(JenisId, 12), UNSIGNED)) + 1) as id FROM master_jnaskah")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}


			$save_data = [
				'JenisId' => tb_key().'.'.$id,
				'JenisName' => $this->input->post('JenisName'),
				'Isi' => $this->input->post('Isi'),
			];

			
			$save_master_bahasa = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_jnaskah'));
		} else {
			set_message('Gagal Menyimpan Data','error');
			redirect(BASE_URL('administrator/anri_master_jnaskah'));			
		}

	}
	// Tutup proses simpan data jenis naskah

	// Edit data jenis naskah
	public function update($id)
	{
		$this->data['master_jnaskah'] = $this->model_master_jnaskah->find($id);
		$this->tempanri('backend/standart/administrator/master_jnaskah/master_jnaskah_update', $this->data);
	}
	// Tutup edit data jenis naskah

	// Proses update data jenis naskah
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('JenisName', 'JenisName', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('Isi', 'Isi', 'trim|required');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'JenisName' => $this->input->post('JenisName'),
				'Isi' => $this->input->post('Isi'),
			];

			
			$save_master_jnaskah = $this->model_master_jnaskah->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_jnaskah'));
		} else {
			set_message('Gagal Menyimpan Data','error');
			redirect(BASE_URL('administrator/anri_master_jnaskah'));			
		}
	}
	// Tutup proses update data jenis naskah

	// Hapus data jenis naskah
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
		    set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }
		redirect(BASE_URL('administrator/anri_master_jnaskah'));
	}
	// Tutup hapus data jenis naskah

	// Proses hapus data jenis naskah
	private function remove($id)
	{
		$master_jnaskah = $this->db->where('JenisId',$id)->delete('master_jnaskah');
		return $master_jnaskah;
	}
	// Tutup proses hapus data jenis naskah
}