<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_naskah_belum_berkas extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_naskah_belum_berkas');
	}

	//Buka Daftar Naskah Belum Diberkaskan	
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Daftar Naskah Belum Diberkaskan';
		$this->tempanri('backend/standart/administrator/mail_tl/list_naskah_masuk_blmberkas', $this->data);
	}
	//Tutup Daftar Naskah Belum Diberkaskan	
				
	//Buka Ambil Data Seluruh Naskah Belum Diberkaskan
	public function get_data_naskah_belum_berkas()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_belum_berkas->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;

			if ($field->UrgensiName=='Amat Segera') {
				$xx = "<span class='badge bg-red'>".$field->UrgensiName."</span>";
			} elseif ($field->UrgensiName=='Segera') {
				$xx = "<span class='badge bg-red'>".$field->UrgensiName."</span>";
			} else {
				$xx = $field->UrgensiName;
			}
			$row[] = $xx;

			$row[] = $field->Nomor;
			$row[] = $field->Hal;
			// if(($field->NTipe == 'outbox') || ($field->NTipe == 'outboxmemo') || ($field->NTipe == 'outboxins')){
			// 	$row[] = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->Instansipengirim."'")->row()->RoleName;
			// } else {
			// 	$row[] = $field->Instansipengirim;
			// }

					$TES=$field->Instansipengirim;

					if (!$TES){

						$row[] = $field->Namapengirim;
					
					}else{

						$row[] = $TES;
					}

			$row[] = date('d-m-Y',strtotime($field->Tgl));
			                                    
   //          $query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."'");

   //          $files = BASE_URL.'/FilesUploaded/'.get_data_inbox('NFileDir',$field->NId).'/';


		 //  	$z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			//   	foreach ($query->result_array() as $row21) {
			//   		$z1 .= '<tr>';
			//     	$z1 .= '<td><center><a href="'.$files.$row21['FileName_fake'].'" target="_new" title="'.$row21['FileName_fake'].'" class="btn btn-danger btn-sm"><i class="fa fa-download"></i></a><br><br></center></td>';
			//     	$z1 .= '</tr>';
			// 	}
			// $z1 .= "</table>";	
			// $row[] = $z1;



			$files = $this->db->query("SELECT FileName_fake, FileName_real from inbox_files WHERE NId = '".$field->NId."' ");

			$fileupload = '<span class="text-red"><b>Belum upload file</b></span>';

			if($files->num_rows() != 0){
				$fileupload = "";
				$fileupload_masyarakat = "";
				$fileupload_pd = "";
				$btn1 = "hidden";
				$btn2 = "";
			
				foreach($files->result() as $filename){
					$path_file = BASE_URL('FilesUploaded/naskah/'.$filename->FileName_fake);
					$path_file_masyarakat = BASE_URL('doc/uploads/'.$filename->FileName_fake);
					$path_file_pd = BASE_URL('doc/uploads/pemerintah/'.$filename->FileName_fake); 

					$fileupload = '<a target="_blank" href='.$path_file.' class="btn btn-danger btn-sm"><i class="fa fa-download"></i></a> ';
					$fileupload_masyarakat = '<a target="_blank" href='.$path_file_masyarakat.' class="btn btn-danger btn-sm"><i class="fa fa-download"></i></a> ';
					$fileupload_pd = '<a target="_blank" href='.$path_file_pd.' class="btn btn-danger btn-sm"><i class="fa fa-download"></i></a> ';
				}
			}
				

			if($field->NFileDir == 'uploads/pemerintah'){
				$row[] = $fileupload_pd;
			} elseif ($field->NFileDir == 'uploads') {
				// code...
				$row[] = $fileupload_masyarakat;
			} else {
				$row[] = $fileupload;
			}

			
	        if($field->ReceiverAs == 'cc1')
	        {
	          $row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';
	        }
	        elseif($field->ReceiverAs == 'to_notadinas')
	        { 
	          $row[] = '<a target="_blank" href="'. site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
	        }
	        elseif($field->ReceiverAs == 'to_undangan')
	        { 
	          $row[] = '<a target="_blank" href="'. site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
	        }
	        elseif($field->ReceiverAs == 'to_sprint')
	        {
	          $row[] = '<a target="_blank" href="'. site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
	        }
	        elseif($field->ReceiverAs == 'to_keluar')
	        {
	          $row[] = '<a target="_blank" href="'. site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
	        }
	        elseif($field->ReceiverAs == 'to_nadin')
	        {
	          $row[] = '<a target="_blank" href="'. site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
	        } 
	        else
	        {
	        	$row[] = '';
	        } 
                 
			$row[] = '<center><a href="'.site_url('administrator/anri_naskah_belum_berkas/view_naskah_berkas/view/'.$field->NId).'" title="Proses Pemberkasan" class="btn btn-danger btn-sm"><i class="fa fa-folder-o"></i></a></center>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_belum_berkas->count_all(),
			"recordsFiltered" => $this->model_list_naskah_belum_berkas->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Belum Diberkaskan

	//Buka View Naskah Berkas
	public function view_naskah_berkas($tipe,$NId)
	{
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_berkas', $this->data);
	}
	//Tutup View Naskah Berkas

	//Buka Input Berkas
	public function update_naskah_berkas()
	{
		$nid = $this->input->post('NId');
		$berkasid = $this->input->post('BerkasId');
		
		$data = array(
			'BerkasId' => $berkasid, 
		);
		$this->db->where('NId', $nid);
		$this->db->update('inbox', $data);

		set_message('Data Berhasil Disimpan', 'success');
		redirect(BASE_URL('administrator/anri_naskah_belum_berkas'));
	}
	//Tutup Input Berkas
		
}