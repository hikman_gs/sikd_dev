<?php
defined('BASEPATH') or exit('No direct script access allowed');

// use NcJoes\OfficeConverter\OfficeConverter;
class Surat_keterangan extends Admin
{
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_konsepnaskah_koreksi');
		$this->load->model('model_list_keterangan');
		$this->load->model('model_list_log_surat_keterangan');
	}

	// Fungsi List Pengumuman
	public function index()
	{
		//cek akses ambil dari helper
		// check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->template->title('Naskah Template');

		$this->tempanri('backend/standart/administrator/master_list_naskah/menu_list_naskah', $this->data);
	}
	// Akhir Fungsi List Pengumuman
	// 
	public function detail_pdf($value = '')
	{
		$tanggal = date('Y-m-d H:i:s');
		$this->load->library('Pdf');

		$data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$data['Hal'] 			= $this->input->post('Hal');
		$data['TglReg'] 		= date('Y-m-d H:i:s');
		$data['RoleCode'] 	= $this->input->post('rolecode');
		$data['TtdText']    = $this->input->post('TtdText');
		//atas nama
		$data['TtdText2']   = $this->input->post('TtdText2');

		if ($data['TtdText'] == 'none') {
			$data['Approve_People']	= $this->session->userdata('peopleid');
			$data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$data['Approve_People'] 	= $this->input->post('tmpid_atasan');
			$data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$data['Approve_People'] 	= $this->input->post('Approve_People');
			$data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		//penambahan atas nama
		if ($data['TtdText2'] == 'AL') {
			$data['Approve_People3'] 	= $this->input->post('tmpid_atas_nama');
		} else {
			$data['Approve_People3'] 	= $this->input->post('Approve_People3');
		}

		//
		$data['ClCode'] 	= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;

		$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

		$this->db->select("*");
		$this->db->where_in('PeopleId', $data['RoleId_To']);
		$this->db->order_by('GroupId', 'ASC');
		$this->db->order_by('Golongan', 'DESC');
		$this->db->order_by('Eselon ', 'ASC');
		$data_ket = $this->db->get('v_login')->result();

		// var_dump($data_ket);
		// die();

		$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4','7') ")->row()->Pangkat;

		$pangkat_ttd = $this->db->query("SELECT Pangkat FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep']  . "' AND GroupId IN ('3','4','7') ")->row()->Pangkat;

		//tcpdf
		$age = $this->session->userdata('roleid');
		if ($data['TtdText'] == 'PLT') {
			$tte = 'Plt. ' . get_data_people('RoleName', $data['Approve_People']);
		} elseif ($data['TtdText'] == 'PLH') {
			$tte = 'Plh. ' . get_data_people('RoleName', $data['Approve_People']);
		} elseif ($data['TtdText2'] == 'Atas_Nama') {
			$tte = 'a.n ' . get_data_people('RoleName', $data['Approve_People3']) . '<br>' . get_data_people('RoleName', $data['Approve_People']);
		} else {
			$tte = get_data_people('RoleName', $data['Approve_People']);
		}
		$PEMERIKSA='PEMERIKSA';

		$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td width="240px" style="text-align:center;">Tempat, tanggal, bulan dan tahun</td>
			</tr>
			<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $tte . ',</td>				
			</tr>
				<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $PEMERIKSA . '</td>				
			</tr>
		</table>
		<br>
		<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td rowspan="7" width="60px" style="valign:bottom;">
				<br><br>
				<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
				<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
				<td><br></td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">' . $data['Nama_ttd_konsep'] . '
				</td>
			</tr>
			<tr nobr="true">
				<td>' . $pangkat_ttd . '
				</td>
			</tr>
		</table>';


		$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
		$pdf->SetMargins(30, 30, 20);
		$pdf->SetAutoPageBreak(TRUE, 25);
		// 
		$pdf->AddPage('P', 'F4');
		if ($this->session->userdata('roleid') == 'uk.1') {
			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			$pdf->Ln(10);
		} else {
			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
		}
		$pdf->Ln(5);
		$pdf->Cell(0, 10, 'SURAT KETERANGAN', 0, 0, 'C');
		$pdf->Ln(6);
		$pdf->Cell(0, 10, 'NOMOR : .........../' . $data['ClCode'] . '/' . $data['RoleCode'], 0, 0, 'C');
		$pdf->Ln(15);
		$pdf->Cell(70, 5, 'Yang bertandatangan di bawah ini : ', 0, 0, 'L');
		$pdf->Ln(7);
		$pdf->Cell(45, 5, 'a. Nama', 0, 0, 'L');
		$pdf->Cell(5, 5, ':', 0, 0, 'L');
		$pdf->Cell(108, 5, $data['Nama_ttd_konsep'], 0, 0, 'L');
		$pdf->Ln(6);
		$pdf->Cell(45, 5, 'b. Jabatan', 0, 0, 'L');
		$pdf->Cell(5, 5, ':', 0, 0, 'L');
		$pdf->MultiCell(108, 5, ucwords(strtolower(get_data_people('RoleName', $data['Approve_People']))) . "\n", 0, 'J', 0, 2, '', '', true);
		$pdf->Ln(7);
		$pdf->Cell(67, 5, 'dengan ini menerangkan bahwa : ', 0, 0, 'L');
		$pdf->Ln(7);
		$i = 0;
		foreach ($data_ket as $row) {
			$i++;
			$pdf->SetX(30);
			$pdf->Cell(45, 5, 'a. Nama/NIP.', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->MultiCell(108, 5, $row->PeopleName . ' / NIP.' . $row->NIP, 0, 'L');
			$pdf->Ln(2);
			$pdf->SetX(30);
			$pdf->Cell(45, 5, 'b. Pangkat/Golongan', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Cell(108, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
			$pdf->Ln(2);
			$pdf->SetX(30);
			$pdf->Cell(45, 5, 'c. Jabatan', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$uc  = ucwords(strtolower($row->PeoplePosition));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			// $pdf->MultiCell(85,5,$str,0,'J');
			$pdf->MultiCell(108, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			$pdf->Ln(2);
			$pdf->SetX(30);
			$pdf->Cell(45, 5, 'd. Maksud', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			// $pdf->MultiCell(108, 5, $data['Hal']."\n", 0, 'J', 0, 2, '' ,'', true);
			$pdf->writeHTMLCell(108, 0, '', '', $data['Hal'] . "\n", 0, 1, 0, false, 'J', false);
		}
		$pdf->Ln(7);
		$pdf->Cell(30, 5, 'Demikian Surat Keterangan ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
		$pdf->Ln(15);
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
		$pdf->Output('surat_keterangan_view.pdf', 'I');
	}

	public function create()
	{
		// check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();

		// data klasifikasi
		$data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
		// data yang ditugaskan
		$data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();
		// data pemeriksa
		$data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
		// data atasan
		$data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		// data tmpid_atasan
		$data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		// data a.n
		$data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
		// data nama a.n
		$data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		// data nama u.b
		$data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		$data['title'] = 'Pencatatan Surat Keterangan';
		$this->tempanri('backend/standart/administrator/master_list_naskah/surat_keterangan/create', $data);
	}


	// Fungsi Simpan Pengumuman
	public function store()
	{
		//cek akses ambil dari helper
		// check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->form_validation->set_rules('ClId', 'Kode Klasifikasi', 'trim|required');
		$this->form_validation->set_rules('rolecode', 'Unit Pengolah', 'trim|required');
		$this->form_validation->set_rules('Hal', 'Maksud', 'trim|required');
		$this->form_validation->set_rules('RoleId_To[]', 'menerangkan bahwa', 'trim|required');
		$this->form_validation->set_rules('TtdText', 'Pemeriksa', 'trim|required');
		$this->form_validation->set_rules('TtdText2', 'a.n', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
			// data klasifikasi
			$data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
			// data yang ditugaskan
			$data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();
			// data pemeriksa
			$data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
			// data atasan
			$data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
			// data tmpid_atasan
			$data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
			// data a.n
			$data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
			// data nama a.n
			$data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
			// data nama u.b
			$data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
			$data['title'] = 'Pencatatan Surat Keterangan';
			$this->tempanri('backend/standart/administrator/master_list_naskah/surat_keterangan/create', $data);
		} else {
			// $gir = $this->session->userdata('peopleid') . date('dmyhis');

		$gir_awal= $this->session->userdata('peopleid').date('dmyhis');	
		$jumlah_karakter    =strlen($gir_awal);		
		if ($jumlah_karakter == 17){

			$sub_gir = substr($gir_awal,0,16);
			$cek_NId_Temp="SELECT NId_Temp FROM konsep_naskah WHERE NId_Temp = '$_POST[$sub_gir]'";
			if ($cek_NId_Temp > 0) {

				$sub_gir = substr($gir_awal,0,16)+1;

			}else{

				$sub_gir = substr($gir_awal,0,16);

			}

		}else{

			$sub_gir = $this->session->userdata('peopleid').date('dmyhis');

		}

			$tanggal = date('Y-m-d H:i:s');
			$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

			if ($this->input->post('TtdText') == 'none') {
				$z = $this->session->userdata('approvelname');
				$x =  $this->session->userdata('peopleid');
			} elseif ($this->input->post('TtdText') == 'AL') {
				$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
				$x = $this->input->post('tmpid_atasan');
			} else {
				$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
				$x = $this->input->post('Approve_People');
			}

			if ($this->input->post('TtdText2') == 'none') {
				$b = ('');
				$b = ('');
			} elseif ($this->input->post('TtdText2') == 'AL') {
				$b = $this->input->post('tmpid_atas_nama');
			} else {
				$b = $this->input->post('Approve_People3');
			}


			try {

				$data_konsep = [
					// 'NId_Temp'   		=> $gir,
					// 'GIR_Id'       		=> $gir,

					'NId_Temp'   		=> $sub_gir,
					'GIR_Id'       		=> $sub_gir,

					'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Keterangan'")->row()->JenisId,
					'ReceiverAs'       	=> 'to_sket',
					'ClId'       		=> $this->input->post('ClId'),
					'Number'       		=> '0',
					'RoleCode'       	=> $this->input->post('rolecode'),
					'RoleId_From'      	=> $this->session->userdata('roleid'),
					'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
					'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
					'Konten'       		=> $this->input->post('Konten'),
					'Approve_People'    => $x,
					'Approve_People3'    => $b,
					'TtdText'       	=> $this->input->post('TtdText'),
					'TtdText2'       	=> $this->input->post('TtdText2'),
					'Konsep'       		=> '1',
					'TglReg'       		=> $tanggal,
					'Hal'       		=> $this->input->post('Hal'),
					'TglNaskah'       	=> $tanggal,
					'CreateBy'       	=> $this->session->userdata('peopleid'),
					'Ket'       		=> 'outboxsket',
					'NFileDir'			=> 'naskah',
					'Nama_ttd_konsep' 	=> $z,
					'Nama_ttd_atas_nama' 	=> $x,
					'lokasi'			=> $lok,
					'RoleId_From_Asal' 	=> $this->session->userdata('roleid')
				];


				// var_dump($data_konsep);
				// die();

				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;

						$save_files = [
							'FileKey' 			=> tb_key(),
						   'GIR_Id' 			=> $sub_gir,
							'NId' 				=> $data_konsep['NId_Temp'],
							'PeopleID' 			=> $this->session->userdata('peopleid'),
							'PeopleRoleID' 		=> $this->session->userdata('roleid'),
							'FileName_real' 	=> $file_name,
							'FileName_fake' 	=> $test_title_name_copy,
							'FileStatus' 		=> 'available',
							'EditedDate'        => $tanggal,
							'Keterangan' 		=> '',
						];

						$save_file = $this->model_inboxfile->store($save_files);
					}
				}

				$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

				$data_konsep['id_koreksi'] = $sub_gir;

				//menyimpan konsep awal
				$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep);
				// akhir menyimpan konsep awal

				//Simpan tujuan draft
				$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

				$save_recievers = [
					// 'NId' 			=> $gir,
					// 'NKey' 			=> tb_key(),
					// 'GIR_Id' 		=> $girid_ir,
					'NId' 			=> $sub_gir,
					'NKey' 			=> tb_key(),
					'GIR_Id' 		=> $sub_gir,
					'From_Id' 		=> $this->session->userdata('peopleid'),
					'RoleId_From' 	=> $this->session->userdata('roleid'),
					'To_Id' 		=> $x,
					'RoleId_To' 	=> get_data_people('RoleId', $x),
					'ReceiverAs' 	=> 'to_draft_sket',
					'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
					'StatusReceive' => 'unread',
					'ReceiveDate' 	=> $tanggal,
					'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
				];
				//memunculkan konsep awal
				$save_reciever = $this->model_inboxreciever->store($save_recievers);

				$save_recievers['id_koreksi'] = $sub_gir;

				//memunculkan konsep awal
				$save_reciever_awal = $this->model_inboxreciever_koreksi->store($save_recievers);
				//akhir memunculkan konsep awal

				set_message('Data Berhasil Disimpan', 'success');
				redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
			} catch (\Exception $e) {
				set_message('Gagal Menyimpan Data', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}
	}
	// Akhir Fungsi Simpan Klasifikasi
	// 
	// 
	public function edit($NId = NULL)
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		// data klasifikasi
		$data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
		// data yang ditugaskan
		$data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();
		// data pemeriksa
		$data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
		// data atasan
		$data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		// data tmpid_atasan
		$data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		// data a.n
		$data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
		// data nama a.n
		$data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		// data nama u.b
		$data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		$data['title'] = 'Ubah Data Pencatatan Surat Keterangan';
		$data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/master_list_naskah/surat_keterangan/edit', $data);
	}

	// Buka edit data klasifikasi
	public function update($NId = NULL)
	{
		//cek akses ambil dari helper
		$this->form_validation->set_rules('ClId', 'Kode Klasifikasi', 'trim|required');
		$this->form_validation->set_rules('rolecode', 'Unit Pengolah', 'trim|required');
		$this->form_validation->set_rules('Hal', 'Maksud', 'trim|required');
		$this->form_validation->set_rules('RoleId_To[]', 'menerangkan bahwa', 'trim|required');
		$this->form_validation->set_rules('TtdText', 'Pemeriksa', 'trim|required');
		$this->form_validation->set_rules('TtdText2', 'a.n', 'trim|required');

		// var_dump($this->input->post());
		// die();

		if ($this->form_validation->run() == FALSE) {
			$data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
			// data klasifikasi
			$data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
			// data yang ditugaskan
			$data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();
			// data pemeriksa
			$data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
			// data atasan
			$data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
			// data tmpid_atasan
			$data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
			// data a.n
			$data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
			// data nama a.n
			$data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
			// data nama u.b
			$data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
			$data['title'] = 'Pencatatan Surat Keterangan';
			$data['data'] = $this->model_konsepnaskah->find($NId);
			$this->tempanri('backend/standart/administrator/master_list_naskah/surat_keterangan/edit', $data);
		} else {
			$gir = $this->session->userdata('peopleid') . date('dmyhis');
			$tanggal = date('Y-m-d H:i:s');
			$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

			if ($this->input->post('TtdText') == 'none') {
				$z = $this->session->userdata('approvelname');
				$x =  $this->session->userdata('peopleid');
			} elseif ($this->input->post('TtdText') == 'AL') {
				$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
				$x = $this->input->post('tmpid_atasan');
			} else {
				$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
				$x = $this->input->post('Approve_People');
			}

			if ($this->input->post('TtdText2') == 'none') {
				$b = ('');
				$b = ('');
			} elseif ($this->input->post('TtdText2') == 'AL') {
				$b = $this->input->post('tmpid_atas_nama');
			} else {
				$b = $this->input->post('Approve_People3');
			}


			try {

				$data_konsep = [
					'NId_Temp'   		=> $gir,
					'GIR_Id'       		=> $gir,
					'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Keterangan'")->row()->JenisId,
					'ReceiverAs'       	=> 'to_draft_sket',
					'ClId'       		=> $this->input->post('ClId'),
					'Number'       		=> '0',
					'RoleCode'       	=> $this->input->post('rolecode'),
					'RoleId_From'      	=> $this->session->userdata('roleid'),
					'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
					'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
					'Konten'       		=> $this->input->post('Konten'),
					'Approve_People'    => $x,
					'Approve_People3'    => $b,
					'TtdText'       	=> $this->input->post('TtdText'),
					'TtdText2'       	=> $this->input->post('TtdText2'),
					'Konsep'       		=> '1',
					'TglReg'       		=> $tanggal,
					'Hal'       		=> $this->input->post('Hal'),
					'TglNaskah'       	=> $tanggal,
					'CreateBy'       	=> $this->session->userdata('peopleid'),
					'Ket'       		=> 'outboxsket',
					'NFileDir'			=> 'naskah',
					'Nama_ttd_konsep' 	=> $z,
					'Nama_ttd_atas_nama' 	=> $x,
					'lokasi'			=> $lok,
					'RoleId_From_Asal' 	=> $this->session->userdata('roleid')
				];


				// var_dump($data_konsep);
				// die();

				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;

						$save_files = [
							'FileKey' 			=> tb_key(),
							'GIR_Id' 			=> $gir,
							'NId' 				=> $data_konsep['NId_Temp'],
							'PeopleID' 			=> $this->session->userdata('peopleid'),
							'PeopleRoleID' 		=> $this->session->userdata('roleid'),
							'FileName_real' 	=> $file_name,
							'FileName_fake' 	=> $test_title_name_copy,
							'FileStatus' 		=> 'available',
							'EditedDate'        => $tanggal,
							'Keterangan' 		=> '',
						];

						$save_file = $this->model_inboxfile->store($save_files);
					}
				}
				$this->db->where('NId_Temp', $NId);
				$this->db->update('konsep_naskah', $data_konsep);

				$data_konsep['id_koreksi'] = $gir;

				//menyimpan konsep awal
				$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep);
				// akhir menyimpan konsep awal

				//Simpan tujuan draft
				$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

				$save_recievers = [
					'NId' 			=> $gir,
					'NKey' 			=> tb_key(),
					'GIR_Id' 		=> $girid_ir,
					'From_Id' 		=> $this->session->userdata('peopleid'),
					'RoleId_From' 	=> $this->session->userdata('roleid'),
					'To_Id' 		=> $x,
					'RoleId_To' 	=> get_data_people('RoleId', $x),
					'ReceiverAs' 	=> 'to_draft_sket',
					'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
					'StatusReceive' => 'unread',
					'ReceiveDate' 	=> $tanggal,
					'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
				];
				//memunculkan konsep awal

				$save_recievers['id_koreksi'] = $gir;

				//memunculkan konsep awal
				$save_reciever_awal = $this->model_inboxreciever_koreksi->store($save_recievers);
				//akhir memunculkan konsep awal

				set_message('Data Berhasil Diubah', 'success');
				redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
			} catch (\Exception $e) {
				set_message('Gagal Menyimpan Data', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}
	}
	// Tutup edit data klasifikasi

	// Buka hapus klasifikasi
	public function delete($id = null)
	{

		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		// check exist id
		$exist = $this->db->query("SELECT * FROM master_notice WHERE notice_id = " . $id)->num_rows();
		// exist
		if ($exist > 0) {
			$delete = $this->db->delete('master_notice', array('notice_id' => $id));

			if ($delete) {
				set_message('Data Berhasil Dihapus', 'success');
			} else {
				set_message('Data Gagal Dihapus', 'error');
			}
		} else {

			set_message('id not found', 'error');
		}

		redirect(BASE_URL('administrator/notice'));
	}
	// Tutup hapus klasifikasi
	// 
	// 
	function get_ajax()
	{

		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		// $data = $this->db->query("SELECT * FROM v_tem_sket")->result();
		// var_dump($data);
		// die();

		// die();
		$list = $this->model_list_keterangan->get_datatables();
		// echo "string";
		// var_dump($list);
		// die();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			if ($field->StatusReceive == 'unread') {

				$x  = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '" . $field->NId . "'")->num_rows();
				if ($x > 0) {
					$xx1 = "<b>" . $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp='" . $field->NId . "'")->row()->nosurat . "</b>";
				} else {
					$xx1 = '';
				}

				$xx3 = "<b>" . date('d-m-Y', strtotime($field->Tgl)) . "</b>";

				if ($field->ReceiverAs == 'bcc') {
					$xx4 = "<b>" . $this->db->query("SELECT RoleName FROM role WHERE RoleId='" . $field->RoleId_From . "'")->row()->RoleName . "<font color=red> (Tembusan) </font></b>";
				} else {
					$xx4 = "<b>" . $this->db->query("SELECT RoleName FROM role WHERE RoleId='" . $field->RoleId_From . "'")->row()->RoleName . "</b>";
				}

				$x1  = $this->db->query("SELECT Hal FROM konsep_naskah WHERE NId_Temp = '" . $field->NId . "'")->num_rows();
				if ($x1 > 0) {
					$xx5 = "<b>" . $this->db->query("SELECT Hal FROM konsep_naskah WHERE NId_Temp='" . $field->NId . "'")->row()->Hal . "</b>";
				} else {
					$xx5 = '';
				}
			} else {

				$x  = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '" . $field->NId . "'")->num_rows();
				if ($x > 0) {
					$xx1 = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp='" . $field->NId . "'")->row()->nosurat;
				} else {
					$xx1 = '';
				}

				$xx3 = date('d-m-Y', strtotime($field->Tgl));

				if ($field->ReceiverAs == 'bcc') {

					$xx4 = $this->db->query("SELECT RoleName FROM role WHERE RoleId='" . $field->RoleId_From . "'")->row()->RoleName . "<font color=red><b> (Tembusan) </b></font>";
				} else {
					$xx4 = $this->db->query("SELECT RoleName FROM role WHERE RoleId='" . $field->RoleId_From . "'")->row()->RoleName;
				}

				$x1  = $this->db->query("SELECT Hal FROM konsep_naskah WHERE NId_Temp = '" . $field->NId . "'")->num_rows();
				if ($x1 > 0) {
					$xx5 = $this->db->query("SELECT Hal FROM konsep_naskah WHERE NId_Temp='" . $field->NId . "'")->row()->Hal;
				} else {
					$xx5 = '';
				}
			}

			$row[] = $xx1;
			$row[] = $xx3;
			$row[] = $xx4;
			$row[] = $xx5;


			$xyz  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_Temp = '" . $field->GIR_Id . "'")->row();

			if ($xyz->Konsep == 1) {

				$row[] = '<a target="_blank" href="' . site_url('administrator/anri_mail_tl/iframe_jejak_histori/' . $field->GIR_Id) . '" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/' . $field->GIR_Id) . '" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
			} else {

				$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '" . $field->GIR_Id . "' AND Keterangan = 'outbox'")->num_rows();

				if ($x > 0) {

					$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '" . $field->NId_Temp . "' AND Keterangan = 'outbox'")->row();


					$row[] = '<a target="_blank" href="' . site_url('administrator/anri_mail_tl/iframe_jejak_histori/' . $field->GIR_Id) . '" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . BASE_URL('FilesUploaded/naskah/' . $xyz->FileName_real) . '" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_final_histori_koreksi/view/' . $field->GIR_Id . '?dt=3') . '" title="Lihat Jejak Draf" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';
				} else {


					$row[] = '<a target="_blank" href="' . site_url('administrator/anri_mail_tl/iframe_jejak_histori/' . $field->GIR_Id) . '" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/' . $field->GIR_Id) . '" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
				}
			}

			$data[] = $row;
		}
		$output = array(
			"draw" => @$_POST['draw'],
			"recordsTotal" => $this->model_list_keterangan->count_all(),
			"recordsFiltered" => $this->model_list_keterangan->count_filtered(),
			"data" => $data,
		);
		// output to json format
		echo json_encode($output);
	}


	public function list_tbl()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['title'] = 'Kotak Masuk Surat Keterangan';
		$this->tempanri('backend/standart/administrator/master_list_naskah/surat_keterangan/list_tbl', $this->data);
	}

	public function list_log()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['title'] = 'Data Pencatatan Surat Keterangan';
		$this->tempanri('backend/standart/administrator/master_list_naskah/surat_keterangan/log', $this->data);
	}

	public function get_log_surat_keterangan()
	{


		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_log_surat_keterangan->get_datatables($limit, $no);

		// var_dump($list);
		// die();

		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();

			$row[] = $no;
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $field->JenisId . "'")->row()->JenisName;
			$row[] = $field->nosurat;
			$row[] = date('d-m-Y', strtotime($field->TglNaskah));
			$row[] = $field->Hal;

			$xx2  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_temp = '" . $field->GIR_Id . "'")->row();

			if ($xx2->Konsep == 5) {
				$row[] = "<font color = 'blue'><b>Naskah Telah Dibatalkan Untuk Dikirim Oleh Penandatangan</b></font>";
			} elseif ($xx2->Konsep == 2) {

				$count_tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '" . $field->NId_Temp . "'")->num_rows();

				if ($count_tujuan > 0) {

					$tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '" . $field->NId_Temp . "'")->result();
					foreach ($tujuan as $key => $value) {
						$exp = explode(',', $value->RoleId_To);
						if ($value->RoleId_To == '-') {
							$z1 = '';
						} else {
							$z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
							foreach ($exp as $k => $v) {
								$z1 .= "<tr>";
								$z1 .= "<td> - " . $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName . "</td>";
								$z1 .= "</tr>";
							}
							$z1 .= "</table>";
						}
					}
				}

				$count_tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '" . $field->NId_Temp . "'")->num_rows();

				if ($count_tembusan > 0) {

					$tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '" . $field->NId_Temp . "'")->result();
					foreach ($tembusan as $key => $value) {
						$exp = explode(',', $value->RoleId_Cc);
						if (($value->RoleId_Cc == '') || ($value->RoleId_Cc == '-')) {
							$z2 = '';
						} else {
							$z2 = "<br> <b>Tembusan :</b> <table border ='0' cellpadding='0' cellspacing='0'>";
							foreach ($exp as $k => $v) {
								$z2 .= "<tr>";
								$z2 .= "<td> - " . $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName . "</td>";
								$z2 .= "</tr>";
							}
							$z2 .= "</table>";
						}
					}
				}

				$row[] = $z1 . $z2;
			} elseif ($xx2->Konsep == 1) {
				$row[] = "<font color = 'red'><b>Naskah Belum Disetujui Oleh Penandatangan</b></font>";
			} elseif ($xx2->Konsep == 0) {
				$row[] = "<font color = 'brown'><b>Naskah Sudah Disetujui dan Belum Dikirim Oleh Penandatangan</b></font>";
			} else {
				$row[] = "<font color = 'red'><b>Naskah Belum Dikirim Oleh Penandatangan</b></font>";
			}


			$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $field->NId_Temp . "' AND GIR_Id = '" . $field->GIR_Id . "' AND Keterangan != 'outbox'")->result();

			$files = BASE_URL . '/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {
				$z21 .= "<tr>";
				$z21 .= "<td><a href='" . $files . $value1->FileName_fake . "' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
				$z21 .= "</tr>";
			}
			$z21 .= "</table>";

			$row[] = $z21;

			$xyz  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_Temp = '" . $field->GIR_Id . "'")->row();

			if ($xyz->Konsep == 1) {
				// $row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
				$row[] = '<a target="_blank" href="' . site_url('administrator/anri_mail_tl/iframe_jejak_histori/' . $field->GIR_Id) . '" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/' . $field->GIR_Id) . '" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
			} else {

				$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '" . $field->GIR_Id . "' AND Keterangan = 'outbox'")->num_rows();

				if ($x > 0) {

					$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '" . $field->NId_Temp . "' AND Keterangan = 'outbox'")->row();

					// $row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
					$row[] = '<a target="_blank" href="' . site_url('administrator/anri_mail_tl/iframe_jejak_histori/' . $field->GIR_Id) . '" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . BASE_URL('FilesUploaded/naskah/' . $xyz->FileName_real) . '" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_final_histori_koreksi/view/' . $field->GIR_Id . '?dt=3') . '" title="Lihat Jejak Draf" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';
				} else {

					// $row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
					$row[] = '<a target="_blank" href="' . site_url('administrator/anri_mail_tl/iframe_jejak_histori/' . $field->GIR_Id) . '" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/' . $field->GIR_Id) . '" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
				}
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_log_surat_keterangan->count_all(),
			"recordsFiltered" => $this->model_list_log_surat_keterangan->count_filtered(),
			"data" => $data,
		);

		echo json_encode($output);
	}





	public function test_word()
	{

		$this->load->library('Phpword');


		// $this->template->title('Tambah Pengumuman');

		// $this->tempanri('backend/standart/administrator/master_notice/master_notice_create');

		// $phpWord = new \PhpOffice\PhpWord\PhpWord();
		$template = new \PhpOffice\PhpWord\TemplateProcessor('uploads/test123.docx');
		$template->setValue('test', 'uhuy');
		$filename = 'test123ubah.docx';
		$template->save('uploads/' . $filename);
		/* Note: any element you append to a document must reside inside of a Section. */

		// Adding an empty Section to the document...
		// $section = $phpWord->addSection();
		// Adding Text element to the Section having font styled by default...
		// $section->addText(
		// 	'"Learn from yesterday, live for today, hope for tomorrow. '
		// 	. 'The important thing is not to stop questioning." '
		// 	. '(Albert Einstein)'
		// );

		/*
		 * Note: it's possible to customize font style of the Text element you add in three ways:
		 * - inline;
		 * - using named font style (new font style object will be implicitly created);
		 * - using explicitly created font style object.
		 */

		// Adding Text element with font customized inline...
		// $section->addText(
		// 	'"Hikman Ganda Sasmita, '
		// 	. 'and is never the result of selfishness." '
		// 	. '(Napoleon Hill)',
		// 	array('name' => 'Tahoma', 'size' => 10)
		// );

		// Adding Text element with font customized using named font style...
		// $fontStyleName = 'oneUserDefinedStyle';
		// $phpWord->addFontStyle(
		// 	$fontStyleName,
		// 	array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
		// );
		// $section->addText(
		// 	'"The greatest accomplishment is not in never falling, '
		// 	. 'but in rising again after you fall." '
		// 	. '(Vince Lombardi)',
		// 	$fontStyleName
		// );

		// Adding Text element with font customized using explicitly created font style object...
		// $fontStyle = new \PhpOffice\PhpWord\Style\Font();
		// $fontStyle->setBold(true);
		// $fontStyle->setName('Tahoma');
		// $fontStyle->setSize(13);
		// $myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Hikman Ganda Sasmita)');
		// $myTextElement->setFontStyle($fontStyle);

		// Saving the document as OOXML file...
		// $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		// $objWriter->save('helloWorld.docx');

		// $objWriter->save('uploads/'.$filename);
		// send results to browser to download
		// header('Content-Description: File Transfer');
		// header('Content-Type: application/octet-stream');
		// header('Content-Disposition: attachment; filename='.$filename);
		// header('Content-Transfer-Encoding: binary');
		// header('Expires: 0');
		// header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		// header('Pragma: public');
		// header('Content-Length: ' . filesize($filename));
		// flush();
		// readfile('uploads/'.$filename);
		// unlink('uploads/'.$filename); // deletes the temporary file
		exit;
	}
}
