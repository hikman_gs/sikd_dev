<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_reg_naskah_ttl extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_inbox');
		$this->load->model('model_inboxfile');	
	}

	// List pengguna
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		//model_custome di running di autoload -> application/config/autoload.php ->autoload model
		$this->data['people'] = $this->model_custome->get_people();
		$this->tempanri('backend/standart/administrator/reg_naskah_masuk/inbox_add_ttl', $this->data);
	}
	//Tutup list pengguna

	// Simpan naskah masuk ttl
	public function naskah_masuk_add_save()
	{	
		$nid = $this->session->userdata('peopleid').date('dmyhis');
		$gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try{
			$this->load->helper('string'); 
			$save_data = [
				'NKey'         		=> tb_key(),
				'NId'				=> $nid,
				'CreatedBy'			=> $this->session->userdata('peopleid'),
				'CreationRoleId'	=> $this->session->userdata('roleid'),
				'NTglReg' 			=> $tanggal,
				'Tgl' 				=> date('Y-m-d',strtotime($this->input->post('Tgl'))),
				'JenisId' 			=> $this->input->post('JenisId'),
				'Nomor' 			=> $this->input->post('Nomor'),
				'Hal' 				=> $this->input->post('Hal'),
				'Instansipengirim'	=> $this->input->post('Instansipengirim'),
				'Pengirim'			=> 'eksternal',
				'UrgensiId' 		=> $this->input->post('UrgensiId'),
				'SifatId' 			=> $this->input->post('SifatId'),
				'NTipe' 			=> 'inboxuk',
				'NFileDir'			=> 'naskah',
				'BerkasId'			=> $this->input->post('BerkasId'),
			];    

			$save_inbox = $this->model_inbox->store($save_data);
			if (count((array) $this->input->post('test_title_name'))) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
			            'GIR_Id' 			=> $gir,
			            'NId' 				=> $nid,
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);

				}
			}

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_log_naskah_ttl'));
		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_reg_naskah_ttl'));
		}
	}
	// Tutup Simpan naskah masuk ttl

	// Upload naskah masuk ttl
	public function upload_naskah_masuk_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'inbox_files',
		]);
	}
	// Tutup upload naskah masuk ttl

	// Hapus file naskah masuk ttl
	public function delete_naskah_masuk_file($uuid)
	{

		$dirname = BASE_URL.'/uploads/tmp/'.$uuid;	

		if(rmdir($dirname))
        {
          echo ("$dirname successfully removed");
        }
        else
        {
          echo ("$dirname couldn't be removed"); 
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => $this->input->get('previewLink'), 
            'field_name'        => 'title', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'test',
            'primary_key'       => 'id_test',
            'upload_path'       => 'uploads/test/'
        ]);
	}
	// Tutup Hapus file naskah masuk ttl
	
}