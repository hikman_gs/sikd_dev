<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_naskah_sudah_kirim extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_naskah_kirim');	
	}

	//NASKAH MASUK SUDAH kirim
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Naskah Sudah Dikirim';
		$this->tempanri('backend/standart/administrator/mail_tl/naskah_sudah_kirim', $this->data);
	}
	//Tutup NASKAH MASUK SUDAH APPROVE
	
	//Ambil Data Seluruh Naskah Sudah Dikirim
	public function get_data_naskah_kirim()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_kirim->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$field->JenisId."'")->row()->JenisName;	
			$row[] = $field->nosurat;		
			$row[] = date('d-m-Y',strtotime($field->TglNaskah));
			$row[] = $field->Hal;

			$xxz  = $this->db->query("SELECT ReceiverAs, RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->row();
			
			if($xxz->ReceiverAs == 'to_keluar') {

				$z1 = $xxz->RoleId_To;
				$z2 = '';

			} else {

				$tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
				foreach ($tujuan as $key => $value) {					  
				  $exp = explode(',', $value->RoleId_To);
				  if($value->RoleId_To == '-'){
				  	$z1 = '';
				  } else {
				  	  $z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
					  	foreach ($exp as $k => $v) {
					  		$z1 .= "<tr>";
					    	$z1 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
					    	$z1 .= "</tr>";
					  	}
					  $z1 .= "</table>";	
				  }			  
				}

				$tembusan  = $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '".$field->GIR_Id."' AND GIR_Id = '".$field->NId_Temp."' AND ReceiverAs = 'bcc' ORDER BY RoleId_To ASC")->result();

				$x  = $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '".$field->GIR_Id."'AND GIR_Id = '".$field->NId_Temp."' AND ReceiverAs = 'bcc'")->num_rows();

				if ($x == '0') {
					$z2 = '';
				} else {
					$z2 = "<br> <b>Tembusan :</b> <table border ='0' cellpadding='0' cellspacing='0'>";
					foreach ($tembusan as $key => $value) {					  
						$z2 .= "<tr>";
					   	$z2 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$value->RoleId_To."'")->row()->RoleName."</td>";
					   	$z2 .= "</tr>";
					}
					$z2 .= "</table>";						
				}
			}

			$row[] = $z1.$z2;


          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	

			$row[] = $z21;

			$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->num_rows();

			if($x > 0) {

				$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->row();

				$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';

			} else {

				$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->NId_Temp).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';			
			
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_kirim->count_all(),
			"recordsFiltered" => $this->model_list_naskah_kirim->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Sudah Dikirim
	
}