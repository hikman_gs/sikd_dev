<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 *| --------------------------------------------------------------------------
 *| Anri Controller
 *| --------------------------------------------------------------------------
 *| For authentication
 *|
 */
class Anri extends Admin
{

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Login user
	 *
	 */
	public function index()
	{
		redirect(base_url());
	}

	public function anri_check()
	{

		//captcha checking
		if ($this->input->post('captcha') != $this->input->post('captchaword')) {

			$this->session->set_flashdata('error', 'Kode Security Captcha Salah !');
			redirect(base_url());
		} else {

			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$where = array(
				'PeopleUsername' => $username,
				'PeoplePassword' => sha1($password),
				'PeopleIsActive' => '1',
			);

			$cek = $this->db->get_where("people", $where)->num_rows();
			if ($cek > 0) {

				$datax = $this->db->query("SELECT * From v_login WHERE PeopleUsername='" . $username . "'")->result();

				foreach ($datax as $row) {
					//create session pople id
					$data_session = array(
						'peopleid' => $row->PeopleId,
						'groupid' => $row->GroupId,
						'groupname' => $row->GroupName,
						'peopleusername' => $row->PeopleUsername,
						'peoplename' => $row->PeopleName,
						'peopleposition' => $row->PeoplePosition,
						'roleid' => $row->RoleId,
						'namabagian' => $row->RoleDesc,
						'gjabatanid' => $row->gjabatanId,
						'roleatasan' => $row->RoleAtasan,
						'rolecode' => $row->RoleCode,
						'nik' => $row->NIK,
						'groleid' => $row->GRoleId,
						'approvelname' => $row->ApprovelName,
						'primaryroleid' => $row->PrimaryRoleId,
						'status' => "anri_ok_dong"

					);

					$this->session->set_userdata($data_session);
					redirect(base_url('administrator/anri_dashboard'));
				}
			} else {
				$this->session->set_flashdata('error', 'Username dan password salah !');
				redirect(base_url());
			}
		}
	}

	public function anri_logout()
	{
		$this->session->sess_destroy();
		// redirect(base_url(''));
		redirect('');
	}
}
