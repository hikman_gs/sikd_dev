<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Notice extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('Model_notice');
		
	}

	// Fungsi List Pengumuman
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
				
		$this->template->title('Pengumuman');

		$this->tempanri('backend/standart/administrator/master_notice/master_notice_list', $this->data);
	}
	// Akhir Fungsi List Pengumuman

	public function create()
	{
        

				
		$this->template->title('Tambah Pengumuman');

		$this->tempanri('backend/standart/administrator/master_notice/master_notice_create');
	}	

	// Fungsi Simpan Pengumuman
	public function store()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->form_validation->set_rules('priority', 'priority', 'trim|required');
		$this->form_validation->set_rules('date_range', 'date_range', 'trim|required');
	    $this->form_validation->set_rules('notice_name', 'notice_name', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('notice_description', 'notice_description', 'trim');
		
		if ($this->form_validation->run() == FALSE) {
				$this->tempanri('backend/standart/administrator/master_notice/master_notice_create');
			}else{

				if (!empty($date_range = $this->input->post('date_range'))) {
					$pecah = explode('-', $date_range);
					$tgl_awal = date("Y-m-d", strtotime($pecah[0]));
					$tgl_akhir = date("Y-m-d", strtotime($pecah[1]));
				}

				$data = [
					'user_id' => $this->session->userdata('peopleid'),
					'user_name' => $this->session->userdata('peopleusername'),
					'notice_name' => $this->input->post('notice_name'),
					'notice_description' => $this->input->post('notice_description'),
					'priority' => $this->input->post('priority'),
					'tgl_awal' => $tgl_awal,
					'tgl_akhir' => $tgl_akhir,
				];
				
				$save_notice = $this->db->insert('master_notice',$data);

				if ($save_notice ) {
					set_message('Data Berhasil Disimpan','success');
					redirect(BASE_URL('administrator/notice'));
				}else{
					set_message('Gagal Menyimpan Data', 'error');
					redirect(BASE_URL('administrator/notice'));
				}
				

			}
	}
	// Akhir Fungsi Simpan Klasifikasi
	// 
	// 
	public function edit($id)
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
				
		$this->template->title('Edit Pengumuman');
		$data['notice'] = $this->db->get_where('master_notice', ['notice_id' => $id])->row_array();

		// die();
		if (!empty($data['notice']['tgl_awal'])) {
			$data['notice']['tgl_awal'] = date("m/d/Y", strtotime($data['notice']['tgl_awal']));
		}else{
			$data['notice']['tgl_awal'] = "00/00/0000";
		}

		if (!empty($data['notice']['tgl_akhir'])) {
			$data['notice']['tgl_akhir'] = date("m/d/Y", strtotime($data['notice']['tgl_akhir']));
		}else{
			$data['notice']['tgl_akhir'] = "00/00/0000";
		}

		$this->tempanri('backend/standart/administrator/master_notice/master_notice_edit', $data);
	}	
	
	// Buka edit data klasifikasi
	public function update()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$id = $_POST['id'];

		$this->form_validation->set_rules('priority', 'priority', 'trim|required');
		$this->form_validation->set_rules('date_range', 'date_range', 'trim|required');
	    $this->form_validation->set_rules('notice_name', 'notice_name', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('notice_description', 'notice_description', 'trim');
		
		if ($this->form_validation->run() == FALSE) {
				$this->tempanri('backend/standart/administrator/master_notice/master_notice_edit');
			}
			else
			{
				if (!empty($date_range = $this->input->post('date_range'))) {
					$pecah = explode('-', $date_range);
					$tgl_awal = date("Y-m-d", strtotime($pecah[0]));
					$tgl_akhir = date("Y-m-d", strtotime($pecah[1]));
				}

				$data = [
					'user_id' => $this->session->userdata('peopleid'),
					'user_name' => $this->session->userdata('peopleusername'),
					'notice_name' => $this->input->post('notice_name'),
					'notice_description' => $this->input->post('notice_description'),
					'priority' => $this->input->post('priority'),
					'tgl_awal' => $tgl_awal,
					'tgl_akhir' => $tgl_akhir
				];

				// cek exist
				$exist = $this->db->query("SELECT * FROM master_notice WHERE notice_id = ".$id)->num_rows();
				
				if ($exist > 0) {
					$this->db->where('notice_id', $id);
					$update_notice = $this->db->update('master_notice', $data);

					if ($update_notice ) {
						set_message('Data Berhasil diubah','success');
						redirect(BASE_URL('administrator/notice'));
					}else{
						set_message('Gagal Merubah Data', 'error');
						redirect(BASE_URL('administrator/notice'));
					}
				}else{
					set_message('Gagal ID tidak tersedia', 'error');
					redirect(BASE_URL('administrator/notice'));
				}

				
			}

	}
	// Tutup edit data klasifikasi

	// Buka hapus klasifikasi
	public function delete($id=null)
	{

		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		// check exist id
		$exist = $this->db->query("SELECT * FROM master_notice WHERE notice_id = ".$id)->num_rows();
		// exist
		if($exist > 0) {
			$delete = $this->db->delete('master_notice', array('notice_id' => $id));

			if ($delete) {
				set_message('Data Berhasil Dihapus','success');
			}else{
				set_message('Data Gagal Dihapus','error');
			}
			
		}else{

			set_message('id not found','error');

		}

		redirect(BASE_URL('administrator/notice'));
	}
	// Tutup hapus klasifikasi
	// 
	// 
	function get_ajax() {
		
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

        $list = $this->Model_notice->get_datatables();
        $data = array();
        $no = @$_POST['start'];
        foreach ($list as $notice) {
            $no++;
            $row = array();
            $row[] = $no.".";
            $row[] = $notice->notice_name;
			switch ($notice->priority) {
			  case 1:
			    $row[] = '<span class="badge bg-green">New Feature</span>';
			    break;
			  case 2:
			    $row[] = '<span class="badge bg-yellow">Maintenance</span>';
			    break;
			  case 3:
			    $row[] = '<span class="badge bg-blue">Update</span>';
			    break;
			  default:
			    $row[] = 'none';
			}
            $row[] = $notice->notice_description;
            $row[] = $notice->tgl_awal;
            $row[] = $notice->tgl_akhir;
            
            // add html for action
            $row[] = '<a href="'. BASE_URL('administrator/notice/edit/'.$notice->notice_id).'" title="Hapus Data" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;'.'<a href="'. BASE_URL('administrator/notice/delete/'.$notice->notice_id).'" title="Hapus Data" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';

            $data[] = $row;
        }
        $output = array(
                    "draw" => @$_POST['draw'],
                    "recordsTotal" => $this->Model_notice->count_all(),
                    "recordsFiltered" => $this->Model_notice->count_filtered(),
                    "data" => $data,
                );
        // output to json format
        echo json_encode($output);
    }

}