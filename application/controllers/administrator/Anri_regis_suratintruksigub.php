<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_regis_suratintruksigub extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');	
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_konsepnaskah_koreksi');
	}

	//Registrasi Surat Tugas
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		$this->data['title'] = 'Pencatatan Surat Tugas';
		$this->tempanri('backend/standart/administrator/surat_instruksi/regis', $this->data);
	}
	// Tutup Registrasi Surat Tugas

	//Fungsi Lihat Surat Tugas
	public function lihat_naskah_dinas2()
	{

		$tanggal = date('Y-m-d H:i:s');
		$this->load->library('Pdf');
      	// ob_start();

		$data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$data['Hal'] 		= $this->input->post('Hal');
		$data['Hal_pengantar'] = $this->input->post('Hal_pengantar');
		$data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$data['RoleId_To_2'] 	= $this->input->post('RoleId_To_2');
		$data['Konten'] 	= $this->input->post('Konten');
		$data['TglReg'] 		= date('Y-m-d H:i:s');
		$data['RoleCode'] 	= $this->input->post('RoleCode');
		$data['ClCode'] 	= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$this->input->post('ClId')."'")->row()->ClCode;
		$data['TtdText']    = (!empty($this->input->post('TtdText')) ?  $this->input->post('TtdText') : '');
		$data['TtdText2']    = (!empty($this->input->post('TtdText2')) ?  $this->input->post('TtdText2') : '');
		
		if($data['TtdText'] == 'none') {
			$data['Approve_People']	= $this->session->userdata('peopleid');
			$data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif($data['TtdText'] == 'AL') {
			$data['Approve_People'] 	= $this->input->post('tmpid_atasan');
			$data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;
		} else {
			$data['Approve_People'] 	= $this->input->post('Approve_People');
			$data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}

		$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = 'uk.1' ")->row()->Header;
		$sql_gub= $this->db->query("SELECT PeopleName FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeopleName;
		$sql_namagub= $this->db->query("SELECT PeoplePosition FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeoplePosition;

		// var_dump($data_perintah);
		// die();

		$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

		//tcpdf
		$age = $this->session->userdata('roleid') == 'uk.1';
		$xx = $this->session->userdata('roleid');
		
		
		$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td rowspan="7" width="60px" style="valign:bottom;">
				<br><br>
				<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
				<td width="180px" style="text-align: left; font-size:9;">'.$sql_namagub.',</td>
			</tr>
			<tr nobr="true">
				<td><br></td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">
				</td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">'.$sql_gub.'
				</td>
			</tr>
		</table>';


		$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
		$pdf->SetMargins(30, 30, 30);
		$pdf->SetAutoPageBreak(TRUE, 30);

		// 
		$pdf->AddPage('P', 'F4');
		$pdf->SetFont('Arial','',12);

	
		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),25,5,168,0,'PNG');
		$pdf->Ln(13);
		$pdf->SetX(38);
		$pdf->Cell(0,10,'INSTRUKSI GUBERNUR JAWA BARAT',0,0,'C');
		$pdf->Ln(7);
		$pdf->SetX(38);
		$pdf->Cell(0,10,'NOMOR : .........../'.$data['ClCode'].'/'.$data['RoleCode'],0,0,'C');
		$pdf->Ln(10);
		$pdf->SetX(38);
		$pdf->Cell(0,5,'TENTANG',0,0,'C');
		$pdf->Ln(7);
		$pdf->writeHTMLCell(160, 0, '', '',$data['Hal'], 0, 1, 0, true, 'C', true);
		
		$pdf->Ln(2);
		$pdf->SetX(38);
		$pdf->Cell(0,10,'GUBERNUR JAWA BARAT,',0,1,'C');
		$pdf->Ln(3);
		$pdf->SetX(30);
		$pdf->writeHTMLCell(160, 0, '', '', $data['Hal_pengantar'], 0, 1, 0, true, 'J', true);
		$pdf->Ln(5);
		$pdf->Cell(55,5,'Dengan ini meninstruksikan',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->Ln(7);
		$pdf->Cell(30,5,'Kepada',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		
		$i=0;
		
		if (!empty($data['RoleId_To'])) {
	  	$filter_RoleId_To=array_filter($data['RoleId_To']);
			foreach( $filter_RoleId_To as $v){
			$xx1 =  $this->db->query("SELECT PeoplePosition FROM v_login WHERE PeopleId = '" . $v . "'")->row()->PeoplePosition;

			$uc  = ucwords(strtolower($xx1));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			$str = str_replace('Dprd', 'DPRD', $str);

			$i++;
			$pdf->SetX(65);
			$pdf->Cell(10,5,$i.'. ',0,0,'L');
			$pdf->SetX(70);
			$pdf->MultiCell(117, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
			
			}
		
		}


		if (!empty($data['RoleId_To_2'])) {
		$filter_RoleId_To=array_filter($data['RoleId_To_2']);
		foreach($filter_RoleId_To as $row){

		$i++;
		$pdf->SetX(65);
		$pdf->Cell(10,5,$i.'. ',0,0,'L');
		$pdf->SetX(70);
		$pdf->MultiCell(117, 5, $row."\n", 0, 'J', 0, 2, '' ,'', true);
		
			}
			
		}
		$pdf->Ln(2);
		$pdf->Cell(30,5,'Untuk',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->Ln(8);

		if (!empty($data['Konten'])) {
	 	$filter_konten=array_filter($data['Konten']);
		$jum_lamp=1;

		    foreach ($filter_konten as $isi_konten) {

		    	switch ($jum_lamp){
	            case 1: 
	                $lamp_romawi = "KESATU";
	                break;
	            case 2:
	                $lamp_romawi =  "KEDUA";
	                break;
	            case 3:
	                $lamp_romawi =  "KETIGA";
	                break;
	            case 4:
	                $lamp_romawi =  "KEEMPAT";
	                break;
	            case 5:
	                $lamp_romawi =  "KELIMA";
	                break;
	            case 6:
	                $lamp_romawi =  "KEENAM";
	                break;
	            case 7:
	                $lamp_romawi =  "KETUJUH";
	                break;
	          
	            case 8:
	                return "KEDELAPAN";
	                break;
	            }

	            $pdf->Cell(30,5,$lamp_romawi,0,0,'L');
	            $pdf->Cell(5,5,':',0,0,'L');
	           	$pdf->SetX(65);
	           	$pdf->writeHTMLCell(125, 0, '', '', $isi_konten, 0, 1, 0, true, 'J', true);
	            $pdf->Ln(3);
	            $jum_lamp++;
		    }
		}
	    $pdf->Ln(3);
	    $pdf->Cell(30,5,'Instruksi ini mulai berlaku pada tanggal ditetapkan.',0,1,'L');
		$pdf->Ln(10);
		$pdf->SetX(125);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Ditetapkan di ..............',0,1,'L');
		$pdf->SetX(120);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Pada tanggal ..................',0,1,'L');
		$pdf->Ln(2);
		if($data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$data['Approve_People']).',',0,'C');
		}elseif($data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$data['Approve_People']).',',0,'C');
		}elseif($data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$data['Approve_People']).',',0,'C');
		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,$sql_namagub.',',0,'C');
		}
		$pdf->Ln(5);
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

		$pdf->Output('surat_instruksigub_view.pdf','I');
		
     //    $this->load->view('backend/standart/administrator/pdf/view_instruksigub_pdf',$data);

	    // $content = ob_get_contents();
	    // ob_end_clean();
        
     //    $config = array(
     //        'orientation' 	=> 'p',
     //        'format' 		=> 'F4',
     //        'marges' 		=> array(30, 30, 20, 10),
     //    );

     //    $this->pdf = new HtmlPdf($config);
     //    $this->pdf->initialize($config);
     //    $this->pdf->pdf->SetDisplayMode('fullpage');
     //    $this->pdf->writeHTML($content);
     //    $this->pdf->Output('naskah_dinas_tindaklanjut-'.$tanggal.'.pdf', 'H');
	}
	// Akhir Fungsi Surat Tugas
	//Simpan Registrasi Surat Tugas
	public function post_surat_intruksi_gub()
	{

		// $gir = $this->session->userdata('peopleid').date('dmyhis');

		$gir_awal= $this->session->userdata('peopleid').date('dmyhis');	
		$jumlah_karakter    =strlen($gir_awal);		
		if ($jumlah_karakter == 17){

			$sub_gir = substr($gir_awal,0,16);
			$cek_NId_Temp="SELECT NId_Temp FROM konsep_naskah WHERE NId_Temp = '$_POST[$sub_gir]'";
			if ($cek_NId_Temp > 0) {

				$sub_gir = substr($gir_awal,0,16)+1;

			}else{

				$sub_gir = substr($gir_awal,0,16);

			}

		}else{

			$sub_gir = $this->session->userdata('peopleid').date('dmyhis');

		}


		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Lokasi;

		if($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
			$x =  $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;
			$x = $this->input->post('tmpid_atasan');	
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
			$x = $this->input->post('Approve_People');
		}


		try{

			$data_konsep = [
				// 'NId_Temp'   		=> $gir,
				// 'GIR_Id'       		=> $gir,
				'NId_Temp'   		=> $sub_gir,
				'GIR_Id'       		=> $sub_gir,

				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Instruksi Gubernur'")->row()->JenisId,
				'ReceiverAs'       	=> 'to_instruksi_gub',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('RoleCode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=>  (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
				'RoleId_To_2'     	=>  (!empty($this->input->post('RoleId_To_2')) ?  json_encode(array_filter($this->input->post('RoleId_To_2'))) : ''),
				// 'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : ''),
				'Approve_People'    => $x,
				'TtdText'       	=> $this->input->post('TtdText'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'       		=> $this->input->post('Hal_pengantar'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxinstruksigub',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];


				//memunculkan konsep awal
			$data_konsep_awal = [
				// 'NId_Temp'   		=> $gir,
				// 'GIR_Id'       		=> $gir,
				'NId_Temp'   		=> $sub_gir,
				'GIR_Id'       		=> $sub_gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Instruksi Gubernur'")->row()->JenisId,
				'ReceiverAs'       	=> 'to_draft_instruksi_gub',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('RoleCode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=>  (!empty($this->input->post('RoleId_To')) ?  json_encode(array_filter($this->input->post('RoleId_To'))) : ''),
				'RoleId_To_2'       	=>  (!empty($this->input->post('RoleId_To_2')) ?  json_encode(array_filter($this->input->post('RoleId_To_2'))) : ''),
				// 'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : ''),
				'Approve_People'    => $x,
				'TtdText'       	=> $this->input->post('TtdText'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'     => $this->input->post('Hal_pengantar'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxinstruksigub',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				// 'id_koreksi'    	=> $gir,
				'id_koreksi'    	=> $sub_gir,
			];

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
			            // 'GIR_Id' 			=> $gir,
			            'GIR_Id' 			=> $sub_gir,
			            'NId' 				=> $data_konsep['NId_Temp'],
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);

				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

			//memunculkan konsep awal
			$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep_awal);
			//akhir memunculkan konsep awal

			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid').date('dmyhis');
			
			$save_recievers = [
				// 'NId' 			=> $gir,
				// 'NKey' 			=> tb_key(),
				// 'GIR_Id' 		=> $girid_ir,
				'NId' 			=> $sub_gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $sub_gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_instruksi_gub',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_recievers_koreksi = [
				// 'NId' 			=> $gir,
				// 'NKey' 			=> tb_key(),
				// 'GIR_Id' 		=> $girid_ir,
				'NId' 			=> $sub_gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $sub_gir,
				'id_koreksi'	=> $sub_gir,
				
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_instruksi_gub',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
				'id_koreksi'	=> $gir,
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);
			

			//memunculkan konsep awal

			$save_reciever_koreksi = $this->model_inboxreciever_koreksi->store($save_recievers_koreksi);
			//akhir memunculkan konsep awal

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
					
	}
	//Akhir Simpan Registrasi Surat Tugas
//Edit naskah dinas
	public function postedit_instruksigub($NId)
	{
		$gir = $NId;
		$tanggal = date('Y-m-d H:i:s');

		$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

		if ($naskah1->Ket != 'outboxsprint') {
			$sifat =  $this->input->post('SifatId');
		} else {
			$sifat =  NULL;
		}

		if ($naskah1->Ket == 'outboxkeluar') {
			$alamat =  $this->input->post('Alamat');

			$role = $this->input->post('RoleId_To');
			$konten = $this->input->post('Konten');
			//penambahan
			$lampiran =$this->input->post('lampiran');
			//akhir penambahan

		} elseif ($naskah1->Ket == 'outboxedaran') {
			$alamat =  $this->input->post('Alamat');
			$role = $this->input->post('RoleId_To');
			$konten = $this->input->post('Konten');
		} elseif ($naskah1->Ket == 'outboxinstruksigub') {
			$alamat =  $this->input->post('Alamat');
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$role2 = (!empty($this->input->post('RoleId_To_2')) ?  json_encode(array_filter($this->input->post('RoleId_To_2'))) : '');
			$konten = (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : '');
		} elseif ($naskah1->Ket == 'outboxsuratizin') {
			$alamat =  $this->input->post('Alamat');
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$konten = (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : '');

		} else {
			$alamat =  NULL;
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$konten = $this->input->post('Konten');
			$lampiran =(!empty($this->input->post('isi_lampiran')) ?  json_encode($this->input->post('isi_lampiran')) : '');
		}


		// } else {
		// 	$alamat =  '';
		// 	$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
		// 	$konten = $this->input->post('Konten');
		// }

		if ($naskah1->Ket == 'outboxkeluar') {
			$ket =  'outboxkeluar';
		} elseif ($naskah1->Ket == 'outboxedaran') {
			$ket =  'outboxedaran';
		} elseif ($naskah1->Ket == 'outboxnotadinas') {
			$ket =  'outboxnotadinas';
		} elseif ($naskah1->Ket == 'outboxsprint') {
			$ket =  'outboxsprint';
		} elseif ($naskah1->Ket == 'outboxinstruksigub') {
			$ket =  'outboxinstruksigub';
		} elseif ($naskah1->Ket == 'outboxsupertugas') {
			$ket =  'outboxsupertugas';
		} elseif ($naskah1->Ket == 'outboxsuratizin') {
			$ket =  'outboxsuratizin';
		} else {
		}


		try {
			$CI = &get_instance();
			$this->load->helper('string');

			$data_konsep = [
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'SifatId'       	=> $sifat,
				'Alamat'       		=> $alamat,
				'ClId'       		=> $this->input->post('ClId'),
				'RoleId_To'       	=> $role,
				'RoleId_To_2'       => $role2,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $konten,
				'lampiran'       	=> $lampiran,
			
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'       		=> $this->input->post('Hal_pengantar'),
				'Nama_ttd_pengantar' 		=> $this->input->post('Nama_ttd_pengantar'),
				'RoleCode' 					=> $this->input->post('RoleCode'),

			];

			//menyimpan record koreksi pada konsep naskah koreksi
			$type_naskah = $this->db->query("SELECT type_naskah FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->type_naskah;
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$data_konsep2 = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'id_koreksi'		=> $girid_ir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('RoleCode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $role,
				'RoleId_To_2'       => $role2,
				'Alamat'       		=> $alamat,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $konten,
				'lampiran'       	=> $lampiran,
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Approve_People'       	=> $this->db->query("SELECT Approve_People FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People,
				'Approve_People3'    =>  $this->db->query("SELECT Approve_People3 FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People3,
				'TtdText'       	=> $this->db->query("SELECT TtdText FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText,
				'TtdText2'       	=>  $this->db->query("SELECT TtdText2 FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText2,
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'     => $this->input->post('Hal_pengantar'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> $ket,
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep'   => $this->db->query("SELECT Nama_ttd_konsep FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_konsep,

				'Nama_ttd_atas_nama'       	=> $this->db->query("SELECT Nama_ttd_atas_nama FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_atas_nama,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'type_naskah'			=> $type_naskah,
			
				'Nama_ttd_pengantar' 	=> $this->input->post('Nama_ttd_pengantar'),

			];

			$this->db->where('NId_Temp', $NId);
			$this->db->update('konsep_naskah', $data_konsep);

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'RoleCode'       	=> $this->input->post('RoleCode'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outboxkeluar',
					];
					$save_file = $this->model_inboxfile->store($save_files);
				}

				//penambahan koreksi
				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploadedkoreksi/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);
					}
				}
				//akhir penambahan koreksi
			}


			//penambahan koreksi2
			$save_konsep2 	= $this->model_konsepnaskah_koreksi->store($data_konsep2);
			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$save_recievers = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),

				'To_Id' 		=> $this->db->query("SELECT To_Id FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->To_Id,
				'RoleId_To'       	=> $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->RoleId_To,
				'id_koreksi'		=>	$girid_ir,
				'ReceiverAs' 	=> 'to_koreksi',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'read',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];
			$save_reciever2 = $this->model_inboxreciever_koreksi->store($save_recievers);
			//akhir penambahan koreksi2

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup Edit naskah dinas


}