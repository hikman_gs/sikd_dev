<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_naskah_teruskan_approve extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_naskah_teruskan_approve');	
	}

	//Naskah Teruskan Approve
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Pencatatan Naskah Teruskan Untuk Disetujui';
		$this->tempanri('backend/standart/administrator/mail_tl/naskah_teruskan_approve', $this->data);
	}
	//Tutup Naskah Teruskan Approve

	//Ambil Data Seluruh Naskah Teruskan Approve
	public function get_data_naskah_bs()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_teruskan_approve->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;

			$naskah = $this->db->query("SELECT NId_Temp, JenisId, nosurat, Hal, RoleId_To FROM konsep_naskah WHERE NId_Temp = '".$field->NId."'")->row();

			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;	
	
			$row[] = date('d-m-Y',strtotime($field->ReceiveDate));
			$row[] = $naskah->Hal;
			$row[] = $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$field->RoleId_To."'")->row()->RoleName;

          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	

			$row[] = $z21;
			
			$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->NId."' AND Keterangan = 'outbox'")->num_rows();

			if($x > 0) {

				$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->NId."' AND Keterangan = 'outbox'")->row();

				$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" title="Lihat Histori Naskah" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';	

			} else {

				$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" title="Lihat Histori Naskah" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_teruskan_approve/log_naskah_dinas_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> ';				
			
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_teruskan_approve->count_all(),
			"recordsFiltered" => $this->model_list_naskah_teruskan_approve->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Teruskan Approve

	//View PDF Naskah Dinas
	public function log_naskah_dinas_pdf($id)
	{
		$this->load->library('HtmlPdf');
      	ob_start();

      	$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$id."' LIMIT 1")->row();

      	$this->data['NId'] 				= $id;
      	$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
      	$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;
      	$this->data['RoleId_To'] 		= $naskah->RoleId_To;
      	$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
      	$this->data['Konten'] 			= $naskah->Konten;
      	$this->data['lampiran'] 		= $naskah->lampiran;

		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$naskah->SifatId."'")->row()->SifatName;
	      	$this->data['Jumlah'] 			= $naskah->Jumlah;
	      	$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$naskah->MeasureUnitId."'")->row()->MeasureUnitName;			
      		$this->data['Hal'] 				= $naskah->Hal;	      	
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}

      	$this->data['TglReg'] 			= $naskah->TglReg;
      	$this->data['Approve_People'] 	= $naskah->Approve_People;
      	$this->data['TtdText'] 			= $naskah->TtdText;
      	$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
      	$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$naskah->ClId."'")->row()->ClCode;
		
	    if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxsprint'){ 
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxundangan'){ 
			$this->load->view('backend/standart/administrator/pdf/view_konsep_undangan_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxkeluar'){ 
        	$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_dinas_tindaklanjut_pdf',$this->data);
		}else {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_naskah_dinas_tindaklanjut_pdf',$this->data);
		}

	    $content = ob_get_contents();
	    ob_end_clean();
        
           $config = array(
		            'orientation' 	=> 'p',
		            'format' 		=> 'f4',
		            'marges' 		=> array(30, -2, 20, 30),
		        );


        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('naskah_dinas_tindaklanjut-'.$id.'.pdf', 'H');
	}
	//Akhir View PDF Naskah Dinas

}