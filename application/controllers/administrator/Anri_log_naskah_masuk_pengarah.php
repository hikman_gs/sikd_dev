<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_log_naskah_masuk_pengarah extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_log_naskah_masuk_pengarah');
	}

	//Log Naskah Masuk
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/log_registrasi/log_naskah_masuk_pengarah', $this->data);
	}
	//Tutup Log Naskah Masuk
	
	//Ambil Data Log Seluruh Naskah Masuk
	public function get_data_log_surat_all()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_log_naskah_masuk_pengarah->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			$btn1 = "";
			$btn2 = "hidden";
			
			$row[] = $no;
			$ClCode = '<span class="text-red"><b>Belum diisi</b></span>';
			if($field->ClId != "1"){
			$ClCode = $this->db->query("SELECT ClCode from classification where ClId = '".$field->ClId."'")->row()->ClCode;
			}
			$Urgensi = '<span class="text-red"><b>Belum diisi</b></span>';
			if($field->UrgensiId != NULL){
			$Urgensi = $this->db->query("SELECT UrgensiName FROM master_urgensi WHERE UrgensiId ='".$field->UrgensiId."'")->row()->UrgensiName;
			}
			$row[] = $ClCode.'/<br/>'.$Urgensi;
			
			$uraian = '<small>Nomor : '.$field->Nomor.' Tgl.'.date('d/m/Y',strtotime($field->Tgl)).'</small><br/>'.$field->Hal;

			$row[] = $uraian;
			$row[] = $field->Instansipengirim;
			$tujuan = '<span class="text-red"><b>Belum diisi</b></span>';
			$roleid = $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '".$field->NId."' and ReceiverAs = 'to' ORDER BY RoleId_To ASC ");
			if($roleid->num_rows() != 0){
				$tujuan = "";
				$btn1 = "hidden";
				$btn2 = "";
				foreach($roleid->result() as $role){
					$tujuan .= get_data_people('RoleName',$role->RoleId_To).',<br>';
				}
			}
			$row[] = substr($tujuan,0,-2);
			
			$files = $this->db->query("SELECT FileName_fake, FileName_real from inbox_files WHERE NId = '".$field->NId."' ");
			$fileupload = '<span class="text-red"><b>Belum upload file</b></span>';
			if($files->num_rows() != 0){
				$fileupload = "";
				$btn1 = "hidden";
				$btn2 = "";
				foreach($files->result() as $filename){
					$path_file = BASE_URL('FilesUploaded/naskah/'.$filename->FileName_fake);
					$fileupload = '<a target="_blank" href='.$path_file.' class="btn btn-danger btn-sm"><i class="fa fa-download"></i></a>, ';
				}
			}
				
			$row[] = $fileupload;
			

			if($field->CreationRoleId == $this->session->userdata('roleid')){
				
					$row[] = '<a href="'.site_url('administrator/anri_log_naskah_masuk_pengendali/view_log_naskah_metadata_kendali/log/'.$field->NId).'" title="kartu kendali" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-envelope"></i> </a> <a  href="'.site_url('administrator/anri_log_naskah_masuk_pengarah/edit_naskah_masuk_pengarah/log/'.$field->NId.'?dt=1').'" title="Lihat Detail Naskah" class="btn btn-primary btn-sm '.$btn1.' "><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/'.$field->NId.'?dt=1').'" title="Lihat Detail Naskah" class="btn btn-danger btn-sm '.$btn2.' "><i class="fa fa-edit"></i></a>';
              	
            } 


			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_log_naskah_masuk_pengarah->count_all(),
			"recordsFiltered" => $this->model_list_log_naskah_masuk_pengarah->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Log Seluruh Naskah Masuk	

	//Hapus naskah masuk
	public function hapus_naskah_masuk($NId)
	{
		$this->data['NId'] = $NId;

		$query1 	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '".$NId."'");				
		
		foreach($query1->result_array() as $row1):
			$fileDir = $row1['NFileDir'];
		endforeach;	

		if($fileDir != ''){

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$NId."'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach($query->result_array() as $row):
				if(is_file($files.$row['FileName_fake']))
					unlink($files.$row['FileName_fake']);
			endforeach;		
		}	

		$this->db->query("DELETE FROM `inbox_receiver` WHERE NId = '".$NId."'");
		$this->db->query("DELETE FROM `inbox_files` WHERE NId = '".$NId."'");
		$this->db->query("DELETE FROM `inbox` WHERE NId = '".$NId."'");

		$this->tempanri('backend/standart/administrator/log_registrasi/log_naskah_masuk', $this->data);
	}
	//Tutup Hapus naskah masuk

	//Buka view log metadata
	public function view_log_naskah_metadata($tipe,$NId)
	{

		$this->data['title'] 	= 'Detail Naskah Masuk';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox'] = $this->db->query("SELECT * FROM inbox WHERE NId = '".$NId."' LIMIT 1")->row();


		$this->tempanri('backend/standart/administrator/log_registrasi/inbox_log_view_metadata', $this->data);
	}
	//Tutup view log metadata

	//Buka view log naskah histori
	public function view_log_naskah_histori($tipe,$NId)
	{

		$this->data['title'] 	= 'Detail Naskah Masuk';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->tempanri('backend/standart/administrator/log_registrasi/inbox_log_view_histori', $this->data);
	}
	//Tutup view log naskah histori
	//tambahan invoice
	public function view_log_naskah_metadata_invoice($tipe,$NId)
	{

		$this->data['title'] 	= 'Detail Naskah Masuk';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox'] = $this->db->query("SELECT * FROM inbox WHERE NId = '".$NId."' LIMIT 1")->row();


		$this->tempanri('backend/standart/administrator/log_registrasi/inbox_log_view_metadata_invoice', $this->data);
	}
	
	public function edit_naskah_masuk_pengarah($tipe,$NId){
		
		$this->data['title'] 	= 'Detail Naskah Masuk Pengarah';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['data'] = $this->db->query("SELECT * FROM inbox WHERE NId = '".$NId."' LIMIT 1")->row();

		$this->tempanri('backend/standart/administrator/reg_naskah_masuk_setda/inbox_edit_pengarah', $this->data);
	}

}