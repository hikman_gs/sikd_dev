<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_grole extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_grole');
	}

	// List grup unit kerja
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_grole/master_grole_list', $this->data);
	}
	// Tutup list grup unit kerja

	// Tambah data grup unit kerja
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_grole/master_grole_add', $this->data);
	}
	// Tutup tambah data grup unit kerja

	// Proses simpan data grup unit kerja
	public function add_save()
	{

		$this->form_validation->set_rules('GRoleName', 'GRoleName', 'trim|required|max_length[250]');

		if ($this->form_validation->run()) {
			$table = 'master_grole';

			$query = $this->db->query("SELECT (max(convert(substr(GRoleId, 12), UNSIGNED)) + 1) as id FROM master_grole")->row()->id;


			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}			
			
			$save_data = [
				'GRoleId' => tb_key().'.'.$id,
				'GRoleName' => $this->input->post('GRoleName'),
			];

			
			$save_master_grole = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_grole'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_grole'));			
		}

	}
	// Tutup proses simpan data grup unit kerja

	// Edit data grup unit kerja
	public function update($id)
	{
		$this->data['master_grole'] = $this->model_master_grole->find($id);
		$this->tempanri('backend/standart/administrator/master_grole/master_grole_update', $this->data);
	}
	// Tutup edit data grup unit kerja

	// Proses update data grup unit kerja
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('GRoleName', 'GRoleName', 'trim|required|max_length[250]');
		
		if ($this->form_validation->run()) {
			$save_data = [
				'GRoleName' => $this->input->post('GRoleName'),
			];

			
			$save_master_grole = $this->model_master_grole->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_grole'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_grole'));			
		}

	}
	// Tutup proses update data grup unit kerja

	// Hapus data grup unit kerja
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_grole'));				
	}
	// Tutup hapus data grup unit kerja

	// Proses hapus data grup unit kerja
	private function remove($id)
	{
		$master_grole = $this->db->where('GRoleId',$id)->delete('master_grole');
		return $master_grole;
	}
	// Tutup proses hapus data grup unit kerja
}