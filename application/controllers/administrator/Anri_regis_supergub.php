<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_regis_supergub extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');	
		$this->load->model('model_inboxreciever');
	}

	//Registrasi Surat Tugas
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		$this->data['title'] = 'Pencatatan Surat Tugas';
		$this->tempanri('backend/standart/administrator/surat_tugas/regisgub', $this->data);
	}
	// Tutup Registrasi Surat Tugas

	//Fungsi Lihat Surat Tugas
	public function lihat_konsep_supergub()
	{

		$tanggal = date('Y-m-d H:i:s');
		$this->load->library('HtmlPdf');
      	ob_start();

      	$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$this->data['Hal'] 			= $this->input->post('Hal');
		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['RoleCode'] 	= $this->input->post('rolecode');
		

		if($this->input->post('TtdText') == 'none') {
			$this->data['Approve_People']	= $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$this->data['Approve_People'] 	= $this->input->post('tmpid_atasan');		
		} else {
			$this->data['Approve_People'] 	= $this->input->post('Approve_People');
		}
		
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$this->input->post('ClId')."'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');

		if($this->input->post('TtdText') == 'none') {
			$this->data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;		
		} else {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}
		
        $this->load->view('backend/standart/administrator/pdf/view_supergub_pdf',$this->data);

	    $content = ob_get_contents();
	    ob_end_clean();
        
        $config = array(
            'orientation' 	=> 'p',
            'format' 		=> 'a4',
            'marges' 		=> array(30, 30, 20, 30),
        );


        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('naskah_dinas_tindaklanjut-'.$tanggal.'.pdf', 'H');

	}
	// Akhir Fungsi Surat Tugas

	//Simpan Registrasi Surat Tugas
	public function post_supergub()
	{

		// $gir = $this->session->userdata('peopleid').date('dmyhis');
		$gir_awal= $this->session->userdata('peopleid').date('dmyhis');	
		$jumlah_karakter    =strlen($gir_awal);		
		if ($jumlah_karakter == 17){

			$sub_gir = substr($gir_awal,0,16);
			$cek_NId_Temp="SELECT NId_Temp FROM konsep_naskah WHERE NId_Temp = '$_POST[$sub_gir]'";
			if ($cek_NId_Temp > 0) {

				$sub_gir = substr($gir_awal,0,16)+1;

			}else{

				$sub_gir = substr($gir_awal,0,16);

			}

		}else{

			$sub_gir = $this->session->userdata('peopleid').date('dmyhis');

		}


		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Lokasi;

		if($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;	
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}	

		if($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');		
		} else {
			$x = $this->input->post('Approve_People');
		}	

		try{

			$data_konsep = [
				// 'NId_Temp'   		=> $gir,
				// 'GIR_Id'       		=> $gir,
				'NId_Temp'   		=> $sub_gir,
				'GIR_Id'       		=> $sub_gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Perintah'")->row()->JenisId,
				'ReceiverAs'       	=> 'to_sprint',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',',$this->input->post('RoleId_To')) : ''),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'TtdText'       	=> $this->input->post('TtdText'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxsprintgub',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];

				//memunculkan konsep awal

			$data_konsep_AWAL = [
				// 'NId_Temp'   		=> $gir,
				// 'GIR_Id'       		=> $gir,
				'NId_Temp'   		=> $sub_gir,
				'GIR_Id'       		=> $sub_gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Perintah'")->row()->JenisId,
				'ReceiverAs'       	=> 'to_sprint',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',',$this->input->post('RoleId_To')) : ''),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'TtdText'       	=> $this->input->post('TtdText'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxsprintgub',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'id_koreksi'    	=> $sub_gir,
			];

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
			            // 'GIR_Id' 			=> $gir,
			            'GIR_Id' 			=> $sub_gir,
			            'NId' 				=> $data_konsep['NId_Temp'],
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);

				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

			//memunculkan konsep awal
			$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep_awal);
			//akhir memunculkan konsep awal

			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid').date('dmyhis');
			
			$save_recievers = [
				// 'NId' 			=> $gir,
				// 'NKey' 			=> tb_key(),
				// 'GIR_Id' 		=> $girid_ir,
				'NId' 			=> $sub_gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $sub_gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_sprint',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			$save_recievers_koreksi = [
				// 'NId' 			=> $gir,
				// 'NKey' 			=> tb_key(),
				// 'GIR_Id' 		=> $girid_ir,
				'NId' 			=> $sub_gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $sub_gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_sprint',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_reciever_koreksi = $this->model_inboxreciever_koreksi->store($save_recievers_koreksi);
			//akhir memunculkan konsep awal
			
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
					
	}
	//Akhir Simpan Registrasi Surat Tugas

}