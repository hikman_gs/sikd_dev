<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_berkas_unit extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_berkas');								
		$this->load->model('model_list_berkas');
	}

	//Buka Daftar berkas unit kerja
	public function index()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/berkas/list_berkas', $this->data);
	}
	//Tutup Daftar berkas unit kerja

	// Fungsi Tambah Data berkas unit
	public function berkas_unit_add()
	{
		$this->data['clasification'] = $this->model_custome->classification();
		$this->tempanri('backend/standart/administrator/berkas/berkas_unit_add', $this->data);
	}
	// Akhir Fungsi Tambah Data berkas unit

	//Buka Simpan Berkas
	public function berkas_unit_save()
	{
		try {
			$this->load->helper('string'); 

			$xxz = $this->input->post('ClId');

			$rva = $this->input->post('RetensiValue_Active');
			$rvi = $this->input->post('RetensiValue_InActive');
			$year = date('Y');
			$trva = $rva + $year;
			$trvi = $rva + $rvi + $year;

			$date = date('m-d');
			$date_rva = $trva.'-'.$date;
			$date_rvi = $trvi.'-'.$date;

			$tutup = $this->input->post('HitungId');
			$tanggal = date('Y-m-d');
			if($tutup == 'closed') {
				$x = '0000-00-00';
			} else {
				$x = $tanggal;
			}

			$tahun = date('Y');
			$nberkas = $this->db->query("SELECT (max(BerkasNumber) + 1) as BerkasNumber FROM berkas WHERE ClId='".$this->input->post('ClId')."' AND YEAR(CreationDate) = '".$tahun."' AND RoleId='".$_SESSION['roleid']."'")->row()->BerkasNumber;
			if (!empty($nberkas)){
				$id = $nberkas;
			} else {
			    $id = 1;
			}

			$data = [
				'BerkasKey' 				=> tb_key(),
				'BerkasId' 					=> substr(preg_replace("/[^0-9]/", '',sha1(rand())), 0,5),
				'RoleId' 					=> $this->session->userdata('roleid'),
				'ClId' 						=> $this->input->post('ClId'),
				'Klasifikasi'				=> $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$xxz."'")->row()->ClCode,
				'BerkasNumber' 				=> $id,
				'BerkasName' 				=> $this->input->post('BerkasName'),
				'RetensiValue_Active' 		=> $date_rva,
				'RetensiTipe_Active' 		=> $rva.'//',
				'RetensiValue_InActive' 	=> $date_rvi,
				'RetensiTipe_InActive' 		=> $rvi.'//',
				'Tutup_Berkas'		 		=> $x,
				'SusutId' 					=> $this->input->post('SusutId'),
				'BerkasLokasi' 				=> $this->input->post('BerkasLokasi'),
				'BerkasDesc' 				=> $this->input->post('BerkasDesc'),
				'CreatedBy' 				=> $this->session->userdata('peopleid'),
				'CreationDate' 				=> date('Y-m-d'),
				'BerkasStatus' 				=> 'open',
				'BerkasCountSince' 			=> $this->input->post('HitungId'),
				'KetAkhir' 					=> '',
			];

			$save = $this->model_berkas->store($data);

			set_message('Data Berhasil Disimpan', 'success');	
			redirect(BASE_URL('administrator/anri_berkas_unit/'));

		} catch (Exception $e) {
			set_message('Data Gagal Disimpan', 'error');
			redirect(BASE_URL('administrator/anri_berkas_unit/'));
		}
	}
	//Tutup simoan berkas

	//Buka Ambil Data Seluruh arsip aktif
	public function get_data_berkas()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_berkas->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;

			if ($field->BerkasStatus=='open') {
				$xx = '<center><a href="'.site_url('administrator/anri_berkas_unit/berkas_unit_update_status?id='.$field->BerkasId.'&st=closed').'" title="Tutup Berkas" class="btn btn-success btn-sm"><i class="fa fa-unlock"></i></a></center>';
			} else {
				$xx = '<center><a href="'.site_url('administrator/anri_berkas_unit/berkas_unit_update_status?id='.$field->BerkasId.'&st=open').'" title="Buka Berkas" class="btn btn-danger btn-sm"><i class="fa fa-lock"></i></a></center>';
			}
			$row[] = $xx;

			$row[] = $field->Klasifikasi;

			$xx1 = $this->db->query("SELECT Klasifikasi, BerkasNumber, tahun_cipta FROM v_hitung_nomor WHERE BerkasId='".$field->BerkasId."'")->row();

			$row[] = $xx1->Klasifikasi.'/'.$xx1->BerkasNumber.'/'.$xx1->tahun_cipta;

			$row[] = $field->BerkasName;

			$row[] = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId."'")->row()->RoleName;
			$row[] = date('d-m-Y',strtotime($field->CreationDate));
			$row[] = date('d-m-Y',strtotime($field->RetensiValue_Active));

        	if($this->db->query("SELECT BerkasId FROM inbox WHERE BerkasId = '".$field->BerkasId."'")->num_rows() > 0)
      		{
      			if ($field->BerkasStatus=='open') {
					$row[] = '<a href="'.site_url('administrator/anri_berkas_unit/berkas_unit_update/'.$field->BerkasId).'" title="Ubah Berkas" class="btn-primary btn-sm btn"><i class="fa fa-edit"></i></a>';
				} else {
					$row[] = '';
				}	
          	} else {
				$row[] = '<a href="'.site_url('administrator/anri_berkas_unit/berkas_unit_update/'.$field->BerkasId).'" title="Ubah Berkas" class="btn-primary btn-sm btn"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_berkas_unit/berkas_unit_delete/'.$field->BerkasId).'" title="Hapus Berkas" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
          	}
            
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_berkas->count_all(),
			"recordsFiltered" => $this->model_list_berkas->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh arsip aktif

	//Buka ubah berkas
	public function berkas_unit_update_status()
	{
		$tb_key = tb_key();
		$id = $_GET['id'];
		$st = $_GET['st'];

		$tanggal = date('Y-m-d');

    	$xx1 = $this->db->query("SELECT BerkasId, RetensiTipe_Active, RetensiTipe_InActive, BerkasCountSince FROM berkas WHERE BerkasId = '".$id."'")->row();
  		
		//Jika retensi dihitung dari tutup berkas, maka field tutup_berkas = tanggal tutup berkas, tanggal akhir retensi diambil berdasarkan akumulasi tutup berkas 
		if ($xx1->BerkasCountSince=='closed') {
			$x = $tanggal;
		} 

	    $tahunaktif = explode('/', $xx1->RetensiTipe_Active);
	    $tahuninaktif = explode('/', $xx1->RetensiTipe_InActive);

		$rva = $tahunaktif[0];
		$rvi = $tahuninaktif[0];

		$year = date('Y');
		$trva = $rva + $year;
		$trvi = $rva + $rvi + $year;

		$date = date('m-d');
		$date_rva = $trva.'-'.$date;
		$date_rvi = $trvi.'-'.$date;

		$data = array(
            'BerkasStatus' 				=> $st,
			'RetensiValue_Active' 		=> $date_rva,
			'RetensiValue_InActive' 	=> $date_rvi,            
            'Tutup_Berkas'		 		=> $x,
        );

		$this->db->where('BerkasId', $id);
		$this->db->where('BerkasKey', $tb_key);
		$update = $this->db->update('berkas', $data);
		set_message('Status Berhasil Diubah', 'success');	
		redirect(base_url('administrator/anri_berkas_unit'));
	}
	//Tutup ubah berkas

	//Buka proses edit berkas
	public function berkas_unit_update()
	{
		$this->data['clasification'] = $this->model_custome->classification();
		$this->tempanri('backend/standart/administrator/berkas/berkas_unit_update', $this->data);
	}
	//Tutup proses edit berkas

	//Buka edit berkas
	public function berkas_unit_save_update()
	{
		
		$rva = $this->input->post('RetensiValue_Active');
		$rvi = $this->input->post('RetensiValue_InActive');
		$year = date('Y');
		$trva = $rva + $year;
		$trvi = $rva + $rvi + $year;

		$date = date('m-d');
		$date_rva = $trva.'-'.$date;
		$date_rvi = $trvi.'-'.$date;
		$id = $this->input->post('BerkasId');

		$tutup = $this->input->post('HitungId');
		$tanggal = date('Y-m-d');
		if($tutup == 'closed') {
			$x = '0000-00-00';
		} else {
			$x = $tanggal;
		}
		
		//Cek berkas di inbox
		if($this->db->query("SELECT BerkasId FROM inbox WHERE BerkasId = '".$this->input->post('BerkasId')."'")->num_rows() > 0)
  		{
  			$c1 = $this->input->post('Codeklas');
  			$k1 = $this->input->post('BerkasKlas');
  			$n1 = $this->input->post('Nomorberkas');
  		}
  		else 
  		{

			$tahun = date('Y');
			$nberkas = $this->db->query("SELECT (max(BerkasNumber) + 1) as BerkasNumber FROM berkas WHERE ClId='".$this->input->post('ClId')."' AND YEAR(CreationDate) = '".$tahun."' AND RoleId='".$_SESSION['roleid']."'")->row()->BerkasNumber;
			if (!empty($nberkas)){
				$id = $nberkas;
			} else {
			    $id = 1;
			}

  			$c1 = $this->input->post('ClId');
  			$k1 = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$this->input->post('ClId')."'")->row()->ClCode;

  			//Jika kode klas semula dengan kode klas yang dipilih sama maka eksekusi
  			if($this->input->post('Codeklas') == $this->input->post('ClId'))
  			{
  				$n1 = $this->input->post('Nomorberkas');
  			} else {
  				$n1 = $id;
  			}
  			
  		}

		$data = [
			'ClId' 						=> $c1,
			'Klasifikasi'				=> $k1,
			'BerkasNumber' 				=> $n1,
			'BerkasName' 				=> $this->input->post('BerkasName'),
			'RetensiValue_Active' 		=> $date_rva,
			'RetensiTipe_Active' 		=> $rva.'//',
			'RetensiValue_InActive' 	=> $date_rvi,
			'RetensiTipe_InActive' 		=> $rvi.'//',
			'Tutup_Berkas'		 		=> $x,
			'SusutId' 					=> $this->input->post('SusutId'),
			'BerkasLokasi' 				=> $this->input->post('BerkasLokasi'),
			'BerkasDesc' 				=> $this->input->post('BerkasDesc'),
			'CreatedBy' 				=> $this->session->userdata('peopleid'),
			'BerkasCountSince' 			=> $this->input->post('HitungId'),
		];

		$this->db->where('BerkasId',$this->input->post('BerkasId'));
		$this->db->update('berkas',$data);

		set_message('Data Berhasil Disimpan', 'success');
		redirect(BASE_URL('administrator/anri_berkas_unit'));	
	}
	//Tutup edit berkas

	public function berkas_unit_delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->berkas_unit_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->berkas_unit_remove($id);
			}
		}

		if ($remove) {
		    set_message('Data Berhasil Dihapus','success');
        } else {
            set_message(cclang('error_delete', 'berkas_unit'), 'error');
        }

		redirect_back();
	}

	//Buka hapus berkas
	private function berkas_unit_remove($id)
	{
		$berkas_unit = $this->db->where('BerkasId',$id)->delete('berkas');
		return $berkas_unit;
	}
	//Tutup hapus berkas

	public function getclassification($clid){

		$classification = $this->db->query("SELECT RetensiThn_Active, RetensiThn_InActive, SusutId FROM classification WHERE ClId='".$clid."' ")->result();

		$tahun = date('Y');
		$nberkas = $this->db->query("SELECT (max(BerkasNumber) + 1) as BerkasNumber FROM berkas WHERE ClId='".$clid."' AND YEAR(CreationDate) = '".$tahun."' AND RoleId='".$_SESSION['roleid']."'")->row()->BerkasNumber;
			if (!empty($nberkas)){
				$id = $nberkas;
			} else {
			    $id = 1;
			}
			
			$classification[] = ['BerkasNumber'=>$id];
		
		echo json_encode($classification);

	}

}
