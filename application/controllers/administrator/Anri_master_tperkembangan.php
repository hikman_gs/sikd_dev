<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_tperkembangan extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_tperkembangan');
	}

	// List tingkat perkembangan
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_tperkembangan/master_tperkembangan_list', $this->data);
	}
	// Tutup list tingkat perkembangan

	// Tambah data tingkat perkembangan
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_tperkembangan/master_tperkembangan_add', $this->data);
	}
	// Tutup tambah data tingkat perkembangan

	// Proses simpan data tingkat perkembangan
	public function add_save()
	{

		$this->form_validation->set_rules('TPName', 'TPName', 'trim|required|max_length[50]');
		

		if ($this->form_validation->run()) {
			$table = 'master_tperkembangan';

			$query = $this->db->query("SELECT (max(convert(substr(TPId, 12), UNSIGNED)) + 1) as id FROM master_tperkembangan")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

			$save_data = [
				'TPId' => tb_key().'.'.$id,
				'TPName' => $this->input->post('TPName'),
			];

			
			$save_master_tperkembangan = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_tperkembangan'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_tperkembangan'));			
		}

	}
	// Tutup proses simpan data tingkat perkembangan

	// Edit data tingkat perkembangan
	public function update($id)
	{
		$this->data['master_tperkembangan'] = $this->model_master_tperkembangan->find($id);
		$this->tempanri('backend/standart/administrator/master_tperkembangan/master_tperkembangan_update', $this->data);
	}
	// Tutup edit data tingkat perkembangan

	// Proses update data tingkat perkembangan
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('TPName', 'TPName', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'TPName' => $this->input->post('TPName'),
			];

			
			$save_master_tperkembangan = $this->model_master_tperkembangan->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_tperkembangan'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_tperkembangan'));			
		}

	}
	// Tutup proses update data tingkat perkembangan

	// Hapus data tingkat perkembangan
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_tperkembangan'));
	}
	// Tutup hapus data tingkat perkembangan

	// Proses hapus data tingkat perkembangan
	private function remove($id)
	{
		$master_tperkembangan = $this->db->where('TPId',$id)->delete('master_tperkembangan');
		return $master_tperkembangan;
	}
	// Tutup proses hapus data tingkat perkembangan
}