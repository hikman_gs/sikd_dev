<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_urgensi extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_urgensi');
	}

	// List tingkat urgensi
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_urgensi/master_urgensi_list', $this->data);
	}
	// Tutup list tingkat urgensi

	// Tambah data tingkat urgensi
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_urgensi/master_urgensi_add', $this->data);
	}
	// Tutup tambah data tingkat urgensi

	// Proses simpan data tingkat urgensi
	public function add_save()
	{

		$this->form_validation->set_rules('UrgensiName', 'UrgensiName', 'trim|required|max_length[50]');
		

		if ($this->form_validation->run()) {
			$table = 'master_urgensi';

			$query = $this->db->query("SELECT (max(convert(substr(UrgensiId, 12), UNSIGNED)) + 1) as id FROM master_urgensi")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

			$save_data = [
				'UrgensiId' => tb_key().'.'.$id,
				'UrgensiName' => $this->input->post('UrgensiName'),
			];

			
			$save_master_urgensi = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_urgensi'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_urgensi'));
			
		}

	}
	// Tutup proses simpan data tingkat urgensi

	// Edit data tingkat urgensi
	public function update($id)
	{
		$this->data['master_urgensi'] = $this->model_master_urgensi->find($id);
		$this->tempanri('backend/standart/administrator/master_urgensi/master_urgensi_update', $this->data);
	}
	// Tutup edit data tingkat urgensi

	// Proses update data tingkat urgensi
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('UrgensiName', 'UrgensiName', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'UrgensiName' => $this->input->post('UrgensiName'),
			];

			
			$save_master_urgensi = $this->model_master_urgensi->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_urgensi'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_urgensi'));			
		}

	}
	// Tutup proses update data tingkat urgensi

	// Hapus data tingkat urgensi
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}	

		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_urgensi'));							
	}
	// Tutup hapus data tingkat urgensi

	// Proses hapus data tingkat urgensi
	private function remove($id)
	{
		$master_urgensi = $this->db->where('UrgensiId',$id)->delete('master_urgensi');
		return $master_urgensi;
	}
	// Tutup proses hapus data tingkat urgensi
}