<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Integrasi_notifikasi_simulasi extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		
	}

	// List Bahasa
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/notifikasi_simulasi', $this->data);
	}
	// Tutup list bahasa
	 
	
	public function detail($nip) {
		

		$this->data['nip'] = $nip;
		
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/notifikasi_detail_simulasi', $this->data);
	}
	
	public function updateemail(){

        $data = array( 
			'Email' => $this->input->post('email')
		);
		$update = $this->db->update('people_simulasi', $data, array('PeopleUsername' => $this->input->post('nip')));
		if($update) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => TRUE));
    }
	
	public function updatepensiun(){

        $data = array( 
			'PeopleIsActive' => $this->input->post('kedudukan_pns')
		);
		$update = $this->db->update('people_simulasi', $data, array('PeopleUsername' => $this->input->post('nip')));
		if($update) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => TRUE));
    }
	
	public function updatestatus($nip){

		$datax = array(
			'status' => 1,
		);
		$this->db->update('siap_notif_simulasi', $datax, array('peg_nip' => $nip));
        redirect(base_url("administrator/integrasi_notifikasi"));
    }

	public function updateUnitKerja(){
		
		// ambil posisi terbaru berdasarkan uk_role param 
		$uk  = $this->db->query("SELECT * FROM mapping_per_pd WHERE unit_kerja_id = '".$this->input->post('unit_kerja_id')."'");
		$ukk = $uk->row(); 
		
		$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('PrimaryRoleId')."'");
		$role = $role1->row();
		
		/// Buat Update People
		$data = array( 
			'PrimaryRoleId'  => $role->RoleId,
			'PeoplePosition' => $role->RoleName,
			'RoleAtasan' => $role->RoleParentId, 
		);
		$updatex = $this->db->update('people_simulasi', $data, array('PeopleUsername' => $this->input->post('nip')));

		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$this->db->update('siap_notif_simulasi', $datax, array('peg_nip' => $this->input->post('nip')));
		
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => $ukk, "data" => $role, "data1" => $data));
    }
	
	
	public function updateAtasan(){
		
		// ambil posisi terbaru berdasarkan uk_role param 
		$uk  = $this->db->query("SELECT * FROM mapping_per_pd WHERE unit_kerja_id = '".$this->input->post('unit_kerja_id')."'");
		$ukk = $uk->row(); 
		
		/// Buat Update People
        $data = array(
			'RoleAtasan' => $ukk->RoleId
		);
		$updatex = $this->db->update('people_simulasi', $data, array('PeopleUsername' => $this->input->post('nip')));
		
		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$this->db->update('siap_notif_simulasi', $datax, array('peg_nip' => $this->input->post('nip')));
		
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => $ukk, "data" => $data));
    }
	
	
	public function updateNip(){

		/// Buat Update People
        $data = array( 
			'NIP' => $this->input->post('nip')
		);

		$updatex = $this->db->update('people_simulasi', $data, array('PeopleUsername' => $this->input->post('nip')));

		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$this->db->update('siap_notif_simulasi', $datax, array('peg_nip' => $this->input->post('nip')));
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => $x, "data" => $data));
    }
	
	public function updateNama(){

		/// Buat Update People
		$data = array( 
			'PeopleName' => $this->input->post('peg_nama'),
		);
		$updatex = $this->db->update('people_simulasi', $data, array('PeopleUsername' => $this->input->post('nip')));
		
		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$update = $this->db->update('siap_notif_simulasi', $datax, array('peg_nip' => $this->input->post('nip')));
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Data Berhasil diubah.</div>');
		}
		echo json_encode(array("status" => TRUE, "data" => $data));
    }
 
	public function updateJabatan(){

		// ambil posisi terbaru berdasarkan 
		$uk  = $this->db->query("SELECT * FROM mapping_per_pd WHERE unit_kerja_id = '".$this->input->post('unit_kerja_id')."'");
		$ukk = $uk->row(); 
		
		/// Buat Update People
		$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('PrimaryRoleId')."'");
		$role = $role1->row();
        $data = array( 
			'PrimaryRoleId'  => $this->input->post('PrimaryRoleId'),
			'PeoplePosition' => $role->RoleName
		);
		$updatepeople = $this->db->update('people_simulasi', $data, array('PeopleUsername' => $this->input->post('nip')));
		
		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$update = $this->db->update('siap_notif_simulasi', $datax, array('peg_nip' => $this->input->post('nip')));
		if($updatepeople) { 
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Data Berhasil diubah.</div>');
		}
		
		echo json_encode(array("status" => TRUE, "data" => $data));
	}

}