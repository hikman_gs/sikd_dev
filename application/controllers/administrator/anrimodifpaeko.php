<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_naskah_belum_approve extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_naskah_ba');
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inbox');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_ttd');
	}

	//List NASKAH MASUK BELUM APPROVE
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['title'] = 'Naskah Belum Disetujui';
		$this->tempanri('backend/standart/administrator/mail_tl/naskah_belum_approve', $this->data);
	}
	//List NASKAH MASUK BELUM APPROVE

	//Ambil Data Seluruh Naskah Belum Disetujui
	public function get_data_naskah_ba()
	{			
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_ba->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = date('d-m-Y',strtotime($field->TglReg));
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$field->JenisId."'")->row()->JenisName;
			
			$row[] = $field->Hal;
 
          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	

			$row[] = $z21;
//Tambahan kemsos
			//Cek no surat dulu, jika nomor surat beri ada maka tolah keluarkan pesan
			$konsep123 = $this->db->query("SELECT Number FROM konsep_naskah WHERE NId_Temp = '".$field->NId_Temp."' AND GIR_Id = '".$field->GIR_Id."'")->row();

			if($konsep123->Number == '0') {

				$row[] = '<button type="button" onclick=ko_nomor('.$field->NId_Temp.','.$field->GIR_Id.') title="Masukkan Nomor Naskah" class="btn btn-danger btn-sm"><i class="fa fa-sticky-note-o"></i></button> <button type="button" onclick=ko_verifikasi('.$field->NId_Temp.','.$field->GIR_Id.') title="Masukkan Kode Verifikasi" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId_Temp).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_teruskan('.$field->NId_Temp.','.$field->GIR_Id.') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';

			} else {

				$row[] = '<button type="button" onclick=ko_nomor('.$field->NId_Temp.','.$field->GIR_Id.') title="Masukkan Nomor Naskah" class="btn btn-danger btn-sm"><i class="fa fa-sticky-note-o"></i></button> <button type="button" onclick=ko_verifikasi('.$field->NId_Temp.','.$field->GIR_Id.') title="Masukkan Kode Verifikasi" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId_Temp).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';

			}	

			$row[] = '<a href="'.site_url('administrator/anri_list_naskah_belum_approve/edit_naskahdinas/'.$field->NId_Temp).'" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/'.$field->NId_Temp).'" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_ba->count_all(),
			"recordsFiltered" => $this->model_list_naskah_ba->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Belum Disetujui

	//PDF naskah dinas tindaklanjut
	public function naskahdinas_tindaklanjut_pdf($NId)
	{
		$this->load->library('HtmlPdf');
      	ob_start();

      	$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$NId."' LIMIT 1")->row();

      	$this->data['NId'] 				= $NId;
      	$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
      	$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;
      	$this->data['RoleId_To'] 		= $naskah->RoleId_To;
      	$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
      	$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Alamat']			= $naskah->Alamat;
		$this->data['Unit_pengolah']	= $naskah->Up;
		$this->data['Number']			= $naskah->Number;
		
		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$naskah->SifatId."'")->row()->SifatName;
	      	$this->data['Jumlah'] 			= $naskah->Jumlah;
	      	$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$naskah->MeasureUnitId."'")->row()->MeasureUnitName;			
      		$this->data['Hal'] 				= $naskah->Hal;	      	
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}

      	$this->data['TglReg'] 			= $naskah->TglReg;
      	$this->data['Approve_People'] 	= $naskah->Approve_People;
      	$this->data['TtdText'] 			= $naskah->TtdText;
      	$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
      	$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$naskah->ClId."'")->row()->ClCode;
		
	    if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxsprint'){ 
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxundangan'){ 
			$this->load->view('backend/standart/administrator/pdf/view_konsep_undangan_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxkeluar'){ 
        	$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_dinas_tindaklanjut_pdf',$this->data);
		}else {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_naskah_dinas_tindaklanjut_pdf',$this->data);
		}

	    $content = ob_get_contents();
	    ob_end_clean();
        
          $config = array(
            'orientation' 	=> 'p',
            'format' 		=> 'F4',
            'marges' 		=> array(30, 30, 20, 30),
        );

       


        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('naskah_dinas_tindaklanjut-'.$NId.'.pdf', 'H');

	}
	//Akhir PDF naskah dinas tindaklanjut	

	//Naskah Dinas Edit
	public function edit_naskahdinas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/nota_dinas/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Edit naskah dinas
	public function postedit_notadinas($NId)
	{
		$gir = $NId;
		$tanggal = date('Y-m-d H:i:s');

      	$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '".$NId."' LIMIT 1")->row();

		if ($naskah1->Ket != 'outboxsprint') {
			$sifat =  $this->input->post('SifatId');
		} else {
			$sifat =  NULL;		
		}

		if ($naskah1->Ket == 'outboxkeluar') {
			$alamat =  $this->input->post('Alamat');
			$role = $this->input->post('RoleId_To');
		} else {
			$alamat =  NULL;	
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',',$this->input->post('RoleId_To')) : '');	
		}

		try{
				$CI =& get_instance();
				$this->load->helper('string');

				$data_konsep = [
					'Jumlah'       		=> $this->input->post('Jumlah'),
					'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
					'UrgensiId'       	=> $this->input->post('UrgensiId'),
					'SifatId'       	=> $sifat,
					'Alamat'       		=> $alamat,
					'ClId'       		=> $this->input->post('ClId'),
					'RoleId_To'       	=> $role,
					'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
					'Konten'       		=> $this->input->post('Konten'),
					'Hal'       		=> $this->input->post('Hal'),
				];

				$this->db->where('NId_Temp', $NId);
        		$this->db->update('konsep_naskah', $data_konsep);

				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
							FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;

						$save_files = [
							'FileKey' 			=> tb_key(),
				            'GIR_Id' 			=> $gir,
				            'NId' 				=> $NId,
				            'PeopleID' 			=> $this->session->userdata('peopleid'),
				            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
				            'FileName_real' 	=> $file_name,
				            'FileName_fake' 	=> $test_title_name_copy,
				            'FileStatus' 		=> 'available',
				            'EditedDate'        => $tanggal,
				            'Keterangan' 		=> '',
						];
						$save_file = $this->model_inboxfile->store($save_files);
					}
				}

				set_message('Data Berhasil Disimpan', 'success');
				redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup Edit naskah dinas

	//hapus file notadinas registrasi
	public function hapus_file5($NId,$name_file)
	{

		$files = glob('./FilesUploaded/naskah/'.$name_file);
		foreach($files as $file){
			if(is_file($file))
				unlink($file);
		}

		$this->db->query("DELETE FROM inbox_files WHERE NId = '".$NId."' AND FileName_fake = '".$name_file."'");

		set_message('Data Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//tutup hapus file notadinas registrasi

	//Hapus nota dinas registrasi
	public function hapus_nota_dinas_tindaklanjut($NId)
	{
		$query1 	= $this->db->query("SELECT NFileDir FROM konsep_naskah WHERE NId_Temp = '".$NId."'");						
		foreach($query1->result_array() as $row1):
			$fileDir = $row1['NFileDir'];
		endforeach;	

		if($fileDir != ''){

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$NId."'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach($query->result_array() as $row):
				if(is_file($files.$row['FileName_fake']))
					unlink($files.$row['FileName_fake']);
			endforeach;		
		}

		$this->db->query("DELETE FROM `konsep_naskah` WHERE NId_Temp = '".$NId."'");
		$this->db->query("DELETE FROM `inbox_files` WHERE NId = '".$NId."'");

		redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));		

	}
	//Akhir Hapus nota dinas registrasi

	// Approve Naskah
		public function approve_naskah()
	{

		$NId = $this->input->post('NId_Temp');
		$GIR_Id = $this->input->post('GIR_Id');

		//user number identification
		$peopleid = $this->session->userdata('peopleid');

		//Tambahan kemsos
		//Cek no surat dulu, jika nomor surat beri ada maka tolah keluarkan pesan
		$konsep123 = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '".$NId."' AND GIR_Id = '".$GIR_Id."'")->row();

		if($konsep123->nosurat == '') {

			set_message('Nomor naskah wajib diisi, Silahkan berikan nomor naskah dahulu', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());	

		} else {

			$tanggal = date('Y-m-d H:i:s');

			$this->load->library('user_agent');
			get_data_konsepnaskah('NId_Temp',$NId);

			$password = $this->input->post('password');

			$kalimat = str_replace(' ', '', $password);
			
			if($kalimat == '') {
				set_message('Passphrase Wajib Diisi', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());	
			} else {

				//Proses cek NIK pada API Cloud BSRE
				$curl = curl_init();

				curl_setopt_array($curl, array(
				  CURLOPT_URL => "https://esign.kemsos.go.id/api/user/status/".$nik,
				  CURLOPT_RETURNTRANSFER => true,
				  CURLOPT_ENCODING => "",
				  CURLOPT_MAXREDIRS => 10,
				  CURLOPT_TIMEOUT => 0,
				  CURLOPT_FOLLOWLOCATION => true,
				  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				  CURLOPT_CUSTOMREQUEST => "GET",
				  CURLOPT_HTTPHEADER => array(
				    "Authorization: Basic ZXNpZ246cXdlcnR5",
				    "Cookie: JSESSIONID=175AA6E762B0B5629212359FF3F4F66A; BIGipServereSIGN_Pool=1785899200.20480.0000; TS011f12b2=0104ec0744a099832eb1c7bd8c95129402e02cd157cee33628361cb97dda55bc9e1789cdcbed8c23e0c45582dc7b29379ec9632adbedf61d8be59b863236d9d11e4c24a8e8e5aaf40a3210e3e46f62977ba82837f4; TS214caa54029=0887f8431bab28009726ea82f84761883252144f6f169d548b030a4aeb93de48febff10b2778c713845fecec3fcd5121"
				  ),
				));

				$response = curl_exec($curl);
				curl_close($curl);	

			    $result = json_decode($response);
			 
			    if($result->status_code == 1111)
			    {

			    	//Jika NIK terdaftar, Cek Passphrase
					//Proses cek Passphrase pada API Cloud BSRE

					//url api sertifikat elektronik
					$url_signed = 'https://esign.kemsos.go.id/api/sign/pdf';

					//passphrase for the certificate
					$passphrase = $this->input->post('password');
						
					//curl initialization
					$curl = curl_init();

					$asal = FCPATH.'FilesUploaded/example/example.pdf';

					curl_setopt_array($curl, array(
					  CURLOPT_URL => $url_signed,
					  CURLOPT_RETURNTRANSFER => true,
					  CURLOPT_ENCODING => "",
					  CURLOPT_MAXREDIRS => 10,
					  CURLOPT_TIMEOUT => 0,
					  CURLOPT_FOLLOWLOCATION => true,
					  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					  CURLOPT_CUSTOMREQUEST => "POST",
					  CURLOPT_POSTFIELDS => array('file'=> new CURLFILE($asal,'application/pdf'),'peopleid' => $peopleid,'passphrase' => $passphrase,'tampilan' => 'invisible','page' => '1','image' => 'false','linkQR' => 'https://google.com','xAxis' => '0','yAxis' => '0','width' => '200','height' => '100'),
					  CURLOPT_HTTPHEADER => array(
					    "Authorization: Basic ZXNpZ246cXdlcnR5",
					    "Cookie: JSESSIONID=29EF6FF0B6EC475975BEDAF09773E94C; BIGipServereSIGN_Pool=1785899200.20480.0000; TS011f12b2=0104ec0744a58e85495936bdc6c737df536410dcb34ad21387e04a8d3727131be6cf9e0f5d1f228340cb7eeae3a9fda281dcf289dac002b3202dc13b89978bc4153fc588677db7884526f6e9e37a8d47c9a4c8e917; TS214caa54029=0887f8431bab28008a5ee098747d05c06258082834dc968ca3d5c616fc44c99ff61e19fab26227d001f2b8bd9e6f3dff"
					  ),
					));

					$response = curl_exec($curl);
					//Fungsi untuk mengambil nilai http request
					$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE); 
					//===
					curl_close($curl);

				    $result = json_decode($response);

				    // $x = $httpCode;
				    // echo $x;
				    // die();
				 				 	
				    if($httpCode == '200')
				    {

				    	//Jika Passphrase Benar
				        $logopath = './uploads/anri.png'; //Logo yang akan di insert

						$CI =& get_instance();
						$this->load->library('ciqrcode');
						$this->load->helper('string');

				        $config['cacheable']    = true; //boolean, the default is true
				        $config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
				        $config['errorlog']     = './uploads/'; //string, the default is application/logs/
				        $config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
				        $config['quality']      = true; //boolean, the default is true
				        $config['size']         = '1024'; //interger, the default is 1024
				        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
				        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
				        $CI->ciqrcode->initialize($config);

						$image_name				= $NId.'-'.date('Ymd').'.png'; //buat name dari qr code sesuai dengan

				        $params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/').$NId; //data yang akan di jadikan QR CODE
				        $params['level'] 		= 'H'; //H=High
				        $params['size'] 		= 10;
				        $params['savename'] 	= FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
				        $CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

						$fullpath = FCPATH.$config['imagedir'].$image_name;

			            $QR = imagecreatefrompng($fullpath);
			             // memulai menggambar logo dalam file qrcode
			             $logo = imagecreatefromstring(file_get_contents($logopath));
			             imagecolortransparent($logo , imagecolorallocatealpha($logo , 0, 0, 0, 127));
			 
			             imagealphablending($logo , false);
			             imagesavealpha($logo , true);
			 
			             $QR_width = imagesx($QR);//get logo width
			             $QR_height = imagesy($QR);//get logo width
			 
			             $logo_width = imagesx($logo);
			             $logo_height = imagesy($logo);
			             
			            // Scale logo to fit in the QR Code
			             $logo_qr_width = $QR_width/4;
			             $scale = $logo_width/$logo_qr_width;
			             $logo_qr_height = $logo_height/$scale;

			             //220 -> left,  210 -> top
			             imagecopyresampled($QR, $logo, 220, 210, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);		 
			             // imagecopyresampled($QR, $logo, $QR_width/4, $QR_height/3.5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
			             // 
			             // Simpan kode QR lagi, dengan logo di atasnya
			             
			             imagepng($QR,$fullpath);

				        //Simpan QRCode
						$data_ttd = [
								'NId' 				=> $NId,
								'PeopleId' 			=> $this->session->userdata('peopleid'),
								'RoleId' 			=> $this->session->userdata('roleid'),
								'Verifikasi'  		=> random_string('sha1',10),
								'Identitas_Akses' 	=> '',
								'QRCode' 			=> $image_name,
								'TglProses' 		=> date('Y-m-d H:i:s'),
							];

						$save_ttd 		= $this->model_ttd->store($data_ttd);

						//Update Konsep = 0
						//Konsep 0 untuk sudah approve, 1 baru registrasi, 2 sudah dikirim
						$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Konsep' => 0 ]);

						//Create PDF File
						$this->load->library('HtmlPdf');
				      	ob_start();

				      	$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$NId."' LIMIT 1")->row();

				      	$this->data['NId'] 				= $NId;
				      	$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
				      	$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;
				      	$this->data['RoleId_To'] 		= $naskah->RoleId_To;
				      	$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
				      	$this->data['Konten'] 			= $naskah->Konten;

						if ($naskah->Ket != 'outboxsprint') {
							$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$naskah->SifatId."'")->row()->SifatName;
					      	$this->data['Jumlah'] 			= $naskah->Jumlah;
					      	$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$naskah->MeasureUnitId."'")->row()->MeasureUnitName;			
				      		$this->data['Hal'] 				= $naskah->Hal;	      	
						}

						if ($naskah->Ket == 'outboxkeluar') {
							$this->data['Alamat'] 			= $naskah->Alamat;
						}
			
				      	$this->data['TglNaskah'] 		= $naskah->TglNaskah;
				      	$this->data['Approve_People'] 	= $naskah->Approve_People;
				      	$this->data['TtdText'] 			= $naskah->TtdText;
				      	$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
				      	$this->data['Number'] 			= $naskah->Number;
				      	$this->data['nosurat'] 			= $naskah->nosurat;
				      	$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$naskah->ClId."'")->row()->ClCode;
				      	$this->data['lokasi'] 			= $naskah->lokasi;
						
					    if ($naskah->Ket == 'outboxnotadinas') {
							$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf',$this->data);
						}else if($naskah->Ket == 'outboxsprint'){ 
							$this->load->view('backend/standart/administrator/mail_tl/surattugas_inisiatif_pdf',$this->data);
						}else if($naskah->Ket == 'outboxundangan'){ 
							$this->load->view('backend/standart/administrator/mail_tl/undangan_inisiatif_pdf',$this->data);
						}else if($naskah->Ket == 'outboxkeluar'){ 
							$this->load->view('backend/standart/administrator/mail_tl/suratdinas_inisiatif_pdf',$this->data);
						}else {
							$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf',$this->data);
						}

					    $content = ob_get_contents();
					    ob_end_clean();
				        
				        $config = array(
				            'orientation' 	=> 'p',
				            'format' 		=> 'a4',
				            'marges' 		=> array(25, 5, 25, 15),
				        );


				        $this->pdf = new HtmlPdf($config);
				        $this->pdf->initialize($config);
				        $this->pdf->pdf->SetDisplayMode('fullpage');
				        $this->pdf->writeHTML($content);

				        if ($naskah->Ket == 'outboxnotadinas') {
				        	$a = 'nota_dinas-'.$NId.'.pdf';
				        	$this->pdf->Output('FilesUploaded/naskah/nota_dinas-'.$NId.'.pdf', 'F');
						}else if($naskah->Ket == 'outboxsprint'){ 
							$a = 'sprint-'.$NId.'.pdf';
							$this->pdf->Output('FilesUploaded/naskah/sprint-'.$NId.'.pdf', 'F');
						}else if($naskah->Ket == 'outboxundangan'){ 
							$a = 'undangan-'.$NId.'.pdf';
							$this->pdf->Output('FilesUploaded/naskah/undangan-'.$NId.'.pdf', 'F');
						}else if($naskah->Ket == 'outboxkeluar'){ 
							$a = 'surat_dinas-'.$NId.'.pdf';
							$this->pdf->Output('FilesUploaded/naskah/surat_dinas-'.$NId.'.pdf', 'F');	
						}else {
							$a = 'nadin_lain-'.$NId.'.pdf';
							$this->pdf->Output('FilesUploaded/naskah/nadin_lain-'.$NId.'.pdf', 'F');
						}
							
							//Simpan Inbox File
							$save_files = [
							'FileKey' 			=> tb_key(),
				            'GIR_Id' 			=> $GIR_Id,
				            'NId' 				=> $NId,
				            'PeopleID' 			=> $this->session->userdata('peopleid'),
				            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
				            'FileName_real' 	=> $a,
				            'FileName_fake' 	=> $a,
				            'FileStatus' 		=> 'available',
				            'EditedDate'        => $tanggal,
				            'Keterangan' 		=> 'outbox',
						];

						$save_file = $this->model_inboxfile->store($save_files);

						//Update Status
						$this->db->where('NId',$NId);
						$this->db->where('RoleId_To',$this->session->userdata('roleid'));
						$this->db->update('inbox_receiver',['Status' => 1, 'StatusReceive' => 'read']);
						
						//============= Sertifikat Elektronik Menggunakan Esign-Client-Gateway PHP
						//url api sertifikat elektronik
						$url_signed = 'https://esign.kemsos.go.id/api/sign/pdf';

						//loads a PDF file 
				 	 	if ($naskah->Ket == 'outboxnotadinas') {
				 	    	$asal = FCPATH.'FilesUploaded/naskah/nota_dinas-'.$NId.'.pdf';
						}else if($naskah->Ket == 'outboxsprint'){ 
							$asal = FCPATH.'FilesUploaded/naskah/sprint-'.$NId.'.pdf';
						}else if($naskah->Ket == 'outboxundangan'){ 
							$asal = FCPATH.'FilesUploaded/naskah/undangan-'.$NId.'.pdf';
						}else if($naskah->Ket == 'outboxkeluar'){ 
							$asal = FCPATH.'FilesUploaded/naskah/surat_dinas-'.$NId.'.pdf';
						}else {
							$asal = FCPATH.'FilesUploaded/naskah/nadin_lain-'.$NId.'.pdf';
						}		

						//passphrase for the certificate
						$passphrase = $this->input->post('password');

						//save pdf
				 	 	if ($naskah->Ket == 'outboxnotadinas') {
				 	    	$tujuan = FCPATH.'FilesUploaded/naskah/nota_dinas-'.$NId.'.pdf';
						}else if($naskah->Ket == 'outboxsprint'){ 
							$tujuan = FCPATH.'FilesUploaded/naskah/sprint-'.$NId.'.pdf';
						}else if($naskah->Ket == 'outboxundangan'){ 
							$tujuan = FCPATH.'FilesUploaded/naskah/undangan-'.$NId.'.pdf';
						}else if($naskah->Ket == 'outboxkeluar'){ 
							$tujuan = FCPATH.'FilesUploaded/naskah/surat_dinas-'.$NId.'.pdf';
						}else {
							$tujuan = FCPATH.'FilesUploaded/naskah/nadin_lain-'.$NId.'.pdf';
						}		
									
						//curl initialization
						$curl = curl_init();

						curl_setopt_array($curl, array(
						  CURLOPT_URL => $url_signed,
						  CURLOPT_RETURNTRANSFER => true,
						  CURLOPT_ENCODING => "",
						  CURLOPT_MAXREDIRS => 10,
						  CURLOPT_TIMEOUT => 0,
						  CURLOPT_FOLLOWLOCATION => true,
						  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						  CURLOPT_CUSTOMREQUEST => "POST",
						  CURLOPT_POSTFIELDS => array('file'=> new CURLFILE($asal,'application/pdf'),'peopleid' => $peopleid,'passphrase' => $passphrase,'tampilan' => 'invisible','page' => '1','image' => 'false','linkQR' => 'https://google.com','xAxis' => '0','yAxis' => '0','width' => '200','height' => '100'),
						  CURLOPT_HTTPHEADER => array(
						    "Authorization: Basic ZXNpZ246cXdlcnR5",
		    				"Cookie: JSESSIONID=B352FE4645BF67B041FBA86DBDAF1A2A; BIGipServereSIGN_Pool=1785899200.20480.0000; TS011f12b2=0104ec0744c049fefae33ec5c4b4e2b8ad6a78a0d211986d539e8795105be8cb5b920e1f3e3ab953027df000c67034b1e75501045c468e29210e8f5976713ecc62113214dcb4c0432d0500c938a012e8d0e8ffac96; TS214caa54029=0887f8431bab280021a6995c67a54914f6ecb616e4baf22bbb74cc1ab9d03bba93fe1702d8319f0c2e9fea50014d6093"
						  ),
						));

						$response = curl_exec($curl);
						file_put_contents($tujuan, $response);
						curl_close($curl);
						//============= Akhir Sertifikat Elektronik Menggunakan Esign-Client-Gateway PHP

						set_message('Data Berhasil Disimpan', 'success');
						$this->load->library('user_agent');
						redirect($this->agent->referrer());	

				    }			     
				    else
				    {

				    	//Jika Passphrase Salah
			        	set_message('Passphrase Anda Salah', 'error');
						redirect($this->agent->referrer());	

					}
			    }			     
			    else
			    {	

					//Jika NIK Tidak Terdaftar
		        	set_message('peopleid Anda Belum Terdaftar', 'error');
					redirect($this->agent->referrer());				

				}					
			}		
		}
	}
	//Tutup Approve Naskah

	//Fungsi Teruskan Draft Naskah Dinas
	public function k_teruskan()
	{

		$nid = $this->input->post('NId_Temp2');
		$girid = $this->input->post('GIR_Id2');

		$gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;	
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}	

		if($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');		
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '".$nid."' ORDER BY TglNaskah DESC LIMIT 1")->row();

	    if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		}else if($naskah->Ket == 'outboxsprint'){ 
			$xxz = 'to_draft_sprint';
		}else if($naskah->Ket == 'outboxundangan'){ 
			$xxz = 'to_draft_undangan';
		}else if($naskah->Ket == 'outboxkeluar'){ 
			$xxz = 'to_draft_keluar';
		}else {
			$xxz = 'to_draft_nadin';
		}

		try{

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id',$girid)->update('konsep_naskah',['RoleCode' => $this->session->userdata('rolecode'),'RoleId_From' => $this->session->userdata('roleid'),'Approve_People' => $x,'TtdText' => $this->input->post('TtdText'), 'Nama_ttd_konsep' => $z]);

			//Update Status
			$this->db->where('NId',$nid);
			$this->db->where('RoleId_To',$this->session->userdata('roleid'));
			$this->db->update('inbox_receiver',['Status' => 1, 'StatusReceive' => 'read']);			
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
		
	}
	//Fungsi Teruskan Draft Naskah Dinas

	// Beri Nomor
	public function nomor_dong()
	{


		$NId = $this->input->post('NId_Temp3');
		$GIR_Id = $this->input->post('GIR_Id3');

		$nomor = $this->input->post('nonaskah');

		$tahun 	= date('Y');
		$bulan = date('m');

		$cari = $this->db->query("SELECT ClId FROM konsep_naskah WHERE NId_Temp = '".$NId."'")->row()->ClId;	
		    
		$klas = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$cari."'")->row()->ClCode;

		$unit = $this->db->query("SELECT RoleCode FROM role WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->RoleCode;
		
		$nosurat = $nomor.'/'.$unit.'/'.$klas.'/'.$bulan.'/'.$tahun;

		$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Number' => $nomor,'nosurat' => $nosurat]);
		
		set_message('Nomor Berhasil Disimpan', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());					
	

	}
	//Tutup Beri Nomor	

	//kirim ke tu
	public function k_teruskan_tu()
	{

		$nid = $this->input->post('NId_Temp2');
		$girid = $this->input->post('GIR_Id2');

		$gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

	

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '".$nid."' ORDER BY TglNaskah DESC LIMIT 1")->row();

	    if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		}else if($naskah->Ket == 'outboxsprint'){ 
			$xxz = 'to_draft_sprint';
		}else if($naskah->Ket == 'outboxundangan'){ 
			$xxz = 'to_draft_undangan';
		}else if($naskah->Ket == 'outboxkeluar'){ 
			$xxz = 'to_draft_keluar';
		}else {
			$xxz = 'to_draft_nadin';
		}

		try{

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id',$girid)->update('konsep_naskah',['RoleCode' => $this->session->userdata('rolecode'),'RoleId_From' => $this->session->userdata('roleid')]);

			//Update Status
			$this->db->where('NId',$nid);
			$this->db->where('RoleId_To',$this->session->userdata('roleid'));
			$this->db->update('inbox_receiver',['Status' => 1, 'StatusReceive' => 'read']);			
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
		
	}
	
}