<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// use NcJoes\OfficeConverter\OfficeConverter;
class Konsep_naskah_menu extends Admin	
{
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');
		
	}

	// Fungsi List Pengumuman
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->template->title('Naskah Template');

		$this->tempanri('backend/standart/administrator/master_list_naskah/menu_list_naskah', $this->data);
	}
	// Akhir Fungsi List Pengumuman

	public function create($jenis_surat = NULL)
	{
		// check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		$this->data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
		$this->data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();
		$this->data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
		$this->data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		$this->data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		$this->data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
		$this->data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		$this->data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();

		// var_dump($this->data['ClId']);

		// die();
		$this->data['title'] = 'Pencatatan Surat Tugas';
		$this->tempanri('backend/standart/administrator/master_list_naskah/surat_perintah_regis', $this->data);
	}	


	public function detail_pdf($value='')
	{
		$tanggal = date('Y-m-d H:i:s');
		$this->load->library('Pdf');

		$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$this->data['Hal'] 			= $this->input->post('Hal');
		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['RoleCode'] 	= $this->input->post('rolecode');

		if ($this->input->post('TtdText') == 'none') {
			$this->data['Approve_People']	= $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$this->data['Approve_People'] 	= $this->input->post('tmpid_atasan');
		} else {
			$this->data['Approve_People'] 	= $this->input->post('Approve_People');
		}

		//penambahan atas nama

		if($this->input->post('TtdText2') == 'AL') {
			$this->data['Approve_People3'] 	= $this->input->post('tmpid_atas_nama');	
		} else {
			$this->data['Approve_People3'] 	= $this->input->post('Approve_People3');
		}

		//


		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');
		//atas nama
		$this->data['TtdText2']     = $this->input->post('TtdText2');


		if ($this->input->post('TtdText') == 'none') {
			$this->data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}


		$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

		$this->db->select("*");
		$this->db->where_in('PeopleId',$this->data['RoleId_To']);
		$this->db->order_by('GroupId', 'ASC');
		$this->db->order_by('Golongan', 'DESC');
		$this->db->order_by('Eselon ', 'ASC');
		$data_perintah = $this->db->get('v_login')->result();

		// var_dump($data_perintah);
		// die();

		$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

		//tcpdf
		$age = $this->session->userdata('roleid') == 'uk.1';
		$xx = $this->session->userdata('roleid');
		
		
		$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td rowspan="7" width="60px" style="valign:bottom;">
				<br><br>
				<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
				<td width="180px" style="text-align: left; font-size:9;">'.get_data_people('RoleName', $this->data['Approve_People']).',</td>
			</tr>
			<tr nobr="true">
				<td><br></td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
				</td>
			</tr>
			<tr nobr="true">
				<td>'.$tujuan. '
				</td>
			</tr>
		</table>';


		$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
		$pdf->SetMargins(30, 30, 30);
		$pdf->SetAutoPageBreak(TRUE, 30);
		// 
		$pdf->AddPage('P', 'F4');

		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
		$pdf->Ln(10);
		$pdf->Cell(0,10,'SURAT PERINTAH',0,0,'C');
		$pdf->Ln(5);
		$pdf->Cell(0,10,'NOMOR : .........../'.$this->data['ClCode'].'/'.$this->data['RoleCode'],0,0,'C');
		$pdf->Ln(15);
		$pdf->Cell(30,5,'DASAR',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->writeHTMLCell(125, 5, '', '', $this->data['Hal'], '', 1, 0, true, 'J', true);
		$pdf->Cell(0,10,'MEMERINTAHKAN:',0,1,'C');
		$pdf->Ln(5);
		$pdf->Cell(30,5,'Kepada',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$i=0;
		foreach($data_perintah as $row){
			$i++;
			$pdf->SetX(65);
			$pdf->Cell(10,5,$i.'. ',0,0,'L');
			$pdf->SetX(75);
			$pdf->Cell(25,5,'Nama',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->MultiCell(95,5,$row->PeopleName,0,'L');
			$pdf->SetX(75);
			$pdf->Cell(25,5,'Pangkat',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->Cell(95,5,$row->Pangkat,0,1,'L');
			$pdf->SetX(75);
			$pdf->Cell(25,5,'NIP',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->Cell(95,5,$row->NIP,0,1,'L');
			$pdf->SetX(75);
			$pdf->Cell(25,5,'Jabatan',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$uc  = ucwords(strtolower($row->PeoplePosition));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			$pdf->MultiCell(85,5,$str,0,'L');
		}
		$pdf->Ln(10);
		$pdf->Cell(30,5,'Untuk',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->writeHTMLCell(90, 0, '', '', $this->data['Konten'], '', 1, 0, false, 'J', false);
		// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
		$pdf->Ln(10);
		$pdf->SetX(125);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Ditetapkan di ..............',0,1,'L');
		$pdf->SetX(120);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Pada tanggal ..................',0,1,'L');
		$pdf->Ln(5);
		if($this->data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
		}elseif($this->data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n '.get_data_people('RoleName',$this->data['Approve_People3'].','),0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
		}
		$pdf->Ln(5);
		$pdf->SetX(100);
		$pdf->MultiCell(90,10,'PEMERIKSA',0,'C');
		
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

		$pdf->Output('naskah_dinas_tindaklanjut'.$tanggal.'.pdf','I');
	}

	// Fungsi Simpan Pengumuman
	public function store()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->form_validation->set_rules('priority', 'priority', 'trim|required');
		$this->form_validation->set_rules('date_range', 'date_range', 'trim|required');
		$this->form_validation->set_rules('notice_name', 'notice_name', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('notice_description', 'notice_description', 'trim');
		
		if ($this->form_validation->run() == FALSE) {
			$this->tempanri('backend/standart/administrator/master_notice/master_notice_create');
		}else{

			if (!empty($date_range = $this->input->post('date_range'))) {
				$pecah = explode('-', $date_range);
				$tgl_awal = date("Y-m-d", strtotime($pecah[0]));
				$tgl_akhir = date("Y-m-d", strtotime($pecah[1]));
			}

			$data = [
				'user_id' => $this->session->userdata('peopleid'),
				'user_name' => $this->session->userdata('peopleusername'),
				'notice_name' => $this->input->post('notice_name'),
				'notice_description' => $this->input->post('notice_description'),
				'priority' => $this->input->post('priority'),
				'tgl_awal' => $tgl_awal,
				'tgl_akhir' => $tgl_akhir,
			];

			$save_notice = $this->db->insert('master_notice',$data);

			if ($save_notice ) {
				set_message('Data Berhasil Disimpan','success');
				redirect(BASE_URL('administrator/notice'));
			}else{
				set_message('Gagal Menyimpan Data', 'error');
				redirect(BASE_URL('administrator/notice'));
			}


		}
	}
	// Akhir Fungsi Simpan Klasifikasi
	// 
	// 
	public function edit($id)
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->template->title('Edit Pengumuman');
		$data['notice'] = $this->db->get_where('master_notice', ['notice_id' => $id])->row_array();

		// die();
		if (!empty($data['notice']['tgl_awal'])) {
			$data['notice']['tgl_awal'] = date("m/d/Y", strtotime($data['notice']['tgl_awal']));
		}else{
			$data['notice']['tgl_awal'] = "00/00/0000";
		}

		if (!empty($data['notice']['tgl_akhir'])) {
			$data['notice']['tgl_akhir'] = date("m/d/Y", strtotime($data['notice']['tgl_akhir']));
		}else{
			$data['notice']['tgl_akhir'] = "00/00/0000";
		}

		$this->tempanri('backend/standart/administrator/master_notice/master_notice_edit', $data);
	}	
	
	// Buka edit data klasifikasi
	public function update()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$id = $_POST['id'];

		$this->form_validation->set_rules('priority', 'priority', 'trim|required');
		$this->form_validation->set_rules('date_range', 'date_range', 'trim|required');
		$this->form_validation->set_rules('notice_name', 'notice_name', 'trim|required|max_length[150]');
		$this->form_validation->set_rules('notice_description', 'notice_description', 'trim');
		
		if ($this->form_validation->run() == FALSE) {
			$this->tempanri('backend/standart/administrator/master_notice/master_notice_edit');
		}
		else
		{
			if (!empty($date_range = $this->input->post('date_range'))) {
				$pecah = explode('-', $date_range);
				$tgl_awal = date("Y-m-d", strtotime($pecah[0]));
				$tgl_akhir = date("Y-m-d", strtotime($pecah[1]));
			}

			$data = [
				'user_id' => $this->session->userdata('peopleid'),
				'user_name' => $this->session->userdata('peopleusername'),
				'notice_name' => $this->input->post('notice_name'),
				'notice_description' => $this->input->post('notice_description'),
				'priority' => $this->input->post('priority'),
				'tgl_awal' => $tgl_awal,
				'tgl_akhir' => $tgl_akhir
			];

				// cek exist
			$exist = $this->db->query("SELECT * FROM master_notice WHERE notice_id = ".$id)->num_rows();

			if ($exist > 0) {
				$this->db->where('notice_id', $id);
				$update_notice = $this->db->update('master_notice', $data);

				if ($update_notice ) {
					set_message('Data Berhasil diubah','success');
					redirect(BASE_URL('administrator/notice'));
				}else{
					set_message('Gagal Merubah Data', 'error');
					redirect(BASE_URL('administrator/notice'));
				}
			}else{
				set_message('Gagal ID tidak tersedia', 'error');
				redirect(BASE_URL('administrator/notice'));
			}


		}

	}
	// Tutup edit data klasifikasi

	// Buka hapus klasifikasi
	public function delete($id=null)
	{

		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		// check exist id
		$exist = $this->db->query("SELECT * FROM master_notice WHERE notice_id = ".$id)->num_rows();
		// exist
		if($exist > 0) {
			$delete = $this->db->delete('master_notice', array('notice_id' => $id));

			if ($delete) {
				set_message('Data Berhasil Dihapus','success');
			}else{
				set_message('Data Gagal Dihapus','error');
			}
			
		}else{

			set_message('id not found','error');

		}

		redirect(BASE_URL('administrator/notice'));
	}
	// Tutup hapus klasifikasi
	// 
	// 
	function get_ajax() {
		
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$list = $this->Model_notice->get_datatables();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $notice) {
			$no++;
			$row = array();
			$row[] = $no.".";
			$row[] = $notice->notice_name;
			switch ($notice->priority) {
				case 1:
				$row[] = '<span class="badge bg-green">New Feature</span>';
				break;
				case 2:
				$row[] = '<span class="badge bg-yellow">Maintenance</span>';
				break;
				case 3:
				$row[] = '<span class="badge bg-blue">Update</span>';
				break;
				default:
				$row[] = 'none';
			}
			$row[] = $notice->notice_description;
			$row[] = $notice->tgl_awal;
			$row[] = $notice->tgl_akhir;

            // add html for action
			$row[] = '<a href="'. BASE_URL('administrator/notice/edit/'.$notice->notice_id).'" title="Hapus Data" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;'.'<a href="'. BASE_URL('administrator/notice/delete/'.$notice->notice_id).'" title="Hapus Data" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';

			$data[] = $row;
		}
		$output = array(
			"draw" => @$_POST['draw'],
			"recordsTotal" => $this->Model_notice->count_all(),
			"recordsFiltered" => $this->Model_notice->count_filtered(),
			"data" => $data,
		);
        // output to json format
		echo json_encode($output);
	}




public function test_word()
{
	
	$this->load->library('Phpword');


	// $this->template->title('Tambah Pengumuman');

		// $this->tempanri('backend/standart/administrator/master_notice/master_notice_create');

	// $phpWord = new \PhpOffice\PhpWord\PhpWord();
	$template = new \PhpOffice\PhpWord\TemplateProcessor('uploads/test123.docx');
	$template->setValue('test', 'uhuy');
	$filename = 'test123ubah.docx';
	$template->save('uploads/'.$filename);
	/* Note: any element you append to a document must reside inside of a Section. */

		// Adding an empty Section to the document...
			// $section = $phpWord->addSection();
		// Adding Text element to the Section having font styled by default...
			// $section->addText(
			// 	'"Learn from yesterday, live for today, hope for tomorrow. '
			// 	. 'The important thing is not to stop questioning." '
			// 	. '(Albert Einstein)'
			// );

		/*
		 * Note: it's possible to customize font style of the Text element you add in three ways:
		 * - inline;
		 * - using named font style (new font style object will be implicitly created);
		 * - using explicitly created font style object.
		 */

		// Adding Text element with font customized inline...
		// $section->addText(
		// 	'"Hikman Ganda Sasmita, '
		// 	. 'and is never the result of selfishness." '
		// 	. '(Napoleon Hill)',
		// 	array('name' => 'Tahoma', 'size' => 10)
		// );

		// Adding Text element with font customized using named font style...
		// $fontStyleName = 'oneUserDefinedStyle';
		// $phpWord->addFontStyle(
		// 	$fontStyleName,
		// 	array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
		// );
		// $section->addText(
		// 	'"The greatest accomplishment is not in never falling, '
		// 	. 'but in rising again after you fall." '
		// 	. '(Vince Lombardi)',
		// 	$fontStyleName
		// );

		// Adding Text element with font customized using explicitly created font style object...
		// $fontStyle = new \PhpOffice\PhpWord\Style\Font();
		// $fontStyle->setBold(true);
		// $fontStyle->setName('Tahoma');
		// $fontStyle->setSize(13);
		// $myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Hikman Ganda Sasmita)');
		// $myTextElement->setFontStyle($fontStyle);

		// Saving the document as OOXML file...
		// $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		// $objWriter->save('helloWorld.docx');

		// $objWriter->save('uploads/'.$filename);
		// send results to browser to download
		// header('Content-Description: File Transfer');
		// header('Content-Type: application/octet-stream');
		// header('Content-Disposition: attachment; filename='.$filename);
		// header('Content-Transfer-Encoding: binary');
		// header('Expires: 0');
		// header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		// header('Pragma: public');
		// header('Content-Length: ' . filesize($filename));
		// flush();
		// readfile('uploads/'.$filename);
		// unlink('uploads/'.$filename); // deletes the temporary file
		exit;
	}
}