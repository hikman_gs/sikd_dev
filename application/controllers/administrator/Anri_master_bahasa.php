<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_bahasa extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
				
		$this->load->model('model_master_bahasa');
	}

	// List Bahasa
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_bahasa/master_bahasa_list', $this->data);
	}
	// Tutup list bahasa

	// Tambah data bahasa
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_bahasa/master_bahasa_add', $this->data);
	}
	// Tutup tambah data bahasa

	// Proses simpan data bahasa
	public function add_save()
	{
		$this->form_validation->set_rules('LangName', 'LangName', 'trim|required|max_length[20]');
		

		if ($this->form_validation->run()) {
			$table = 'master_bahasa';

			$query = $this->db->query("SELECT (max(convert(substr(LangId, 12), UNSIGNED)) + 1) as id FROM master_bahasa")->row()->id;


			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

			$save_data = [
				'LangId' => tb_key().'.'.$id,
				'LangName' => $this->input->post('LangName'),
			];

			
			$save_master_bahasa = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');	
			redirect(BASE_URL('administrator/anri_master_bahasa'));
		} else {
			set_message('Gagal Menyimpan Data','error');
			redirect(BASE_URL('administrator/anri_master_bahasa'));
		}
	}
	// Tutup proses simpan data bahasa

	// Edit data bahasa
	public function update($id)
	{
		$this->data['master_bahasa'] = $this->model_master_bahasa->find($id);
		$this->tempanri('backend/standart/administrator/master_bahasa/master_bahasa_update', $this->data);
	}
	// Tutup edit data bahasa

	// Proses update data bahasa
	public function update_save($id)
	{
		$this->form_validation->set_rules('LangName', 'LangName', 'trim|required|max_length[20]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'LangName' => $this->input->post('LangName'),
			];

			
			$save_master_bahasa = $this->model_master_bahasa->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_bahasa'));
		} else {
			set_message('Gagal Menyimpan Data','error');
			redirect(BASE_URL('administrator/anri_master_bahasa'));
		}

	}
	// Tutup proses update data bahasa

	// Hapus data bahasa
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
		    set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_bahasa'));
	}
	// Tutup hapus data bahasa

	// Proses hapus data bahasa
	private function remove($id)
	{
		$master_bahasa = $this->db->where('LangId',$id)->delete('master_bahasa');
		return $master_bahasa;
	}
	// Tutup proses hapus data bahasa
}