<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_sifat extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_sifat');
	}

	// List sifat arsip
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_sifat/master_sifat_list', $this->data);
	}
	// Tutup list sifat arsip

	// Tambah data sifat arsip
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_sifat/master_sifat_add', $this->data);
	}
	// Tutup tambah data sifat arsip

	// Proses simpan data sifat arsip
	public function add_save()
	{

		$this->form_validation->set_rules('SifatName', 'SifatName', 'trim|required|max_length[25]');
		$this->form_validation->set_rules('KodeSifat', 'KodeSifat', 'trim|required|max_length[5]');
		

		if ($this->form_validation->run()) {
			$table = 'master_sifat';

			$query = $this->db->query("SELECT (max(convert(substr(SifatId, 12), UNSIGNED)) + 1) as id FROM master_sifat")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

			$save_data = [
				'SifatId' => tb_key().'.'.$id,
				'SifatName' => $this->input->post('SifatName'),
				'KodeSifat' => $this->input->post('KodeSifat'),
			];

			
			$save_master_sifat = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_sifat'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_sifat'));			
		}

	}
	// Tutup proses simpan data sifat arsip

	// Edit data sifat arsip
	public function update($id)
	{
		$this->data['master_sifat'] = $this->model_master_sifat->find($id);
		$this->tempanri('backend/standart/administrator/master_sifat/master_sifat_update', $this->data);
	}
	// Tutup edit data sifat arsip

	// Proses update data sifat arsip
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('SifatName', 'SifatName', 'trim|required|max_length[25]');
		$this->form_validation->set_rules('KodeSifat', 'KodeSifat', 'trim|required|max_length[5]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'SifatName' => $this->input->post('SifatName'),
				'KodeSifat' => $this->input->post('KodeSifat'),
			];

			
			$save_master_sifat = $this->model_master_sifat->change($id, $save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_sifat'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_sifat'));			
		}

	}
	// Tutup proses update data sifat arsip

	// Hapus data sifat arsip
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
            set_message('Data Berhasil Disimpan','success');
        } else {
            set_message('Data Berhasil Dihapus','success');
        }

		redirect(BASE_URL('administrator/anri_master_sifat'));
	}
	// Tutup hapus data sifat arsip

	// Proses hapus data sifat arsip
	private function remove($id)
	{
		$master_sifat = $this->db->where('SifatId',$id)->delete('master_sifat');
		return $master_sifat;
	}
	// Tutup proses hapus data sifat arsip
}