<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_disposisi extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_disposisi');
	}

	//NASKAH MASUK DISPOSISI
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Daftar Semua Disposisi';
		$this->tempanri('backend/standart/administrator/disposisi/list', $this->data);
	}
	//TUTUP NASKAH MASUK DISPOSISI
			
	//Ambil Data Semua Disposisi
	public function get_data_disposisi()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_disposisi->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;

			if ($field->StatusReceive=='unread') {
				$xx1 = "<b>".date('d-m-Y',strtotime($field->ReceiveDate))."</b>";

				$xx3 = "<b>".$this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName."</b>";
				$xx4 = "<b>".$this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_To."'")->row()->RoleName."</b>";	

				$xx6 = "<b>".$this->db->query("SELECT Hal FROM inbox WHERE NId='".$field->NId."'")->row()->Hal."</b>";

				$count_disposisi  = $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."'")->num_rows();
				if($count_disposisi > 0){
				$disposisi  = $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."'")->result();
					foreach ($disposisi as $key => $value) {					  
					  $exp = explode('|', $value->Disposisi);
					  if($value->Disposisi == '-'){
					  	$z1 = '';
					  } else {
					  	  $z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
						  	foreach ($exp as $k => $v) {
						  		$z1 .= "<tr>";
						    	$z1 .= "<td><b> - ".$this->db->query("SELECT DisposisiName FROM master_disposisi WHERE DisposisiId = '".$v."'")->row()->DisposisiName."</b></td>";
						    	$z1 .= "</tr>";
						  	}
						  $z1 .= "</table>";	
					  }			  
					}
				}
			} else {
				$xx1 = date('d-m-Y',strtotime($field->ReceiveDate));
				$xx3 = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName;
				$xx4 = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_To."'")->row()->RoleName;	

				$xx6 = $this->db->query("SELECT Hal FROM inbox WHERE NId='".$field->NId."'")->row()->Hal;

				$count_disposisi  = $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."'")->num_rows();
				if($count_disposisi > 0){
				$disposisi  = $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."'")->result();
					foreach ($disposisi as $key => $value) {					  
					  $exp = explode('|', $value->Disposisi);
					  if($value->Disposisi == '-'){
					  	$z1 = '';
					  } else {
					  	  $z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
						  	foreach ($exp as $k => $v) {
						  		$z1 .= "<tr>";
						    	$z1 .= "<td> - ".$this->db->query("SELECT DisposisiName FROM master_disposisi WHERE DisposisiId = '".$v."'")->row()->DisposisiName."</td>";
						    	$z1 .= "</tr>";
						  	}
						  $z1 .= "</table>";	
					  }			  
					}
				}
			}

			$row[] = $xx1;
			$row[] = $xx3;
			$row[] = $xx4;
			$row[] = $xx6;
			$row[] = $z1;

			$row[] = '<a href="'.site_url('administrator/anri_mail_tl/view_naskah_masuk/view/'.$field->NId.'?dt=4').'" title="Lihat Detail Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_disposisi->count_all(),
			"recordsFiltered" => $this->model_list_disposisi->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Semua Disposisi
}