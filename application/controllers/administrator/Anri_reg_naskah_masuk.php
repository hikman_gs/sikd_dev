<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_reg_naskah_masuk extends Admin	
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_inbox');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');		
	}

	// List pengguna
	public function index()
	{
		//check_access($this->uri->segment(2));
		
		//model_custome di running di autoload -> application/config/autoload.php ->autoload model
		$this->data['people'] = $this->model_custome->get_people();
		$this->tempanri('backend/standart/administrator/reg_naskah_masuk/inbox_add', $this->data);
	}
	//Tutup list pengguna

	// Simpan naskah masuk
	public function naskah_masuk_add_save()
	{	
		$nid = $this->session->userdata('peopleid').date('dmyhis');
		$gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try{
				$this->load->helper('string'); 
				$save_data = [
					'NKey'         		=> tb_key(),
					'NId'				=> $nid,
					'CreatedBy'			=> $this->session->userdata('peopleid'),
					'CreationRoleId'	=> $this->session->userdata('roleid'),
					'NTglReg' 			=> $tanggal,
					'Tgl' 				=> date('Y-m-d',strtotime($this->input->post('Tgl'))),
					'JenisId' 			=> $this->input->post('JenisId'),
					//penambahan
					'JenisIdg' 			=> $this->input->post('JenisIdg'),
					//akhirpenambahan
					'Nomor' 			=> $this->input->post('Nomor'),
					'NAgenda' 			=> $this->input->post('NAgenda'),
					'Hal' 				=> $this->input->post('Hal'),
					'Instansipengirim'	=> $this->input->post('Instansipengirim'),
					'Pengirim'			=> 'eksternal',
					'UrgensiId' 		=> $this->input->post('UrgensiId'),
					'SifatId' 			=> $this->input->post('SifatId'),
					'NTipe' 			=> 'inbox',
					'NFileDir'			=> 'naskah',
					'BerkasId'			=> '1',
				];

				$save_inbox = $this->model_inbox->store($save_data);
				if (count((array) $this->input->post('test_title_name'))) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
							FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;

						$save_files = [
							'FileKey' 			=> tb_key(),
				            'GIR_Id' 			=> $gir,
				            'NId' 				=> $nid,
				            'PeopleID' 			=> $this->session->userdata('peopleid'),
				            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
				            'FileName_real' 	=> $file_name,
				            'FileName_fake' 	=> $test_title_name_copy,
				            'FileStatus' 		=> 'available',
				            'EditedDate'        => $tanggal,
				            'Keterangan' 		=> '',
						];

						$save_file = $this->model_inboxfile->store($save_files);

					}
				}
				if (count((array) $this->input->post('Kepada_Yth'))) {
					foreach ($_POST['Kepada_Yth'] as $receiver) {
						$save_recievers = [
							'NId' 			=> $nid,
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $gir,
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $receiver,
							'RoleId_To' 	=> get_data_people('RoleId',$receiver),
							'ReceiverAs' 	=> 'to',
							'Msg' 			=> '',
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> get_data_people('RoleDesc',$receiver),
							//penambahan
							'JenisIdg' 		=> $this->input->post('JenisIdg'),
							//akhirpenambahan
						];

						$save_reciever = $this->model_inboxreciever->store($save_recievers);
					}
				}

				if (count((array) $this->input->post('Tembusan'))) {
					foreach ($_POST['Tembusan'] as $tembusan) {
						$data_tembusan = [
							'NId' 			=> $nid,
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $gir,
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $tembusan,
							'RoleId_To' 	=> get_data_people('RoleId',$tembusan),
							'ReceiverAs' 	=> 'bcc',
							'Msg' 			=> '',
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> get_data_people('RoleDesc',$tembusan),
							//penambahan
							'JenisIdg' 		=> $this->input->post('JenisIdg'),
							//akhirpenambahan
						];

						$simpan_reciever = $this->model_inboxreciever->store($data_tembusan);
					}
				}

				set_message('Data Berhasil Disimpan', 'success');
				redirect(BASE_URL('administrator/anri_log_naskah_masuk'));
		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_reg_naskah_masuk'));
		}
	}
	// Tutup Simpan naskah masuk

	// Upload naskah masuk
	public function upload_naskah_masuk_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'inbox_files',
		]);
	}
	// Tutup upload naskah masuk

	// Hapus file naskah masuk
	public function delete_naskah_masuk_file($uuid)
	{

		$dirname = BASE_URL.'/uploads/tmp/'.$uuid;	

		if(rmdir($dirname))
        {
          echo ("$dirname successfully removed");
        }
        else
        {
          echo ("$dirname couldn't be removed"); 
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => $this->input->get('previewLink'), 
            'field_name'        => 'title', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'test',
            'primary_key'       => 'id_test',
            'upload_path'       => 'uploads/test/'
        ]);
	}
	// Tutup Hapus file naskah masuk

	// Edit naskah masuk
	public function edit_naskah_masuk($tipe,$NId)
	{

		$this->data['tipe'] 	= $tipe;
		$this->data['people'] 	= $this->model_custome->get_people();
		$this->data['data']   	= $this->model_inbox->find($NId);
		$this->data['NId']   	= $NId;
		
		$this->tempanri('backend/standart/administrator/reg_naskah_masuk/inbox_edit', $this->data);
	}
	// Tutup Edit naskah masuk
	
	// Buka Hapus file Inbox
	public function hapus_file2($NId,$name_file)
	{

		$query1 	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '".$NId."'");						
		foreach($query1->result_array() as $row1):
			$fileDir = $row1['NFileDir'];
		endforeach;	

		if($fileDir != ''){

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$NId."' AND FileName_fake = '".$name_file."'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach($query->result_array() as $row):
				if(is_file($files.$row['FileName_fake']))
					unlink($files.$row['FileName_fake']);
			endforeach;		
		}	


		$this->db->query("DELETE FROM inbox_files WHERE NId = '".$NId."' AND FileName_fake = '".$name_file."'");

		set_message('Data Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	// Tutup Hapus file inbox

	// Edit naskah masuk
	public function post_naskah_masuk_edit($tipe,$NId)
	{	
		$x = $this->input->post('xdt');
		$tanggal = date('Y-m-d H:i:s');

		try{
			$this->load->helper('string'); 
			$inb = $this->model_inbox->find($NId);
			$save_data = [
				'Tgl' 				=> date('Y-m-d',strtotime($this->input->post('Tgl'))),
				'JenisId' 			=> $this->input->post('JenisId'),
				'Nomor' 			=> $this->input->post('Nomor'),
				'NAgenda' 			=> $this->input->post('NAgenda'),
				'Hal' 				=> $this->input->post('Hal'),
				'Instansipengirim' 	=> $this->input->post('Instansipengirim'),
				'UrgensiId' 		=> $this->input->post('UrgensiId'),
				'SifatId' 			=> $this->input->post('SifatId'),
			];

			$this->db->where('NId',$NId);
			$this->db->update('inbox',$save_data);
			
			if (count((array) $this->input->post('test_title_name'))) {
				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);


					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
			            'GIR_Id' 			=> $NId,
			            'NId' 				=> $NId,
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);

				}
			}

			set_message('Data Berhasil Disimpan', 'success');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_metadata/log/'.$NId.'?dt='.$x));
			}else{
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/'.$NId.'?dt='.$x));
			}

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_metadata/log/'.$NId.'?dt='.$x));
			}else{
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/'.$NId.'?dt='.$x));
			}
		}
	}
	// Tutup Edit naskah masuk
	
	// Hapus tujuan naskah masuk
	public function hapus_tujuan($tipe,$NId,$RoleId_To,$GIR_Id)
	{
		try {
			$this->db->query("DELETE FROM `inbox_receiver` WHERE NId = '".$NId."' AND GIR_Id = '".$GIR_Id."' AND RoleId_To ='".$RoleId_To."'");
			set_message('Data Berhasil Dihapus', 'success');
			$this->load->library('user_agent');
		redirect($this->agent->referrer());		
		}catch(Exception $e) {
			set_message('Gagal Menghapus Data', 'error');
			$this->load->library('user_agent');
		}
	}
	// Tutup Hapus tujuan naskah masuk
	
	// Edit tujuan naskah masuk
	public function edit_tujuan($tipe,$NId)
	{
		$this->data['tipe'] 	= $tipe;
		$this->data['NId'] 		= $NId;
		$this->data['people'] 	= $this->model_custome->get_people();
		$this->tempanri('backend/standart/administrator/log_registrasi/tujuan', $this->data);
	}
	// Tutup Edit tujuan naskah masuk

	// Simpan tujuan naskah masuk
	public function post_tambah_tujuan($tipe,$NId)
	{
		$GIR_Id = $this->db->query("SELECT GIR_Id FROM inbox_receiver WHERE NId ='".$NId."' ORDER BY ReceiveDate ASC ")->row();

		$gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try{
			if (count((array) $this->input->post('Kepada_Yth'))) {
				foreach ($_POST['Kepada_Yth'] as $receiver) {
					$save_recievers = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> (empty($GIR_Id) ? $gir : $GIR_Id->GIR_Id),
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $receiver,
						'RoleId_To' 	=> get_data_people('RoleId',$receiver),
						'ReceiverAs' 	=> 'to',
						'Msg' 			=> '',
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc',$receiver),
					];

					$save_reciever = $this->model_inboxreciever->store($save_recievers);
				}
			}

			if (count((array) $this->input->post('Tembusan'))) {
				foreach ($_POST['Tembusan'] as $tembusan) {
					$data_tembusan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> (empty($GIR_Id) ? $gir : $GIR_Id->GIR_Id),
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tembusan,
						'RoleId_To' 	=> get_data_people('RoleId',$tembusan),
						'ReceiverAs' 	=> 'bcc',
						'Msg' 			=> '',
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc',$tembusan),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tembusan);
				}
			}

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_reg_naskah_masuk/edit_tujuan/log/'.$NId));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_reg_naskah_masuk/edit_tujuan/log/'.$NId));
		}
	}
	// Tutup Simpan tujuan naskah masuk

}