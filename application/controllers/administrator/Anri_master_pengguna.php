<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Anri_master_pengguna extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('Model_list_people');
	}

	// List pengguna
	public function index()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->tempanri('backend/standart/administrator/people/people_list', $this->data);
	}
	//Tutup list pengguna

	// Ajax tampil pengguna
	public function ajax_list_people()
	{
		$list = $this->Model_list_people->get_datatables_people();
		$data = array();
		foreach ($list as $a) {
			$row = array();
			$row[] = '<input type="checkbox" class="flat-red check" name="id[]" value="' . $a->PeopleId . '">';
			$row[] = $a->PeopleName;
			$row[] = $a->RoleDesc;
			$row[] = $a->PeoplePosition;
			$row[] = $a->PeopleUsername;

			if ($a->PeopleIsActive == 1) {
				$row[] = 'Aktif';
			} else {
				$row[] = "<font color=red><b>" . 'Tidak Aktif' . "</b></font>";
			}


			$btnhapus = '';
			if (cek_data('inbox_receiver', 'To_id', $a->PeopleId) == '0') {
				$btnhapus = '<a href="' . BASE_URL('administrator/anri_master_pengguna/delete/' . $a->PeopleId) . '" title="Hapus Data" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			}

			$row[] = '<a href="' . BASE_URL('administrator/anri_master_pengguna/reset/' . $a->PeopleId) . '" title="Kembalikan Kata Sandi" class="btn btn-sm btn-warning"><i class="fa fa-key"></i></a> <a href="' . BASE_URL('administrator/anri_master_pengguna/update/' . $a->PeopleId) . '" title="Ubah Data" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a> ' . $btnhapus;

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Model_list_people->count_all(),
			"recordsFiltered" => $this->Model_list_people->count_filtered(),
			"data" => $data,
		);
		echo json_encode($output);
	}
	// Tutup ajax tampil pengguna

	// Reset Password
	public function reset($id)
	{
		$save_data = [
			'PeoplePassword' => sha1('Simanisjuara20!'),
		];
		$save_people = $this->db->where(array('PeopleKey' => tb_key(), 'PeopleId' => $id))->update('people', $save_data);
		set_message('password berhasil direset', 'success');
		redirect(BASE_URL('administrator/anri_master_pengguna'));
	}
	// Tutup reset password

	// Edit data pengguna
	public function update($id)
	{
		$this->template->title('people Edit');
		$this->tempanri('backend/standart/administrator/people/people_update', $this->data);
	}
	// Tutup edit data pengguna

	// Proses update data pengguna
	public function update_save()
	{
		$this->form_validation->set_rules('PeopleName', 'PeopleName', 'trim|required|max_length[150]');

		if ($this->form_validation->run()) {
			$id = $this->input->post('PeopleId');
			if (!empty($this->input->post('PeoplePassword'))) {
				$save_data = [
					'GroupId' => $this->input->post('GroupId'),
					'PrimaryRoleId' => $this->input->post('RoleId'),
					'RoleAtasan' => $this->input->post('RoleAtasan'),
					'PeopleName' => $this->input->post('PeopleName'),
					'PeoplePosition' => $this->input->post('PeoplePosition'),
					'NIP' => $this->input->post('NIP'),
					'NIK' => $this->input->post('NIK'),
					'ApprovelName' => $this->input->post('ApprovelName'),
					'Email' => $this->input->post('Email'),
					'PeopleIsActive' => $this->input->post('PeopleIsActive'),
					'PeopleUsername' => $this->input->post('PeopleUsername'),
					'PeoplePassword' => sha1($this->input->post('PeoplePassword')),
				];
			} else {
				$save_data = [
					'GroupId' => $this->input->post('GroupId'),
					'PrimaryRoleId' => $this->input->post('RoleId'),
					'RoleAtasan' => $this->input->post('RoleAtasan'),
					'PeopleName' => $this->input->post('PeopleName'),
					'PeoplePosition' => $this->input->post('PeoplePosition'),
					'NIP' => $this->input->post('NIP'),
					'NIK' => $this->input->post('NIK'),
					'ApprovelName' => $this->input->post('ApprovelName'),
					'Email' => $this->input->post('Email'),
					'PeopleIsActive' => $this->input->post('PeopleIsActive'),
				];
			}

			$save_people = $this->db->where(array('PeopleKey' => tb_key(), 'PeopleId' => $id))->update('people', $save_data);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_master_pengguna'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_pengguna'));
		}
	}
	// Tutup Proses update data pengguna

	// Proses simpan data pengguna
	public function add_save()
	{
		$this->form_validation->set_rules('PeopleKey', 'PeopleKey', 'trim|required|max_length[15]');


		if ($this->form_validation->run()) {
			$table = 'people';
			$save_data = [
				'PeopleKey' => tb_key(),
				'GroupId' => $this->input->post('GroupId'),
				'PrimaryRoleId' => $this->input->post('RoleId'),
				'RoleAtasan' => $this->input->post('RoleAtasan'),
				'PeopleName' => $this->input->post('PeopleName'),
				'PeoplePosition' => $this->input->post('PeoplePosition'),
				'NIP' => $this->input->post('NIP'),
				'ApprovelName' => $this->input->post('ApprovelName'),
				'Email' => $this->input->post('Email'),
				'NIK' => $this->input->post('NIK'),
				'PeopleIsActive' => $this->input->post('PeopleIsActive'),
				'PeopleUsername' => $this->input->post('PeopleUsername'),
				'PeoplePassword' => sha1($this->input->post('PeoplePassword')),
			];


			$save_people = $this->db->insert($table, $save_data);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_master_pengguna'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_pengguna'));
		}
	}
	// Tutup proses simpan data pengguna

	// Hapus data pengguna
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) > 0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
			set_message('Data Berhasil Dihapus', 'success');
		} else {
			set_message('Gagal Menghapus Data', 'error');
		}

		redirect(BASE_URL('administrator/anri_master_pengguna'));
	}
	// Tutup Hapus data pengguna

	// Proses hapus data pengguna
	private function remove($id)
	{
		$people = $this->db->where('PeopleId', $id)->delete('people');
		return $people;
	}
	// Tutup Proses hapus data pengguna

}
