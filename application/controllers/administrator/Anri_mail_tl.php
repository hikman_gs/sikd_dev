<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Anri_mail_tl extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inbox');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inbox_disposisi');
		$this->load->model('model_inbox_koreksi');
		$this->load->model('model_ttd');
		$this->load->model('model_inboxreciever_koreksi');
	}

	//Buka iframe history
	public function iframe_histori($NId)
	{
		$this->data['NId'] = $NId;
		$this->load->view('backend/standart/administrator/mail_tl/table_histori', $this->data);
	}
	//Tutup iframe history

	//Buka iframe history
	public function iframe_jejak_histori($NId)
	{
		$this->data['NId'] = $NId;
		$this->load->view('backend/standart/administrator/mail_tl/table_jejak_histori', $this->data);
	}
	//Tutup iframe history

	//Buka View Naskah Masuk
	public function view_naskah_masuk($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND To_Id = '" . $this->session->userdata('peopleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('To_Id', $this->session->userdata('peopleid'));
		$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori', $this->data);
	}

		//Buka View Naskah Masuk
	public function view_naskah_keluar($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND To_Id = '" . $this->session->userdata('peopleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('To_Id', $this->session->userdata('peopleid'));
		$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_surat_keluar_dinas', $this->data);
	}

	public function view_naskah_draft_histori($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;


		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND To_Id = '" . $this->session->userdata('peopleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		$this->data['data_koreksi']	= $this->db->query("SELECT id_koreksi FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' AND RoleId_From = '" . $this->session->userdata('roleid') . "'LIMIT 1")->row();
		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('To_Id', $this->session->userdata('peopleid'));
		$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_draft_histori', $this->data);
	}
	// public function view_naskah_masuk($tipe, $NId)
	// {

	// 	$this->data['NId'] 		= $NId;
	// 	$this->data['tipe'] 	= $tipe;

	// 	$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

	// 	//UPDATE READ
	// 	$this->db->where('NId', $NId);
	// 	$this->db->where('RoleId_To', $this->session->userdata('roleid'));
	// 	$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

	// 	$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori', $this->data);
	// }
	//Tutup View Naskah Masuk

	//Buka iframe KOREKSI
	public function iframe_koreksi($NId, $GIR_Id)
	{
		$this->data['NId'] 			= $NId;
		$this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['Koreksi'] 	= $this->db->query("SELECT Koreksi FROM inbox_koreksi WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row('Koreksi');
		$this->data['Msg'] 			= $this->db->query("SELECT Msg FROM inbox_receiver_koreksi WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row()->Msg;

		$this->load->view('backend/standart/administrator/mail_tl/table_koreksi', $this->data);
	}
	//Tutup iframe KOREKSI




	// //Buka iframe disposisi
	// public function iframe_koreksi($NId, $GIR_Id)
	// {
	// 	$this->data['NId'] 			= $NId;
	// 	$this->data['GIR_Id'] 		= $GIR_Id;
	// 	$this->data['Koreksi'] 	= $this->db->query("SELECT Koreksi FROM inbox_koreksi WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row('Koreksi');
	// 	$this->data['Msg'] 			= $this->db->query("SELECT Msg FROM inbox_receiver WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row()->Msg;

	// 	$this->load->view('backend/standart/administrator/mail_tl/table_koreksi', $this->data);
	// }
	// //Tutup iframe disposisi


	//Buka view naskah histori KOREKSI
	public function view_naskah_histori_koreksi($tipe, $NId)
	{
		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		// $this->data['id_koreksi']= $id_koreksi;
		// $this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['data_koreksi']	= $this->db->query("SELECT id_koreksi FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' AND RoleId_From = '" . $this->session->userdata('roleid') . "'LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver_koreksi', ['StatusReceive' => 'read']);

		// $this->data['data_koreksi'] = $this->db->query("SELECT * FROM konsep_naskah_koreksi WHERE NId = '".$NId)->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_ukdispu', $this->data);
		// var_dump($this->session->userdata('roleid'));
		// die();
	}

	public function view_naskah_final_histori_koreksi($tipe, $NId)
	{
		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		// $this->data['id_koreksi']= $id_koreksi;
		// $this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['data_koreksi']	= $this->db->query("SELECT id_koreksi FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' AND RoleId_From = '" . $this->session->userdata('roleid') . "'LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver_koreksi', ['StatusReceive' => 'read']);

		// $this->data['data_koreksi'] = $this->db->query("SELECT * FROM konsep_naskah_koreksi WHERE NId = '".$NId)->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_final_histori_ukdispu', $this->data);
		// var_dump($this->session->userdata('roleid'));
		// die();
	}
	public function view_riwayat_peruser_koreksi($tipe, $NId)
	{
		$this->data['NId'] 			= $NId;


		$this->data['tipe'] 		= $tipe;
		// $this->data['id_koreksi']= $id_koreksi;
		// $this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['data_koreksi']	= $this->db->query("SELECT id_koreksi FROM inbox_receiver_koreksi WHERE GIR_Id = '" . $NId . "' AND RoleId_From = '" . $this->session->userdata('roleid') . "'LIMIT 1")->row();

		// var_dump($this->data['NId'] 	);
		// die();


		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver_koreksi WHERE GIR_Id = '" . $NId . "' AND From_Id = '" . $this->session->userdata('peopleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
	

		$this->db->where('From_Id', $this->session->userdata('peopleid'));
		$this->db->update('inbox_receiver_koreksi', ['StatusReceive' => 'read']);

		// $this->data['data_koreksi'] = $this->db->query("SELECT * FROM konsep_naskah_koreksi WHERE NId = '".$NId)->row();
		$this->tempanri('backend/standart/administrator/mail_tl/riwayat_user_koreksi', $this->data);
		// var_dump($this->session->userdata('roleid'));
		// die();
	}
	//Tutup view naskah histori KOREKSI

	// //Buka view naskah histori
	// public function view_naskah_histori_koreksi($tipe, $NId)
	// {
	// 	$this->data['NId'] 			= $NId;
	// 	$this->data['tipe'] 		= $tipe;
	// 	$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
	// 	$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_ukdispu', $this->data);
	// }
	// //Tutup view naskah histori

	//PENAMBAHAN KOREKSI
	public function view_naskah_uk($tipe, $NId)
	{
		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		// $this->data['id_koreksi'] 	= $id_koreksi;
		// $this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['data_koreksi']	= $this->db->query("SELECT id_koreksi FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' AND RoleId_From = '" . $this->session->userdata('roleid') . "'LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver_koreksi WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver_koreksi', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_ukdispu', $this->data);
	}
	//AKHIR PENAMBAHAN KOREKSI
	// public function view_naskah_uk($tipe, $NId)
	// {

	// 	$this->data['NId'] 		= $NId;
	// 	$this->data['tipe'] 	= $tipe;

	// 	$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

	// 	//UPDATE READ
	// 	$this->db->where('NId', $NId);
	// 	$this->db->where('RoleId_To', $this->session->userdata('roleid'));
	// 	$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

	// 	$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_ukdispu', $this->data);
	// }


	//tambahan dispu
	public function view_naskah_koreksi($tipe, $NId)
	{

		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_koreksi', $this->data);
	}
	//Tutup View Naskah disposisi

	//Buka selesai tindaklanut
	public function selesai_tindaklanjut($tipe, $NId)
	{
		$x = $_GET['dt'];
		//UPDATE STATUS
		$this->db->where('NId', $NId);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['Status' => 1]);

		set_message('Data Berhasil Disimpan', 'success');
		if ($tipe == 'log') {
			redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
		} else {
			redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId . '?dt=' . $x));
		}
	}
	//Tutup selesai tindaklanut

	//tambahan dispu
	//Buka selesai selesai_naskah
	public function selesai_naskah($tipe, $NId)
	{
		$x = $_GET['dt'];

		//UPDATE STATUS
		$this->db->where('NId', $NId);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['Status' => 1]);

		set_message('Data Berhasil Disimpan', 'success');
	
			$this->tempanri('backend/standart/administrator/mail_tl/list_sudin_kirim_keluar_btl', $this->data);
		
	}
	//Tutup selesai selesai_naskah
	//akhir tambahan dispu

	//Buka view naskah histori
	public function view_naskah_histori($tipe, $NId)
	{
		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori', $this->data);
	}
	//Tutup view naskah histori

	//Buka view metadata
	public function view_naskah_metadata($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox']	 = $this->db->query("SELECT NTglReg, JenisId, Tgl, Nomor, NAgenda, Hal, Instansipengirim, UrgensiId, SifatId FROM inbox WHERE NId = '" . $NId . "' LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_metadata', $this->data);
	}
	//Tutup view metadata

	public function view_naskah_metadata_koreksi($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox']	 = $this->db->query("SELECT NTglReg, JenisId, Tgl, Nomor, NAgenda, Hal, Instansipengirim, UrgensiId, SifatId FROM inbox WHERE NId = '" . $NId . "' LIMIT 1")->row();
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_metadata_koreksi', $this->data);
	}


	public function view_naskah_kirim_keluar($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND To_Id = '" . $this->session->userdata('peopleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('To_Id', $this->session->userdata('peopleid'));
		$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_kirim_keluar', $this->data);
	}

	//Buka view naskah dinas
	public function view_naskah_dinas($tipe, $NId)
	{
		$this->data['NId'] 			= $NId;
		$this->data['tipe'] 		= $tipe;
		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();
		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_naskah', $this->data);
	}
	//Buka view naskah dinas

	//Buka iframe disposisi
	public function iframe_disposisi($NId, $GIR_Id)
	{
		$this->data['NId'] 			= $NId;
		$this->data['GIR_Id'] 		= $GIR_Id;
		$this->data['Disposisi'] 	= $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row('Disposisi');
		$this->data['Msg'] 			= $this->db->query("SELECT Msg FROM inbox_receiver WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row()->Msg;

		$this->load->view('backend/standart/administrator/mail_tl/table_disposisi', $this->data);
	}
	//Tutup iframe disposisi


	//tambahan dispu
	public function view_naskah_disposisi($tipe, $NId)
	{

		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->data['data_inbox']	= $this->db->query("SELECT Status FROM inbox_receiver WHERE NId = '" . $NId . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ORDER BY ReceiveDate DESC LIMIT 1")->row();

		//UPDATE READ
		$this->db->where('NId', $NId);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['StatusReceive' => 'read']);

		$this->tempanri('backend/standart/administrator/mail_tl/inbox_view_histori_disposisi', $this->data);
	}
	//Tutup View Naskah disposisi

	//Buka hapus histori
	public function hapus_histori($tipe, $NId, $GIR_Id)
	{
		$x = $_GET['dt'];

		$query1	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '" . $NId . "'");

		foreach ($query1->result_array() as $row1) :
			$fileDir = $row1['NFileDir'];
		endforeach;

		if ($fileDir != '') {

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach ($query->result_array() as $row) :
				if (is_file($files . $row['FileName_fake']))
					unlink($files . $row['FileName_fake']);
			endforeach;
		}

		$this->db->query("DELETE FROM inbox_receiver WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'");

		$this->db->query("DELETE FROM inbox_disposisi WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'");

		$this->db->query("DELETE FROM inbox_files WHERE NId = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'");


		$cari = $this->db->query("SELECT GIR_Id FROM inbox_receiver WHERE NId = '" . $NId . "' AND ReceiverAs in ('to','to_notadinas','to_forward','cc1') ORDER BY ReceiveDate DESC LIMIT 0,1")->row();

		//UPDATE STATUS
		$this->db->where('NId', $NId);
		$this->db->where('GIR_Id', $cari->GIR_Id);
		$this->db->where('RoleId_To', $this->session->userdata('roleid'));
		$this->db->update('inbox_receiver', ['Status' => 0]);

		set_message('Data Berhasil Dihapus', 'success');
		if ($tipe == 'log') {
			redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
		} else {
			redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId . '?dt=' . $x));
		}
	}
	//Tutup hapus histori

	//View PDF Naskah Masuk
	public function log_naskah_masuk_pdf($id)
	{
		$this->load->library('HtmlPdf');
		ob_start();
		$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $id . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		//penambahan dispu
		$id_approve = $this->db->query("SELECT * FROM inbox_receiver WHERE NId = '" . $id . "' ORDER BY From_Id DESC LIMIT 1")->row();
		//akhir penambahan dispu
		$this->data['NId'] 				= $id;
		$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
		$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Jumlah'] 			= $naskah->Jumlah;
		$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
		$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		$this->data['TglNaskah'] 		= $naskah->TglNaskah;
		$this->data['Hal'] 				= $naskah->Hal;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['Approve_People3'] 	= $naskah->Approve_People3;

		//penambahan dispu
		$this->data['nama_approve'] 			= $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $id_approve->Nama_ttd_konsep . "'")->row()->RoleName;
		//akhir penambahan dispu

		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['Number'] 			= $naskah->Number;
		$this->data['nosurat'] 			= $naskah->nosurat;
		$this->data['lokasi'] 			= $naskah->lokasi;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->view('backend/standart/administrator/mail_tl/inbox_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxsprint') {
			$this->load->view('backend/standart/administrator/mail_tl/surattugas_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxundangan') {
			$this->load->view('backend/standart/administrator/mail_tl/undangan_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxkeluar') {
			$this->load->view('backend/standart/administrator/mail_tl/suratdinas_pdf', $this->data);
		} else {
			$this->load->view('backend/standart/administrator/mail_tl/inbox_pdf', $this->data);
		}

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 10),
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('konsep_naskah.pdf', 'H');
	}
	//Akhir View PDF Naskah Masuk						

	//View PDF Naskah Masuk
	public function log_naskah_masuk_pdf_log($id)
	{

		$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $id . "' ORDER BY TglNaskah DESC LIMIT 1")->row();
		//penambahan dispu
		$id_approve = $this->db->query("SELECT * FROM inbox_receiver WHERE NId = '" . $id . "' ORDER BY From_Id DESC LIMIT 1")->row();
		//akhir penambahan
		$this->data['NId'] 				= $id;
		$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
		$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['Nama_ttd_pengantar'] 		= $naskah->Nama_ttd_pengantar;
		$this->data['RoleId_To_2'] 		= $naskah->RoleId_To_2;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Alamat'] 			= $naskah->Alamat;
		$this->data['Hal'] 				= $naskah->Hal;
		$this->data['lampiran'] 		= $naskah->lampiran;
		$this->data['Hal_pengantar'] 	= $naskah->Hal_pengantar;
		$this->data['lampiran2'] 		= $naskah->lampiran2;
		$this->data['lampiran3'] 		= $naskah->lampiran3;
		$this->data['lampiran4'] 		= $naskah->lampiran4;
		$this->data['type_naskah'] 		= $naskah->type_naskah;

		if (!empty($naskah->SifatId)) {
			$this->data['SifatId'] 		= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
		}

		if (!empty($naskah->Jumlah)) {
			$this->data['Jumlah'] 		= $naskah->Jumlah;
		}

		if (!empty($naskah->MeasureUnitId)) {
			$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		}
		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 		= $naskah->Alamat;
		}

		$this->data['TglNaskah'] 		= $naskah->TglNaskah;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['Approve_People3'] 	= $naskah->Approve_People3;
		//penambahan dispu
		$this->data['nama_approve'] 	= $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $id_approve->RoleId_From . "'")->row()->RoleName;
		//akhir penambahan
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['Number'] 			= $naskah->Number;
		$this->data['nosurat'] 			= $naskah->nosurat;
		$this->data['lokasi'] 			= $naskah->lokasi;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;
		$this->data['RoleCode']			= $naskah->RoleCode;

		// if ($naskah->Ket == 'outboxnotadinas') {
		// 	$this->load->library('Pdf');
		// 	$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

		// 	$this->db->select("*");
		// 	$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
		// 	$this->db->order_by('GroupId', 'ASC');
		// 	$this->db->order_by('Golongan', 'DESC');
		// 	$this->db->order_by('Eselon ', 'ASC');
		// 	$data_nota_dinas = $this->db->get('v_login')->result();

		// 	$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
		// 	$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
		// 	//tcpdf

		// 	$r_atasan =  $this->session->userdata('roleatasan');
		// 	$r_biro =  $this->session->userdata('groleid');
		// 	$age = $this->session->userdata('roleid');
		// 	if ($this->data['TtdText'] == 'PLT') {
		// 		$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} elseif ($this->data['TtdText'] == 'PLH') {
		// 		$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
		// 		$tte = 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',' . '<br>' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	} else {
		// 		$tte = get_data_people('RoleName', $this->data['Approve_People']) . ',';
		// 	}
		// 	$dari = penulisan_dinas(get_data_people('RoleName', $this->data['Approve_People']));

		// 	$table = '<table style="font-size:10px;" cellpadding="1px" nobr="true">
		// 		<tr nobr="true">
		// 			<td width="240px" style="text-align:center; line-height:15px;">' . $tte . '</td>
		// 		</tr>
		// 	</table>
		// 	<br>
		// 	<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
		// 		<tr nobr="true">
		// 			<td rowspan="7" width="60px" style="valign:bottom;">
		// 			<br><br>
		// 			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
		// 			<td width="180px">Ditandatangani secara elekronik oleh:</td>
		// 		</tr>
		// 		<tr nobr="true">
		// 			<td width="180px" style="text-align: left; font-size:9;">' . $tte . '</td>
		// 		</tr>
		// 		<tr nobr="true">
		// 			<td><br></td>
		// 		</tr>
		// 		<tr nobr="true">
		// 			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '</td>
		// 		</tr>
		// 		<tr nobr="true">
		// 			<td>' . $pangkat_ttd . '
		// 			</td>
		// 		</tr>
		// 	</table>';

		// 	$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		// 	$pdf->SetPrintHeader(false);
		// 	$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
		// 	$pdf->SetMargins(30, 30, 20);
		// 	$pdf->SetAutoPageBreak(TRUE, 25);
		// 	// 
		// 	$pdf->AddPage('P', 'F4');
		// 	if ($this->session->userdata('roleid') == 'uk.1') {
		// 		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 10, 160, 0, 'PNG', '', 'N');
		// 		$pdf->Ln(10);
		// 	} else {
		// 		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
		// 	}
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(0, 10, 'NOTA DINAS', 0, 0, 'C');
		// 	$pdf->Ln(15);
		// 	// $pdf->Cell(0,10,'NOMOR : .........../'.$data['ClCode'].'/'.$data['RoleCode'],0,0,'C');
		// 	$pdf->SetFont('Arial', '', 10);
		// 	$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$n = 1;
		// 	$RoleId_To_decode = explode(',', $this->data['RoleId_To']);
		// 	$jum_roleid_to = count($RoleId_To_decode);

		// 	foreach ($RoleId_To_decode as $r => $v) {
		// 		$xx1 =  $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleID = '" . $v . "'AND GroupId  IN('3','4','7') ")->row();
		// 		$str = penulisan_dinas($xx1->PeoplePosition);
				
		// 		if ($jum_roleid_to > 1) {
		// 			$pdf->SetX(63);
		// 			$pdf->Cell(5, 0, $n . ".", 0, 0, 'L');
		// 			if ($xx1->GroupId = 7) {
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 		} else {
		// 			$pdf->SetX(63);
		// 			if ($xx1->GroupId = 7) {
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 		}
		// 		$n++;
		// 	}
		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Dari', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->MultiCell(125, 5, $dari, 0, 'L', 0, 2, '', '', true);
		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
		// 	if (!empty($this->data['RoleId_Cc'])) {
		// 		$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 		$k = 1;
		// 		$RoleId_Cc_decode = explode(',', $this->data['RoleId_Cc']);
		// 		$jum_roleid_cc = count($RoleId_Cc_decode);
		// 		foreach ($RoleId_Cc_decode as $c => $cc) {
		// 			$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $cc . "'")->row()->RoleName;
		// 			$str = penulisan_dinas($xx1);
					
		// 			if ($jum_roleid_cc > 1) {
		// 				$pdf->SetX(63);
		// 				$pdf->Cell(5, 0, $k . ".", 0, 0, 'L');
		// 				$pdf->MultiCell(120, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			} else {
		// 				$pdf->SetX(63);
		// 				$pdf->MultiCell(125, 0, $str, 0, 'L', 0, 2, '', '', true);
		// 			}
		// 			$k++;
		// 		}
		// 	} else {
		// 		$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 		$pdf->SetX(63);
		// 		$pdf->MultiCell(125, 0, '-', 0, 'L', 0, 2, '', '', true);
		// 	}

		// 	$pdf->Ln(0);
		// 	$pdf->Cell(30, 5, 'Tanggal', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, get_bulan($this->data['TglNaskah']), 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Nomor', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, $this->data['nosurat'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Sifat', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, $this->data['SifatId'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Lampiran', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->Cell(125, 5, $this->data['Jumlah'] . ' ' . $this->data['MeasureUnitId'], 0, 0, 'L');
		// 	$pdf->Ln(5);
		// 	$pdf->Cell(30, 5, 'Hal', 0, 0, 'L');
		// 	$pdf->Cell(3, 5, ':', 0, 0, 'L');
		// 	$pdf->MultiCell(125, 5, $this->data['Hal'], 0, '', 0, 1, '', '', true);
		// 	$complex_cell_border = array(
		// 		'B' => array('width' => 0.8, 'color' => array(0, 0, 0), 'dash' => '1', 'cap' => 'round'),
		// 	);
		// 	$pdf->Ln(0);
		// 	$pdf->Cell(160, 5, '', $complex_cell_border, 1, 'C', 0, '', 0);
		// 	$pdf->Ln(3);
		// 	// $pdf->MultiCell(160, 5, '', 'T', 'J', 0, 1, '' ,'', true);
		// 	$pdf->writeHTML($this->data['Konten'], true, 0, true, 0);
		// 	$pdf->Ln(10);
		// 	$pdf->SetX(100);
		// 	$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
		// 	if (!empty($naskah->lampiran)) {

		// 		$lampiran_decode = array_filter(json_decode($naskah->lampiran));

		// 		$no_lamp = 1;
		// 		$jum_lamp = count($lampiran_decode);

		// 		foreach ($lampiran_decode as $isilamp) {
		// 			switch ($no_lamp) {
		// 				case 1:
		// 					if ($jum_lamp > 1) {
		// 						$lamp_romawi = "I";
		// 					} else {
		// 						$lamp_romawi = "";
		// 					}
		// 					break;
		// 				case 2:
		// 					$lamp_romawi = "II";
		// 					break;
		// 				case 3:
		// 					$lamp_romawi = "III";
		// 					break;
		// 				case 4:
		// 					$lamp_romawi = "IV";
		// 					break;
		// 				case 5:
		// 					$lamp_romawi = "V";
		// 					break;
		// 				case 6:
		// 					$lamp_romawi = "VI";
		// 					break;
		// 				case 7:
		// 					$lamp_romawi = "VII";
		// 					break;
		// 				case 8:
		// 					$lamp_romawi = "VIII";
		// 					break;
		// 				case 9:
		// 					$lamp_romawi = "IX";
		// 					break;
		// 				case 10:
		// 					$lamp_romawi = "X";
		// 					break;
		// 				default:
		// 					$lamp_romawi = "";
		// 			}
		// 			$header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
		// 				<tr>
		// 			        <td width="65px">Lampiran ' . $lamp_romawi . ' : </td>
		// 			        <td colspan="2" width="200px">Nota Dinas ' . $dari . '</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td width="50px">NOMOR</td>
		// 			        <td width="10px">:</td>
		// 			        <td width="140px">' . $naskah->nosurat . '</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td>TANGGAL</td>
		// 			        <td>:</td>
		// 			        <td>' . get_bulan($this->data['TglNaskah']) . '</td>
		// 			    </tr>
		// 			    <tr>
		// 			        <td>&nbsp;</td>
		// 			        <td>PERIHAL</td>
		// 			        <td>:</td>
		// 			        <td>' . $this->data['Hal'] . '</td>
		// 			    </tr>
		// 			</table>';
		// 			# code...
		// 			$pdf->AddPage('P', 'F4');
		// 			$pdf->SetX(95);
		// 			$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);
		// 			$pdf->Ln(10);
		// 			$pdf->SetX(30);
		// 			$pdf->writeHTML($isilamp, true, 0, true, 0);
		// 			// $pdf->writeHTMLCell(161, 0, '', '', $isilamp, 0, 1, 0, true, '', true);
		// 			$pdf->MultiCell(90, 5, '', 0, 'C');

		// 			$pdf->Ln(2);
		// 			$pdf->Ln(5);
		// 			$pdf->SetX(100);
		// 			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
		// 			$no_lamp++;
		// 		}
		// 	}
		// 	$pdf->Output('surat_nota_dinas_view.pdf', 'I');
		// 	die();
		if ($naskah->Ket == 'outboxsprint') {
			$this->load->library('Pdf');
			// $this->load->view('backend/standart/administrator/mail_tl/surattugas_pdf', $this->data);
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_perintah = $this->db->get('v_login')->result();

			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;

			//tcpdf
			$r_atasan =  $this->session->userdata('roleatasan');
			$r_biro =  $this->session->userdata('groleid');

			$age = $this->session->userdata('roleid');
			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',' . '<br>' . get_data_people('RoleName', $this->data['Approve_People']) . ',';
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']) . ',';
			}

			$table = '<table style="font-size:12px; line-height:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<br>
			<td width="240px" style="text-align:center;">Ditetapkan di ' . $this->data['lokasi'] . '</td>
			</tr>
			<tr nobr="true">
			<td width="240px" style="text-align:center;">Pada tanggal ' . get_bulan($this->data['TglNaskah']) . '<br></td>
			</tr>
			<tr nobr="true">
			<td width="240px" style="text-align:center; line-height:15px;">' . $tte . '</td>
			</tr>
			</table>
			<br>
			<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . '</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $pangkat_ttd . '
			</td>
			</tr>
			</table>';

			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 25);
			// 
			$pdf->AddPage('P', 'F4');

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			$pdf->Ln(7);
			$pdf->Cell(0, 10, 'SURAT PERINTAH', 0, 0, 'C');
			$pdf->Ln(5);
			$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
			$pdf->Ln(12);
			$pdf->Cell(25, 5, 'DASAR', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(130, 3, '', '', $this->data['Hal'], '', 1, 0, true, 'J', true);
			$pdf->Ln(3);
			$pdf->Cell(0, 10, 'MEMERINTAHKAN:', 0, 1, 'C');
			$pdf->Ln(3);
			$pdf->Cell(25, 5, 'Kepada', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$i = 0;
			foreach ($data_perintah as $row) {
				$i++;
				$pdf->SetX(60);
				$pdf->Cell(5, 5, $i . '. ', 0, 0, 'L');
				$pdf->SetX(65);
				$pdf->Cell(40, 5, 'Nama', 0, 0, 'L');
				$pdf->Cell(3, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(76, 5, $row->PeopleName, 0, 'L');
				$pdf->SetX(65);
				$pdf->Cell(40, 5, 'Pangkat/Golongan', 0, 0, 'L');
				$pdf->Cell(3, 5, ':', 0, 0, 'L');
				$pdf->Cell(76, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
				$pdf->SetX(65);
				$pdf->Cell(40, 5, 'NIP', 0, 0, 'L');
				$pdf->Cell(3, 5, ':', 0, 0, 'L');
				$pdf->Cell(76, 5, $row->NIP, 0, 1, 'L');
				$pdf->SetX(65);
				$pdf->Cell(40, 5, 'Jabatan', 0, 0, 'L');
				$pdf->Cell(3, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				$pdf->MultiCell(76, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			}
			$pdf->Ln(5);
			$pdf->Cell(25, 5, 'Untuk', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(130, 0, '', '', $this->data['Konten'], '', 1, 0, false, 'J', false);
			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
			$pdf->Ln(3);

			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			$pdf->Output('konsep_naskah_surat_perintah.pdf', 'I');
			die();
		} else if ($naskah->Ket == 'outboxinstruksigub') {
			$this->load->library('Pdf');
			// ob_start();
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = 'uk.1' ")->row()->Header;
			$sql_gub = $this->db->query("SELECT PeopleName FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeopleName;
			$sql_namagub = $this->db->query("SELECT PeoplePosition FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeoplePosition;
			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');


			$table = '<table style="border: 1px solid black;font-size:9px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9px;">' . $sql_namagub . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:9px;">' . $sql_gub . '
			</td>
			</tr>
			<tr nobr="true">
			</tr>
			</table>';


			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 30);
			$pdf->AddPage('P', 'F4');

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 168, 0, 'PNG');
			$pdf->Ln(13);
			$pdf->SetX(38);
			$pdf->Cell(0, 10, 'INSTRUKSI GUBERNUR JAWA BARAT', 0, 0, 'C');
			$pdf->Ln(7);
			$pdf->SetX(38);
			$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(38);
			$pdf->Cell(0, 5, 'TENTANG', 0, 0, 'C');
			$pdf->Ln(8);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal'], 0, 1, 0, true, 'C', true);
			$pdf->Ln(7);
			$pdf->Cell(0, 10, 'GUBERNUR JAWA BARAT', 0, 1, 'C');
			$pdf->Ln(2);
			$pdf->SetX(30);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal_pengantar'], 0, 1, 0, true, 'J', true);
			$pdf->Ln(5);
			$pdf->Cell(55, 5, 'Dengan ini meninstruksikan', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Ln(7);
			$pdf->Cell(30, 5, 'Kepada', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');

			$i = 0;
			if (!empty($this->data['RoleId_To'])) {
				$RoleId_To_decode = explode(',', $this->data['RoleId_To']);


				foreach ($RoleId_To_decode as $lamp => $v) {
					$xx1 =  $this->db->query("SELECT PeoplePosition FROM v_login WHERE PeopleId = '" . $v . "'")->row()->PeoplePosition;

					$uc  = ucwords(strtolower($xx1));
					$str = str_replace('Dan', 'dan', $uc);
					$str = str_replace('Uptd', 'UPTD', $str);
					$str = str_replace('Dprd', 'DPRD', $str);

					$i++;
					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
					$pdf->SetX(70);
					$pdf->MultiCell(117, 5,  $str . "\n", 0, 'J', 0, 2, '', '', true);
				}
			}

			if (!empty($this->data['RoleId_To_2'])) {
				$RoleId_To_decode2 = json_decode($this->data['RoleId_To_2']);
				foreach ($RoleId_To_decode2 as $lamp => $v) {
					$i++;
					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');
					$pdf->SetX(70);
					$pdf->MultiCell(117, 5,  $v . "\n", 0, 'J', 0, 2, '', '', true);
				}
			}

			$pdf->Ln(2);
			$pdf->Cell(30, 5, 'Untuk', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Ln(8);


			if (!empty($this->data['Konten'])) {
				$Konten_decode = json_decode($this->data['Konten']);
				$jum_lamp = count($Konten_decode);
				$no_lamp = 1;

				foreach ($Konten_decode as $isi_konten) {
					switch ($no_lamp) {
						case 1:
							$lamp_romawi = "KESATU";
							break;
						case 2:
							$lamp_romawi =  "KEDUA";
							break;
						case 3:
							$lamp_romawi =  "KETIGA";
							break;
						case 4:
							$lamp_romawi =  "KEEMPAT";
							break;
						case 5:
							$lamp_romawi =  "KELIMA";
							break;
						case 6:
							$lamp_romawi =  "KEENAM";
							break;
						case 7:
							$lamp_romawi =  "KETUJUH";
							break;
						case 8:
							return "KEDELAPAN";
							break;
					}

					$pdf->Cell(30, 5, $lamp_romawi, 0, 0, 'L');
					$pdf->Cell(5, 5, ':', 0, 0, 'L');
					$pdf->SetX(65);
					$pdf->writeHTMLCell(125, 0, '', '', $isi_konten, 0, 1, 0, true, 'J', true);
					$pdf->Ln(3);
					$no_lamp++;
				}
			}
			$pdf->Ln(3);
			$pdf->Cell(30, 5, 'Instruksi ini mulai berlaku pada tanggal ditetapkan.', 0, 1, 'L');
			$pdf->Ln(10);
			$pdf->SetX(125);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Ditetapkan di ..............', 0, 1, 'L');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Pada tanggal ..................', 0, 1, 'L');
			$pdf->Ln(2);
			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, $sql_namagub . ',', 0, 'C');
			}
			$pdf->Ln(5);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('surat_instruksi_view.pdf', 'I');
			die();
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;
			$this->db->select("*");
			$this->db->where_in('PeopleId', $this->data['RoleId_To']);
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_perintah = $this->db->get('v_login')->result();

			$sql_gub = $this->db->query("SELECT PeopleName FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeopleName;
			$sql_namagub = $this->db->query("SELECT PeoplePosition FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeoplePosition;

			//tcpdf
			$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;
			$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
			$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			//tcpdf


			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeoplePosition = '" . $jabatan_ttd_konsep . "'")->row()->Pangkat;
			//tcpdf  

			if ($data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
					$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . $jabatan_ttd_konsep;
			} else {
				$tte = $jabatan_ttd_konsep;
			}


			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $pangkat_ttd . '
			</td>
			</tr>
			</table>';


			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');

			if ($this->session->userdata('roleid') == 'uk.1') {

				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 174, 0, 'PNG', '', 'N');
				$pdf->Ln(-3);
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(3);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'SURAT PERNYATAAN MELAKSANAKAN TUGAS', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->SetX(45);



			if (!empty($this->data['nosurat'])) {
				$no = $naskah->nosurat;
			} else {

				$no = '.........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'];
			}



			$pdf->Cell(0, 10, 'NOMOR :' .	$no, 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(30, 5, 'Yang bertandatangan di bawah ini :', 0, 0, 'L');

			$pdf->Ln(5);
			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->MultiCell(85, 6, get_data_people('PeopleName', $this->data['Nama_ttd_pengantar']) . ',', 0, 'L');
			// if($this->data['TtdText'] == 'PLT') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }elseif($this->data['TtdText'] == 'PLH') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }elseif($this->data['TtdText2'] == 'Atas_Nama') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');

			// }else{
			// 	$pdf->SetX(97);
			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }



			$variable_nip	 		= $this->data['Approve_People'];
			$variable_nip3 			= $this->data['Approve_People3'];
			$variable_nip_ttd 		= $this->data['Nama_ttd_pengantar'];

			$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;

			// if($data['TtdText'] == 'PLT') {




			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

			// }elseif($data['TtdText'] == 'PLH') {

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {

			// $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;
			// }else{

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;
			// }

			// $gol =$this->data['Approve_People'];
			$gol_ttd = $this->data['Nama_ttd_pengantar'];
			// $gol3 =$this->data['Approve_People3'];

			$golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			$pangkat_ttd_pengantar = $this->db->query("SELECT Pangkat FROM people WHERE PeopleId = '" .  $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			$jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->PeoplePosition;


			// if($data['TtdText'] == 'PLT') {




			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

			// }elseif($data['TtdText'] == 'PLH') {
			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {
			// $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;
			// }else{

			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;
			// }

			// $jab_ttd =$this->data['Approve_People'];
			// $jab_ttd3 =$this->data['Approve_People3'];

			// if($data['TtdText'] == 'PLT') {			


			// $jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

			// }elseif($data['TtdText'] == 'PLH') {
			// $jabatan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {
			// $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;
			// }else{

			// $jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;
			// }


			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');

			if($nipttd =='170592' ){
				$nipttd=' - ';				
			}else{				
				$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;
			}	

			$pdf->MultiCell(85, 6, $nipttd, 0, 'L');

			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Pangkat/Golongan', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');

			if(!empty($pangkat_ttd_pengantar )) {

				$pangkat_kosong=' / ';
			}else{
				$pangkat_kosong=' - ';
			}

			$pdf->Cell(85,5,$pangkat_ttd_pengantar.$pangkat_kosong.$golongan_ttd,0,1,'L');

			$pdf->SetX(46);
			$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
			$pdf->SetX(92);
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$uc  = ucwords(strtolower($jabatan_ttd));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			// $pdf->MultiCell(85,5,$str,0,'J');
			$pdf->MultiCell(84, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);





			$pdf->Ln(3);
			$pdf->Cell(30, 5, 'Dengan ini menerangkan dengan sesungguhnya bahwa :', 0, 0, 'L');
			$pdf->Ln(6);


			$i = 0;
			foreach ($data_perintah as $row) {
				$i++;


				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');

				$pdf->MultiCell(85, 6, $row->PeopleName, 0, 'L');


				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->NIP, 0, 1, 'L');

				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Pangkat/Golongan', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');

				$pdf->SetX(46);
				$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
				$pdf->SetX(92);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(84, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			}
			$pdf->Ln(6);

			// $pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan'.' '.$this->data['Hal'].' '.'Nomor'.' '.$row->NIP.'' .' '.'terhitung'.' '.$row->NIP.''.' '.'telah nyata menjalankan tugas sebagai'.' '.$row->NIP.''.' '.'di'.' '.$row->NIP.'' ,0, 1, 0, true, 'J', true);
			$pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan' . ' ' . $this->data['Hal'] . '.', 0, 1, 0, true, 'J', true);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Konten'] . '', 0, 1, 0, true, 'J', true);

			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
			$pdf->Ln(5);

			$pdf->writeHTMLCell(160, 0, '', '', '&nbsp;&nbsp;&nbsp;&nbsp;Demikian Surat Pernyataan Melaksanakan Tugas ini dibuat dengan sesungguhnya dengan mengingat sumpah jabatan/pegawai negeri sipil dan apabila dikemudian hari isi surat pernyataan ini ternyata tidak benar yang berakibat kerugian bagi negara, maka saya bersedia menanggung kerugian tersebut.', 0, 1, 0, true, 'J', true);
			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');


			$pdf->Ln(10);
			$pdf->SetX(128);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Tempat, tanggal, bulan dan tahun', 0, 1, 'C');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Ln(5);

			if ($data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']);
			} else {
				$tte = $jabatan_ttd_konsep;
			}


			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, $jabatan_ttd_konsep . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, $jabatan_ttd_konsep . ',', 0, 'C');
			}
			$pdf->Ln(4);

			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			if (!empty($this->data['RoleId_Cc'])) {

				$pdf->Ln(12);
				$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Ln(5);
				$pdf->Cell(30, 5, '', 0, 0, 'L');
				$pdf->Cell(5, 5, '', 0, 0, 'L');
				$filter_RoleId_To = array_filter($this->data['RoleId_Cc']);
				$hitung = count($filter_RoleId_To);
				$i = 0;

				foreach ($filter_RoleId_To as $v) {

					$y = 'Yth';
					$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;
					$uc  = ucwords(strtolower($xx1));
					$str = str_replace('Dan', 'dan', $uc);
					$str = str_replace('Uptd', 'UPTD', $str);
					$str = str_replace('Dprd', 'DPRD', $str);
					$i++;

					if (!empty($hitung)) {
						if ($hitung >= 1) {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . "; \n", 0, 'J', 0, 2, '', '', true);
						} elseif ($i == $hitung) {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . ".\n", 0, 'J', 0, 2, '', '', true);
						} else {
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
							$pdf->SetX(34);
							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str . ";\n", 0, 'J', 0, 2, '', '', true);
						}
					} else {
						$$pdf->SetX(34);
						$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
						$pdf->SetX(42);
						$pdf->MultiCell(150, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
					}
				}
			}



			$pdf->Output('surat_pernyataan_tugas_view.pdf', 'I');
			die();
			//surat izin
		} else if ($naskah->Ket == 'outboxsuratizin') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;
			$this->db->select("*");
			$this->db->where_in('PeopleId', $this->data['RoleId_To']);
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_perintah = $this->db->get('v_login')->result();

			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
			$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');

			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']);
			} else {

				$tte = $jabatan_ttd_konsep;
			}

			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $pangkat_ttd . '
			</td>
			</tr>
			</table>';


			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 30);

			$pdf->AddPage('P', 'F4');

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG');
			$pdf->Ln(9);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'SURAT IZIN', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->SetX(45);

			$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'TENTANG', 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->SetX(48);
			$pdf->writeHTMLCell(130, 0, '', '', $this->data['Hal'] , 0, 1, 0, true, 'C', true);
			
			$pdf->Ln(5);
			$pdf->Cell(30, 9, 'Dasar', 0, 0, 'L');
			$pdf->Cell(5, 9, ':', 0, 0, 'L');
			$i = a;
			if (!empty($this->data['Konten'])) {
				$filter_RoleId_To = $this->data['Konten'];

				foreach ($filter_RoleId_To as $row) {
					$pdf->Ln(2);

					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');

					$pdf->SetX(70);
					$pdf->MultiCell(117, 5, $row . "\n", 0, 'J', 0, 2, '', '', true);
					$i++;
				}
			}

			if (!empty($this->data['Konten'])) {
				$Konten_decode = json_decode($this->data['Konten']);
				$i = a;
				foreach ($Konten_decode as $row) {
					$pdf->Ln(2);

					$pdf->SetX(65);
					$pdf->Cell(10, 5, $i . '. ', 0, 0, 'L');

					$pdf->SetX(70);
					$pdf->MultiCell(117, 5, $row . "\n", 0, 'J', 0, 2, '', '', true);
					$i++;
				}
			}

			$pdf->Ln(3);
			$pdf->SetX(45);
			$pdf->Cell(0, 10, 'MEMBERI IZIN :', 0, 0, 'C');
			$pdf->Ln(12);
			$pdf->Cell(30, 5, 'Kepada :', 0, 0, 'L');

			$pdf->Ln(3);
			$i = 0;
			foreach ($data_perintah as $row) {
				$i++;
				$pdf->Ln(6);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Nama', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(110, 6, $row->PeopleName, 0, 'L');

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'NIP', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(85, 5, $row->NIP, 0, 1, 'L');

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Jabatan', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(110, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);


				$instansi = $this->db->query("SELECT GroleId FROM role WHERE RoleId = '" . $this->session->userdata('primaryroleid') . "'  ")->row()->GroleId;

				$instansi_nama = $this->db->query("SELECT GroleName FROM master_grole WHERE GroleId = '" . $instansi . "'  ")->row()->GroleName;

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Instansi', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($instansi_nama));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(110, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);

				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(25, 5, 'Untuk', 0, 0, 'L');
				$pdf->SetX(60);
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				// $pdf->MultiCell(85,5,$str,0,'J');

				$pdf->writeHTMLCell(110, 5, '', '', $this->data['Hal_pengantar'] , 0, 1, 0, true, 'J', true);
			}
			$pdf->Ln(12);

			$pdf->SetX(125);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Ditetapkan di ..................', 0, 1, 'L');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'Pada tanggal .....................', 0, 1, 'L');


			// $pdf->SetFont('Arial','',10);
			$pdf->Ln(4);
			if ($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']) . ',', 0, 'C');
			} else {
				$pdf->SetX(100);
				$pdf->MultiCell(90, 5, $jabatan_ttd_konsep . ',', 0, 'C');
			}
			$pdf->Ln(2);
			$pdf->SetX(130);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');
			$pdf->Ln(3);

			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			$pdf->Output('surat_izin_view.pdf', 'I');
			die();


			// Akhir Fungsi Surat izin


			// $this->load->view('backend/standart/administrator/mail_tl/surattugas_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxundangan') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/undangan_pdf', $this->data);

		} else if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/nodin_pdf', $this->data);

		} else if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;


			$this->data['TglReg'] 			= $naskah->TglReg;
			$this->data['Approve_People'] 	= $naskah->Approve_People;
			$this->data['TtdText'] 			= $naskah->TtdText;
			$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
			$this->data['TtdText2'] 			= $naskah->TtdText2;
			$this->data['Approve_People3'] 	= $naskah->Approve_People3;
			$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
			$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;


			// 			$data_perintah = $this->db->get('v_login')->result();

			// 			// var_dump($data_perintah);
			// 			// die();

			// 			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			// 			//tcpdf

			// 			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			// 			$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			// 			$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
			// 			$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->PeoplePosition;

			// 			//tcpdf
			// 			$age = $this->session->userdata('roleid') == 'uk.1';
			// 			$xx = $this->session->userdata('roleid');

			// 			if ($this->data['TtdText'] == 'PLT') {
			// 				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			// 			} elseif ($this->data['TtdText'] == 'PLH') {
			// 				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			// 			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
			// 				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			// 				// .'<br>'.get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']);
			// 			} elseif ($this->data['TtdText2'] == 'untuk_beliau') {
			// 				$tte = 'a.b' . '. ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $atasanub) . '<br>' . get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']);
			// 			} else {

			// 				$tte = $this->data['nama_approve'];
			// 			}



			// 			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			// 			<tr nobr="true">
			// 				<td rowspan="7" width="60px" style="valign:bottom;">
			// 				<br><br>
			// 				<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			// 				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			// 			</tr>
			// 			<tr nobr="true">
			// 				<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			// 			</tr>
			// 			<tr nobr="true">
			// 				<td><br></td>
			// 			</tr>
			// 			<tr nobr="true">
			// 				<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			// 				</td>
			// 			</tr>
			// 			<tr nobr="true">
			// 				<td>' . $pangkat_ttd . '
			// 				</td>
			// 			</tr>
			// 		</table>';

			// 			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			// 			$pdf->SetPrintHeader(false);
			// 			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			// 			$pdf->SetMargins(30, 30, 30);
			// 			$pdf->SetAutoPageBreak(TRUE, 30);
			// 			// 
			// 			$pdf->AddPage('P', 'F4');

			// 			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG');
			// 			$pdf->Ln(10);
			// 			$pdf->SetX(120);
			// 			$pdf->SetFont('Arial', '', 11);

			// 			$pdf->Cell(30, 5, 'Tempat / Tanggal / Bulan / Tahun ', 0, 1, 'L');
			// 			$pdf->Ln(2);
			// 			$pdf->SetX(120);
			// 			// $pdf->SetFont('Arial','',10);
			// 			$pdf->Cell(30, 5, 'Kepada', 0, 1, 'L');


			// 			$pdf->Cell(20, 5, 'Nomor', 0, 0, 'L');
			// 			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			// 			$pdf->Cell(5, 5, $this->data['nosurat'], 0, 0, 'L');
			// 			$pdf->SetX(120);
			// 			$pdf->Cell(10, 5, 'Yth. ', 0, 0, 'L');
			// 			$pdf->SetX(130);
			// 			$pdf->writeHTMLCell(60, 5, '', '', $this->data['RoleId_To'], '', 1, 0, true, 'L', true);
			// 			$pdf->SetX(126);
			// 			$pdf->Cell(10, 5, 'di ', 0, 0, 'L');
			// 			// $pdf->SetX(130); ini dimatikan
			// 			$pdf->Ln(5);
			// 			$pdf->SetX(132);
			// 			$pdf->MultiCell(56, 5, $this->data['Alamat'], 0, 'L');
			// 			$y_kanan = $pdf->GetY(); // ini tambahan
			// 			$pdf->Ln(5);
			// 			$pdf->SetY(58);
			// 			$pdf->Cell(20, 5, 'Sifat', 0, 0, 'L');
			// 			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			// 			$pdf->Cell(60, 5, $this->data['SifatId'], 0, 0, 'L');
			// 			$pdf->SetX(130);
			// 			// $pdf->Cell(10,5,'di',0,0,'L');
			// 			$pdf->Ln(5);
			// 			$pdf->Cell(20, 5, 'Lampiran', 0, 0, 'L');
			// 			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			// 			$pdf->Cell(60, 5, $this->data['Jumlah'] . ' ' . $this->data['MeasureUnitId'], 0, 0, 'L');
			// 			$pdf->SetX(133);
			// 			$pdf->MultiCell(56, 5, '', 0, 'L');
			// 			$pdf->Cell(20, 5, 'Hal', 0, 0, 'L');
			// 			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			// 			$pdf->writeHTMLCell(60, 5, '', '', $this->data['Hal'], 0, 1, 0, true, 'L', true); // ini 0 jadi 1
			// 			$y_kiri = $pdf->GetY(); // ini tambahan
			// 			$pdf->Ln(25);
			// 			$pdf->SetX(50);
			// 			// ini tambahan
			// 			if ($y_kiri > $y_kanan) {
			// 				$pdf->SetY($y_kiri + 10);
			// 			} else if ($y_kiri < $y_kanan) {
			// 				$pdf->SetY($y_kanan + 10);
			// 			}
			// 			$pdf->SetX(50);
			// 			$pdf->writeHTMLCell(140, 0, '', '', $this->data['Konten'], 0, 1, 0, true, 'J', true);




			// 			// $pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			// 			// $pdf->SetPrintHeader(false);
			// 			// $pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
			// 			// $pdf->SetMargins(30, 30, 30);
			// 			// $pdf->SetAutoPageBreak(TRUE, 30);
			// 			// // 
			// 			// $pdf->AddPage('P', 'F4');

			// 			// $pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
			// 			// $pdf->Ln(10);
			// 			// $pdf->SetX(120);
			// 			// $pdf->SetFont('Arial','',11);
			// 			// $pdf->Cell(30,5,'Tempat / Tanggal / Bulan / Tahun ',0,1,'L');
			// 			// $pdf->Ln(2);
			// 			// $pdf->SetX(120);
			// 			// $pdf->SetFont('Arial','',11);
			// 			// // $pdf->SetFont('Arial','',10);
			// 			// $pdf->Cell(30,5,'Kepada',0,1,'L');


			// 			// $pdf->Cell(20,5,'Nomor',0,0,'L');
			// 			// $pdf->Cell(5,5,':',0,0,'L');
			// 			// $pdf->Cell(5,5,$this->data['nosurat'],0,0,'L');
			// 			// $pdf->SetX(120);
			// 			// $pdf->Cell(10,5,'Yth. ',0,0,'L');
			// 			// $pdf->SetX(130);

			// 			// $pdf->writeHTMLCell(60, 5, '', '', $this->data['RoleId_To'], '', 1, 0, true, 'L', true);
			// 			// $pdf->SetX(126);
			// 			// $pdf->Cell(10,5,'di. ',0,0,'L');
			// 			// // $pdf->SetX(130);
			// 			// $pdf->Ln(5);
			// 			// $pdf->SetX(132);
			// 			// $pdf->MultiCell(56,5,$this->data['Alamat'],0,'L');
			// 			// $y_kanan = $pdf->GetY(); // ini tambahan						
			// 			// $pdf->Ln(5);
			// 			// $pdf->SetY(58);
			// 			// $pdf->Cell(20,5,'Sifat',0,0,'L');
			// 			// $pdf->Cell(5,5,':',0,0,'L');
			// 			// $pdf->Cell(60,5,$this->data['SifatId'],0,0,'L');
			// 			// $pdf->SetX(130);
			// 			// // $pdf->Cell(10,5,'di',0,0,'L');
			// 			// $pdf->Ln(5);
			// 			// $pdf->Cell(20,5,'Lampiran',0,0,'L');
			// 			// $pdf->Cell(5,5,':',0,0,'L');
			// 			// $pdf->Cell(60,5,$this->data['Jumlah'].' '.$this->data['MeasureUnitId'],0,0,'L');
			// 			// $pdf->SetX(133);
			// 			// $pdf->MultiCell(56,5,'',0,'L');
			// 			// $pdf->Cell(20,5,'Hal',0,0,'L');
			// 			// $pdf->Cell(5,5,':',0,0,'L');
			// 			// $pdf->writeHTMLCell(60, 5, '', '', $this->data['Hal'], 0, 0, 0, true, 'L', true);
			// 			// $y_kiri = $pdf->GetY(); // ini tambahan
			// 			// $pdf->Ln(25);	
			// 			// $pdf->SetX(50);
			// 			// // ini tambahan
			// 			// if ($y_kiri > $y_kanan) {
			// 			// 	$pdf->SetY($y_kiri + 10);
			// 			// }else if ($y_kiri < $y_kanan){
			// 			// 	$pdf->SetY($y_kanan + 10);
			// 			// }
			// 			// $pdf->SetX(50);	
			// 			// $pdf->writeHTMLCell(140, 0, '', '', $this->data['Konten'], 0, 1, 0, true, 'J', true);


			// 			// $pdf->SetFont('Arial','',10);
			// 			$r_atasan =  $this->session->userdata('roleatasan');
			// 			$r_biro =  $this->session->userdata('groleid');
			// 			$age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
			// 			$atasanub = get_data_people('RoleAtasan', $Nama_ttd_atas_nama);

			// 			$pdf->Ln(6);
			// 			$pdf->SetFont('Arial', '', 10);
			// 			if ($this->data['TtdText'] == 'PLT') {
			// 				$pdf->SetX(100);
			// 				$pdf->SetFont('Arial', '', 10);
			// 				$pdf->SetFont('Arial', '', 8);
			// 				$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 			} elseif ($this->data['TtdText'] == 'PLH') {
			// 				$pdf->SetX(100);
			// 				$pdf->SetFont('Arial', '', 10);
			// 				$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
			// 				$pdf->SetX(100);
			// 				$pdf->SetFont('Arial', '', 10);
			// 				$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 				$pdf->SetX(100);
			// 				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 			} elseif ($this->data['TtdText2'] == 'untuk_beliau') {
			// 				$pdf->SetX(100);
			// 				$pdf->MultiCell(90, 5, 'u.b. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 				$pdf->SetX(100);
			// 				$pdf->MultiCell(90, 5, get_data_people('RoleName', $atasanub) . ',', 0, 'C');
			// 				$pdf->SetX(100);
			// 				$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']) . ',', 0, 'C');
			// 			} else {
			// 				$pdf->SetX(100);
			// 				$pdf->MultiCell(90, 5, $this->data['nama_approve'] . ',', 0, 'C');
			// 			}

			// 			$pdf->Ln(2);
			// 			$pdf->SetX(130);
			// 			// $pdf->SetFont('Arial','',10);
			// 			$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');

			// 			$pdf->Ln(5);

			// 			$pdf->SetX(100);

			// 			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);


			// 			if (!empty($this->data['RoleId_Cc'])) {
			// 				$exp = explode(',', $this->data['RoleId_Cc']);
			// 				$z = count($exp);

			// 				$pdf->Ln(12);
			// 				$pdf->Cell(30, 5, 'Tembusan', 0, 0, 'L');
			// 				$pdf->SetX(53);
			// 				$pdf->Cell(5, 5, ':', 0, 0, 'L');
			// 				$pdf->Ln(5);
			// 				$pdf->Cell(30, 5, '', 0, 0, 'L');
			// 				$pdf->Cell(5, 5, '', 0, 0, 'L');

			// 				$exp = explode(',', $this->data['RoleId_Cc']);
			// 				$z = count($exp);
			// 				$i = 0;
			// 				foreach ($exp as $v) {
			// 					$y = 'Yth';
			// 					$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;
			// 					$uc  = ucwords(strtolower($xx1));
			// 					$str = str_replace('Dan', 'dan', $uc);
			// 					$str = str_replace('Uptd', 'UPTD', $str);
			// 					$str = str_replace('Dprd', 'DPRD', $str);
			// 					$i++;

			// 					if (!empty($hitung)) {

			// 						if ($hitung > 1) {
			// 							# code...
			// 							if ($i == $hitung - 1) {
			// 								# code...
			// 								$pdf->Ln(1);
			// 								$pdf->SetX(30);
			// 								$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
			// 								$pdf->SetX(34);
			// 								$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
			// 								$pdf->SetX(42);
			// 								$pdf->MultiCell(150, 5, $str . "; dan\n", 0, 'J', 0, 2, '', '', true);
			// 							} elseif ($i == $hitung) {
			// 								$pdf->Ln(1);
			// 								$pdf->SetX(30);
			// 								$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
			// 								$pdf->SetX(34);
			// 								$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
			// 								$pdf->SetX(42);
			// 								$pdf->MultiCell(150, 5, $str . ".\n", 0, 'J', 0, 2, '', '', true);
			// 							} else {
			// 								$pdf->Ln(1);
			// 								$pdf->SetX(30);
			// 								$pdf->Cell(30, 5, $i . '. ', 0, 0, 'L');
			// 								$pdf->SetX(34);
			// 								$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
			// 								$pdf->SetX(42);
			// 								$pdf->MultiCell(150, 5, $str . ";\n", 0, 'J', 0, 2, '', '', true);
			// 							}
			// 						} else {
			// 							$$pdf->SetX(34);
			// 							$pdf->Cell(30, 5, $y . '. ', 0, 0, 'L');
			// 							$pdf->SetX(42);
			// 							$pdf->MultiCell(150, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			// 						}
			// 					}
			// 				}
			// 			}
			// 			if (!empty($this->data['lampiran2'])) {
			// 				$lamp_romawi1 = "I";
			// 			} else {
			// 				$lamp_romawi1 = "";
			// 			}

			// 			if ($this->data['type_naskah'] == 'XxJyPn38Yh.2') {
			// 				$type_surat = "SURAT UNDANGAN ";
			// 			} elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
			// 				$type_surat = "SURAT PANGGILAN ";
			// 			} else {
			// 				$type_surat = "SURAT ";
			// 			}

			// 			$header_lampiran = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
			// 	<tr>
			//         <td width="65px">Lampiran ' . $lamp_romawi1 . ' : </td>
			//         <td colspan="2" width="200px">' . $type_surat . '' . get_data_people('RoleName', $naskah->Approve_People3) . '</td>
			//     </tr>
			//     <tr>
			//         <td>&nbsp;</td>
			//          <td width="50px">NOMOR</td>
			//         <td width="10px">:</td>
			//         <td width="140px">' . $this->data['nosurat'] . '</td>
			//     </tr>

			//     <tr>

			//         <td>&nbsp;</td>
			//         <td>TANGGAL</td>
			//         <td>:</td>
			//         <td>Tanggal/Bulan/Tahun</td>
			//     </tr>
			//     <tr>
			//         <td>&nbsp;</td>
			//         <td>PERIHAL</td>
			//         <td>:</td>
			//         <td>' . $this->data['Hal'] . '</td>
			//     </tr>
			// </table>';

			// 			$jum_lamp2 = $this->data['lampiran2'];
			// 			$no_lamp2 = 1;
			// 			if (!empty($this->data['lampiran2'])) {
			// 				switch ($no_lamp2) {
			// 					case 1:
			// 						$lamp_romawi2 = "II";

			// 						break;
			// 				}
			// 			}

			// 			if ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
			// 				$type_surat = "SURAT UNDANGAN ";
			// 			} elseif ($this->data['type_naskah']  == 'XxJyPn38Yh.3') {
			// 				$type_surat = "SURAT PANGGILAN ";
			// 			} else {
			// 				$type_surat = "SURAT ";
			// 			}

			// 			$header_lampiran2 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
			// 	<tr>
			//         <td width="65px">Lampiran ' . $lamp_romawi2 . ' : </td>
			//         <td colspan="2" width="200px">' . $type_surat . '' . get_data_people('RoleName', $naskah->Approve_People3) . '</td>
			//     </tr>
			//     <tr>
			//         <td>&nbsp;</td>
			//          <td width="50px">NOMOR</td>
			//         <td width="10px">:</td>
			//         <td width="140px">' . $this->data['nosurat'] . '</td>
			//     </tr>

			//     <tr>

			//         <td>&nbsp;</td>
			//         <td>TANGGAL</td>
			//         <td>:</td>
			//         <td>Tanggal/Bulan/Tahun</td>
			//     </tr>
			//     <tr>
			//         <td>&nbsp;</td>
			//         <td>PERIHAL</td>
			//         <td>:</td>
			//         <td>' . $this->data['Hal'] . '</td>
			//     </tr>
			// </table>';

			// 			$jum_lamp3 = $this->data['lampiran3'];
			// 			$no_lamp3 = 1;
			// 			if (!empty($this->data['lampiran3'])) {
			// 				switch ($no_lamp3) {
			// 					case 1:
			// 						$lamp_romawi3 = "III";

			// 						break;
			// 				}
			// 			}

			// 			if ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
			// 				$type_surat = "SURAT UNDANGAN ";
			// 			} elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
			// 				$type_surat = "SURAT PANGGILAN ";
			// 			} else {
			// 				$type_surat = "SURAT ";
			// 			}

			// 			$header_lampiran3 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
			// 	<tr>
			//         <td width="65px">Lampiran ' . $lamp_romawi3 . ' : </td>
			//         <td colspan="2" width="200px">' . $type_surat . '' . get_data_people('RoleName', $naskah->Approve_People3) . '</td>
			//     </tr>
			//     <tr>
			//         <td>&nbsp;</td>
			//          <td width="50px">NOMOR</td>
			//         <td width="10px">:</td>
			//         <td width="140px">' . $this->data['nosurat'] . '</td>
			//     </tr>

			//     <tr>

			//         <td>&nbsp;</td>
			//         <td>TANGGAL</td>
			//         <td>:</td>
			//         <td>Tanggal/Bulan/Tahun</td>
			//     </tr>
			//     <tr>
			//         <td>&nbsp;</td>
			//         <td>PERIHAL</td>
			//         <td>:</td>
			//         <td>' . $this->data['Hal'] . '</td>
			//     </tr>
			// </table>';


			// 			$jum_lamp4 = $this->data['lampiran4'];
			// 			$no_lamp4 = 1;

			// 			if (!empty($this->data['lampiran4'])) {
			// 				switch ($no_lamp4) {
			// 					case 1:
			// 						$lamp_romawi4 = "IV";

			// 						break;
			// 				}
			// 			}

			// 			if ($this->data['type_naskah']  == 'XxJyPn38Yh.2') {
			// 				$type_surat = "SURAT UNDANGAN ";
			// 			} elseif ($this->data['type_naskah'] == 'XxJyPn38Yh.3') {
			// 				$type_surat = "SURAT PANGGILAN ";
			// 			} else {
			// 				$type_surat = "SURAT ";
			// 			}

			// 			$header_lampiran4 = '<table style="font-size:10px;" cellpadding="1px" nobr="true" width="10px">
			// 	<tr>
			//         <td width="65px">Lampiran ' . $lamp_romawi4 . ' : </td>
			//         <td colspan="2" width="200px">' . $type_surat . '' . get_data_people('RoleName', $naskah->Approve_People3) . '</td>
			//     </tr>
			//     <tr>
			//         <td>&nbsp;</td>
			//          <td width="50px">NOMOR</td>
			//         <td width="10px">:</td>
			//         <td width="140px">' . $this->data['nosurat'] . '</td>
			//     </tr>

			//     <tr>

			//         <td>&nbsp;</td>
			//         <td>TANGGAL</td>
			//         <td>:</td>
			//         <td>Tanggal/Bulan/Tahun</td>
			//     </tr>
			//     <tr>
			//         <td>&nbsp;</td>
			//         <td>PERIHAL</td>
			//         <td>:</td>
			//         <td>' . $this->data['Hal'] . '</td>
			//     </tr>
			// </table>';


			// 			if (!empty($this->data['lampiran'])) {
			// 				$pdf->AddPage('P', 'F4');
			// 				$pdf->SetFont('Arial', '', 11);
			// 				$pdf->SetX(95);
			// 				$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran, 0, 1, 0, false, 'J', false);

			// 				$pdf->Ln(10);
			// 				$pdf->SetX(30);
			// 				$pdf->writeHTMLCell(160, 0, '', '', $this->data['lampiran'], 0, 1, 0, true, 'J', true);

			// 				$r_atasan =  $this->session->userdata('roleatasan');
			// 				$r_biro =  $this->session->userdata('groleid');
			// 				$age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
			// 				$atasanub = get_data_people('RoleAtasan', $Nama_ttd_atas_nama);

			// 				$pdf->Ln(6);
			// 				$pdf->SetFont('Arial', '', 10);
			// 				if ($this->data['TtdText'] == 'PLT') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText'] == 'PLH') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText2'] == 'untuk_beliau') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'u.b. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $atasanub) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']) . ',', 0, 'C');
			// 				} else {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, $this->data['nama_approve'] . ',', 0, 'C');
			// 				}
			// 				$pdf->Ln(2);
			// 				$pdf->SetX(130);
			// 				// $pdf->SetFont('Arial','',10);
			// 				$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');

			// 				$pdf->Ln(5);

			// 				$pdf->SetX(100);
			// 				$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			// 			}

			// 			if (!empty($this->data['lampiran2'])) {
			// 				$pdf->AddPage('P', 'F4');
			// 				$pdf->SetX(95);
			// 				$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran2, '', 1, 0, false, 'J', false);
			// 				$pdf->Ln(10);
			// 				$pdf->SetX(30);
			// 				$pdf->writeHTMLCell(160, 0, '', '', $this->data['lampiran2'], 0, 1, 0, true, 'J', true);
			// 				$r_atasan =  $this->session->userdata('roleatasan');
			// 				$r_biro =  $this->session->userdata('groleid');
			// 				$age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
			// 				$atasanub = get_data_people('RoleAtasan', $Nama_ttd_atas_nama);

			// 				$pdf->Ln(6);

			// 				if ($this->data['TtdText'] == 'PLT') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText'] == 'PLH') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText2'] == 'untuk_beliau') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'u.b. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $atasanub) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']) . ',', 0, 'C');
			// 				} else {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, $this->data['nama_approve'] . ',', 0, 'C');
			// 				}
			// 				$pdf->Ln(2);
			// 				$pdf->SetX(130);
			// 				// $pdf->SetFont('Arial','',10);
			// 				$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');

			// 				$pdf->Ln(5);

			// 				$pdf->SetX(100);
			// 				$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			// 			}

			// 			if (!empty($this->data['lampiran3'])) {

			// 				$pdf->AddPage('P', 'F4');
			// 				$pdf->SetX(95);
			// 				$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran3, '', 1, 0, false, 'J', false);
			// 				$pdf->Ln(10);
			// 				$pdf->SetX(30);
			// 				$pdf->writeHTMLCell(160, 0, '', '', $this->data['lampiran3'], 0, 1, 0, true, 'J', true);

			// 				$r_atasan =  $this->session->userdata('roleatasan');
			// 				$r_biro =  $this->session->userdata('groleid');
			// 				$age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
			// 				$atasanub = get_data_people('RoleAtasan', $Nama_ttd_atas_nama);

			// 				$pdf->Ln(6);
			// 				if ($this->data['TtdText'] == 'PLT') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText'] == 'PLH') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText2'] == 'untuk_beliau') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'u.b. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $atasanub) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']) . ',', 0, 'C');
			// 				} else {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, $this->data['nama_approve'] . ',', 0, 'C');
			// 				}
			// 				$pdf->Ln(2);
			// 				$pdf->SetX(130);
			// 				// $pdf->SetFont('Arial','',10);
			// 				$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');

			// 				$pdf->Ln(5);

			// 				$pdf->SetX(100);
			// 				$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			// 			}

			// 			if (!empty($this->data['lampiran4'])) {

			// 				$pdf->AddPage('P', 'F4');
			// 				$pdf->SetX(95);
			// 				$pdf->writeHTMLCell(80, 0, '', '', $header_lampiran4, '', 1, 0, false, 'J', false);
			// 				$pdf->Ln(10);
			// 				$pdf->SetX(30);
			// 				$pdf->writeHTMLCell(160, 0, '', '', $this->data['lampiran4'], 0, 1, 0, true, 'J', true);

			// 				$r_atasan =  $this->session->userdata('roleatasan');
			// 				$r_biro =  $this->session->userdata('groleid');
			// 				$age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
			// 				$atasanub = get_data_people('RoleAtasan', $Nama_ttd_atas_nama);

			// 				$pdf->Ln(6);
			// 				if ($this->data['TtdText'] == 'PLT') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText'] == 'PLH') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'a.n. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Approve_People']) . ',', 0, 'C');
			// 				} elseif ($this->data['TtdText2'] == 'untuk_beliau') {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, 'u.b. ' . get_data_people('RoleName', $this->data['Approve_People3']) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $atasanub) . ',', 0, 'C');
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, get_data_people('RoleName', $this->data['Nama_ttd_atas_nama']) . ',', 0, 'C');
			// 				} else {
			// 					$pdf->SetX(100);
			// 					$pdf->MultiCell(90, 5, $this->data['nama_approve'] . ',', 0, 'C');
			// 				}
			// 				$pdf->Ln(2);
			// 				$pdf->SetX(130);
			// 				// $pdf->SetFont('Arial','',10);
			// 				$pdf->Cell(30, 5, 'PEMERIKSA', 0, 1, 'L');

			// 				$pdf->Ln(5);

			// 				$pdf->SetX(100);
			// 				$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			// 			}

			// 			$pdf->Output('surat_dinas_view.pdf', 'I');

			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/suratdinas_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxedaran') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/suratedaran_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxsket') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_ket = $this->db->get('v_login')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf
			$age = $this->session->userdata('roleid');
			// if ($this->data['TtdText'] == 'PLT') {
			// 	$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			// } elseif ($data['TtdText'] == 'PLH') {
			// 	$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			// } elseif ($data['TtdText2'] == 'Atas_Nama') {
			// 	$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			// } else {
			// 	$tte = get_data_people('RoleName', $this->data['Approve_People']);
			// }
			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}	

			$PEMERIKSA='PEMERIKSA';

		$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td width="240px" style="text-align:center;">Tempat, tanggal, bulan dan tahun</td>
			</tr>
			<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $tte . ',</td>				
			</tr>
				<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $PEMERIKSA . '</td>				
			</tr>
		</table>
		<br>
		<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td rowspan="7" width="60px" style="valign:bottom;">
				<br><br>
				<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
				<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
				<td><br></td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
				</td>
			</tr>
			<tr nobr="true">
				<td>' . $pangkat_ttd . '
				</td>
			</tr>
		</table>';



			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 25);
			// 
			$pdf->AddPage('P', 'F4');

			if ($age == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
				$pdf->Ln(10);
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}

			$pdf->Ln(5);
			$pdf->Cell(0, 10, 'SURAT KETERANGAN', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(70, 5, 'Yang bertandatangan di bawah ini : ', 0, 0, 'L');
			$pdf->Ln(7);
			$pdf->Cell(45, 5, 'a. Nama', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->Cell(108, 5, $this->data['Nama_ttd_konsep'], 0, 0, 'L');
			$pdf->Ln(6);	
			$pdf->Cell(45, 5, 'b. Jabatan', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->MultiCell(108, 5, ucwords(strtolower(get_data_people('RoleName', $this->data['Approve_People']))) . "\n", 0, 'J', 0, 2, '', '', true);
			$pdf->Ln(7);
			$pdf->Cell(67, 5, 'dengan ini menerangkan bahwa : ', 0, 0, 'L');
			$pdf->Ln(7);
			$i = 0;
			foreach ($data_ket as $row) {
				$i++;
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'a. Nama/NIP', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(108, 5, $row->PeopleName . ' / NIP.' . $row->NIP, 0, 'L');
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'b. Pangkat/Golongan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->Cell(108, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
				$pdf->Ln(2);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'c. Jabatan', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(108, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
				$pdf->Ln(3);
				$pdf->SetX(30);
				$pdf->Cell(45, 5, 'd. Maksud', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->writeHTMLCell(108, 0, '', '', $this->data['Hal'] . "\n", 0, 1, 0, false, 'J', false);
			}
			$pdf->Ln(7);
			$pdf->Cell(30, 5, 'Demikian Surat Keterangan ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
			$pdf->Ln(15);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('surat_keterangan_view.pdf', 'I');
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_ket = $this->db->get('v_login')->result();

			// $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf
			$age = $this->session->userdata('roleid');
			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}

			$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td width="240px" style="text-align:center;">' . $this->data['lokasi'] . ', ' . get_bulan($this->data['TglNaskah']) . '</td>
			</tr>
			<tr nobr="true">
			<td width="240px" style="text-align:center;">' . $tte . ',</td>
			</tr>
			</table>
			<br>
			<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $pangkat_ttd . '
			</td>
			</tr>
			</table>';



			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');

			if ($age == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 160, 0, 'PNG', '', 'N');
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(10);
			$pdf->Cell(0, 10, 'PENGUMUMAN', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
			$pdf->Ln(10);
			$pdf->Cell(0, 10, 'TENTANG', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Hal'] . "\n", 0, 1, false, true, 'C', false);
			$pdf->Ln(10);
			$pdf->writeHTMLCell(160, 0, '', '', $this->data['Konten'] . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(13);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('pengumuman_view.pdf', 'I');
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$this->load->library('Pdf');
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(',', $this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Golongan', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_ket = $this->db->get('v_login')->result();

			// $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;
			$pangkat_ttd = $this->db->query("SELECT Pangkat FROM v_login WHERE PeopleId = '" . $naskah->Nama_ttd_atas_nama . "'")->row()->Pangkat;
			//tcpdf
			// var_dump($this->data);
			// die();
			$age = $this->session->userdata('roleid');
			if ($this->data['TtdText'] == 'PLT') {
				$tte = 'Plt. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText'] == 'PLH') {
				$tte = 'Plh. ' . get_data_people('RoleName', $this->data['Approve_People']);
			} elseif ($this->data['TtdText2'] == 'Atas_Nama') {
				$tte = 'a.n ' . get_data_people('RoleName', $this->data['Approve_People3']) . '<br>' . get_data_people('RoleName', $this->data['Approve_People']);
			} else {
				$tte = get_data_people('RoleName', $this->data['Approve_People']);
			}

			$table = '<table style="font-size:12px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td width="240px" style="text-align:center;">' . $this->data['lokasi'] . ', ' . get_bulan($this->data['TglNaskah']) . '</td>
			</tr>
			<tr nobr="true">
				<td width="240px" style="text-align:center;">' . $tte . ',</td>
			</tr>
			</table>
			<br>
			<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<td rowspan="7" width="60px" style="valign:bottom;">
			<br><br>
			<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
			<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
			<td width="180px" style="text-align: left; font-size:9;">' . $tte . ',</td>
			</tr>
			<tr nobr="true">
			<td><br></td>
			</tr>
			<tr nobr="true">
			<td style="font-size:10px;">' . $this->data['Nama_ttd_konsep'] . '
			</td>
			</tr>
			<tr nobr="true">
			<td>' . $pangkat_ttd . '
			</td>
			</tr>
			</table>';

			$konten = json_decode($this->data['Konten']);

			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
			$pdf->SetMargins(30, 30, 20);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');
			if ($this->session->userdata('roleid') == 'uk.1') {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 25, 5, 160, 0, 'PNG', '', 'N');
			} else {
				$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
			}
			$pdf->Ln(10);
			$pdf->Cell(0, 10, 'REKOMENDASI', 0, 0, 'C');
			$pdf->Ln(6);
			$pdf->Cell(0, 10, 'NOMOR : ' . $naskah->nosurat, 0, 0, 'C');
			$pdf->Ln(15);
			$pdf->Cell(30, 0, 'Dasar', 0, 0, 'L');
			$pdf->Cell(5, 0, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(125, 0, '', '', $this->data['Hal'] . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(0);
			$pdf->Cell(30, 0, 'Menimbang', 0, 0, 'L');
			$pdf->Cell(5, 0, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(125, 0, '', '', $konten->menimbang . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(5);
			$pdf->MultiCell(160, 5, $tte . ', memberikan rekomendasi kepada :', 0, 'L');
			$pdf->Ln(0);
			$i = 0;
			foreach ($data_ket as $row) {
				$i++;
				$pdf->SetX(30);
				$pdf->Cell(55, 5, 'a. Nama/obyek', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$pdf->MultiCell(95, 5, $row->PeopleName, 0, 'L');
				$pdf->Ln(5);
				$pdf->SetX(30);
				$pdf->Cell(55, 5, 'b. Jabatan/Tempat/Identitas', 0, 0, 'L');
				$pdf->Cell(5, 5, ':', 0, 0, 'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				// $pdf->MultiCell(85,5,$str,0,'J');
				$pdf->MultiCell(95, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
			}
			$pdf->Ln(5);
			$pdf->Cell(30, 5, 'Untuk', 0, 0, 'L');
			$pdf->Cell(5, 5, ':', 0, 0, 'L');
			$pdf->writeHTMLCell(125, 0, '', '', $konten->untuk . "\n", 0, 1, false, true, 'J', false);
			$pdf->Ln(0);
			$pdf->Cell(30, 5, 'Demikian rekomendasi ini dibuat untuk dipergunakan seperlunya.', 0, 0, 'L');
			$pdf->Ln(10);
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
			$pdf->Output('surat_rekomendasi_view.pdf', 'I');
		} else {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/mail_tl/inbox_pdf_log', $this->data);
		}

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 10),
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('konsep_naskah.pdf', 'H');
	}
	//Akhir View PDF Naskah Masuk

	//PDF nota dinas tindaklanjut
	public function nota_dinas_tindaklanjut_pdf($NId)
	{
		$this->load->library('HtmlPdf');
		ob_start();

		$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

		$this->data['NId'] 				= $NId;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Jumlah'] 			= $naskah->Jumlah;
		$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		$this->data['TglReg'] 			= $naskah->TglReg;
		$this->data['Hal'] 				= $naskah->Hal;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_tindaklanjut_pdf', $this->data);

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 20),
		);


		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	//Akhir PDF nota dinas tindaklanjut

	//Buka view naskah dinas tindaklanjut edit
	public function view_naskah_dinas_tindaklanjut_edit($tipe, $NId)
	{
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['naskah'] 	= $this->model_konsepnaskah->find($NId);

		$this->tempanri('backend/standart/administrator/mail_tl/view_naskah_dinas_tindaklanjut_edit', $this->data);
	}
	//Tutup view naskah dinas tindaklanjut edit

	//Buka verifikasi
	public function verifikasi($NId)
	{
		$tanggal = date('Y-m-d H:i:s');
		$GIR_Id = $this->db->query("SELECT GIR_Id FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' ")->row()->GIR_Id;

		try {
			$this->load->library('user_agent');
			get_data_konsepnaskah('NId_Temp', $NId);
			$password = $this->input->post('password');
			$where 	  = array('PeoplePassword' => sha1($password),);
			$cek 	  = $this->db->get_where("people", $where)->num_rows();

			$kalimat = str_replace(' ', '', $password);
			if ($kalimat == '') {
				set_message('Password Wajib Diisi', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			} else {

				if ($cek > 0) {

					$logopath = './uploads/anri.png'; //Logo yang akan di insert

					//Untuk menghitung nomor nota dinas
					$tahun 	= date('Y');
					$query = $this->db->query("SELECT (max(Number) + 1) as id FROM konsep_naskah WHERE ReceiverAs = 'to_notadinas' AND YEAR(TglNaskah) = '" . $tahun . "' AND RoleId_From = '" . $this->session->userdata('roleid') . "'")->row()->id;

					if (!empty($query[0])) {
						$id = $query;
					} else {
						$id = 1;
					}

					$cari = $this->db->query("SELECT ClId FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->ClId;

					$klas = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $cari . "'")->row()->ClCode;

					$nosurat = $id . '/' . $klas . '/' . $tahun;


					//KONSEP NASKAH
					$this->db->where('NId_Temp', $NId);
					$this->db->update('konsep_naskah', ['Konsep' => 0, 'Number' => $id, 'nosurat' => $nosurat, 'TglNaskah' => date('Y-m-d H:i:s')]);


					$CI = &get_instance();
					$this->load->library('ciqrcode'); //pemanggilan library QR CODE
					$this->load->helper('string');

					$config['cacheable']    = true; //boolean, the default is true
					$config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
					$config['errorlog']     = './uploads/'; //string, the default is application/logs/
					$config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
					$config['quality']      = true; //boolean, the default is true
					$config['size']         = '1024'; //interger, the default is 1024
					$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
					$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
					$CI->ciqrcode->initialize($config);

					$image_name				= $NId . '-' . date('Ymd') . '.png'; //buat name dari qr code sesuai dengan

					$params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/') . $NId; //data yang akan di jadikan QR CODE
					$params['level'] 		= 'H'; //H=High
					$params['size'] 		= 10;
					$params['savename'] 	= FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
					$CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

					//Memasukkan logo di qrcode
					$fullpath = FCPATH . $config['imagedir'] . $image_name;

					$QR = imagecreatefrompng($fullpath);
					// memulai menggambar logo dalam file qrcode
					$logo = imagecreatefromstring(file_get_contents($logopath));
					imagecolortransparent($logo, imagecolorallocatealpha($logo, 0, 0, 0, 127));

					imagealphablending($logo, false);
					imagesavealpha($logo, true);

					$QR_width = imagesx($QR); //get logo width
					$QR_height = imagesy($QR); //get logo width

					$logo_width = imagesx($logo);
					$logo_height = imagesy($logo);

					// Scale logo to fit in the QR Code
					$logo_qr_width = $QR_width / 4;
					$scale = $logo_width / $logo_qr_width;
					$logo_qr_height = $logo_height / $scale;

					//220 -> left,  210 -> top
					imagecopyresampled($QR, $logo, 220, 210, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
					// imagecopyresampled($QR, $logo, $QR_width/4, $QR_height/3.5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
					// 
					// Simpan kode QR lagi, dengan logo di atasnya

					imagepng($QR, $fullpath);

					$data_ttd = [
						'NId' 				=> $NId,
						'TglProses' 		=> '',
						'PeopleId' 			=> $this->session->userdata('peopleid'),
						'RoleId' 			=> $this->session->userdata('roleid'),
						'Verifikasi'  		=> random_string('sha1', 10),
						'Identitas_Akses' 	=> '',
						'QRCode' 			=> $image_name,
						'TglProses' 		=> date('Y-m-d H:i:s'),
					];

					$save_ttd 		= $this->model_ttd->store($data_ttd);

					//Create PDF File
					$this->load->library('HtmlPdf');
					ob_start();
					$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' ORDER BY TglNaskah DESC LIMIT 1")->row();
					$this->data['NId'] 				= $NId;
					$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
					$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
					$this->data['RoleId_To'] 		= $naskah->RoleId_To;
					$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
					$this->data['Konten'] 			= $naskah->Konten;
					$this->data['Jumlah'] 			= $naskah->Jumlah;
					$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
					$this->data['TglNaskah'] 		= $naskah->TglNaskah;
					$this->data['Hal'] 				= $naskah->Hal;
					$this->data['Approve_People'] 	= $naskah->Approve_People;
					$this->data['TtdText'] 			= $naskah->TtdText;
					$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
					$this->data['Number'] 			= $naskah->Number;
					$this->data['nosurat'] 			= $naskah->nosurat;
					$this->data['lokasi'] 			= $naskah->lokasi;
					$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

					$this->load->view('backend/standart/administrator/mail_tl/inbox_pdf', $this->data);
					$content = ob_get_contents();
					ob_end_clean();

					$config = array(
						'orientation' => 'p',
						'format' => 'f4',
						'marges' 		=> array(30, 30, 20, 20),
					);

					$this->pdf = new HtmlPdf($config);
					$this->pdf->initialize($config);
					$this->pdf->pdf->SetDisplayMode('fullpage');
					$this->pdf->writeHTML($content);

					$a = 'nota_dinas_tindaklanjut-' . $NId . '.pdf';
					$this->pdf->Output('FilesUploaded/naskah/nota_dinas_tindaklanjut-' . $NId . '.pdf', 'F');

					//Simpan Inbox File
					//Karena naskah nya merupakan tindaklanjut, maka nilai gir_id ditukar dengan nid
					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $NId,
						'NId' 				=> $GIR_Id,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $a,
						'FileName_fake' 	=> $a,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outbox',
					];

					$save_file = $this->model_inboxfile->store($save_files);

					set_message('Data Berhasil Disimpan', 'success');
					redirect(BASE_URL('administrator/anri_list_notadinas_sdh_ap/'));
					redirect($this->agent->referrer());
				} else {
					set_message('Kode Verifikasi Salah', 'error');
					redirect($this->agent->referrer());
				}
			}
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_dinas/' . $NId));
		}
	}
	//Tutup verifikasi

	//Buka kirim naskah dinas
	public function naskah_dinas_kirim()
	{
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$nid = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			$this->load->helper('string');

			if (empty($this->input->post('nid'))) {

				$kepada 	= explode(',', get_data_konsepnaskah('RoleId_To', $this->input->post('temp')));
				$tembusan 	= explode(',', get_data_konsepnaskah('RoleId_Cc', $this->input->post('temp')));

				$save_data = [
					'NKey'         		=> tb_key(),
					'NId'				=> $nid,
					'CreatedBy'			=> $this->session->userdata('peopleid'),
					'CreationRoleId'	=> $this->session->userdata('roleid'),
					'NTglReg' 			=> $tanggal,
					'Tgl' 				=> date('Y-m-d', strtotime($this->input->post('Tgl'))),
					'JenisId' 			=> get_data_konsepnaskah('JenisId', $this->input->post('temp')),
					'Nomor' 			=> get_data_konsepnaskah('Number', $this->input->post('temp')),
					'Hal' 				=> get_data_konsepnaskah('Hal', $this->input->post('temp')),
					'Pengirim'			=> 'internal',
					'NTipe' 			=> get_data_konsepnaskah('Ket', $this->input->post('temp')),
					'NFileDir'			=> 'naskah',
					'BerkasId'			=> '1',
				];
				$save_inbox = $this->model_inbox->store($save_data);

				if (count((array) $kepada)) {
					foreach ($kepada as $data_kepada) {
						$datakepada = [
							'NId' 			=> $this->input->post('temp'),
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $gir,
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $data_kepada,
							'RoleId_To' 	=> get_data_people('RoleId', $data_kepada),
							'ReceiverAs' 	=> get_data_konsepnaskah('ReceiverAs ', $this->input->post('temp')),
							'Msg' 			=> get_data_konsepnaskah('Konten', $this->input->post('temp')),
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> get_data_people('RoleDesc', $data_kepada),
						];

						$save_kepada = $this->model_inboxreciever->store($datakepada);
					}
				}

				if (count((array) $tembusan)) {
					foreach ($tembusan as $data_tembusan) {
						$datatembusan = [
							'NId' 			=> $this->input->post('temp'),
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $gir,
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $data_tembusan,
							'RoleId_To' 	=> get_data_people('RoleId', $data_tembusan),
							'ReceiverAs' 	=> 'bcc',
							'Msg' 			=> get_data_konsepnaskah('Konten', $this->input->post('temp')),
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> get_data_people('RoleDesc', $data_tembusan),
						];

						$save_tembusan = $this->model_inboxreciever->store($datatembusan);
					}
				}
			} else {

				$kepada 	= explode(',', get_data_konsepnaskah('RoleId_To', $this->input->post('nid')));
				$tembusan 	= explode(',', get_data_konsepnaskah('RoleId_Cc', $this->input->post('nid')));

				if (count((array) $kepada)) {
					foreach ($kepada as $data_kepada) {
						$datakepada = [
							'NId' 			=> $this->input->post('girid'),
							'NKey' 			=> tb_key(),
							'GIR_Id' 		=> $this->input->post('nid'),
							'From_Id' 		=> $this->session->userdata('peopleid'),
							'RoleId_From' 	=> $this->session->userdata('roleid'),
							'To_Id' 		=> $this->db->query("SELECT PeopleId FROM people WHERE PrimaryRoleId = '" . $data_kepada . "'")->row()->PeopleId,
							'RoleId_To' 	=> $data_kepada,
							'ReceiverAs' 	=> 'to_notadinas',
							'Msg' 			=> get_data_konsepnaskah('Konten', $this->input->post('nid')),
							'StatusReceive' => 'unread',
							'ReceiveDate' 	=> $tanggal,
							'To_Id_Desc' 	=> $this->db->query("SELECT RoleDesc FROM role WHERE RoleId = '" . $data_kepada . "'")->row()->RoleDesc,
						];

						$save_kepada = $this->model_inboxreciever->store($datakepada);
					}
				}
				if (count((array) $tembusan)) {

					foreach ($tembusan as $data_tembusan) {
						if ($data_tembusan != '') {
							$datatembusan = [
								'NId' 			=> $this->input->post('girid'),
								'NKey' 			=> tb_key(),
								'GIR_Id' 		=> $this->input->post('nid'),
								'From_Id' 		=> $this->session->userdata('peopleid'),
								'RoleId_From' 	=> $this->session->userdata('roleid'),
								'To_Id' 		=> $this->db->query("SELECT PeopleId FROM people WHERE PrimaryRoleId = '" . $data_tembusan . "'")->row()->PeopleId,
								'RoleId_To' 	=> $data_tembusan,
								'ReceiverAs' 	=> 'bcc',
								'Msg' 			=> get_data_konsepnaskah('Konten', $this->input->post('nid')),
								'StatusReceive' => 'unread',
								'ReceiveDate' 	=> $tanggal,
								'To_Id_Desc' 	=> $this->db->query("SELECT RoleDesc FROM role WHERE RoleId = '" . $data_tembusan . "'")->row()->RoleDesc,
							];

							$save_tembusan = $this->model_inboxreciever->store($datatembusan);
						}
					}
				}
			}

			$this->db->where('NId_Temp', $this->input->post('nid'));
			$this->db->update('konsep_naskah', ['Konsep' => 2]);

			$data = array('status' => 'success',);
			echo json_encode($data);
		} catch (\Exception $e) {
			$data = array('status' => 'error',);
			echo json_encode($data);
		}
	}
	//Tutup kirim naskah dinas

	//Hapus naskah dinas tindaklanjut
	public function hapus_naskah_dinas_tindaklanjut($tipe, $NId)
	{
		try {
			$cari = $this->db->query("SELECT GIR_Id FROM inbox_receiver WHERE NId = '" . $NId . "' ORDER BY ReceiveDate DESC LIMIT 0,1")->row();

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('GIR_Id', $cari->GIR_Id);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 0]);

			$query1 	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '" . $NId . "'");
			foreach ($query1->result_array() as $row1) :
				$fileDir = $row1['NFileDir'];
			endforeach;

			if ($fileDir != '') {

				$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $this->input->post('temp') . "'");

				$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

				foreach ($query->result_array() as $row) :
					if (is_file($files . $row['FileName_fake']))
						unlink($files . $row['FileName_fake']);
				endforeach;
			}

			$query21 	= $this->db->query("SELECT QRCode FROM ttd WHERE NId = '" . $this->input->post('temp') . "'");

			$files21 = FCPATH . 'FilesUploaded/qrcode/';

			foreach ($query21->result_array() as $row21) :
				if (is_file($files21 . $row21['QRCode']))
					unlink($files21 . $row21['QRCode']);
			endforeach;

			$this->db->query("DELETE FROM `konsep_naskah` WHERE NId_Temp = '" . $this->input->post('temp') . "'");
			$this->db->query("DELETE FROM `ttd` WHERE NId = '" . $this->input->post('temp') . "'");
			$this->db->query("DELETE FROM `inbox_files` WHERE NId = '" . $this->input->post('temp') . "'");

			$data = array('status' => 'success',);
			echo json_encode($data);
		} catch (\Exception $e) {
			$data = array('status' => 'error',);
			echo json_encode($data);
		}
	}
	//Akhir Hapus naskah dinas tindaklanjut

	//Buka inbox teruskan
	public function inbox_teruskan($tipe, $NId)
	{
		$x = $this->input->post('xdt');
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			if (count((array) $this->input->post('tujuan'))) {
				foreach ($_POST['tujuan'] as $receiver) {
					$save_recievers = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $receiver,
						'RoleId_To' 	=> get_data_people('RoleId', $receiver),
						'ReceiverAs' 	=> 'to_forward',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $receiver),
					];

					$save_reciever = $this->model_inboxreciever->store($save_recievers);
				}
			}

			if (count((array) $this->input->post('tembusan'))) {
				foreach ($_POST['tembusan'] as $tembusan) {
					$data_tembusan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tembusan,
						'RoleId_To' 	=> get_data_people('RoleId', $tembusan),
						'ReceiverAs' 	=> 'bcc',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tembusan),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tembusan);
				}
			}

			if (!empty($_POST['upload_teruskan_name'])) {

				foreach ($_POST['upload_teruskan_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_teruskan_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_teruskan_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup inbox teruskan


//kirim keluar dinas
	public function inbox_kirim_keluar_dinas($tipe, $NId)
	{
		$x = $this->input->post('xdt');
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			if (count((array) $this->input->post('tujuan'))) {
				foreach ($_POST['tujuan'] as $receiver) {
					$save_recievers = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $receiver,
						'RoleId_To' 	=> get_data_people('RoleId', $receiver),
						'ReceiverAs' 	=> 'to',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $receiver),
					];

					$save_reciever = $this->model_inboxreciever->store($save_recievers);
				}
			}

			if (count((array) $this->input->post('tembusan'))) {
				foreach ($_POST['tembusan'] as $tembusan) {
					$data_tembusan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tembusan,
						'RoleId_To' 	=> get_data_people('RoleId', $tembusan),
						'ReceiverAs' 	=> 'bcc',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tembusan),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tembusan);
				}
			}

			if (!empty($_POST['upload_teruskan_name'])) {

				foreach ($_POST['upload_teruskan_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_teruskan_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_teruskan_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			//UPDATE STATUS inbox
			$this->db->where('NId', $NId);
		
			$this->db->update('inbox', ['Pengirim' => 'eksternal']);


			set_message('Data Berhasil Disimpan', 'success');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup inbox kirim keluar dinas

	//Buka inbox nota dinas
	public function inbox_nota_dinas($NId)
	{
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			if (count((array) $this->input->post('tujuan'))) {
				foreach ($_POST['tujuan'] as $receiver) {
					$save_recievers = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $receiver,
						'RoleId_To' 	=> get_data_people('RoleId', $receiver),
						'ReceiverAs' 	=> 'cc',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $receiver),
					];

					$save_reciever = $this->model_inboxreciever->store($save_recievers);
				}
			}

			if (count((array) $this->input->post('tembusan'))) {
				foreach ($_POST['tembusan'] as $tembusan) {
					$data_tembusan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tembusan,
						'RoleId_To' 	=> get_data_people('RoleId', $tembusan),
						'ReceiverAs' 	=> 'bcc',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tembusan),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tembusan);
				}
			}

			if (!empty($_POST['upload_notadinas_name'])) {

				foreach ($_POST['upload_notadinas_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_notadinas_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_notadinas_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $data_konsep['NId_Temp'],
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup inbox nota dinas

	//Buka inbox disposisi
	public function post_inbox_disposisi($tipe, $NId)
	{
		$x = $this->input->post('xdt');
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			$CI = &get_instance();
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
			$this->load->helper('string');
			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
			$config['errorlog']     = './uploads/'; //string, the default is application/logs/
			$config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
			$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
			$CI->ciqrcode->initialize($config);
			$image_name				= $NId . random_string('sha1', 10) . '-' . date('Ymd') . '.png'; //buat name dari qr code sesuai dengan


			$data_dispo = [
				'NId' 		=> $NId,
				'GIR_Id' 	=> $gir,
				'NoIndex' 	=> $this->input->post('NoIndex'),
				'Sifat' 	=> $this->input->post('Sifat'),
				'Disposisi' => implode('|', $this->input->post('Disposisi')),
				'RoleId' 	=> $this->session->userdata('roleid'),
				'QRCode' 	=> $image_name,
			];

			$params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/') . $data_dispo['GIR_Id']; //data yang akan di jadikan QR CODE
			$params['level'] 		= 'H'; //H=High
			$params['size'] 		= 10;
			$params['savename'] 	= FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
			$CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

			$save_dispo = $this->model_inbox_disposisi->store($data_dispo);

			if (count((array) $this->input->post('RoleId'))) {
				foreach ($_POST['RoleId'] as $tujuan) {
					$data_tujuan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tujuan,
						'RoleId_To' 	=> get_data_people('RoleId', $tujuan),
						'ReceiverAs' 	=> 'cc1',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tujuan),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tujuan);
				}
			}

			if (count((array) $this->input->post('tujuan_lainnya'))) {
				foreach ($_POST['tujuan_lainnya'] as $tujuanlain) {
					$data_tujuanlain = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tujuanlain,
						'RoleId_To' 	=> get_data_people('RoleId', $tujuanlain),
						'ReceiverAs' 	=> 'cc1',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tujuanlain),
					];

					$simpan_reciever = $this->model_inboxreciever->store($data_tujuanlain);
				}
			}


			if (!empty($_POST['upload_disposisi_name'])) {

				foreach ($_POST['upload_disposisi_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_disposisi_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_disposisi_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
			} else {
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId . '?dt=' . $x));
			}
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
			} else {
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId . '?dt=' . $x));
			}
		}
	}
	//Tutup inbox disposisi

	//inbox KOREKSI
	public function post_inbox_koreksi($tipe, $NId)
	{
		$x = $this->input->post('xdt');
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		try {
			$CI = &get_instance();
			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
			$this->load->helper('string');
			$config['cacheable']    = true; //boolean, the default is true
			$config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
			$config['errorlog']     = './uploads/'; //string, the default is application/logs/
			$config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
			$config['quality']      = true; //boolean, the default is true
			$config['size']         = '1024'; //interger, the default is 1024
			$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
			$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
			$CI->ciqrcode->initialize($config);
			$image_name				= $NId . random_string('sha1', 10) . '-' . date('Ymd') . '.png'; //buat name dari qr code sesuai dengan


			$data_dispo = [
				'NId' 		=> $NId,
				'GIR_Id' 	=> $gir,
				'NoIndex' 	=> $this->input->post('NoIndex'),
				'Koreksi' => implode('|', $this->input->post('Koreksi')),
				'RoleId' 	=> $this->session->userdata('roleid'),
				'QRCode' 	=> $image_name,
			];

			$params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/') . $data_dispo['GIR_Id']; //data yang akan di jadikan QR CODE
			$params['level'] 		= 'H'; //H=High
			$params['size'] 		= 10;
			$params['savename'] 	= FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
			$CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

			$save_dispo = $this->model_inbox_koreksi->store($data_dispo);

			if (count((array) $this->input->post('RoleId'))) {
				foreach ($_POST['RoleId'] as $tujuan) {
					$data_tujuan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tujuan,
						'RoleId_To' 	=> get_data_people('RoleId', $tujuan),
						'ReceiverAs' 	=> 'koreksi',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tujuan),
					];

					$simpan_reciever = $this->model_inboxreciever_koreksi->store($data_tujuan);
				}
			}

			if (count((array) $this->input->post('tujuan_lainnya'))) {
				foreach ($_POST['tujuan_lainnya'] as $tujuanlain) {
					$data_tujuanlain = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $tujuanlain,
						'RoleId_To' 	=> get_data_people('RoleId', $tujuanlain),
						'ReceiverAs' 	=> 'cc1',
						'Msg' 			=> $this->input->post('pesan'),
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> get_data_people('RoleDesc', $tujuanlain),
					];

					$simpan_reciever = $this->model_inboxreciever_koreksi->store($data_tujuanlain);
				}
			}


			if (!empty($_POST['upload_disposisi_name'])) {

				foreach ($_POST['upload_disposisi_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_disposisi_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploadedkoreksi/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_disposisi_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile_koreksi->store($save_files);
				}
			}

			//UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver_koreksi', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
			} else {
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_uk/view/' . $NId . '?dt=' . $x));
			}
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId . '?dt=' . $x));
			} else {
				redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_uk/view/' . $NId . '?dt=' . $x));
			}
		}
	}
	//Tutup inbox KOREKSI

	//Fungsi Lihat Pengatar Nota Dinas Tindaklanjut
	public function lihat_pengantar_naskah_dinas_tindaklanjut()
	{
		$this->load->library('HtmlPdf');
		ob_start();

		$tipe = $this->input->post('xdt2');
		$NId = $this->input->post('xdt1');

		$this->data['NId'] 			= $NId;
		$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$this->data['Jumlah'] 		= $this->input->post('Jumlah');
		$this->data['MeasureUnitId'] = $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $this->input->post('MeasureUnitId') . "'")->row()->MeasureUnitName;
		$this->data['Pesan'] 		= $this->input->post('Pesan');
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['Hal_pengantar'] 			= $this->input->post('Hal_pengantar');
		$this->data['Approve_People'] 			= (($this->input->post('TtdText') == 'none') ? $this->session->userdata('peopleid') : $this->input->post('Approve_People'));
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');

		$this->load->view('backend/standart/administrator/pdf/view_pengantar_nota_dinas_tindaklanjut_pdf', $this->data);

		$content = ob_get_contents();
		ob_end_clean();
		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 20),
		);


		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('pengantar_naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	// Akhir Fungsi Pengantar Nota Dinas Tindaklanjut

	//Fungsi Lihat Nota Dinas Tindaklanjut
	public function lihat_naskah_dinas_tindaklanjut()
	{
		$this->load->library('HtmlPdf');
		ob_start();

		$tipe = $this->input->post('xdt2');
		$NId = $this->input->post('xdt1');

		$this->data['NId'] 			= $NId;
		$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$this->data['Jumlah'] 		= $this->input->post('Jumlah');
		$this->data['MeasureUnitId'] = $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $this->input->post('MeasureUnitId') . "'")->row()->MeasureUnitName;
		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['Hal'] 			= $this->input->post('Hal');
		$this->data['Approve_People'] 			= (($this->input->post('TtdText') == 'none') ? $this->session->userdata('peopleid') : $this->input->post('Approve_People'));
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');

		if ($this->input->post('TtdText') == 'none') {
			$this->data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} else {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		$this->load->view('backend/standart/administrator/pdf/view_nota_dinas_tindaklanjut_pdf', $this->data);

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' => 'p',
			'format' => 'f4',
			'marges' 		=> array(30, 30, 20, 20),
		);


		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	// Akhir Fungsi Nota Dinas Tindaklanjut

	//Fungsi Nota Dinas Tindaklanjut
	public function post_naskah_dinas_tindaklanjut()
	{
		$x = $this->input->post('xdt');
		$tipe = $this->input->post('xdt2');
		$NId = $this->input->post('xdt1');
		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

		if ($this->input->post('TtdText') == 'none') {
			$xz = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$xz = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$xz = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		try {

			$data_konsep = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $NId,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Nota Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'ReceiverAs'       	=> 'to_notadinas',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '', //get_nomor_surat('to_nadin'),
				// 'Number'       		=> $data,
				'RoleCode'       	=> $this->session->userdata('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> ($this->input->post('RoleId_To') == NULL ? $this->input->post('RoleId_To') : implode(',', $this->input->post('RoleId_To'))),
				'RoleId_Cc'       	=> ($this->input->post('RoleId_Cc') == NULL ? $this->input->post('RoleId_Cc') : implode(',', $this->input->post('RoleId_Cc'))),
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'TtdText'       	=> $this->input->post('TtdText'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				// 'Hal_pengantar'     => $this->input->post('Hal_pengantar'),
				'NFileDir'     		=> 'naskah',
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'nadin',
				// 'Pesan'       		=> $this->input->post('Pesan'),
				// 'Nama_ttd_pengantar'=> $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".get_field_people('PeopleId')."'")->row()->ApprovelName,		
				'Nama_ttd_konsep'  	=> $xz,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];

			if (!empty($_POST['upload_naskahdinas_name'])) {
				foreach ($_POST['upload_naskahdinas_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['upload_naskahdinas_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_naskahdinas_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;
					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

			// 	UPDATE STATUS
			$this->db->where('NId', $NId);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1]);

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_dinas/' . $tipe . '/' . $NId . '?dt=' . $x));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	// Akhir Fungsi Nota Dinas Tindaklanjut

	//Fungsi Ubah Nota Dinas Tindaklanjut
	public function post_naskah_dinas_tindaklanjut_edit($tipe, $NId)
	{
		$x = $this->input->post('xdt');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

		if ($this->input->post('TtdText') == 'none') {
			$xz = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->session->userdata('peopleid') . "'")->row()->ApprovelName;
		} else {
			$xz = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('pdf') == '1') {
			$this->load->library('HtmlPdf');
			ob_start();

			$this->data['NId'] 				= $NId;
			$this->data['RoleId_To'] 		= $this->input->post('RoleId_To');
			$this->data['RoleId_Cc'] 		= $this->input->post('RoleId_Cc');
			$this->data['Konten'] 			= $this->input->post('Konten');
			$this->data['TglReg'] 			= $tanggal;
			$this->data['Hal'] 				= $this->input->post('Hal');
			$this->data['Approve_People'] 	= (($this->input->post('TtdText') == 'none') ? $this->session->userdata('peopleid') : $this->input->post('Approve_People'));
			$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;

			$this->load->view('backend/standart/administrator/pdf/view_nota_dinas_tindaklanjut_pdf', $this->data);

			$content = ob_get_contents();
			ob_end_clean();

			$config = array(
				'orientation' 	=> 'p',
				'format' 		=> 'f4',
				'marges' 		=> array(30, 30, 20, 20),
			);


			$this->pdf = new HtmlPdf($config);
			$this->pdf->initialize($config);
			$this->pdf->pdf->SetDisplayMode('fullpage');
			$this->pdf->writeHTML($content);
			$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
		}
		if ($this->input->post('pdf') == '2') {

			try {
				$naskah = $this->model_konsepnaskah->find($NId);

				$data_konsep = [
					'Jumlah'       		=> $this->input->post('Jumlah'),
					'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
					'SifatId'       	=> $this->input->post('SifatId'),
					'ClId'       		=> $this->input->post('ClId'),
					'RoleId_To'       	=> ($this->input->post('RoleId_To') == NULL ? $this->input->post('RoleId_To') : implode(',', $this->input->post('RoleId_To'))),
					'RoleId_Cc'       	=> ($this->input->post('RoleId_Cc') == NULL ? $this->input->post('RoleId_Cc') : implode(',', $this->input->post('RoleId_Cc'))),
					'Konten'       		=> $this->input->post('Konten'),
					'Approve_People'    => (($this->input->post('TtdText') == 'none') ? $this->session->userdata('peopleid') : $this->input->post('Approve_People')),
					'TtdText'       	=> $this->input->post('TtdText'),
					'Hal'       		=> $this->input->post('Hal'),
					'Nama_ttd_konsep'  	=> $xz,
					'lokasi'			=> $lok,
				];

				$this->db->where('NId_Temp', $NId);
				$this->db->update('konsep_naskah', $data_konsep);

				if (!empty($_POST['upload_naskahdinas_name'])) {

					foreach ($_POST['upload_naskahdinas_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['upload_naskahdinas_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['upload_naskahdinas_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;
						$save_files = [
							'FileKey' 			=> tb_key(),
							'GIR_Id' 			=> $NId,
							'NId' 				=> $naskah->GIR_Id,
							'PeopleID' 			=> $this->session->userdata('peopleid'),
							'PeopleRoleID' 		=> $this->session->userdata('roleid'),
							'FileName_real' 	=> $file_name,
							'FileName_fake' 	=> $test_title_name_copy,
							'FileStatus' 		=> 'available',
							'EditedDate'        => $tanggal,
							'Keterangan' 		=> '',
						];

						$save_file = $this->model_inboxfile->store($save_files);
					}
				}

				set_message('Data Berhasil Diubah', 'success');


				//xdt 4 --> Daftar Semua Disposisi
				if ($this->input->post('xdt') == '4') {
					redirect(BASE_URL('administrator/anri_mail_tl/view_naskah_dinas/' . $tipe . '/' . $naskah->GIR_Id . '?dt=' . $x));
				} else {
					redirect(BASE_URL('administrator/anri_list_notadinas_btl_ap'));
				}
			} catch (\Exception $e) {
				set_message('Gagal Merubah Data', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}
	}
	//Tutup Ubah Nota Dinas Tindaklanjut

	//Fungsi hapus file Ubah Nota Dinas Tindaklanjut
	public function hapus_file($NId, $name_file)
	{

		$files = glob('./FilesUploaded/naskah/' . $name_file);
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file);
		}

		$this->db->query("DELETE FROM inbox_files WHERE NId = '" . $NId . "' AND FileName_fake = '" . $name_file . "'");

		set_message('Data Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//Tutup hapus file Ubah Nota Dinas Tindaklanjut

	//Fungsi Hapus Nota Dinas Tindaklanjut
	public function hapus_naskah_dinas_tindaklanjut2($NId)
	{
		try {

			$x = $this->input->post('girid');

			$cari = $this->db->query("SELECT GIR_Id FROM inbox_receiver WHERE NId = '" . $this->input->post('girid') . "' ORDER BY ReceiveDate DESC LIMIT 0,1")->row()->GIR_Id;

			//UPDATE STATUS
			$this->db->where('NId', $x);
			$this->db->where('GIR_Id', $cari);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 0]);

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $this->input->post('temp') . "'");

			$files = FCPATH . 'FilesUploaded/naskah/';

			foreach ($query->result_array() as $row) :
				if (is_file($files . $row['FileName_fake']))
					unlink($files . $row['FileName_fake']);
			endforeach;

			$this->db->query("DELETE FROM `konsep_naskah` WHERE NId_Temp = '" . $this->input->post('temp') . "'");
			$this->db->query("DELETE FROM `inbox_files` WHERE NId = '" . $this->input->post('temp') . "'");

			$data = array('status' => 'success',);
			echo json_encode($data);
		} catch (\Exception $e) {
			$data = array('status' => 'error',);
			echo json_encode($data);
		}
	}
	//Tutup Hapus Nota Dinas Tindaklanjut

}
