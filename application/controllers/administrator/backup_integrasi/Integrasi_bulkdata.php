<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Integrasi_bulkdata extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		
	}

	// List Bahasa
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));


		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/bulk_data', $this->data);
	}
	// Tutup list bahasa
	
	public function tarik()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		ini_set('max_execution_time', 0); 

		$host = $this->db->hostname;
		$user = $this->db->username;
		$pass = $this->db->password;
		$db   = $this->db->database;

		// Create connection
		$conn = new mysqli($host, $user, $pass, $db);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
			
		//echo '<pre>';
		for ($x = 1; $x <= 35; $x++) {
			

			//$x = 1;
			//echo '<br>';
			$datana = 'Data Siap : ';
			//echo '<br>';
			// Read JSON file
			$json = file_get_contents('https://siap.jabarprov.go.id/api/get-pegawai?page='.$x);
			$json_data = json_decode($json,true);
			
				 foreach($json_data as $i) {
					if(is_array($i) || is_object($i)){
						foreach ($i as $h) {
							$percent = intval($x/35 * 100)."%"; 
							
							
							$sql1 = "SELECT peg_nip FROM siap_pegawai WHERE peg_nip='".$h['peg_nip']."'";
							$result = $conn->query($sql1);
			
							if ($result->num_rows == '0') {
								// output data of each row
								
								$sql = "INSERT INTO siap_pegawai (peg_nip, nip_atasan, peg_nama, nm_gol_akhir, jabatan_nama, eselon_nm, jabatan_id, unit_kerja_id, satuan_kerja_id, peg_foto_url, satuan_kerja_nama, unit_kerja_nama, jabatan_jenis, pangkat, peg_nama_jenis_pns, kedudukan_pns, is_atasan_bayangan, is_atasan_tugas_tambahan, peg_email)
								VALUES('".$h['peg_nip']."', '".$h['nip_atasan']."', '".str_replace("'","\'", $h['peg_nama'])."', '".$h['nm_gol_akhir']."', '".$h['jabatan_nama']."', '".$h['eselon_nm']."', '".$h['jabatan_id']."', '".$h['unit_kerja_id']."', '".$h['satuan_kerja_id']."', '".$h['peg_foto_url']."', '".$h['satuan_kerja_nama']."', '".$h['unit_kerja_nama']."', '".$h['jabatan_jenis']."', '".$h['pangkat']."', '".$h['peg_nama_jenis_pns']."', '".$h['kedudukan_pns']."', '".$h['is_atasan_bayangan']."', '".$h['is_atasan_tugas_tambahan']."', '".str_replace("'","\'", (str_replace("'-","-", $h['peg_email'])))."')";
							
								if ($conn->query($sql) === TRUE) {
									echo $datana .=  "Data ".$h['peg_nama']." Berhasil ditambahkan";
									echo '<br>';
								} else {
									echo '<br>';
									echo "Error: " . $sql . "<br>" . $conn->error;
									echo '<br>';
								}
							} else {
								$sql = "UPDATE siap_pegawai SET nip_atasan = '".$h['nip_atasan']."', peg_nama = '".str_replace("'","\'", $h['peg_nama'])."', nm_gol_akhir = '".$h['nm_gol_akhir']."', jabatan_nama ='".$h['jabatan_nama']."', eselon_nm = '".$h['eselon_nm']."', jabatan_id = '".$h['jabatan_id']."', unit_kerja_id=  '".$h['unit_kerja_id']."', satuan_kerja_id = '".$h['satuan_kerja_id']."', peg_foto_url = '".$h['peg_foto_url']."', satuan_kerja_nama = '".$h['satuan_kerja_nama']."', unit_kerja_nama = '".$h['unit_kerja_nama']."', jabatan_jenis = '".$h['jabatan_jenis']."' , pangkat = '".$h['pangkat']."', peg_nama_jenis_pns = '".$h['peg_nama_jenis_pns']."', kedudukan_pns = '".$h['kedudukan_pns']."', is_atasan_bayangan =  '".$h['is_atasan_bayangan']."', is_atasan_tugas_tambahan = '".$h['is_atasan_tugas_tambahan']."', peg_email = '".str_replace("'","\'", (str_replace("'-","-", $h['peg_email'])))." WHERE peg_nip=".$h['peg_nip']."";

								if ($conn->query($sql) === TRUE) {
									echo $datana =  "Data ".$h['peg_nama']." Berhasil diupdate";
									echo '<br>';
								} else {
									echo '<br>';
									echo "Error: " . $sql . "<br>" . $conn->error;
									echo '<br>';
								}
							}
							//$conn->close
							    echo '<script>
    parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
    parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">'.$percent.' is processed.</div>";</script>';
			//
						}
					}
			}
		}
		
		//$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/bulk_data', $this->data);
	}
}