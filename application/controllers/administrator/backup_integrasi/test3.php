<html>
<head>
    <title>Testing Otentikasi dengan SSO ke Zimbra</title>
</head>

<body>
    <h1>Testing connect to Zimbra</h1>

<?php
    /**
    * Globals. Can be stored in external config.inc.php or retreived from a DB.
    */
    $PREAUTH_KEY="fc0936a5c596d6d3053cfdb852e9eb24c7f748e301d83483bb8be059a2c972dc";
    $WEB_MAIL_PREAUTH_URL="http://mail.jabarprov.go.id/service/preauth";
	
    /**
    * User's email address and domain. In this example obtained from a GET query parameter. 
    * i.e. preauthExample.php?email=user@domain.com&domain=domain.com
    * You could also parse the email instead of passing domain as a separate parameter
    */
    //$user = $_GET["user"];
    $user = "aandriya";
    //$domain=$_GET["domain"];
    $domain = "jabarprov.go.id";
   	
    $email = "{$user}@{$domain}";

    if(empty($PREAUTH_KEY)) {
        die("Need preauth key for domain ".$domain);
    }
    
    /**
    * Create preauth token and preauth URL
    */
    $timestamp=time()*1000;
    $preauthToken=hash_hmac("sha1",$email."|name|0|".$timestamp,$PREAUTH_KEY);
    $preauthURL = $WEB_MAIL_PREAUTH_URL."?account=".$email."&by=name&timestamp=".$timestamp."&expires=0&preauth=".$preauthToken;
    
    /**
     * Redirect to Zimbra preauth URL
     */
    header("Location: $preauthURL");
?>

</body>
</html>