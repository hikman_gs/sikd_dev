<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_suratizin_btl extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_suratizin_btl');
	}

	//Buka List Undangan Belum TL
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Daftar Surat Izin Belum Tindaklanjut';
		$this->tempanri('backend/standart/administrator/surat_izin/list_btl', $this->data);
	}
	//Tutup List Undangan Belum TL			

	//Ambil Data Semua Surat Tugas Belum TL
	public function get_data_surattugas_btl()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_suratizin_btl->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;		

			if ($field->StatusReceive=='unread') {

				$x  = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '".$field->NId."'")->num_rows();
				if($x > 0) {
					$xx1 = "<b>".$this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp='".$field->NId."'")->row()->nosurat."</b>";
				} else {
					$xx1 ='';
				}

				$xx3 = "<b>".date('d-m-Y',strtotime($field->Tgl))."</b>";

				if($field->ReceiverAs=='bcc') {
					$xx4 = "<b>".$this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName."<font color=red> (Tembusan) </font></b>";	
				} else {
					$xx4 = "<b>".$this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName."</b>";					
				}

				$x1  = $this->db->query("SELECT Hal FROM konsep_naskah WHERE NId_Temp = '".$field->NId."'")->num_rows();
				if($x1 > 0) {
					$xx5 = "<b>".$this->db->query("SELECT Hal FROM konsep_naskah WHERE NId_Temp='".$field->NId."'")->row()->Hal."</b>";
				} else {
					$xx5 ='';
				}

			} else {

				$x  = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '".$field->NId."'")->num_rows();
				if($x > 0) {
					$xx1 = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp='".$field->NId."'")->row()->nosurat;
				} else {
					$xx1 ='';
				}

				$xx3 = date('d-m-Y',strtotime($field->Tgl));

				if($field->ReceiverAs=='bcc') {

					$xx4 = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName."<font color=red><b> (Tembusan) </b></font>";	
				} else {
					$xx4 = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName;					
				}

				$x1  = $this->db->query("SELECT Hal FROM konsep_naskah WHERE NId_Temp = '".$field->NId."'")->num_rows();
				if($x1 > 0) {
					$xx5 = $this->db->query("SELECT Hal FROM konsep_naskah WHERE NId_Temp='".$field->NId."'")->row()->Hal;
				} else {
					$xx5 ='';
				}

			}
			
			$row[] = $xx1;
			$row[] = $xx3;
			$row[] = $xx4;
			$row[] = $xx5;

			$row[] = '<a href="'.site_url('administrator/anri_mail_tl/view_naskah_masuk/view/'.$field->NId.'?dt=14').'" title="Lihat Detail Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_suratizin_btl->count_all(),
			"recordsFiltered" => $this->model_list_suratizin_btl->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Semua Surat Tugas Belum TL

}