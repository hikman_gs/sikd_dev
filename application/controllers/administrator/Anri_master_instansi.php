<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_instansi extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_instansi');
	}

	// List instansi
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_instansi/master_instansi_list', $this->data);
	}
	// Tutup list instansi

	// Tambah data instansi
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_instansi/master_instansi_add', $this->data);
	}
	// Tutup tambah data instansi

	// Proses simpan data instansi
	public function add_save()
	{

		$this->form_validation->set_rules('kode_instansi', 'Kode Instansi', 'trim|required|max_length[35]');
		$this->form_validation->set_rules('nama_instansi', 'Nama Instansi', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('nama_resmi', 'Nama Resmi', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('tipe_instansi', 'Tipe Instansi', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('tgl', 'Tgl', 'trim|required');
		$this->form_validation->set_rules('fungsi', 'Fungsi', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('mandat', 'Mandat', 'trim|required|max_length[250]');
		$this->form_validation->set_rules('status_instansi', 'Status Instansi', 'trim|required|max_length[2]');
		

		if ($this->form_validation->run()) {
			$table = "master_instansi";
			$save_data = [
				'kode_instansi' => $this->input->post('kode_instansi'),
				'nama_instansi' => $this->input->post('nama_instansi'),
				'nama_resmi' => $this->input->post('nama_resmi'),
				'tipe_instansi' => $this->input->post('tipe_instansi'),
				'tgl' => $this->input->post('tgl'),
				'fungsi' => $this->input->post('fungsi'),
				'mandat' => $this->input->post('mandat'),
				'status_instansi' => $this->input->post('status_instansi'),
				'creation_by' => $this->input->post('creation_by'),
				'created_date' => $this->input->post('created_date'),
			];


			
			$save_master_instansi = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_instansi'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_instansi'));
		}

	}
	// Tutup proses simpan data instansi

	// Edit data instansi
	public function update($id)
	{
		$this->data['master_instansi'] = $this->model_master_instansi->find($id);
		$this->tempanri('backend/standart/administrator/master_instansi/master_instansi_update', $this->data);
	}
	// Tutup edit data instansi

	// Proses update data instansi
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('kode_instansi', 'Kode Instansi', 'trim|required|max_length[35]');
		$this->form_validation->set_rules('nama_instansi', 'Nama Instansi', 'trim|required|max_length[250]');
		
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'kode_instansi' => $this->input->post('kode_instansi'),
				'nama_instansi' => $this->input->post('nama_instansi'),
				'nama_resmi' => $this->input->post('nama_resmi'),
				'tipe_instansi' => $this->input->post('tipe_instansi'),
				'tgl' => $this->input->post('tgl'),
				'fungsi' => $this->input->post('fungsi'),
				'mandat' => $this->input->post('mandat'),
				'status_instansi' => $this->input->post('status_instansi'),
			];

			
			$save_master_instansi = $this->model_master_instansi->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_instansi'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_instansi'));
		}

	}
	// Tutup proses update data instansi

	// Hapus data instansi
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_instansi'));			
	}
	// Tutup hapus data instansi

	// Proses hapus data instansi
	private function remove($id)
	{
		$master_instansi = $this->db->where('id',$id)->delete('master_instansi');
		return $master_instansi;
	}
	// Tutup proses hapus data instansi

}