<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_log_naskah_ttl extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_naskah_ttl');
		$this->load->model('model_inbox');
		$this->load->model('model_inboxfile');	
	}

	//Log Naskah Masuk
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/log_registrasi/log_naskah_ttl', $this->data);
	}
	//Tutup Log Naskah Masuk
	
	//Ambil Data Log Seluruh Naskah Masuk
	public function get_data_log_surat_all()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_ttl->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;

			if ($field->UrgensiName=='Amat Segera') {
				$xx = "<span class='badge bg-red'>".$field->UrgensiName."</span>";
			} elseif ($field->UrgensiName=='Segera') {
				$xx = "<span class='badge bg-red'>".$field->UrgensiName."</span>";
			} else {
				$xx = $field->UrgensiName;
			}
			$row[] = $xx;

			$row[] = $field->Nomor;
			$row[] = $field->Hal;
			$row[] = date('d-m-Y',strtotime($field->Tgl));
			$row[] = date('d-m-Y H:i:s',strtotime($field->NTglReg));

            $query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->NId."'");

            $files = BASE_URL.'/FilesUploaded/'.get_data_inbox('NFileDir',$field->NId).'/';

		  	$z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			  	foreach ($query->result_array() as $rowa) {
			  		$z1 .= "<tr>";
			    	$z1 .= "<td><a href='".$files.$rowa['FileName_fake']."' target='_new' title='".$rowa['FileName_fake']."' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			    	$z1 .= "</tr>";
			  	}
			$z1 .= "</table>";

			$row[] = $z1;

			if($field->CreationRoleId == $this->session->userdata('roleid')){
				$row[] = '<a href="'.site_url('administrator/anri_log_naskah_ttl/edit_naskah_masuk/log/'.$field->NId).'" title="Ubah Naskah Tanpa Tindaklanjut" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_log_naskah_ttl/hapus_naskah_masuk/'.$field->NId).'" title="Hapus Naskah" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
            }

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_ttl->count_all(),
			"recordsFiltered" => $this->model_list_naskah_ttl->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Log Seluruh Naskah Masuk	

	//Hapus naskah masuk
	public function hapus_naskah_masuk($NId)
	{
		$this->data['NId'] = $NId;

		$query1 	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '".$NId."'");				
		
		foreach($query1->result_array() as $row1):
			$fileDir = $row1['NFileDir'];
		endforeach;	

		if($fileDir != ''){

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$NId."'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach($query->result_array() as $row):
				if(is_file($files.$row['FileName_fake']))
					unlink($files.$row['FileName_fake']);
			endforeach;		
		}	

		$this->db->query("DELETE FROM `inbox_files` WHERE NId = '".$NId."'");
		$this->db->query("DELETE FROM `inbox` WHERE NId = '".$NId."'");

		$this->tempanri('backend/standart/administrator/log_registrasi/log_naskah_ttl', $this->data);
	}
	//Tutup Hapus naskah masuk

	// Edit naskah masuk ttl
	public function edit_naskah_masuk($tipe,$NId)
	{

		$this->data['tipe'] 	= $tipe;
		$this->data['people'] 	= $this->model_custome->get_people();
		$this->data['data']   	= $this->model_inbox->find($NId);
		$this->data['NId']   	= $NId;
		
		$this->tempanri('backend/standart/administrator/reg_naskah_masuk/inbox_edit_ttl', $this->data);
	}
	// Tutup Edit naskah masuk ttl

	// Edit naskah masuk ttl
	public function post_naskah_masuk_edit($tipe,$NId)
	{	

		$tanggal = date('Y-m-d H:i:s');

		try{
			$this->load->helper('string'); 
			$inb = $this->model_inbox->find($NId);
			$save_data = [
				'Tgl' 				=> date('Y-m-d',strtotime($this->input->post('Tgl'))),
				'JenisId' 			=> $this->input->post('JenisId'),
				'Nomor' 			=> $this->input->post('Nomor'),
				'Hal' 				=> $this->input->post('Hal'),
				'Instansipengirim' 	=> $this->input->post('Instansipengirim'),
				'UrgensiId' 		=> $this->input->post('UrgensiId'),
				'SifatId' 			=> $this->input->post('SifatId'),
				'BerkasId'			=> $this->input->post('BerkasId'),
			];

			$this->db->where('NId',$NId);
			$this->db->update('inbox',$save_data);
			
			if (count((array) $this->input->post('test_title_name'))) {
				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);


					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
			            'GIR_Id' 			=> $NId,
			            'NId' 				=> $NId,
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);

				}
			}

			set_message('Data Berhasil Disimpan', 'success');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_ttl'));
			}else{
				redirect(BASE_URL('administrator/anri_log_naskah_ttl'));
			}

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			if ($tipe == 'log') {
				redirect(BASE_URL('administrator/anri_log_naskah_ttl'));
			}else{
				redirect(BASE_URL('administrator/anri_log_naskah_ttl'));
			}
		}
	}
	// Tutup Edit naskah masuk ttl

	// Buka Hapus file Inbox ttl
	public function hapus_file2($NId,$name_file)
	{

		$query1 	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '".$NId."'");						
		foreach($query1->result_array() as $row1):
			$fileDir = $row1['NFileDir'];
		endforeach;	

		if($fileDir != ''){

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$NId."' AND FileName_fake = '".$name_file."'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach($query->result_array() as $row):
				if(is_file($files.$row['FileName_fake']))
					unlink($files.$row['FileName_fake']);
			endforeach;		
		}	


		$this->db->query("DELETE FROM inbox_files WHERE NId = '".$NId."' AND FileName_fake = '".$name_file."'");

		set_message('Data Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	// Tutup Hapus file inbox ttl

}