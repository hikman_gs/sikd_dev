<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_dashboard extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
	
		$this->load->model('model_jumlah');	
	}

	// Fungsi Cek Hak Akses
	function cek_hak_akses()
	{
        $check_access = $this->db->select('group_id, modul_name')->where(array('group_id'=>$this->session->userdata('groupid'),'modul_name'=>$this->uri->segment(2)))->get('modul_group')->num_rows();
        //cek akses
        if ($check_access>0) {
          	set_message('Anda Tidak Memiliki Akses Modul Aplikasi Ini', 'error');
          	redirect('administrator/anri_dashboard');
        }
	}
	// Akhir Fungsi Cek Hak Akses

	// Fungsi Halaman Dashboard
	public function index()
	{
		$tgl_sekarang = date('Y-m-d');

		$this->db->where('tgl_awal <=', $tgl_sekarang);
		$this->db->where('tgl_akhir >=', $tgl_sekarang);


		$query = $this->db->get('master_notice')->result();
		$query_tampil = $this->db->get('master_notice')->result_array();
		$this->data['pengumuman'] = $query;

		$judul_pengumuman = array_column($query_tampil, 'priority');

		if (in_array(0, $judul_pengumuman))
		  {
		  	$tampil_pengumuman = 1;
		  }else{
		  	$tampil_pengumuman = 0;
		  }

		if (in_array(1, $judul_pengumuman) OR in_array(2, $judul_pengumuman) OR in_array(3, $judul_pengumuman))
		  {
		  	$tampil_sofware_update = 1;
		  }else{
		  	$tampil_sofware_update = 0;
		  }

		  $this->data['tampil_pengumuman'] = $tampil_pengumuman;
		  $this->data['tampil_sofware_update'] = $tampil_sofware_update;
		  
		$this->template->title('Dashboard');		
		$this->tempanri('backend/standart/administrator/anri/anri_dashboard', $this->data);
	}
	// Akhir Fungsi Halaman Dashboard
	
	// Fungsi Halaman Ubah Password
	public function profile()
	{
		$this->template->title('Profil');
		$this->tempanri('backend/standart/administrator/profile/profile_view', $this->data);	
	}
	// Tutup Fungsi Halaman Ubah Password

	// Fungsi Ubah Kata Sandi
	public function change_password_profile()
	{
		$PeopleId = $this->session->userdata('peopleid');
		$old_password = $this->input->post('old_password');	
		$new_password = $this->input->post('new_password');

		$now_password = $this->db->get_where("people", array('PeopleId' =>$PeopleId))->row()->PeoplePassword;

		if (sha1($old_password)==$now_password) {			
			$data_update = array('PeoplePassword' => sha1($new_password));			
			$update_password_people = $this->db->where('PeopleId',$PeopleId)->update('people',$data_update);
			set_message('Password Berhasil Dirubah', 'success');
			redirect(BASE_URL('administrator/anri_logout'));
		} else {
			set_message('Password Lama Salah', 'error');
			redirect(BASE_URL('administrator/anri_dashboard/profile'));
		}		
	}
	// Tutup Fungsi Ubah Kata Sandi

	public function naskah_masuk_plt()
	{
		$this->data['inboxs'] = $this->db->query("SELECT i.UrgensiId,i.Nomor,i.Namapengirim,i.Tgl,i.NTglReg,i.NId FROM inbox_receiver AS r JOIN inbox AS i ON r.NId = i.NId WHERE ReceiverAs = 'to_nadin' AND RoleId_From = '".$this->session->userdata('roleid')."'")->result();
		$this->data['title'] = 'Naskah Masuk PLT/PLH';
		$this->tempanri('backend/standart/administrator/inbox/inbox_list', $this->data);
	}

	public function naskah_masuk_plt_regis()
	{
		$this->data['title'] = 'Registrasi Naskah Masuk PLT/PLH';
		$this->data['people'] = $this->model_custome->get_people();
		$this->tempanri('backend/standart/administrator/inbox/regis_plt', $this->data);
	}

	public function post_naskah_masuk_plt_regis()
	{
		set_message('Data Gagal Disimpan', 'warning');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());	
	}

	public function modul_group()
	{
		$this->cek_hak_akses();
		$this->template->title('Modul Access Group');
		$this->tempanri('backend/standart/administrator/modul_group/modul_group_list', $this->data);
	}

	public function modul_group_view()
	{
		$this->template->title('Modul Access Group View');
		$this->tempanri('backend/standart/administrator/modul_group/modul_group_view', $this->data);
	}

	public function modul_group_save()
	{
		$this->form_validation->set_rules('group_id', 'Group ', 'trim|required|max_length[11]');
		$this->form_validation->set_rules('modul_name', 'Modul ', 'trim|required|max_length[100]');
		

		if ($this->form_validation->run()) {
			$table = 'modul_group';
			$id = $this->input->post('group_id');

			$nama_controller = $this->db->query("SELECT nama_controller FROM table_modul WHERE nama_table = '".$this->input->post('modul_name')."'")->row()->nama_controller;

			$save_data = array(
				'group_id' => $id,
				'modul_name' => $this->input->post('modul_name'),
				'nama_controller' => $nama_controller,
			);
			$save_modul_group = $this->db->insert($table,$save_data);

      		set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_dashboard/modul_group_view/'.$id));
		} else {
			set_message('Gagal Menyimpan Data','error');
			redirect(BASE_URL('administrator/anri_dashboard/modul_group/'));
		}
	}

	public function modul_group_delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->modul_group_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->modul_group_remove($id);
			}
		}

		if ($remove) {
		    set_message('Data Berhasil Dihapus','success');
        } else {
            set_message(cclang('error_delete', 'master_bahasa'), 'error');
        }

		redirect_back();
	}

	private function modul_group_remove($id)
	{
		$modul_group = $this->db->where('id_mg',$id)->delete('modul_group');
		return $modul_group;
	}

	public function table_modul()
	{
		$this->template->title('Tabel Modul List');
		$this->tempanri('backend/standart/administrator/table_modul/table_modul_list', $this->data);
	}

	public function table_modul_add()
	{
		$this->template->title('Tabel Modul Add');
		$this->tempanri('backend/standart/administrator/table_modul/table_modul_add', $this->data);
	}

	public function table_modul_add_save()
	{
		$this->form_validation->set_rules('nama_table', 'nama_table', 'trim|required|max_length[100]');
		

		if ($this->form_validation->run()) {
			$table = 'table_modul';
			
			$save_data = [
					'level_modul' => $this->input->post('level_modul'),
					'id_child' => $this->input->post('id_child'),
					'nama_menu' => $this->input->post('nama_menu'),
					'nama_controller' => $this->input->post('nama_controller'),
					'nama_table' => $this->input->post('nama_table'),
			];
			
			
			$save_table_modul = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_dashboard/table_modul'));
		} else {
			set_message('Gagal Menyimpan Data','error');
			redirect(BASE_URL('administrator/anri_dashboard/table_modul'));

		}
	}

	public function table_modul_update($id)
	{
		$this->template->title('Bahasa Edit');
		$this->data['table_modul'] = $this->model_table_modul->find($id);
		$this->tempanri('backend/standart/administrator/table_modul/table_modul_update', $this->data);
	}

	public function table_modul_update_save($id)
	{
		$this->template->title('Bahasa Edit');
		
		$this->form_validation->set_rules('LangName', 'LangName', 'trim|required|max_length[20]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'LangName' => $this->input->post('LangName'),
			];

			
			$save_table_modul = $this->model_table_modul->change($id, $save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_dashboard/table_modul'));
		}
	}

	public function table_modul_delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->table_modul_remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->table_modul_remove($id);
			}
		}

		if ($remove) {
		    set_message('Data Berhasil Dihapus','success');
            //set_message(cclang('has_been_deleted', 'table_modul'), 'success');
        } else {
            set_message(cclang('error_delete', 'table_modul'), 'error');
        }

		redirect_back();
	}

	private function table_modul_remove($id)
	{
		$table_modul = $this->db->where('id_tm',$id)->delete('table_modul');
		return $table_modul;
	}



	public function usul_musnah()
	{
		//$this->cek_hak_akses();
		$this->tempanri('backend/standart/administrator/berkas/usul_musnah', $this->data);
	}

	public function musnah()
	{
		//$this->cek_hak_akses();
		$this->tempanri('backend/standart/administrator/berkas/musnah', $this->data);
	}

	public function usul_serah()
	{
		//$this->cek_hak_akses();
		$this->tempanri('backend/standart/administrator/berkas/usul_serah', $this->data);
	}

	public function serah()
	{
		//$this->cek_hak_akses();
		$this->tempanri('backend/standart/administrator/berkas/serah', $this->data);
	}
	
}