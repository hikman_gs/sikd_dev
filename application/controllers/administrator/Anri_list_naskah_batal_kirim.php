<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_naskah_batal_kirim extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_naskah_batal_kirim');	
	}

	//NASKAH MASUK SUDAH APPROVE
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Naskah Batal Dikirim';
		$this->tempanri('backend/standart/administrator/mail_tl/naskah_batal_kirim', $this->data);
	}
	//Tutup NASKAH MASUK SUDAH APPROVE

	//Ambil Data Seluruh Naskah Batal Dikirim
	public function get_data_naskah_bs()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_batal_kirim->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$field->JenisId."'")->row()->JenisName;	
			$row[] = $field->nosurat;		
			$row[] = date('d-m-Y',strtotime($field->TglNaskah));
			$row[] = $field->Hal;

			$xxz  = $this->db->query("SELECT ReceiverAs, RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->row();
			
			if($xxz->ReceiverAs == 'to_keluar') {

				$z1 = $xxz->RoleId_To;
				$z2 = '';

			} else {

				$count_tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

				if($count_tujuan > 0){

				$tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
					foreach ($tujuan as $key => $value) {					  
					  $exp = explode(',', $value->RoleId_To);
					  if($value->RoleId_To == '-'){
					  	$z1 = '';
					  } else {
					  	  $z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
						  	foreach ($exp as $k => $v) {
						  		$z1 .= "<tr>";
						    	$z1 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
						    	$z1 .= "</tr>";
						  	}
						  $z1 .= "</table>";	
					  }			  
					}
				}

				$count_tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

				if($count_tembusan > 0){

				$tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
					foreach ($tembusan as $key => $value) {					  
					  $exp = explode(',', $value->RoleId_Cc);
					  if(($value->RoleId_Cc == '') || ($value->RoleId_Cc == '-')){
					  	$z2 = '';
					  } else {
					  	  $z2 = "<br> <b>Tembusan :</b> <table border ='0' cellpadding='0' cellspacing='0'>";
						  	foreach ($exp as $k => $v) {
						  		$z2 .= "<tr>";
						    	$z2 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
						    	$z2 .= "</tr>";
						  	}
						  $z2 .= "</table>";	
					  }			  
					}
				}
			}
			
			$row[] = $z1.$z2;

          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	

			$row[] = $z21;
			
			$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->num_rows();

			if($x > 0) {

				$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->row();

				$row[] = '<a href="'.site_url('administrator/Anri_list_naskah_batal_kirim/balik_lagi/'.$field->NId_Temp.'/'.$field->GIR_Id).'" title="Membatalkan Pengiriman Naskah" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda ingin memindahkan naskah ini ke menu pengiriman ?\')"><i class="fa fa-check-circle"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';	

			} else {

				$row[] = '<a href="'.site_url('administrator/Anri_list_naskah_batal_kirim/balik_lagi/'.$field->NId_Temp.'/'.$field->GIR_Id).'" title="Membatalkan Pengiriman Naskah" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda ingin memindahkan naskah ini ke menu pengiriman ?\')"><i class="fa fa-check-circle"></i></a> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_sudah_approve/log_naskah_dinas_pdf/'.$field->NId_Temp).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> ';				
			
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_batal_kirim->count_all(),
			"recordsFiltered" => $this->model_list_naskah_batal_kirim->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Batal Dikirim

	//View PDF Naskah Dinas
	public function log_naskah_dinas_pdf($id)
	{
		$this->load->library('HtmlPdf');
      	ob_start();

      	$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$id."' ORDER BY TglNaskah DESC LIMIT 1")->row();

      	$this->data['NId'] 				= $id;
      	$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
      	$this->data['RoleId_To'] 		= $naskah->RoleId_To;
      	$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;
      	$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
      	$this->data['Konten'] 			= $naskah->Konten;
      	$this->data['Jumlah'] 			= $naskah->Jumlah;
      	$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$naskah->MeasureUnitId."'")->row()->MeasureUnitName;      	
      	$this->data['TglNaskah'] 		= $naskah->TglNaskah;
      	$this->data['Hal'] 				= $naskah->Hal;
      	$this->data['Approve_People'] 	= $naskah->Approve_People;
      	$this->data['TtdText'] 			= $naskah->TtdText;
      	$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
      	$this->data['Number'] 			= $naskah->Number;
      	$this->data['nosurat'] 			= $naskah->nosurat;
      	$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$naskah->ClId."'")->row()->ClCode;
      	$this->data['lokasi'] 			= $naskah->lokasi;
      	
	    $this->load->view('backend/standart/administrator/mail_tl/nadin_pdf',$this->data);
	    $content = ob_get_contents();
	    ob_end_clean();
        
          $config = array(
            'orientation' 	=> 'p',
            'format' 		=> 'F4',
            'marges' 		=> array(30, 30, 20, 30),
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('konsep_naskah.pdf', 'H');
	}
	//Akhir View PDF Naskah Dinas


	// Batal Kirim Naskah
	public function balik_lagi($NId,$GIR_Id)
	{

		$konsep = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_Temp = '".$NId."' AND GIR_Id = '".$GIR_Id."'")->row()->Konsep;

			//Konsep 0 = sudah approve
			$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Konsep' => '0']);

			set_message('Pemindahan Naskah Ke Menu Pengiriman Berhasil Dilakukan', 'success');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());	
	}
	// Akhir Batal Kirim Naskah

}