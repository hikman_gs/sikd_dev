<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Anri_list_naskah_belum_approve extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_list_naskah_ba');
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_konsepnaskah_koreksi');
		$this->load->model('model_inboxfile');
		$this->load->model('model_log');
		$this->load->model('model_inbox');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_ttd');
	}

	//List NASKAH MASUK BELUM APPROVE
	public function index()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['title'] = 'Naskah Belum Disetujui';
		$this->tempanri('backend/standart/administrator/mail_tl/naskah_belum_approve', $this->data);
	}
	//List NASKAH MASUK BELUM APPROVE

	//Ambil Data Seluruh Naskah Belum Disetujui
	public function get_data_naskah_ba()
	{
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_ba->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = date('d-m-Y', strtotime($field->TglReg));
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $field->JenisId . "'")->row()->JenisName;
			$row[] = $field->Hal;
			$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $field->NId_Temp . "' AND GIR_Id = '" . $field->GIR_Id . "' AND Keterangan != 'outbox'")->result();

			$files = BASE_URL . '/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {
				$z21 .= "<tr>";
				$z21 .= "<td><a href='" . $files . $value1->FileName_fake . "' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
				$z21 .= "</tr>";
			}
			$z21 .= "</table>";

			$row[] = $z21;
			//Tambahan kemsos
			//Cek no surat dulu, jika nomor surat beri ada maka tolah keluarkan pesan
			$konsep123 = $this->db->query("SELECT Number FROM konsep_naskah WHERE NId_Temp = '" . $field->NId_Temp . "' AND GIR_Id = '" . $field->GIR_Id . "'")->row();

			if ($konsep123->Number == '0') {

				$row[] = '<button type="button" onclick=ko_verifikasi(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Masukkan Kode Verifikasi" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button> <a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <button type="button" onclick=ko_teruskan_tu(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Dinomeri" class="btn btn-warning btn-sm"><i class="fa fa-sort-numeric-asc" aria-hidden="true"></i></button> <button type="button" onclick=ko_teruskan(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button>';
			} else {

				$row[] = '<button type="button" onclick=ko_verifikasi(' . $field->NId_Temp . ',' . $field->GIR_Id . ') title="Masukkan Kode Verifikasi" class="btn btn-success btn-sm"><i class="fa fa-check"></i></button></a>  <a target="_blank" href="' . site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/' . $field->GIR_Id) . '" title="Lihat Naskah Sudah di Nomori" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="' . site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/' . $field->NId_Temp) . '" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>';
			}

			$jenis_naskah = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $field->JenisId . "'")->row()->JenisName;
			if ($jenis_naskah == 'Nota Dinas') {
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_notadinas/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			}elseif($jenis_naskah == 'Surat Dinas'){
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_naskahdinas/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
		}elseif($jenis_naskah == 'Surat Edaran'){
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_suratedaran/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';
			}elseif($jenis_naskah == 'Surat Perintah'){
				$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_surattugas/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="' . site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $field->GIR_Id . '?dt=2') . '" title="Lihat Konsep Koreksi" class="btn btn-primary btn-sm"><i class="fa fa-arrow-circle-left" aria-hidden="true"></i></a> <a href="' . site_url('administrator/anri_mail_tl/iframe_histori/' . $field->GIR_Id) . '" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> </a> <a href="' . site_url('administrator/anri_list_naskah_belum_approve/hapus_nota_dinas_tindaklanjut/' . $field->NId_Temp) . '" title="Hapus Data" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';

			}
			$data[] = $row;

		}


		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_ba->count_all(),
			"recordsFiltered" => $this->model_list_naskah_ba->count_filtered(),
			"data" => $data,
		);

		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Belum Disetujui

	//PDF naskah dinas tindaklanjut
	public function naskahdinas_tindaklanjut_pdf($NId)
	{
		

		$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

		$this->data['NId'] 				= $NId;
		$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
		$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['lampiran'] 		= $naskah->lampiran;
		$this->data['Alamat']			= $naskah->Alamat;
		$this->data['RoleCode'] 		= $naskah->RoleCode;
		$this->data['Number']			= $naskah->Number;
		$this->data['Hal'] 				= $naskah->Hal;

		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
			$this->data['Jumlah'] 			= $naskah->Jumlah;
			$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}

		$this->data['TglReg'] 			= $naskah->TglReg;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Approve_People3'] 	= $naskah->Approve_People3;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxsprint') {
			
			// $this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);


			$this->load->library('Pdf');
			// $this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);
			$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

			$this->db->select("*");
			$this->db->where_in('PeopleId', explode(",",$this->data['RoleId_To']));
			$this->db->order_by('GroupId', 'ASC');
			$this->db->order_by('Pangkat', 'DESC');
			$this->db->order_by('Eselon ', 'ASC');
			$data_perintah = $this->db->get('people')->result();

			$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			//tcpdf
			$age = $this->session->userdata('roleid') == 'uk.1';
			$xx = $this->session->userdata('roleid');

			$r_atasan =  $this->session->userdata('roleatasan'); 
			$r_biro =  $this->session->userdata('groleid');
			$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3';


			$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px">
				<tr nobr="true">
					<td rowspan="7" width="60px" style="valign:bottom;">
					<br><br>
					<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
					<td width="180px">Ditandatangani secara elekronik oleh:</td>
				</tr>
				<tr nobr="true">
					<td width="180px" style="text-align: left; font-size:9;">'.get_data_people('RoleName', $this->data['Approve_People']).',</td>
				</tr>
				<tr nobr="true">
					<td><br></td>
				</tr>
				<tr nobr="true">
					<td>
						<p style="float: right!important;">'.$this->data['Nama_ttd_konsep'].'</p>
					</td>
				</tr>
				<tr nobr="true">
					<td>'.$tujuan. '
					</td>
				</tr>
			</table>';

			$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
			$pdf->SetPrintHeader(false);
			$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
			$pdf->SetMargins(30, 30, 30);
			$pdf->SetAutoPageBreak(TRUE, 30);
			// 
			$pdf->AddPage('P', 'F4');

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
			$pdf->Ln(10);
			$pdf->Cell(0,10,'SURAT PERINTAH',0,0,'C');
			$pdf->Ln(5);
			$pdf->Cell(0,10,'NOMOR : .........../'.$this->data['ClCode'].'/'.$this->data['RoleCode'],0,0,'C');
			$pdf->Ln(15);
			$pdf->Cell(30,5,'DASAR',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->writeHTMLCell(125, 5, '', '', $this->data['Hal'], '', 1, 0, true, 'J', true);
			$pdf->Cell(0,10,'MEMERINTAHKAN:',0,1,'C');
			$pdf->Ln(5);
			$pdf->Cell(30,5,'Kepada',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$i=0;
			foreach($data_perintah as $row){
				$i++;
				$pdf->SetX(65);
				$pdf->Cell(10,5,$i.'. ',0,0,'L');
				$pdf->SetX(75);
				$pdf->Cell(25,5,'Nama',0,0,'L');
				$pdf->Cell(5,5,':',0,0,'L');
				$pdf->MultiCell(95,5,$row->PeopleName,0,'L');
				$pdf->SetX(75);
				$pdf->Cell(25,5,'Pangkat',0,0,'L');
				$pdf->Cell(5,5,':',0,0,'L');
				$pdf->Cell(95,5,$row->Pangkat,0,1,'L');
				$pdf->SetX(75);
				$pdf->Cell(25,5,'NIP',0,0,'L');
				$pdf->Cell(5,5,':',0,0,'L');
				$pdf->Cell(95,5,$row->NIP,0,1,'L');
				$pdf->SetX(75);
				$pdf->Cell(25,5,'Jabatan',0,0,'L');
				$pdf->Cell(5,5,':',0,0,'L');
				$uc  = ucwords(strtolower($row->PeoplePosition));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $str);
				$pdf->MultiCell(85,5,$str,0,'L');
			}
			$pdf->Ln(10);
			$pdf->Cell(30,5,'Untuk',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->writeHTMLCell(90, 0, '', '', $this->data['Konten'], '', 1, 0, false, 'J', false);
			// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
			$pdf->Ln(10);
			$pdf->SetX(125);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30,5,'Ditetapkan di ..............',0,1,'L');
			$pdf->SetX(120);
			// $pdf->SetFont('Arial','',10);
			$pdf->Cell(30,5,'Pada tanggal ..................',0,1,'L');

			if($this->data['TtdText'] == 'PLT') {
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
			}elseif($this->data['TtdText'] == 'PLH') {
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
			}elseif($this->data['TtdText2'] == 'Atas_Nama') {
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,'a.n '.get_data_people('RoleName',$this->data['Approve_People3'].','),0,'C');
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
			}else{
				$pdf->SetX(100);
				$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People'].','),0,'C');
			}
			$pdf->Ln(5);
			$pdf->SetX(100);
			$pdf->MultiCell(90,10,'PEMERIKSA',0,'C');
			
			$pdf->SetX(100);
			$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

			$pdf->Output('surat_perintah_view.pdf','I');
			die();
		} else if ($naskah->Ket == 'outboxsprintgub') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_supergub_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxundangan') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_undangan_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxedaran') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_edaran_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxkeluar') {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_dinas_tindaklanjut_pdf', $this->data);
		} else {
			$this->load->library('HtmlPdf');
			ob_start();
			$this->load->view('backend/standart/administrator/pdf/view_konsep_naskah_dinas_tindaklanjut_pdf', $this->data);
		}

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' 	=> 'p',
			'format' 		=> 'F4',
			'marges' 		=> array(30, 30, 20, 20),
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	//Akhir PDF naskah dinas tindaklanjut	


	public function view_naskah_koreksi_pdf($NId)
	{
		$this->load->library('HtmlPdf');
		ob_start();
		$naskah = $this->db->query("SELECT * FROM konsep_naskah_koreksi WHERE id_koreksi = '" . $NId . "' LIMIT 1")->row();
		$this->data['NId'] 				= $NId;
		$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
		$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
		$this->data['RoleId_To'] 		= $naskah->RoleId_To;
		$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['lampiran'] 		= $naskah->lampiran;
		$this->data['Konten'] 			= $naskah->Konten;
		$this->data['Alamat']			= $naskah->Alamat;
		$this->data['RoleCode'] 		= $naskah->RoleCode;
		$this->data['Number']			= $naskah->Number;
		$this->data['Hal'] 				= $naskah->Hal;

		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
			$this->data['Jumlah'] 			= $naskah->Jumlah;
			$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
			
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}

		$this->data['TglReg'] 			= $naskah->TglReg;
		$this->data['Approve_People'] 	= $naskah->Approve_People;
		$this->data['TtdText'] 			= $naskah->TtdText;
		$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
		$this->data['TtdText2'] 			= $naskah->TtdText2;
		$this->data['Approve_People3'] 	= $naskah->Approve_People3;
		$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;

		if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_koreksi_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxsprint') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxsprintgub') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_supergub_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxundangan') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_undangan_tindaklanjut_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxkeluar') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_dinas_koreksi_pdf', $this->data);
		} else if ($naskah->Ket == 'outboxedaran') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_edaran_tindaklanjut_pdf', $this->data);
		} else {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_naskah_dinas_tindaklanjut_pdf', $this->data);
		}

		$content = ob_get_contents();
		ob_end_clean();

		$config = array(
			'orientation' 	=> 'p',
			'format' 		=> 'F4',
			'marges' 		=> array(30, 30, 20, 20),
		);

		$this->pdf = new HtmlPdf($config);
		$this->pdf->initialize($config);
		$this->pdf->pdf->SetDisplayMode('fullpage');
		$this->pdf->writeHTML($content);
		$this->pdf->Output('naskah_dinas_tindaklanjut-' . $NId . '.pdf', 'H');
	}
	//Akhir PDF view koreksi	


public function edit_naskahdinas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_dinas/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Naskah Dinas Edit
	public function edit_notadinas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/nota_dinas/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Naskah Dinas Edit
	public function edit_surattugas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_tugas/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Naskah Dinas Edit
	public function edit_suratedaran($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/surat_edaran/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Edit naskah dinas
	public function postedit_notadinas($NId)
	{
		$gir = $NId;
		$tanggal = date('Y-m-d H:i:s');

		$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

		if ($naskah1->Ket != 'outboxsprint') {
			$sifat =  $this->input->post('SifatId');
		} else {
			$sifat =  NULL;
		}

		if ($naskah1->Ket == 'outboxkeluar') {
			$alamat =  $this->input->post('Alamat');

			$role = $this->input->post('RoleId_To');
		} elseif ($naskah1->Ket == 'outboxedaran') {
			$alamat =  $this->input->post('Alamat');
			$role = $this->input->post('RoleId_To');
		} else {
			$alamat =  NULL;
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
		}

			if ($naskah1->Ket == 'outboxkeluar') {
			$ket =  'outboxkeluar';
			} elseif ($naskah1->Ket == 'outboxedaran') {
				$ket =  'outboxedaran';
			} elseif ($naskah1->Ket == 'outboxnotadinas') {
				$ket =  'outboxnotadinas';
			} elseif ($naskah1->Ket == 'outboxsprint') {
				$ket =  'outboxsprint';
			} else {

			}


		try {
			$CI = &get_instance();
			$this->load->helper('string');

			$data_konsep = [
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'SifatId'       	=> $sifat,
				'Alamat'       		=> $alamat,
				'ClId'       		=> $this->input->post('ClId'),
				'RoleId_To'       	=> $role,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'lampiran'       	=> $this->input->post('lampiran'),
				'Hal'       		=> $this->input->post('Hal'),
			];

			//menyimpan record koreksi pada konsep naskah koreksi
				$girid_ir = $this->session->userdata('peopleid').date('dmyhis');
				
				$data_konsep2 = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'id_koreksi'		=> $girid_ir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $role,
				'Alamat'       		=> $alamat,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'lampiran'       		=> $this->input->post('lampiran'),		
				'Approve_People'       	=> $this->db->query("SELECT Approve_People FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People,
				'Approve_People3'    => $b,
				'TtdText'       	=> $this->db->query("SELECT TtdText FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText,
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> $ket,
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep'   => $this->db->query("SELECT Nama_ttd_konsep FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_konsep,

				'Nama_ttd_atas_nama' 	=> $a,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];

			$this->db->where('NId_Temp', $NId);
			$this->db->update('konsep_naskah', $data_konsep);

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'RoleCode'       	=> $this->input->post('rolecode'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outboxkeluar',
					];
					$save_file = $this->model_inboxfile->store($save_files);
				}

					//penambahan koreksi
					if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploadedkoreksi/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

				

						}
				}
				//akhir penambahan koreksi
			}


			//penambahan koreksi2
			$save_konsep2 	= $this->model_konsepnaskah_koreksi->store($data_konsep2);
				//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid').date('dmyhis');
					
				$save_recievers = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				
				'To_Id' 		=> $this->db->query("SELECT To_Id FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->To_Id,
				'RoleId_To'       	=> $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->RoleId_To,
				'id_koreksi'		=>	$girid_ir,
				'ReceiverAs' 	=> 'to_koreksi',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'read',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];
			$save_reciever2 = $this->model_inboxreciever_koreksi->store($save_recievers);
			//akhir penambahan koreksi2

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup Edit naskah dinas

	// public function postedit_notadinas($NId)
	// {
	// 	$gir = $NId;
	// 	$tanggal = date('Y-m-d H:i:s');

	// 	$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

	// 	if ($naskah1->Ket != 'outboxsprint') {
	// 		$sifat =  $this->input->post('SifatId');
	// 	} else {
	// 		$sifat =  NULL;
	// 	}

	// 	if ($naskah1->Ket == 'outboxkeluar') {
	// 		$alamat =  $this->input->post('Alamat');

	// 		$role = $this->input->post('RoleId_To');
	// 	} elseif ($naskah1->Ket == 'outboxedaran') {
	// 		$alamat =  $this->input->post('Alamat');
	// 		$role = $this->input->post('RoleId_To');
	// 	} else {
	// 		$alamat =  NULL;
	// 		$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
	// 	}

	// 	try {
	// 		$CI = &get_instance();
	// 		$this->load->helper('string');

	// 		$data_konsep = [
	// 			'Jumlah'       		=> $this->input->post('Jumlah'),
	// 			'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
	// 			'UrgensiId'       	=> $this->input->post('UrgensiId'),
	// 			'SifatId'       	=> $sifat,
	// 			'Alamat'       		=> $alamat,
	// 			'ClId'       		=> $this->input->post('ClId'),
	// 			'RoleId_To'       	=> $role,
	// 			'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
	// 			'Konten'       		=> $this->input->post('Konten'),
	// 			'lampiran'       	=> $this->input->post('lampiran'),
	// 			'Hal'       		=> $this->input->post('Hal'),
	// 		];

	// 		$this->db->where('NId_Temp', $NId);
	// 		$this->db->update('konsep_naskah', $data_konsep);

	// 		if (!empty($_POST['test_title_name'])) {

	// 			foreach ($_POST['test_title_name'] as $idx => $file_name) {
	// 				$test_title_name_copy = date('Ymd') . '-' . $file_name;

	// 				//untuk merename nama file 
	// 				rename(
	// 					FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
	// 					FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
	// 				);

	// 				//untuk menghapus folder upload sementara setelah simpan data dieksekusi
	// 				rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

	// 				$listed_image[] = $test_title_name_copy;

	// 				$save_files = [
	// 					'FileKey' 			=> tb_key(),
	// 					'GIR_Id' 			=> $gir,
	// 					'NId' 				=> $NId,
	// 					'PeopleID' 			=> $this->session->userdata('peopleid'),
	// 					'PeopleRoleID' 		=> $this->session->userdata('roleid'),
	// 					'RoleCode'       	=> $this->input->post('rolecode'),
	// 					'FileName_real' 	=> $file_name,
	// 					'FileName_fake' 	=> $test_title_name_copy,
	// 					'FileStatus' 		=> 'available',
	// 					'EditedDate'        => $tanggal,
	// 					'Keterangan' 		=> 'outboxkeluar',
	// 				];
	// 				$save_file = $this->model_inboxfile->store($save_files);
	// 			}
	// 		}

	// 		set_message('Data Berhasil Disimpan', 'success');
	// 		redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
	// 	} catch (\Exception $e) {
	// 		set_message('Gagal Menyimpan Data', 'error');
	// 		$this->load->library('user_agent');
	// 		redirect($this->agent->referrer());
	// 	}
	// }
	//Tutup Edit naskah dinas

	//hapus file notadinas registrasi
	public function hapus_file5($NId, $name_file)
	{

		$files = glob('./FilesUploaded/naskah/' . $name_file);
		foreach ($files as $file) {
			if (is_file($file))
				unlink($file);
		}

		$this->db->query("DELETE FROM inbox_files WHERE NId = '" . $NId . "' AND FileName_fake = '" . $name_file . "'");

		set_message('Data Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//tutup hapus file notadinas registrasi

	//Hapus nota dinas registrasi
	public function hapus_nota_dinas_tindaklanjut($NId)
	{
		$query1 	= $this->db->query("SELECT NFileDir FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'");
		foreach ($query1->result_array() as $row1) :
			$fileDir = $row1['NFileDir'];
		endforeach;

		if ($fileDir != '') {

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '" . $NId . "'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach ($query->result_array() as $row) :
				if (is_file($files . $row['FileName_fake']))
					unlink($files . $row['FileName_fake']);
			endforeach;
		}

		$this->db->query("DELETE FROM `konsep_naskah` WHERE NId_Temp = '" . $NId . "'");
		$this->db->query("DELETE FROM `inbox_files` WHERE NId = '" . $NId . "'");

		redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
	}
	//Akhir Hapus nota dinas registrasi

	/*// Approve Naskah
	public function approve_naskah()
	{

		$NId = $this->input->post('NId_Temp');
		$GIR_Id = $this->input->post('GIR_Id');

		$tanggal = date('Y-m-d H:i:s');
		//Tambahan kemsos
		//Cek no surat dulu, jika nomor surat beri ada maka tolah keluarkan pesan
		$konsep123 = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row();

		if ($konsep123->nosurat == '') {

			set_message('Nomor naskah wajib diisi, Silahkan berikan nomor naskah dahulu', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} else {


			$this->load->library('user_agent');
			get_data_konsepnaskah('NId_Temp', $NId);
			$password = $this->input->post('password');
			$where 	  = array('PeoplePassword' => sha1($password),);
			$cek 	  = $this->db->get_where("people", $where)->num_rows();

			$kalimat = str_replace(' ', '', $password);
			if ($kalimat == '') {
				set_message('Password Wajib Diisi', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			} else {

				if ($cek > 0) {

					$konsep = $this->db->query("SELECT ReceiverAs FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row()->ReceiverAs;

					//Untuk menghitung nomor nota dinas
					//$tahun 	= date('Y');
					//$query = $this->db->query("SELECT (max(Number) + 1) as id FROM konsep_naskah WHERE ReceiverAs = '".$konsep."' AND YEAR(TglNaskah) = '".$tahun."' AND RoleId_From = '".$this->session->userdata('roleid')."'")->row()->id;

					if (!empty($query[0])) {
						$id = $query;
					} else {
						$id = 1;
					}

					$nomor = $this->input->post('nonaskah');

					$tahun 	= date('Y');
					$bulan = date('m');

					$cari = $this->db->query("SELECT ClId FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->ClId;

					$klas = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $cari . "'")->row()->ClCode;

					$unit = $this->db->query("SELECT RoleCode FROM role WHERE RoleCode  = '" . $unit  . "'")->row()->RoleCode;

					// $nosurat = $nomor . '/' . $klas . '/' . $unit;

					$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id', $GIR_Id)->update('konsep_naskah', ['Konsep' => '0']);

					// $logopath = './uploads/anri.png'; //Logo yang akan di insert

					$CI = &get_instance();
					$this->load->library('ciqrcode');
					$this->load->helper('string');

					$config['cacheable']    = true; //boolean, the default is true
				        $config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
				        $config['errorlog']     = './uploads/'; //string, the default is application/logs/
				        $config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
				        $config['quality']      = true; //boolean, the default is true
				        $config['size']         = '1024'; //interger, the default is 1024
				        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
				        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
				        $CI->ciqrcode->initialize($config);

						$image_name				= $NId.'-'.date('Ymd').'.png'; //buat name dari qr code sesuai dengan

				        // $params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/').$NId; //data yang akan di jadikan QR CODE
				      	// $params['data'] 		= base_url('verifikasi_dokumen'); //data yang akan di jadikan QR CODE
						$params['data'] 		= base_url('verifikasi_dokumen'); 
				        $params['level'] 		= 'H'; //H=High
				        $params['size'] 		= 10;
				        $params['savename'] 	= FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
				        $CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

						$fullpath = FCPATH.$config['imagedir'].$image_name;

			            $QR = imagecreatefrompng($fullpath);
			             // memulai menggambar logo dalam file qrcode
			             $logo = imagecreatefromstring(file_get_contents($logopath));
			             imagecolortransparent($logo , imagecolorallocatealpha($logo , 0, 0, 0, 127));
			 
			             imagealphablending($logo , false);
			             imagesavealpha($logo , true);
			 
			             $QR_width = imagesx($QR);//get logo width
			             $QR_height = imagesy($QR);//get logo width
			 
			             $logo_width = imagesx($logo);
			             $logo_height = imagesy($logo);
			             
			            // Scale logo to fit in the QR Code
			             $logo_qr_width = $QR_width/4;
			             $scale = $logo_width/$logo_qr_width;
			             $logo_qr_height = $logo_height/$scale;

			             //220 -> left,  210 -> top
			             imagecopyresampled($QR, $logo, 220, 210, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);		 
			             // imagecopyresampled($QR, $logo, $QR_width/4, $QR_height/3.5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
			             // 
			             // Simpan kode QR lagi, dengan logo di atasnya
			             
			             imagepng($QR,$fullpath);

			             $kode_verifikasi = random_string('sha1',10);

				        //Simpan QRCode
						$data_ttd = [
								'NId' 				=> $NId,
								'PeopleId' 			=> $this->session->userdata('peopleid'),
								'RoleId' 			=> $this->session->userdata('roleid'),
								'Verifikasi'  		=> $kode_verifikasi,
								'Identitas_Akses' 	=> '',
								'QRCode' 			=> $image_name,
								'TglProses' 		=> date('Y-m-d H:i:s'),
							];

						$save_ttd 		= $this->model_ttd->store($data_ttd);

						//Update Konsep = 0
						//Konsep 0 untuk sudah approve, 1 baru registrasi, 2 sudah dikirim
						$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Konsep' => 0 ]);


					//Create PDF File
					$this->load->library('HtmlPdf');
					ob_start();

					$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

					$this->data['NId'] 				= $NId;
					$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
					$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
					$this->data['RoleId_To'] 		= $naskah->RoleId_To;
					$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
					$this->data['Konten'] 			= $naskah->Konten;
					$this->data['lampiran'] 		= $naskah->lampiran;
					$this->data['RoleCode'] 		= $naskah->RoleCode;
					$this->data['Hal'] 				= $naskah->Hal;
					$this->data['Alamat'] 			= $naskah->Alamat;

					if ($naskah->Ket != 'outboxsprint') {
						$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
						$this->data['Jumlah'] 			= $naskah->Jumlah;
						$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
									
					}

					if ($naskah->Ket == 'outbox') {
						$this->data['Alamat'] 			= $naskah->Alamat;
					}

					$this->data['TglNaskah'] 		= $naskah->TglNaskah;
					$this->data['Approve_People'] 	= $naskah->Approve_People;
					$this->data['Approve_People3'] 	= $naskah->Approve_People3;
					$this->data['TtdText'] 			= $naskah->TtdText;
					$this->data['TtdText2'] 			= $naskah->TtdText2;
					$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
					$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
					$this->data['Number'] 			= $naskah->Number;
					$this->data['nosurat'] 			= $naskah->nosurat;
					$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;
					$this->data['lokasi'] 			= $naskah->lokasi;
					$this->data['id_dokumen'] 		= $kode_verifikasi;

					if ($naskah->Ket == 'outboxnotadinas') {
						$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf', $this->data);
					} else if ($naskah->Ket == 'outboxsprint') {
						$this->load->view('backend/standart/administrator/mail_tl/surattugas_inisiatif_pdf', $this->data);
					} else if ($naskah->Ket == 'outboxundangan') {
						$this->load->view('backend/standart/administrator/mail_tl/undangan_inisiatif_pdf', $this->data);
					} else if ($naskah->Ket == 'outboxedaran') {
						$this->load->view('backend/standart/administrator/mail_tl/suratedaran_inisiatif_pdf', $this->data);
					} else if ($naskah->Ket == 'outboxkeluar') {
						$this->load->view('backend/standart/administrator/mail_tl/suratdinas_inisiatif_pdf', $this->data);
					} else {
						$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf', $this->data);
					}

					$content = ob_get_contents();
					ob_end_clean();

					$config = array(
						'orientation' 	=> 'p',
						'format' 		=> 'F4',
						'marges' 		=> array(30, 30, 20, 30),
					);
					//(_,_,KANAN,_)//

					$this->pdf = new HtmlPdf($config);
					$this->pdf->initialize($config);
					$this->pdf->pdf->SetDisplayMode('fullpage');
					$this->pdf->writeHTML($content);

					if ($naskah->Ket == 'outboxnotadinas') {
						$a = 'nota_dinas-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf', 'F');
					} else if ($naskah->Ket == 'outboxsprint') {
						$a = 'sprint-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/sprint-' . $NId . '.pdf', 'F');
					} else if ($naskah->Ket == 'outboxundangan') {
						$a = 'undangan-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/undangan-' . $NId . '.pdf', 'F');
					} else if ($naskah->Ket == 'outboxedaran') {
						$a = 'surat_edaran-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/surat_edaran-' . $NId . '.pdf', 'F');

					} else if ($naskah->Ket == 'outboxkeluar') {
						$a = 'surat_dinas-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/surat_dinas-' . $NId . '.pdf', 'F');
					} else {
						$a = 'nadin_lain-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/nadin_lain-' . $NId . '.pdf', 'F');
					}

					//Simpan Inbox File
					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $GIR_Id,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $a,
						'FileName_fake' 	=> $a,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outbox',
						'Id_dokumen' 		=> $kode_verifikasi,
					];

					$save_file = $this->model_inboxfile->store($save_files);

					//Update Status
					$this->db->where('NId', $NId);
					$this->db->where('RoleId_To', $this->session->userdata('roleid'));
					$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);

					set_message('Data Berhasil Disimpan', 'success');
					$this->load->library('user_agent');
					redirect($this->agent->referrer());
				} else {
					set_message('Kode Verifikasi Salah', 'error');
					redirect($this->agent->referrer());
				}
			}
		}
	}
	//Tutup Approve Naskah */

	/* // Approve Naskah
	public function approve_naskah()
	{

		$NId = $this->input->post('NId_Temp');
		$GIR_Id = $this->input->post('GIR_Id');

		$tanggal = date('Y-m-d H:i:s');
		//Tambahan kemsos
		//Cek no surat dulu, jika nomor surat beri ada maka tolah keluarkan pesan
		$konsep123 = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row();

		if ($konsep123->nosurat == '') {

			set_message('Nomor naskah wajib diisi, Silahkan berikan nomor naskah dahulu', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} else {


			$this->load->library('user_agent');
			get_data_konsepnaskah('NId_Temp', $NId);
			$password = $this->input->post('password');
			$where 	  = array('PeoplePassword' => sha1($password),);
			$cek 	  = $this->db->get_where("people", $where)->num_rows();

			$kalimat = str_replace(' ', '', $password);
			if ($kalimat == '') {
				set_message('Password Wajib Diisi', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			} else {

				if ($cek > 0) {

					$konsep = $this->db->query("SELECT ReceiverAs FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row()->ReceiverAs;

					//Untuk menghitung nomor nota dinas
					//$tahun 	= date('Y');
					//$query = $this->db->query("SELECT (max(Number) + 1) as id FROM konsep_naskah WHERE ReceiverAs = '".$konsep."' AND YEAR(TglNaskah) = '".$tahun."' AND RoleId_From = '".$this->session->userdata('roleid')."'")->row()->id;

					if (!empty($query[0])) {
						$id = $query;
					} else {
						$id = 1;
					}

					/* $nomor = $this->input->post('nonaskah');

					$tahun 	= date('Y');
					$bulan = date('m');

					$cari = $this->db->query("SELECT ClId FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->ClId;

					$klas = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $cari . "'")->row()->ClCode;

					$unit = $this->db->query("SELECT RoleCode FROM role WHERE RoleCode  = '" . $unit  . "'")->row()->RoleCode;

					// $nosurat = $nomor . '/' . $klas . '/' . $unit; 

					$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id', $GIR_Id)->update('konsep_naskah', ['Konsep' => '0']);

					// $logopath = './uploads/anri.png'; //Logo yang akan di insert

					$CI = &get_instance();
					$this->load->library('ciqrcode');
					$this->load->helper('string');

					$config['cacheable']    = true; //boolean, the default is true
				        $config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
				        $config['errorlog']     = './uploads/'; //string, the default is application/logs/
				        $config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
				        $config['quality']      = true; //boolean, the default is true
				        $config['size']         = '1024'; //interger, the default is 1024
				        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
				        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
				        $CI->ciqrcode->initialize($config);

						$image_name				= $NId.'-'.date('Ymd').'.png'; //buat name dari qr code sesuai dengan

				        // $params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/').$NId; //data yang akan di jadikan QR CODE
				      	// $params['data'] 		= base_url('verifikasi_dokumen'); //data yang akan di jadikan QR CODE
						$params['data'] 		= base_url('verifikasi_dokumen'); 
				        $params['level'] 		= 'H'; //H=High
				        $params['size'] 		= 10;
				        $params['savename'] 	= FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
				        $CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

						$fullpath = FCPATH.$config['imagedir'].$image_name;

			            $QR = imagecreatefrompng($fullpath);
			             // memulai menggambar logo dalam file qrcode
			             $logo = imagecreatefromstring(file_get_contents($logopath));
			             imagecolortransparent($logo , imagecolorallocatealpha($logo , 0, 0, 0, 127));
			 
			             imagealphablending($logo , false);
			             imagesavealpha($logo , true);
			 
			             $QR_width = imagesx($QR);//get logo width
			             $QR_height = imagesy($QR);//get logo width
			 
			             $logo_width = imagesx($logo);
			             $logo_height = imagesy($logo);
			             
			            // Scale logo to fit in the QR Code
			             $logo_qr_width = $QR_width/4;
			             $scale = $logo_width/$logo_qr_width;
			             $logo_qr_height = $logo_height/$scale;

			             //220 -> left,  210 -> top
			             imagecopyresampled($QR, $logo, 220, 210, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);		 
			             // imagecopyresampled($QR, $logo, $QR_width/4, $QR_height/3.5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
			             // 
			             // Simpan kode QR lagi, dengan logo di atasnya
			             
			             imagepng($QR,$fullpath);

			             $kode_verifikasi = random_string('sha1',10);

				        //Simpan QRCode
						$data_ttd = [
								'NId' 				=> $NId,
								'PeopleId' 			=> $this->session->userdata('peopleid'),
								'RoleId' 			=> $this->session->userdata('roleid'),
								'Verifikasi'  		=> $kode_verifikasi,
								'Identitas_Akses' 	=> '',
								'QRCode' 			=> $image_name,
								'TglProses' 		=> date('Y-m-d H:i:s'),
							];

						$save_ttd 		= $this->model_ttd->store($data_ttd);

						//Update Konsep = 0
						//Konsep 0 untuk sudah approve, 1 baru registrasi, 2 sudah dikirim
						$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Konsep' => 0 ]);


					//Create PDF File
					$this->load->library('HtmlPdf');
					ob_start();

					$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

					$this->data['NId'] 				= $NId;
					$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
					$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
					$this->data['RoleId_To'] 		= $naskah->RoleId_To;
					$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
					$this->data['Konten'] 			= $naskah->Konten;
					$this->data['lampiran'] 			= $naskah->lampiran;
					$this->data['RoleCode'] 		= $naskah->RoleCode;
					$this->data['Hal'] 				= $naskah->Hal;
					$this->data['Alamat'] 			= $naskah->Alamat;

					if ($naskah->Ket != 'outboxsprint') {
						$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
						$this->data['Jumlah'] 			= $naskah->Jumlah;
						$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
									
					}

					if ($naskah->Ket == 'outboxkeluar') {
						$this->data['Alamat'] 			= $naskah->Alamat;
					}

					$this->data['TglNaskah'] 		= $naskah->TglNaskah;
					$this->data['Approve_People'] 	= $naskah->Approve_People;
					$this->data['Approve_People3'] 	= $naskah->Approve_People3;
					$this->data['TtdText'] 			= $naskah->TtdText;
					$this->data['TtdText2'] 			= $naskah->TtdText2;
					$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
					$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
					$this->data['Number'] 			= $naskah->Number;
					$this->data['nosurat'] 			= $naskah->nosurat;
					$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;
					$this->data['lokasi'] 			= $naskah->lokasi;
					$this->data['id_dokumen'] 		= $kode_verifikasi;

					if ($naskah->Ket == 'outboxnotadinas') {
						$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf', $this->data);
					} else if ($naskah->Ket == 'outboxsprint') {
						$this->load->view('backend/standart/administrator/mail_tl/surattugas_inisiatif_pdf', $this->data);
					} else if ($naskah->Ket == 'outboxundangan') {
						$this->load->view('backend/standart/administrator/mail_tl/undangan_inisiatif_pdf', $this->data);
					} else if ($naskah->Ket == 'outboxedaran') {
						$this->load->view('backend/standart/administrator/mail_tl/suratedaran_inisiatif_pdf', $this->data);
					} else if ($naskah->Ket == 'outboxkeluar') {
						$this->load->view('backend/standart/administrator/mail_tl/suratdinas_inisiatif_pdf', $this->data);
					} else {
						$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf', $this->data);
					}

					$content = ob_get_contents();
					ob_end_clean();

					$config = array(
						'orientation' 	=> 'p',
						'format' 		=> 'F4',
						'marges' 		=> array(30, 30, 20, 30),
					);
					//(_,_,KANAN,_)//

					$this->pdf = new HtmlPdf($config);
					$this->pdf->initialize($config);
					$this->pdf->pdf->SetDisplayMode('fullpage');
					$this->pdf->writeHTML($content);

					if ($naskah->Ket == 'outboxnotadinas') {
						$a = 'nota_dinas-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf', 'F');
					} else if ($naskah->Ket == 'outboxsprint') {
						$a = 'sprint-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/sprint-' . $NId . '.pdf', 'F');
					} else if ($naskah->Ket == 'outboxundangan') {
						$a = 'undangan-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/undangan-' . $NId . '.pdf', 'F');
					} else if ($naskah->Ket == 'outboxedaran') {
						$a = 'surat_edaran-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/surat_edaran-' . $NId . '.pdf', 'F');

					} else if ($naskah->Ket == 'outboxkeluar') {
						$a = 'surat_dinas-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/surat_dinas-' . $NId . '.pdf', 'F');
					} else {
						$a = 'nadin_lain-' . $NId . '.pdf';
						$this->pdf->Output('FilesUploaded/naskah/nadin_lain-' . $NId . '.pdf', 'F');
					}

					//Simpan Inbox File
					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $GIR_Id,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $a,
						'FileName_fake' 	=> $a,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outbox',
						'Id_dokumen' 		=> $kode_verifikasi,
					];

					$save_file = $this->model_inboxfile->store($save_files);

					//Update Status
					$this->db->where('NId', $NId);
					$this->db->where('RoleId_To', $this->session->userdata('roleid'));
					$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);

					set_message('Data Berhasil Disimpan', 'success');
					$this->load->library('user_agent');
					redirect($this->agent->referrer());
				} else {
					set_message('Kode Verifikasi Salah', 'error');
					redirect($this->agent->referrer());
				}
			}
		}
	}
	//Tutup Approve Naskah */


	// Approve Naskah
	public function approve_naskah()
	{
		$NId = $this->input->post('NId_Temp');
		$GIR_Id = $this->input->post('GIR_Id');

		//user number identification
		$nik = $this->session->userdata('nik');
		// $nik = '3273280809690005';


		//Tambahan kemsos
		//Cek no surat dulu, jika nomor surat beri ada maka tolah keluarkan pesan
		$konsep123 = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' AND GIR_Id = '" . $GIR_Id . "'")->row();

		if ($konsep123->nosurat == '') {

			set_message('Nomor naskah wajib diisi, Silahkan berikan nomor naskah dahulu', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		} else {

			$tanggal = date('Y-m-d H:i:s');

			$this->load->library('user_agent');
			get_data_konsepnaskah('NId_Temp', $NId);

			$password = $this->input->post('password');

			$kalimat = str_replace(' ', '', $password);

			if ($kalimat == '') {
				set_message('Passphrase Wajib Diisi', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			} else {

				//Proses cek NIK pada API Cloud BSRE
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => "http://103.122.5.59/api/user/status/" . $nik,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "GET",
					CURLOPT_HTTPHEADER => array(
						"Authorization: Basic U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=",
						"Cookie: JSESSIONID="
					),
				));

				$response = curl_exec($curl);
				curl_close($curl);

				$result = json_decode($response);

				if ($result->status_code == 1111) {

					//Jika NIK terdaftar, Cek Passphrase
					//Proses cek Passphrase pada API Cloud BSRE

					//url api sertifikat elektronik
					$url_signed = 'http://103.122.5.59/api/sign/pdf';

					//passphrase for the certificate
					$passphrase = $this->input->post('password');

					//curl initialization
					$curl = curl_init();

					$asal = FCPATH . 'FilesUploaded/example/example.pdf';

					curl_setopt_array($curl, array(
						CURLOPT_URL => $url_signed,
						CURLOPT_RETURNTRANSFER => true,
						CURLOPT_ENCODING => "",
						CURLOPT_MAXREDIRS => 10,
						CURLOPT_TIMEOUT => 0,
						CURLOPT_FOLLOWLOCATION => true,
						CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
						CURLOPT_CUSTOMREQUEST => "POST",
						CURLOPT_POSTFIELDS => array('file' => new CURLFILE($asal, 'application/pdf'), 'nik' => $nik, 'passphrase' => $passphrase, 'tampilan' => 'invisible', 'page' => '1', 'image' => 'false', 'linkQR' => 'https://google.com', 'xAxis' => '0', 'yAxis' => '0', 'width' => '200', 'height' => '100'),
						CURLOPT_HTTPHEADER => array(
							"Authorization: Basic U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=",
							"Cookie: JSESSIONID="
						),
					));

					$response = curl_exec($curl);
					//Fungsi untuk mengambil nilai http request
					$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
					//===
					curl_close($curl);

					$result = json_decode($response);

					// $x = $httpCode;
					// echo $x;
					// die();

					if ($httpCode == '200') {

						//Jika Passphrase Benar
						// $logopath = './uploads/anri.png'; //Logo yang akan di insert

						$CI = &get_instance();
						$this->load->library('ciqrcode');
						$this->load->helper('string');

						$config['cacheable']    = true; //boolean, the default is true
						$config['cachedir']     = './uploads/tmp/'; //string, the default is application/cache/
						$config['errorlog']     = './uploads/'; //string, the default is application/logs/
						$config['imagedir']     = './FilesUploaded/qrcode/'; //direktori penyimpanan qr code
						$config['quality']      = true; //boolean, the default is true
						$config['size']         = '1024'; //interger, the default is 1024
						$config['black']        = array(224, 255, 255); // array, default is array(255,255,255)
						$config['white']        = array(70, 130, 180); // array, default is array(0,0,0)
						$CI->ciqrcode->initialize($config);

						$image_name				= $NId . '-' . date('Ymd') . '.png'; //buat name dari qr code sesuai dengan

						// $params['data'] 		= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/').$NId; //data yang akan di jadikan QR CODE
						$params['data'] 		= base_url('verifikasi_dokumen');
						$params['level'] 		= 'H'; //H=High
						$params['size'] 		= 10;
						$params['savename'] 	= FCPATH . $config['imagedir'] . $image_name; //simpan image QR CODE ke folder assets/images/
						$CI->ciqrcode->generate($params); // fungsi untuk generate QR CODE

						$fullpath = FCPATH . $config['imagedir'] . $image_name;

						$QR = imagecreatefrompng($fullpath);
						// memulai menggambar logo dalam file qrcode
						$logo = imagecreatefromstring(file_get_contents($logopath));
						imagecolortransparent($logo, imagecolorallocatealpha($logo, 0, 0, 0, 127));

						imagealphablending($logo, false);
						imagesavealpha($logo, true);

						$QR_width = imagesx($QR); //get logo width
						$QR_height = imagesy($QR); //get logo width

						$logo_width = imagesx($logo);
						$logo_height = imagesy($logo);

						// Scale logo to fit in the QR Code
						$logo_qr_width = $QR_width / 4;
						$scale = $logo_width / $logo_qr_width;
						$logo_qr_height = $logo_height / $scale;

						//220 -> left,  210 -> top
						imagecopyresampled($QR, $logo, 220, 210, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
						// imagecopyresampled($QR, $logo, $QR_width/4, $QR_height/3.5, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
						// 
						// Simpan kode QR lagi, dengan logo di atasnya

						imagepng($QR, $fullpath);

						$kode_verifikasi = random_string('sha1', 10);

						//Simpan QRCode
						$data_ttd = [
							'NId' 				=> $NId,
							'PeopleId' 			=> $this->session->userdata('peopleid'),
							'RoleId' 			=> $this->session->userdata('roleid'),
							'Verifikasi'  		=> $kode_verifikasi,
							'Identitas_Akses' 	=> '',
							'QRCode' 			=> $image_name,
							'TglProses' 		=> date('Y-m-d H:i:s'),
						];

						$save_ttd 		= $this->model_ttd->store($data_ttd);

						//Update Konsep = 0
						//Konsep 0 untuk sudah approve, 1 baru registrasi, 2 sudah dikirim
						$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id', $GIR_Id)->update('konsep_naskah', ['Konsep' => 0]);

						//Create PDF File
						$this->load->library('HtmlPdf');
						ob_start();

						$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

						$this->data['NId'] 				= $NId;
						$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
						$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId . "'")->row()->JenisName;
						$this->data['RoleId_To'] 		= $naskah->RoleId_To;
						$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
						$this->data['Konten'] 			= $naskah->Konten;
						$this->data['RoleCode'] 		= $naskah->RoleCode;
						$this->data['Hal'] 				= $naskah->Hal;
						$this->data['Alamat'] 			= $naskah->Alamat;

						if ($naskah->Ket != 'outboxsprint') {
							$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '" . $naskah->SifatId . "'")->row()->SifatName;
							$this->data['Jumlah'] 			= $naskah->Jumlah;
							$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '" . $naskah->MeasureUnitId . "'")->row()->MeasureUnitName;
						}

						if ($naskah->Ket == 'outboxkeluar') {
							$this->data['Alamat'] 			= $naskah->Alamat;
						}

						$this->data['TglNaskah'] 		= $naskah->TglNaskah;
						$this->data['Approve_People'] 	= $naskah->Approve_People;
						$this->data['Approve_People3'] 	= $naskah->Approve_People3;
						$this->data['TtdText'] 			= $naskah->TtdText;
						$this->data['TtdText2'] 			= $naskah->TtdText2;
						$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
						$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
						$this->data['Number'] 			= $naskah->Number;
						$this->data['nosurat'] 			= $naskah->nosurat;
						$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $naskah->ClId . "'")->row()->ClCode;
						$this->data['lokasi'] 			= $naskah->lokasi;
						$this->data['id_dokumen'] 		= $kode_verifikasi;

						if ($naskah->Ket == 'outboxnotadinas') {
							$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf', $this->data);
						} else if ($naskah->Ket == 'outboxsprint') {
							$this->load->view('backend/standart/administrator/mail_tl/surattugas_inisiatif_pdf', $this->data);
						} else if ($naskah->Ket == 'outboxsprintgub') {
							$this->load->view('backend/standart/administrator/mail_tl/supergub_inisiatif_pdf', $this->data);
						} else if ($naskah->Ket == 'outboxundangan') {
							$this->load->view('backend/standart/administrator/mail_tl/undangan_inisiatif_pdf', $this->data);
						} else if ($naskah->Ket == 'outboxedaran') {
							$this->load->view('backend/standart/administrator/mail_tl/suratedaran_inisiatif_pdf', $this->data);
						} else if ($naskah->Ket == 'outboxkeluar') {
							$this->load->view('backend/standart/administrator/mail_tl/suratdinas_inisiatif_pdf', $this->data);
						} else {
							$this->load->view('backend/standart/administrator/mail_tl/nodin_inisiatif_pdf', $this->data);
						}

						$content = ob_get_contents();
						ob_end_clean();

						$config = array(
							'orientation' 	=> 'p',
							'format' 		=> 'F4',
							'marges' 		=> array(30, 30, 20, 20),
						);
						//(_,_,KANAN,_)//


						$this->pdf = new HtmlPdf($config);
						$this->pdf->initialize($config);
						$this->pdf->pdf->SetDisplayMode('fullpage');
						$this->pdf->writeHTML($content);

						if ($naskah->Ket == 'outboxnotadinas') {
							$a = 'nota_dinas-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxsprint') {
							$a = 'sprint-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/sprint-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxsprintgub') {
							$a = 'sprint-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/sprintgub-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxundangan') {
							$a = 'undangan-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/undangan-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxedaran') {
							$a = 'surat_edaran-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/surat_edaran-' . $NId . '.pdf', 'F');
						} else if ($naskah->Ket == 'outboxkeluar') {
							$a = 'surat_dinas-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/surat_dinas-' . $NId . '.pdf', 'F');
						} else {
							$a = 'nadin_lain-' . $NId . '.pdf';
							$this->pdf->Output('FilesUploaded/naskah/nadin_lain-' . $NId . '.pdf', 'F');
						}

						//Simpan Inbox File
						$save_files = [
							'FileKey' 			=> tb_key(),
							'GIR_Id' 			=> $GIR_Id,
							'NId' 				=> $NId,
							'PeopleID' 			=> $this->session->userdata('peopleid'),
							'PeopleRoleID' 		=> $this->session->userdata('roleid'),
							'FileName_real' 	=> $a,
							'FileName_fake' 	=> $a,
							'FileStatus' 		=> 'available',
							'EditedDate'        => $tanggal,
							'Keterangan' 		=> 'outbox',
							'Id_dokumen' 		=> $kode_verifikasi,
						];

						$save_file = $this->model_inboxfile->store($save_files);

						//Update Status
						$this->db->where('NId', $NId);
						$this->db->where('RoleId_To', $this->session->userdata('roleid'));
						$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);

						//============= Sertifikat Elektronik Menggunakan Esign-Client-Gateway PHP
						//url api sertifikat elektronik
						$url_signed = 'http://103.122.5.59/api/sign/pdf';

						//loads a PDF file 
						if ($naskah->Ket == 'outboxnotadinas') {
							$asal = FCPATH . 'FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsprint') {
							$asal = FCPATH . 'FilesUploaded/naskah/sprint-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsprintgub') {
							$asal = FCPATH . 'FilesUploaded/naskah/sprintgub-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxundangan') {
							$asal = FCPATH . 'FilesUploaded/naskah/undangan-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxedaran') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_edaran-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxkeluar') {
							$asal = FCPATH . 'FilesUploaded/naskah/surat_dinas-' . $NId . '.pdf';
						} else {
							$asal = FCPATH . 'FilesUploaded/naskah/nadin_lain-' . $NId . '.pdf';
						}

						//passphrase for the certificate
						$passphrase = $this->input->post('password');

						//save pdf
						if ($naskah->Ket == 'outboxnotadinas') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/nota_dinas-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsprint') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/sprint-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxsprintgub') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/sprintgub-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxundangan') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/undangan-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxedaran') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_edaran-' . $NId . '.pdf';
						} else if ($naskah->Ket == 'outboxkeluar') {
							$tujuan = FCPATH . 'FilesUploaded/naskah/surat_dinas-' . $NId . '.pdf';
						} else {
							$tujuan = FCPATH . 'FilesUploaded/naskah/nadin_lain-' . $NId . '.pdf';
						}

						//curl initialization
						$curl = curl_init();

						curl_setopt_array($curl, array(
							CURLOPT_URL => $url_signed,
							CURLOPT_RETURNTRANSFER => true,
							CURLOPT_ENCODING => "",
							CURLOPT_MAXREDIRS => 10,
							CURLOPT_TIMEOUT => 0,
							CURLOPT_FOLLOWLOCATION => true,
							CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
							CURLOPT_CUSTOMREQUEST => "POST",
							CURLOPT_POSTFIELDS => array('file' => new CURLFILE($asal, 'application/pdf'), 'nik' => $nik, 'passphrase' => $passphrase, 'tampilan' => 'invisible', 'page' => '1', 'image' => 'false', 'linkQR' => 'https://google.com', 'xAxis' => '0', 'yAxis' => '0', 'width' => '200', 'height' => '100'),
							CURLOPT_HTTPHEADER => array(
								"Authorization: Basic U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=",
								"Cookie: JSESSIONID="
							),
						));

						$response = curl_exec($curl);
						file_put_contents($tujuan, $response);
						curl_close($curl);
						//============= Akhir Sertifikat Elektronik Menggunakan Esign-Client-Gateway PHP

						//penambahan log dispu
						$this->load->model('model_log');
						$this->model_log->simpan_user();
						$data['notifikasi'] = 'Data berhasil disimpan';
						$data['judul'] = 'Insert Data Berhasil';
						$this->load->view('notifikasi', $data);
						//akhir penambahan dispu

						set_message('Data Berhasil Disimpan', 'success');
						$this->load->library('user_agent');
						redirect($this->agent->referrer());
					} else {

						$this->load->model('model_log');
						$this->model_log->gagal_user();
						$data['notifikasi'] = 'Data berhasil disimpan';
						$data['judul'] = 'Insert Data Berhasil';
						$this->load->view('notifikasi', $data);
						//akhir penambahan dispu


						//Jika Passphrase Salah
						set_message('Passphrase Anda Salah, Silahkan Coba Lagi', 'error');
						redirect($this->agent->referrer());
					}
				} else {

					//Jika NIK Tidak Terdaftar
					set_message('NIK Anda Belum Terdaftar', 'error');
					redirect($this->agent->referrer());
				}
			}
		}
	}
	//Tutup Approve Naskah */

	//Fungsi Teruskan Draft Naskah Dinas
	public function k_teruskan()
	{

		$nid = $this->input->post('NId_Temp2');
		$girid = $this->input->post('GIR_Id2');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_sekdis') . "' AND GroupId= '4'")->row()->ApprovelName;
		} elseif ($this->input->post('TtdText') == 'UK') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_lainya') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$y = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$y = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_sekdis') . "'")->row()->ApprovelName;
		} else {
			$y = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$v =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$v = $this->input->post('tmpid_sekdis');
		} else {
			$v = $this->input->post('Approve_People');
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();
		if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		} else if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x, 'TtdText' => $this->input->post('TtdText'), 'Nama_ttd_konsep' => $z]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Fungsi Teruskan Draft Naskah Dinas

	// Beri Nomor
	public function nomor_dong()
	{


		$NId = $this->input->post('NId_Temp3');
		$GIR_Id = $this->input->post('GIR_Id3');

		$nomor = $this->input->post('nonaskah');

		$tahun 	= date('Y');
		$bulan = date('m');

		$cari = $this->db->query("SELECT ClId FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->ClId;

		$klas = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $cari . "'")->row()->ClCode;

		$unit = $this->db->query("SELECT RoleCode FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->RoleCode;

		$nosurat = $nomor . '/' . $klas . '/' . $unit;

		$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id', $GIR_Id)->update('konsep_naskah', ['Number' => $nomor, 'nosurat' => $nosurat]);

		set_message('Nomor Berhasil Disimpan', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//Tutup Beri Nomor	

	//kirim ke tu
	public function k_teruskan_tu()
	{
		$nid = $this->input->post('NId_Temp4');
		$girid = $this->input->post('GIR_Id4');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		} else if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
}
