<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Integrasi_bulkdata extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		$this->load->model('Model_integrasi', 'integrasi');
		
	}

	// List Bahasa
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));


		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/bulk_data', $this->data);
	}
	// Tutup list bahasa

	public function tarik()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		ini_set('max_execution_time', 0); 

		$host = $this->db->hostname;
		$user = $this->db->username;
		$pass = $this->db->password;
		$db   = $this->db->database;

		// Create connection
		$conn = new mysqli($host, $user, $pass, $db);
		// Check connection
		if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
		}
			
		//echo '<pre>';
		///////Testing 04-Dec-2020 By And	
		$jmlPage = 34;
		for ($x = 1; $x <= $jmlPage; $x++) {
			

			//$x = 1;
			//echo '<br>';
			$datana = 'Data Siap : ';
			//echo '<br>';
			// Read JSON file
			$json = file_get_contents('https://siap.jabarprov.go.id/api/get-pegawai?page='.$x.'&perpage=1000');
			$json_data = json_decode($json,true);
			
				 foreach($json_data as $i) {
					if(is_array($i) || is_object($i)){
						foreach ($i as $h) {
							$percent = number_format($x/$jmlPage * 100,1)."%"; 
							
							
							$sql1 = "SELECT peg_nip FROM siap_pegawai WHERE peg_nip='".$h['peg_nip']."'";
							$result = $conn->query($sql1);
			
							if ($result->num_rows == '0') {
								// output data of each row
								
								$sql = "INSERT INTO siap_pegawai (peg_nip, nip_atasan, peg_nama, nm_gol_akhir, jabatan_nama, eselon_nm, jabatan_id, unit_kerja_id, satuan_kerja_id, peg_foto_url, satuan_kerja_nama, unit_kerja_nama, jabatan_jenis, pangkat, peg_nama_jenis_pns, kedudukan_pns, is_atasan_bayangan, is_atasan_tugas_tambahan, peg_email)
								VALUES('".$h['peg_nip']."', '".$h['nip_atasan']."', '".str_replace("'","\'", $h['peg_nama'])."', '".$h['nm_gol_akhir']."', '".$h['jabatan_nama']."', '".$h['eselon_nm']."', '".$h['jabatan_id']."', '".$h['unit_kerja_id']."', '".$h['satuan_kerja_id']."', '".$h['peg_foto_url']."', '".$h['satuan_kerja_nama']."', '".$h['unit_kerja_nama']."', '".$h['jabatan_jenis']."', '".$h['pangkat']."', '".$h['peg_nama_jenis_pns']."', '".$h['kedudukan_pns']."', '".$h['is_atasan_bayangan']."', '".$h['is_atasan_tugas_tambahan']."', '".str_replace("'","\'", (str_replace("'-","-", $h['peg_email'])))."')";
							
								if ($conn->query($sql) === TRUE) {
									echo $datana .=  "Data ".$h['peg_nama']." Berhasil ditambahkan";
									echo '<br>';
								} else {
									echo '<br>';
									echo "Error: " . $sql . "<br>" . $conn->error;
									echo '<br>';
								}
							} else {
								
								/*$sql = "UPDATE siap_pegawai SET nip_atasan = '".$h['nip_atasan']."', peg_nama = '".str_replace("'","\'", $h['peg_nama'])."', nm_gol_akhir = '".$h['nm_gol_akhir']."', jabatan_nama ='".$h['jabatan_nama']."', eselon_nm = '".$h['eselon_nm']."', jabatan_id = '".$h['jabatan_id']."', unit_kerja_id=  '".$h['unit_kerja_id']."', satuan_kerja_id = '".$h['satuan_kerja_id']."', peg_foto_url = '".$h['peg_foto_url']."', satuan_kerja_nama = '".$h['satuan_kerja_nama']."', unit_kerja_nama = '".$h['unit_kerja_nama']."', jabatan_jenis = '".$h['jabatan_jenis']."' , pangkat = '".$h['pangkat']."', peg_nama_jenis_pns = '".$h['peg_nama_jenis_pns']."', kedudukan_pns = '".$h['kedudukan_pns']."', is_atasan_bayangan =  '".$h['is_atasan_bayangan']."', is_atasan_tugas_tambahan = '".$h['is_atasan_tugas_tambahan']."', peg_email = '".str_replace("'","\'", (str_replace("'-","-", $h['peg_email'])))."' WHERE peg_nip='".$h['peg_nip']."'";*/

								$sql2 = "DELETE FROM siap_pegawai WHERE peg_nip='".$h['peg_nip']."'";

								if ($conn->query($sql2) === TRUE) {
									echo $datana =  "Data ".$h['peg_nama']." Berhasil diupdate";
									echo '<br>';
								} else {
									echo '<br>';
									echo "Error: " . $sql2 . "<br>" . $conn->error;
									echo '<br>';
								}

								$sql = "INSERT INTO siap_pegawai (peg_nip, nip_atasan, peg_nama, nm_gol_akhir, jabatan_nama, eselon_nm, jabatan_id, unit_kerja_id, satuan_kerja_id, peg_foto_url, satuan_kerja_nama, unit_kerja_nama, jabatan_jenis, pangkat, peg_nama_jenis_pns, kedudukan_pns, is_atasan_bayangan, is_atasan_tugas_tambahan, peg_email)
								VALUES('".$h['peg_nip']."', '".$h['nip_atasan']."', '".str_replace("'","\'", $h['peg_nama'])."', '".$h['nm_gol_akhir']."', '".$h['jabatan_nama']."', '".$h['eselon_nm']."', '".$h['jabatan_id']."', '".$h['unit_kerja_id']."', '".$h['satuan_kerja_id']."', '".$h['peg_foto_url']."', '".$h['satuan_kerja_nama']."', '".$h['unit_kerja_nama']."', '".$h['jabatan_jenis']."', '".$h['pangkat']."', '".$h['peg_nama_jenis_pns']."', '".$h['kedudukan_pns']."', '".$h['is_atasan_bayangan']."', '".$h['is_atasan_tugas_tambahan']."', '".str_replace("'","\'", (str_replace("'-","-", $h['peg_email'])))."')";

								if ($conn->query($sql) === TRUE) {
									echo $datana =  "Data ".$h['peg_nama']." Berhasil diupdate";
									echo '<br>';
								} else {
									echo '<br>';
									echo "Error: " . $sql . "<br>" . $conn->error;
									echo '<br>';
								}
							}
							//$conn->close
							    echo '<script>
    parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.';background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
    parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">'.$percent.' is processed.</div>";</script>';
			//
						}
					}
			}
		}
		//$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/bulk_data', $this->data);
	}
	
	/**
	 * Method tarik_pegawai
	 *
	 * @return void display persentase ke view
	 */
	public function tarik_pegawai(){
		ini_set('max_execution_time', 0); 
		$this->benchmark->mark('code_start');
		$cek = $this->get_pegawai(1,1);
		$latest = $cek['count'];
		$loop = ceil($latest/100);
		$progres;
		$percent;
		$proses = 0;
		$nip_all = array();
		$get_nip= $this->integrasi->get_all_nip();
		foreach ($get_nip as $key) {
			array_push($nip_all, $key['peg_nip']);
		}
		$jenis = array(
			'2' => 'Struktural',
			'3' => 'Fungsional Tertentu',
			'4' => 'Fungsional Umum'
		);
		$data = array(
			'kedudukan_pns'=>'Non Aktif',
		);
		// set all pegawai non aktif sebelum update pegawai
		$this->integrasi->pegawai_non_aktif($data);
		for ($i=1; $i<=$loop; $i++) { 
			$percent = number_format(($i/$loop)*100,1);
			$json_data = $this->get_pegawai($i,100);
			$data_pegawai=array();
			$update_pegawai=array();
			sleep(0.001);
				echo '<script>
				parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.'%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
				parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Import Pegawai : '.$percent.' % '.' is processed.</div>"</script>';
				ob_flush(); 
    			flush(); 
			foreach ($json_data['data'] as $key) {
					$temp = array(
						'peg_nip' => $key['peg_nip'],
						'nip_atasan'=> $key['nip_atasan'], 
						'peg_ktp'=> $key['peg_ktp'], 
						'peg_nama'=> $key['peg_nama_lengkap'], 
						'nm_gol_akhir'=> $key['nm_gol_akhir'], 
						'jabatan_nama'=> $key['jabatan_nama'], 
						'eselon_nm'=> $key['eselon_nm'], 
						'jabatan_id'=> $key['jabatan_id'], 
						'unit_kerja_id'=> $key['unit_kerja_id'], 
						'satuan_kerja_id'=> $key['satuan_kerja_id'], 
						'peg_foto_url'=> $key['peg_foto_url'], 
						'satuan_kerja_nama'=> $key['satuan_kerja_nama'], 
						'unit_kerja_nama'=> $key['unit_kerja_nama'], 
						'jabatan_jenis'=> $jenis[$key['jabatan_jenis']], 
						'pangkat'=> $key['nm_pkt_akhir'], 
						'peg_nama_jenis_pns'=> $key['peg_nama_jenis_pns'], 
						'kedudukan_pns'=> $key['kedudukan_pns'], 
						'peg_email'=> $key['peg_email']
					);
					if(in_array($key['peg_nip'], $nip_all)){
						array_push($update_pegawai, $temp);
						$proses +=1;
					}else{
						array_push($data_pegawai,$temp);
						$proses +=1;
					}
			}
				
				if(count($update_pegawai)>0){
					$this->integrasi->update_pegawai_batch($update_pegawai);	
				}
				if(count($data_pegawai)>0){
					$this->integrasi->tambah_pegawai_batch($data_pegawai);	
				}
				sleep(0.001);
				echo '<script>
				parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.'%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
				parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Import Pegawai : '.$percent.' % '.' is processed.</div>"</script>';
				ob_flush(); 
    			flush(); 
		}
		$this->benchmark->mark('code_end');
		echo '<script>
								parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.'%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
								parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Proses import pegawai selesai dalam '.$this->benchmark->elapsed_time('code_start', 'code_end').' detik.</div>"</script>';
	}	
	
	/**
	 * Method cek_jabatan_jenis
	 *
	 * @param $jabatan_jenis [jabatan jenis diperoleh dari field jabatan_jenis api pegawai]
	 *
	 * @return void
	 */
	public function cek_jabatan_jenis($jabatan_jenis)
	{
		if($jabatan_jenis=='3'){
			return "Fungsional Tertentu";
		}else if($jabatan_jenis=='4'){
			return "Fungsional Umum";
		}else if($jabatan_jenis=='2'){
			return "Struktural";
		}else{
			return "-";
		}
	}
	
	/**
	 * Method tarik_unit_kerja
	 *
	 * @return void
	 */
	public function tarik_unit_kerja(){
		$this->benchmark->mark('code_start');
		$latest = $this->cek_latest_unit();
		$loop = floor($latest/100);
		$sikd_per_pd = array();
		$per_pd = $this->integrasi->get_unitkerja_perpd();
		foreach ($per_pd as $key) {
			array_push($sikd_per_pd, $key->unit_kerja_id);
		}
		$progres;
		$percent;
		$proses=0;
		for ($i=1; $i<=$loop; $i++) { 
			$percent = number_format(($i/$loop)*100,1);
			$json_data = $this->get_unit_kerja($i,100);
			$data_unit_update=array();
			$data_unit_insert=array();
			$data_perpd=array();
			foreach ($json_data['result'] as $key) {
					$temp = array(
						'satuan_kerja_id' => $key['satuan_kerja_id'],
						'unit_kerja_id' => $key['unit_kerja_id'],
						'unit_kerja_parent' => $key['unit_kerja_parent'],
						'unit_kerja_nama' => $key['unit_kerja_nama'],
						'eselon' => $key['eselon'],
						'level' => $key['level'],
						'lv1_unit_kerja_nama' => $key['lv1_unit_kerja_nama'],
						'lv2_unit_kerja_nama' => $key['lv2_unit_kerja_nama'],
						'lv3_unit_kerja_nama' => $key['lv3_unit_kerja_nama'],
						'lv4_unit_kerja_nama' => $key['lv4_unit_kerja_nama'],
						'lv5_unit_kerja_nama' => $key['lv5_unit_kerja_nama'],
						'lv6_unit_kerja_nama' => $key['lv6_unit_kerja_nama'],
						'lv7_unit_kerja_nama' => $key['lv7_unit_kerja_nama'],
					);

						$temp3 = array(
							'mapping_pd_id' => $this->integrasi->get_id_bysatker($key['satuan_kerja_id']),
							'satuan_kerja_id' => $key['satuan_kerja_id'],
							'unit_kerja_id' => $key['unit_kerja_id'],
							'unit_kerja_parent' => $key['unit_kerja_parent'],
							'unit_kerja_nama' => $key['unit_kerja_nama'],
							'eselon' => $key['eselon'],
							'aktif' =>'1',
						);
				
						if($this->integrasi->cek_per_pd($key['unit_kerja_id']) > 0){
							array_push($data_perpd, $temp3);
							$proses +=1;

							echo '<script>
								parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.'%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
								parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Import Unit Kerja : '.$percent.' % '.' is processed.</div>";</script>';
						}else{
							$this->integrasi->tambah_mapping_perpd($temp3);
							$proses +=1;
						}
						if($this->integrasi->cek_unit_kerja($key['unit_kerja_id']) > 0){
							array_push($data_unit_update, $temp);
							$remove=array_search($key['unit_kerja_id'], $sikd_per_pd);
							unset($sikd_per_pd[$remove]);
						}else{
							$this->integrasi->tambah_unit_kerja($temp);
						}

				}
				$this->integrasi->update_unit_batch($data_unit_update);
				$this->integrasi->update_perpd_batch($data_perpd);

				echo '<script>
								parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.'%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
								parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Import Unit Kerja : '.$percent.' % '.' is processed.</div>";</script>';
		}
			$tidak_aktif = array(
				'aktif' => 0,
				// 'RoleDesc'=>'',
				// 'gjabatanId'=>'',
				// 'RoleId' =>'',
				// 'RoleParentId' =>'',
				// 'GRoleId'=>'',
				// 'rolecode_id'=>'',
			);

			$sikd_per_pd = array_chunk($sikd_per_pd, 100);
			foreach ($sikd_per_pd as $key) {
				$this->integrasi->update_status_perpd($key, $tidak_aktif);
			}
			$this->benchmark->mark('code_end');
			echo '<script>
								parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.'%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
								parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Proses import unit kerja selesai dalam '.$this->benchmark->elapsed_time('code_start', 'code_end').' detik.</div>";</script>';
			
	}	

	/**
	 * Method tarik_opd
	 * mengambil data siap jabar untuk masuk ke tabel mapping_pd
	 * @return void
	 */
	public function tarik_opd()
	{
		$this->benchmark->mark('code_start');
		$latest = $this->cek_latest_opd();
		$loop = floor($latest/100)+1;
		$proses=0;
		$sikd_mappingpd=array();
		$pd= $this->integrasi->get_satker_pd();
		foreach ($pd as $key) {
			array_push($sikd_mappingpd, $key->satuan_kerja_id);
		}
		for ($i=1; $i<=$loop; $i++) { 
			$json_data = $this->get_opd($i,100);
			$data_pd=array();
			foreach ($json_data['result'] as $key) {
					$temp = array(
						'satuan_kerja_id' => $key['satuan_kerja_id'],
						'unit_kerja_nama' => $key['satuan_kerja_nama'],
						'aktif'=> '1',
					);
						if($this->integrasi->cek_pd($key['satuan_kerja_id']) > 0){
							array_push($data_pd, $temp);
							$remove=array_search($key['satuan_kerja_id'], $sikd_mappingpd);
							unset($sikd_mappingpd[$remove]);
							$proses +=1;
						}else{
							$this->integrasi->tambah_mapping_pd($temp);
							$proses +=1;
						}
				}
				$this->integrasi->update_opd_batch($data_pd);
		}
			$tidak_aktif = array(
				'aktif' => 0,
				// 'GRoleId' =>'',
				// 'GRoleName' => '',
				// 'rolecode_id' => '',
			);
			$this->integrasi->update_status_pd($sikd_mappingpd, $tidak_aktif);
			$this->tarik_unit_kerja();
			// echo "done";
	}

	public function tarik_jabatan()
	{
		$this->benchmark->mark('code_start');
		$cek = $this->get_jabatan(1,1);
		$latest = $cek['count'];
		$loop = ceil($latest/100);
		$proses = 0;
		$percent;
		for ($i=1; $i <= $loop; $i++) { 
			$percent = number_format(($i/$loop)*100,1);
			$json_data = $this->get_jabatan($i, 100);
			$data_jabatan = array();
			foreach ($json_data['data'] as $key) {
				$atasan;
				if(isset($key['atasan_jabatan_id'])){
					$atasan = $key['atasan_jabatan_id'];
				}else{
					$atasan = '';
				}
				$temp = array(
					'jabatan_id' => $key['jabatan_id'], 
					'jabatan_nama' => $key['jabatan_nama'], 
					'jabatan_kelas' => $key['jabatan_kelas'],
					'jabatan_jenis' => $key['jabatan_jenis'], 
					'unit_kerja_nama' => $key['unit_kerja_nama'], 
					'satuan_kerja_nama' => $key['satuan_kerja_nama'], 
					'jf_id' => $key['jf_id'],
					'jfu_id' => $key['jfu_id'],
					'eselon_nm' => $key['eselon_nm'], 
					'eselon_id' => $key['eselon_id'],
					'satuan_kerja_id' => $key['satuan_kerja_id'], 
					'unit_kerja_id'=> $key['unit_kerja_id'],
					'atasan_jabatan_id' => $atasan,
					// 'atasan_jabatan_id' => $key['atasan_jabatan_id']
				);
				
				if($this->integrasi->cek_id_jabatan($key['jabatan_id'])>0){
					$this->integrasi->update_jabatan_single($temp, $key['jabatan_id']);
					$proses+=1;

					echo '<script>
								parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.'%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
								parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Import Jabatan : '.$percent.' % '.' is processed.</div>";</script>';
				}else{
					$this->integrasi->tambah_jabatan($temp);
					$proses +=1;
				}
			}
			
		}
		$this->benchmark->mark('code_end');
		echo '<script>
		parent.document.getElementById("progressbar").innerHTML="<div style=\"width:'.$percent.'%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
		parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Proses import jabatan selesai dalam '.$this->benchmark->elapsed_time('code_start', 'code_end').' detik.</div>";</script>';
			
			// echo "proses ".$proses." data, selesai dengan waktu : ".$this->benchmark->elapsed_time('code_start', 'code_end')." detik";
	}

	public function cek_latest_unit(){
		//manual parameter, data update per tanggal 14 Januari 2021
		$latest = 8013;
		$database_latest = $this->integrasi->get_total_unit_kerja();
		$toReturn;
		if($database_latest<=$latest){
			$latest=$latest;
			$json_data = $this->get_unit_kerja($latest);
			while(count($json_data['result'])!=0){
			$latest+=10;
			$json_data = $this->get_unit_kerja($latest);
			}
			$toReturn = $latest;
		}else{
			$latest=$database_latest;
			$json_data = $this->get_unit_kerja($latest);
			while(count($json_data['result'])!=0){
			$latest+=10;
			$json_data = $this->get_unit_kerja($latest);
			}
			$toReturn = $latest;
		}
		return $toReturn;
	}

	public function cek_latest_opd(){
		//manual parameter, data update per tanggal 14 Januari 2021
		$latest = 40;
		$database_latest = $this->integrasi->get_total_opd();
		$toReturn;
		if($database_latest<=$latest){
			$latest=$latest;
			$json_data = $this->get_opd($latest);
			while(count($json_data['result'])!=0){
			$latest+=1;
			$json_data = $this->get_opd($latest);
			}
			$toReturn = $latest;
		}else{
			$latest=$database_latest;
			$json_data = $this->get_opd($latest);
			while(count($json_data['result'])!=0){
			$latest+=1;
			$json_data = $this->get_opd($latest);
			}
			$toReturn = $latest;
		}
		return $toReturn;
	}

	public function get_pegawai($start, $perpage=1)
	{
		$json;
		$url = 'https://siap.jabarprov.go.id/integrasi/api/v1/pegawai/data?page='.$start.'&perpage='.$perpage.'&params={"peg_status":true}';
		$opts = array('http' => 
		array(
			'method'  => 'GET',
			'header'  => "Content-Type: application/json\r\n".
				"Authorization: Basic ".base64_encode("diskominfo_aptika:diskominfo_aptika2020")."\r\n",
				'ignore_errors' => true,
			)
		);
		$kirim = stream_context_create($opts);
		$json = @file_get_contents($url, false, $kirim);
		if($json===false){
			echo '<script>
		parent.document.getElementById("progressbar").innerHTML="<div style=\"width:0%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
		parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Koneksi ke Server SIAP Jabar bermasalah.</div>"</script>';
		exit();
		}else{
			return json_decode($json,true);	
		}
	}

	public function get_unit_kerja($start, $perpage=1)
	{
		$json;
		$url = 'https://siap.jabarprov.go.id/api/get-unitkerja?page='.$start.'&perpage='.$perpage;
		$opts = array('http' => 
		array(
			'method'  => 'GET',
			'header'  => "Content-Type: application/json\r\n".
				"Authorization: Basic ".base64_encode("diskominfo_aptika:diskominfo_aptika2020")."\r\n",
				'ignore_errors' => true,
			)
		);
		$kirim = stream_context_create($opts);
		$json = @file_get_contents($url, false, $kirim);
		if($json=== false){
			echo '<script>
		parent.document.getElementById("progressbar").innerHTML="<div style=\"width:0%;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
		parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Koneksi ke Server SIKD bermasalah.</div>"</script>';
		exit();
		}else{
			return json_decode($json,true);	
		}
	}

	public function get_opd($start, $perpage=1)
	{
		$url = 'https://siap.jabarprov.go.id/api/get-opd?page='.$start.'&perpage='.$perpage;
		$opts = array('http' => 
		array(
			'method'  => 'GET',
			'header'  => "Content-Type: application/json\r\n".
				"Authorization: Basic ".base64_encode("diskominfo_aptika:diskominfo_aptika2020")."\r\n",
				'ignore_errors' => true,
			)
		);
		$kirim = stream_context_create($opts);
		$json = @file_get_contents($url, false, $kirim);
		if($json=== false){
			echo '<script>
		parent.document.getElementById("progressbar").innerHTML="<div style=\"width:0px;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
		parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Koneksi ke Server SIKD bermasalah.</div>"</script>';
			exit();
		}else{
			return json_decode($json,true);	
		}

	}

	public function get_jabatan($start, $perpage=1)
	{	
		$json;
		$url = 'https://siap.jabarprov.go.id/integrasi/api/v1/jabatan/list?page='.$start.'&perpage='.$perpage;
		$opts = array('http' => 
		array(
			'method'  => 'GET',
			'header'  => "Content-Type: application/json\r\n".
				"Authorization: Basic ".base64_encode("diskominfo_aptika:diskominfo_aptika2020")."\r\n",
				'ignore_errors' => true,
			)
		);
		$kirim = stream_context_create($opts);
		$json = @file_get_contents($url, false, $kirim);
		if($json=== false){
			echo '<script>
		parent.document.getElementById("progressbar").innerHTML="<div style=\"width:0px;background:linear-gradient(to bottom, rgba(125,126,125,1) 0%,rgba(14,14,14,1) 100%); ;height:35px;\">&nbsp;</div>";
		parent.document.getElementById("information").innerHTML="<div style=\"text-align:center; font-weight:bold\">Koneksi ke Server SIKD bermasalah.</div>"</script>';
			exit();
		}else{
			// print_r(json_decode($json,true));	
			return json_decode($json,true);
		}
	}
}