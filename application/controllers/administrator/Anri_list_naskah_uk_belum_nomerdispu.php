<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_naskah_uk_belum_nomerdispu extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_list_naskah_belum_nomor');
		$this->load->model('model_list_naskah_ba');
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inbox');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_ttd');
		$this->load->model('model_inboxreciever_koreksi');
	}

	//List NASKAH MASUK BELUM APPROVE
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['title'] = 'Naskah Belum Disetujui';
		$this->tempanri('backend/standart/administrator/mail_tl/naskah_uk_belum_nomerdispu2', $this->data);
	}
	//List NASKAH MASUK BELUM APPROVE

	//Ambil Data Seluruh Naskah Belum Disetujui
	public function get_data_naskah_ba()
	{			
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_belum_nomor->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$field->JenisId."'")->row()->JenisName;	
			$row[] = $field->nosurat;		
			$row[] = date('d-m-Y',strtotime($field->TglNaskah));
			$row[] = $field->Hal;

			$xx3  = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_temp = '".$field->GIR_Id."'")->row();	
			$xx2  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_temp = '".$field->GIR_Id."'")->row();	

			if ($xx2->Konsep == 5 ) {
				$row[] = "<font color = 'blue'><b>Naskah Telah Dibatalkan Untuk Dikirim Oleh Penandatangan</b></font>";
				
			} elseif ($xx2->Konsep == 2 ) {

				$xxz  = $this->db->query("SELECT ReceiverAs, RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->row();
				
				if($xxz->ReceiverAs == 'to_keluar') {

					$z1 = $xxz->RoleId_To;
					$z2 = '';

				} else {

					$count_tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

					if($count_tujuan > 0){

					$tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
						foreach ($tujuan as $key => $value) {					  
						  $exp = explode(',', $value->RoleId_To);
						  if($value->RoleId_To == '-'){
						  	$z1 = '';
						  } else {
						  	  $z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
							  	foreach ($exp as $k => $v) {
							  		$z1 .= "<tr>";
							    	$z1 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
							    	$z1 .= "</tr>";
							  	}
							  $z1 .= "</table>";	
						  }			  
						}
					}

					$count_tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

					if($count_tembusan > 0){

					$tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
						foreach ($tembusan as $key => $value) {					  
						  $exp = explode(',', $value->RoleId_Cc);
						  if(($value->RoleId_Cc == '') || ($value->RoleId_Cc == '-')){
						  	$z2 = '';
						  } else {
						  	  $z2 = "<br> <b>Tembusan :</b> <table border ='0' cellpadding='0' cellspacing='0'>";
							  	foreach ($exp as $k => $v) {
							  		$z2 .= "<tr>";
							    	$z2 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
							    	$z2 .= "</tr>";
							  	}
							  $z2 .= "</table>";	
						  }			  
						}
					}
				}
				
				$row[] = $z1.$z2;

			} elseif ($xx3->nosurat <= 0 ) {
				$row[] = "<font color = 'red'><b>Naskah Belum Diberi nomor oleh Unit Kearsipan</b></font>";
			} elseif ($xx2->Konsep == 0 ) {
				$row[] = "<font color = 'brown'><b>Naskah Sudah Disetujui dan Belum Dikirim Oleh Penandatangan</b></font>";
			} else {
				$row[] = "<font color = 'red'><b>Naskah Belum Dikirim ke Penandatangan</b></font>";
			}


          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	

			$row[] = $z21;
			
			$xyz  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_Temp = '".$field->GIR_Id."'")->row();

			if ($xyz->Konsep == 1) { 
				$row[] = '<button type="button" onclick=ko_nomor('.$field->NId_Temp.','.$field->GIR_Id.') title="Masukkan Nomor Naskah" class="btn btn-danger btn-sm"><i class="fa fa-sticky-note-o"></i></button> <button type="button" onclick=ko_teruskan('.$field->NId_Temp.','.$field->GIR_Id.') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye">  </i></a>';
				//tombol edit diuk dihilangkan hikman 
				 // <a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_naskahdinas/' . $field->NId_Temp) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>
			} else {

				$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->GIR_Id."' AND Keterangan = 'outbox'")->num_rows();

				if($x > 0) {

					$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->row();

					$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';

				} else {

					$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
					
				}	
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_belum_nomor->count_all(),
			"recordsFiltered" => $this->model_list_naskah_belum_nomor->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Belum Disetujui

	//PDF naskah dinas tindaklanjut
	public function naskahdinas_tindaklanjut_pdf($NId)
	{
		$this->load->library('HtmlPdf');
      	ob_start();

      	$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$NId."' LIMIT 1")->row();

      	$this->data['NId'] 				= $NId;
      	$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
      	$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;
      	$this->data['RoleId_To'] 		= $naskah->RoleId_To;
      	$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
		$this->data['RoleCode'] 		= $naskah->RoleCode;
      	$this->data['Konten'] 			= $naskah->Konten;
      	$this->data['lampiran'] 		= $naskah->lampiran;
		$this->data['Alamat']			= $naskah->Alamat;
		$this->data['Number']			= $naskah->Number;

		
		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$naskah->SifatId."'")->row()->SifatName;
	      	$this->data['Jumlah'] 			= $naskah->Jumlah;
	      	$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$naskah->MeasureUnitId."'")->row()->MeasureUnitName;			
      		$this->data['Hal'] 				= $naskah->Hal;	      	
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
			$this->data['RoleCode'] 		= $naskah->RoleCode;
		}

       	$this->data['TglReg'] 			= $naskah->TglReg;
      	$this->data['Approve_People'] 	= $naskah->Approve_People;
      	$this->data['Approve_People3'] 	= $naskah->Approve_People3;
      
      	$this->data['To_Id_Desc'] 			= $this->db->query("SELECT To_Id_Desc FROM inbox_receiver WHERE NID = '".$naskah->NID."'")->row()->JenisName;

      	$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
      	$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
      	$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$naskah->ClId."'")->row()->ClCode;
		
	    if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxsprint'){ 
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxundangan'){ 
			$this->load->view('backend/standart/administrator/pdf/view_konsep_undangan_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxkeluar'){ 
        	$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_dinas_tindaklanjut_pdf',$this->data);
		}else {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_naskah_dinas_tindaklanjut_pdf',$this->data);
		}

	    $content = ob_get_contents();
	    ob_end_clean();
        
          $config = array(
            'orientation' 	=> 'p',
            'format' 		=> 'F4',
            'marges' 		=> array(30, 30, 20, 30),
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('naskah_dinas_tindaklanjut-'.$NId.'.pdf', 'H');

	}
	//Akhir PDF naskah dinas tindaklanjut	

	//Naskah Dinas Edit
	public function edit_naskahdinas($NId)
	{
		$this->data['title'] = 'Ubah Data Pencatatan Naskah Dinas';
		$this->data['data'] = $this->model_konsepnaskah->find($NId);
		$this->tempanri('backend/standart/administrator/nota_dinas/edit', $this->data);
	}
	//Tutup Naskah Dinas Edit

	//Edit naskah dinas
	public function postedit_notadinas($NId)
	{
		$gir = $NId;
		$tanggal = date('Y-m-d H:i:s');

      	$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '".$NId."' LIMIT 1")->row();

		if ($naskah1->Ket != 'outboxsprint') {
			$sifat =  $this->input->post('SifatId');
		} else {
			$sifat =  NULL;		
		}

		if ($naskah1->Ket == 'outboxkeluar') {
			$alamat =  $this->input->post('Alamat');
			$role = $this->input->post('RoleId_To');
		} else {
			$alamat =  NULL;	
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',',$this->input->post('RoleId_To')) : '');	
		}

		try{
				$CI =& get_instance();
				$this->load->helper('string');

				$data_konsep = [
					'Jumlah'       		=> $this->input->post('Jumlah'),
					'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
					'UrgensiId'       	=> $this->input->post('UrgensiId'),
					'SifatId'       	=> $sifat,
					'Alamat'       		=> $alamat,
					'ClId'       		=> $this->input->post('ClId'),
					'RoleId_To'       	=> $role,
					'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
					'Konten'       		=> $this->input->post('Konten'),
					'Hal'       		=> $this->input->post('Hal'),
				];

				$this->db->where('NId_Temp', $NId);
        		$this->db->update('konsep_naskah', $data_konsep);

				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
							FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;

						$save_files = [
							'FileKey' 			=> tb_key(),
				            'GIR_Id' 			=> $gir,
				            'NId' 				=> $NId,
				            'PeopleID' 			=> $this->session->userdata('peopleid'),
				            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
				            'RoleCode'       	=> $this->input->post('RoleCode'),
				            'FileName_real' 	=> $file_name,
				            'FileName_fake' 	=> $test_title_name_copy,
				            'FileStatus' 		=> 'available',
				            'EditedDate'        => $tanggal,
				            'Keterangan' 		=> '',
						];
						$save_file = $this->model_inboxfile->store($save_files);
					}
				}

				set_message('Data Berhasil Disimpan', 'success');
				redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup Edit naskah dinas

	//hapus file notadinas registrasi
	public function hapus_file5($NId,$name_file)
	{

		$files = glob('./FilesUploaded/naskah/'.$name_file);
		foreach($files as $file){
			if(is_file($file))
				unlink($file);
		}

		$this->db->query("DELETE FROM inbox_files WHERE NId = '".$NId."' AND FileName_fake = '".$name_file."'");

		set_message('Data Berhasil Dihapus', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());
	}
	//tutup hapus file notadinas registrasi

	//Hapus nota dinas registrasi
	public function hapus_nota_dinas_tindaklanjut($NId)
	{
		$query1 	= $this->db->query("SELECT NFileDir FROM konsep_naskah WHERE NId_Temp = '".$NId."'");						
		foreach($query1->result_array() as $row1):
			$fileDir = $row1['NFileDir'];
		endforeach;	

		if($fileDir != ''){

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$NId."'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach($query->result_array() as $row):
				if(is_file($files.$row['FileName_fake']))
					unlink($files.$row['FileName_fake']);
			endforeach;		
		}

		$this->db->query("DELETE FROM `konsep_naskah` WHERE NId_Temp = '".$NId."'");
		$this->db->query("DELETE FROM `inbox_files` WHERE NId = '".$NId."'");

		redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));		

	}
	//Akhir Hapus nota dinas registrasi



	//Fungsi Teruskan Draft Naskah Dinas
public function k_teruskan()
	{

		$nid = $this->input->post('NId_Temp2');
		$girid = $this->input->post('GIR_Id2');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_sekdis') . "' AND GroupId= '4'")->row()->ApprovelName;
		} elseif ($this->input->post('TtdText') == 'UK') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_lainya') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$y = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$y = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_sekdis') . "'")->row()->ApprovelName;
		} else {
			$y = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$v =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$v = $this->input->post('tmpid_sekdis');
		} else {
			$v = $this->input->post('Approve_People');
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();
		// if ($naskah->Ket == 'outboxnotadinas') {
		// 	$xxz = 'to_draft_notadinas';
		if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_keluar';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas1';
		} else if ($naskah->Ket == 'outboxinstruksigub') {
			$xxz = 'to_draft_instruksi_gub';
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$xxz = 'to_draft_super_tugas';
		} else if ($naskah->Ket == 'outboxsket') {
			$xxz = 'to_draft_sket';
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$xxz = 'to_draft_pengumuman';
		} else if ($naskah->Ket == 'outboxsuratizin') {
			$xxz = 'to_draft_surat_izin';
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$xxz = 'to_draft_rekomendasi';
		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];
			

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			$save_recievers_jejak = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'meneruskan',
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];


			$save_reciever_jejak = $this->model_inboxreciever_koreksi->store($save_recievers_jejak);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x, 'TtdText' => $this->input->post('TtdText'), 'Nama_ttd_konsep' => $z]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_uk_belum_nomerdispu'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Fungsi Teruskan Draft Naskah Dinas

}