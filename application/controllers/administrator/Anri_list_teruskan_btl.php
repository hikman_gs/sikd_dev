<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_teruskan_btl extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_teruskan_btl');
	}

	//NASKAH Teruskan Belum TL
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Daftar Naskah Teruskan Belum Tindaklanjut';
		$this->tempanri('backend/standart/administrator/teruskan/list_btl', $this->data);
	}
	//TUTUP NASKAH Teruskan Belum TL
			
	//Ambil Data Semua Naskah Teruskan Belum TL
	public function get_data_teruskan_btl()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_teruskan_btl->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;			

			if ($field->StatusReceive=='unread') {

				$xx3 = "<b>".date('d-m-Y',strtotime($field->Tgl))."</b>";

				if($field->ReceiverAs=='bcc') {
					$xx4 = "<b>".$this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName."<font color=red> (Tembusan) </font></b>";	
				} else {
					$xx4 = "<b>".$this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName."</b>";					
				}

				$xx5 = "<b>".$this->db->query("SELECT Hal FROM inbox WHERE NId='".$field->NId."'")->row()->Hal."</b>";

				$xx1 = "<b>".$field->Msg."</b>";

			} else {

				$xx3 = date('d-m-Y',strtotime($field->Tgl));

				if($field->ReceiverAs=='bcc') {

					$xx4 = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName."<font color=red><b> (Tembusan) </b></font>";	
				} else {
					$xx4 = $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$field->RoleId_From."'")->row()->RoleName;					
				}

				$xx5 = $this->db->query("SELECT Hal FROM inbox WHERE NId='".$field->NId."'")->row()->Hal;

				$xx1 = $field->Msg;				

			}
			
			$row[] = $xx3;
			$row[] = $xx4;
			$row[] = $xx5;
			$row[] = $xx1;

			$row[] = '<a href="'.site_url('administrator/anri_mail_tl/view_naskah_masuk/view/'.$field->NId.'?dt=5').'" title="Lihat Detail Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_teruskan_btl->count_all(),
			"recordsFiltered" => $this->model_list_teruskan_btl->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Semua Naskah Teruskan Belum TL

}