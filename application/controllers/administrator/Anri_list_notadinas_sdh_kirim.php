<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_notadinas_sdh_kirim extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_nota_dinas_sdh_kirim');
	}

	//Daftar Nota Dinas Belum Approve
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Daftar Nota Dinas Yang Sudah Dikirim';
		$this->tempanri('backend/standart/administrator/nota_dinas/list_sdh_kirim', $this->data);
	}
	//Tutup Nota Dinas Belum Approve			

	//Ambil Data Semua Nota Dinas
	public function get_data_nodin_sdh_kirim()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_nota_dinas_sdh_kirim->get_datatables($no,$limit);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $field->nosurat;		
			$row[] = date('d-m-Y',strtotime($field->TglNaskah));
			$row[] = $field->Hal;

				$tujuan  = $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '".$field->GIR_Id."' AND GIR_Id = '".$field->NId_Temp."' AND ReceiverAs = 'to_notadinas' ORDER BY RoleId_To ASC")->result();
			  	
			  	$z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";					
			  	foreach ($tujuan as $key => $value) {					  
					$z1 .= "<tr>";		 	
				 	$z1 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$value->RoleId_To."'")->row()->RoleName."</td>";
				   	$z1 .= "</tr>";
				}
				$z1 .= "</table>";	

				
				$tembusan  = $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '".$field->GIR_Id."' AND GIR_Id = '".$field->NId_Temp."' AND ReceiverAs = 'bcc' ORDER BY RoleId_To ASC")->result();

				$x  = $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '".$field->GIR_Id."'AND GIR_Id = '".$field->NId_Temp."' AND ReceiverAs = 'bcc'")->num_rows();

				if ($x == '0') {
					$z2 = '';
				} else {
					$z2 = "<br> <b>Tembusan :</b> <table border ='0' cellpadding='0' cellspacing='0'>";
					foreach ($tembusan as $key => $value) {					  
						$z2 .= "<tr>";
					   	$z2 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$value->RoleId_To."'")->row()->RoleName."</td>";
					   	$z2 .= "</tr>";
					}
					$z2 .= "</table>";						
				}
		  

			$row[] = $z1.$z2;

            $xq  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->GIR_Id."' AND GIR_Id = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->num_rows();
            
            //Jika Sumber Naskah Dinas inisiatif yang diterima, maka munculkan file naskah dinas nya, dan jika naskah sudah disposisi atau diteruskan jalankan fungsi ini untuk melihat pesan disposisi yang ditulis pada textfield pesan
            if($xq > 0) {

              $xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->GIR_Id."' AND GIR_Id = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->row();
              
              $files = BASE_URL.'/FilesUploaded/naskah/';

			  $row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.$files.$xyz->FileName_real.'" title="Lihat Nota Dinas" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';

            } else { 

				$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->NId_Temp).'" title="Lihat Nota Dinas" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
			}	

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_nota_dinas_sdh_kirim->count_all(),
			"recordsFiltered" => $this->model_list_nota_dinas_sdh_kirim->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Semua Nota Dinas
	
	//Ambil Data  Nota Dinas Belum AP
	public function get_data_nodin_btl_ap()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_nodin_btl_ap->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;

			if ($field->StatusReceive=='unread') {
				$xx1 = "<b>".$field->Nomor."</b>";
				$xx3 = "<b>".$field->Instansipengirim."</b>";
				$xx4 = "<b>".date('d-m-Y',strtotime($field->Tgl))."</b>";
				$xx5 = "<b>".date('d-m-Y H:i:s',strtotime($field->NTglReg))."</b>";
			} else {
				$xx1 = $field->Nomor;
				$xx3 = $field->Instansipengirim;
				$xx4 = date('d-m-Y',strtotime($field->Tgl));
				$xx5 = date('d-m-Y H:i:s',strtotime($field->NTglReg));
			}
			$row[] = $xx1;
			$row[] = $xx3;
			$row[] = $xx4;
			$row[] = $xx5;

			$row[] = '<a href="'.site_url('administrator/anri_mail_tl/view_naskah_masuk/view/'.$field->NId.'?dt=7').'" title="Lihat Detail Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_nodin_btl_ap->count_all(),
			"recordsFiltered" => $this->model_list_nodin_btl_ap->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Nota Dinas Belum AP

}