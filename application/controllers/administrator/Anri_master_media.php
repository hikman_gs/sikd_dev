<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_media extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_media');
	}

	// List media arsip
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_media/master_media_list', $this->data);
	}
	// Tutup list media arsip

	// Tambah data media arsip
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_media/master_media_add', $this->data);
	}
	// Tutup tambah data media arsip

	// Proses simpan data media arsip
	public function add_save()
	{

		$this->form_validation->set_rules('MediaName', 'MediaName', 'trim|required|max_length[50]');		
		if ($this->form_validation->run()) {
			$table = 'master_media';

			$query = $this->db->query("SELECT (max(convert(substr(MediaId, 12), UNSIGNED)) + 1) as id FROM master_media")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

			$save_data = [
				'MediaId' => tb_key().'.'.$id,
				'MediaName' => $this->input->post('MediaName'),
			];

			
			$save_master_media = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_media'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_media'));			
		}

	}
	// Tutup proses simpan data media arsip

	// Edit data media arsip
	public function update($id)
	{
		$this->data['master_media'] = $this->model_master_media->find($id);
		$this->tempanri('backend/standart/administrator/master_media/master_media_update', $this->data);
	}
	// Tutup edit data media arsip

	// Proses update data media arsip
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('MediaName', 'MediaName', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'MediaName' => $this->input->post('MediaName'),
			];

			
			$save_master_media = $this->model_master_media->change($id, $save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_media'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_media'));			
		}

	}
	// Tutup proses update data media arsip

	// Hapus data media arsip
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
		    set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_media'));
	}
	// Tutup hapus data media arsip

	// Proses hapus data media arsip
	private function remove($id)
	{
		$master_media = $this->db->where('MediaId',$id)->delete('master_media');
		return $master_media;
	}
	// Tutup proses hapus data media arsip
}