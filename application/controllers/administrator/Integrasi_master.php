<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Integrasi_master extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		$this->load->model('Master_model', 'master');
		$this->load->model('Model_integrasi', 'integrasi');
	}

	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		// $GRoleId = $this->input->post('mapping_pd_id');
		// if($GRoleId) {
		// 	$GRoleId = $this->input->post('mapping_pd_id');
		// } else {
		// 	$GRoleId = '0';
		// }
		$data['perangkat']=$this->integrasi->get_mappd();
		// $this->db->where('unit_kerja_id !=', '');
		// $this->db->where('satuan_kerja_id', $GRoleId);
		// $this->db->from('siap_unit_kerja');
		
		// $SukRole = $this->db->get();
		// $SukRole = $SukRole->result(); 
		
		// $this->data['GRoleId'] = $GRoleId;
		// $this->data['SukRole'] = $SukRole;
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/masterdata', $data);
	}

	public function get_datatable_pd()
	{
		$mapping_pd = $this->input->post('mapping_pd');
		// $mapping_pd ='1012';
		$list = $this->master->get_mappingpd($mapping_pd);
		$rolecode = $this->master->get_role_code($mapping_pd);
		// $grole_id = $this->master->get_grole_id($mapping_pd);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $mapping) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $this->get_nama_unit($mapping->satuan_kerja_id, $mapping->unit_kerja_id);
			$row[] = '<a class="btn btn-block btn-success btn-sm" title="UPDATE DATA" onclick="mapping('.$mapping->mapping_per_pd_id.',1);" style="color:white">Mapping</a><a class="btn btn-block btn-danger btn-sm" title="UPDATE DATA" onclick="mapping('.$mapping->mapping_per_pd_id.',2);" style="color:white">Update Role</a>';
			// $row[] = $this->master->get_role_desc($mapping->RoleId).'<br/>'.$this->get_grole_select($mapping->mapping_per_pd_id, $rolecode, $mapping->RoleId);
			$row[] = $this->master->get_role_desc($mapping->RoleId).'<br/>'.$this->get_grole_select($mapping->mapping_per_pd_id, $rolecode, $mapping->RoleId);
				# code...
			// $row[] = $this->cek_username_sikd($mapping->peg_nip);
			// $row[] = $this->cek_nip_tambah($mapping->peg_nip);
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->master->mapping_all($mapping_pd),
			"recordsFiltered" => $this->master->mapping_filtered($mapping_pd),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function get_grole_select($map_id, $grole_id, $RoleId)
	{
		$list = $this->master->get_list_grole_by_rolecode($grole_id);
		$return = '<select name="select_'.$map_id.'" class="form-control select_grole" style="width:100%!important;"><option value="0">-Pilih Mapping-</option>';
		
		foreach ($list as $key) {
			if($RoleId==$key->RoleId){
				$return = $return.'<option value='.$key->RoleId.' selected>'.$key->RoleDesc.'</option>';
			}else{
				$return = $return.'<option value='.$key->RoleId.'>'.$key->RoleDesc.'</option>';
			}
			
		}
		return $return.'</select>';
	}

	public function get_nama_unit($satker, $unit)
	{
		$map = $this->master->get_mapping($unit);
		$toReturn = '<b>'.$map->unit_kerja_nama.'</b>';
		$unit = $map->unit_kerja_parent;
		while($unit != null){
			$map = $this->master->get_mapping($unit);
			$toReturn = $map->unit_kerja_nama.' - '.$toReturn;
			$unit = $map->unit_kerja_parent;
		}

		return $toReturn;
	}

	public function get_nama_unit_reverse($satker, $unit)
	{
		$map = $this->master->get_mapping($unit);
		$toReturn = '<b>'.$map->unit_kerja_nama.'</b>';
		$unit = $map->unit_kerja_parent;
		while($unit != null){
			$map = $this->master->get_mapping($unit);
			$toReturn = $toReturn.' '. $map->unit_kerja_nama;
			$unit = $map->unit_kerja_parent;
		}

		return $toReturn;
	}

	public function get_detail_mapping($map_id, $role_uk)
	{
		$cek_uk = $this->master->cek_status_uk($role_uk);
		$data_uk = $cek_uk->row();		
		$data['mapping'] = $this->master->get_detail_mapping($map_id);
		$data['role'] = $this->master->get_role_uk($role_uk);
		$list_eselon = array("I.a", "I.b", "II.a", "II.b", "III.a", "III.b", "IV.a", "IV.b");
		$nama_unit;
		$nama_baru;
		$desc_baru;
		if(strpos($data['role']['grolename'], 'PROVINSI JAWA BARAT')!==false){
			$nama_unit = $data['mapping']['nama'].' '.$data['role']['grolename'];	
		}else{
			$nama_unit = $data['mapping']['nama'].' '.$data['role']['grolename'].' PROVINSI JAWA BARAT';	
		}
		if(in_array($data['mapping']['eselon'], $list_eselon)){
			$nama_baru = 'KEPALA '.$nama_unit;
			if(substr($nama_unit, 0, strlen('SEKRETARIAT')) === 'SEKRETARIAT'){
				$nama_baru = str_replace('SEKRETARIAT','SEKRETARIS', $nama_unit);
			}else if(substr($nama_unit, 0, strlen('ASISTEN')) === 'ASISTEN'){
				$nama_baru=$nama_unit;
			}else if(substr($nama_unit, 0, strlen('DIREKTUR')) === 'DIREKTUR'){
				$nama_baru=$nama_unit;
			}else if(substr($nama_unit, 0, strlen('WAKIL DIREKTUR')) === 'WAKIL DIREKTUR'){
				$nama_baru=$nama_unit;
			}
		}else{
			$nama_naru = $nama_unit;
		}


		$data['role_baru']['name'] =  $nama_baru;
		$data['role_baru']['desc'] = $nama_unit;
		// $data['nama_lengkap'] = $nama_unit;
		$data['nama_role'] = $nama_unit;
		$data['status_uk']= ($cek_uk->num_rows() >0)?"Role sudah digunakan":"Role belum digunakan";
		$data['color_uk']= ($cek_uk->num_rows() >0)?"1":"0";
		echo json_encode($data);
		// dd($data);
	}

	public function update_mapping($mark)
	{
		
		// $eselon = $this->input->post('form_eselon');
		// $list_eselon = array("I.a", "I.b", "II.a", "II.b", "III.a", "III.b", "IV.a", "IV.b");
		// $role_name;
		// if(in_array($eselon, $list_eselon)){
		// 	$role_name='KEPALA '.$unit_nama;
		// 	if(substr($unit_nama, 0, strlen('SEKRETARIAT')) === 'SEKRETARIAT'){
		// 		$role_name = str_replace('SEKRETARIAT','SEKRETARIS', $unit_nama);
		// 	}else if(substr($unit_nama, 0, strlen('ASISTEN')) === 'ASISTEN'){
		// 		$role_name=$unit_nama;
		// 	}else if(substr($unit_nama, 0, strlen('DIREKTUR')) === 'DIREKTUR'){
		// 		$role_name=$unit_nama;
		// 	}else if(substr($unit_nama, 0, strlen('WAKIL DIREKTUR')) === 'WAKIL DIREKTUR'){
		// 		$role_name=$unit_nama;
		// 	}

		// }else{
		// 	$role_name=$unit_nama;
		// }
		
		if($mark==1){
			$id = $this->input->post('mapping_per_pd_id');
			$role_desc = $this->input->post('form_role_desc');
			$unit_nama= $this->input->post('form_role_name');
			$role_id = $this->input->post('form_role_id');
			$data = array( 
				// 'RoleDesc' => $role_desc,
				// 'gjabatanId' => $this->input->post('form_gjabatan'),
				'RoleId' => $role_id,
				// 'RoleParentId' => $this->input->post('form_parentrole_id'),
				// 'GRoleId' => $this->input->post('form_grole_id'),
				// 'rolecode_id' => $this->input->post('form_role_code')
			);
			$this->master->update_mapping($data, $id);
			echo json_encode(array("status" => TRUE)); 
		}else if($mark==2){
			$role_id = $this->input->post('role_id_update');
			$role = array( 
				'RoleDesc' => $this->input->post('desc_baru'),
				'RoleName' => $this->input->post('role_baru'),
			);
			$this->master->update_role($role, $role_id);
			echo json_encode(array("status" => TRUE));
		}
		 
	}



	// public function update(){

	// 	$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('role')."'");
	// 	$role = $role1->row();
	// 	$data = array( );
	// 	if ($role) {
	// 		$data = array( 
	// 			'RoleDesc' => $role->RoleDesc,
	// 			'gjabatanId' => $role->gjabatanId,
	// 			'RoleId' => $role->RoleId,
	// 			'RoleParentId' => $role->RoleParentId,
	// 			'GRoleId' => $role->GRoleId,
	// 			'rolecode_id' => $role->RoleCode
	// 		);
	// 		$update = $this->db->update('mapping_per_pd', $data, array('mapping_per_pd_id' => $this->input->post('mapping_per_pd_id')));
	// 		$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
	// 	} else {
	// 		$data = array( 
	// 			'RoleDesc' =>'',
	// 			'gjabatanId' => '',
	// 			'RoleId' => '',
	// 			'RoleParentId' => '',
	// 			'GRoleId' =>'',
	// 			'rolecode_id' =>''
	// 		);
	// 		$update = $this->db->update('mapping_per_pd', $data, array('mapping_per_pd_id' => $this->input->post('mapping_per_pd_id')));
	// 		$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
	// 	}
		
	// 	echo json_encode(array("status" => $data, "role" => $role));
	// }
	
	// public function insert(){

	// 	$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('role')."'");
	// 	$role = $role1->row();
	// 	$siapunitkerja  = $this->db->query("SELECT * FROM siap_unit_kerja WHERE unit_kerja_id = '".$this->input->post('uk_id')."'");
	// 	$siapunitkerja = $siapunitkerja->row();
		
	// 	$data = array( 
	// 		'unit_kerja_nama' => $siapunitkerja->unit_kerja_nama,
	// 		'unit_kerja_parent' => $siapunitkerja->unit_kerja_parent,
	// 		'satuan_kerja_id' => $siapunitkerja->satuan_kerja_id,
	// 		'unit_kerja_id' => $siapunitkerja->unit_kerja_id,
	// 		'eselon' => $siapunitkerja->eselon,
	// 		'RoleDesc' => $role->RoleDesc,
	// 		'gjabatanId' => $role->gjabatanId,
	// 		'RoleId' => $role->RoleId,
	// 		'RoleParentId' => $role->RoleParentId,
	// 		'GRoleId' => $role->GRoleId,
	// 		'rolecode_id' => $role->RoleCode,
	// 		'mapping_pd_id' => $this->input->post('mapping_pd_id')
	// 	);
	// 	$update = $this->db->insert('mapping_per_pd', $data);
	// 	if($update) {
	// 		$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
	// 	}
	// 	echo json_encode(array("data" => $data, "role" => $siapunitkerja));
	// }
	
	
	public function getdata($GRoleId) {
		//1002
		$list = $this->Master_model->get_datatables($GRoleId);
		$data = array();
        $no   = $_POST['start'];
        foreach ($list as $l) {

            $no++;
            $row = array(); 
			

            $row[] = '<div class="text-right">'.$no.'.</div>';
            $row[] = 'Unit Kerja ';
            $row[] = $this->getUnitKerjaPrent($l->unit_kerja_parent) .' - '.$l->unit_kerja_nama;
            $row[] = $this->selectRole($l->unit_kerja_id, $l->satuan_kerja_id, $GRoleId); 
 
            $data[] = $row;
        }
 
        $output = array(
				"draw"            => $_POST['draw'],
				"recordsTotal"    => $this->Master_model->count_all($GRoleId),
				"recordsFiltered" => $this->Master_model->count_filtered($GRoleId),
				"data"            => $data,
		);
        echo json_encode($output);
	}
	
	
	function selectRole($unit_kerja_id, $satuan_kerja_id, $GRoleId) {
		
		$mapping = $this->db->query("SELECT mapping_pd_id,satuan_kerja_id, GRoleId FROM mapping_pd WHERE satuan_kerja_id = '".$GRoleId."' LIMIT 1");
		$qs1 = $mapping->row(); 
		
		$mappingpd = $this->db->query("SELECT mapping_per_pd_id,RoleId, RoleDesc FROM mapping_per_pd WHERE unit_kerja_id = '".$unit_kerja_id."' LIMIT 1");
		$q11 = $mappingpd->row(); 
		
		$pr  = $this->db->query("SELECT * FROM role WHERE GRoleId = '".$qs1->GRoleId."'");
		$qs  = $pr->result();
		$html  = ''; 
		
		$updatebtn = '';
		//if($q11->RoleDesc != '') {
			$updatebtn = '<button type="button" id="btn-filter" data-id="'.$unit_kerja_id.'" class="btn btn-danger btn-update'.$unit_kerja_id.'">Update</button>';
		//} else {
			//$updatebtn = '<button type="button" id="btn-filter" data-id="'.$unit_kerja_id.'" class="btn btn-success btn-insert'.$unit_kerja_id.'">Insert</button>';
		//}
		$p = $q11->RoleId."---- SELECT RoleId, RoleDesc FROM mapping_per_pd WHERE unit_kerja_id = '".$unit_kerja_id."' LIMIT 1";
		$html .= $q11->RoleDesc.'<br><br>'.$updatebtn.'<br><br><select name="RoleId" style="width:450px;" class="form-control roleName-'.$unit_kerja_id.'">';
		$html .= '<option value="0">---</option>';
		foreach ($qs as $q) {
			$selected = ($q->RoleId == $q11->RoleId)?'selected':'';
			$html .= '<option value="'.$q->RoleId.'"  '.$selected.'>'.$q->RoleDesc.'</option>';
		}
			
		$html .='</select>';
		$html .="<script>$(document).ready(function()
{   
	$('.btn-update".$unit_kerja_id."').on('click', function () {
		var id = $(this).data('id');
		var role = $('.roleName-'+id).val();
		var mppdid = $('.mppdid-'+id).val();
		var uk_id = ".$unit_kerja_id.";
		var mapping_per_pd_id = ".(isset($q11->mapping_per_pd_id)?$q11->mapping_per_pd_id:0).";
        swal({
                title: 'Apakah Data Akan diubah ??',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: BASE_URL + 'administrator/integrasi_master/update',
                        dataType: 'JSON',
                        data: {
                            role: role,
                            id: id,
                            uk_id: uk_id,
                            mapping_per_pd_id: mapping_per_pd_id,
                            mppdid: mppdid
                        },
                        success: function(response, status, xhr) {
							$('#sdq').DataTable().ajax.reload(null,false);
							setTimeout(function() {
                               toastr.success('Data Berhasil diperbarui!!');
                            }, 500);
                        }
                    });
                    return false;
                }
            });
    });
	
	$('.btn-insert".$unit_kerja_id."').on('click', function () {
		var id = $(this).data('id');
		var mapping_pd_id = $('.mapping_pd_id').val(); 
		var role = $('.roleName-'+id).val(); 
		var GRoleId = ".$GRoleId.";
		var uk_id = ".$unit_kerja_id.";
		var mapping_pd_id = ".$qs1->mapping_pd_id.";
	
        swal({
                title: 'Apakah Data Akan diubah ??',
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#DD6B55',
                confirmButtonText: 'Ya',
                cancelButtonText: 'Tidak',
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) { 
                    $.ajax({
                        type: 'POST',
                        url: BASE_URL + 'administrator/integrasi_master/insert',
                        dataType: 'JSON',
                        data: {
                            role: role,
                            id: id,
                            uk_id: uk_id,
                            mapping_pd_id: mapping_pd_id,
                            GRoleId: GRoleId
                        },
                        success: function(response, status, xhr) {
							console.log(response);
							$('#sdq').DataTable().ajax.reload(null,false);
							setTimeout(function() {
                               toastr.success('Data Berhasil ditambahkan!!');
                            }, 500);
                        }
                    });
                    return false;
                }
            });
    });
});</script>";
		
		return $html;
	}
	
	function getUnitKerjaPrent($e) {

		$ci=& get_instance();
		
		$p = $ci->db->query("SELECT unit_kerja_nama FROM siap_unit_kerja WHERE unit_kerja_id = '".$e."'");
		$qs = $p->row();
		$qs = isset($qs->unit_kerja_nama)?$qs->unit_kerja_nama:"-";
		
		return $qs;
	}
}