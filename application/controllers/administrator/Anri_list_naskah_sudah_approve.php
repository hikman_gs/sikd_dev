<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_naskah_sudah_approve extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_naskah_bs');	
		$this->load->model('model_inbox');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_konsepnaskah');
	}

	//NASKAH MASUK SUDAH APPROVE
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Naskah Belum Dikirim';
		$this->tempanri('backend/standart/administrator/mail_tl/naskah_sudah_approve', $this->data);
	}
	//Tutup NASKAH MASUK SUDAH APPROVE

	//Ambil Data Seluruh Naskah Belum Dikirim
	public function get_data_naskah_bs()
	{	
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_bs->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$field->JenisId."'")->row()->JenisName;	
			$row[] = $field->nosurat;		
			$row[] = date('d-m-Y',strtotime($field->TglNaskah));
			$row[] = $field->Hal;

			$xxz  = $this->db->query("SELECT ReceiverAs, RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->row();

			if($xxz->ReceiverAs == 'to_keluar') {

				$z1 = $xxz->RoleId_To;
				$z2 = '';

			} else {

				$count_tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

				if($count_tujuan > 0){

				$tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
					foreach ($tujuan as $key => $value) {					  
					  $exp = explode(',', $value->RoleId_To);
					  if($value->RoleId_To == '-'){
					  	$z1 = '';
					  } else {
					  	  $z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
						  	foreach ($exp as $k => $v) {
						  		$z1 .= "<tr>";
						    	$z1 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
						    	$z1 .= "</tr>";
						  	}
						  $z1 .= "</table>";	
					  }			  
					}
				}

				$count_tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

				if($count_tembusan > 0){

				$tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
					foreach ($tembusan as $key => $value) {					  
					  $exp = explode(',', $value->RoleId_Cc);
					  if(($value->RoleId_Cc == '') || ($value->RoleId_Cc == '-')){
					  	$z2 = '';
					  } else {
					  	  $z2 = "<br> <b>Tembusan :</b> <table border ='0' cellpadding='0' cellspacing='0'>";
						  	foreach ($exp as $k => $v) {
						  		$z2 .= "<tr>";
						    	$z2 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
						    	$z2 .= "</tr>";
						  	}
						  $z2 .= "</table>";	
					  }			  
					}
				}
			}	

			$row[] = $z1.$z2;

          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	

			$row[] = $z21;
			
			$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->num_rows();

			if($x > 0) {

				$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->row();

					$xxz1  = $this->db->query("SELECT ReceiverAs FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->row();

					//Jika surat dinas, maka yang muncul adalah akan diteruskan kemana
					if($xxz1->ReceiverAs == 'to_keluar') {

						$row[] = '<button type="button" onclick=ko_teruskan('.$field->NId_Temp.','.$field->GIR_Id.') title="Teruskan Setelah Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <a href="'.site_url('administrator/anri_list_naskah_sudah_approve/batal_kirim/'.$field->NId_Temp.'/'.$field->GIR_Id).'" title="Membatalkan Pengiriman Naskah" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda ingin membatalkan pengiriman naskah ini ?\')"><i class="fa fa-remove"></i></a>';	

					} else {

						$row[] = '<a href="'.site_url('administrator/anri_list_naskah_sudah_approve/kirim_naskah/'.$field->NId_Temp.'/'.$field->GIR_Id).'" title="Kirim Naskah" class="btn btn-success btn-sm" onclick="return confirm(\'Anda ingin mengirimkan naskah ini ?\')"><i class="fa fa-arrow-circle-right"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <a href="'.site_url('administrator/anri_list_naskah_sudah_approve/batal_kirim/'.$field->NId_Temp.'/'.$field->GIR_Id).'" title="Membatalkan Pengiriman Naskah" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda ingin membatalkan pengiriman naskah ini ?\')"><i class="fa fa-remove"></i></a>';	

					}
					
			} else {

					$xxz1  = $this->db->query("SELECT ReceiverAs FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->row();

					//Jika surat dinas, maka yang muncul adalah akan diteruskan kemana
					if($xxz1->ReceiverAs == 'to_keluar') {

						$row[] = '<button type="button" onclick=ko_teruskan('.$field->NId_Temp.','.$field->GIR_Id.') title="Teruskan Setelah Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_sudah_approve/log_naskah_dinas_pdf/'.$field->NId_Temp).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <a href="'.site_url('administrator/anri_list_naskah_sudah_approve/batal_kirim/'.$field->NId_Temp.'/'.$field->GIR_Id).'" title="Membatalkan Pengiriman Naskah" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda ingin membatalkan pengiriman naskah ini ?\')"><i class="fa fa-remove"></i></a>';

					} else {

						$row[] = '<a href="'.site_url('administrator/anri_list_naskah_sudah_approve/kirim_naskah/'.$field->NId_Temp.'/'.$field->GIR_Id).'" title="Kirim Naskah" class="btn btn-success btn-sm" onclick="return confirm(\'Anda ingin mengirimkan naskah ini ?\')"><i class="fa fa-arrow-circle-right"></i></a> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_sudah_approve/log_naskah_dinas_pdf/'.$field->NId_Temp).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> <a href="'.site_url('administrator/anri_list_naskah_sudah_approve/batal_kirim/'.$field->NId_Temp.'/'.$field->GIR_Id).'" title="Membatalkan Pengiriman Naskah" class="btn btn-danger btn-sm" onclick="return confirm(\'Anda ingin membatalkan pengiriman naskah ini ?\')"><i class="fa fa-remove"></i></a>';		
					}			
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_bs->count_all(),
			"recordsFiltered" => $this->model_list_naskah_bs->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Belum Dikirim

	//View PDF Naskah Dinas
	public function log_naskah_dinas_pdf($id)
	{
		$this->load->library('HtmlPdf');
      	ob_start();

      	$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$id."' ORDER BY TglNaskah DESC LIMIT 1")->row();

      	$this->data['NId'] 				= $id;
      	$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
      	$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;
      	$this->data['RoleId_To'] 		= $naskah->RoleId_To;
      	$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
      	$this->data['Konten'] 			= $naskah->Konten;
      	$this->data['lampiran'] 		= $naskah->lampiran;
		$this->data['Hal'] 				= $naskah->Hal;	

		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$naskah->SifatId."'")->row()->SifatName;
	      	$this->data['Jumlah'] 			= $naskah->Jumlah;
	      	$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$naskah->MeasureUnitId."'")->row()->MeasureUnitName;      	
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}
	
      	$this->data['TglNaskah'] 		= $naskah->TglNaskah;
      	$this->data['Approve_People'] 	= $naskah->Approve_People;
      	$this->data['Approve_People3'] 	= $naskah->Approve_People3;
      	$this->data['TtdText'] 			= $naskah->TtdText;
      	$this->data['TtdText2'] 			= $naskah->TtdText2;
      	$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
      	$this->data['Nama_ttd_atas_nama'] 	= $naskah->Nama_ttd_atas_nama;
      	$this->data['Number'] 			= $naskah->Number;
      	$this->data['nosurat'] 			= $naskah->nosurat;
      	$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$naskah->ClId."'")->row()->ClCode;
      	$this->data['lokasi'] 			= $naskah->lokasi;
      	
	    if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->view('backend/standart/administrator/mail_tl/nodin_pdf',$this->data);
		}else if($naskah->Ket == 'outboxsprint'){ 
			$this->load->view('backend/standart/administrator/mail_tl/surattugas_pdf',$this->data);
		}else if($naskah->Ket == 'outboxundangan'){ 
			$this->load->view('backend/standart/administrator/mail_tl/undangan_pdf',$this->data);
		}else if ($naskah->Ket == 'outboxedaran') {
			$this->load->view('backend/standart/administrator/mail_tl/suratedaran_pdf', $this->data);
		}else if($naskah->Ket == 'outboxkeluar'){ 
        	$this->load->view('backend/standart/administrator/mail_tl/suratdinas_pdf',$this->data);
		}else {
			$this->load->view('backend/standart/administrator/mail_tl/nadin_pdf',$this->data);
		}	
	    
	    $content = ob_get_contents();
	    ob_end_clean();
        
   $config = array(
            'orientation' 	=> 'p',
            'format' 		=> 'F4',
            'marges' 		=> array(30, 30, 20, 30),
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('konsep_naskah.pdf', 'H');
	}
	//Akhir View PDF Naskah Dinas

	//Kirim Naskah
	public function kirim_naskah($NId,$GIR_Id)
	{

		$tanggal = date('Y-m-d H:i:s');
		$gir = $this->session->userdata('peopleid').date('dmyhis');

		$konsep = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$NId."' AND GIR_Id = '".$GIR_Id."' AND Konsep = '0'")->result();

		foreach ($konsep as $k => $v) {

			$kepada 	= explode(',', $v->RoleId_To);
			
			$tembusan 	= explode(',', $v->RoleId_Cc);

			$save_data = [
				'NKey'         		=> tb_key(),
				'NId'				=> $NId,
				'CreatedBy'			=> $this->session->userdata('peopleid'),
				'CreationRoleId'	=> $this->session->userdata('roleid'),
				'NTglReg' 			=> $tanggal,
				'Tgl' 				=> date('Y-m-d H:i:s',strtotime($v->TglNaskah)),
				'JenisId' 			=> get_data_konsepnaskah('JenisId',$NId),
				'UrgensiId' 		=> (!empty(get_data_konsepnaskah('UrgensiId',$NId)) ?  get_data_konsepnaskah('UrgensiId',$NId) : ''),
				'SifatId' 			=> (!empty(get_data_konsepnaskah('SifatId',$NId)) ?  get_data_konsepnaskah('SifatId',$NId) : ''),
				'Nomor' 			=> get_data_konsepnaskah('nosurat',$NId),
				'Hal' 				=> get_data_konsepnaskah('Hal',$NId),
				'Pengirim'			=> 'internal',
				'NAgenda'			=> '',
				'NTipe' 			=> get_data_konsepnaskah('Ket',$NId),
				'Namapengirim' 		=> $this->session->userdata('namabagian'),
				'NFileDir'		    => 'naskah',
				'BerkasId'			=> '1',
			];

			$save_inbox = $this->model_inbox->store($save_data);

			if ($kepada[0] != NULL) {
				foreach ($kepada as $receiver) {
					//perbaikan untuk notadinas
					$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

					// if ($naskah1->Ket == 'outboxnotadinas') {
					// 	$to_id = $receiver;
					// 	$RoleId_To_notadinas = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '".$receiver."'")->row()->PrimaryRoleId;

					// }else if($naskah1->Ket == 'outboxsupertugas') {
					// 	$to_id = $receiver;
					// 	$RoleId_To_notadinas=$receiver;

					// } else {
					// 	$to_id = $this->db->query("SELECT PeopleId FROM people WHERE PrimaryRoleId = '".$receiver."'")->row()->PeopleId;
					// 	$RoleId_To_notadinas =$receiver;
					// }


					if ($naskah1->Ket == 'outboxnotadinas') {
						$to_id = $receiver;
						$RoleId_To_notadinas = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '".$receiver."'")->row()->PrimaryRoleId;

					}else if($naskah1->Ket == 'outboxsupertugas') {
						$to_id = $receiver;
						$RoleId_To_notadinas = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '".$receiver."'")->row()->PrimaryRoleId;

					}else if($naskah1->Ket == 'outboxsuratizin') {
						$to_id = $receiver;
						$RoleId_To_notadinas = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '".$receiver."'")->row()->PrimaryRoleId;

					}else if($naskah1->Ket == 'outboxpengumuman') {
						$to_id = $receiver;
						$RoleId_To_notadinas = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '".$receiver."'")->row()->PrimaryRoleId;

					}
					else if($naskah1->Ket == 'outboxsket') {
						$to_id = $receiver;
						$RoleId_To_notadinas = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '".$receiver."'")->row()->PrimaryRoleId;
					}
					else if($naskah1->Ket == 'outboxrekomendasi') {
						$to_id = $receiver;
						$RoleId_To_notadinas=$receiver;

					}
					else if($naskah1->Ket == 'outboxinstruksigub') {
						$to_id = $receiver;
						$RoleId_To_notadinas = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '".$receiver."'")->row()->PrimaryRoleId;
						$RoleId_To_kedua = $this->db->query("SELECT RoleId_To_2 FROM konsep_naskah WHERE NId_Temp = '" . $NId . "'")->row()->RoleId_To_2;

					} else {

						$to_id = $this->db->query("SELECT PeopleId FROM people WHERE PrimaryRoleId = '".$receiver."'")->row()->PeopleId;
						$RoleId_To_notadinas=$receiver;

						
					}


					//akhir perbaikan untuk notadinas
					$save_recievers = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,  
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 			=> $to_id,
						'RoleId_To' 	=> $RoleId_To_notadinas,
						'RoleId_To_2' 	=> $RoleId_To_kedua,
						'ReceiverAs' 	=> $v->ReceiverAs,
						'Msg' 			=> '',
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> $this->db->query("SELECT RoleDesc FROM role WHERE RoleId = '".$receiver."'")->row()->RoleDesc,
						'Status'		=> '0',
					];

					$save_reciever = $this->model_inboxreciever->store($save_recievers);
				}
			}

			if ($tembusan[0] != NULL) {
				foreach ($tembusan as $tembusan) {
					$save_tembusan = [
						'NId' 			=> $NId,
						'NKey' 			=> tb_key(),
						'GIR_Id' 		=> $gir,
						'From_Id' 		=> $this->session->userdata('peopleid'),
						'RoleId_From' 	=> $this->session->userdata('roleid'),
						'To_Id' 		=> $this->db->query("SELECT PeopleId FROM people WHERE PrimaryRoleId = '".$tembusan."'")->row()->PeopleId,
						'RoleId_To' 	=> $tembusan,
						'ReceiverAs' 	=> 'bcc',
						'Msg' 			=> '',
						'StatusReceive' => 'unread',
						'ReceiveDate' 	=> $tanggal,
						'To_Id_Desc' 	=> $this->db->query("SELECT RoleDesc FROM role WHERE RoleId = '".$tembusan."'")->row()->RoleDesc,
						'Status'		=> '0',
					];

					$save_temb = $this->model_inboxreciever->store($save_tembusan);
				}
			}
		}

		$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Konsep' => '2']);

		set_message('Data Berhasil Disimpan', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());	

	}
	//Tutup Kirim Naskah
	

	// Batal Kirim Naskah
	public function batal_kirim($NId,$GIR_Id)
	{

		$konsep = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_Temp = '".$NId."' AND GIR_Id = '".$GIR_Id."'")->row()->Konsep;

			//Konsep 5 = batal kirim
			$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Konsep' => '5']);

			set_message('Membatalkan Pengiriman Naskah Berhasil Dilakukan', 'success');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());	
	}
	// Akhir Batal Kirim Naskah

	//Fungsi Teruskan Surat Dinas Yang Sudah Diapprove
	public function k_kirim()
	{

		if($this->input->post('TtdText') == '-') {

			set_message('Tujuan Teruskan Wajib Dipilih', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());

		} else {

			$NId = $this->input->post('NId_Temp7');
			$GIR_Id = $this->input->post('GIR_Id7');

			$gir = $this->session->userdata('peopleid').date('dmyhis');
			$tanggal = date('Y-m-d H:i:s');

			if($this->input->post('TtdText') == 'AL') {
				$x = $this->input->post('tmpid_atasan');		
			} else {
				$x = $this->input->post('Approve_People');
			}

			$konsep = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$NId."' AND GIR_Id = '".$GIR_Id."' AND Konsep = '0'")->row();

			try{

				$save_data = [
					'NKey'         		=> tb_key(),
					'NId'				=> $NId,
					'CreatedBy'			=> $this->session->userdata('peopleid'),
					'CreationRoleId'	=> $this->session->userdata('roleid'),
					'NTglReg' 			=> $tanggal,
					'Tgl' 				=> date('Y-m-d H:i:s',strtotime($konsep->TglNaskah)),
					'JenisId' 			=> get_data_konsepnaskah('JenisId',$NId),
					'UrgensiId' 		=> get_data_konsepnaskah('UrgensiId',$NId),
					'SifatId' 			=> get_data_konsepnaskah('SifatId',$NId),
					'Nomor' 			=> get_data_konsepnaskah('nosurat',$NId),
					'Hal' 				=> get_data_konsepnaskah('Hal',$NId),
					'Pengirim'			=> 'internal',
					'NAgenda'			=> '',
					'NTipe' 			=> get_data_konsepnaskah('Ket',$NId),
					'Namapengirim' 		=> $this->session->userdata('namabagian'),
					'NFileDir'		    => 'naskah',
					'BerkasId'			=> '1',
				];

				$save_inbox = $this->model_inbox->store($save_data);

				$save_recievers = [
					'NId' 			=> $NId,
					'NKey' 			=> tb_key(),
					'GIR_Id' 		=> $gir,
					'From_Id' 		=> $this->session->userdata('peopleid'),
					'RoleId_From' 	=> $this->session->userdata('roleid'),
					'To_Id' 		=> $x,
					'RoleId_To' 	=> get_data_people('RoleId',$x),
					'ReceiverAs' 	=> 'to_keluar',
					'Msg' 			=> $this->input->post('pesan'),
					'StatusReceive' => 'unread',
					'ReceiveDate' 	=> $tanggal,
					'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
					'Status'		=> '0',
				];

				$save_reciever = $this->model_inboxreciever->store($save_recievers);

				//Update konsep menjadi 2, walaupun dokumen diteruskan. karena dokumen tsb sudah di ttd
				$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Konsep' => '2']);		

				set_message('Data Berhasil Disimpan', 'success');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());

			}catch(\Exception $e){
				set_message('Gagal Menyimpan Data', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}	
		
	}
	//Fungsi Teruskan Surat Dinas Yang Sudah Diapprove
	public function k_teruskan()
	{

		if($this->input->post('TtdText') == '-') {

			set_message('Tujuan Teruskan Wajib Dipilih', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());

		} else {

			$NId = $this->input->post('NId_Temp2');
			$GIR_Id = $this->input->post('GIR_Id2');

			$gir = $this->session->userdata('peopleid').date('dmyhis');
			$tanggal = date('Y-m-d H:i:s');

			if($this->input->post('TtdText') == 'AL') {
				$x = $this->input->post('tmpid_atasan');		
			} else {
				$x = $this->input->post('Approve_People');
			}

			$konsep = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$NId."' AND GIR_Id = '".$GIR_Id."' AND Konsep = '0'")->row();

			try{

				$save_data = [
					'NKey'         		=> tb_key(),
					'NId'				=> $NId,
					'CreatedBy'			=> $this->session->userdata('peopleid'),
					'CreationRoleId'	=> $this->session->userdata('roleid'),
					'NTglReg' 			=> $tanggal,
					'Tgl' 				=> date('Y-m-d H:i:s',strtotime($konsep->TglNaskah)),
					'JenisId' 			=> get_data_konsepnaskah('JenisId',$NId),
					'UrgensiId' 		=> get_data_konsepnaskah('UrgensiId',$NId),
					'SifatId' 			=> get_data_konsepnaskah('SifatId',$NId),
					'Nomor' 			=> get_data_konsepnaskah('nosurat',$NId),
					'Hal' 				=> get_data_konsepnaskah('Hal',$NId),
					'Pengirim'			=> 'internal',
					'NAgenda'			=> '',
					'NTipe' 			=> get_data_konsepnaskah('Ket',$NId),
					'Namapengirim' 		=> $this->session->userdata('namabagian'),
					'NFileDir'		    => 'naskah',
					'BerkasId'			=> '1',
				];

				$save_inbox = $this->model_inbox->store($save_data);

				$save_recievers = [
					'NId' 			=> $NId,
					'NKey' 			=> tb_key(),
					'GIR_Id' 		=> $gir,
					'From_Id' 		=> $this->session->userdata('peopleid'),
					'RoleId_From' 	=> $this->session->userdata('roleid'),
					'To_Id' 		=> $x,
					'RoleId_To' 	=> get_data_people('RoleId',$x),
					'ReceiverAs' 	=> 'to_forward',
					'Msg' 			=> $this->input->post('pesan'),
					'StatusReceive' => 'unread',
					'ReceiveDate' 	=> $tanggal,
					'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
					'Status'		=> '0',
				];

				$save_reciever = $this->model_inboxreciever->store($save_recievers);

				//Update konsep menjadi 2, walaupun dokumen diteruskan. karena dokumen tsb sudah di ttd
				$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Konsep' => '2']);		

				set_message('Data Berhasil Disimpan', 'success');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());

			}catch(\Exception $e){
				set_message('Gagal Menyimpan Data', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}	
		
	}
	//Fungsi Teruskan Surat Dinas Yang Sudah Diapprove
	
}