<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Anri_regis_super_m_tugas extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_konsepnaskah_koreksi');
	}

	//Registrasi Surat Tugas
	public function index()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		$this->data1['people'] = $this->db->query("SELECT * FROM people WHERE PeopleId != 'root' ")->result();
		$this->data['title'] = 'Pencatatan Surat Tugas';
		$this->tempanri('backend/standart/administrator/surat_m_tugas/regis', $this->data);
	}
	// Tutup Registrasi Surat Tugas

	//Fungsi Lihat Surat Tugas
	public function lihat_naskah_dinas2()
	{

		$tanggal = date('Y-m-d H:i:s');
		// $this->load->library('HtmlPdf');
		$this->load->library('Pdf');
		// ob_start();

		$this->data['RoleId_To'] 			= $this->input->post('RoleId_To');
		$this->data['Nama_ttd_pengantar'] 	= $this->input->post('Nama_ttd_pengantar');
		$this->data['RoleId_Cc'] 			= $this->input->post('RoleId_Cc');
		$this->data['Hal'] 					= $this->input->post('Hal');
		$this->data['Konten'] 				= $this->input->post('Konten');
		$this->data['TglReg'] 				= date('Y-m-d H:i:s');
		$this->data['RoleCode'] 			= $this->input->post('RoleCode');

		if ($this->input->post('TtdText') == 'none') {
			$this->data['Approve_People']	= $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$this->data['Approve_People'] 	= $this->input->post('tmpid_atasan');
		} else {
			$this->data['Approve_People'] 	= $this->input->post('Approve_People');
		}

		//penambahan atas nama

		if($this->input->post('TtdText2') == 'AL') {
			$this->data['Approve_People3'] 	= $this->input->post('tmpid_atas_nama');	
		} else {
			$this->data['Approve_People3'] 	= $this->input->post('Approve_People3');
		}

		//


		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');
		//atas nama
		$this->data['TtdText2']     = $this->input->post('TtdText2');


		if ($this->input->post('TtdText') 			== 'none') {
			$this->data['Nama_ttd_konsep']			= $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') 	== 'AL') {
			$this->data['Nama_ttd_konsep'] 			= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$this->data['Nama_ttd_konsep'] 		     = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}


		$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

		$this->db->select("*");
		$this->db->where_in('PeopleId',$this->data['RoleId_To']);
		$this->db->order_by('GroupId', 'ASC');
		$this->db->order_by('Golongan', 'DESC');
		$this->db->order_by('Eselon ', 'ASC');
		$data_perintah = $this->db->get('v_login')->result();

		// var_dump($data_perintah);
		// die();

		$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "' AND GroupId IN ('3','4','7') ")->row()->Pangkat;
		

		// $Golongan = $this->db->query("SELECT Golongan FROM people WHERE primaryroleid = '" . $this->data['primaryroleid'] . "' AND GroupId IN ('3','4') ")->row()->Golongan;

		$NIP = $this->db->query("SELECT NIP FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

		$Golongan = $this->db->query("SELECT Golongan FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

		$jabatan = $this->db->query("SELECT PeoplePosition FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

 

		$NIP_ttd_konsep = $this->db->query("SELECT NIP FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4') ")->row()->NIP;
		$jabatan_ttd_konsep = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleName = '" . $this->data['Nama_ttd_konsep'] . "'  AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

		
		
		//tcpdf
		$age = $this->session->userdata('roleid') == 'uk.1';
		$xx = $this->session->userdata('roleid');
				
		if($data['TtdText'] == 'PLT') {
			$tte = 'Plt. '.get_data_people('RoleName', $this->data['Approve_People']);
		}elseif($data['TtdText'] == 'PLH') {
			$tte = 'Plh. '.get_data_people('RoleName', $this->data['Approve_People']);
		}elseif($this->input->post('TtdText2') == 'Atas_Nama') {
			$tte = 'a.n'.'. '.get_data_people('RoleName', $this->data['Approve_People3']).'<br>'.get_data_people('RoleName', $this->data['Approve_People']);
		}else{
			$tte = get_data_people('RoleName', $this->data['Approve_People']);
		}

		$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td rowspan="7" width="60px" style="valign:bottom;">
				<br><br>
				<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
				<td width="180px" style="text-align: left; font-size:9;">'.$tte.',</td>
			</tr>
			<tr nobr="true">
				<td><br></td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
				</td>
			</tr>
			<tr nobr="true">
				<td>'.$tujuan. '
				</td>
			</tr>
		</table>';


		$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
		$pdf->SetMargins(30, 30, 20);
		$pdf->SetAutoPageBreak(TRUE, 25);
		// 
		$pdf->AddPage('P', 'F4');
		if ($this->session->userdata('roleid') == 'uk.1') {

			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 174, 0, 'PNG', '', 'N');
			$pdf->Ln(-3);
		} else {
			$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
		}
		$pdf->Ln(3);
		$pdf->SetX(45);
		$pdf->Cell(0,10,'SURAT PERNYATAAN MELAKSANAKAN TUGAS',0,0,'C');
		$pdf->Ln(6);
		$pdf->SetX(45);
		$pdf->Cell(0,10,'NOMOR : .........../'.$this->data['ClCode'].'/'.$this->data['RoleCode'],0,0,'C');
		$pdf->Ln(15);
		$pdf->Cell(30,5,'Yang bertandatangan di bawah ini :',0,0,'L');
	
		$pdf->Ln(5);
		$pdf->SetX(46);
			$pdf->Cell(25,5,'Nama',0,0,'L');
			$pdf->SetX(92);
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Nama_ttd_pengantar']).',',0,'L');
			// if($this->data['TtdText'] == 'PLT') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }elseif($this->data['TtdText'] == 'PLH') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }elseif($this->data['TtdText2'] == 'Atas_Nama') {

			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');

			// }else{
			// 	$pdf->SetX(97);
			// 	$pdf->MultiCell(85,6,get_data_people('PeopleName',$this->data['Approve_People']).',',0,'L');
			// }



			$variable_nip	 		=$this->data['Approve_People'];
			$variable_nip3 			=$this->data['Approve_People3'];
			$variable_nip_ttd 		=$this->data['Nama_ttd_pengantar'];

			$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;

			// if($data['TtdText'] == 'PLT') {

			


			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

			// }elseif($data['TtdText'] == 'PLH') {

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {

			// $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;
			// }else{

			// $nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip . "' AND GroupId IN ('3','4','7') ")->row()->NIP;
			// }
			
			// $gol =$this->data['Approve_People'];
			$gol_ttd =$this->data['Nama_ttd_pengantar'];
			// $gol3 =$this->data['Approve_People3'];

			$golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Golongan;

			$pangkat_ttd_pengantar = $this->db->query("SELECT Pangkat FROM people WHERE PeopleId = '" .  $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

			$jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $gol_ttd . "' AND GroupId IN ('3','4') ")->row()->PeoplePosition;
			

			// if($data['TtdText'] == 'PLT') {

			


			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

			// }elseif($data['TtdText'] == 'PLH') {
			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {
			// $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;
			// }else{

			// $golongan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $gol . "' AND GroupId IN ('3','4','7') ")->row()->Golongan;
			// }

			// $jab_ttd =$this->data['Approve_People'];
			// $jab_ttd3 =$this->data['Approve_People3'];

			// if($data['TtdText'] == 'PLT') {			


			// $jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

			// }elseif($data['TtdText'] == 'PLH') {
			// $jabatan_ttd = $this->db->query("SELECT Golongan FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;

			// }elseif($this->input->post('TtdText') == 'Atas_Nama') {
			// $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;
			// }else{

			// $jabatan_ttd = $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $jab_ttd . "' AND GroupId IN ('3','4','7') ")->row()->PeoplePosition;
			// }


			$pdf->SetX(46);
			$pdf->Cell(25,5,'NIP',0,0,'L');
			$pdf->SetX(92);
			$pdf->Cell(5,5,':',0,0,'L');

			if($nipttd =='170592' ){
				$nipttd=' - ';				
			}else{				
				$nipttd = $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $variable_nip_ttd . "' AND GroupId IN ('3','4') ")->row()->NIP;
			}	
			
			$pdf->MultiCell(85,6,$nipttd,0,'L');

			$pdf->SetX(46);
			$pdf->Cell(25,5,'Pangkat/Golongan',0,0,'L');
			$pdf->SetX(92);
			$pdf->Cell(5,5,':',0,0,'L');

			if(!empty($pangkat_ttd_pengantar )) {

				$pangkat_kosong=' / ';
			}else{
				$pangkat_kosong=' - ';
			}

			$pdf->Cell(85,5,$pangkat_ttd_pengantar.$pangkat_kosong.$golongan_ttd,0,1,'L');

			$pdf->SetX(46);
			$pdf->Cell(25,5,'Jabatan',0,0,'L');
			$pdf->SetX(92);
			$pdf->Cell(5,5,':',0,0,'L');
			$uc  = ucwords(strtolower($jabatan_ttd));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			// $pdf->MultiCell(85,5,$str,0,'J');
			$pdf->MultiCell(84, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
	

				


		$pdf->Ln(3);
		$pdf->Cell(30,5,'Dengan ini menerangkan dengan sesungguhnya bahwa :',0,0,'L');
		$pdf->Ln(6);
	

		$i=0;
		foreach($data_perintah as $row){
			$i++;
		

			$pdf->SetX(46);
			$pdf->Cell(25,5,'Nama',0,0,'L');
			$pdf->SetX(92);
			$pdf->Cell(5,5,':',0,0,'L');

			$pdf->MultiCell(85,6,$row->PeopleName,0,'L');


			$pdf->SetX(46);
			$pdf->Cell(25,5,'NIP',0,0,'L');
			$pdf->SetX(92);
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->Cell(85,5,$row->NIP,0,1,'L');

			$pdf->SetX(46);
			$pdf->Cell(25,5,'Pangkat/Golongan',0,0,'L');
			$pdf->SetX(92);
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->Cell(85,5,$row->Pangkat.' / '.$row->Golongan,0,1,'L');

			$pdf->SetX(46);
			$pdf->Cell(25,5,'Jabatan',0,0,'L');
			$pdf->SetX(92);
			$pdf->Cell(5,5,':',0,0,'L');
			$uc  = ucwords(strtolower($row->PeoplePosition));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			// $pdf->MultiCell(85,5,$str,0,'J');
			$pdf->MultiCell(84, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
		}
		$pdf->Ln(6);

		// $pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan'.' '.$this->data['Hal'].' '.'Nomor'.' '.$row->NIP.'' .' '.'terhitung'.' '.$row->NIP.''.' '.'telah nyata menjalankan tugas sebagai'.' '.$row->NIP.''.' '.'di'.' '.$row->NIP.'' ,0, 1, 0, true, 'J', true);
		$pdf->writeHTMLCell(160, 0, '', '', 'Yang diangkat berdasarkan'.' '.$this->data['Hal'].'.' ,0, 1, 0, true, 'J', true);
		$pdf->writeHTMLCell(160, 0, '', '', $this->data['Konten'].'' ,0, 1, 0, true, 'J', true);

		// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
		$pdf->Ln(5);

		$pdf->writeHTMLCell(160, 0, '', '', '&nbsp;&nbsp;&nbsp;&nbsp;Demikian Surat Pernyataan Melaksanakan Tugas ini dibuat dengan sesungguhnya dengan mengingat sumpah jabatan/pegawai negeri sipil dan apabila dikemudian hari isi surat pernyataan ini ternyata tidak benar yang berakibat kerugian bagi negara, maka saya bersedia menanggung kerugian tersebut.' ,0, 1, 0, true, 'J', true);
		// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');


		$pdf->Ln(10);
		$pdf->SetX(128);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Tempat, tanggal, bulan dan tahun',0,1,'C');
		$pdf->SetX(120);
		// $pdf->SetFont('Arial','',10);
		$pdf->Ln(5);
		if($this->data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n. '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');

		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}
		$pdf->Ln(2);
		$pdf->SetX(130);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'PEMERIKSA',0,1,'L');
		$pdf->Ln(4);		
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

		if (!empty($this->data['RoleId_Cc'] )) {

			$pdf->Ln(12);
			$pdf->Cell(30,5,'Tembusan',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->Ln(5);
			$pdf->Cell(30,5,'',0,0,'L');
			$pdf->Cell(5,5,'',0,0,'L');
			$filter_RoleId_To=array_filter($this->data['RoleId_Cc']);
			$hitung = count($filter_RoleId_To);
			$i = 0;

			foreach( $filter_RoleId_To as $v){

			$y = 'Yth';
			$xx1 = $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;			
			$uc  = ucwords(strtolower($xx1));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			$str = str_replace('Dprd', 'DPRD', $str);
			$i++;

			if (!empty($hitung)) {
					if ($hitung >= 1) {
						$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30,5,$i.'. ',0,0,'L');
							$pdf->SetX(34);
							$pdf->Cell(30,5,$y.'. ',0,0,'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str."; \n", 0, 'J', 0, 2, '' ,'', true);

						} elseif($i == $hitung){
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30,5,$i.'. ',0,0,'L');
							$pdf->SetX(34);
							$pdf->Cell(30,5,$y.'. ',0,0,'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str.".\n", 0, 'J', 0, 2, '' ,'', true);
						}else{
							$pdf->Ln(1);
							$pdf->SetX(30);
							$pdf->Cell(30,5,$i.'. ',0,0,'L');
							$pdf->SetX(34);
							$pdf->Cell(30,5,$y.'. ',0,0,'L');
							$pdf->SetX(42);
							$pdf->MultiCell(150, 5, $str.";\n", 0, 'J', 0, 2, '' ,'', true);



						}

							}else{
						$$pdf->SetX(34);
						$pdf->Cell(30,5,$y.'. ',0,0,'L');
						$pdf->SetX(42);
						$pdf->MultiCell(150, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
					
					


			}


		}

		}



		$pdf->Output('surat_pernyataan_tugas_view.pdf','I');

		// $this->load->view('backend/standart/administrator/pdf/view_surattugas_pdf', $this->data);

		// $content = ob_get_contents();
		// ob_end_clean();

		// $config = array(
		// 	'orientation' 	=> 'p',
		// 	'format' 		=> 'a4',
		// 	'marges' 		=> array(30, 30, 20, 30),
		// );


		// $this->pdf = new HtmlPdf($config);
		// $this->pdf->initialize($config);
		// $this->pdf->pdf->SetDisplayMode('fullpage');
		// $this->pdf->writeHTML($content);
		// $this->pdf->Output('naskah_dinas_tindaklanjut-' . $tanggal . '.pdf', 'H');
	}
	// Akhir Fungsi Surat Tugas

	//Simpan Registrasi Surat Tugas
	public function post_surat_m_tugas()
	{


		$gir_awal= $this->session->userdata('peopleid').date('dmyhis');	
		$jumlah_karakter    =strlen($gir_awal);		
		if ($jumlah_karakter == 17){

			$sub_gir = substr($gir_awal,0,16);
			$cek_NId_Temp="SELECT NId_Temp FROM konsep_naskah WHERE NId_Temp = '$_POST[$sub_gir]'";
			if ($cek_NId_Temp > 0) {

				$sub_gir = substr($gir_awal,0,16)+1;

			}else{

				$sub_gir = substr($gir_awal,0,16);

			}

		}else{

			$sub_gir = $this->session->userdata('peopleid').date('dmyhis');

		}

		// $gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

	

		// if($this->input->post('TtdText2') == 'none') {
		// 		$b =('');
			
		// 		} elseif($this->input->post('TtdText2') == 'Atas_Nama') {		
		// 		$b = $this->input->post('tmpid_atas_nama');	
		// 		} else {
		// 		$b = $this->input->post('Approve_People3');
		// 		}	

		if($this->input->post('TtdText2') == 'none') {
		
		$b = ('');
		} elseif($this->input->post('TtdText2') == 'AL') {		
		$b = $this->input->post('tmpid_atas_nama');	
		} else {
		$b = $this->input->post('Approve_People3');
		}	

		try {
			
			$data_konsep = [

				// 'NId_Temp'   		=> $gir,
				// 'GIR_Id'       		=> $gir,
				'NId_Temp'   		=> $sub_gir,
				'GIR_Id'       		=> $sub_gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Pernyataan Melaksanakan Tugas'")->row()->JenisId,
				'ReceiverAs'       	=> 'to_super_tugas_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('RoleCode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'Nama_ttd_pengantar'       	=> $this->input->post('Nama_ttd_pengantar'),
				'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
			
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'Approve_People3'   => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxsupertugas',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'Nama_ttd_atas_nama' 	=> $b,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];
			//menyimpan konsep awal 
				$data_konsep_awal = [

				// 'NId_Temp'   		=> $gir,
				// 'GIR_Id'       		=> $gir,

				'NId_Temp'   		=> $sub_gir,
				'GIR_Id'       		=> $sub_gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Pernyataan Melaksanakan Tugas'")->row()->JenisId,
				'ReceiverAs'       	=> 'to_super_tugas_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('RoleCode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'Nama_ttd_pengantar'       	=> $this->input->post('Nama_ttd_pengantar'),
				'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
				
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'Approve_People3'    => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxsupertugas',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'Nama_ttd_atas_nama' 	=> $b,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'id_koreksi'    	=> $sub_gir,
			];
			//akhir menyimpan konsep awal 

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						// 'GIR_Id' 			=> $gir,
						'GIR_Id' 			=> $sub_gir,
						'NId' 				=> $data_konsep['NId_Temp'],
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

			//menyimpan konsep awal
			$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep_awal);
			// akhir menyimpan konsep awal

			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$save_recievers = [
				// 'NId' 			=> $gir,
				// 'NKey' 			=> tb_key(),
				// 'GIR_Id' 		=> $girid_ir,
				'NId' 			=> $sub_gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $sub_gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'to_draft_super_tugas',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];
			//memunculkan konsep awal
			$save_recievers_awal = [
				// 'NId' 			=> $gir,
				// 'NKey' 			=> tb_key(),
				// 'GIR_Id' 		=> $girid_ir,
				'NId' 			=> $sub_gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $sub_gir,
				'id_koreksi'	=> $sub_gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'to_draft_super_tugas',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			
			];
			//akhir memunculkan konsep awal
			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			//memunculkan konsep awal
			$save_reciever_awal = $this->model_inboxreciever_koreksi->store($save_recievers_awal);
			//akhir memunculkan konsep awal
			
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Akhir Simpan Registrasi Surat Tugas

	public function postedit_notadinas($NId)
	{
		$gir = $NId;
		$tanggal = date('Y-m-d H:i:s');

		$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

		if ($naskah1->Ket != 'outboxsprint') {
			$sifat =  $this->input->post('SifatId');
		} else {
			$sifat =  NULL;
		}

		if ($naskah1->Ket == 'outboxkeluar') {
			$alamat =  $this->input->post('Alamat');

			$role = $this->input->post('RoleId_To');
			$konten = $this->input->post('Konten');
			//penambahan
			$lampiran =$this->input->post('lampiran');
			//akhir penambahan

		} elseif ($naskah1->Ket == 'outboxedaran') {
			$alamat =  $this->input->post('Alamat');
			$role = $this->input->post('RoleId_To');
			$konten = $this->input->post('Konten');
		} elseif ($naskah1->Ket == 'outboxinstruksigub') {
			$alamat =  $this->input->post('Alamat');
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$role2 = (!empty($this->input->post('RoleId_To_2')) ?  json_encode(array_filter($this->input->post('RoleId_To_2'))) : '');
			$konten = (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : '');
		} elseif ($naskah1->Ket == 'outboxsuratizin') {
			$alamat =  $this->input->post('Alamat');
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$konten = (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : '');

		} else {
			$alamat =  NULL;
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$konten = $this->input->post('Konten');
			$lampiran =(!empty($this->input->post('isi_lampiran')) ?  json_encode($this->input->post('isi_lampiran')) : '');
		}


		// } else {
		// 	$alamat =  '';
		// 	$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
		// 	$konten = $this->input->post('Konten');
		// }

		if ($naskah1->Ket == 'outboxkeluar') {
			$ket =  'outboxkeluar';
		} elseif ($naskah1->Ket == 'outboxedaran') {
			$ket =  'outboxedaran';
		} elseif ($naskah1->Ket == 'outboxnotadinas') {
			$ket =  'outboxnotadinas';
		} elseif ($naskah1->Ket == 'outboxsprint') {
			$ket =  'outboxsprint';
		} elseif ($naskah1->Ket == 'outboxinstruksigub') {
			$ket =  'outboxinstruksigub';
		} elseif ($naskah1->Ket == 'outboxsupertugas') {
			$ket =  'outboxsupertugas';
		} elseif ($naskah1->Ket == 'outboxsuratizin') {
			$ket =  'outboxsuratizin';
		} else {
		}


		try {
			$CI = &get_instance();
			$this->load->helper('string');

			$data_konsep = [
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'SifatId'       	=> $sifat,
				'Alamat'       		=> $alamat,
				'ClId'       		=> $this->input->post('ClId'),
				'RoleId_To'       	=> $role,
				'RoleId_To_2'       => $role2,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $konten,
				'lampiran'       	=> $lampiran,
			
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'       		=> $this->input->post('Hal_pengantar'),
				'Nama_ttd_pengantar' 		=> $this->input->post('Nama_ttd_pengantar'),
				'RoleCode' 					=> $this->input->post('RoleCode'),

			];

			//menyimpan record koreksi pada konsep naskah koreksi
			$type_naskah = $this->db->query("SELECT type_naskah FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->type_naskah;
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$data_konsep2 = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'id_koreksi'		=> $girid_ir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('RoleCode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $role,
				'RoleId_To_2'       => $role2,
				'Alamat'       		=> $alamat,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $konten,
				'lampiran'       	=> $lampiran,
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Approve_People'       	=> $this->db->query("SELECT Approve_People FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People,
				'Approve_People3'    =>  $this->db->query("SELECT Approve_People3 FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People3,
				'TtdText'       	=> $this->db->query("SELECT TtdText FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText,
				'TtdText2'       	=>  $this->db->query("SELECT TtdText2 FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText2,
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'     => $this->input->post('Hal_pengantar'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> $ket,
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep'   => $this->db->query("SELECT Nama_ttd_konsep FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_konsep,

				'Nama_ttd_atas_nama'       	=> $this->db->query("SELECT Nama_ttd_atas_nama FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_atas_nama,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'type_naskah'			=> $type_naskah,
			
				'Nama_ttd_pengantar' 	=> $this->input->post('Nama_ttd_pengantar'),

			];

			$this->db->where('NId_Temp', $NId);
			$this->db->update('konsep_naskah', $data_konsep);

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'RoleCode'       	=> $this->input->post('RoleCode'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outboxkeluar',
					];
					$save_file = $this->model_inboxfile->store($save_files);
				}

				//penambahan koreksi
				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploadedkoreksi/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);
					}
				}
				//akhir penambahan koreksi
			}


			//penambahan koreksi2
			$save_konsep2 	= $this->model_konsepnaskah_koreksi->store($data_konsep2);
			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$save_recievers = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),

				'To_Id' 		=> $this->db->query("SELECT To_Id FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->To_Id,
				'RoleId_To'       	=> $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->RoleId_To,
				'id_koreksi'		=>	$girid_ir,
				'ReceiverAs' 	=> 'to_koreksi',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'read',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];
			$save_reciever2 = $this->model_inboxreciever_koreksi->store($save_recievers);
			//akhir penambahan koreksi2

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup Edit naskah dinas

}
