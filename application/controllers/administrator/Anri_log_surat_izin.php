<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_log_surat_izin extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_log_surat_izin');
	}

	//Log Surat Tugas
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Data Pencatatan Surat Tugas';
		$this->tempanri('backend/standart/administrator/surat_izin/log', $this->data);
	}
	//End Log Surat Tugas
	
	//Ambil Data log Surat Tugas
	public function get_data_surat_tugas()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_log_surat_izin->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$field->JenisId."'")->row()->JenisName;	
			$row[] = $field->nosurat;		
			$row[] = date('d-m-Y',strtotime($field->TglNaskah));
			$row[] = $field->Hal;

			$xx2  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_temp = '".$field->GIR_Id."'")->row();	

			if ($xx2->Konsep == 5 ) {
				$row[] = "<font color = 'blue'><b>Naskah Telah Dibatalkan Untuk Dikirim Oleh Penandatangan</b></font>";
				
			} elseif ($xx2->Konsep == 2 ) {

				$count_tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

				if($count_tujuan > 0){

				$tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
					foreach ($tujuan as $key => $value) {					  
					  $exp = explode(',', $value->RoleId_To);
					  if($value->RoleId_To == '-'){
					  	$z1 = '';
					  } else {
					  	  $z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
						  	foreach ($exp as $k => $v) {
						  		$z1 .= "<tr>";
						    	$z1 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
						    	$z1 .= "</tr>";
						  	}
						  $z1 .= "</table>";	
					  }			  
					}
				}

				$count_tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

				if($count_tembusan > 0){

				$tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
					foreach ($tembusan as $key => $value) {					  
					  $exp = explode(',', $value->RoleId_Cc);
					  if(($value->RoleId_Cc == '') || ($value->RoleId_Cc == '-')){
					  	$z2 = '';
					  } else {
					  	  $z2 = "<br> <b>Tembusan :</b> <table border ='0' cellpadding='0' cellspacing='0'>";
						  	foreach ($exp as $k => $v) {
						  		$z2 .= "<tr>";
						    	$z2 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
						    	$z2 .= "</tr>";
						  	}
						  $z2 .= "</table>";	
					  }			  
					}
				}

				$row[] = $z1.$z2;

			} elseif ($xx2->Konsep == 1 ) {
				$row[] = "<font color = 'red'><b>Naskah Belum Disetujui Oleh Penandatangan</b></font>";
			} elseif ($xx2->Konsep == 0 ) {
				$row[] = "<font color = 'brown'><b>Naskah Sudah Disetujui dan Belum Dikirim Oleh Penandatangan</b></font>";
			} else {
				$row[] = "<font color = 'red'><b>Naskah Belum Dikirim Oleh Penandatangan</b></font>";
			}


          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	

			$row[] = $z21;
			
			$xyz  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_Temp = '".$field->GIR_Id."'")->row();

			if ($xyz->Konsep == 1) { 
				// $row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';


					$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_jejak_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';

			} else {

				$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->GIR_Id."' AND Keterangan = 'outbox'")->num_rows();

				if($x > 0) {

					$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->row();

					// $row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
					$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_jejak_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_final_histori_koreksi/view/'.$field->GIR_Id.'?dt=3').'" title="Lihat Jejak Draf" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';
				} else {

					// $row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
					$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_jejak_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';	
				}	
			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_log_surat_izin->count_all(),
			"recordsFiltered" => $this->model_list_log_surat_izin->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data log Surat Tugas

}