<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_notadinas_btl_ap extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
	}

	//Daftar Nota Dinas Belum Approve
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Daftar Nota Dinas Belum Disetujui';
		$this->tempanri('backend/standart/administrator/nota_dinas/list_btl_ap_new', $this->data);
	}
	//Tutup Nota Dinas Belum Approve			

}