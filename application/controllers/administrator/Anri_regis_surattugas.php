<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Anri_regis_surattugas extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}

		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_konsepnaskah_koreksi');
	}

	//Registrasi Surat Tugas
	public function index()
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		$this->data['title'] = 'Pencatatan Surat Tugas';
		$this->tempanri('backend/standart/administrator/surat_tugas/regis', $this->data);
	}
	// Tutup Registrasi Surat Tugas

	//Fungsi Lihat Surat Tugas
	public function lihat_naskah_dinas2()
	{

		$tanggal = date('Y-m-d H:i:s');
		// $this->load->library('HtmlPdf');
		$this->load->library('Pdf');
		// ob_start();

		$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$this->data['Hal'] 			= $this->input->post('Hal');
		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['RoleCode'] 	= $this->input->post('rolecode');

		if ($this->input->post('TtdText') == 'none') {
			$this->data['Approve_People']	= $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$this->data['Approve_People'] 	= $this->input->post('tmpid_atasan');
		} else {
			$this->data['Approve_People'] 	= $this->input->post('Approve_People');
		}

		//penambahan atas nama

		if($this->input->post('TtdText2') == 'AL') {
			$this->data['Approve_People3'] 	= $this->input->post('tmpid_atas_nama');	
		} else {
			$this->data['Approve_People3'] 	= $this->input->post('Approve_People3');
		}

		//


		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');
		//atas nama
		$this->data['TtdText2']     = $this->input->post('TtdText2');


		if ($this->input->post('TtdText') == 'none') {
			$this->data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}


		$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

		$this->db->select("*");
		$this->db->where_in('PeopleId',$this->data['RoleId_To']);
		$this->db->order_by('GroupId', 'ASC');
		$this->db->order_by('Golongan', 'DESC');
		$this->db->order_by('Eselon ', 'ASC');
		$data_perintah = $this->db->get('v_login')->result();

		// var_dump($data_perintah);
		// die();

		$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

		//tcpdf
		$age = $this->session->userdata('roleid') == 'uk.1';
		$xx = $this->session->userdata('roleid');
		
		
		$table = '<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td rowspan="7" width="60px" style="valign:bottom;">
				<br><br>
				<img src="'.base_url('/uploads/kosong.jpg').'" widht="55" height="60"></td>
				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
				<td width="180px" style="text-align: left; font-size:9;">'.get_data_people('RoleName', $this->data['Approve_People']).',</td>
			</tr>
			<tr nobr="true">
				<td><br></td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">'.$this->data['Nama_ttd_konsep'].'
				</td>
			</tr>
			<tr nobr="true">
				<td>'.$tujuan. '
				</td>
			</tr>
		</table>';


		$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );  
		$pdf->SetMargins(30, 30, 30);
		$pdf->SetAutoPageBreak(TRUE, 30);
		// 
		$pdf->AddPage('P', 'F4');

		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header),30,5,160,0,'PNG');
		$pdf->Ln(10);
		$pdf->Cell(0,10,'SURAT PERINTAH',0,0,'C');
		$pdf->Ln(5);
		$pdf->Cell(0,10,'NOMOR : .........../'.$this->data['ClCode'].'/'.$this->data['RoleCode'],0,0,'C');
		$pdf->Ln(15);
		$pdf->Cell(30,5,'DASAR',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->writeHTMLCell(125, 5, '', '', $this->data['Hal'], '', 1, 0, true, 'J', true);
		$pdf->Ln(5);
		$pdf->Cell(0,10,'MEMERINTAHKAN:',0,1,'C');
		$pdf->Ln(5);
		$pdf->Cell(30,5,'Kepada',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$i=0;
		foreach($data_perintah as $row){
			$i++;
			$pdf->SetX(65);
			$pdf->Cell(10,5,$i.'. ',0,0,'L');
			$pdf->SetX(75);
			$pdf->Cell(25,5,'Nama',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->MultiCell(85,5,$row->PeopleName,0,'L');
			$pdf->SetX(75);
			$pdf->Cell(25,5,'Pangkat',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->Cell(85,5,$row->Pangkat,0,1,'L');
			$pdf->SetX(75);
			$pdf->Cell(25,5,'NIP',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$pdf->Cell(85,5,$row->NIP,0,1,'L');
			$pdf->SetX(75);
			$pdf->Cell(25,5,'Jabatan',0,0,'L');
			$pdf->Cell(5,5,':',0,0,'L');
			$uc  = ucwords(strtolower($row->PeoplePosition));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			// $pdf->MultiCell(85,5,$str,0,'J');
			$pdf->MultiCell(84, 5, $str."\n", 0, 'J', 0, 2, '' ,'', true);
		}
		$pdf->Ln(10);
		$pdf->Cell(30,5,'Untuk',0,0,'L');
		$pdf->Cell(5,5,':',0,0,'L');
		$pdf->writeHTMLCell(125, 0, '', '', $this->data['Konten'], 0, 1, 0, true, 'J', true);
		// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');
		$pdf->Ln(10);
		$pdf->SetX(125);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Ditetapkan di ..............',0,1,'L');
		$pdf->SetX(120);
		// $pdf->SetFont('Arial','',10);
		$pdf->Cell(30,5,'Pada tanggal ..................',0,1,'L');
		$pdf->Ln(5);
		if($this->data['TtdText'] == 'PLT') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plt. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText'] == 'PLH') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'Plh. '.get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}elseif($this->data['TtdText2'] == 'Atas_Nama') {
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,'a.n '.get_data_people('RoleName',$this->data['Approve_People3']).',',0,'C');
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
			$pdf->Cell(30,5,'Pada tanggal ..................',0,1,'L');
		}else{
			$pdf->SetX(100);
			$pdf->MultiCell(90,5,get_data_people('RoleName',$this->data['Approve_People']).',',0,'C');
		}
		$pdf->Ln(5);
		
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);

		$pdf->Output('surat_perintah_view.pdf','I');

		// $this->load->view('backend/standart/administrator/pdf/view_surattugas_pdf', $this->data);

		// $content = ob_get_contents();
		// ob_end_clean();

		// $config = array(
		// 	'orientation' 	=> 'p',
		// 	'format' 		=> 'a4',
		// 	'marges' 		=> array(30, 30, 20, 30),
		// );


		// $this->pdf = new HtmlPdf($config);
		// $this->pdf->initialize($config);
		// $this->pdf->pdf->SetDisplayMode('fullpage');
		// $this->pdf->writeHTML($content);
		// $this->pdf->Output('naskah_dinas_tindaklanjut-' . $tanggal . '.pdf', 'H');
	}
	// Akhir Fungsi Surat Tugas

	//Simpan Registrasi Surat Tugas
	public function post_surattugas()
	{

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}


		if($this->input->post('TtdText2') == 'none') {
				$a =('');
				$a = ('');
				} elseif($this->input->post('TtdText2') == 'AL') {		
				$a = $this->input->post('tmpid_atas_nama');	
				} else {
				$a = $this->input->post('Approve_People3');
				}	

		if($this->input->post('TtdText2') == 'none') {
				$b =('');
				$b = ('');
				} elseif($this->input->post('TtdText2') == 'AL') {		
				$b = $this->input->post('tmpid_atas_nama');	
				} else {
				$b = $this->input->post('Approve_People3');
				}	


		try {

			$data_konsep = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Perintah'")->row()->JenisId,
				'ReceiverAs'       	=> 'to_sprint',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'Approve_People3'    => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxsprint',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'Nama_ttd_atas_nama' 	=> $x,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];
			//menyimpan konsep awal 
				$data_konsep_awal = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Perintah'")->row()->JenisId,
				'ReceiverAs'       	=> 'to_sprint',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'Approve_People3'    => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxsprint',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'Nama_ttd_atas_nama' 	=> $x,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'id_koreksi'    	=> $gir,
			];
			//akhir menyimpan konsep awal 

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $data_konsep['NId_Temp'],
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);
				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

			//menyimpan konsep awal
			$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep_awal);
			// akhir menyimpan konsep awal

			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$save_recievers = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'to_draft_sprint',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];
			//memunculkan konsep awal
			$save_recievers_awal = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'to_draft_sprint',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
				'id_koreksi'    	=> $gir,
			];
			//akhir memunculkan konsep awal
			$save_reciever = $this->model_inboxreciever->store($save_recievers);

			//memunculkan konsep awal
			$save_reciever_awal = $this->model_inboxreciever_koreksi->store($save_recievers_awal);
			//akhir memunculkan konsep awal
			
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Akhir Simpan Registrasi Surat Tugas

}
