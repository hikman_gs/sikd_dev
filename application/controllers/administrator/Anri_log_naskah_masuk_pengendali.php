<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_log_naskah_masuk_pengendali extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('Model_list_log_naskah_masuk_pengendali');
	}

	//Log Naskah Masuk
	public function index()
	{

        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/log_registrasi/log_naskah_masuk_pengendali', $this->data);

	}
	//Tutup Log Naskah Masuk
	
	//Ambil Data Log Seluruh Naskah Masuk
	public function get_data_log_surat_all()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->Model_list_log_naskah_masuk_pengendali->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $field->Nomor;
			$row[] = $field->Hal;
			$row[] = $field->Instansipengirim;
			$row[] = date('d-m-Y',strtotime($field->Tgl));
			$ClName = '<span class="text-red"><b>Belum diisi</b></span>';
			if($field->ClId != "1"){
			$ClName = $this->db->query("SELECT Concat(ClCode,' - ',ClName) as ClName from classification where ClId = '".$field->ClId."'")->row()->ClName;
			}
			$row[] = $ClName;
			$Urgensi = '<span class="text-red"><b>Belum diisi</b></span>';
			if($field->UrgensiId != NULL){
			$Urgensi = $this->db->query("SELECT UrgensiName FROM master_urgensi WHERE UrgensiId ='".$field->UrgensiId."'")->row()->UrgensiName;
			}
			$row[] = $Urgensi;

			if($field->CreationRoleId == $this->session->userdata('roleid')){
				
					$row[] = '<a href="'.site_url('administrator/anri_log_naskah_masuk_pengendali/view_log_naskah_metadata_kendali/log/'.$field->NId).'" title="kartu kendali" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-envelope"></i> <a href="'.site_url('administrator/anri_reg_naskah_masuk_setda/edit_naskah_masuk_pengendali/log/'.$field->NId.'?dt=1').'" title="Lihat Detail Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>';
              	
            } 

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Model_list_log_naskah_masuk_pengendali->count_all(),
			"recordsFiltered" => $this->Model_list_log_naskah_masuk_pengendali->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Log Seluruh Naskah Masuk	

	//Hapus naskah masuk
	public function hapus_naskah_masuk($NId)
	{
		$this->data['NId'] = $NId;

		$query1 	= $this->db->query("SELECT NFileDir FROM inbox WHERE NId = '".$NId."'");				
		
		foreach($query1->result_array() as $row1):
			$fileDir = $row1['NFileDir'];
		endforeach;	

		if($fileDir != ''){

			$query 	= $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$NId."'");

			$files = FCPATH . 'FilesUploaded/' . $fileDir . '/';

			foreach($query->result_array() as $row):
				if(is_file($files.$row['FileName_fake']))
					unlink($files.$row['FileName_fake']);
			endforeach;		
		}	

		$this->db->query("DELETE FROM `inbox_receiver` WHERE NId = '".$NId."'");
		$this->db->query("DELETE FROM `inbox_files` WHERE NId = '".$NId."'");
		$this->db->query("DELETE FROM `inbox` WHERE NId = '".$NId."'");

		$this->tempanri('backend/standart/administrator/log_registrasi/log_naskah_masuk', $this->data);
	}
	//Tutup Hapus naskah masuk




	public function view_log_naskah_metadata_kendali($tipe,$NId)
	{

		$this->data['title'] 	= 'Detail Naskah Masuk';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['classification'] = $this->db->query("SELECT Concat(ClCode,' - ',ClName) as ClName from classification where ClId = '".$field->ClId."'")->row()->ClName;
			
		$this->data['inbox'] = $this->db->query("SELECT * FROM inbox WHERE NId = '".$NId."' LIMIT 1")->row();


		$this->tempanri('backend/standart/administrator/log_registrasi/inbox_log_view_metadata_karkendali', $this->data);
	}
	
	//Buka view log metadata
	public function view_log_naskah_metadata($tipe,$NId)
	{

		$this->data['title'] 	= 'Detail Naskah Masuk';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox'] = $this->db->query("SELECT * FROM inbox WHERE NId = '".$NId."' LIMIT 1")->row();


		$this->tempanri('backend/standart/administrator/log_registrasi/inbox_log_view_metadata', $this->data);
	}
	//Tutup view log metadata

	//Buka view log naskah histori
	public function view_log_naskah_histori($tipe,$NId)
	{

		$this->data['title'] 	= 'Detail Naskah Masuk';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;

		$this->tempanri('backend/standart/administrator/log_registrasi/inbox_log_view_histori', $this->data);
	}
	//Tutup view log naskah histori
	//tambahan invoice
	public function view_log_naskah_metadata_invoice($tipe,$NId)
	{

		$this->data['title'] 	= 'Detail Naskah Masuk';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox'] = $this->db->query("SELECT * FROM inbox WHERE NId = '".$NId."' LIMIT 1")->row();


		$this->tempanri('backend/standart/administrator/log_registrasi/inbox_log_view_metadata_invoice', $this->data);
	}
	public function edit_naskah_masuk_setda($tipe,$NId){
		
		$this->data['title'] 	= 'Detail Naskah Masuk';
		$this->data['NId'] 		= $NId;
		$this->data['tipe'] 	= $tipe;
		$this->data['inbox'] = $this->db->query("SELECT * FROM inbox WHERE NId = '".$NId."' LIMIT 1")->row();
		
		$this->tempanri('backend/standart/administrator/log_registrasi/inbox_log_view_metadata', $this->data);
	}

}