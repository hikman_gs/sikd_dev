<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Integrasi_master extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		
	}


	public function index($id=0)
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$GRoleId = $this->input->post('GRoleId');
		if($GRoleId) {
			$GRoleId = $this->input->post('GRoleId');
		} else {
			$GRoleId = '1001';
		} 
		$eselon = $this->input->post('eselon');
		if($eselon) {
			$eselon = $this->input->post('eselon');
		} else {
			$eselon = 'XxJyPn38Yh.2';
		} 
		
		//$this->db->where('gjabatanId', $eselon);
		$this->db->where('satuan_kerja_id', $GRoleId);
		$this->db->from('siap_unit_kerja');
		$SukRole = $this->db->get();
		$SukRole = $SukRole->result();

		$this->data['GRoleId'] = $GRoleId;
		$this->data['SukRole'] = $SukRole;
		$this->data['title'] = 'Daftar Semua Naskah Dinas Lainnya';
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/masterdata', $this->data);
	}

	
}