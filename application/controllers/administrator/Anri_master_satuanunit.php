<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_satuanunit extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_satuanunit');
	}

	// List satuan unit
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_satuanunit/master_satuanunit_list', $this->data);
	}
	// Tutup list satuan unit

	// Tambah data satuan unit
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_satuanunit/master_satuanunit_add', $this->data);
	}
	// Tutup tambah data satuan unit

	// Proses simpan data satuan unit
	public function add_save()
	{

		$this->form_validation->set_rules('MeasureUnitName', 'MeasureUnitName', 'trim|required|max_length[50]');
		

		if ($this->form_validation->run()) {
			$table = 'master_satuanunit';

			$query = $this->db->query("SELECT (max(convert(substr(MeasureUnitId, 12), UNSIGNED)) + 1) as id FROM master_satuanunit")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

			$save_data = [
				'MeasureUnitId' => tb_key().'.'.$id,
				'MeasureUnitName' => $this->input->post('MeasureUnitName'),
			];
			
			$save_master_satuanunit = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_satuanunit'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_satuanunit'));			
		}

	}
	// Tutup proses simpan data satuan unit

	// Edit data satuan unit
	public function update($id)
	{
		$this->data['master_satuanunit'] = $this->model_master_satuanunit->find($id);
		$this->tempanri('backend/standart/administrator/master_satuanunit/master_satuanunit_update', $this->data);
	}
	// Tutup edit data satuan unit

	// Proses update data satuan unit
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('MeasureUnitName', 'MeasureUnitName', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'MeasureUnitName' => $this->input->post('MeasureUnitName'),
			];

			
			$save_master_satuanunit = $this->model_master_satuanunit->change($id, $save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_satuanunit'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_satuanunit'));			
		}

	}
	// Tutup proses update data satuan unit

	// Hapus data satuan unit
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_satuanunit'));							
	}
	// Tutup hapus data satuan unit

	// Proses hapus data satuan unit
	private function remove($id)
	{
		$master_satuanunit = $this->db->where('MeasureUnitId',$id)->delete('master_satuanunit');
		return $master_satuanunit;
	}
	// Tutup proses hapus data satuan unit
}