<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// import library dari REST_Controller
// require APPPATH . 'libraries/REST_Controller.php';

class Integrasi_api_simulasi extends REST_Controller {

    function __construct()
    {
        // Construct the parent class
        parent::__construct(); 
    }

	public function pegawai_post() {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
		
		$json = file_get_contents('php://input'); //outputs nothing
		$json_data = json_decode($json,true);
		
		foreach($json_data as $i) {
			if(is_array($i) || is_object($i)){
				foreach ($i as $h) {
					
					if(($h['peg_nip'] === null) ||  ($h['peg_nama'] === null) ||  ($h['peg_status'] === null)) {
						$this->response( [
							'status' => 'error',
							'message' => 'Semua Field harus diisi'
						], 200 );
					} else {
						$data = array(
							'peg_nip' => $h['peg_nip'], 
							'peg_nama' => $h['peg_nama'], 
							'peg_status' => $h['peg_status'],
							'tanggal' => date('Y-m-d H:i:s')
						);
						$insert_id = $this->db->insert('siap_notif_simulasi', $data);
						$results =  file_get_contents('https://siap.jabarprov.go.id/api/get-pegawai?nips[]='.$h['peg_nip']);
						$cetak   =  json_decode($results);
						$cetak   =  $cetak->result;  
						
						$datax = array( 
							'peg_nip' => $cetak[0]->peg_nip,
							'jabatan_id' =>$cetak[0]->jabatan_id,
							'unit_kerja_id' =>$cetak[0]->unit_kerja_id,
							'satuan_kerja_id' =>$cetak[0]->satuan_kerja_id,
							'peg_foto_url' =>$cetak[0]->peg_foto_url,
							'satuan_kerja_nama' =>$cetak[0]->satuan_kerja_nama,
							'unit_kerja_nama' =>$cetak[0]->unit_kerja_nama,
							'jabatan_jenis' =>$cetak[0]->jabatan_jenis,
							'pangkat' =>$cetak[0]->pangkat,
							'peg_nama_jenis_pns' =>$cetak[0]->peg_nama_jenis_pns,
							'kedudukan_pns' =>$cetak[0]->kedudukan_pns,
							'is_atasan_bayangan' =>$cetak[0]->is_atasan_bayangan,
							'is_atasan_tugas_tambahan' =>$cetak[0]->is_atasan_tugas_tambahan,
							'peg_email' =>$cetak[0]->peg_email
						);
						
						$this->db->update('siap_pegawai', $datax, array('peg_nip' => $h['peg_nip']));
						
						//$this->response( [
						//	'status' => 'success',
						//	'message' => 'Data berhasil diinput'
						//], 200 );
					}
				}
			}
		}
		
		$this->response( [
			'status' => 'success',
			'message' => 'Data berhasil diinput'
		], 200 );
	}
	
		
    public function notif_get()
    {
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
        $method = $_SERVER['REQUEST_METHOD'];
        if($method == "OPTIONS") {
            die();
        }
		$query = $this->db->query('SELECT peg_nip, peg_nama, peg_status FROM siap_notif');
		$sql   = $query->result();

        if ( $sql ) {
			$jumlah =  $query->num_rows();;
			$this->response( ["status" => "success", "data" => $sql, "count" => $jumlah], 200 );
		} else {
            // Set the response and exit
            $this->response( [
                'status' => 'error',
                'message' => 'Pegawai tidak ditemukan'
            ], 200 );
		}
    }
}