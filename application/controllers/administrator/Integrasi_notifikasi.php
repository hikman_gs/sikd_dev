<?php
defined('BASEPATH') OR exit('No direct script access allowed');



class Integrasi_notifikasi extends Admin
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('Model_integrasi', 'integrasi');
	}

	// List Bahasa
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/notifikasi', $this->data);
	}
	// Tutup list bahasa
	 
	
	public function detail($nip) {
		

		$data['nip'] = $nip;
		$data['groupData'] = $this->integrasi->get_group();

		//dd($data['groupData']);exit();
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/notifikasi_detail', $data);
	}
	
	public function updateemail(){

        $data = array( 
			'Email' => $this->input->post('email')
		);
		$update = $this->db->update('people', $data, array('PeopleUsername' => $this->input->post('nip')));
		if($update) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => TRUE));
    }
	
	public function updatepensiun(){
		
		$kddkanpns = $this->input->post('kedudukan_pns');
		if($kddkanpns == 'Aktif') {
			$pns = 1;
		} else {
			$pns = 0;
		}
        $data = array( 
			'PeopleIsActive' => $pns
		);
		$update = $this->db->update('people', $data, array('PeopleUsername' => $this->input->post('nip')));
		if($update) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => $data));
    }
	
	public function updatestatus($nip){

		$datax = array(
			'status' => 1,
		);
		$this->db->update('siap_notif', $datax, array('peg_nip' => $nip));
        redirect(base_url("administrator/integrasi_notifikasi"));
    }

	public function updateUnitKerja(){
		
		// ambil posisi terbaru berdasarkan uk_role param 
		$uk  = $this->db->query("SELECT * FROM mapping_per_pd WHERE unit_kerja_id = '".$this->input->post('unit_kerja_id')."'");
		$ukk = $uk->row(); 
		
		$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('PrimaryRoleId')."'");
		$role = $role1->row();
		
		/// Buat Update People
		$data = array( 
			'PrimaryRoleId'  => $ukk->RoleId,
			'PeoplePosition' => $role->RoleName,
			'RoleAtasan' => $role->RoleParentId, 
		);
		$updatex = $this->db->update('people', $data, array('PeopleUsername' => $this->input->post('nip')));

		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$this->db->update('siap_notif', $datax, array('peg_nip' => $this->input->post('nip')));
		
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => $ukk, "data" => $role, "data1" => $data));
    }
	
	
	public function updateAtasan(){

		/// Get NIP ATASAN
		$nipAtasan  = $this->db->query("SELECT nip_atasan FROM siap_pegawai WHERE peg_nip = '".$this->input->post('nip')."'");
		$nipAtasan1 = $nipAtasan->row(); 
		
		$NIPRoleAtasan  = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleUsername = '".$nipAtasan1->nip_atasan."'");
		$NIPRoleAtasan1 = $NIPRoleAtasan->row(); 
		
		/// Buat Update People
        $data = array(
			'RoleAtasan' => $NIPRoleAtasan1->PrimaryRoleId
		);
		$updatex = $this->db->update('people', $data, array('PeopleUsername' => $this->input->post('nip')));
		
		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$this->db->update('siap_notif', $datax, array('peg_nip' => $this->input->post('nip')));
		
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => $nipAtasan1, "data" => $data));
    }
	
	
	public function updateNip(){

		/// Buat Update People
        $data = array( 
			'NIP' => $this->input->post('nip')
		);

		$updatex = $this->db->update('people', $data, array('PeopleUsername' => $this->input->post('nip')));

		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$this->db->update('siap_notif', $datax, array('peg_nip' => $this->input->post('nip')));
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => $x, "data" => $data));
    }

    public function updateNik(){

		/// Buat Update People
		$nip = $this->input->post('nip');
        $data = array( 
			'NIK' => $this->input->post('nik')
		);
        $this->db->where('nip',$nip);
		$updatex = $this->db->update('people', $data);

		// $datax = array(
		// 	'tanggal_update' => date('Y-m-d H:i:s'),
		// 	'user_id' => $this->session->userdata('peopleusername')
		// );
		// $this->db->update('siap_notif', $datax, array('peg_nip' => $this->input->post('nip')));
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
        echo json_encode(array("status" => $x, "data" => $data));
    }
	
	public function updateNama(){

		/// Buat Update People
		$data = array( 
			'PeopleName' => $this->input->post('peg_nama'),
		);
		$updatex = $this->db->update('people', $data, array('PeopleUsername' => $this->input->post('nip')));
		
		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$update = $this->db->update('siap_notif', $datax, array('peg_nip' => $this->input->post('nip')));
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Data Berhasil diubah.</div>');
		}
		echo json_encode(array("status" => TRUE, "data" => $data));
    }

    public function updateGroup(){

		/// Buat Update People
		$this->db->where('PeopleUsername', $this->input->post('nip'));
		$updatex = $this->db->update('people', array('GroupId' => $this->input->post('groupId')));

		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Data Berhasil diubah.</div>');
		}
		echo json_encode(array("status" => TRUE, "data" => $data));
    }

    public function updatePangkat(){

		/// Buat Update People
		$this->db->where('PeopleUsername', $this->input->post('nip'));
		$updatex = $this->db->update('people', array('Pangkat' => $this->input->post('pangkat')));
		
		if($updatex) {
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"> <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Data Berhasil diubah.</div>');
		}
		echo json_encode(array("status" => TRUE, "data" => $data));
    }
 
	public function updateJabatan(){
		//var_dump($this->input->post('PeoplePosition'));exit();
		// ambil posisi terbaru berdasarkan 
		$roleIds = $this->input->post('RoleIds');
		 //var_dump($roleId);exit();
		
		$uk  = $this->db->query("SELECT * FROM mapping_per_pd WHERE unit_kerja_id = '".$this->input->post('unit_kerja_id')."'");
		$ukk = $uk->row(); 
		/// Buat Update People
		$cekstruktural  = $this->db->query("SELECT jabatan_jenis FROM siap_pegawai WHERE peg_nip = '".$this->input->post('nip')."'");
		$cekstruktural1 = $cekstruktural->row(); 
		$flag2 = '0';

		$people22 = $this->db->query("SELECT RoleId,eselon FROM mapping_per_pd WHERE unit_kerja_id = '".$this->input->post('unit_kerja_id')."'");
		$qry22 = $people22->row();
		$people33 = $this->db->query("SELECT gjabatanId,GRoleId FROM role WHERE RoleId = '".$qry22->RoleId."'");
		$qry33 = $people33->row();

		if($cekstruktural1->jabatan_jenis != 'Struktural') {
			/*if (($cekstruktural1->jabatan_jenis == 'Fungsional Umum') OR (($cekstruktural1->jabatan_jenis == 'Fungsional Tertentu') and ($ukk->gjabatanId != 'XxJyPn38Yh.11') and ($ukk->gjabatanId != 'XxJyPn38Yh.7'))){
				//$role1  = $this->db->query("SELECT * FROM role WHERE RoleParentId = '".$ukk->RoleId."'");
				$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('RoleIds')."'");
			
			}else{
				$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('RoleIds')."'");
				//$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$ukk->RoleId."'");
				//$role1  = $this->db->query("SELECT * FROM role WHERE RoleParentId = '".$this->input->post('RoleId')."'");
			

			}*/
			if (($cekstruktural1->jabatan_jenis == 'Fungsional Umum') OR (($cekstruktural1->jabatan_jenis == 'Fungsional Tertentu') and ($qry33->gjabatanId != 'XxJyPn38Yh.11') and ($qry33->gjabatanId != 'XxJyPn38Yh.7'))){
								
								if (($cekstruktural1->jabatan_jenis == 'Fungsional Tertentu') and ($qry22->eselon != 'IV.a') and ($qry22->eselon != 'IV.b')) {
									//$role1 = $this->db->query("SELECT * FROM role WHERE GRoleId = '".$qry3->GRoleId."' and gjabatanId in ('XxJyPn38Yh.7','XxJyPn38Yh.11') ");
									$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('RoleIds')."'");
									$flag2 = '1';
									//var_dump('IEU '.$people.'TSET');exit();
								}else{
									
									//$role1 = $this->db->query("SELECT * FROM role WHERE RoleParentId = '".$qry2->RoleId."'");
									$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('RoleIds')."'");
									$flag2 = '1';
									//var_dump('IEU '.$people.'TSET');exit();
								}
			}
			if ($flag2 == '0') {
				$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$ukk->RoleId."'");
			}
		} else {
			$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$ukk->RoleId."'");
			//$role1  = $this->db->query("SELECT * FROM role WHERE RoleParentId = '".$this->input->post('RoleId')."'");
		}
		
		$role   = $role1->row(); 

		
	
		$RoleName1  = $role->RoleName;
		$rest       = substr($RoleName1, 0, 10);
		//$gjabatanId = substr($role->gjabatanId, -1, 1);
		$grupid     = '';
		
		/*if((strtolower($rest) != 'sekretaris') AND ($gjabatanId == '4')) {
			$grupid = '3';
		} elseif((strtolower($rest) == 'sekretaris') AND ($gjabatanId == '4')) {
			$grupid = '4';
		} elseif (($gjabatanId == '1') OR ($gjabatanId == '2') OR ($gjabatanId == '3') OR ($gjabatanId == '5')) {
			$grupid = '3';
		} elseif (($gjabatanId == '7') OR ($gjabatanId == '11')) {
			$grupid = '7';
		}*/

		if((strtolower($rest) != 'sekretaris') AND ($role->gjabatanId == 'XxJyPn38Yh.4')) {
			$grupid = '3';
		} elseif((strtolower($rest) == 'sekretaris') AND ($role->gjabatanId == 'XxJyPn38Yh.4')) {
			$grupid = '4';
		} elseif (($role->gjabatanId == 'XxJyPn38Yh.1') OR ($role->gjabatanId == 'XxJyPn38Yh.2') OR ($role->gjabatanId == 'XxJyPn38Yh.3') OR ($role->gjabatanId == 'XxJyPn38Yh.5')) {
			$grupid = '3';
		} elseif (($role->gjabatanId == 'XxJyPn38Yh.7') OR ($role->gjabatanId == 'XxJyPn38Yh.11')) {
			$grupid = '7';
		}

		if (($role->gjabatanId == 'XxJyPn38Yh.1') OR ($role->gjabatanId == 'XxJyPn38Yh.2') OR ($role->gjabatanId == 'XxJyPn38Yh.3')){
			$RoleAtasan2 = 'uk.1';
		}else
		{
			$RoleAtasan2 = $role->RoleParentId;
		}
		
        $data = array( 
			'PrimaryRoleId'  => $role->RoleId,
			'PeoplePosition' => $role->RoleName,
			'RoleAtasan' => $RoleAtasan2,
			'GroupId' => $grupid
		);
		$updatepeople = $this->db->update('people', $data, array('PeopleUsername' => $this->input->post('nip')));
		
		$datax = array(
			'tanggal_update' => date('Y-m-d H:i:s'),
			'user_id' => $this->session->userdata('peopleusername')
		);
		$update = $this->db->update('siap_notif', $datax, array('peg_nip' => $this->input->post('nip')));
		if($updatepeople) { 
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>Data Berhasil diubah.</div>');
		}
		
		echo json_encode(array("status" => TRUE, "role" => $ukk, "updatepeople" => $data, "cekstruktural1" => $cekstruktural1));
	}

}