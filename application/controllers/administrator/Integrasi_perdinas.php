<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Integrasi_perdinas extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('Perdinas_model');
	}

	// List Bahasa
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/data_perdinas', $this->data);
	}
	// Tutup list bahasa
	
	public function get_data1($p)
	{
		$draw   = intval($this->input->get("draw"));
		$start  = intval($this->input->get("start"));
		$length = intval($this->input->get("length"));

		$this->db->where('satuan_kerja_id', $p);
		$this->db->from('siap_pegawai');
		$list_users = $this->db->get();
 
		$data = [];
		$n = 1;
		foreach($list_users->result() as $r) {
			$data[] = array( 
				$r->peg_nip,
				$r->peg_nama,
				$r->jabatan_nama,
							'<div class="text-center">
								<a title="Detail" href="'.base_url("administrator/integrasi_notifikasi/detail/".$r->peg_nip).'" class="btn btn-success  btn-sm" role="button"><i class="fa fa-info" aria-hidden="true"></i> Detail</a> 
							</div>'
			); 
		}
		
		$result = array(
                  "draw" => $draw,
                  "recordsTotal" => $list_users->num_rows(),
                  "recordsFiltered" => $list_users->num_rows(),
                  "data" => $data
               );
		echo json_encode($result);
		exit();
	}
	
	
	public function get_data($GRoleId) {
		//1002
		$list = $this->Perdinas_model->get_datatables($GRoleId);
		$data = array();
        $no   = $_POST['start'];
        foreach ($list as $l) {

            $no++;
            $row = array(); 

            $row[] = '<div class="text-right">'.$no.'</div>';
            $row[] = $l->peg_nip;
            $row[] = $l->peg_nama;
            $row[] = $l->jabatan_nama; 
            $row[] = '<div class="text-center">
								<a title="Detail" href="'.base_url("administrator/integrasi_notifikasi/detail/".$l->peg_nip).'" class="btn btn-success  btn-sm" role="button"><i class="fa fa-info" aria-hidden="true"></i> Detail</a> 
							</div>'; 
 
            $data[] = $row;
        }
 
        $output = array(
			"draw"            => $_POST['draw'],
			"recordsTotal"    => $this->Perdinas_model->count_all($GRoleId),
			"recordsFiltered" => $this->Perdinas_model->count_filtered($GRoleId),
			"data"            => $data,
		);
        echo json_encode($output);
	}
	
}