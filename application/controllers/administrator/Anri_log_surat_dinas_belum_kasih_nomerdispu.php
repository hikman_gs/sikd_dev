<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_log_surat_dinas_belum_kasih_nomerdispu extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('Model_list_surat_dinas_belum_dinomeridispu');
	}

	//Log Surat Dinas Keluar
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Surat Keluar belum kasih nomer';
		$this->tempanri('backend/standart/administrator/surat_dinas/log_surat_dinas_belum_kasih_nomerdispu', $this->data);			
	}
	//End Log Surat Dinas Keluar
	public function nomor_dong()
	{


		$NId = $this->input->post('NId_Temp3');
		$GIR_Id = $this->input->post('NId_Temp3');

		$nomor = $this->input->post('nonaskah');

		$tahun 	= date('Y');
		$bulan = date('m');

		$cari = $this->db->query("SELECT ClId FROM konsep_naskah WHERE NId_Temp = '".$NId."'")->row()->ClId;	
		    
		$klas = $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$cari."'")->row()->ClCode;

		$unit = $this->db->query("SELECT RoleCode FROM role WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->RoleCode;
		
		$nosurat = $nomor.'/'.$klas.'/'.$unit.'/'.$tahun;

		$save_people = $this->db->where('NId_Temp', $NId)->where('GIR_Id',$GIR_Id)->update('konsep_naskah',['Number' => $nomor,'nosurat' => $nosurat]);
		
		set_message('Nomor Berhasil Disimpan', 'success');
		$this->load->library('user_agent');
		redirect($this->agent->referrer());					
	

	}
	//Tutup Beri Nomor	
	//Ambil Data log surat dinas keluar
	public function get_data_surat_dinas_keluar()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->Model_list_surat_dinas_belum_dinomeridispu->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;
			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$field->JenisId."'")->row()->JenisName;	
			$row[] = $field->nosurat;		
			$row[] = date('d-m-Y',strtotime($field->TglNaskah));
			$row[] = $field->Hal;

			$xx3  = $this->db->query("SELECT nosurat FROM konsep_naskah WHERE NId_temp = '".$field->GIR_Id."'")->row();	
			$xx2  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_temp = '".$field->GIR_Id."'")->row();	

			if ($xx2->Konsep == 5 ) {
				$row[] = "<font color = 'blue'><b>Naskah Telah Dibatalkan Untuk Dikirim Oleh Penandatangan</b></font>";
				
			} elseif ($xx2->Konsep == 2 ) {

				$xxz  = $this->db->query("SELECT ReceiverAs, RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->row();
				
				if($xxz->ReceiverAs == 'to_keluar') {

					$z1 = $xxz->RoleId_To;
					$z2 = '';

				} else {

					$count_tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

					if($count_tujuan > 0){

					$tujuan  = $this->db->query("SELECT RoleId_To FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
						foreach ($tujuan as $key => $value) {					  
						  $exp = explode(',', $value->RoleId_To);
						  if($value->RoleId_To == '-'){
						  	$z1 = '';
						  } else {
						  	  $z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
							  	foreach ($exp as $k => $v) {
							  		$z1 .= "<tr>";
							    	$z1 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
							    	$z1 .= "</tr>";
							  	}
							  $z1 .= "</table>";	
						  }			  
						}
					}

					$count_tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->num_rows();

					if($count_tembusan > 0){

					$tembusan  = $this->db->query("SELECT RoleId_Cc FROM konsep_naskah WHERE NId_temp = '".$field->NId_Temp."'")->result();
						foreach ($tembusan as $key => $value) {					  
						  $exp = explode(',', $value->RoleId_Cc);
						  if(($value->RoleId_Cc == '') || ($value->RoleId_Cc == '-')){
						  	$z2 = '';
						  } else {
						  	  $z2 = "<br> <b>Tembusan :</b> <table border ='0' cellpadding='0' cellspacing='0'>";
							  	foreach ($exp as $k => $v) {
							  		$z2 .= "<tr>";
							    	$z2 .= "<td> - ".$this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName."</td>";
							    	$z2 .= "</tr>";
							  	}
							  $z2 .= "</table>";	
						  }			  
						}
					}
				}
				
				$row[] = $z1.$z2;

			} elseif ($xx3->nosurat <= 0 ) {
				$row[] = "<font color = 'red'><b>Naskah Belum Diberi nomer oleh Pengolah</b></font>";
			} elseif ($xx2->Konsep == 0 ) {
				$row[] = "<font color = 'brown'><b>Naskah Sudah Disetujui dan Belum Dikirim Oleh Penandatangan</b></font>";
			} else {
				$row[] = "<font color = 'red'><b>Naskah Belum Dikirim Oleh Penandatangan</b></font>";
			}


          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	

			$row[] = $z21;
			
			$xyz  = $this->db->query("SELECT Konsep FROM konsep_naskah WHERE NId_Temp = '".$field->GIR_Id."'")->row();

			if ($xyz->Konsep == 1) { 
				$row[] = '<button type="button" onclick=ko_nomor('.$field->NId_Temp.','.$field->GIR_Id.') title="Masukkan Nomor Naskah" class="btn btn-danger btn-sm"><i class="fa fa-sticky-note-o"></i></button> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf_log/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
			} else {

				$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->GIR_Id."' AND Keterangan = 'outbox'")->num_rows();

				if($x > 0) {

					$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->NId_Temp."' AND Keterangan = 'outbox'")->row();

					$row[] = '<button type="button" onclick=ko_nomor('.$field->NId_Temp.','.$field->GIR_Id.') title="Masukkan Nomor Naskah" class="btn btn-danger btn-sm"><i class="fa fa-sticky-note-o"></i></button> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';

				} else {

					$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->GIR_Id).'" title="Lihat Histori Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$field->GIR_Id).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a>';
					
				}	
			}



			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Model_list_surat_dinas_belum_dinomeridispu->count_all(),
			"recordsFiltered" => $this->Model_list_surat_dinas_belum_dinomeridispu->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data log surat dinas keluar




}