<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_role extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_role');
	}

	// List role
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/role/role_list', $this->data);
	}
	// Tutup list role

	// Proses simpan data role
	public function add_save()
	{

		$this->form_validation->set_rules('RoleName', 'RoleName', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('RoleDesc', 'RoleDesc', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('RoleCode', 'RoleCode', 'trim|required|max_length[25]');
		$this->form_validation->set_rules('gjabatanId', 'GjabatanId', 'trim|required|max_length[14]');
		

		if ($this->form_validation->run()) {
			$id = $this->input->post('RoleId');
			$save_data = [
				'RoleKey' => tb_key(),
				'RoleId' => parent_unit($id),
				'RoleParentId' => $this->input->post('RoleId'),//RoleParentId select dari data 
				'RoleName' => $this->input->post('RoleName'),
				'RoleDesc' => $this->input->post('RoleDesc'),
				'RoleCode' => $this->input->post('RoleCode'),
				'RoleStatus' => (empty($this->input->post('RoleStatus')) ? '0' : '1'),
				'gjabatanId' => $this->input->post('gjabatanId'),
				'GRoleId' => $this->input->post('GRoleId'),
			];

			
			$save_role = $this->model_role->store($save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_role'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_role'));
		}

	}
	// Tutup proses simpan data role

	// Proses update data role
	public function update_save()
	{
		
		$id 	= $this->input->post('RoleId');
		$data 	= array(
	        'RoleName' => $this->input->post('RoleName'),
			'RoleDesc' => $this->input->post('RoleDesc'),
			'RoleCode' => $this->input->post('RoleCode'),
			'RoleStatus' => (empty($this->input->post('RoleStatus')) ? '0' : '1'),
			'gjabatanId' => $this->input->post('gjabatanId'),
			'GRoleId' => $this->input->post('GRoleId'),
		);

		$this->model_role->edit($id,$data);

	}
	// Tutup proses update data role

	// Hapus data role
	public function delete($id = null)
	{
		if ($this->db->query("SELECT RoleParentId FROM role WHERE RoleParentId = '".strip_tags($this->input->post('data_id'))."'")->num_rows() > 0) {
			$data = array('status' => 'error_check_parent',);
			echo json_encode($data);
		}elseif($this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE RoleId_To = '".strip_tags($this->input->post('data_id'))."'")->num_rows() > 0){
			$data = array('status' => 'error_berkas',);
			echo json_encode($data);
		}elseif($this->db->query("SELECT RoleId_From FROM inbox_receiver WHERE RoleId_From = '".strip_tags($this->input->post('data_id'))."'")->num_rows() > 0){
			$data = array('status' => 'error_berkas',);
			echo json_encode($data);
		}elseif($this->db->query("SELECT PrimaryRoleId FROM people WHERE PrimaryRoleId = '".strip_tags($this->input->post('data_id'))."'")->num_rows() > 0){
			$data = array('status' => 'error_aja',);
			echo json_encode($data);			
		}else{
			if ($this->input->post('parent') == 'root') {
				$data = array('status' => 'error_check_parent',);
				echo json_encode($data);
			}else{
				$data = array('status' => 'success');
				$this->model_role->hapus($this->input->post('data_id'));
				echo json_encode($data);
			}
		}	
	}
	// Tutup hapus data role
	
}