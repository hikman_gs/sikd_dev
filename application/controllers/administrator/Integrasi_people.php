<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Integrasi_people extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		$this->load->model('Master_model');
		$this->load->model('Model_integrasi', 'integrasi');
	}

	public function index($id=0)
	{
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		$data['perangkat']=$this->integrasi->get_mappd();
		$data['group']=$this->integrasi->get_group();
        //cek akses ambil dari helper
		
		// $GRoleId = $this->input->post('mapping_pd_id');
		// if($GRoleId) {
		// 	$GRoleId = $this->input->post('mapping_pd_id');
		// } else {
		// 	$GRoleId = '0';
		// }
		
		// $this->db->where('unit_kerja_id !=', '');
		// $this->db->where('satuan_kerja_id', $GRoleId);
		// $this->db->from('siap_unit_kerja');
		
		// $SukRole = $this->db->get();
		// $SukRole = $SukRole->result(); 
		
		// $this->data['GRoleId'] = $GRoleId;
		// $this->data['SukRole'] = $SukRole;
		$this->tempanri('backend/standart/administrator/integrasi_sikd_siap/masterpeople', $data);
	}

	public function get_pegawai_pd()
	{
		
		$satuan_kerja_id = $this->input->post('satuan_kerja');
		// echo $satuan_kerja_id;
		$list = $this->integrasi->get_siap_pegawai($satuan_kerja_id);
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $siap) {
			$no++;
			$row = array();
			$row[] = $no;
			$row[] = $siap->peg_nama;
			$row[] = $siap->unit_kerja_nama;
			$row[] = $siap->pangkat;
			$row[] = $this->cek_username_sikd($siap->peg_nip);
			$row[] = $this->cek_nip_tambah($siap->peg_nip);
			$data[] = $row;
		}
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->integrasi->siap_pegawai_all($satuan_kerja_id),
			"recordsFiltered" => $this->integrasi->siap_pegawai_filtered($satuan_kerja_id),
			"data" => $data,
		);
		echo json_encode($output);
	}

	public function cek_username_sikd($nip)
	{
		$cek = $this->integrasi->cek_username($nip);
		if($cek>0){
			return "<span class='label label-success'>Ada</span>";
		}else{
			return "<span class='label label-danger'>--</span>";
		}
	}

	public function cek_nip_tambah($nip)
	{
		$cek = $this->integrasi->cek_username($nip);
		if($cek>0){
			return "";
		}else{
			return '<a class="btn btn-block btn-success btn-sm" href="javascript:void(0)" title="Tambah data SIKD" onclick="detail_pegawai(\''.$nip.'\')" style="color:white"><i class="fa fa-plus-circle"></i> Tambah ke SIKD</a>';;
		}
	}

	public function tambah_people()
	{
		$role = $this->input->post('input_role');
		$nip = $this->input->post('input_nip');
		$nama = $this->input->post('input_nama');
		$people = array(
			'PeopleKey' 		=> 'XxJyPn38Yh',
			'PeopleId'			=> $this->integrasi->people_last_id()+1,
			'PeopleName'		=> $nama,
			'PeoplePosition' 	=> $this->input->post('input_jabatan'),
			'PeopleUsername'	=> $nip,
			'PeoplePassword'	=> sha1('Simanisjuara20!'),
			'PeopleActiveStartDate' => date("Y-m-d"),
			'PeopleActiveEndDate'=> date('Y-m-d', strtotime('+30 years')),
			'PeopleIsActive'	=> '1',
			'PrimaryRoleId'		=> ($role=='')?$role='':$role,
			'GroupId'			=> $this->input->post('input_group'),
			'RoleAtasan'		=> ($role=='')?'':$this->integrasi->get_role_atasan($role),
			'NIP'				=> $nip,
			'ApprovelName'		=> $nama,
			'Email'				=> $this->input->post('input_email'),
			'NIK'				=> $this->input->post('input_nik'),
			'Pangkat'			=> $this->input->post('input_pangkat'),
			'eselon'			=> $this->input->post('input_eselon')

		);
		$this->integrasi->tambah_people($people);
		// print_r($people);
		echo json_encode(array("status" => TRUE)); 
	}

	// public function asdf_group()
	// {
		// $this->db->like('PeopleUsername','19');
		// $people = $this->db->get('people')->result();
		// $no;
		// foreach ($people as $list) {
		// 	$no++;
		// 	// echo $list->PeopleUsername."</br>";
		// 	$data = array(
		// 		'pangkat' => $this->asdf_pegawai($list->PeopleUsername),
		// 	);
		// 	// print_r($data);
		// 	// echo $list->PeopleId."</br>";
		// 	$this->asdf_update_grup($list->PeopleId, $data);
		// 	// echo $list->PeopleUsername." eselon nya ".$this->asdf_pegawai($list->PeopleUsername)."</br>";
		// 	// echo $this->asdf_pegawai($list->PeopleUsername);
		// }
		// echo "done ".$no;
		// $role = $this->db->get('mapping_per_pd');
		// $list = $role->result();
		// $batch_role = array();
		// foreach ($list as $key) {
		// 	$id_role=str_replace('XxJyPn38Yh.','',$key->GRoleId);
		// 	$temp = array(
		// 		'mapping_per_pd_id'=>$key->mapping_per_pd_id,
		// 		'rolecode_id' => $id_role
		// 	);
		// 	array_push($batch_role, $temp);
		// 	// echo $id_role."</br>";
		// }
		// // dd($batch_role);
		// $this->db->update_batch('mapping_per_pd', $batch_role, 'mapping_per_pd_id');
	// }

	// public function asdf_update_grup($id, $data)
	// {
	// 	$this->db->where('PeopleId', $id);
	// 	$this->db->update('people', $data);
		
	// }

	// public function asdf_pegawai($nip)
	// {
	// 	// $this->db->like()
	// 	$this->db->where('peg_nip', $nip);
	// 	$test = $this->db->get('siap_pegawai');
	// 	$query = $test->row(); 
	// 	return $query->pangkat;
	// 	// $eselon = $query->eselon_nm;
	// 	// $grup = $this->find_group($query->eselon_nm, $query->jabatan_nama);
	// 	// return $grup;
		
	// }
		
	/**
	 * Method find_group
	 *
	 * @param $eselon $eselon [explicite description]
	 *
	 * @return ditentukan based tabel groups
	 * eselon_nm ada III / IV dianggap struktural GroupId = 3
	 * jabatan_nama SEKRETARIS groupId = 4
	 * eselon_nm null GroupId = 7
	 */
	public function find_group($eselon, $jabatan)
	{
		if(preg_match('~\b(II|III|IV)\b~i',$eselon)){
			if(preg_match('~\b(SEKRETARIS)\b~i',$jabatan)){
				return "4";
			}else{
				return "3";
			}
		}else{
			return "7";
		}
	}

	public function get_detail_pegawai($nip)
	{
		$data['group'] = $this->find_group($data['people']['eselon_nm'], $data['people']['jabatan_nama']);
		$data['people'] = $this->integrasi->get_detail_pegawai($nip);
		if($data['people']['role_id']==null || strlen(trim($data['people']['role_id']))<1){
			$data['mapping_kosong'] = 'TRUE';
		}else{
			$data['role'] = $this->integrasi->get_role($data['people']['role_parent_id']);
			$data['role_pegawai'] = $this->integrasi->get_role_pegawai($data['people']['role_id']);
			$data['mapping_kosong'] = 'FALSE';
		}
		echo json_encode($data);
	}

	public function update(){

		$role1  = $this->db->query("SELECT * FROM role WHERE RoleId = '".$this->input->post('role')."'");
		$role = $role1->row();
		$data = array( );
		if ($role) {
			$data = array( 
				'RoleDesc' => $role->RoleDesc,
				'gjabatanId' => $role->gjabatanId,
				'RoleId' => $role->RoleId,
				'RoleParentId' => $role->RoleParentId,
				'GRoleId' => $role->GRoleId
			);
			$update = $this->db->update('mapping_per_pd', $data, array('mapping_per_pd_id' => $this->input->post('mapping_per_pd_id')));
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		} else {
			$data = array( 
				'RoleDesc' =>'',
				'gjabatanId' => '',
				'RoleId' => '',
				'RoleParentId' => '',
				'GRoleId' =>''
			);
			$update = $this->db->update('mapping_per_pd', $data, array('mapping_per_pd_id' => $this->input->post('mapping_per_pd_id')));
			$this->session->set_flashdata('items_', '<div class="alert alert-info alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button> Data Berhasil diubah.</div>');
		}
		
		echo json_encode(array("status" => $data, "role" => $role));
	}
}