<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_disposisi extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_disposisi');
	}

	// List disposisi
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_disposisi/master_disposisi_list', $this->data);
	}
	// Tutup list disposisi

	// Tambah data disposisi
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_disposisi/master_disposisi_add', $this->data);
	}
	// Tutup tambah data disposisi

	// Proses simpan data disposisi
	public function add_save()
	{

		$this->form_validation->set_rules('DisposisiName', 'DisposisiName', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('gjabatanId', 'GjabatanId', 'trim|required|max_length[14]');
		

		if ($this->form_validation->run()) {
			$table = 'master_disposisi';

			$query = $this->db->query("SELECT (max(convert(substr(DisposisiId, 12), UNSIGNED)) + 1) as id FROM master_disposisi")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}

      		$save_data = [
				'DisposisiId' => tb_key().'.'.$id,
				'DisposisiName' => $this->input->post('DisposisiName'),
				'gjabatanId' => $this->input->post('gjabatanId'),
			];

			
			$save_master_disposisi = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_disposisi'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_disposisi'));			
		}

	}
	// Tutup proses simpan data disposisi

	// Edit data disposisi
	public function update($id)
	{
		$this->data['master_disposisi'] = $this->model_master_disposisi->find($id);
		$this->tempanri('backend/standart/administrator/master_disposisi/master_disposisi_update', $this->data);
	}
	// Tutup edit data disposisi

	// Proses update data disposisi
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('DisposisiName', 'DisposisiName', 'trim|required|max_length[100]');
		$this->form_validation->set_rules('gjabatanId', 'GjabatanId', 'trim|required|max_length[14]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'DisposisiName' => $this->input->post('DisposisiName'),
				'gjabatanId' => $this->input->post('gjabatanId'),
			];

			
			$save_master_disposisi = $this->model_master_disposisi->change($id, $save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_disposisi'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_disposisi'));			
		}

	}
	// Tutup proses update data disposisi

	// Hapus data disposisi
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_disposisi'));			
	}
	// Tutup hapus data disposisi

	// Proses hapus data disposisi
	private function remove($id)
	{
		$master_disposisi = $this->db->where('disposisiId',$id)->delete('master_disposisi');
		return $master_disposisi;
	}
	// Tutup proses hapus data disposisi
}