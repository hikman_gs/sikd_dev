<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_suratdinas_kirim_keluar_btl extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_sudin_keluar_btl');
	}

	//Naskah Masuk Belum TL
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Daftar Semua Naskah Masuk Belum Tindaklanjut';
		$this->tempanri('backend/standart/administrator/mail_tl/list_sudin_kirim_keluar_btl', $this->data);
	}
	//Tutup Naskah Masuk Belum TL
		
	//Ambil Data Seluruh Naskah Masuk Belum TL
	public function get_data_surat_all_btl()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_sudin_keluar_btl->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;

			if ($field->UrgensiName=='Amat Segera') {
				$xx = "<span class='badge bg-red'>".$field->UrgensiName."</span>";
			} elseif ($field->UrgensiName=='Segera') {
				$xx = "<span class='badge bg-red'>".$field->UrgensiName."</span>";
			} else {
				if ($field->StatusReceive=='unread') {
					$xx = "<b>".$field->UrgensiName."</b>";
				} else {
					$xx = $field->UrgensiName;
				}
			}
			$row[] = $xx;

			if ($field->StatusReceive=='unread') {
				$xx1 = "<b>".$field->Nomor."</b>";
				$xx2 = "<b>".$field->Hal."</b>";

				if($field->ReceiverAs=='bcc') {
					$xx3 = "<b>".$field->Namapengirim.$field->Instansipengirim." <font color=red> (Tembusan) </font></b>";
				} else {
					$xx3 = "<b>".$field->Namapengirim.$field->Instansipengirim."</b>";		
				}


				$xx4 = "<b>".date('d-m-Y',strtotime($field->Tgl))."</b>";
				$xx5 = "<b>".date('d-m-Y H:i:s',strtotime($field->NTglReg))."</b>";
			} else {
				$xx1 = $field->Nomor;
				$xx2 = $field->Hal;

				if($field->ReceiverAs=='bcc') {
					$xx3 = $field->Instansipengirim." <font color=red><b> (Tembusan) </b></font>";
				} else {
					$xx3 = "".$field->Namapengirim.$field->Instansipengirim."";			
				}
				$xx4 = date('d-m-Y',strtotime($field->Tgl));
				$xx5 = date('d-m-Y H:i:s',strtotime($field->NTglReg));
			}
			$row[] = $xx1;
			$row[] = $xx2;
			$row[] = $xx3;
			$row[] = $xx4;
			$row[] = $xx5;

			$row[] = '<a href="'.site_url('administrator/anri_mail_tl/view_naskah_kirim_keluar/view/'.$field->NId.'?dt=2').'" title="Lihat Detail Naskah" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" target="_new" title="Lihat Histori Naskah" class="btn btn-warning btn-sm"><i class="fa fa-list"></i></a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_sudin_keluar_btl->count_all(),
			"recordsFiltered" => $this->model_list_sudin_keluar_btl->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Masuk Belum TL

}