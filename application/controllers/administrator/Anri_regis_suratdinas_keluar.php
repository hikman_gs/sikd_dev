<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_regis_suratdinas_keluar extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');	
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_konsepnaskah_koreksi');
	}

	// Registrasi Surat Dinas Keluar
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		$this->data['title'] = 'Registrasi Surat Dinas Keluar';
		$this->tempanri('backend/standart/administrator/surat_dinas/regis', $this->data);
	}
	//Registrasi Surat Dinas Keluar

	//Fungsi Lihat Surat Dinas Keluar 
	public function lihat_naskah_dinas3()
	{

		$tanggal = date('Y-m-d H:i:s');
		$this->load->library('HtmlPdf');
      	ob_start();

      	$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');
      	$this->data['Alamat'] 		= $this->input->post('Alamat');
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$this->data['Jumlah'] 		= $this->input->post('Jumlah');
		$this->data['RoleCode'] 	= $this->input->post('RoleCode');

		$this->data['SifatId'] 		= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$this->input->post('SifatId')."'")->row()->SifatName;

		$this->data['type_naskah'] 		= $this->db->query("SELECT type_name FROM master_type_surat WHERE type_naskah = '".$this->input->post('type_naskah')."'")->row()->Type_Name;

		$this->data['MeasureUnitId'] = $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$this->input->post('MeasureUnitId')."'")->row()->MeasureUnitName;			
		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['lampiran'] 		= $this->input->post('lampiran');
		$this->data['lampiran2'] 	= $this->input->post('lampiran2');
		$this->data['lampiran3'] 	= $this->input->post('lampiran3');
		$this->data['lampiran4'] 	= $this->input->post('lampiran4');


		$this->data['TglReg'] 		= date('Y-m-d H:i:s');
		$this->data['Hal'] 			= $this->input->post('Hal');

		if($this->input->post('TtdText') == 'none') {
			$this->data['Approve_People']	= $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$this->data['Approve_People'] 	= $this->input->post('tmpid_atasan');		
		} else {
			$this->data['Approve_People'] 	= $this->input->post('Approve_People');
		}

		if($this->input->post('TtdText2') == 'AL') {
			$this->data['Approve_People3'] 	= $this->input->post('tmpid_atas_nama');	
		} else {
			$this->data['Approve_People3'] 	= $this->input->post('Approve_People3');
		}


		$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$this->input->post('ClId')."'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');
		$this->data['TtdText2']     = $this->input->post('TtdText2');

		if($this->input->post('TtdText') == 'none') {
			$this->data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;		
		} else {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}
		
        $this->load->view('backend/standart/administrator/pdf/view_suratdinas_pdf',$this->data);

	    $content = ob_get_contents();
	    ob_end_clean();
        
        $config = array(
            'orientation' 	=> 'p',
            'format' 		=> 'F4',
            'marges' 		=> array(30, 30, 20, 15),
        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('naskah_dinas_tindaklanjut-'.$tanggal.'.pdf', 'H');

	}
	// Akhir Fungsi Surat Dinas Keluar


	//Fungsi Simpan Regis Surat Dinas Keluar
	public function post_suratdinas_keluar()
	{

		$gir_awal= $this->session->userdata('peopleid').date('dmyhis');	
		$jumlah_karakter    =strlen($gir_awal);		
		if ($jumlah_karakter == 17){

			$sub_gir = substr($gir_awal,0,16);
			$cek_NId_Temp="SELECT NId_Temp FROM konsep_naskah WHERE NId_Temp = '$_POST[$sub_gir]'";
			if ($cek_NId_Temp > 0) {

				$sub_gir = substr($gir_awal,0,16)+1;

			}else{

				$sub_gir = substr($gir_awal,0,16);

			}

		}else{

			$sub_gir = $this->session->userdata('peopleid').date('dmyhis');

		}

		// $gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Lokasi;

		if($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;	
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}	

		if($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');		
		} else {
			$x = $this->input->post('Approve_People');
		}


	if($this->input->post('TtdText2') == 'none') {
		$a =('');
		$a = ('');
		} elseif($this->input->post('TtdText2') == 'AL') {		
		$a = $this->input->post('tmpid_atas_nama');	
		} else {
		$a = $this->input->post('Approve_People3');
		}	

		if($this->input->post('TtdText2') == 'none') {
		$b =('');
		$b = ('');
		} elseif($this->input->post('TtdText2') == 'AL') {		
		$b = $this->input->post('tmpid_atas_nama');	
		} else {
		$b = $this->input->post('Approve_People3');
		}	



		try{

			$data_konsep = [
				// 'NId_Temp'   		=> $gir,
				// 'GIR_Id'       		=> $gir,
				'NId_Temp'   		=> $sub_gir,
				'GIR_Id'       		=> $sub_gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'type_naskah'       => $this->input->post('type_naskah'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('RoleCode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $this->input->post('RoleId_To'),
				'Alamat'       		=> $this->input->post('Alamat'),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'lampiran'       	=> $this->input->post('lampiran'),
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Approve_People'    => $x,
				'Approve_People3'   => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxkeluar',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 		=> $z,
				'Nama_ttd_atas_nama' 	=> $x,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];
			//memunculkan konsep awal
			$data_konsep_awal = [
				// 'NId_Temp'   		=> $gir,
				// 'GIR_Id'       		=> $gir,
				'NId_Temp'   		=> $sub_gir,
				'GIR_Id'       		=> $sub_gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'type_naskah'       => $this->input->post('type_naskah'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('RoleCode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $this->input->post('RoleId_To'),
				'Alamat'       		=> $this->input->post('Alamat'),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'lampiran'       	=> $this->input->post('lampiran'),
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Approve_People'    => $x,
				'Approve_People3'    => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxkeluar',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 		=> $z,
				'Nama_ttd_atas_nama' 	=> $x,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'id_koreksi'    	=> $sub_gir,
			];

			//akhir memunculkan konsep awal

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
			            // 'GIR_Id' 			=> $gir,
			            'GIR_Id' 			=> $sub_gir,
			            'NId' 				=> $data_konsep['NId_Temp'],
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);

				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);
			//memunculkan konsep awal
			$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep_awal);
			//akhir memunculkan konsep awal


			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid').date('dmyhis');
			
			$save_recievers = [
				// 'NId' 			=> $gir,
				// 'NKey' 			=> tb_key(),
				// 'GIR_Id' 		=> $girid_ir,
				'NId' 			=> $sub_gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $sub_gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_keluar',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];


			//memunculkan konsep awal
			$save_reciever = $this->model_inboxreciever->store($save_recievers);

				$save_recievers_koreksi = [
				// 'NId' 			=> $gir,
				// 'NKey' 			=> tb_key(),
				// 'GIR_Id' 		=> $girid_ir,
				// 'id_koreksi'	=> $gir,

				'NId' 			=> $sub_gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $sub_gir,
				'id_koreksi'	=> $sub_gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_keluar',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_reciever_koreksi = $this->model_inboxreciever_koreksi->store($save_recievers_koreksi);
			//akhir memunculkan konsep awal

			
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
		
	}
	//Fungsi Simpan Regis Surat Dinas Keluar

	public function postedit_notadinas($NId)
	{
		$gir = $NId;
		$tanggal = date('Y-m-d H:i:s');

		$naskah1 = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();

		if ($naskah1->Ket != 'outboxsprint') {
			$sifat =  $this->input->post('SifatId');
		} else {
			$sifat =  NULL;
		}

		if ($naskah1->Ket == 'outboxkeluar') {
			$alamat =  $this->input->post('Alamat');

			$role = $this->input->post('RoleId_To');
			$konten = $this->input->post('Konten');
			//penambahan
			$lampiran =$this->input->post('lampiran');
			//akhir penambahan

		} elseif ($naskah1->Ket == 'outboxedaran') {
			$alamat =  $this->input->post('Alamat');
			$role = $this->input->post('RoleId_To');
			$konten = $this->input->post('Konten');
		} elseif ($naskah1->Ket == 'outboxinstruksigub') {
			$alamat =  $this->input->post('Alamat');
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$role2 = (!empty($this->input->post('RoleId_To_2')) ?  json_encode(array_filter($this->input->post('RoleId_To_2'))) : '');
			$konten = (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : '');
		} elseif ($naskah1->Ket == 'outboxsuratizin') {
			$alamat =  $this->input->post('Alamat');
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$konten = (!empty($this->input->post('Konten')) ?  json_encode(array_filter($this->input->post('Konten'))) : '');

		} else {
			$alamat =  NULL;
			$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
			$konten = $this->input->post('Konten');
			$lampiran =(!empty($this->input->post('isi_lampiran')) ?  json_encode($this->input->post('isi_lampiran')) : '');
		}


		// } else {
		// 	$alamat =  '';
		// 	$role = (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : '');
		// 	$konten = $this->input->post('Konten');
		// }

	if($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;	
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}	

		if($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');		
		} else {
			$x = $this->input->post('Approve_People');
		}

	if($this->input->post('TtdText2') == 'none') {
	
		$b = ('');
		} elseif($this->input->post('TtdText2') == 'AL') {		
		$b = $this->input->post('tmpid_atas_nama');	
		} else {
		$b = $this->input->post('Approve_People3');
		}	


		if ($naskah1->Ket == 'outboxkeluar') {
			$ket =  'outboxkeluar';
		} elseif ($naskah1->Ket == 'outboxedaran') {
			$ket =  'outboxedaran';
		} elseif ($naskah1->Ket == 'outboxnotadinas') {
			$ket =  'outboxnotadinas';
		} elseif ($naskah1->Ket == 'outboxsprint') {
			$ket =  'outboxsprint';
		} elseif ($naskah1->Ket == 'outboxinstruksigub') {
			$ket =  'outboxinstruksigub';
		} elseif ($naskah1->Ket == 'outboxsupertugas') {
			$ket =  'outboxsupertugas';
		} elseif ($naskah1->Ket == 'outboxsuratizin') {
			$ket =  'outboxsuratizin';
		} else {
		}


		try {
			$CI = &get_instance();
			$this->load->helper('string');

			$data_konsep = [
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'SifatId'       	=> $sifat,
				'Alamat'       		=> $alamat,
				'ClId'       		=> $this->input->post('ClId'),
				'RoleId_To'       	=> $role,
				'RoleId_To_2'       => $role2,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $konten,
				'lampiran'       	=> $lampiran,
			
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'       		=> $this->input->post('Hal_pengantar'),
				'Nama_ttd_pengantar' 		=> $this->input->post('Nama_ttd_pengantar'),
				'RoleCode' 					=> $this->input->post('RoleCode'),

				'Approve_People'    => $x,
				'Approve_People3'   => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),				
				'Nama_ttd_konsep' 		=> $z,
				'Nama_ttd_atas_nama' 	=> $x,

			];

			//menyimpan record koreksi pada konsep naskah koreksi
			$type_naskah = $this->db->query("SELECT type_naskah FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->type_naskah;
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$data_konsep2 = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'id_koreksi'		=> $girid_ir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_keluar',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
			
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> $role,
				'RoleId_To_2'       => $role2,
				'Alamat'       		=> $alamat,
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $konten,
				'lampiran'       	=> $lampiran,
				'lampiran2'       	=> $this->input->post('lampiran2'),
				'lampiran3'       	=> $this->input->post('lampiran3'),
				'lampiran4'       	=> $this->input->post('lampiran4'),
				// 'Approve_People'       	=> $this->db->query("SELECT Approve_People FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People,
				// 'Approve_People3'    =>  $this->db->query("SELECT Approve_People3 FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People3,
				// 'TtdText'       	=> $this->db->query("SELECT TtdText FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText,
				// 'TtdText2'       	=>  $this->db->query("SELECT TtdText2 FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText2,
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'Hal_pengantar'     => $this->input->post('Hal_pengantar'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> $ket,
				'NFileDir'			=> 'naskah',
				// 'Nama_ttd_konsep'   => $this->db->query("SELECT Nama_ttd_konsep FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_konsep,

				// 'Nama_ttd_atas_nama'       	=> $this->db->query("SELECT Nama_ttd_atas_nama FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_atas_nama,
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'type_naskah'			=> $type_naskah,
				'RoleCode' 			=> $this->input->post('RoleCode'),
				'Nama_ttd_pengantar' 	=> $this->input->post('Nama_ttd_pengantar'),

				'Approve_People'    	=> $x,
				'Approve_People3'  		=> $b,
				'TtdText'       		=> $this->input->post('TtdText'),
				'TtdText2'       		=> $this->input->post('TtdText2'),				
				'Nama_ttd_konsep' 		=> $z,
				'Nama_ttd_atas_nama' 	=> $x,

			];

			$this->db->where('NId_Temp', $NId);
			$this->db->update('konsep_naskah', $data_konsep);

			if (!empty($_POST['test_title_name'])) {

				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(
						FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
						FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
					);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;

					$save_files = [
						'FileKey' 			=> tb_key(),
						'GIR_Id' 			=> $gir,
						'NId' 				=> $NId,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'PeopleRoleID' 		=> $this->session->userdata('roleid'),
						'RoleCode'       	=> $this->input->post('RoleCode'),
						'FileName_real' 	=> $file_name,
						'FileName_fake' 	=> $test_title_name_copy,
						'FileStatus' 		=> 'available',
						'EditedDate'        => $tanggal,
						'Keterangan' 		=> 'outboxkeluar',
					];
					$save_file = $this->model_inboxfile->store($save_files);
				}

				//penambahan koreksi
				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploadedkoreksi/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);
					}
				}
				//akhir penambahan koreksi
			}


			//penambahan koreksi2
			$save_konsep2 	= $this->model_konsepnaskah_koreksi->store($data_konsep2);
			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

			$save_recievers = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),

				'To_Id' 		=> $this->db->query("SELECT To_Id FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->To_Id,
				'RoleId_To'       	=> $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->RoleId_To,
				'id_koreksi'		=>	$girid_ir,
				'ReceiverAs' 	=> 'to_koreksi',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'read',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];
			$save_reciever2 = $this->model_inboxreciever_koreksi->store($save_recievers);
			//akhir penambahan koreksi2

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Tutup Edit naskah dinas

}