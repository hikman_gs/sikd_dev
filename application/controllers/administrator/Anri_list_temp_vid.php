<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_temp_vid extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
	}

	//Daftar Template Dokumen
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_vid_template/list_temp_vid', $this->data);
	}
	//Tutup Daftar Template Dokumen			

}