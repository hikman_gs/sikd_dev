<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_list_naskah_koreksi extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_list_naskah_koreksi');
		$this->load->model('model_list_naskah_ba');
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_log');
		$this->load->model('model_inbox');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_ttd');
		$this->load->model('model_list_koreksi_btl');
		$this->load->model('model_inboxreciever_koreksi');

	}

	//Naskah Teruskan Approve
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->data['title'] = 'Pencatatan Naskah Teruskan Untuk Disetujui';
		$this->tempanri('backend/standart/administrator/koreksi/list_btl', $this->data);
	}
	//Tutup Naskah Teruskan Approve

	//Ambil Data Seluruh Naskah Teruskan Approve
	public function get_data_naskah_bs()
	{	
		
		$limit = $_POST['length'];
		$no = $_POST['start'];
		$list = $this->model_list_naskah_koreksi->get_datatables($limit, $no);
		$data = array();

		foreach ($list as $field) {
			$no++;
			$row = array();
			
			$row[] = $no;

			$naskah = $this->db->query("SELECT NId_Temp, JenisId, nosurat, Hal, RoleId_To FROM konsep_naskah WHERE NId_Temp = '".$field->NId."'")->row();

			
			

			$row[] = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;	
	
			$row[] = date('d-m-Y',strtotime($field->ReceiveDate));
			$row[] = $naskah->Hal;

			$count_koreksi  = $this->db->query("SELECT Koreksi  FROM inbox_koreksi WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."'")->num_rows();
			if($count_koreksi > 0){
				$koreksi = $this->db->query("SELECT Koreksi FROM inbox_koreksi WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."'")->result();
				foreach ($koreksi as $key => $value) {					  
					$exp = explode('|', $value->Koreksi);
					if($value->Koreksi == '-'){
						$z1 = '';
					} else {
						$z1 = "<table border ='0' cellpadding='0' cellspacing='0'>";
						foreach ($exp as $k => $v) {
							$z1 .= "<tr>";
							$z1 .= "<td> - ".$this->db->query("SELECT KoreksiName FROM master_koreksi WHERE KoreksiId = '".$v."'")->row()->KoreksiName."</td>";
							$z1 .= "</tr>";
						}
						$z1 .= "</table>";	
					}				  
				}
			}

			$row[] = $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$field->RoleId_To."'")->row()->RoleName;

          	$query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$field->NId."' AND GIR_Id = '".$field->GIR_Id."' AND Keterangan != 'outbox'")->result();

          	$files = BASE_URL.'/FilesUploaded/naskah/';

			$z21 = "<table border ='0' cellpadding='0' cellspacing='0'>";
			foreach ($query as $key1 => $value1) {					  
				$z21 .= "<tr>";
			   	$z21 .= "<td><a href='".$files.$value1->FileName_fake."' target='_new' title='Lihat Lampiran' class='btn btn-danger btn-sm'><i class='fa fa-download'></i></a><br><br></td>";
			   	$z21 .= "</tr>";
			}
			$z21 .= "</table>";	


			$pesan = "<p class='text-warning'>segera perbaiki</p> ";
			$row[] = $pesan.$z1;
			
			$x  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$field->NId."' AND Keterangan = 'outbox'")->num_rows();

			if($x > 0) {

				$xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$field->NId."' AND Keterangan = 'outbox'")->row();

				$row[] = '<a target="_blank" href="'.site_url('administrator/anri_mail_tl/iframe_histori/'.$field->NId).'" title="Lihat Histori Naskah" class="btn btn-success btn-sm"><i class="fa fa-edit"></i></a> <a target="_blank" href="'.BASE_URL('FilesUploaded/naskah/'.$xyz->FileName_real).'" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a> &nbsp; Selesei';	

			} else {
				$cari_jenis_surat = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;	
				$jenis_naskah = $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $naskah->JenisId. "'")->row()->JenisName;

				if ($jenis_naskah == 'Surat Perintah') {
					$row[] = '<a href="' . site_url('administrator/surat_perintah/edit/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>  <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';
					
				}elseif ($jenis_naskah == 'Surat Dinas') {
					$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_naskahdinas/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>  <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';

				}elseif ($jenis_naskah == 'Nota Dinas') {
					$row[] = '<a href="' . site_url('administrator/surat_nota_dinas/edit/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>  <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';

				}elseif ($jenis_naskah == 'Surat Pernyataan Melaksanakan Tugas') {
					$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_superpelaksanaantugas/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>  <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';

				}elseif ($jenis_naskah == 'Surat Keterangan') {
					$row[] = '<a href="' . site_url('administrator/surat_keterangan/edit/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>  <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';

				}elseif ($jenis_naskah == 'Pengumuman') {
					$row[] = '<a href="' . site_url('administrator/surat_pengumuman/edit/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>  <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';

				}elseif ($jenis_naskah == 'Rekomendasi') {
					$row[] = '<a href="' . site_url('administrator/surat_rekomendasi/edit/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>  <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';

				}elseif ($jenis_naskah = 'Surat Izin') {
					$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_surat_izin/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a>  <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';

				} elseif ($jenis_naskah == 'Instruksi Gubernur') {
					$row[] = '<a href="' . site_url('administrator/anri_list_naskah_belum_approve/edit_suratinstruksi/' . $field->NId) . '" title="Ubah Data" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></a> <button type="button" onclick=ko_teruskan(' . $field->NId . ',' . $field->GIR_Id . ') title="Teruskan Untuk Ditandatangani" class="btn btn-warning btn-sm"><i class="fa fa-arrow-circle-right "></i></button> <a target="_blank" href="'.site_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$field->NId).'" title="Lihat Naskah" class="btn btn-danger btn-sm"><i class="fa fa-eye"></i></a> <a target="_blank" href="'.site_url('administrator/anri_mail_tl/view_naskah_histori_koreksi/view/'.$field->NId.'?dt=3').'" title="Lihat Histori Konsep" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-arrow-down" aria-hidden="true"></i></a>';

				}

			}

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->model_list_naskah_koreksi->count_all(),
			"recordsFiltered" => $this->model_list_naskah_koreksi->count_filtered(),
			"data" => $data,
		);
		
		echo json_encode($output);
	}
	//Tutup Ambil Data Seluruh Naskah Teruskan Approve

	//View PDF Naskah Dinas
	public function log_naskah_dinas_pdf($id)
	{
		$this->load->library('HtmlPdf');
      	ob_start();

      	$naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '".$id."' LIMIT 1")->row();

      	$this->data['NId'] 				= $id;
      	$this->data['Asal_naskah'] 		= $naskah->RoleId_From;
      	$this->data['Naskah'] 			= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$naskah->JenisId."'")->row()->JenisName;
      	$this->data['RoleId_To'] 		= $naskah->RoleId_To;
      	$this->data['RoleId_Cc'] 		= $naskah->RoleId_Cc;
      	$this->data['Konten'] 			= $naskah->Konten;
      	$this->data['Alamat'] 			= $naskah->Alamat;

		if ($naskah->Ket != 'outboxsprint') {
			$this->data['SifatId'] 			= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$naskah->SifatId."'")->row()->SifatName;
	      	$this->data['Jumlah'] 			= $naskah->Jumlah;
	      	$this->data['MeasureUnitId'] 	= $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$naskah->MeasureUnitId."'")->row()->MeasureUnitName;			
      		$this->data['Hal'] 				= $naskah->Hal;	      	
		}

		if ($naskah->Ket == 'outboxkeluar') {
			$this->data['Alamat'] 			= $naskah->Alamat;
		}
		$this->data['Alamat'] 			= $naskah->Alamat;
      	$this->data['TglReg'] 			= $naskah->TglReg;
      	$this->data['Approve_People'] 	= $naskah->Approve_People;
      	$this->data['TtdText'] 			= $naskah->TtdText;
      	$this->data['Nama_ttd_konsep'] 	= $naskah->Nama_ttd_konsep;
      	$this->data['ClCode'] 			= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$naskah->ClId."'")->row()->ClCode;
		
	    if ($naskah->Ket == 'outboxnotadinas') {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_nota_dinas_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxsprint'){ 
			$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_tugas_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxundangan'){ 
			$this->load->view('backend/standart/administrator/pdf/view_konsep_undangan_tindaklanjut_pdf',$this->data);
		}else if($naskah->Ket == 'outboxkeluar'){ 
        	$this->load->view('backend/standart/administrator/pdf/view_konsep_surat_dinas_tindaklanjut_pdf',$this->data);
		}else {
			$this->load->view('backend/standart/administrator/pdf/view_konsep_naskah_dinas_tindaklanjut_pdf',$this->data);
		}

	    $content = ob_get_contents();
	    ob_end_clean();
        
           $config = array(
		            'orientation' 	=> 'p',
		            'format' 		=> 'f4',
		            'marges' 		=> array(30, 30, 20, 30),
		        );


        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('naskah_dinas_tindaklanjut-'.$id.'.pdf', 'H');
	}
	//Akhir View PDF Naskah Dinas
		public function k_teruskan()
	{

		$nid = $this->input->post('NId');
		$girid = $this->input->post('GIR_Id');

		$gir = $this->session->userdata('peopleid') . date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');

		if ($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} elseif($this->input->post('TtdText') == 'SEKDIS') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_sekdis')."' AND GroupId= '4'")->row()->ApprovelName;	
		} elseif ($this->input->post('TtdText') == 'UK') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_lainya') . "'")->row()->ApprovelName;
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$y = $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$y = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_sekdis') . "'")->row()->ApprovelName;
		} else {
			$y = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		if ($this->input->post('TtdText') == 'none') {
			$v =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'SEKDIS') {
			$v = $this->input->post('tmpid_sekdis');
		} else {
			$v = $this->input->post('Approve_People');
		}

		if ($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');
		} else {
			$x = $this->input->post('Approve_People');
		}

		$naskah = $this->db->query("SELECT Ket FROM konsep_naskah WHERE NId_Temp = '" . $nid . "' ORDER BY TglNaskah DESC LIMIT 1")->row();

		if ($naskah->Ket == 'outboxnotadinas') {
			$xxz = 'to_draft_notadinas';
		} else if ($naskah->Ket == 'outboxsprint') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxundangan') {
			$xxz = 'to_draft_undangan';
		} else if ($naskah->Ket == 'outboxkeluar') {
			$xxz = 'to_draft_keluar';
		} else if ($naskah->Ket == 'outboxsuratizin') {
			$xxz = 'to_surat_izin_keluar';
		} else if ($naskah->Ket == 'outboxsupertugas') {
			$xxz = 'to_draft_super_tugas';
		} else if ($naskah->Ket == 'outboxinstruksigub') {
			$xxz = 'to_draft_intruksi_gub';
		} else if ($naskah->Ket == 'outboxedaran') {
			$xxz = 'to_draft_edaran';
		} else if ($naskah->Ket == 'outboxsprintgub') {
			$xxz = 'to_draft_sprint';
		} else if ($naskah->Ket == 'outboxsket') {
			$xxz = 'to_draft_sket';
		} else if ($naskah->Ket == 'outboxpengumuman') {
			$xxz = 'to_draft_pengumuman';
		} else if ($naskah->Ket == 'outboxrekomendasi') {
			$xxz = 'to_draft_rekomendasi';
		} else {
			$xxz = 'to_draft_nadin';
		}

		try {

			$save_recievers = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> $xxz,
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

				$save_recievers_jejak = [
				'NId' 			=> $nid,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $gir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId', $x),
				'ReceiverAs' 	=> 'meneruskan',
				'Msg' 			=> $this->input->post('pesan'),
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
			];

				$save_reciever_jejak = $this->model_inboxreciever_koreksi->store($save_recievers_jejak);

			//Update konsep naskah
			$save_people = $this->db->where('NId_Temp', $nid)->where('GIR_Id', $girid)->update('konsep_naskah', ['RoleId_From' => $this->session->userdata('roleid'), 'Approve_People' => $x, 'TtdText' => $this->input->post('TtdText'), 'Nama_ttd_konsep' => $z]);

			//Update Status
			$this->db->where('NId', $nid);
			$this->db->where('RoleId_To', $this->session->userdata('roleid'));
			$this->db->update('inbox_receiver', ['Status' => 1, 'StatusReceive' => 'read']);
			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
		} catch (\Exception $e) {
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
	}
	//Fungsi Teruskan Draft Naskah Dinas
}