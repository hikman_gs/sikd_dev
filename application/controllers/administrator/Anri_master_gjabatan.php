<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_master_gjabatan extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_master_gjabatan');
	}

	// List grup jabatan
	public function index()
	{
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		
		$this->tempanri('backend/standart/administrator/master_gjabatan/master_gjabatan_list', $this->data);
	}
	// Tutup list grup jabatan

	// Tambah data grup jabatan
	public function add()
	{
		$this->tempanri('backend/standart/administrator/master_gjabatan/master_gjabatan_add', $this->data);
	}
	// Tutup tambah data grup jabatan

	// Proses simpan data grup jabatan
	public function add_save()
	{

		$this->form_validation->set_rules('gjabatanName', 'GjabatanName', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('gtujuanNaskah', 'GtujuanNaskah', 'trim|required|max_length[50]');
		

		if ($this->form_validation->run()) {
			$table = 'master_gjabatan';

			$query = $this->db->query("SELECT (max(convert(substr(gjabatanId, 12), UNSIGNED)) + 1) as id FROM master_gjabatan")->row()->id;

		    
			if (!empty($query[0])){
			   	$id = $query;
			}
			else
			{
				$id = 1;
			}
			
      		$save_data = [
				'gjabatanId' => tb_key().'.'.$id,
				'gjabatanName' => $this->input->post('gjabatanName'),
				'gtujuanNaskah' => $this->input->post('gtujuanNaskah'),
			];

			
			$save_master_gjabatan = $this->db->insert($table,$save_data);
			set_message('Data Berhasil Disimpan','success');
			redirect(BASE_URL('administrator/anri_master_gjabatan'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_gjabatan'));			
		}

	}
	// Tutup proses simpan data grup jabatan

	// Edit data grup jabatan
	public function update($id)
	{
		$this->data['master_gjabatan'] = $this->model_master_gjabatan->find($id);
		$this->tempanri('backend/standart/administrator/master_gjabatan/master_gjabatan_update', $this->data);
	}
	// Tutup edit data grup jabatan

	// Proses update data grup jabatan
	public function update_save($id)
	{
		
		$this->form_validation->set_rules('gjabatanName', 'GjabatanName', 'trim|required|max_length[50]');
		$this->form_validation->set_rules('gtujuanNaskah', 'GtujuanNaskah', 'trim|required|max_length[50]');
		
		if ($this->form_validation->run()) {
		
			$save_data = [
				'gjabatanName' => $this->input->post('gjabatanName'),
				'gtujuanNaskah' => $this->input->post('gtujuanNaskah'),
			];

			
			$save_master_gjabatan = $this->model_master_gjabatan->change($id, $save_data);
			set_message('Data Berhasil Diubah','success');
			redirect(BASE_URL('administrator/anri_master_gjabatan'));
		} else {
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/anri_master_gjabatan'));			
		}

	}
	// Tutup proses update data grup jabatan

	// Hapus data grup jabatan
	public function delete($id = null)
	{
		$this->load->helper('file');

		$arr_id = $this->input->get('id');
		$remove = false;

		if (!empty($id)) {
			$remove = $this->remove($id);
		} elseif (count($arr_id) >0) {
			foreach ($arr_id as $id) {
				$remove = $this->remove($id);
			}
		}

		if ($remove) {
            set_message('Data Berhasil Dihapus','success');
        } else {
            set_message('Gagal Menghapus Data','error');
        }

		redirect(BASE_URL('administrator/anri_master_gjabatan'));				
	}
	// Tutup hapus data grup jabatan

	// Proses hapus data grup jabatan
	private function remove($id)
	{
		$master_gjabatan = $this->db->where('gjabatanId',$id)->delete('master_gjabatan');
		return $master_gjabatan;
	}
	// Tutup proses hapus data grup jabatan
}