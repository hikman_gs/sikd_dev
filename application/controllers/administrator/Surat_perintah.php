<?php
defined('BASEPATH') or exit('No direct script access allowed');

// use NcJoes\OfficeConverter\OfficeConverter;
class Surat_perintah extends Admin
{
	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('status') != "anri_ok_dong") {
			redirect(base_url("administrator/masuk"));
		}
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');
		$this->load->model('model_inboxreciever');
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_konsepnaskah_koreksi');
	}

	// Fungsi List Pengumuman
	public function index()
	{
		//cek akses ambil dari helper
		// check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		$this->template->title('Naskah Template');
		$this->tempanri('backend/standart/administrator/master_list_naskah/menu_list_naskah', $this->data);
	}
	// Akhir Fungsi List Pengumuman
	// 
	public function detail_pdf($value = '')
	{
		$tanggal = date('Y-m-d H:i:s');
		$this->load->library('Pdf');

		$data['RoleId_To'] 	= $this->input->post('RoleId_To');
		$data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');
		$data['Hal'] 			= $this->input->post('Hal');
		$data['Konten'] 		= $this->input->post('Konten');
		$data['TglReg'] 		= date('Y-m-d H:i:s');
		$data['RoleCode'] 	= $this->input->post('rolecode');

		if ($this->input->post('TtdText') == 'none') {
			$data['Approve_People']	= $this->session->userdata('peopleid');
			$data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif ($this->input->post('TtdText') == 'AL') {
			$data['Approve_People'] 	= $this->input->post('tmpid_atasan');
			$data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
		} else {
			$data['Approve_People'] 	= $this->input->post('Approve_People');
			$data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
		}

		//penambahan atas nama
		if ($this->input->post('TtdText2') == 'AL') {
			$data['Approve_People3'] 	= $this->input->post('tmpid_atas_nama');
		} else {
			$data['Approve_People3'] 	= $this->input->post('Approve_People3');
		}

		//
		$data['ClCode'] 	= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '" . $this->input->post('ClId') . "'")->row()->ClCode;
		$data['TtdText']    = $this->input->post('TtdText');
		//atas nama
		$data['TtdText2']   = $this->input->post('TtdText2');

		$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

		$this->db->select("*");
		$this->db->where_in('PeopleId', $data['RoleId_To']);
		$this->db->order_by('GroupId', 'ASC');
		$this->db->order_by('Golongan', 'DESC');
		$this->db->order_by('Eselon ', 'ASC');
		$data_perintah = $this->db->get('v_login')->result();

		$tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat;

		//tcpdf

		$age = $this->session->userdata('roleid');
		if ($data['TtdText'] == 'PLT') {
			$tte = 'Plt. ' . get_data_people('RoleName', $data['Approve_People']) . ',';
		} elseif ($data['TtdText'] == 'PLH') {
			$tte = 'Plh. ' . get_data_people('RoleName', $data['Approve_People']) . ',';
		} elseif ($data['TtdText2'] == 'Atas_Nama') {
			$tte = 'a.n. ' . get_data_people('RoleName', $data['Approve_People3']) . ',' . '<br>' . get_data_people('RoleName', $data['Approve_People']) . ',';
		} else {
			$tte = get_data_people('RoleName', $data['Approve_People']) . ',';
		}

		$table = '<table style="font-size:12px; line-height:15px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
			<br><br>
				<td width="240px" style="text-align:center;">Ditetapkan di ..............</td>
			</tr>
			<tr nobr="true">
				<td width="240px" style="text-align:center;">Pada tanggal .................. <br></td>
			</tr>
			<tr nobr="true">
				<td width="240px" style="text-align:center; line-height:15px;">' . $tte . '</td>
			</tr>
		</table>
		<br>
		<table style="border: 1px solid black;font-size:10px;" cellpadding="1px" nobr="true">
			<tr nobr="true">
				<td rowspan="7" width="60px" style="valign:bottom;">
				<br><br>
				<img src="' . base_url('/uploads/kosong.jpg') . '" widht="55" height="60"></td>
				<td width="180px">Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr nobr="true">
				<td width="180px" style="text-align: left; font-size:9;">' . $tte . '</td>
			</tr>
			<tr nobr="true">
				<td><br></td>
			</tr>
			<tr nobr="true">
				<td style="font-size:10px;">' . $data['Nama_ttd_konsep'] . '
				</td>
			</tr>
			<tr nobr="true">
				<td>' . $tujuan . '
				</td>
			</tr>
		</table>';

		$pdf = new Pdf('P', 'mm', 'F4', true, 'UTF-8', false);
		$pdf->SetPrintHeader(false);
		$pdf->setHeaderData('', 0, '', '', array(0, 0, 0), array(255, 255, 255));
		$pdf->SetMargins(30, 30, 30);
		$pdf->SetAutoPageBreak(TRUE, 25);
		$pdf->AddPage('P', 'F4');
		$pdf->Image(base_url('FilesUploaded/kop/' . $sql_header), 30, 5, 160, 0, 'PNG', '', 'N');
		$pdf->Ln(7); // dari 10 ke 5
		$pdf->Cell(0, 10, 'SURAT PERINTAH', 0, 0, 'C');
		$pdf->Ln(5);
		$pdf->Cell(0, 10, 'NOMOR : .........../' . $data['ClCode'] . '/' . $data['RoleCode'], 0, 0, 'C');
		$pdf->Ln(12); // dari 15 ke 12
		$pdf->Cell(25, 5, 'DASAR', 0, 0, 'L'); // 30 ke 25
		$pdf->Cell(3, 5, ':', 0, 0, 'L');
		$pdf->writeHTMLCell(130, 3, '', '', $data['Hal'], '', 1, 0, true, 'J', true); //125 ke 130
		$pdf->Ln(3); // dari 5 ke 3
		$pdf->Cell(0, 3, 'MEMERINTAHKAN:', 0, 1, 'C');
		$pdf->Ln(3); // dari 5 ke 3
		$pdf->Cell(25, 5, 'Kepada', 0, 0, 'L'); // 30 ke 25
		$pdf->Cell(5, 5, ':', 0, 0, 'L');
		$i = 0;
		foreach ($data_perintah as $row) {
			$i++;
			$pdf->SetX(60);
			$pdf->Cell(5, 5, $i . '. ', 0, 0, 'L');
			$pdf->SetX(65);
			$pdf->Cell(40, 5, 'Nama', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$pdf->MultiCell(76, 5, $row->PeopleName, 0, 'L');
			$pdf->SetX(65);
			$pdf->Cell(40, 5, 'Pangkat/Golongan', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$pdf->Cell(76, 5, $row->Pangkat . ' / ' . $row->Golongan, 0, 1, 'L');
			$pdf->SetX(65);
			$pdf->Cell(40, 5, 'NIP', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$pdf->Cell(76, 5, $row->NIP, 0, 1, 'L');
			$pdf->SetX(65);
			$pdf->Cell(40, 5, 'Jabatan', 0, 0, 'L');
			$pdf->Cell(3, 5, ':', 0, 0, 'L');
			$uc  = ucwords(strtolower($row->PeoplePosition));
			$str = str_replace('Dan', 'dan', $uc);
			$str = str_replace('Uptd', 'UPTD', $str);
			// $pdf->MultiCell(85,5,$str,0,'J');
			$pdf->MultiCell(76, 5, $str . "\n", 0, 'J', 0, 2, '', '', true);
		}
		$pdf->Ln(5); // dari 10 ke 5
		$pdf->Cell(25, 5, 'Untuk', 0, 0, 'L'); // 30 ke 25
		$pdf->Cell(3, 5, ':', 0, 0, 'L');
		$pdf->writeHTMLCell(130, 0, '', '', $data['Konten'], '', 1, 0, true, 'J', true); // 125 ke 130
		// $pdf->MultiCell(125,5,$pdf->WriteHTML($this->data['Konten']),0,'J');		
		$pdf->SetX(100);
		$pdf->writeHTMLCell(90, 0, '', '', $table, '', 1, 0, false, 'L', false);
		$pdf->Output('surat_perintah_view.pdf', 'I');
	}

	public function create()
	{
		// check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		$data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		// data klasifikasi
		$data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
		// data yang ditugaskan
		$data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();
		// data pemeriksa
		$data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
		// data atasan
		$data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		// data tmpid_atasan
		$data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		// data a.n
		$data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
		// data nama a.n
		$data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		// data nama u.b
		$data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		$data['title'] = 'Pencatatan Surat Perintah';
		$this->tempanri('backend/standart/administrator/master_list_naskah/surat_perintah/create', $data);
	}


	// Fungsi Simpan Pengumuman
	public function store()
	{
		//cek akses ambil dari helper
		// check_access($this->session->userdata('groupid'), $this->uri->segment(2));
		$this->form_validation->set_rules('ClId', 'Kode Klasifikasi', 'trim|required');
		$this->form_validation->set_rules('rolecode', 'Unit Pengolah', 'trim|required');
		$this->form_validation->set_rules('Hal', 'Dasar', 'trim|required');
		$this->form_validation->set_rules('RoleId_To[]', 'Yang Ditugaskan', 'trim|required');
		$this->form_validation->set_rules('Konten', 'Konsep Surat Perintah', 'trim|required');
		$this->form_validation->set_rules('TtdText', 'Pemeriksa', 'trim|required');
		$this->form_validation->set_rules('TtdText2', 'a.n', 'trim|required');
		$this->form_validation->set_rules('RoleId_To[]', 'Yang Ditugaskan', 'trim|required');

		if ($this->form_validation->run() == FALSE) {
			$data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
			// data klasifikasi
			$data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
			// data yang ditugaskan
			$data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();
			// data pemeriksa
			$data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
			// data atasan
			$data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
			// data tmpid_atasan
			$data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
			// data a.n
			$data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
			// data nama a.n
			$data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
			// data nama u.b
			$data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
			$data['title'] = 'Pencatatan Surat Perintah';
			$this->tempanri('backend/standart/administrator/master_list_naskah/surat_perintah/create', $data);
		} else {


			// $gir = $this->session->userdata('peopleid') . date('dmyhis');

		$gir_awal= $this->session->userdata('peopleid').date('dmyhis');	
		$jumlah_karakter    =strlen($gir_awal);		
		if ($jumlah_karakter == 17){

			$sub_gir = substr($gir_awal,0,16);
			$cek_NId_Temp="SELECT NId_Temp FROM konsep_naskah WHERE NId_Temp = '$_POST[$sub_gir]'";
			if ($cek_NId_Temp > 0) {

				$sub_gir = substr($gir_awal,0,16)+1;

			}else{

				$sub_gir = substr($gir_awal,0,16);

			}

		}else{

			$sub_gir = $this->session->userdata('peopleid').date('dmyhis');

		}

			$tanggal = date('Y-m-d H:i:s');
			$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

			if ($this->input->post('TtdText') == 'none') {
				$z = $this->session->userdata('approvelname');
				$x =  $this->session->userdata('peopleid');
			} elseif ($this->input->post('TtdText') == 'AL') {
				$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
				$x = $this->input->post('tmpid_atasan');
			} else {
				$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
				$x = $this->input->post('Approve_People');
			}

			if ($this->input->post('TtdText2') == 'none') {
				$b = ('');
				$b = ('');
			} elseif ($this->input->post('TtdText2') == 'AL') {
				$b = $this->input->post('tmpid_atas_nama');
			} else {
				$b = $this->input->post('Approve_People3');
			}

			try {
				$data_konsep = [
					// 'NId_Temp'   		=> $gir,
					// 'GIR_Id'       		=> $gir,
					'NId_Temp'   		=> $sub_gir,
					'GIR_Id'       		=> $sub_gir,
					'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Perintah'")->row()->JenisId,
					'ReceiverAs'       	=> 'to_sprint',
					'ClId'       		=> $this->input->post('ClId'),
					'Number'       		=> '0',
					'RoleCode'       	=> $this->input->post('rolecode'),
					'RoleId_From'      	=> $this->session->userdata('roleid'),
					'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
					'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
					'Konten'       		=> $this->input->post('Konten'),
					'Approve_People'    => $x,
					'Approve_People3'    => $b,
					'TtdText'       	=> $this->input->post('TtdText'),
					'TtdText2'       	=> $this->input->post('TtdText2'),
					'Konsep'       		=> '1',
					'TglReg'       		=> $tanggal,
					'Hal'       		=> $this->input->post('Hal'),
					'TglNaskah'       	=> $tanggal,
					'CreateBy'       	=> $this->session->userdata('peopleid'),
					'Ket'       		=> 'outboxsprint',
					'NFileDir'			=> 'naskah',
					'Nama_ttd_konsep' 	=> $z,
					'Nama_ttd_atas_nama' 	=> $x,
					'lokasi'			=> $lok,
					'RoleId_From_Asal' 	=> $this->session->userdata('roleid')
				];

				//menyimpan konsep awal 
				$data_konsep_awal = [
					'NId_Temp'   		=> $sub_gir,
					'GIR_Id'       		=> $sub_gir,
					'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Perintah'")->row()->JenisId,
					'ReceiverAs'       	=> 'to_sprint',
					'ClId'       		=> $this->input->post('ClId'),
					'Number'       		=> '0',
					'RoleCode'       	=> $this->input->post('rolecode'),
					'RoleId_From'      	=> $this->session->userdata('roleid'),
					'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
					'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
					'Konten'       		=> $this->input->post('Konten'),
					'Approve_People'    => $x,
					'Approve_People3'    => $b,
					'TtdText'       	=> $this->input->post('TtdText'),
					'TtdText2'       	=> $this->input->post('TtdText2'),
					'Konsep'       		=> '1',
					'TglReg'       		=> $tanggal,
					'Hal'       		=> $this->input->post('Hal'),
					'TglNaskah'       	=> $tanggal,
					'CreateBy'       	=> $this->session->userdata('peopleid'),
					'Ket'       		=> 'outboxsprint',
					'NFileDir'			=> 'naskah',
					'Nama_ttd_konsep' 	=> $z,
					'Nama_ttd_atas_nama' 	=> $x,
					'lokasi'			=> $lok,
					'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
					'id_koreksi'    	=> $sub_gir,
				];
				//akhir menyimpan konsep awal 

				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
						);
						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;
						$save_files = [
							'FileKey' 			=> tb_key(),
							// 'GIR_Id' 			=> $gir,
							'GIR_Id' 			=> $sub_gir,
							'NId' 				=> $data_konsep['NId_Temp'],
							'PeopleID' 			=> $this->session->userdata('peopleid'),
							'PeopleRoleID' 		=> $this->session->userdata('roleid'),
							'FileName_real' 	=> $file_name,
							'FileName_fake' 	=> $test_title_name_copy,
							'FileStatus' 		=> 'available',
							'EditedDate'        => $tanggal,
							'Keterangan' 		=> '',
						];

						$save_file = $this->model_inboxfile->store($save_files);
					}
				}

				$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

				$data_konsep['id_koreksi'] = $sub_gir;

				//menyimpan konsep awal
				$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep);
				// akhir menyimpan konsep awal

				//Simpan tujuan draft
				$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

				$save_recievers = [
					// 'NId' 			=> $gir,
					// 'NKey' 			=> tb_key(),
					// 'GIR_Id' 		=> $girid_ir,
					'NId' 			=> $sub_gir,
					'NKey' 			=> tb_key(),
					'GIR_Id' 		=> $sub_gir,
					'From_Id' 		=> $this->session->userdata('peopleid'),
					'RoleId_From' 	=> $this->session->userdata('roleid'),
					'To_Id' 		=> $x,
					'RoleId_To' 	=> get_data_people('RoleId', $x),
					'ReceiverAs' 	=> 'to_draft_sprint',
					'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
					'StatusReceive' => 'unread',
					'ReceiveDate' 	=> $tanggal,
					'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
				];
				//memunculkan konsep awal
				$save_reciever = $this->model_inboxreciever->store($save_recievers);
				$save_recievers['id_koreksi'] = $sub_gir;

				//memunculkan konsep awal
				$save_reciever_awal = $this->model_inboxreciever_koreksi->store($save_recievers);
				//akhir memunculkan konsep awal

				set_message('Data Berhasil Disimpan', 'success');
				redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
			} catch (\Exception $e) {
				set_message('Gagal Menyimpan Data', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}
	}
	// Akhir Fungsi Simpan Klasifikasi
	// 
	// 
	public function edit($NId = NULL)
	{
		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		// data klasifikasi
		$data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
		// data yang ditugaskan
		$data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();


		// data pemeriksa
		$data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
		// data atasan
		$data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		// data tmpid_atasan
		$data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		// data a.n
		$data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
		// data nama a.n
		$data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
		// data nama u.b
		$data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
		$data['title'] = 'Ubah Data Pencatatan Surat Perintah';
		$data['data'] = $this->model_konsepnaskah->find($NId);

		$this->tempanri('backend/standart/administrator/master_list_naskah/surat_perintah/edit', $data);
	}


	public function update($NId = NULL)
	{


		//cek akses ambil dari helper
		$this->form_validation->set_rules('ClId', 'Kode Klasifikasi', 'trim|required');
		$this->form_validation->set_rules('rolecode', 'Unit Pengolah', 'trim|required');
		$this->form_validation->set_rules('Hal', 'Dasar', 'trim|required');
		$this->form_validation->set_rules('Konten', 'Konsep Surat Perintah', 'trim|required');
		$this->form_validation->set_rules('RoleId_To[]', 'Yang Ditugaskan', 'trim|required');
		$this->form_validation->set_rules('TtdText', 'Pemeriksa', 'trim|required');
		$this->form_validation->set_rules('TtdText2', 'a.n', 'trim|required');

		// var_dump($this->input->post());
		// die();

		if ($this->form_validation->run() == FALSE) {
			$data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
			// data klasifikasi
			$data['ClId'] = $this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result();
			// data yang ditugaskan
			$data['RoleId_To'] = $this->db->query("SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '" . $this->session->userdata('rolecode') . "' ORDER BY GroupId ASC")->result();
			// data pemeriksa
			$data['TtdText'] = $this->db->query('SELECT * FROM master_ttd')->result();
			// data atasan
			$data['AtasanId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
			// data tmpid_atasan
			$data['Approve_People'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
			// data a.n
			$data['TtdText2'] = $this->db->query('SELECT * FROM master_atas_nama')->result();
			// data nama a.n
			$data['NamaId'] = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
			// data nama u.b
			$data['Approve_People3'] = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
			$data['title'] = 'Pencatatan Surat Perintah';
			$data['data'] = $this->model_konsepnaskah->find($NId);
			$this->tempanri('backend/standart/administrator/master_list_naskah/surat_perintah/edit', $data);
		} else {

			$gir = $NId;
			$tanggal = date('Y-m-d H:i:s');
			$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Lokasi;

			// echo $gir;
			// die();

			if ($this->input->post('TtdText') == 'none') {
				$z = $this->session->userdata('approvelname');
				$x =  $this->session->userdata('peopleid');
			} elseif ($this->input->post('TtdText') == 'AL') {
				$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('tmpid_atasan') . "'")->row()->ApprovelName;
				$x = $this->input->post('tmpid_atasan');
			} else {
				$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '" . $this->input->post('Approve_People') . "'")->row()->ApprovelName;
				$x = $this->input->post('Approve_People');
			}

			if ($this->input->post('TtdText2') == 'none') {
				$b = ('');
				$b = ('');
			} elseif ($this->input->post('TtdText2') == 'AL') {
				$b = $this->input->post('tmpid_atas_nama');
			} else {
				$b = $this->input->post('Approve_People3');
			}

			try {

				//DATA KONSEP NASKAH
				$data_konsep = [
					'ClId'       		=> $this->input->post('ClId'),
					'RoleCode'       	=> $this->input->post('rolecode'),
					'RoleId_From'      	=> $this->session->userdata('roleid'),
					'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
					'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
					'Hal'       		=> $this->input->post('Hal'),
					'Konten'       		=> $this->input->post('Konten'),
					'Approve_People'    => $x,
					'Approve_People3'    => $b,
					'TtdText'       	=> $this->input->post('TtdText'),
					'TtdText2'       	=> $this->input->post('TtdText2'),
					'Konsep'       		=> '1',
				];



				if (!empty($_POST['test_title_name'])) {

					foreach ($_POST['test_title_name'] as $idx => $file_name) {
						$test_title_name_copy = date('Ymd') . '-' . $file_name;

						//untuk merename nama file 
						rename(
							FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
							FCPATH . 'FilesUploaded/naskah/' . $test_title_name_copy
						);

						//untuk menghapus folder upload sementara setelah simpan data dieksekusi
						rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);

						$listed_image[] = $test_title_name_copy;

						$save_files = [
							'FileKey' 			=> tb_key(),
							'GIR_Id' 			=> $gir,
							'NId' 				=> $NId,
							'PeopleID' 			=> $this->session->userdata('peopleid'),
							'PeopleRoleID' 		=> $this->session->userdata('roleid'),
							'RoleCode'       	=> $this->input->post('rolecode'),
							'FileName_real' 	=> $file_name,
							'FileName_fake' 	=> $test_title_name_copy,
							'FileStatus' 		=> 'available',
							'EditedDate'        => $tanggal,
							'Keterangan' 		=> 'outboxkeluar',
						];
						$save_file = $this->model_inboxfile->store($save_files);
					}

					//penambahan koreksi
					if (!empty($_POST['test_title_name'])) {
						foreach ($_POST['test_title_name'] as $idx => $file_name) {
							$test_title_name_copy = date('Ymd') . '-' . $file_name;
							//untuk merename nama file 
							rename(
								FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx] . '/' . $file_name,
								FCPATH . 'FilesUploadedkoreksi/naskah/' . $test_title_name_copy
							);
							//untuk menghapus folder upload sementara setelah simpan data dieksekusi
							rmdir(FCPATH . 'uploads/tmp/' . $_POST['test_title_uuid'][$idx]);
						}
					}
					//akhir penambahan koreksi
				}

				//UPDATE KONSEP NASKAH
				$this->db->where('NId_Temp', $NId);
				$this->db->update('konsep_naskah', $data_konsep);
				// var_dump($update_data);
				// die();
				// die();
				$type_naskah = $this->db->query("SELECT type_naskah FROM konsep_naskah_koreksi WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->type_naskah;

				$data_konsep2 = [
					'NId_Temp'   		=> $gir,
					'GIR_Id'       		=> $gir,
					'id_koreksi'		=> $gir,
					'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Surat Perintah'")->row()->JenisId,
					'ReceiverAs'       	=> 'to_keluar',
					'ClId'       		=> $this->input->post('ClId'),
					'Number'       		=> '0',
					'RoleCode'       	=> $this->input->post('rolecode'),
					'RoleId_From'      	=> $this->session->userdata('roleid'),
					'RoleId_To'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_To')) : ''),
					'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',', $this->input->post('RoleId_Cc')) : ''),
					'Konten'       		=> $this->input->post('Konten'),
					'Approve_People'    => $this->db->query("SELECT Approve_People FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Approve_People,
					'Approve_People3'    => $b,
					'TtdText'       	=> $this->db->query("SELECT TtdText FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->TtdText,
					'TtdText2'       	=> $this->input->post('TtdText2'),
					'Konsep'       		=> '1',
					'TglReg'       		=> $tanggal,
					'Hal'       		=> $this->input->post('Hal'),
					'TglNaskah'       	=> $tanggal,
					'CreateBy'       	=> $this->session->userdata('peopleid'),
					'Ket'       		=> $ket,
					'NFileDir'			=> 'naskah',
					'Nama_ttd_konsep'   => $this->db->query("SELECT Nama_ttd_konsep FROM konsep_naskah WHERE NId_Temp = '" . $gir . "' LIMIT 1")->row()->Nama_ttd_konsep,

					'Nama_ttd_atas_nama' => $a,
					'lokasi'			=> $lok,
					'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
					'type_naskah'			=> $type_naskah,
				];

				//menyimpan konsep awal
				$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep2);
				// akhir menyimpan konsep awal

				//Simpan tujuan draft
				$girid_ir = $this->session->userdata('peopleid') . date('dmyhis');

				$save_recievers = [
					'NId' 			=> $gir,
					'NKey' 			=> tb_key(),
					'GIR_Id' 		=> $girid_ir,
					'From_Id' 		=> $this->session->userdata('peopleid'),
					'RoleId_From' 	=> $this->session->userdata('roleid'),

					'To_Id' 		=> $this->db->query("SELECT To_Id FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->To_Id,
					'RoleId_To'       	=> $this->db->query("SELECT RoleId_To FROM inbox_receiver WHERE NId = '" . $NId . "' LIMIT 1")->row()->RoleId_To,
					'id_koreksi'		=>	$girid_ir,
					'ReceiverAs' 	=> 'to_koreksi',
					'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
					'StatusReceive' => 'read',
					'ReceiveDate' 	=> $tanggal,
					'To_Id_Desc' 	=> get_data_people('RoleDesc', $x),
				];
				$save_reciever2 = $this->model_inboxreciever_koreksi->store($save_recievers);

				set_message('Data Berhasil Diubah', 'success');
				redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));
			} catch (\Exception $e) {
				set_message('Gagal Menyimpan Data', 'error');
				$this->load->library('user_agent');
				redirect($this->agent->referrer());
			}
		}
	}

	// Buka hapus klasifikasi
	public function delete($id = null)
	{

		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		// check exist id
		$exist = $this->db->query("SELECT * FROM master_notice WHERE notice_id = " . $id)->num_rows();
		// exist
		if ($exist > 0) {
			$delete = $this->db->delete('master_notice', array('notice_id' => $id));

			if ($delete) {
				set_message('Data Berhasil Dihapus', 'success');
			} else {
				set_message('Data Gagal Dihapus', 'error');
			}
		} else {

			set_message('id not found', 'error');
		}

		redirect(BASE_URL('administrator/notice'));
	}
	// Tutup hapus klasifikasi
	// 
	// 
	function get_ajax()
	{

		//cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$list = $this->Model_notice->get_datatables();
		$data = array();
		$no = @$_POST['start'];
		foreach ($list as $notice) {
			$no++;
			$row = array();
			$row[] = $no . ".";
			$row[] = $notice->notice_name;
			switch ($notice->priority) {
				case 1:
					$row[] = '<span class="badge bg-green">New Feature</span>';
					break;
				case 2:
					$row[] = '<span class="badge bg-yellow">Maintenance</span>';
					break;
				case 3:
					$row[] = '<span class="badge bg-blue">Update</span>';
					break;
				default:
					$row[] = 'none';
			}
			$row[] = $notice->notice_description;
			$row[] = $notice->tgl_awal;
			$row[] = $notice->tgl_akhir;

			// add html for action
			$row[] = '<a href="' . BASE_URL('administrator/notice/edit/' . $notice->notice_id) . '" title="Hapus Data" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;' . '<a href="' . BASE_URL('administrator/notice/delete/' . $notice->notice_id) . '" title="Hapus Data" class="btn btn-sm btn-danger" onclick="return confirm(\'Anda yakin ingin menghapus data ini ?\')"><i class="fa fa-trash-o"></i></a>';

			$data[] = $row;
		}
		$output = array(
			"draw" => @$_POST['draw'],
			"recordsTotal" => $this->Model_notice->count_all(),
			"recordsFiltered" => $this->Model_notice->count_filtered(),
			"data" => $data,
		);
		// output to json format
		echo json_encode($output);
	}




	public function test_word()
	{

		$this->load->library('Phpword');


		// $this->template->title('Tambah Pengumuman');

		// $this->tempanri('backend/standart/administrator/master_notice/master_notice_create');

		// $phpWord = new \PhpOffice\PhpWord\PhpWord();
		$template = new \PhpOffice\PhpWord\TemplateProcessor('uploads/test123.docx');
		$template->setValue('test', 'uhuy');
		$filename = 'test123ubah.docx';
		$template->save('uploads/' . $filename);
		/* Note: any element you append to a document must reside inside of a Section. */

		// Adding an empty Section to the document...
		// $section = $phpWord->addSection();
		// Adding Text element to the Section having font styled by default...
		// $section->addText(
		// 	'"Learn from yesterday, live for today, hope for tomorrow. '
		// 	. 'The important thing is not to stop questioning." '
		// 	. '(Albert Einstein)'
		// );

		/*
		 * Note: it's possible to customize font style of the Text element you add in three ways:
		 * - inline;
		 * - using named font style (new font style object will be implicitly created);
		 * - using explicitly created font style object.
		 */

		// Adding Text element with font customized inline...
		// $section->addText(
		// 	'"Hikman Ganda Sasmita, '
		// 	. 'and is never the result of selfishness." '
		// 	. '(Napoleon Hill)',
		// 	array('name' => 'Tahoma', 'size' => 10)
		// );

		// Adding Text element with font customized using named font style...
		// $fontStyleName = 'oneUserDefinedStyle';
		// $phpWord->addFontStyle(
		// 	$fontStyleName,
		// 	array('name' => 'Tahoma', 'size' => 10, 'color' => '1B2232', 'bold' => true)
		// );
		// $section->addText(
		// 	'"The greatest accomplishment is not in never falling, '
		// 	. 'but in rising again after you fall." '
		// 	. '(Vince Lombardi)',
		// 	$fontStyleName
		// );

		// Adding Text element with font customized using explicitly created font style object...
		// $fontStyle = new \PhpOffice\PhpWord\Style\Font();
		// $fontStyle->setBold(true);
		// $fontStyle->setName('Tahoma');
		// $fontStyle->setSize(13);
		// $myTextElement = $section->addText('"Believe you can and you\'re halfway there." (Hikman Ganda Sasmita)');
		// $myTextElement->setFontStyle($fontStyle);

		// Saving the document as OOXML file...
		// $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
		// $objWriter->save('helloWorld.docx');

		// $objWriter->save('uploads/'.$filename);
		// send results to browser to download
		// header('Content-Description: File Transfer');
		// header('Content-Type: application/octet-stream');
		// header('Content-Disposition: attachment; filename='.$filename);
		// header('Content-Transfer-Encoding: binary');
		// header('Expires: 0');
		// header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		// header('Pragma: public');
		// header('Content-Length: ' . filesize($filename));
		// flush();
		// readfile('uploads/'.$filename);
		// unlink('uploads/'.$filename); // deletes the temporary file
		exit;
	}
}
