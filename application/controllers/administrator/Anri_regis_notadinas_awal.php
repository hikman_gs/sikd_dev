<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Anri_regis_notadinas extends Admin	
{
	public function __construct()
	{
		parent::__construct();

		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$this->load->model('model_konsepnaskah');
		$this->load->model('model_inboxfile');	
		$this->load->model('model_inboxreciever');		
		$this->load->model('model_inboxreciever_koreksi');
		$this->load->model('model_konsepnaskah_koreksi');
	}

	// Registrasi Nota Dinas
	public function index()
	{	
        //cek akses ambil dari helper
		check_access($this->session->userdata('groupid'), $this->uri->segment(2));

		$this->data['role'] = $this->db->query("SELECT * FROM role WHERE RoleId != 'root' ")->result();
		$this->data['title'] = 'Pencatatan Nota Dinas';
		$this->tempanri('backend/standart/administrator/nota_dinas/regis', $this->data);
	}
	//Registrasi Nota Dinas
	public function get_num_naskah($role_ttd){
		$thn = date('Y');
		$num = $this->db->query("SELECT MAX(Number) as Number, DATE_FORMAT(TglReg,'%Y') as Thn_akhir FROM konsep_naskah WHERE RoleId_From = '".$role_ttd."' ")->row();
	}
	//Fungsi Lihat Nota Dinas 
	public function lihat_naskah_dinas()
	{	
	
		$tanggal = date('Y-m-d H:i:s');
		$this->load->library('HtmlPdf');		
      	ob_start();
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Lokasi;
		
      	$this->data['RoleId_To'] 	= $this->input->post('RoleId_To');

      	
		$this->data['RoleId_Cc'] 	= $this->input->post('RoleId_Cc');	
		$this->data['MeasureUnitId'] = $this->db->query("SELECT MeasureUnitName FROM master_satuanunit WHERE MeasureUnitId = '".$this->input->post('MeasureUnitId')."'")->row()->MeasureUnitName;			
		$this->data['TglReg'] 		= date('Y-m-d H:i:s');	
		$this->data['TglNaskah'] 	= date('d-m-Y',strtotime($field->Tgl));
		$this->data['SifatId'] 		= $this->db->query("SELECT SifatName FROM master_sifat WHERE SifatId = '".$this->input->post('SifatId')."'")->row()->SifatName;
		$this->data['Jumlah'] 		= $this->input->post('Jumlah');
		$this->data['Hal'] 			= $this->input->post('Hal');
		$this->data['Konten'] 		= $this->input->post('Konten');
		$this->data['RoleCode'] 	= $this->input->post('rolecode');

		if($this->input->post('TtdText') == 'none') {
			$this->data['Approve_People']	= $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$this->data['Approve_People'] 	= $this->input->post('tmpid_atasan');		
		} else {
			$this->data['Approve_People'] 	= $this->input->post('Approve_People');
		}
		if($this->input->post('TtdText2') == 'AL') {
			$this->data['Approve_People3'] 	= $this->input->post('tmpid_atas_nama');	
		} else {
			$this->data['Approve_People3'] 	= $this->input->post('Approve_People3');
		}


		$this->data['ClCode'] 		= $this->db->query("SELECT ClCode FROM classification WHERE ClId = '".$this->input->post('ClId')."'")->row()->ClCode;
		$this->data['TtdText']     	= $this->input->post('TtdText');
		$this->data['TtdText2']     = $this->input->post('TtdText2');

		if($this->input->post('TtdText') == 'none') {
			$this->data['Nama_ttd_konsep']	= $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;		
		} else {
			$this->data['Nama_ttd_konsep'] 	= $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}
		
        $this->load->view('backend/standart/administrator/pdf/view_nadin_pdf',$this->data);

	    $content = ob_get_contents();
	    ob_end_clean();
        
      $config = array(
            'orientation' 	=> 'p',
            'format' 		=> 'F4',
            'marges' 		=> array(30, 30, 20, 20),
		        );

        $this->pdf = new HtmlPdf($config);
        $this->pdf->initialize($config);
        $this->pdf->pdf->SetDisplayMode('fullpage');
        $this->pdf->writeHTML($content);
        $this->pdf->Output('naskah_dinas_tindaklanjut-'.$tanggal.'.pdf', 'H');

	}
	// Akhir Fungsi Nota Dinas 

	//Fungsi Simpan Regis Nota Dinas
	public function post_notadinas()
	{

		$gir = $this->session->userdata('peopleid').date('dmyhis');
		$tanggal = date('Y-m-d H:i:s');
		$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Lokasi;

		if($this->input->post('TtdText') == 'none') {
			$z = $this->session->userdata('approvelname');
		} elseif($this->input->post('TtdText') == 'AL') {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('tmpid_atasan')."'")->row()->ApprovelName;	
		} else {
			$z = $this->db->query("SELECT ApprovelName FROM people WHERE PeopleId = '".$this->input->post('Approve_People')."'")->row()->ApprovelName;
		}	



		if($this->input->post('TtdText') == 'none') {
			$x =  $this->session->userdata('peopleid');
		} elseif($this->input->post('TtdText') == 'AL') {
			$x = $this->input->post('tmpid_atasan');		
		} else {
			$x = $this->input->post('Approve_People');
		}

		if($this->input->post('TtdText2') == 'none') {
		$a =('');
		$a = ('');
		} elseif($this->input->post('TtdText2') == 'AL') {		
		$a = $this->input->post('tmpid_atas_nama');	
		} else {
		$a = $this->input->post('Approve_People3');
		}	

		if($this->input->post('TtdText2') == 'none') {
			$b =('');
			$b = ('');
			} elseif($this->input->post('TtdText2') == 'AL') {		
			$b = $this->input->post('tmpid_atas_nama');	
			} else {
			$b = $this->input->post('Approve_People3');
			}	

		try{

			$data_konsep = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Nota Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_notadinas',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',',$this->input->post('RoleId_To')) : ''),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'Approve_People3'    => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxnotadinas',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'Nama_ttd_atas_nama' 	=> $x,			
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
			];
				//memunculkan konsep awal
			$data_konsep_awal = [
				'NId_Temp'   		=> $gir,
				'GIR_Id'       		=> $gir,
				'JenisId'       	=> $this->db->query("SELECT JenisId FROM master_jnaskah WHERE JenisName = 'Nota Dinas'")->row()->JenisId,
				'Jumlah'       		=> $this->input->post('Jumlah'),
				'MeasureUnitId'     => $this->input->post('MeasureUnitId'),
				'SifatId'       	=> $this->input->post('SifatId'),
				'UrgensiId'       	=> $this->input->post('UrgensiId'),
				'ReceiverAs'       	=> 'to_notadinas',
				'ClId'       		=> $this->input->post('ClId'),
				'Number'       		=> '0',
				'RoleCode'       	=> $this->input->post('rolecode'),
				'RoleId_From'      	=> $this->session->userdata('roleid'),
				'RoleId_To'       	=> (!empty($this->input->post('RoleId_To')) ?  implode(',',$this->input->post('RoleId_To')) : ''),
				'RoleId_Cc'       	=> (!empty($this->input->post('RoleId_Cc')) ?  implode(',',$this->input->post('RoleId_Cc')) : ''),
				'Konten'       		=> $this->input->post('Konten'),
				'Approve_People'    => $x,
				'Approve_People3'    => $b,
				'TtdText'       	=> $this->input->post('TtdText'),
				'TtdText2'       	=> $this->input->post('TtdText2'),
				'Konsep'       		=> '1',
				'TglReg'       		=> $tanggal,
				'Hal'       		=> $this->input->post('Hal'),
				'TglNaskah'       	=> $tanggal,
				'CreateBy'       	=> $this->session->userdata('peopleid'),
				'Ket'       		=> 'outboxnotadinas',
				'NFileDir'			=> 'naskah',
				'Nama_ttd_konsep' 	=> $z,
				'Nama_ttd_atas_nama' 	=> $x,			
				'lokasi'			=> $lok,
				'RoleId_From_Asal' 	=> $this->session->userdata('roleid'),
				'id_koreksi'    	=> $gir,

			];

				// akhir memunculkan konsep awal

			if (!empty($_POST['upload_naskahdinas_name'])) {

				foreach ($_POST['upload_naskahdinas_name'] as $idx => $file_name) {
					$test_title_name_copy = date('Ymd') . '-' . $file_name;

					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['upload_naskahdinas_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/naskah/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['upload_naskahdinas_uuid'][$idx]);

					$listed_image[] = $test_title_name_copy;
					$save_files = [
						'FileKey' 			=> tb_key(),
			            'GIR_Id' 			=> $gir,
			            'NId' 				=> $data_konsep['NId_Temp'],
			            'PeopleID' 			=> $this->session->userdata('peopleid'),
			            'PeopleRoleID' 		=> $this->session->userdata('roleid'),
			            'FileName_real' 	=> $file_name,
			            'FileName_fake' 	=> $test_title_name_copy,
			            'FileStatus' 		=> 'available',
			            'EditedDate'        => $tanggal,
			            'Keterangan' 		=> '',
					];

					$save_file = $this->model_inboxfile->store($save_files);

				}
			}

			$save_konsep 	= $this->model_konsepnaskah->store($data_konsep);

			//SAVE KONSEP
			$save_konsep_awal 	= $this->model_konsepnaskah_koreksi->store($data_konsep_awal);
			//AKHIR SAVE KONSEP NOTA


			//Simpan tujuan draft
			$girid_ir = $this->session->userdata('peopleid').date('dmyhis');

			$save_recievers = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_notadinas',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_reciever = $this->model_inboxreciever->store($save_recievers);

				//save konsep nota awal 
				$save_recievers_koreksi = [
				'NId' 			=> $gir,
				'NKey' 			=> tb_key(),
				'id_koreksi'	=> $gir,
				'GIR_Id' 		=> $girid_ir,
				'From_Id' 		=> $this->session->userdata('peopleid'),
				'RoleId_From' 	=> $this->session->userdata('roleid'),
				'To_Id' 		=> $x,
				'RoleId_To' 	=> get_data_people('RoleId',$x),
				'ReceiverAs' 	=> 'to_draft_notadinas',
				'Msg' 			=> 'Mohon untuk dapat dikoreksi dan ditandatangani',
				'StatusReceive' => 'unread',
				'ReceiveDate' 	=> $tanggal,
				'To_Id_Desc' 	=> get_data_people('RoleDesc',$x),
			];

			$save_reciever_koreksi = $this->model_inboxreciever_koreksi->store($save_recievers_koreksi);
			//save konsep nota awal 

			set_message('Data Berhasil Disimpan', 'success');
			redirect(BASE_URL('administrator/anri_list_naskah_belum_approve'));

		}catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			$this->load->library('user_agent');
			redirect($this->agent->referrer());
		}
		
	}
	//Fungsi Simpan Regis Nota Dinas

}