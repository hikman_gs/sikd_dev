<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class Tandatangan extends Admin	
{
	private $table_name = 'm_ttd';
	public function __construct()
	{
		parent::__construct();

		$this->load->library('Pdf');
		$this->load->helper('text');
		
		if($this->session->userdata('status') != "anri_ok_dong"){
			redirect(base_url("administrator/masuk"));
		}
		
		$PeopleID = $this->session->userdata('peopleid');
 
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd');
		$jumlahfile = $this->db->count_all_results();
		$this->data['jumlahfile'] = $jumlahfile;
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->select_sum('ukuran');
		$this->db->from('m_ttd');
		$query = $this->db->get();
		$query = $query->row();
		
		$this->data['ukuran'] = isset($query->ukuran)?$query->ukuran:0;
		
		
		 
		$this->db->where('PeopleIDTujuan', $PeopleID);
		$this->db->where('status', 0);
		$this->db->from('m_ttd_kirim');
		$jumlahfilerespon = $this->db->count_all_results();
		$this->data['jumlahfilerespon'] = $jumlahfilerespon;
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->where('status', 0);
		$this->db->group_by('ttd_id');
		$this->db->from('m_ttd_kirim');
		$jumlahfileresponterkirim = $this->db->count_all_results();
		$this->data['jumlahfileresponterkirim'] = $jumlahfileresponterkirim;
		
	}
	
    public function download($id) {
		$this->load->helper('download');
		$this->load->helper('file');
		
		$this->db->where('id', $id);
        $query = $this->db->get($this->table_name);
		$query = $query->row();
		
		if($query->status == 0) {
			$files = FCPATH . 'FilesUploaded/ttd/blm_ttd/';
			$file = $files.$query->file;
		} elseif ($query->status == 3) {
			$files = FCPATH . 'FilesUploaded/ttd/blm_ttd/';
			$file = $files.$query->file;
			if (file_exists($file)) {
				$file = $file;
			} else {
				$files = FCPATH . 'FilesUploaded/ttd/sudah_ttd/';
				$file = $files.$query->file;
			}
		} else {
			$files = FCPATH . 'FilesUploaded/ttd/sudah_ttd/';
			$file = $files.$query->file;
		} 
		
        if(file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="'.basename($file).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            flush(); // Flush system output buffer
            readfile($file);
            die();
        } else {
            http_response_code(404);
			redirect(BASE_URL('administrator/tandatangan/index/0'));
	        die();
        }
	}
	
    public function verifikasi($file) {
		
		$file = $file.'.pdf';
		if($this->getfileStatus($file) == '1') {
			$signed_file = FCPATH . 'FilesUploaded/ttd/sudah_ttd/'.$file;
		} else {
			$signed_file = FCPATH . 'FilesUploaded/ttd/blm_ttd/'.$file;
		}
		$namafile = basename($signed_file, ".pdf");
		
		$curl = curl_init();
		if (ENVIRONMENT == 'development') {
			$urlprod  = 'http://103.122.5.60';
			$nik = '0803202100007062';
		} else {
			$nik = $this->session->userdata('nik');
			$urlprod = 'http://103.122.5.59';
		}
		curl_setopt_array($curl, array( 
			CURLOPT_URL => $urlprod.'/api/sign/verify',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => array('signed_file' => new CURLFILE($signed_file, 'application/pdf', $namafile)),
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic ZXNpZ246cXdlcnR5",
				"Cookie: JSESSIONID=73BD894F2791B5DAA23D7A76FEE5F5FB"
			)
		));
		$response = curl_exec($curl);
		curl_close($curl);

		$result = json_decode($response);
		echo '<pre>';
		echo $result->nama_dokumen;
		if ($result->jumlah_signature > 0) {
			echo '<br>';
			echo $result->summary;
		}
		echo '<br>';
		print_r($result);
	}
	
	
    public function index($id=0) {
		
		$this->data['title'] = 'Tandatangan Elektronik';
		
		$PeopleID = $this->session->userdata('peopleid');
		
		if($id == 'konsep') {
			$this->db->where('status', 0);
		}
		if($id == 'selesai') {
			$this->db->where('status', 1);
		}
		
		$this->db->order_by('tanggal','DESC');
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd'); 
		$tte = $this->db->get();
		$tte = $tte->result();
		$this->data['tte'] = $tte;

		$this->tempanri('backend/standart/administrator/tandatangan/index', $this->data);
	}

    public function upload($id=0) {
		$this->data['GRoleId'] = 'indexttd';

		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd'); 
		$tte = $this->db->get();
		$tte = $tte->result();

 
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd');
		$jumlahfile = $this->db->count_all_results();
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->select_sum('ukuran');
		$this->db->from('m_ttd');
		$query = $this->db->get();
		$query = $query->row();
		
		$this->data['ukuran'] = $query->ukuran;
 
		$this->data['jumlahfile'] = $jumlahfile;
		$this->data['tte'] = $tte;
		$this->tempanri('backend/standart/administrator/tandatangan/upload', $this->data);
	}
	
    public function upload1($id=0) {
 
		$nik = $this->session->userdata('peopleusername');
		$tanggal = date('Y-m-d H:i:s');
		
		try {
			$this->load->helper('string'); 
			if (count((array) $this->input->post('test_title_name'))) {
				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					
					$test_title_name_copy = str_replace('.','_',$nik) . '_' . $file_name;
					
					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/ttd/blm_ttd/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);
					
					$namafile   = explode('.', $_POST['nama_file'][$idx]);	
					$ukuranfile = $_POST['ukuran'][$idx];	
					$data = array( 
						'nama_file' => current($namafile),
						'ukuran' => $ukuranfile,
						'nik' => $nik,
						'file' => $test_title_name_copy,
						'tanggal' 			=> $tanggal,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
					);
					$insert = $this->db->insert('m_ttd', $data);
					$id = $this->db->insert_id();
				}
				set_message('Data Berhasil Disimpan', 'success');
				//redirect(BASE_URL('administrator/tandatangan/detail/'.$id));
				redirect(BASE_URL('administrator/tandatangan/index/0'));
			} else {
							set_message('Gagal Menyimpan Data', 'error');
				redirect(BASE_URL('administrator/tandatangan/index/0'));
			}
		} catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/tandatangan'));
		}
	}
	// Upload naskah masuk
	public function upload_ttd_file()
	{
		$uuid = $this->input->post('qquuid');

		echo $this->upload_file([
			'uuid' 		 	=> $uuid,
			'table_name' 	=> 'ttd',
		]);
	}
	// Tutup upload naskah masuk

	// Hapus file naskah masuk
	public function delete_ttd_file($uuid)
	{

		$dirname = BASE_URL.'/uploads/tmp/'.$uuid;	

		if(rmdir($dirname)) {
          echo ("$dirname successfully removed");
        } else {
          echo ("$dirname couldn't be removed"); 
		}

		echo $this->delete_file([
            'uuid'              => $uuid, 
            'delete_by'         => $this->input->get('by'), 
            'field_name'        => $this->input->get('previewLink'), 
            'field_name'        => 'title', 
            'upload_path_tmp'   => './uploads/tmp/',
            'table_name'        => 'test',
            'primary_key'       => 'id_test',
            'upload_path'       => 'uploads/test/'
        ]);
	}
	
    public function signed($id=0) {
		$this->data['GRoleId'] = 'indexttd';
		
		$this->tempanri('backend/standart/administrator/tandatangan/signed', $this->data);
	}

 
     public function detail2($id=0) {
		
		$this->data['GRoleId'] = 'indexttd';
		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('id',$id);
        $query = $this->db->get($this->table_name);
		$query = $query->row();
		
		
		$this->data['idfile'] = $query->id;
		$this->data['nama_file'] = $query->nama_file;

		if($query->status == 0) {
			$files = BASE_URL . 'FilesUploaded/ttd/blm_ttd/';
			$this->data['file'] = $files.$query->file;
		} else {
			$files = BASE_URL . 'FilesUploaded/ttd/sudah_ttd/';
			$this->data['file'] = $files.$query->file;
		}
		
		//
		$b64Doc = chunk_split(base64_encode(file_get_contents($this->data['file'])));
		$this->data['b64Doc'] = $b64Doc;
		$this->data['filen'] = $query->file;
		$this->tempanri('backend/standart/administrator/tandatangan/detail', $this->data);
	}

     public function detail($id=0) {
		
		$this->data['GRoleId'] = 'indexttd';
		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('id',$id);
        $query = $this->db->get($this->table_name);
		$query = $query->row();
		
		
		$this->data['idfile'] = $query->id;
		$this->data['nama_file'] = $query->nama_file;

		if($query->status == 0) {
			$files = BASE_URL . 'FilesUploaded/ttd/blm_ttd/';
			$this->data['file'] = $files.$query->file;
		} else {
			$files = BASE_URL . 'FilesUploaded/ttd/sudah_ttd/';
			$this->data['file'] = $files.$query->file;
		}
		
 
		$this->data['filen'] = $query->file;

		$data['filen'] = $this->data['filen'];
		$data['idfile'] = $query->id;
		$data['file'] = $this->data['file'];
		$this->load->view('backend/standart/administrator/tandatangan/detail', $data);
	}

    public function signttd_multi($dok) {

		$files = $this->input->post('filename');
		$passphrase = $this->input->post('password'); 
 
		if($passphrase) {
			$file = $this->input->post('file');
			
			foreach ($file as $key => $val) {
				$this->ttd_multi($val, $passphrase,$dok);
			}
			redirect('administrator/tandatangan/index/0');
		}
 		

		$this->data['title'] = 'Tandatangan Elektronik';     
		$this->data['files'] = $files;     
		$this->data['respon'] = $dok;     
		$this->tempanri('backend/standart/administrator/tandatangan/verifikasi', $this->data);
	}
   
	public function ttd_multi($file, $password, $respon) {
		

		if (ENVIRONMENT == 'development') {
			$nik   = '0803202100007062';
			$url   = 'http://103.122.5.60';
			$auth  = 'ZXNpZ246cXdlcnR5';
			$kukis = '21196E81CA1819E8D34FB087F6F535B2';
		} else {
			$nik   = $this->session->userdata('nik');
			$url   = 'http://103.122.5.59';
			$auth  = 'U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=';
			$kukis = '';
		}
 
		if($this->getfileStatus($file) == '1') {
			$asal = FCPATH . 'FilesUploaded/ttd/sudah_ttd/'.$file;
		} else {
			$asal = FCPATH . 'FilesUploaded/ttd/blm_ttd/'.$file;
		}

		//Proses cek NIK pada API Cloud BSRE
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $url."/api/user/status/".$nik,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,    
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Authorization: Basic ".$auth,
				"Cookie: JSESSIONID=".$kukis
			),

		));

		$response = curl_exec($curl);
		curl_close($curl);

		$result = json_decode($response);

		if ($result->status_code == 1111) {

			//Jika NIK terdaftar, Cek Passphrase
			//Proses cek Passphrase pada API Cloud BSRE
			
			//url api sertifikat elektronik
			$url_signed = $url.'/api/sign/pdf';

			//passphrase for the certificate

			//curl initialization
			$curl = curl_init();

			//$asal = FCPATH . 'FilesUploaded/example/pub-inf_3651.pdf';
			$imageTTD = FCPATH . 'FilesUploaded/example/ttd.jpg';
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => $url_signed,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_POSTFIELDS => array('file' => new CURLFILE($asal, 'application/pdf'), 'nik' => $nik, 'passphrase' => $password, 'tampilan' => 'invisible', 'page' => '1', 'image' => 'false', 'imageTTD' => $imageTTD, 'linkQR' => 'https://google.com', 'xAxis' => '0', 'yAxis' => '0', 'width' => '500', 'height' => '113'),
				CURLOPT_HTTPHEADER => array(
					"Authorization: Basic ".$auth,
					"Cookie: JSESSIONID=".$kukis
				),
			));

			$response = curl_exec($curl);
			//Fungsi untuk mengambil nilai http request
			$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			//===
 			
			$tanggal = date('Ymd').'-'.date('H:i:s');

			$result = json_decode($response);
	 
			if ($httpCode == '200') {
				$tujuan   = FCPATH . 'FilesUploaded/ttd/sudah_ttd/'.time() .'_signed.pdf';
				$filebaru = time() .'_signed.pdf';
				file_put_contents($tujuan, $response);

				//Update Status
				$idfile = $this->getfileID($file);
				$this->db->where('id', $idfile); 
				$this->db->update('m_ttd', ['status' => 1, 'file' => $filebaru]);

				//respondokumen
				if($respon == 'respondokumen') {
					//$this->cekurutan();
					$PeopleID = $this->session->userdata('peopleid');
					$this->db->where('PeopleIDTujuan', $PeopleID); 
					$this->db->where('ttd_id', $idfile); 
					$this->db->update('m_ttd_kirim', ['status' => 1, 'tgl_ttd'=>date('Y-m-d H:i:s')]);
				}  
				set_message('Dokumen Berhasil Ditandatangani', 'success'); 
				return $httpCode;
			} else {

				//Jika Passphrase Salah
				set_message('Passphrase Anda Salah, Silahkan Coba Lagi', 'error'); 
			}
			
		} else {
			set_message('NIK Anda Belum Terdaftar', 'error');
		}		
		curl_close($curl);
		
		$this->load->library('user_agent');
		redirect('administrator/tandatangan/index/0');
	}
	
		
	public function getfileID($namafile) {
		
		$this->db->where('file', $namafile); 
		$query = $this->db->get('m_ttd');
		$ret = $query->row();
		return $ret->id;
	}
	
	public function getfileName($id) {
		
		$this->db->where('id', $id); 
		$query = $this->db->get('m_ttd');
		$ret = $query->row();
		return $ret->file;
	}
	
	public function getfileStatus($namafile) {
		
		$this->db->where('file', $namafile); 
		$query = $this->db->get('m_ttd');
		$ret = $query->row();
		return $ret->status;
	}
	public function getImageTtd($id) {
		
		$PeopleID = $this->session->userdata('peopleid');
		$this->db->where('id', $id); 
		$this->db->where('PeopleID', $PeopleID); 
		$query = $this->db->get('m_ttd_spesimen');
		$ret = $query->row();
		return $ret->image;
	}

	
	public function getFile() {
		$nik = $this->session->userdata('peopleusername');
		$tanggal = date('Y-m-d H:i:s');
		
		$files = $this->input->post('filename'); 
		foreach ($files as $key => $val) {
			
			$string = explode('_dan_', $val);
			$folder = $string[0];
			$file   = $string[1];
			$Hal    = $string[2];
			$kopi = copy(
				FCPATH . 'FilesUploaded/'.$folder.'/' . $file, FCPATH . 'FilesUploaded/ttd/blm_ttd/' . $file
			);
			if($kopi) {
				$namafile   = explode('.', $file);
				$ukuranfile = filesize(FCPATH . 'FilesUploaded/'.$folder.'/' . $file);
				$data = array( 
					'nama_file' => $Hal,
					'ukuran' => $ukuranfile,
					'nik' => $nik,
					'file' => $file,
					'tanggal' => $tanggal,
					'PeopleID' => $this->session->userdata('peopleid'),
				);
				$insert = $this->db->insert('m_ttd', $data);
			}
		}
		set_message('Dokumen Berhasil Ditambahkan', 'success');
		//redirect('administrator/tandatangan/index/0');
	}
	
	
	
	public function signandshare() {
		$this->data['GRoleId'] = 'indexttd';

		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd'); 
		$tte = $this->db->get();
		$tte = $tte->result();

 
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd');
		$jumlahfile = $this->db->count_all_results();
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->select_sum('ukuran');
		$this->db->from('m_ttd');
		$query = $this->db->get();
		$query = $query->row();
		
		$this->data['ukuran'] = $query->ukuran;
 
		$this->data['jumlahfile'] = $jumlahfile;
		$this->data['tte'] = $tte;
		$this->tempanri('backend/standart/administrator/tandatangan/signandshare', $this->data);
	}
	
	public function selfsign() {
		$this->data['GRoleId'] = 'indexttd';

		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd'); 
		$tte = $this->db->get();
		$tte = $tte->result();

 
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd');
		$jumlahfile = $this->db->count_all_results();
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->select_sum('ukuran');
		$this->db->from('m_ttd');
		$query = $this->db->get();
		$query = $query->row();
		
		$this->data['ukuran'] = $query->ukuran;
 
		$this->data['jumlahfile'] = $jumlahfile;
		$this->data['tte'] = $tte;
		$this->tempanri('backend/standart/administrator/tandatangan/uploadsendiri', $this->data);
	}
	
	
	public function upload2($id=0) {
 
		$nik = $this->session->userdata('peopleusername');
		$tanggal = date('Y-m-d H:i:s');
		
		try {
			$this->load->helper('string'); 
			if (count((array) $this->input->post('test_title_name'))) {
				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					
					$test_title_name_copy = str_replace('.','_',$nik) . '_' . $file_name;
					
					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/ttd/blm_ttd/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);
					
					$namafile   = explode('.', $_POST['nama_file'][$idx]);	
					$ukuranfile = $_POST['ukuran'][$idx];	
					$data = array( 
						'nama_file' => current($namafile),
						'ukuran' => $ukuranfile,
						'nik' => $nik,
						'file' => $test_title_name_copy,
						'tanggal' 			=> $tanggal,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'tanda' 			=> 1,
					);
					$insert = $this->db->insert('m_ttd', $data);
					$id = $this->db->insert_id();
				}
				set_message('Data Berhasil Disimpan', 'success');
				redirect(BASE_URL('administrator/tandatangan/detailselfsign/'.$id));
				//redirect(BASE_URL('administrator/tandatangan'));
			} else {
							set_message('Gagal Menyimpan Data', 'error');
				redirect(BASE_URL('administrator/tandatangan/index/0'));
			}
		} catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/tandatangan/index/0'));
		}
	}
	
	public function upload3($id=0) {
 
		$nik = $this->session->userdata('peopleusername');
		$tanggal = date('Y-m-d H:i:s');
		
		try {
			$this->load->helper('string'); 
			if (count((array) $this->input->post('test_title_name'))) {
				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					
					$test_title_name_copy = str_replace('.','_',$nik) . '_' . $file_name;
					
					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/ttd/blm_ttd/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);
					
					$namafile   = explode('.', $_POST['nama_file'][$idx]);	
					$ukuranfile = $_POST['ukuran'][$idx];	
					$data = array( 
						'nama_file' => current($namafile),
						'ukuran' => $ukuranfile,
						'nik' => $nik,
						'file' => $test_title_name_copy,
						'tanggal' 			=> $tanggal,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
						'tanda' 			=> 2,
					);
					$insert = $this->db->insert('m_ttd', $data);
					$id = $this->db->insert_id();
				}
				set_message('Data Berhasil Disimpan', 'success');
				redirect(BASE_URL('administrator/tandatangan/detailsignandshare/'.$id));
			} else {
				set_message('Gagal Menyimpan Data', 'error');
				redirect(BASE_URL('administrator/tandatangan/index/0'));
			}
		} catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/tandatangan/index/0'));
		}
	}
	
	
	public function detailsignandshare($id=0) {
		
		$this->data['GRoleId'] = 'indexttd';
		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('id',$id);
        $query = $this->db->get($this->table_name);
		$query = $query->row();
		
		
		$this->data['idfile'] = $query->id;
		$this->data['nama_file'] = $query->nama_file;

		if($query->status == 0) {
			$files = BASE_URL . 'FilesUploaded/ttd/blm_ttd/';
			$this->data['file'] = $files.$query->file;
		} else {
			$files = BASE_URL . 'FilesUploaded/ttd/sudah_ttd/';
			$this->data['file'] = $files.$query->file;
		}

		$this->data['filen'] = $query->file;
		
		$data['filen'] = $this->data['filen'];
		$data['idfile'] = $query->id;
		$data['file'] = $this->data['file'];
		$this->load->view('backend/standart/administrator/tandatangan/detailsignandshare', $data);
	}
	
	public function detailselfsign($id=0,$respon='norespon') {
		
		$this->data['GRoleId'] = 'indexttd';
		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('id',$id);
        $query = $this->db->get($this->table_name);
		$query = $query->row();
		
		
		$this->data['idfile'] = $query->id;
		$this->data['nama_file'] = $query->nama_file;

		
		if($query->status == 0) {
			$files = BASE_URL . 'FilesUploaded/ttd/blm_ttd/';
			$file = $files.$query->file;
		} elseif ($query->status == 3) {
			$files = BASE_URL . 'FilesUploaded/ttd/blm_ttd/';
			$file = $files.$query->file;
			if (file_exists($file)) {
				$file = $file;
			} else {
				$files = BASE_URL . 'FilesUploaded/ttd/sudah_ttd/';
				$file = $files.$query->file;
			}
		} else {
			$files = BASE_URL . 'FilesUploaded/ttd/sudah_ttd/';
			$file = $files.$query->file;
		} 
		
 
		$this->data['filen'] = $query->file;
		$respon = $respon?$respon:'norespon';
		$data['respon']  = $respon;
		$data['filen']  = $this->data['filen'];
		$data['idfile'] = $query->id;
		$data['file']  = $file;

		///
		$this->db->where('PeopleID',$PeopleID);
        $query1 = $this->db->get('m_ttd_spesimen');
		$query1 = $query1->result();
		$jsonSpesimen = json_encode($query1);
 
		$data['jsonSpesimen']  = $jsonSpesimen;
		 
		$this->load->view('backend/standart/administrator/tandatangan/detailselfsign', $data);
	}
	
	public function signttdselfsign() {

		$PeopleID = $this->session->userdata('peopleid');
		
		$files = $this->input->post('filename');
		$passphrase = $this->input->post('password');
		
		$idfile = $this->input->post('idfile');
		$xAxis = $this->input->post('xAxis');
		$yAxis = $this->input->post('yAxis');
		$pageNum = $this->input->post('pageNum');
		$linkQR = BASE_URL.'administrator/tandatangan/verifikasi/'.$idfile;
		$respon = $this->input->post('respon');
		$spesimenID = $this->input->post('spesimenID');
		
		$getImageTtd = $this->getImageTtd($spesimenID);
		$imageTTD = BASE_URL . 'FilesUploaded/ttd/spesimen/'.$getImageTtd;
		
 
		if($passphrase) {
			
			if (ENVIRONMENT == 'development') {
				$nik   = '0803202100007062';
				$url   = 'http://103.122.5.60';
				$auth  = 'ZXNpZ246cXdlcnR5';
				$kukis = '21196E81CA1819E8D34FB087F6F535B2';
			} else {
				$nik   = $this->session->userdata('nik');
				$url   = 'http://103.122.5.59';
				$auth  = 'U0lLREpBQkFSOlNJS0QyMDIxSmFiYXI=';
				$kukis = '';
			}
			
			$filename = $this->getfileName($idfile);
			if($this->getfileStatus($filename) == '1') {
				$asal = FCPATH . 'FilesUploaded/ttd/sudah_ttd/'.$filename;
			} else {
				$asal = FCPATH . 'FilesUploaded/ttd/blm_ttd/'.$filename;
			}
			//Proses cek NIK pada API Cloud BSRE
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $url."/api/user/status/".$nik,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_FOLLOWLOCATION => true,    
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Authorization: Basic ".$auth,
					"Cookie: JSESSIONID=".$kukis
				),

			));

			$response = curl_exec($curl);
			curl_close($curl);

			$result = json_decode($response);

			if ($result->status_code == 1111) {
				//Jika NIK terdaftar, Cek Passphrase
				//Proses cek Passphrase pada API Cloud BSRE
				
				//url api sertifikat elektronik
				$url_signed = $url.'/api/sign/pdf';

				//passphrase for the certificate

				//curl initialization
				$curl = curl_init();

				curl_setopt_array($curl, array(
					CURLOPT_URL => $url_signed,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_ENCODING => "",
					CURLOPT_MAXREDIRS => 10,
					CURLOPT_TIMEOUT => 0,
					CURLOPT_FOLLOWLOCATION => true,
					CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
					CURLOPT_CUSTOMREQUEST => "POST",
					CURLOPT_POSTFIELDS => array('file' => new CURLFILE($asal, 'application/pdf'), 'nik' => $nik, 'passphrase' => $passphrase, 'tampilan' => 'visible', 'page' => $pageNum, 'image' => 'true', 'imageTTD' => $imageTTD, 'linkQR' => $linkQR, 'xAxis' => $xAxis, 'yAxis' => $yAxis, 'width' => '200', 'height' => '100'),
					CURLOPT_HTTPHEADER => array(
						"Authorization: Basic ".$auth,
						"Cookie: JSESSIONID=".$kukis
					),
				));

				$response = curl_exec($curl);
				//Fungsi untuk mengambil nilai http request
				$httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
				//===
				
				$tanggal = date('Ymd').'-'.date('H:i:s');

				$result = json_decode($response);
		 
				if ($httpCode == '200') {
					$tujuan   = FCPATH . 'FilesUploaded/ttd/sudah_ttd/' . $filename .'_'.time() .'_signed.pdf';
					$filebaru = $filename .'_'.time() .'_signed.pdf';
					file_put_contents($tujuan, $response);

					//Update Status
					$idfile = $this->getfileID($filename);
					$this->db->where('id', $idfile); 
					$this->db->update('m_ttd', ['status' => 1, 'file' => $filebaru]);
					
					//respondokumen
					if($respon == 'respondokumen') {
						
						$this->db->where('PeopleIDTujuan', $PeopleID); 
						$this->db->where('ttd_id', $idfile); 
						$this->db->update('m_ttd_kirim', ['status' => 1, 'tgl_ttd'=>date('Y-m-d H:i:s')]);
					}
					set_message('Dokumen Berhasil Ditandatangani', 'success'); 

				} else {

					//Jika Passphrase Salah
					set_message('Passphrase Anda Salah, Silahkan Coba Lagi', 'error'); 
					$this->session->set_flashdata('error', 'Passphrase Anda Salah');
					redirect('administrator/tandatangan/detailselfsign/'.$idfile);
					
				}
				
			} else {
				set_message('NIK Anda Belum Terdaftar', 'error');
			}		
			curl_close($curl);

			if($respon == 'norespon') {
				redirect('administrator/tandatangan/index/0');
			} else {
				redirect('administrator/tandatangan/respons/dokumen');
			}
		}
		
		$this->data['title'] = 'Tandatangan Elektronik';     
		$this->data['files'] = $files;     
		$this->tempanri('backend/standart/administrator/tandatangan/verifikasi', $this->data);
	}
	
	
	public function hapusdokumen() {
		
		$id = $this->input->post('id');
		$filename = $this->getfileName($id);
		if($this->getfileStatus($filename) == '1') {
			$file = FCPATH . 'FilesUploaded/ttd/sudah_ttd/'.$filename;
		} else {
			$file = FCPATH . 'FilesUploaded/ttd/blm_ttd/'.$filename;
		}
		if(is_file($file))
			unlink($file);
			
		$this->db->query("DELETE FROM `m_ttd` WHERE id = '".$id."'");
		
		$this->db->where('ttd_id', $id); 
		$query = $this->db->get('m_ttd_kirim');
		$ret = $query->row();
		if($ret) {
			$this->db->query("DELETE FROM `m_ttd_kirim` WHERE ttd_id = '".$id."'");
		}
		echo json_encode(array("status" => 'success', "id" => $id));
	}
	
	public function editspesimen($id=0) {
		
	} 
	public function hapusspesimen() {
		
		$id = $this->input->post('id');
		$this->db->where('id', $id); 
		$query = $this->db->get('m_ttd_spesimen');
		$ret = $query->row();
		$image = $ret->image;
		
		$file = FCPATH . 'FilesUploaded/ttd/spesimen/'.$image;

		if(is_file($file))
			unlink($file);
			
		$this->db->query("DELETE FROM `m_ttd_spesimen` WHERE id = '".$id."'");
		echo json_encode(array("status" => 'success', "id" => $id));
	} 
	
	public function spesimen($id=0) {
		
		$this->data['title'] = 'Tandatangan Elektronik';
		
		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('PeopleID', $PeopleID);
		$this->db->from('m_ttd_spesimen'); 
		$tte = $this->db->get();
		$tte = $tte->result();
		$this->data['tte'] = $tte;

		$this->tempanri('backend/standart/administrator/tandatangan/indexspesimen', $this->data);
	}
	
	public function uploadspesimen($id=0) {
 
		$nik = $this->session->userdata('peopleusername');
		$tanggal = date('Y-m-d H:i:s');
		
		try {
			$this->load->helper('string'); 
			if (count((array) $this->input->post('test_title_name'))) {
				foreach ($_POST['test_title_name'] as $idx => $file_name) {
					
					$test_title_name_copy = str_replace('.','_',$nik) . '_' . $file_name;
					
					//untuk merename nama file 
					rename(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx].'/'.$file_name,
						FCPATH.'FilesUploaded/ttd/spesimen/'.$test_title_name_copy);

					//untuk menghapus folder upload sementara setelah simpan data dieksekusi
					rmdir(FCPATH.'uploads/tmp/'.$_POST['test_title_uuid'][$idx]);
 
					$data = array( 
						'nama' => $this->input->post('nama'),
						'image' => $test_title_name_copy,
						'PeopleID' 			=> $this->session->userdata('peopleid'),
					);
					$insert = $this->db->insert('m_ttd_spesimen', $data);
					$id = $this->db->insert_id();
				}
				set_message('Data Berhasil Disimpan', 'success');
				redirect(BASE_URL('administrator/tandatangan/spesimen/'));
			} else {
				set_message('Gagal Menyimpan Data', 'error');
				redirect(BASE_URL('administrator/tandatangan/spesimen'));
			}
		} catch(\Exception $e){
			set_message('Gagal Menyimpan Data', 'error');
			redirect(BASE_URL('administrator/tandatangan/spesimen'));
		}
	}
	

    public function uploadspesimens() {
		$this->data['GRoleId'] = 'indexttd';


		$this->tempanri('backend/standart/administrator/tandatangan/uploadspesimens', $this->data);
	}
	
	
    public function savesignandshare() {

		$idfile = $this->input->post('idfile');
		$catatan = $this->input->post('catatan');
		$pesan   		= '';
		$PeopleID	= $this->session->userdata('peopleid');
 
		$tanggal1 = date('Y-m-d H:i:s');
		//---------------------------------------------------------------------
		// userpenandatangan
		if (count((array) $this->input->post('userpenandatangan'))) {
			$urutan = count($this->input->post('userpenandatangan'));
			foreach ($this->input->post('userpenandatangan') as $userpdtt) {
				
				$data = array( 
					'ttd_id' => $idfile, 
					'catatan' => $catatan,
					'pesan' => $pesan,
					'tgl' 			=> $tanggal1,
					'PeopleID' 			=> $this->session->userdata('peopleid'),
					'PeopleIDTujuan'    => $userpdtt,
					'urutan'    => $urutan,
				);
				$insert = $this->db->insert('m_ttd_kirim', $data);
				$urutan--;
			}
			//Update Status 
			$this->db->where('id', $idfile); 
			$this->db->update('m_ttd', ['status' => 3]);
		}
		set_message('Dokumen Berhasil Dikirim', 'success'); 
		redirect('administrator/tandatangan/index/0');
	}
	
	public function respons($dok='') {
		$this->data['title'] = 'Tandatangan Elektronik';
		
		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->where('PeopleIDTujuan', $PeopleID);
		$this->db->order_by('tgl', 'DESC');
		$this->db->from('m_ttd_kirim'); 
		$tte = $this->db->get();
		$tte = $tte->result();
 
		$this->data['tte'] = $tte;
		
 
		$this->tempanri('backend/standart/administrator/tandatangan/respondokumen', $this->data);
	}
	
	public function responsterkirim($dok='') {
		$this->data['title'] = 'Tandatangan Elektronik';
		
		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->order_by('tgl','DESC');
		$this->db->where('PeopleID', $PeopleID);
		$this->db->group_by('ttd_id');
		$this->db->from('m_ttd_kirim'); 
		$tte = $this->db->get();
		$tte = $tte->result();
		$this->data['tte'] = $tte;

		$this->tempanri('backend/standart/administrator/tandatangan/responterkirim', $this->data);
	}
	
	public function dokumendetail($id) {
		$this->data['title'] = 'Tandatangan Elektronik';

		$this->db->order_by('urutan');
		$this->db->where('ttd_id', $id);
		$this->db->from('m_ttd_kirim'); 
		$tte = $this->db->get();
		$result = $tte->result();

		
		$this->db->where('id', $id); 
		$this->db->from('m_ttd');
		$query = $this->db->get();
		$query = $query->row();
 		
		//$this->data['catatan'] = isset($query->catatan)?$query->catatan:'-';
		$this->data['file'] = isset($query->file)?$query->file:'-';
		$this->data['id'] = isset($query->id)?$query->id:'-';
		$this->data['status'] = isset($query->status)?ttd_label($query->status):'-';
		$this->data['PeopleID'] = isset($query->PeopleID)?$query->PeopleID:'-';
		$this->data['namadokumen'] = isset($query->id)?getfileName1TTD($query->id):'-';;
		$this->data['result'] = $result;
		$this->tempanri('backend/standart/administrator/tandatangan/dokumendetail', $this->data);
	}

	public function teruskan($ttd_id) {
		$this->data['title'] = 'Tandatangan Elektronik';

		$tanggal = date('Y-m-d H:i:s');
		//---------------------------------------------------------------------
		$data = array( 
			'ttd_id' => $ttd_id,  
			'PeopleID' 			=> $this->session->userdata('peopleid'),
			'PeopleIDTujuan'    => 0,
			'tgl'    => $tanggal,
		);
		$insert = $this->db->insert('m_ttd_terusankirim', $data);
 
		set_message('Dokumen Berhasil Dikirim', 'success'); 
		redirect('administrator/tandatangan/index/0');
	}
	
	public function ubahstatusdok($ttd_id) { 
	
		//Update Status 
		$this->db->where('id', $ttd_id); 
		$this->db->update('m_ttd_terusankirim', ['status' => 1]);

		set_message('Status Dokumen Berhasil Diubah', 'success'); 
		redirect('administrator/tandatangan/dokumen/kirim');
	}
	
	public function dokumen($dok='') {
		$this->data['title'] = 'Tandatangan Elektronik';
		
		$PeopleID = $this->session->userdata('peopleid');
		
		$this->db->order_by('tgl','DESC');
		$this->db->where('PeopleID', $PeopleID); 
		$this->db->from('m_ttd_terusankirim'); 
		$tte = $this->db->get();
		$tte = $tte->result();
		$this->data['tte'] = $tte;
 
		$this->tempanri('backend/standart/administrator/tandatangan/dokumenterkirim', $this->data);
	}
	
	
	public function teruskandankirim() {

		$idfile = $this->input->post('fileID');
		$catatan = $this->input->post('catatan');
		$pesan   		= '';
		$PeopleID	= $this->session->userdata('peopleid');
 
		$tanggal1 = date('Y-m-d H:i:s');
		//---------------------------------------------------------------------
		// userpenandatangan
		if (count((array) $this->input->post('userpenandatangan'))) {
			$urutan = count($this->input->post('userpenandatangan'));
			foreach ($this->input->post('userpenandatangan') as $userpdtt) {
				
				$data = array( 
					'ttd_id' => $idfile, 
					'catatan' => $catatan,
					'pesan' => $pesan,
					'tgl' 			=> $tanggal1,
					'PeopleID' 			=> $this->session->userdata('peopleid'),
					'PeopleIDTujuan'    => $userpdtt,
					'urutan'    => $urutan,
				);
				$insert = $this->db->insert('m_ttd_terusankirim', $data);
				$urutan--;
			}
		}
		set_message('Dokumen Berhasil Dikirim', 'success'); 
		redirect('administrator/tandatangan/index/0');
	}
	
	public function update(){

		$id = $this->input->post('id');
 
		$this->db->where('id', $id); 
		$this->db->update('m_ttd_terusankirim', ['status' => 1]);
		
		echo json_encode(array("status" => 'success', "id" => $id));
	}
}