<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Verifikasi_dokumen extends Admin	
{
	public function __construct()
	{
		parent::__construct();

	}

	// Fungsi Halaman Verifikasi Dokumen
	public function index()
	{
		$this->load->view('backend/standart/administrator/verifikasi_dokumen/verifikasi_dokumen');
	}
	// Akhir Fungsi Halaman Verifikasi Dokumen
	
	// Fungsi Halaman Cek Verifikasi Dokumen
	public function cek_dong()
	{

			$id_dok = $this->input->post('id_dok');
			$where = array(
				'Id_dokumen' => $id_dok,
			);

			$cek = $this->db->get_where("inbox_files",$where)->num_rows();

			if($cek > 0){

				$datax = $this->db->query("SELECT FileName_fake From inbox_files WHERE Id_dokumen='" . $id_dok . "'")->result();

				foreach ($datax as $row) {

					$files = BASE_URL.'/FilesUploaded/naskah/'.$row->FileName_fake;

					?>
						<embed src='<?= $files; ?>' type='application/pdf' width='100%' height='100%'/>
					<?php		

	 			}
	 
			}else{
				$this->session->set_flashdata('error', 'Kode Dokumen yang dicari tidak terdaftar !');
				// redirect(base_url());
				$this->load->view('backend/standart/administrator/verifikasi_dokumen/verifikasi_dokumen');
			}

	}
	// Akhir Fungsi Halaman Cek Verifikasi Dokumen
	
}