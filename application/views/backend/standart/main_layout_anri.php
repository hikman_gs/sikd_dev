<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <title>Sistem Informasi Kearsipan Dinamis</title>
  <link rel="shortcut icon" href="<?= BASE_ASSET; ?>/favicon.ico">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/morris/morris.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/sweet-alert/sweetalert.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/toastr/build/toastr.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/chosen/chosen.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/custom.css?timestamp=201803311526">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>datetimepicker/jquery.datetimepicker.css" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>js-scroll/style/jquery.jscrollpane.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>flag-icon/css/flag-icon.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" media="all" />
  <?= $this->cc_html->getCssFileTop(); ?>

  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/sweet-alert/sweetalert-dev.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <script src="<?= BASE_ASSET; ?>/toastr/toastr.js"></script>
  <script src="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.js?v=2.1.5"></script>
  <script src="<?= BASE_ASSET; ?>/datetimepicker/build/jquery.datetimepicker.full.js"></script>
  <script src="<?= BASE_ASSET; ?>/editor/dist/js/medium-editor.js"></script>
  <script src="<?= BASE_ASSET; ?>js/cc-extension.js"></script>
  <script src="<?= BASE_ASSET; ?>/js/cc-page-element.js"></script>

  <script>
    var BASE_URL = "<?= base_url(); ?>";
    var HTTP_REFERER = "<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/'; ?>";
    var csrf = '<?= $this->security->get_csrf_token_name(); ?>';
    var token = '<?= $this->security->get_csrf_hash(); ?>';

    $(document).ready(function() {

      toastr.options = {
        "positionClass": "toast-top-center",
      }

      var f_message = '<?= $this->session->flashdata('f_message'); ?>';
      var f_type = '<?= $this->session->flashdata('f_type'); ?>';

      if (f_message.length > 0) {
        toastr[f_type](f_message);
      }

      $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
        checkboxClass: 'icheckbox_minimal-red',
        radioClass: 'iradio_minimal-red'
      });
    });
  </script>
  <?= $this->cc_html->getScriptFileTop(); ?>
</head>

<body class="sidebar-mini skin-blue-light fixed web-body">
  <div class="wrapper">

    <header class="main-header">
      <a href="<?= site_url('administrator/anri_dashboard/'); ?>" class="logo">
        <span class="logo-mini"><b>SIKD</b></span>
        <span class="logo-lg"><b>Sistem Informasi Kearsipan Dinamis</b></span>
      </a>
      <nav class="navbar navbar-static-top">

        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li class="dropdown notifications-menu">
              <?php
              // if ($this->session->userdata('groupid') == 7) {
              //   $notif = $this->db->query("SELECT NId FROM inbox_receiver WHERE RoleId_To='" . $this->session->userdata('roleid') . "' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND StatusReceive = 'unread'")->num_rows();
              // } else {
              //   $notif = $this->db->query("SELECT NId FROM inbox_receiver WHERE RoleId_To='" . $this->session->userdata('roleid') . "' AND StatusReceive = 'unread' AND ReceiverAs NOT IN ('to_draft_keluar','to_draft_nadin','to_draft_notadinas')")->num_rows();
              // }
              ?>
             <!--  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-envelope-o"></i>
                <span class="label label-danger">
                  <font size="2"><?= $notif; ?> </font>
                </span>
              </a> -->
              <!-- <ul class="dropdown-menu"> -->

                <?php
                // if ($this->session->userdata('groupid') == 9) {
                //   $hitung_naskahmasuk = $this->db->query("SELECT NId FROM v_suratmasuk WHERE ReceiverAs in('to','bcc') AND NTipe = 'inbox' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                // } else {
                //   $hitung_naskahmasuk = $this->db->query("SELECT NId FROM v_suratmasuk WHERE ReceiverAs in('to','bcc') AND NTipe = 'inbox' AND StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                // }

                // if ($this->session->userdata('groupid') == 9) {
                //   $hitung_notadinas = $this->db->query("SELECT NId FROM v_tem_notadinas WHERE Status = '0' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                // } else {
                //   $hitung_notadinas = $this->db->query("SELECT NId FROM v_tem_notadinas WHERE Status = '0' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                // }

                // if ($this->session->userdata('groupid') == 9) {
                //   $hitung_disposisi = $this->db->query("SELECT NId FROM v_disposisi WHERE StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ")->num_rows();
                // } else {
                //   $hitung_disposisi = $this->db->query("SELECT NId FROM v_disposisi WHERE StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                // }

                // if ($this->session->userdata('groupid') == 9) {
                //   $hitung_teruskan = $this->db->query("SELECT NId FROM v_tem_teruskan WHERE ReceiverAs ='to_forward' AND StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                // } else {
                //   $hitung_teruskan = $this->db->query("SELECT NId FROM v_tem_teruskan WHERE ReceiverAs ='to_forward' AND StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "' and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                // }

                // if ($this->session->userdata('groupid') == 9) {
                //   $hitung_undangan = $this->db->query("SELECT NId FROM v_tem_undangan WHERE StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                // } else {
                //   $hitung_undangan = $this->db->query("SELECT NId FROM v_tem_undangan WHERE StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "' and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                // }

                // if ($this->session->userdata('groupid') == 9) {
                //   $hitung_tugas = $this->db->query("SELECT NId FROM v_tem_sprint WHERE StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                // } else {
                //   $hitung_tugas = $this->db->query("SELECT NId FROM v_tem_sprint WHERE StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                // }


                // //edit tambah nota
                // if ($this->session->userdata('groupid') == 7) {
                //   $hitung_nota = $this->db->query("SELECT NId FROM v_suratmasuk WHERE  StatusReceive = 'unread' AND Status = '0' AND NTipe = 'outboxnotadinas' AND ReceiverAs ='to' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                // } else {
                //   $hitung_nota = $this->db->query("SELECT NId FROM v_suratmasuk WHERE StatusReceive = 'unread' AND Status = '0' AND NTipe = 'outboxnotadinas' AND ReceiverAs ='to' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                // }

                // if ($this->session->userdata('groupid') == 9) {
                //   $hitung_keluar = $this->db->query("SELECT NId FROM v_tem_suratdinas WHERE StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                // } else {
                //   $hitung_keluar = $this->db->query("SELECT NId FROM v_tem_suratdinas WHERE StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "' and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                // } 
                ?>






                <?php if ($hitung_naskahmasuk > 0) { ?>
                  <!-- <li class="header"><a href="<?= BASE_URL('administrator/anri_list_naskahmasuksemua_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_naskahmasuk; ?></b></font> Naskah Masuk </a></li> -->
                <?php } ?>

                <?php if ($hitung_nota > 0) { ?>
                  <!-- <li class="header"><a href="<?= BASE_URL('administrator/Anri_list_notakeluar_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_nota; ?></b></font> Nota Masuk</a></li> -->
                <?php } ?>

                <?php if ($hitung_notadinas > 0) { ?>
                 <!--  <li class="header"><a href="<?= BASE_URL('administrator/anri_list_notadinas_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_notadinas; ?></b></font> Nota Dinas</a></li> -->
                <?php } ?>

                <?php if ($hitung_disposisi > 0) { ?>
                 <!--  <li class="header"><a href="<?= BASE_URL('administrator/anri_list_disposisi_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_disposisi; ?></b></font> Disposisi</a></li> -->
                <?php } ?>

                <?php if ($hitung_teruskan > 0) { ?>
                <!--   <li class="header"><a href="<?= BASE_URL('administrator/anri_list_teruskan_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_teruskan; ?></b></font> Naskah Teruskan</a></li> -->
                <?php } ?>

                <?php if ($hitung_undangan > 0) { ?>
                 <!--  <li class="header"><a href="<?= BASE_URL('administrator/anri_list_undangan_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_undangan; ?></b></font> Undangan</a> </li> -->
                <?php } ?>

                <?php if ($hitung_tugas > 0) { ?>
                  <!-- <li class="header"><a href="<?= BASE_URL('administrator/anri_list_surattugas_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_tugas; ?></b></font> Surat Tugas</a></li> -->
                <?php } ?>
              <!-- </ul> -->
            </li>

            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?= BASE_URL('uploads/user/default.png'); ?>" class=" user-image" alt="User Image">
                <span class="hidden-xs"><?=  word_limiter($this->session->userdata('peopleposition'), 10);  ?></span> 
              </a>
              <ul class="dropdown-menu">
                <li class="user-header">
                  <img src="<?= BASE_URL('uploads/user/default.png'); ?>" class="img-circle" alt="User Image">
                  <p>
                    <small><?= $this->session->userdata('peoplename'); ?></small> <br><br><br>
                  </p>
                </li>

                <li class="user-footer">
                  <div class="pull-left">
                    <a href="<?= site_url('administrator/anri_dashboard/profile'); ?>" class="btn btn-default btn-flat">Ubah Kata Sandi</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?= site_url('administrator/anri_logout'); ?>" class="btn btn-default btn-flat">Keluar</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <aside class="main-sidebar">
      <section class="sidebar" style="padding-top:0% !important">
        <?php $this->load->view('backend/standart/navigation'); ?>
      </section>
    </aside>

    <div class="content-wrapper">
      <?php cicool()->eventListen('backend_content_top'); ?>
      <?= $template['partials']['content']; ?>
      <?php cicool()->eventListen('backend_content_bottom'); ?>
    </div>

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
      </div>
      <strong<?= date('Y'); ?> <a href="https://anri.go.id/" target="_blank"></a>.</strong>
    </footer>

    <div class="control-sidebar-bg"></div>
  </div>

  <?= $this->cc_html->getHtmlFileBottom(); ?>

  <?= $this->cc_html->getCssFileBottom(); ?>
  <?= $this->cc_html->getScriptFileBottom(); ?>

  <script>
    var AdminLTEOptions = {
      sidebarExpandOnHover: false,
      navbarMenuSlimscroll: false,
    };
  </script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-ZW6ZCJNSMG"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-ZW6ZCJNSMG');
</script>

  <script src="<?= BASE_ASSET; ?>/js/chosen.jquery.min.js" type="text/javascript"></script>
  <script src="<?= BASE_ASSET; ?>jquery-switch-button/jquery.switchButton.js"></script>
  <script src="<?= BASE_ASSET; ?>/js/jquery.ui.touch-punch.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/fastclick/fastclick.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/dist/js/app.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/dist/js/adminlte.js"></script>
  <script src="<?= BASE_ASSET; ?>js-scroll/script/jquery.jscrollpane.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/js/custom.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
  <script>
    $(function() {
      $('#example1').DataTable();
    })
    $(document).ready(function() {
      if ($('#news').length) {
        // if($.cookie('pop') == null) {
        $('#news').modal('toggle');
        //  $.cookie('pop', '1');
        // }
      }
    });
  </script>
</body>

</html>
