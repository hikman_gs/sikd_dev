<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Usul Musnah</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                    <!-- <div class="row pull-right">
                        <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="<?= cclang('add_new_button', ['Master Bahasa']); ?>  (Ctrl+a)" href="<?=  site_url('administrator/anri_dashboard/master_bahasa_add'); ?>"><i class="fa fa-plus-square-o" ></i> <?= cclang('add_new_button', ['Master Bahasa']); ?></a>
                    </div> -->
                    <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Usul Musnah</h3>
                     <h5 class="widget-user-desc">Usul Musnah</h5>
                     <hr>
                  </div>

                  <div class="table-responsive"> 
                          <table id="mytable" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%">
                             <thead>
                                <tr>
                                    <th>Status Berkas</th>
                                    <th>Nomor Berkas</th>
                                    <th>Nama Berkas</th>
                                    <th>Isi Berkas</th>
                                    <th>Unit Kerja</th>
                                    <th>Akhir Retensi Aktif</th>
                                    <th width="50px">Aksi</th>
                                </tr>
                             </thead>

                             <tbody></tbody> 
                          </table>
                        
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
$(document).ready(function() {
    //$('.remove-data').click(function(){
    $("#mytable").on("click", ".remove-data", function () {
      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          // text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });    


      $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
      {
          return {
              "iStart": oSettings._iDisplayStart,
              "iEnd": oSettings.fnDisplayEnd(),
              "iLength": oSettings._iDisplayLength,
              "iTotal": oSettings.fnRecordsTotal(),
              "iFilteredTotal": oSettings.fnRecordsDisplay(),
              "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
              "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
          };
      };
 
      var table = $("#mytable").dataTable({
          initComplete: function() {
              var api = this.api();
              $('#mytable_filter input')
                  .off('.DT')
                  .on('input.DT', function() {
                      api.search(this.value).draw();
              });
          },
              oLanguage: {
              sProcessing: "loading..."
          },
              processing: true,
              serverSide: true,
              ajax: {"url": "<?php echo base_url().'administrator/anri_dashboard/get_data_usul_musnah_json'?>", "type": "POST"},
                    columns: [
                      {"data": "status_berkas"},
                      {"data": "BerkasNumber"},
                      {"data": "BerkasName"},
                      {"data": "BerkasDesc"},
                      {"data": "PeopleName"},
                      {"data": "RetensiValue_Active"},
                      {"data": "view"}
                      /*{
                          data: null,
                          className: "center",
                          defaultContent: '<a href="javascript:void(0);" data-href="<?= site_url('administrator/anri_dashboard/berkas_unit_delete/'. $berkas->BerkasId); ?>" class=" btn-danger btn-sm btn remove-data" title="Hapus"><i class="fa fa-trash" ></i> </a>'
                      }*/
                    ],
                order: [[1, 'asc']],
          rowCallback: function(row, data, iDisplayIndex) {
              var info = this.fnPagingInfo();
              var page = info.iPage;
              var length = info.iLength;
              $('td:eq(0)', row).html();
          }
 
      });
            // end setup datatables
            // get Edit Records
            $('#mytable').on('click','.edit_record',function(){
            var kode=$(this).data('kode');
                        var nama=$(this).data('nama');
                        var harga=$(this).data('harga');
                        var kategori=$(this).data('kategori');
                        $('#ModalUpdate').modal('show');
                        $('[name="title"]').val(title);
                        $('[name="slug"]').val(slug);
      });
            // End Edit Records
            // get Hapus Records
            $('#mytable').on('click','.hapus_record',function(){
            var kode=$(this).data('kode');
            $('#ModalHapus').modal('show');
            $('[name="kode_barang"]').val(kode);
      });
            // End Hapus Records
 
    });
</script>