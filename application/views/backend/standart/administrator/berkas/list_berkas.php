<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Daftar Berkas Unit Kerja</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                        <div class="row pull-right">
                            <a class="btn btn-success" id="btn_add_new" title="Tambah Data" href="<?=  site_url('administrator/anri_berkas_unit/berkas_unit_add'); ?>"><i class="fa fa-plus-square-o" ></i> Tambah Data</a>        
                        </div>                                               
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Daftar Berkas Unit Kerja</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <div class="table-responsive"> 

                  <table id="myTable" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%">                    
                        <thead>
                            <tr>
                              <th>No</th>
                              <th>Status Berkas</th>
                              <th>Kode Klasifikasi</th>
                              <th>Nomor Berkas</th>                              
                              <th>Nama Berkas</th>
                              <th>Unit Kerja</th>
                              <th>Penciptaan Berkas</th>
                              <th>Akhir Retensi Aktif</th>
                              <th width="50px">Aksi</th>
                            </tr>
                        </thead>

                        <tbody></tbody>   

                  </table>
                  </div>

               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->

<script type="text/javascript">
    
    $(document).ready(function() {

        var dataTable =  $('#myTable').DataTable( { 

            "processing": true, 
            "serverSide": true, 
            "order": [[5, "DESC" ]],
            
            "ajax": {
                "url": "<?php echo site_url('administrator/anri_berkas_unit/get_data_berkas')?>",
                "type": "POST",
                //deferloading untuk menampung 10 data pertama dulu
                "deferLoading": 10,
            },

            
            "columnDefs": [
            { 
                "targets": [ 0, 8 ], 
                "orderable": false, 
            },
            ],

        });

   

    });
</script>
