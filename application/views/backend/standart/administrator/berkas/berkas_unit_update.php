<link rel="stylesheet" href="<?= BASE_ASSET; ?>jquery-switch-button/jquery.switchButton.css">

<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/ui.dynatree.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/tree.skinklas.css">

<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.form.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.dynatree.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/dynatree.js"></script>
<section class="content-header">
    <h1>&nbsp;</h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="<?= site_url('administrator/anri_berkas_unit'); ?>"> Daftar Berkas Unit Kerja</a></li>
        <li class="active">Ubah Data</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <h3 class="widget-user-username">Ubah Data Berkas</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>

                      <div class="row">
                        <div class="col-md-12">

                        <?= form_open(BASE_URL('administrator/anri_berkas_unit/berkas_unit_save_update'), [
                            'name'    => 'form_role', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_role', 
                            'enctype' => 'multipart/form-data',
                            'method'  => 'POST'
                            ]); ?>
                        <?php
                        $berkas = $this->db->select('*')->from('berkas')->where('BerkasId',$this->uri->segment(4))->get()->result();
                        foreach ($berkas as $b):
                          $cd = date('Y',strtotime($b->CreationDate));
                          $tahunaktif = explode('/', $b->RetensiTipe_Active);
                          $tahuninaktif = explode('/', $b->RetensiTipe_InActive);
                        ?>
                        <input type="hidden" name="BerkasId" value="<?=$b->BerkasId;?>">
                        <div class="form-group ">
                            <label for="unit" class="col-sm-3">Unit Kerja <i class="required">*</i></label>
                            <div class="col-sm-6">
                                <input type="text" disabled class="form-control" id="unit_kerja" placeholder="Unit Kerja" value="<?= $this->session->userdata('namabagian'); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                          <label for="klasini" class="col-sm-3">Klasifikasi Saat Ini</label>
                           <div class="col-sm-6">
                                <input type="text" class="form-control" name="BerkasKlas" id="BerkasKlas" value="<?= $b->Klasifikasi; ?>" readonly> 
                                <input type="hidden" class="form-control" name="Codeklas" id="Codeklas" value="<?= $b->ClId; ?>">
                                <input type="hidden" class="form-control" name="Nomorberkas" id="Nomorberkas" value="<?= $b->BerkasNumber; ?>">
                           </div>
                        </div>  
                        <div class="form-group">
                          <label for="klas" class="col-sm-3">Klasifikasi <i class="required">*</i></label>
                          <div class="col-md-8" id="treeClassfication">
                            <font color ="red"><b>Klasifikasi tidak dapat dirubah, apabila berkas tersebut telah memiliki isi berkas</b></font>
                            <p></p>
                            <?php  
                                $data = array();
                                foreach ($clasification as $row) {
                                  $data[$row->parent_id][] = $row; 
                                }
                                echo showClassification($data,'0');
                            ?>
                            <input type="hidden" class="form-control" name="ClId" id="ClId" placeholder="ClId" value="<?= $b->ClId; ?>">
                          </div>
                        </div>
                        <div class="form-group ">
                            <label for="judulberkas" class="col-sm-3">Judul Berkas <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="BerkasName" id="BerkasName" value="<?= $b->BerkasName; ?>" placeholder="Judul Berkas" required>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="judulberkas" class="col-sm-3">Hitung Waktu Retensi Dari <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                               <select class="form-control" name="HitungId" id="HitungId" required>
                                  <?php
                                  $hitungdulu = $this->db->query("SELECT Mulai, Ket FROM master_hitung")->result();
                                  foreach ($hitungdulu as $x):
                                  ?>
                                       <option <?= $b->BerkasCountSince == $x->Mulai ? 'selected' : ''; ?>  value="<?= $x->Mulai; ?>"><?= $x->Ket; ?></option>                               
                                  <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="retensiarsip" class="col-sm-3">Retensi Arsip <i class="required">*</i>
                            </label>
                            <div class="col-sm-3">                               
                                <input type="number" min="0" name="RetensiValue_Active" id="RetensiThn_Active" class="form-control" placeholder="Dari" value="<?= $tahunaktif[0]; ?>" required>
                            </div>
                            <div class="col-sm-3">
                                <input type="number" min="0" name="RetensiValue_InActive" id="RetensiThn_InActive" value="<?= $tahuninaktif[0]; ?>" class="form-control" placeholder="Sampai" required>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="judulberkas" class="col-sm-3">Tindakan Penyusutan Akhir <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                               <select class="form-control" name="SusutId" id="SusutId" required>
                                  <?php
                                  $susut = $this->db->query("SELECT SusutId, SusutName FROM master_penyusutan ORDER BY SusutName")->result();
                                  foreach ($susut as $s):
                                  ?>
                                       <option <?= $b->SusutId == $s->SusutId ? 'selected' : ''; ?>  value="<?= $s->SusutId; ?>"><?= $s->SusutName; ?></option>                                 
                                  <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="judulberkas" class="col-sm-3">Lokasi Fisik Berkas </label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="BerkasLokasi" id="BerkasLokasi" value="<?=$b->BerkasLokasi;?>" placeholder="Lokasi Fisik Berkas">
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="judulberkas" class="col-sm-3">Uraian Informasi </label>
                            <div class="col-sm-6">
                              <textarea name="BerkasDesc" class="form-control" id="" cols="30" rows="10"><?=$b->BerkasDesc;?></textarea>
                            </div>
                        </div>
                        <div class="form-group ">
                          <div class="col-sm-12">
                            <input type="submit" name="submit" class="btn btn-flat btn-danger btn_save btn_action" value="Simpan">
                          </div>
                        </div>
                        <?php endforeach;?>
                        <?= form_close();?>
                        
                        </div>
                      </div>
                        
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
