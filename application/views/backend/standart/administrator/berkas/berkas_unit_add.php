<link rel="stylesheet" href="<?= BASE_ASSET; ?>jquery-switch-button/jquery.switchButton.css">

<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/ui.dynatree.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/tree.skinklas.css">

<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.form.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.dynatree.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/dynatree.js"></script>
<section class="content-header">
    <h1>&nbsp;</h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li><a href="<?= site_url('administrator/anri_berkas_unit'); ?>"> Daftar Berkas Unit Kerja</a></li>
        <li class="active">Tambah Data</li>
    </ol>
</section>
<!-- Main content -->

<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Tambah Data Berkas</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>

                        <?= form_open(BASE_URL('administrator/anri_berkas_unit/berkas_unit_save'), [
                                'name'    => 'form_role', 
                                'class'   => 'form-horizontal', 
                                'id'      => 'form_role', 
                                'enctype' => 'multipart/form-data',
                                'method'  => 'POST'
                                ]); ?>
                            <div class="form-group ">
                                <label for="unitkerja" class="col-sm-4">Unit Kerja <i class="required">*</i></label>
                                <div class="col-sm-6">
                                    <input type="text" disabled class="form-control" id="unit_kerja" placeholder="Unit Kerja" value="<?= $this->session->userdata('namabagian'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                              <label for="klasifikasi" class="col-sm-4">Klasifikasi <i class="required">*</i></label>
                              <div class="col-md-6" id="treeClassfication">
                                <?php  
                                    $data = array();
                                    foreach ($clasification as $row) {
                                      $data[$row->parent_id][] = $row; 
                                    }
                                    echo showClassification($data,'0');
                                    
                                    $x = set_value('ClId');
                                ?>
                                <input type="hidden" class="form-control" name="ClId" id="ClId" placeholder="ClId" value="<?= $x; ?>">
                                    
                              </div>
                            </div>
                            <div class="form-group ">
                                <label for="judulberkas" class="col-sm-4">Judul Berkas <i class="required">*</i>
                                </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="BerkasName" id="BerkasName" placeholder="Judul Berkas" required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="judulberkas" class="col-sm-4">Hitung Waktu Retensi Dari <i class="required">*</i>
                                </label>
                                <div class="col-sm-6">
                                    <select name="HitungId" id="HitungId" class="form-control select2" required>
                                      <?php foreach ($this->db->query("SELECT Mulai, Ket FROM master_hitung")->result() as $v) { ?>
                                      <option value="<?= $v->Mulai ?>"><?= $v->Ket ?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="retensiarsip" class="col-sm-4">Retensi Arsip <i class="required">*</i>
                                </label>
                                <div class="col-sm-3">
                                    <input type="number" name="RetensiValue_Active" id="RetensiThn_Active" class="form-control" placeholder="Dari" required>
                                </div>
                                <div class="col-sm-3">
                                    <input type="number" name="RetensiValue_InActive" id="RetensiThn_InActive" class="form-control" placeholder="Sampai" required>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="judulberkas" class="col-sm-4">Tindakan Penyusutan Akhir <i class="required">*</i>
                                </label>
                                <div class="col-sm-6">
                                    <select name="SusutId" id="" class="form-control select2" required>
                                      <?php foreach ($this->db->query("SELECT SusutId, SusutName FROM master_penyusutan")->result() as $v) { ?>
                                      <option value="<?= $v->SusutId ?>"><?= $v->SusutName ?></option>
                                      <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="judulberkas" class="col-sm-4">Lokasi Fisik Berkas </label>
                                <div class="col-sm-6">
                                    <input type="text" class="form-control" name="BerkasLokasi" id="BerkasLokasi" placeholder="Lokasi Fisik Berkas">
                                </div>
                            </div>
                            <div class="form-group ">
                                <label for="judulberkas" class="col-sm-4">Uraian Informasi </label>
                                <div class="col-sm-6">
                                  <textarea name="BerkasDesc" class="form-control" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="form-group ">
                              <div class="col-sm-12">
                                <input type="submit" name="submit" class="btn btn-flat btn-danger btn_save btn_action" value="Simpan"><p></p>
                              </div>
                            </div>
                        <?= form_close();?>
                    </div>
                </div>
                <!--/box body -->
            </div>
        </div>
    </div>
</section>
