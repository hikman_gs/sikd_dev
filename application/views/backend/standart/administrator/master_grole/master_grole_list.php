<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Grup Unit Kerja</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                        <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="Tambah Data" href="<?=  site_url('administrator/anri_master_grole/add'); ?>"><i class="fa fa-plus-square-o" ></i> Tambah Data</a>
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Pengaturan Grup Unit Kerja</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <div class="table-responsive"> 
                  <table class="table table-bordered table-striped table-hover dataTable" id="example">
                     <thead>
                        <tr class="">
                           <th>
                            <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                           </th>
                           <th>Grup Unit Kerja</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody id="tbody_master_grole">
                     <?php 
                     $master_groles = $this->db->get('master_grole')->result();
                     foreach($master_groles as $master_grole): ?>
                        <tr>
                           <td width="5">
                              <input type="checkbox" class="flat-red check" name="id[]" value="<?= $master_grole->GRoleId; ?>">
                           </td>
                           
                           <td><?= _ent($master_grole->GRoleName); ?></td> 
                           <td width="200">
                              <a title="Ubah Data" href="<?= site_url('administrator/anri_master_grole/update/' . $master_grole->GRoleId); ?>" class="btn btn-sm btn-primary"><i class="fa fa-edit "></i></a>
                              <?php
                              if (cek_data('role','GRoleId',$master_grole->GRoleId) == '0') {
                              ?>
                              <a title="Hapus Data" href="javascript:void(0);" data-href="<?= site_url('administrator/anri_master_grole/delete/' . $master_grole->GRoleId); ?>" class="btn btn-sm btn-danger remove-data" ><i class="fa fa-trash"></i></a>
                              <?php  
                              }
                              ?>                           
                           </td>
                        </tr>
                      <?php endforeach; ?>
                     </tbody>
                  </table>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
  $(document).ready(function(){

    $('#example').DataTable( {
        "order": [[ 1, "asc" ]]
    } );
       
  }); /*end doc ready*/     
  
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          // text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });

</script>