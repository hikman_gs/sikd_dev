<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class=""><a  href="<?= site_url('administrator/anri_dashboard/list_naskah_masuk'); ?>">Naskah Masuk</a></li>
      <li class="active">Detail</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Detail</h3>
                     <h5 class="widget-user-desc">Naskah Masuk</h5>
                     <hr>
                  </div>
                  <ul class="menu-baru">
                    <?php if($data_inbox->Status != 1){ ?>
                    <li><i class="fa fa-envelope"></i> <a href="#" data-toggle="modal" data-target="#teruskan"> Teruskan</a></li>
                    <!-- <li><i class="fa fa-envelope"></i> <a href="#" data-toggle="modal" data-target="#notadinas"> Nota Dinas</a></li> -->
                    <li><i class="fa fa-envelope"></i> <a href="#" data-toggle="modal" data-target="#naskahdinas"> Nota Dinas</a></li>
                    <li><i class="fa fa-newspaper-o"></i> <a href="#" data-toggle="modal" data-target="#disposisi">  Disposisi</a></li>
                    <li><i class="fa fa-edit"></i> <a href="<?= site_url('administrator/anri_dashboard/edit_naskah_masuk/view/' . $NId); ?>"> Ubah Metadata</a></li>
                    <li><i class="fa fa-check"></i> <a href="<?= site_url('administrator/anri_dashboard/selesai_tindaklanjut/view/' . $NId); ?>"> Selesai Tindak Lanjut</a></li>
                    <?php } ?>
                  </ul>
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="<?= site_url('administrator/anri_dashboard/view_naskah_masuk/' . $NId); ?>">Lihat Naskah</a></li>
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_naskah_histori/view/' . $NId); ?>">Histori Naskah</a></li>
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_naskah_metadata/view/' . $NId); ?>">Metadata</a></li>
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_naskah_dinas/view/' . $NId); ?>">Approve Nota Dinas</a></li>
                  </ul>
                  <div class="tab-content container">
                    <div class="tab-pane fade in active">
                      <br>
                      <div class="form-horizontal">
                        <div class="form-group">
                          <div class="col-sm-4">
                            <select name="berkas" class="form-control berkas">
                              <option value="">Pilih Naskah</option>
                              <?php foreach ($berkas as $k => $v) { ?>
                                <option data-folder="<?= $path; ?>" data-ext="<?= explode('.',$v->FileName_fake)[1]; ?>" value="<?= $v->FileName_fake; ?>"><?= $v->FileName_fake; ?></option>
                              <?php }  ?>
                            </select>
                          </div>
                          <div class="col-sm-4 body-button">
                          </div>
                        </div>
                      </div>
                      <div class="body-berkas"></div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php $this->load->view('backend/standart/partials/modal-inbox'); ?>
</section>
<!-- /.content -->