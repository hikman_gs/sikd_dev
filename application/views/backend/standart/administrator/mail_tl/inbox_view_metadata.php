<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li>
        <a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a>
      </li>

      <li>
        <?php 

          $x = $_GET['dt'];
          if($x == '1') { ?>

          <a  href="<?= site_url('administrator/anri_list_naskah_masuk'); ?>">Daftar Semua Naskah Masuk</a>

        <?php } elseif($x == '2') { ?>

          <a  href="<?= site_url('administrator/anri_list_naskahmasuk_btl'); ?>">Daftar Naskah Masuk Belum Tindaklanjut</a>

        <?php } elseif($x == '3') { ?>

          <a  href="<?= site_url('administrator/anri_list_disposisi_btl'); ?>">Daftar Disposisi Belum Tindaklanjut</a>

        <?php } elseif($x == '4') { ?>

          <a  href="<?= site_url('administrator/anri_list_disposisi'); ?>">Daftar Semua Disposisi</a>

        <?php } elseif($x == '5') { ?>

          <a  href="<?= site_url('administrator/anri_list_teruskan_btl'); ?>">Daftar Naskah Teruskan Belum Tindaklanjut</a>

        <?php } elseif($x == '6') { ?>

          <a  href="<?= site_url('administrator/anri_list_teruskan'); ?>">Daftar Semua Naskah Teruskan</a>

        <?php } elseif($x == '7') { ?>

          <a  href="<?= site_url('administrator/anri_list_notadinas_btl_ap'); ?>">Daftar Nota Dinas Belum Disetujui</a>

        <?php } elseif($x == '8') { ?>

          <a  href="<?= site_url('administrator/anri_list_notadinas_sdh_ap'); ?>">Daftar Nota Dinas Belum Dikirim</a>

        <?php } elseif($x == '9') { ?>

          <a  href="<?= site_url('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId .'?dt=9'); ?>">Detail Naskah</a>

        <?php } elseif($x == '10') { ?>

          <a  href="<?= site_url('administrator/anri_list_notadinas_btl'); ?>">Daftar Nota Dinas Belum Tindaklanjut</a>

        <?php } elseif($x == '11') { ?>

          <a  href="<?= site_url('administrator/anri_list_notadinas'); ?>">Daftar Semua Nota Dinas</a>

        <?php } elseif($x == '12') { ?>

          <a  href="<?= site_url('administrator/anri_list_undangan_btl'); ?>">Daftar Undangan Belum Tindaklanjut</a>

        <?php } elseif($x == '13') { ?>

          <a  href="<?= site_url('administrator/anri_list_undangan'); ?>">Daftar Semua Undangan</a>

        <?php } elseif($x == '14') { ?>

          <a  href="<?= site_url('administrator/anri_list_surattugas_btl'); ?>">Daftar Surat Tugas Belum Tindaklanjut</a>

        <?php } elseif($x == '15') { ?>

          <a  href="<?= site_url('administrator/anri_list_surattugas'); ?>">Daftar Semua Surat Tugas</a>

        <?php } elseif($x == '16') { ?>

          <a  href="<?= site_url('administrator/anri_list_suratdinas_keluar_btl'); ?>">Daftar Surat Dinas Belum Tindaklanjut</a>

        <?php } elseif($x == '17') { ?>

          <a  href="<?= site_url('administrator/anri_list_suratdinas_keluar'); ?>">Daftar Semua Surat Dinas</a>

        <?php } elseif($x == '18') { ?>

          <a  href="<?= site_url('administrator/anri_list_naskahdinas_lain_btl'); ?>">Daftar Naskah Dinas Lainnya Belum Tindaklanjut</a>

        <?php } elseif($x == '19') { ?>

          <a  href="<?= site_url('administrator/anri_list_naskahdinas_lain'); ?>">Daftar Semua Naskah Dinas Lainnya</a>

        <?php } else { ?>

          <a  href="<?= site_url('administrator/anri_list_naskah_masuk'); ?>"><?= $x; ?></a>

        <?php } ?>
      </li> 

      <li class="active">Detail</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Detail</h3>
                     <h5 class="widget-user-desc">Naskah Masuk</h5>
                     <hr>
                  </div>
                  <ul class="menu-baru">
                    <?php if($data_inbox->Status != 1){ ?>
                    <li><i class="fa fa-envelope"></i> <a href="#" data-toggle="modal" data-target="#teruskan"> Teruskan</a></li>
                    <li><i class="fa fa-envelope"></i> <a href="#" data-toggle="modal" data-target="#naskahdinas"> Nota Dinas</a></li>
                    <li><i class="fa fa-newspaper-o"></i> <a href="#" data-toggle="modal" data-target="#disposisi">  Disposisi</a></li>
                    
                      <?php 
                          $xx1 = $this->db->query("SELECT Pengirim FROM inbox WHERE NId = '".$NId."'")->row();

                          //Jika Sumber Naskah Dinas inisiatif yang diterima, maka ubah metadata dihilangkan. Karena jika aktif user dapat merubah tanggal, nomor, sifata naskah yang tertanam didalam naskahnya
                          if($xx1->Pengirim != 'internal') { 
                       ?>

                          <li><i class="fa fa-edit"></i> <a href="<?= site_url('administrator/anri_reg_naskah_masuk/edit_naskah_masuk/view/' . $NId .'?dt='.$x); ?>"> Ubah Metadata</a></li>

                      <?php } ?>

                    <li><i class="fa fa-check"></i> <a href="<?= site_url('administrator/anri_mail_tl/selesai_tindaklanjut/view/' . $NId .'?dt='.$x); ?>"> Selesai Tindak Lanjut</a></li>
                    <?php } ?>
                  </ul>
                  <ul class="nav nav-tabs">
                    <li><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId .'?dt='.$x); ?>">Histori Naskah</a></li>
                    <li class="active"><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_metadata/view/' . $NId .'?dt='.$x); ?>">Metadata</a></li>
                    <li><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_dinas/view/' . $NId .'?dt='.$x); ?>">Approve Nota Dinas</a></li>
                  </ul>
                  <div class="tab-content container">
                    <div class="tab-pane fade in active">
                      <br>
                      <div class="form-horizontal">
                        <div class="form-group ">
                            <label for="NTglReg" class="col-sm-2 control-label">Tanggal Registrasi</label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right" disabled name="NTglReg"  id="NTglReg" value="<?= date('d-m-Y',strtotime($inbox->NTglReg)); ?>">
                                </div>
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Naskah</label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-12">
                                    <select name="JenisId" disabled class="form-control" id="">
                                        <?php
                                            $query = $this->db->query("SELECT * FROM master_jnaskah")->result();
                                            foreach ($query as $row) {
                                        ?>
                                            <option <?= ($inbox->JenisId == $row->JenisId ? 'selected' : '' ) ?> value="<?= $row->JenisId; ?>"><?= $row->JenisName; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Tanggal Naskah</label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-12">
                                    <input type="text" disabled class="form-control pull-right" name="Tgl"  id="Tgl" value="<?= date('d-m-Y',strtotime($inbox->Tgl)) ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Nomor" class="col-sm-2 control-label">Nomor Asal Naskah
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" disabled name="Nomor" id="Nomor" placeholder="Nomor" value="<?= $inbox->Nomor ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Nomor Agenda</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" disabled name="NAgenda" id="NAgenda" placeholder="Nomor Agenda" value="<?= $inbox->NAgenda ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Hal" class="col-sm-2 control-label">Hal 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" disabled name="Hal" id="Hal" placeholder="Hal" value="<?= $inbox->Hal ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Namapengirim" class="col-sm-2 control-label">Asal Naskah 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" disabled name="Instansipengirim" id="Instansipengirim" placeholder="Instansi Pengirim" value="<?= $inbox->Instansipengirim ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="UrgensiId" class="col-sm-2 control-label">Tingkat Urgensi </label>
                            <div class="col-sm-8">
                                <select name="UrgensiId" disabled class="form-control select2" id="UrgensiId">
                                    <?php
                                        $urg = $this->db->query("SELECT * FROM master_urgensi")->result();
                                        foreach ($urg as $data_urg){
                                    ?>
                                        <option <?= ($data_urg->UrgensiId == $inbox->UrgensiId ? 'selected' : '') ?> value="<?= $data_urg->UrgensiId; ?>"><?= $data_urg->UrgensiName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="SifatId" class="col-sm-2 control-label">Sifat Naskah </label>
                            <div class="col-sm-8">
                                <select name="SifatId" disabled class="form-control select2" id="SifatId">
                                    <?php
                                        $sifat = $this->db->query("SELECT * FROM master_sifat")->result();
                                        foreach ($sifat as $data_sifat){
                                    ?>
                                        <option <?= ($data_sifat->SifatId == $inbox->SifatId ? 'selected' : '') ?> value="<?= $data_sifat->SifatId; ?>"><?= $data_sifat->SifatName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php $this->load->view('backend/standart/partials/modal-inbox'); ?>
</section>
<!-- /.content -->