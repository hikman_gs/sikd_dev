<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">Pencatatan Naskah Tanpa Tindaklanjut</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Registrasi</h3>
                            <h5 class="widget-user-desc">Naskah Tanpa Tindaklanjut</h5>
                            <hr>
                        </div>
                        <?= form_open(BASE_URL('administrator/anri_dashboard/naskah_keluar_add_save')); ?>
                        <div class="form-group row">
                            <div for="tanggal_registrasi" class="col-sm-3">Tanggal Registrasi</div>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" name="tanggal_registrasi" value="<?=date("d/m/Y")?>" readonly>
                            </div>
                        </div>
						
						<div class="form-group row">
                            <div for="jenis_naskah" class="col-sm-3">Jenis Naskah</div>
                            <div class="col-sm-6">
                                <select class="form-control" name="jenis_naskah">
                                    <?php
                                    $jenis_naskah = $this->db->query("select JenisId, JenisName from master_jnaskah order by JenisName")->result();
                                    foreach ($jenis_naskah as $jn) {
                                    ?>
                                    <option value="<?=$jn->JenisId;?>"><?=$jn->JenisName;?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div for="tanggal_naskah" class="col-sm-3">Tanggal Naskah</div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control datepicker" name="tanggal_naskah" id="tanggal_naskah" placeholder="tanggal_naskah" value="<?= set_value('tanggal_naskah'); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div for="nomor_naskah" class="col-sm-3">Nomor Naskah Unit Kerja</div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="nomor_naskah" id="nomor_naskah" placeholder="nomor_naskah" value="<?= set_value('nomor_naskah'); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div for="nomor_agenda" class="col-sm-3">Nomor Agenda</div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="nomor_agenda" id="nomor_agenda" placeholder="nomor_agenda" value="<?= set_value('nomor_agenda'); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div for="hal" class="col-sm-3">Hal</div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="hal" id="hal" placeholder="hal" value="<?= set_value('hal'); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div for="tujuan_naskah" class="col-sm-3">Tujuan Naskah Keluar</div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="tujuan_naskah" id="tujuan_naskah" placeholder="tujuan_naskah" value="<?= set_value('tujuan_naskah'); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div for="berkaskan" class="col-sm-3">Berkaskan</div>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="berkaskan" id="berkaskan" placeholder="berkaskan" value="<?= set_value('berkaskan'); ?>">
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                           <input type="submit" class="btn btn-success btn-flat" value="<?= cclang('save_button'); ?>" >
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
