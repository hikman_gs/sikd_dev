<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<style>
   .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active"><?= $title; ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" > 
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Daftar Pencatatan Naskah Teruskan Untuk Disetujui</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
                  
                  <div class="table-responsive"> 
                    
                  <table id="myTable" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%"> 

                     <thead>
                        <tr>
                           <th>No</th>
                           <th>Jenis Naskah</th>
                           <th>Tanggal Diteruskan</th>                          
                           <th>Hal</th>
                           <th>Diteruskan Kepada</th>
                           <th width="8%">Lampiran</th>
                           <th width="12%">Aksi</th>
                        </tr>
                     </thead>

                     <tbody></tbody>

                  </table>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>

<!-- /.content -->
<!-- Page script -->
<script type="text/javascript">
    
    $(document).ready(function() {

        var dataTable =  $('#myTable').DataTable( { 

            "processing": true, 
            "serverSide": true, 
            "order": [[2, "desc" ]],
            
            "ajax": {
                "url": "<?php echo site_url('administrator/anri_list_naskah_teruskan_approve/get_data_naskah_bs')?>",
                "type": "POST",
                //deferloading untuk menampung 10 data pertama dulu
                "deferLoading": 10,
            },

            
            "columnDefs": [
            { 
                "targets": [ 0, 6 ], 
                "orderable": false, 
            },
            ],

        });   

    });
</script>