<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>

<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />

<style>

#approve .modal-content{
    /* new custom width */
    height: 220px;
}

#k_teruskan .modal-content{
    /* new custom width */
    width: 520px;
}

.select2-container--default .select2-selection--single{border:0px;}
.select2-container{
      border: 1px solid #DEDCDC!important;
      border-radius: 5px!important;
      padding: 2px 0px!important;
}
.chosen-container-single,.chosen-container-multi{width: 100%!important;}

</style>

<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active"><?= $title; ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" > 
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
 
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Daftar Penomoran Naskah Dinas Unit Kearsipan</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <div class="table-responsive"> 
                    
                  <table id="myTable" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%">   

                     <thead>
                        <tr>
                           <th width="5%">No</th>
                           <th width="10%">Tanggal Pencatatan</th>
                           <th width="15%">Jenis Naskah</th>
                           <th width="20%">Hal</th>
                           <th width="5%">Lampiran</th>
                           <th width="10%">#</th>
                           <th width="7%">Aksi</th>
                        </tr>
                     </thead>

                     <tbody></tbody>

                  </table>
                </div>
               </div>
            </div>
         </div>
      </div>
   </div>

    <div id="approve" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Verifikasi Akun</b></h4>
                </div>
                <form action="<?= base_url('administrator/Anri_list_naskah_uk_belum_nomerdispu/approve_naskah'); ?>" class="form-horizontal" method="POST">
                    <div class="modal-body">
                        <div class="content">
                            <div class="form-group">
                                <input type="hidden" id="NId_Temp" name="NId_Temp">
                                <input type="hidden" id="GIR_Id" name="GIR_Id">
                                <div class="col-6">
                                    <label>Masukan Kode Verifikasi</label>
                                    <input type="password" min="1" class="form-control" name="password" id="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Setujui </button>
                                </div>   
                            </div>                             
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div id="k_teruskan" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Teruskan Untuk Ditandatangani</b></h4>
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/k_teruskan'); ?>" class="form-horizontal" method="POST">

                    <div class="modal-body">
                        <div class="content">

                          <div class="form-group">
                            <input type="hidden" id="NId_Temp2" name="NId_Temp2">
                            <input type="hidden" id="GIR_Id2" name="GIR_Id2">

                            <div class="col-6">

                              <label>Penandatangan <sup class="text-danger">*</sup></label>
                              <select name="TtdText" id="TtdText" class="form-control select2 TtdText" required>
                                  <?php foreach ($this->db->query('SELECT * FROM master_ttd')->result() as $data1) { ?>
                                  <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                                  <?php } ?>
                              </select>
                              
                              <?php
                                  $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId LIKE '".$this->session->userdata('roleatasan')."'")->result();
                                  foreach ($AtasanId as $dataz) {
                                    $xx1 = $dataz->PeopleId;
                                  }
                              ?>
                              <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $xx1; ?>">  
                            </div>                              
                          </div>

                          <div class="form-group"> 
                            <div class="col-6 Approve_People hidden">
                                <select name="Approve_People" id="Approve_People" class="form-control select2">
                                    <!-- Perbaikan Eko 09-11-2019 -->
                                    <?php
                                          $query = $this->db->query("SELECT PeopleId, PeoplePosition FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                                          foreach ($query as $dat_people) {
                                      ?>
                                    <option value="<?= $dat_people->PeopleId; ?>">
                                        <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                          }
                                      ?>
                                    <!-- batas akhir perbaikan -->
                                </select>
                            </div> 
                          </div>      

                          <div class="form-group">
                              <div class="col-6">
                                <label>Pesan <sup class="text-danger">*</sup></label>
                                  <input type="text" class="form-control" name="pesan" id="pesan" required>
                              </div>   
                          </div>   

                          <div class="form-group">
                              <div class="col-6"></div>   
                          </div>  

                          <div class="form-group">
                              <div class="col-6">
                                  <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan dan Kirim </button>
                              </div>   
                          </div>   

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</section>
<!-- /.content -->

<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script type="text/javascript">

  $(' .TtdText').change(function() {
      if ($(this).find(':selected').val() == 'none') {
          $('.Approve_People').addClass('hidden');
      } else {
          $('.Approve_People').removeClass('hidden');
          r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
          $('#Approve_People').select2();
          $('#Approve_People').attr('disabled', false);
          if (this.value == "AL") {
              idatasan = $("#tmpid_atasan").val();
              $('#Approve_People').val(idatasan).change();
              $('#Approve_People').attr('disabled', true);
          }
      }
  });

  $(document).ready(function() {

      var dataTable =  $('#myTable').DataTable( { 

          "processing": true, 
          "serverSide": true, 
          "order": [[1, "desc" ]],
          
          "ajax": {
              "url": "<?php echo site_url('administrator/anri_list_naskah_uk_belum_nomerdispu/get_data_naskah_ba')?>",
              "type": "POST",
              //deferloading untuk menampung 10 data pertama dulu
              "deferLoading": 10,
          },

          
          "columnDefs": [
          { 
              "targets": [ 0, 5, 6 ], 
              "orderable": false, 
          },
          ],

      });   

  });

function ko_verifikasi(NId, GIR_Id) {
    $("#NId_Temp").val(NId);
    $("#GIR_Id").val(GIR_Id);
    $("#approve").modal('show');
}

function ko_teruskan(NId, GIR_Id) {
    $("#NId_Temp2").val(NId);
    $("#GIR_Id2").val(GIR_Id);
    $("#k_teruskan").modal('show');
}

</script>