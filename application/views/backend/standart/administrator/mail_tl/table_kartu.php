<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title><?= get_option('site_name'); ?></title>
  <link rel="shortcut icon" href="<?= BASE_ASSET; ?>/favicon.ico">

  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/morris/morris.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/sweet-alert/sweetalert.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/toastr/build/toastr.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/chosen/chosen.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/custom.css?timestamp=201803311526">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>datetimepicker/jquery.datetimepicker.css"/>
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>js-scroll/style/jquery.jscrollpane.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>flag-icon/css/flag-icon.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" media="all" />
  
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/sweet-alert/sweetalert-dev.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <script src="<?= BASE_ASSET; ?>/toastr/toastr.js"></script>
  <script src="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.js?v=2.1.5"></script>
  <script src="<?= BASE_ASSET; ?>/datetimepicker/build/jquery.datetimepicker.full.js"></script>
  <script src="<?= BASE_ASSET; ?>/editor/dist/js/medium-editor.js"></script>
  <script src="<?= BASE_ASSET; ?>js/cc-extension.js"></script>
  <script src="<?= BASE_ASSET; ?>/js/cc-page-element.js"></script>
</head>
<body style="padding: 10px;">
	<h3 align="center"><b>PEMERINTAH PROVINSI JAWA BARAT</b></h3>
	<h3 align="center"><b>SEKRETARIAT DAERAH</b></h3>
	<h3 class="widget-user-username" align="center"><font color="red"><b>KARTU KENDALI PEMBERKASAN</b></font></h3>
  <hr>
  <div class="table-responsive"> 
    <table id="example1" class="table table-bordered table-striped dataTable">
       <thead class="">
		<?php 
        $this->db->select('*');
        $this->db->group_by('GIR_Id'); 
        $this->db->where('NId', $NId); 
        $this->db->order_by('ReceiveDate','DESC');
        $inboxs = $this->db->get('inbox_receiver')->result();
       	foreach($inboxs as $k => $inbox); ?>  
		 
          <tr class="">
             <th width="10%" style="vertical-align:middle" >Indexs</th>
             <td width="1%" style="vertical-align: middle">:</td>
			 <td style="vertical-align:middle"><?= $k+1; ?></td>
             <th width="10%" style="text-align: center">Kode Klasifikasi</th>
			 <td width="1%" style="vertical-align: middle">:</td>
             <td style="vertical-align:middle">Dalam Perbaikan</td>
             <th width="10%" style="vertical-align: middle">No. Agenda </th>
			 <td width="1%" style="vertical-align: middle">:</td>
             <td style="vertical-align: middle"><?= $agenda = $this->db->query("Select NAgenda from inbox where NId='".$NId."'")->row()->NAgenda; ?></td>
          </tr>
         <tr>
            <th width="10%">Hal</th>
            <td width="1%">:</td>
            <td colspan="7"><?= $hal = $this->db->query("Select hal from inbox where NId='".$NId."'")->row()->hal; ?></td>
          </tr>
          <tr>
            <th width="10%">Dari</th>
            <td width="1%">:</td>
            <td colspan="7"><?php 
                if ($inbox->ReceiverAs == 'to') {
                  echo $this->db->query("SELECT Instansipengirim FROM inbox WHERE NId='".$NId."'")->row()->Instansipengirim;
                }else{
                  echo $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$inbox->RoleId_From."'")->row()->RoleName;
                }
                ?></td>
          </tr>
		   <tr class="">
             <th width="10%">Tanggal Surat</th>
             <td width="1%">:</td>
			 <td width="20%"><?= $tgl = $this->db->query("Select tgl from inbox where NId='".$NId."'")->row()->tgl; ?></td>
             <th width="10%">Nomer Surat</th>
			 <td width="1%">:</td>
             <td width="20%"><?= $nomor = $this->db->query("Select nomor from inbox where NId='".$NId."'")->row()->nomor; ?></td>
             <th width="10%">Lampiran</th>
			 <td width="1%">:</td>
             <td width="20%"><?= $lampiran = $this->db->query("Select nomor from inbox where NId='".$NId."'")->row()->nomor; ?></td>
          </tr>
		   <tr class="">
             <th width="10%" style="vertical-align: middle">Pengolah</th>
             <td width="1%" style="vertical-align: middle">:</td>
			 <td style="vertical-align: middle">
				 <?= $penerima = $this->db->query("Select to_id_desc from inbox_receiver where NId='".$NId."'")->row()->to_id_desc; ?>
			   </td>
             <th width="10%" style="text-align: center">Tanggal di Teruskan</th>
			 <td width="1%" style="vertical-align: middle">:</td>
             <td style="vertical-align: middle"><?= date('d-m-Y H:i:s',strtotime($inbox->ReceiveDate)) ?></td>
             <th width="10%" style="vertical-align: middle">Tanda Terima</th>
			 <td width="1%" style="vertical-align: middle">:</td>
             <td></td>
          </tr>
          <tr>
            <th >Catatan</th>
            <td width="1%">:</td>
            <td colspan="7">Naskah di berkaskan di ...</td>
          </tr>
       </thead>
      
       </tbody>
    </table>
  </div>

</script>
</body>
</html>