<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class=""><a  href="<?= site_url('administrator/anri_naskah_sudah_berkas'); ?>">Daftar Naskah Sudah Diberkaskan</a></li>
      <li class="active">Pemberkasan Naskah</li>
   </ol>
</section>
<style>
    .select2-container--default .select2-selection--single{border:0px;}
    .select2-container{
            border: 1px solid #DEDCDC!important;
            border-radius: 5px!important;
            padding: 2px 0px!important;
    }
</style>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Ubah Pemberkasan Naskah</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
                  <div class="tab-content">
                    <div class="tab-pane fade in active">
                      <br>
                      <div class="table-responsive"> 
                          <?php
                          $inboxs  = $this->db->get_where('inbox',array('NId'=>$this->uri->segment(5)))->result();
                          foreach ($inboxs as $row):
                          ?>
                      <?= form_open('', [
                                    'name'    => 'form_group', 
                                    'class'   => 'form-horizontal', 
                                    'id'      => 'form_group', 
                                    'enctype' => 'multipart/form-data', 
                                    'method'  => 'POST'
                        ]); ?>

                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="col-md-2">Nomor</div>
                            <div class="col-md-10"><input type="text" class="form-control" value="<?=$row->Nomor?>" disabled></div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="col-md-2">Hal</div>
                            <div class="col-md-10"><input type="text" class="form-control" value="<?=$row->Hal?>" disabled></div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="col-md-2">Asal Naskah</div>
                            <div class="col-md-10">
                              <?php
                              if($row->NTipe == 'outbox') {
                              ?>  
                                  <input type="text" class="form-control" value="<?=$this->db->query("SELECT RoleName FROM role WHERE RoleId='".$row->Instansipengirim."'")->row()->RoleName;?>" disabled>
                              <?php
                              } else {
                              ?>
                                  <input type="text" class="form-control" value="<?=$row->Instansipengirim?>" disabled>
                              <?php    
                              }
                              ?>
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="col-md-2">Nama Berkas</div>
                            <div class="col-md-10">

                              <?php

                              $xx = '<font color = "red"><b>'.$this->db->query("SELECT BerkasName FROM berkas WHERE BerkasId='".$row->BerkasId."'")->row()->BerkasName.'</font></b>';
                              
                              echo $xx;
                              
                              ?>  
                                  
                            </div>
                          </div>
                        </div>

                        <div class="col-md-12">
                          <div class="form-group">
                            <div class="col-md-2">Lokasi Berkas</div>
                            <div class="col-md-10">

                              <?php

                              $xx = '<font color="red"><b>'.$this->db->query("SELECT BerkasName FROM berkas WHERE BerkasId='".$row->BerkasId."'")->row()->BerkasName.'</font></b>';

                              $x1  = $this->db->query("SELECT RoleId FROM berkas WHERE BerkasId='".$row->BerkasId."'")->row()->RoleId;

                              $xx1 = '<font color = "brown"><b> Unit Kerja '.$this->db->query("SELECT RoleName FROM role WHERE RoleId='".$x1."'")->row()->RoleName.'</font></b>';
                              
                              echo $xx1;
                              
                              ?>  
                                  
                            </div>
                          </div>
                        </div>

                        <?php endforeach;?>
                        <?=form_close();?>
                        <div class="col-md-12">
                        <?= form_open(BASE_URL('administrator/anri_naskah_sudah_berkas/update_naskah_berkas1'), [
                                    'name'    => 'form_group', 
                                    'class'   => 'form-horizontal', 
                                    'id'      => 'form_group', 
                                    'enctype' => 'multipart/form-data', 
                                    'method'  => 'POST'
                        ]); ?>

                        <div class="form-group">
                          <input type="hidden" name="NId" value="<?=$this->uri->segment(5);?>">
                        </div>

                        <div class="form-group">
                          <div class="col-md-2">Pilih Berkas <sup class="text-danger">*</sup></div>
                            <div class="col-md-8">
                              <select name="BerkasId" class="form-control select2" required>
                                  <?php
                                      $query = $this->db->query("SELECT BerkasId, Klasifikasi, BerkasNumber, BerkasName, YEAR(CreationDate) AS tahun_cipta FROM berkas WHERE RoleId = '".$this->session->userdata('roleid')."' AND BerkasStatus = 'open' ORDER BY Klasifikasi ASC")->result();
                                      foreach ($query as $data_berkas){
                                  ?>
                                      <option <?= ($row->BerkasId == $data_berkas->BerkasId ? 'selected' : '') ?> value="<?= $data_berkas->BerkasId; ?>"><?= $data_berkas->Klasifikasi; ?>/<?= $data_berkas->BerkasNumber; ?>/<?= $data_berkas->tahun_cipta; ?> - <?= $data_berkas->BerkasName; ?></option>
                                  <?php } ?>
                              </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                          <div class="row-fluid col-md-7">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-danger"><p></p>
                          </div>
                        </div>

                      </div>
                      <?=form_close();?>
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
    $(document).ready(function(){        
    $('.select2').select2(); 
    }); /*end doc ready*/
</script>