<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li>
        <a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a>
      </li>

      <li class="active">Detail</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Detail Konsep Naskah Koreksi</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
                  <ul class="menu-baru">
                         <ul class="menu-baru" align="center" style="background-color: yellow">
                 

                             <ul class="menu-baru" align="center" style="background-color: yellow">
                     <li> <b><i class="fa fa-blind" aria-hidden="true"></i> <a href="#" data-toggle="modal" data-target="#koreksi"> Kirimkan Naskah Ke Pengonsep</a></li></b>

                  </ul>

                    <?php if($data_inbox->Status != 1){ ?>
                     
                     <!--  <li><i class="fa fa-newspaper-o"></i> <a href="#" data-toggle="modal" data-target="#koreksi">Koreksi</a></li> -->

                      <?php 
                          $xx1 = $this->db->query("SELECT Pengirim FROM inbox WHERE NId = '".$NId."'")->row();

                          //Jika Sumber Naskah Dinas inisiatif yang diterima, maka ubah metadata dihilangkan. Karena jika aktif user dapat merubah tanggal, nomor, sifata naskah yang tertanam didalam naskahnya
                          if($xx1->Pengirim != 'internal') { 
                       ?>


                    
                  
 
                          <!-- <li><i class="fa fa-edit"></i> <a href="<?= site_url('administrator/anri_reg_naskah_masuk/edit_naskah_masuk/view/' . $NId .'?dt='.$x); ?>"> Ubah Metadata</a></li> -->

                      <?php } ?>

                     

                    <?php } ?>

                  </ul>

                  <ul class="nav nav-tabs">
                    <li class="active"><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_uk/view/' . $NId .'?dt='.$x); ?>">Histori Konsep  Naskah</a></li>
           <!--  <li><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_metadata_koreksi/view/' . $NId .'?dt='.$x); ?>">Metadata</a></li>  -->
                    <li><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_dinas/view/' . $NId .'?dt='.$x); ?>"></a></li>
                  </ul>
                   <ul class="nav nav-tabs">
                     
                    
                       <ul class="menu-baru" align="center" style="background-color: yellow">
                     
                  </ul>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade in active">
                      <br>
                      <div class="table-responsive"> 
                        <table id="example1" class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>No</th>
                                 <th>Tanggal</th>
                                 <th>Asal Naskah</th>
                                 <th>Penerima/Posisi Naskah</th>
                                 <th>Keterangan</th>
                                <th width="15%">Field Yang Harus Diperbaiki</th>
                                 <th width="10%">Aksi</th>
                                 <th>Lampiran</th>                              
                               </tr>
                           </thead>
                           <tbody id="tbody_inbox">
                           <?php 
                            $this->db->select('*');
                            $this->db->group_by('GIR_Id'); 
                            $this->db->where('NId', $NId); 
                            $this->db->order_by('ReceiveDate','DESC');
                            $inboxs = $this->db->get('inbox_receiver_koreksi')->result();
                           foreach($inboxs as $k => $inbox): ?>
                              <tr>
                                  <td><?= $k+1; ?></td>
                                  <td><?= date('d-m-Y H:i:s',strtotime($inbox->ReceiveDate)) ?></td> 
                                  <td>
                                    <?php 
                                    if ($inbox->ReceiverAs == 'to') {
                                      echo $this->db->query("SELECT Instansipengirim FROM inbox WHERE NId='".$NId."'")->row()->Instansipengirim;
                                       echo $this->db->query("SELECT Namapengirim FROM inbox WHERE NId='".$NId."'")->row()->Namapengirim;
                                    }else{
                                      echo $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$inbox->RoleId_From."'")->row()->RoleName;
                                    }
                                    ?>
                                  </td>
                                  <td>


                                    <?php
                                    //kondisi baru

                                     $xq  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$inbox->NId."' AND Keterangan = 'outbox'")->num_rows();

                                     if($xq > 0) {
                                       if($inbox->ReceiverAs == 'approvenaskah'){ 
                                        $xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$inbox->NId."' AND Keterangan = 'outbox'")->row();

                                        $files = BASE_URL.'/FilesUploaded/naskah/';
                                         echo '<ul><li><font size="4" color="red" style="background-color: yellow"><b>Melakukan Approve</b></font></li></ul>';

                                      



                                  }else{




                                      $tujuan = $this->db->query("SELECT * FROM inbox_receiver_koreksi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs !='bcc'")->result();
                                        echo '<ul>';
                                        foreach($tujuan as $key => $dat_tujuan) {
                                          if($dat_tujuan->StatusReceive=='unread')
                                            { 
                                              if($dat_tujuan->RoleId_To == $this->session->userdata('roleid')){
                                                echo '<li><font color="red" style="background-color: yellow"><b>'.get_data_people('PeoplePosition',$dat_tujuan->To_Id).' (Belum Dibaca)</b></font></li>';
                                              } else {
                                                echo '<li><font color="red">'.get_data_people('PeoplePosition',$dat_tujuan->To_Id).' (Belum Dibaca)</font></li>';
                                              }
                                            }
                                          else
                                            {
                                              if($dat_tujuan->RoleId_To == $this->session->userdata('roleid')){
                                                echo '<li><font color="brown" style="background-color: yellow"><b>'.get_data_people('PeoplePosition',$dat_tujuan->To_Id).' (Dibaca)</b></font></li>';
                                              } else {
                                                echo '<li>'.get_data_people('PeoplePosition',$dat_tujuan->To_Id).' (Dibaca)</li>';
                                              }
                                            }  
                                        }
                                        echo '</ul>';    


                                      $tembusan = $this->db->query("SELECT * FROM inbox_receiver_koreksi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs ='bcc'")->result();
                                      if(count($tembusan)){
                                        echo 'Tembusan : <br><ul>';
                                          foreach($tembusan as $key => $dat_tembusan) {
                                            if($dat_tembusan->StatusReceive=='unread')
                                              { 
                                                if($dat_tembusan->RoleId_To == $this->session->userdata('roleid')){
                                                  echo '<li><font color="red" style="background-color: yellow"><b>'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Belum Dibaca)</b></font></li>';
                                                } else {
                                                  echo '<li><font color="red">'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Belum Dibaca)</font></li>';
                                                }
                                              }
                                            else
                                              {
                                                if($dat_tembusan->RoleId_To == $this->session->userdata('roleid')){
                                                  echo '<li><font color="brown" style="background-color: yellow"><b>'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Dibaca)</b></font></li>';
                                                } else {
                                                  echo '<li>'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Dibaca)</li>';
                                                }



                                              }  

                                          }
                                        echo '</ul>';
                                      }


                                    } 




                                  }else{ 
                                    $tujuan = $this->db->query("SELECT * FROM inbox_receiver_koreksi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs !='bcc'")->result();
                                    echo '<ul>';
                                    foreach($tujuan as $key => $dat_tujuan) {
                                      if($dat_tujuan->StatusReceive=='unread')
                                      { 
                                        if($dat_tujuan->RoleId_To == $this->session->userdata('roleid')){
                                          echo '<li><font color="red" style="background-color: yellow"><b>'.get_data_people('PeoplePosition',$dat_tujuan->To_Id).' (Belum Dibaca)</b></font></li>';
                                        } else {
                                          echo '<li><font color="red">'.get_data_people('PeoplePosition',$dat_tujuan->To_Id).' (Belum Dibaca)</font></li>';
                                        }
                                      }
                                      else
                                      {
                                        if($dat_tujuan->RoleId_To == $this->session->userdata('roleid')){
                                          echo '<li><font color="brown" style="background-color: yellow"><b>'.get_data_people('PeoplePosition',$dat_tujuan->To_Id).' (Dibaca)</b></font></li>';
                                        } else {
                                          echo '<li>'.get_data_people('PeoplePosition',$dat_tujuan->To_Id).' (Dibaca)</li>';
                                        }
                                      }  
                                    }
                                    echo '</ul>';    
                                    $tembusan = $this->db->query("SELECT * FROM inbox_receiver_koreksi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs ='bcc'")->result();
                                    if(count($tembusan)){
                                      echo 'Tembusan : <br><ul>';
                                      foreach($tembusan as $key => $dat_tembusan) {
                                        if($dat_tembusan->StatusReceive=='unread')
                                        { 
                                          if($dat_tembusan->RoleId_To == $this->session->userdata('roleid')){
                                            echo '<li><font color="red" style="background-color: yellow"><b>'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Belum Dibaca)</b></font></li>';
                                          } else {
                                            echo '<li><font color="red">'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Belum Dibaca)</font></li>';
                                          }
                                        }
                                        else
                                        {
                                          if($dat_tembusan->RoleId_To == $this->session->userdata('roleid')){
                                            echo '<li><font color="brown" style="background-color: yellow"><b>'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Dibaca)</b></font></li>';
                                          } else {
                                            echo '<li>'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Dibaca)</li>';
                                          }



                                        }  

                                      }
                                      echo '</ul>';
                                    }
                                  }



                                    //akhir kondisi baRU
                                  ?> 



                                  </td> 
                                  <td>
                                    <?php 
                                      $t1 = $this->db->query("SELECT ReceiverAs FROM inbox_receiver_koreksi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs != 'bcc'")->row()->ReceiverAs;

                                      if ($t1 == 'to') {
                                        echo "Naskah Masuk";
                                      }elseif($t1 == 'to_undangan'){
                                        echo "Undangan";
                                      }elseif($t1 == 'to_sprint'){
                                        echo "Surat Tugas";
                                      }elseif($t1 == 'to_notadinas'){
                                        echo "Nota Dinas";
                                      }elseif($t1 == 'to_reply'){
                                        echo "Nota Dinas";
                                      }elseif($t1 == 'to_usul'){
                                        echo "Jawaban Nota Dinas";
                                      }elseif($t1 == 'to_forward'){
                                        echo "Teruskan ";
                                      }elseif($t1 == 'koreksi'){
                                        echo "Memberi Perintah Koreksi Konsep";
                                      }elseif($t1 == 'to_keluar'){
                                        echo "Surat Dinas Keluar";
                                      }elseif($t1 == 'to_nadin'){
                                        echo "Naskah Dinas Lainnya";
                                      }elseif($t1 == 'meneruskan'){
                                        echo "meneruskan konsep";
                                      }elseif($t1 == 'approvenaskah'){
                                        echo "Naskah telah di Approve";
                                      }elseif($t1 == 'to_konsep'){
                                        echo "Konsep Naskah";
                                      }elseif($t1 == 'to_memo'){
                                        echo "Memo";
                                      }elseif ($t1 == 'to_draft_notadinas') {
                                        echo "Konsep Nota Dinas";
                                      }else if($t1 == 'to_draft_sprint'){ 
                                        echo "Konsep Surat Tugas";
                                      }else if($t1 == 'to_draft_undangan'){ 
                                        echo "Konsep Undangan";
                                      }else if($t1 == 'to_draft_keluar'){ 
                                        echo "Konsep surat Dinas";
                                      }else if($t1 == 'setuju'){ 
                                        echo "menyetujui setelah dikoreksi";
                                      }else if($t1 == 'to_koreksi'){ 
                                        echo "melakukan koreksi";                                        
                                      }elseif($t1 == 'Meminta Nomber Surat'){
                                        echo "Meminta Nomber Surat";
                                      }else if($t1 == 'to_draft_instruksi_gub'){ 
                                        echo "Konsep Surat Instruksi";
                                      }else if($t1 == 'to_draft_rekomendasi'){ 
                                        echo "Konsep Surat Rekomendasi";
                                      }else if($t1 == 'to_draft_sket'){ 
                                        echo "Konsep Surat Keterangan";
                                      }else if($t1 == 'to_draft_pengumuman'){ 
                                        echo "Konsep Pengumuman";
                                      }else if($t1 == 'to_draft_super_tugas'){ 
                                        echo "Konsep Surat Pernyataan Melaksanakan Tugas";
                                      }else {
                                        echo "Konsep Naskah Dinas Lainnya";
                                      }
                                    ?>
                                  </td>
                                  <td>
                                    <?php
                                    if($inbox->ReceiverAs == 'koreksi'){
                                      $count_koreksi  = $this->db->query("SELECT Koreksi FROM inbox_koreksi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."'")->num_rows();
                                      if($count_koreksi > 0){
                                        $koreksi  = $this->db->query("SELECT Koreksi FROM inbox_koreksi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."'")->result();
                                        
                                        foreach ($koreksi as $key => $value) {
                                          $exp = explode('|', $value->Koreksi);
                                          if($value->Koreksi == '-'){
                                            echo '';
                                          } else {
                                            echo "<ul>";
                                            foreach ($exp as $k => $v) {
                                              echo '<li>'.$this->db->query("SELECT KoreksiName FROM master_koreksi WHERE KoreksiId = '".$v."'")->row()->KoreksiName.'</li>';
                                            }  
                                            echo "</ul>";
                                          }       
                                        }                                       
                                      }
                                    }

                                    ?>

                                    <!-- Jika naskah teruskan munculkan pesan teruskan -->
                                    <?php if($inbox->ReceiverAs == 'koreksi'){ ?>
                                      <center><!-- <a title="Lihat Pesan Koreksi" data-toggle="modal" data-target="#view_detail" data-msg="<?= $inbox->Msg ?>" href="#" class="btn btn-primary btn-sm btn-detail"><i class="fa fa-file-o"></i></a> -->

                                      </center>

                                    <?php } ?>    

                                  </td>                                 
                                  <td>
                                    <?php 
                                    
                                    $xq  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$inbox->NId."' AND Keterangan = 'outbox'")->num_rows();
                                    
                                    //Jika Sumber Naskah Dinas inisiatif yang diterima, maka munculkan file naskah dinas nya, dan jika naskah sudah disposisi atau diteruskan jalankan fungsi ini untuk melihat pesan disposisi yang ditulis pada textfield pesan
                                    if($xq > 0) {

                                      $xq  = $this->db->query("SELECT Ket FROM konsep_naskah WHERE GIR_Id = '".$inbox->NId."' AND NId_Temp = '".$inbox->GIR_Id."' AND Ket = 'nadin'")->num_rows();

                                      //JIka terdapat nota dinas tindaklanjut dari tindaklanjut naskah dinas inisiatif 
                                      if($xq > 0) {

                                        $xyz1  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND Keterangan = 'outbox'")->row();
                                        
                                        $files1 = BASE_URL.'/FilesUploaded/naskah/';

                                        echo '<a href="'.$files1.$xyz1->FileName_real.'" target="_new" title="Lihat Nota Dinas" class="btn btn-success btn-sm"><i class="fa fa-envelope-o"></i></a>';

                                      } else {

                                          $xyz2  = $this->db->query("SELECT Pengirim FROM inbox WHERE NId = '".$inbox->NId."'")->row(); 

                                          //Jika surat berasal dar luar instansi, kosongkan aksi
                                          if($xyz2->Pengirim == 'eksternal') {

                                            echo '';
                                            
                                          } else { 
                                              if($inbox->ReceiverAs == 'approvenaskah'){ 
                                                $xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$inbox->NId."' AND Keterangan = 'outbox'")->row();
                                            
                                            $files = BASE_URL.'/FilesUploaded/naskah/';
                                                   echo '<a href="'.$files.$xyz->FileName_real.'" target="_new" title="Lihat Naskah Dinas" class="btn btn-success btn-sm"><i class="fa fa-envelope-o"></i></a>';
                                                   }

                                            //naskah dinas inisiatif 
                                            // $xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$inbox->NId."' AND Keterangan = 'outbox'")->row();
                                            
                                            // $files = BASE_URL.'/FilesUploaded/naskah/';

                                            // echo '<a href="'.$files.$xyz->FileName_real.'" target="_new" title="Lihat Naskah Dinas" class="btn btn-success btn-sm"><i class="fa fa-envelope-o"></i></a>';


                                              if($inbox->ReceiverAs == 'koreksi'){ ?>
                                        <a title="Lihat Detail Isi Koreksi" data-toggle="modal" data-target="#view_disposisi" data-nid="<?= $inbox->NId ?>" data-girid="<?= $inbox->GIR_Id ?>" href="#" class="btn btn-warning btn-sm btn-view"><i class="fa fa-eye"></i></a>

                                          

                                      <?php }elseif($inbox->ReceiverAs == 'to_koreksi'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Hasil Koreksi" class=".btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>


                                        <?php }elseif($inbox->ReceiverAs == 'to_draft_keluar'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Konsep Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>


                                      <?php }elseif($inbox->ReceiverAs == 'to_draft_sket'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Konsep Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>


                                         <?php }elseif($inbox->ReceiverAs == 'to_draft_notadinas'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>

                                         <?php }elseif($inbox->ReceiverAs == 'to_draft_edaran'){ ?>
                                         <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>
                                         
                                           <?php }elseif($inbox->ReceiverAs == 'to_draft_sprint'){ ?>
                                         <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>

                                       <?php }elseif($inbox->ReceiverAs == 'to_draft_instruksi_gub'){ ?>
                                         <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>

                                     <?php }elseif($inbox->ReceiverAs == 'to_draft_surat_izin'){ ?>
                                         <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>
                                          <?php }elseif($inbox->ReceiverAs == 'to_draft_super_tugas'){ ?>
                                         <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>

                                      <?php }elseif($inbox->ReceiverAs == 'to_draft_nadin'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$inbox->NId) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                        <?php }elseif($inbox->ReceiverAs == 'to_undangan'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$inbox->GIR_Id) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                        <?php }elseif($inbox->ReceiverAs == 'to_sprint'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$inbox->GIR_Id) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                        <?php }elseif($inbox->ReceiverAs == 'to_keluar'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$inbox->GIR_Id) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                        <?php }elseif($inbox->ReceiverAs == 'to_nadin'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$inbox->GIR_Id) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                      <?php 
                                        } $cari = $this->db->query("SELECT RoleId_From, ReceiveDate FROM inbox_receiver_koreksi WHERE NId = '".$NId."' ORDER BY ReceiveDate DESC LIMIT 1")->row();


                                          }
                                      }

                                      if($inbox->ReceiverAs == 'koreksi'){ ?>
                                        <a title="Lihat Detail Koreksi" data-toggle="modal" data-target="#view_disposisi" data-nid="<?= $inbox->NId ?>" data-girid="<?= $inbox->GIR_Id ?>" href="#" class="btn btn-danger btn-sm btn-view"><i class="fa fa-file-o"></i></a>


                                      <?php }
                                      // akhir konsidi xq
                                        

                                    //Jika Sumber Naskah Dinas berasal dari luar lembaga, jalankan fungsi ini
                                    } else {
                                      
                                      if($inbox->ReceiverAs == 'koreksi'){ ?>
                                        <a title="Lihat Detail Isi Koreksi" data-toggle="modal" data-target="#view_disposisi" data-nid="<?= $inbox->NId ?>" data-girid="<?= $inbox->GIR_Id ?>" href="#" class="btn btn-warning btn-sm btn-view"><i class="fa fa-eye"></i></a>
                                          

                                      <?php }elseif($inbox->ReceiverAs == 'to_koreksi'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Hasil Koreksi" class=".btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                      <?php }elseif($inbox->ReceiverAs == 'to_draft_keluar'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Konsep Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>
                                      <?php }elseif($inbox->ReceiverAs == 'to_draft_notadinas'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>
                                      <?php }elseif($inbox->ReceiverAs == 'to_draft_edaran'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>                                               

                                      <?php }elseif($inbox->ReceiverAs == 'to_draft_sket'){ ?>
                                        <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Konsep Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>

                                     <?php }elseif($inbox->ReceiverAs == 'to_draft_sprint'){ ?>
                                       <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>
                                     <?php }elseif($inbox->ReceiverAs == 'to_draft_instruksi_gub'){ ?>
                                       <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>
                                     <?php }elseif($inbox->ReceiverAs == 'to_draft_surat_izin'){ ?>
                                       <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>
                                     <?php }elseif($inbox->ReceiverAs == 'to_draft_super_tugas'){ ?>
                                       <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/view_naskah_koreksi_pdf/'.$inbox->id_koreksi) ?>" title="Lihat Naskah Awal" class="btn btn-primary btn-sm"><i class="fa fa-envelope-o fa-fw"></i></a>
                                     <?php }elseif($inbox->ReceiverAs == 'to_draft_nadin'){ ?>
                                      <a target="_blank" href="<?= base_url('administrator/anri_list_naskah_belum_approve/naskahdinas_tindaklanjut_pdf/'.$inbox->NId) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    <?php }elseif($inbox->ReceiverAs == 'to_undangan'){ ?>
                                      <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$inbox->GIR_Id) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    <?php }elseif($inbox->ReceiverAs == 'to_sprint'){ ?>
                                      <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$inbox->GIR_Id) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    <?php }elseif($inbox->ReceiverAs == 'to_keluar'){ ?>
                                      <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$inbox->GIR_Id) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    <?php }elseif($inbox->ReceiverAs == 'to_nadin'){ ?>
                                      <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$inbox->GIR_Id) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                      <?php 
                                        } $cari = $this->db->query("SELECT RoleId_From, ReceiveDate FROM inbox_receiver_koreksi WHERE NId = '".$NId."' ORDER BY ReceiveDate DESC LIMIT 1")->row();

                                       if($inbox->ReceiverAs != 'to_notadinas') {

                                        if(($cari->RoleId_From == $inbox->RoleId_From) && ($cari->RoleId_From == $this->session->userdata('roleid')) && ($cari->ReceiveDate == $inbox->ReceiveDate)){ ?>
                                         <!--  <a title="Hapus Data" href="#" data-href="<?= site_url('administrator/anri_mail_tl/hapus_histori/view/' . $NId.'/'.$inbox->GIR_Id.'?dt='.$x); ?>" class="btn btn-danger btn-sm hapus-histori"><i class="fa fa-trash-o"></i></a> -->
                                      <?php } } 
                                    } ?>
                                  </td>
                                  <td>
                                    <?php
                                        $query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND Keterangan != 'outbox'");

                                        $files = BASE_URL.'/FilesUploaded/'.get_data_inbox('NFileDir',$inbox->NId).'/';

                                        foreach($query->result_array() as $row):

                                          echo '<center><a href="'.$files.$row['FileName_fake'].'" target="_new" title="'.$row['FileName_fake'].'" class="btn btn-danger btn-sm"><i class="fa fa-download"></i></a><br><br></center>';
                                        endforeach;   
                                    ?>                                  
                                  </td>                                   
                              </tr>
                            <?php endforeach; ?>
                           </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <?php $this->load->view('backend/standart/partials/modal-inbox-uk'); ?>

   <div id="view_disposisi" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detail Koreksi</h4>

          </div>
          <div class="modal-body">
            <div class="body-dispo"></div>
          </div>
        </div>
      </div>
    </div>
    <div id="view_detail" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pesan</h4>
          </div>
          <div class="modal-body">
            <div class="body-view"></div>
          </div>
        </div>
      </div>
    </div>
</section>
<script>
  $('.btn-view').click(function(){
    var url = "<?= site_url('administrator/anri_mail_tl/iframe_koreksi/'); ?>";
    var nid = $(this).data('nid');
    var girid = $(this).data('girid');
    $('.frame').remove();
    var apend = '<iframe class="frame" src="'+url+nid+'/'+girid+'" height="400" width="100%"></iframe>';
    $('.body-dispo').append(apend);
  });
  $('.btn-detail').click(function(){
    var apend = '<div class="frame">'+$(this).data('msg')+'</div>';
    $('.body-view').append(apend);
  });
  
  $('.hapus-histori').click(function(){
      var url = $(this).attr('data-href');
      swal({
          title: "Apakah Data Akan Dihapus ??",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });
</script>
<!-- /.content -->