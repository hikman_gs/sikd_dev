<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li>
        <a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a>
      </li>

      <li>
        <?php 

          $x = $_GET['dt'];
          if($x == '1') { ?>

          <a  href="<?= site_url('administrator/anri_list_naskah_masuk'); ?>">Daftar Semua Naskah Masuk</a>

        <?php } elseif($x == '2') { ?>

          <a  href="<?= site_url('administrator/anri_list_naskahmasuk_btl'); ?>">Daftar Naskah Masuk Belum Tindaklanjut</a>

        <?php } elseif($x == '3') { ?>

          <a  href="<?= site_url('administrator/anri_list_disposisi_btl'); ?>">Daftar Disposisi Belum Tindaklanjut</a>

        <?php } elseif($x == '4') { ?>

          <a  href="<?= site_url('administrator/anri_list_disposisi'); ?>">Daftar Semua Disposisi</a>

        <?php } elseif($x == '5') { ?>

          <a  href="<?= site_url('administrator/anri_list_teruskan_btl'); ?>">Daftar Naskah Teruskan Belum Tindaklanjut</a>

        <?php } elseif($x == '6') { ?>

          <a  href="<?= site_url('administrator/anri_list_teruskan'); ?>">Daftar Semua Naskah Teruskan</a>

        <?php } elseif($x == '7') { ?>

          <a  href="<?= site_url('administrator/anri_list_notadinas_btl_ap'); ?>">Daftar Nota Dinas Belum Disetujui</a>

        <?php } elseif($x == '8') { ?>

          <a  href="<?= site_url('administrator/anri_list_notadinas_sdh_ap'); ?>">Daftar Nota Dinas Belum Dikirim</a>

        <?php } elseif($x == '9') { ?>

          <a  href="<?= site_url('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId .'?dt=9'); ?>">Detail Naskah</a>

        <?php } elseif($x == '10') { ?>

          <a  href="<?= site_url('administrator/anri_list_notadinas_btl'); ?>">Daftar Nota Dinas Belum Tindaklanjut</a>

        <?php } elseif($x == '11') { ?>

          <a  href="<?= site_url('administrator/anri_list_notadinas'); ?>">Daftar Semua Nota Dinas</a>

        <?php } elseif($x == '12') { ?>

          <a  href="<?= site_url('administrator/anri_list_undangan_btl'); ?>">Daftar Undangan Belum Tindaklanjut</a>

        <?php } elseif($x == '13') { ?>

          <a  href="<?= site_url('administrator/anri_list_undangan'); ?>">Daftar Semua Undangan</a>

        <?php } elseif($x == '14') { ?>

          <a  href="<?= site_url('administrator/anri_list_surattugas_btl'); ?>">Daftar Surat Tugas Belum Tindaklanjut</a>

        <?php } elseif($x == '15') { ?>

          <a  href="<?= site_url('administrator/anri_list_surattugas'); ?>">Daftar Semua Surat Tugas</a>

        <?php } elseif($x == '16') { ?>

          <a  href="<?= site_url('administrator/anri_list_suratdinas_keluar_btl'); ?>">Daftar Surat Dinas Belum Tindaklanjut</a>

        <?php } elseif($x == '17') { ?>

          <a  href="<?= site_url('administrator/anri_list_suratdinas_keluar'); ?>">Daftar Semua Surat Dinas</a>

        <?php } elseif($x == '18') { ?>

          <a  href="<?= site_url('administrator/anri_list_naskahdinas_lain_btl'); ?>">Daftar Naskah Dinas Lainnya Belum Tindaklanjut</a>

        <?php } elseif($x == '19') { ?>

          <a  href="<?= site_url('administrator/anri_list_naskahdinas_lain'); ?>">Daftar Semua Naskah Dinas Lainnya</a>

        <?php } else { ?>

          <a  href="<?= site_url('administrator/anri_list_naskah_masuk'); ?>"><?= $x; ?></a>

        <?php } ?>
      </li> 

      <li class="active">Detail</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Detail</h3>
                     <h5 class="widget-user-desc">Naskah Masuk</h5>
                     <hr>
                  </div>
                  <ul class="menu-baru">
                    <?php if($data_inbox->Status != 1){ ?>
                    <li><i class="fa fa-envelope"></i> <a href="#" data-toggle="modal" data-target="#teruskan"> Teruskan</a></li>
                    <li><i class="fa fa-envelope"></i> <a href="#" data-toggle="modal" data-target="#naskahdinas"> Nota Dinas</a></li>
                    <li><i class="fa fa-newspaper-o"></i> <a href="#" data-toggle="modal" data-target="#disposisi">  Disposisi</a></li>

                      <?php 
                          $xx1 = $this->db->query("SELECT Pengirim FROM inbox WHERE NId = '".$NId."'")->row();

                          //Jika Sumber Naskah Dinas inisiatif yang diterima, maka ubah metadata dihilangkan. Karena jika aktif user dapat merubah tanggal, nomor, sifata naskah yang tertanam didalam naskahnya
                          if($xx1->Pengirim != 'internal') { 
                       ?>

                          <li><i class="fa fa-edit"></i> <a href="<?= site_url('administrator/anri_reg_naskah_masuk/edit_naskah_masuk/view/' . $NId .'?dt='.$x); ?>"> Ubah Metadata</a></li>

                      <?php } ?>
                      
                    <li><i class="fa fa-check"></i> <a href="<?= site_url('administrator/anri_mail_tl/selesai_tindaklanjut/view/' . $NId .'?dt='.$x); ?>"> Selesai Tindak Lanjut</a></li>
                    <?php } ?>
                  </ul>
                  <ul class="nav nav-tabs">
                    <li><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_histori/view/' . $NId .'?dt='.$x); ?>">Histori Naskah</a></li>
                    <li><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_metadata/view/' . $NId .'?dt='.$x); ?>">Metadata</a></li>
                    <li class="active"><a href="<?= site_url('administrator/anri_mail_tl/view_naskah_dinas/view/' . $NId .'?dt='.$x); ?>">Approve Nota Dinas</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade in active">
                      <br>
                      <div class="table-responsive"> 
                        <table id="example1" class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>No</th>
                                 <th>Jenis Naskah</th>
                                 <th>Nomor</th>
                                 <th>Tanggal</th>
                                 <th>Tujuan</th>
                                 <th>Hal</th>
                                 <th width="8%">Nota Dinas</th>
                                 <th width="8%">Aksi</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_inbox">
                           <?php 
                           $konsep = $this->db->query("SELECT * FROM konsep_naskah WHERE GIR_Id = '".$NId."' AND RoleId_From = '".$this->session->userdata('roleid')."'")->result();
                           foreach($konsep as $k => $data): ?>
                              <tr>
                                 <td><?= $k+1; ?></td>
                                 <td><?= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$data->JenisId."'")->row()->JenisName ?></td>
                                 <td><?= $data->nosurat ?></td>
                                 <td><?= date('d-m-Y',strtotime($data->TglReg)) ?></td>
                                 <td>
                                  <?php
                                    $exp = explode(',', $data->RoleId_To);
                                    if (count($exp) > 1) {
                                      echo '<ul>';
                                      foreach ($exp as $k => $v) {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
                                        echo '<li>'.$xx1.'</li>';
                                      }
                                      echo '</ul>';
                                    }else{
                                      $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$exp[0]."'")->row()->RoleName;
                                      echo $xx1;
                                    }

                                    $exp = explode(',', $data->RoleId_Cc);
                                    if (count($exp) > 1) {
                                      echo '<b>Tembusan :</b> <br><ul>';
                                      foreach ($exp as $k => $v) {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
                                        echo '<li>'.$xx1.'</li>';
                                      }
                                      echo '</ul>';
                                    }else{
                                      if (!empty($exp[0])) {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$exp[0]."'")->row()->RoleName;
                                        echo '<br><br><b>Tembusan :</b> <br>'.$xx1;
                                      }  
                                    }

                                  ?>
                                 </td>
                                 <td><?= $data->Hal ?></td>
                                 <td>
                                    <!-- Konsep = 1 adalah naskah belum di approve -->
                                    <?php if ($data->Konsep == 1) { 
                                      
                                      //cek jika yang approve adalah yang login maka munculkan fungsi setujui
                                      $xyz = $this->db->query("SELECT NId_Temp FROM konsep_naskah WHERE GIR_Id = '".$NId."' AND Approve_People = '".$this->session->userdata('peopleid')."'")->num_rows();  
                                      if ($xyz > 0) { ?>
                                        <a title="Menyetujui" href="#" data-nid="<?= $data->NId_Temp; ?>" data-qrcode="<?= base_url('FilesUploaded/qrcode/'.$data->NId_Temp) ?>" class="btn btn-success btn-sm verifikasi"><i class="fa fa-check"></i></a>
                                      <?php } ?>
                                      <!-- ================================================== -->
                                        <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/nota_dinas_tindaklanjut_pdf/'.$data->NId_Temp) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>

                                    <!-- Konsep = 0 adalah naskah sudah di approve dan belum dikirim-->  
                                    <?php  }elseif($data->Konsep == 0) { ?>
                                      <a title="Kirim Naskah" href="#" data-nid="<?= $data->NId_Temp; ?>" data-girid="<?= $data->GIR_Id; ?>" class="btn btn-warning btn-sm btn-kirim"><i class="fa fa-upload"></i></a>
                                      <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$data->NId_Temp) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    <?php } ?>
                                    <?php if ($data->Konsep > 1) { ?>
                                    <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$data->NId_Temp) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    <?php } ?>                                  
                                 </td>
                                 <td>                                 
                                  <?php if ($data->Konsep == 1) { ?>
                                  <a href="<?= base_url('administrator/anri_mail_tl/view_naskah_dinas_tindaklanjut_edit/view/'.$data->NId_Temp.'?dt='.$x) ?>" title="Ubah Data" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                  <a href="#" data-temp="<?= $data->NId_Temp; ?>" title="Hapus Data" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i></a>
                                  <?php } ?>
                                </td>
                              </tr>
                            <?php endforeach; ?>
                           </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="approve" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Verifikasi Dokumen</h4>
          </div>
          <div class="modal-body">
            <div class="body-qrcode"></div>
          </div>
        </div>
      </div>
    </div>
   <?php $this->load->view('backend/standart/partials/modal-inbox'); ?>
</section>
<!-- /.content -->
<script>
  $('.verifikasi').click(function(){
    $('.hapus-form').remove();
    $('#approve').modal('toggle');
    var img = $(this).data('qrcode');
    var nid = $(this).data('nid');
    var append = '<form action="<?= base_url('administrator/anri_mail_tl/verifikasi/'); ?>'+nid+'" class="form-horizontal hapus-form" method="POST">'+
              '<div class="form-group">'+
                '<div class="col-md-12">'+
                  '<input type="password" class="form-control" placeholder="Masukan Kode Verifikasi" name="password">'+
                '</div>'+
              '</div>'+
              '<div class="form-group">'+
                '<div class="col-md-12">'+
                  '<button class="btn btn-success">Setujui</button>'+
                '</div>'+
              '</div>'+
            '</form>';
    $('.body-qrcode').append(append);
  });
  $('.btn-kirim').click(function(){
    swal({
          title: "Proses pengiriman nota dinas ??",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
    .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "<?=BASE_URL('administrator/anri_mail_tl/naskah_dinas_kirim')?>",
                  type: "POST",
                  data:{
                      nid: $(this).data('nid'),
                      girid: $(this).data('girid'),
                  },
                  success: function(response){
                    swal({
                      title: "Nota dinas berhasil dikirim",
                      icon: "success",
                    }).then((result) => {
                      window.location.href = "<?=BASE_URL('administrator/anri_mail_tl/view_naskah_dinas/view/'.$NId.'?dt='.$x)?>";
                    });
                  },
                  error: function(){
                    swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
                  }
            });
          }
        });
    });
  $('.btn-delete').click(function(){
    swal({
          title: "Apakah Data Akan Dihapus ??",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
    .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "<?=BASE_URL('administrator/anri_mail_tl/hapus_naskah_dinas_tindaklanjut/view/'.$NId.'?dt='.$x) ?>",
                  type: "POST",
                  data:{
                      temp: $(this).data('temp'),
                  },
                  success: function(response){
                    swal({
                      title: "Data berhasil dihapus",
                      icon: "success",
                    }).then((result) => {
                      window.location.href = "<?=BASE_URL('administrator/anri_mail_tl/view_naskah_dinas/view/'.$NId.'?dt='.$x)?>";
                    });
                  },
                  error: function(){
                    swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
                  }
            });
          }
        });
    });
</script>