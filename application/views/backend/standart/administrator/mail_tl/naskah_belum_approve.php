<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>

<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<link href="<?= BASE_ASSET; ?>/select3/select3.min.css" rel="stylesheet" />
<style>
    #approve .modal-content {
        /* new custom width */
        height: 220px;
    }

    #nomor .modal-content {
        /* new custom width */
        height: 220px;
    }

    #k_teruskan .modal-content {
        /* new custom width */
        width: 520px;
    }

    #k_teruskan_tu .modal-content {
        /* new custom width */
        width: 519px;
    }


    #k_teruskan_uk .modal-content {
        /* new custom width */
        width: 520px;
    }

    #k_tu_ttdnotapimpinan .modal-content {
        /* new custom width */
        width: 520px;
    }

     #k_teruskan_surat_dinas .modal-content {
        /* new custom width */
        width: 520px;
    }

    .select2-container--default .select2-selection--single {
        border: 0px;
    }

    .select2-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 2px 0px !important;
    }

    .chosen-container-single,
    .chosen-container-multi {
        width: 100% !important;
    }

    .select3-container--default .select2-selection--single {
        border: 0px;
    }

    .select3-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 3px 0px !important;
    }

    .chosen-container-single,
    .chosen-container-multi {
        width: 100% !important;
    }
</style>

<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active"><?= $title; ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="row pull-right">
                                <a class="btn btn-flat btn-success" title="Tambah Data" href="<?=  site_url('administrator/anri_list_naskah_sudah_approve'); ?>"><i class="fa fa-arrow-right" ></i> Naskah Belum Dikirim</a>
                            </div>
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Daftar Naskah Belum Disetujui</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>                     

                        <div class="table-responsive">

                            <table id="myTable" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%">

                                <thead>
                                    <tr>
                                        <th width="5%">No</th>
                                        <th width="10%">Tanggal Pencatatan</th>
                                        <th width="15%">Jenis Naskah</th>
                                        <th width="20%">Hal</th>
                                        <th width="5%">Lampiran</th>
                                        <th width="10%">#</th>
                                        <th width="7%">Aksi</th>
                                    </tr>
                                </thead>

                                <tbody></tbody>

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="approve_naskah" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Tandatangani Dokumen</b></h4>
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/approve_naskah'); ?>" class="form-horizontal" method="POST">
                    <div class="modal-body">
                        <div class="content">
                            <div class="form-group">
                                <input type="hidden" id="NId_Temp" name="NId_Temp">
                                <input type="hidden" id="GIR_Id" name="GIR_Id">
                                <input type="hidden" id="nama_lengkap" name="nama_lengkap" value="<?= $this->session->userdata('peoplename'); ?>">
                                <div class="col-6">
                                    <label>Masukan Passphrase</label>
                                    <input type="password" min="1" class="form-control" name="password" id="password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Setujui </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="nomor" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Penomoran Naskah Manual</b></h4>
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/nomor_dong'); ?>" class="form-horizontal" method="POST">
                    <div class="modal-body">
                        <div class="content">
                            <div class="form-group">
                                <input type="hidden" id="NId_Temp3" name="NId_Temp3">
                                <input type="hidden" id="GIR_Id3" name="GIR_Id3">
                                <div class="col-6">
                                    <label>Masukan Nomor Naskah</label>
                                    <input type="text" class="form-control" name="nonaskah" id="nonaskah">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Ambil Nomor </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="k_teruskan" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Teruskan Untuk Ditandatangani</b></h4>
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/k_teruskan'); ?>" class="form-horizontal" method="POST">

                    <div class="modal-body">
                        <div class="content">

                            <div class="form-group">
                                <input type="hidden" id="NId_Temp2" name="NId_Temp2">
                                <input type="hidden" id="GIR_Id2" name="GIR_Id2">

                                <div class="col-6">

                                    <label>Penandatangan <sup class="text-danger">*</sup></label>
                                    <select name="TtdText" id="TtdText" class="form-control select2 TtdText" required>
                                        <?php foreach ($this->db->query('SELECT * FROM master_ttd')->result() as $data1) { ?>
                                            <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                                        <?php } ?>
                                    </select>

                                    <?php
                                    $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND GroupId NOT IN ('8') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
                                    foreach ($AtasanId as $dataz) {
                                        $xx1 = $dataz->PeopleId;
                                    }
                                    ?>
                                    <!-- tambahan dispu -->

                                    <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $xx1; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6 Approve_People hidden">
                                    <select name="Approve_People" id="Approve_People" class="form-control select2">
                                        <!-- Perbaikan Eko 09-11-2019 -->
                                        <?php
                                        $query = $this->db->query("SELECT PeopleId, PeoplePosition,PeopleName FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                                        foreach ($query as $dat_people) {
                                        ?>
                                            <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                        <?php
                                        }
                                        ?>
                                        <!-- batas akhir perbaikan -->
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <label>Pesan <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" name="pesan" id="pesan" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan dan Kirim </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- MODAL TERUSKAN -->
    <div id="teruskan" class="modal fade" role="dialog">
        <div class="modal-lg modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Teruskan</h4>
                </div>
                <div class="modal-body">
                    <form action="<?= base_url('administrator/anri_mail_tl/inbox_teruskan/' . $tipe . '/' . $NId); ?>" class="form-horizontal" method="POST">
                        <div class="form-group">
                            <div class="col-md-9">
                                <?php $x = (!empty($_GET['dt']) ?  $_GET['dt']  : ''); ?>
                                <input type="hidden" name="xdt" class="form-control" value="<?= $x; ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">Tujuan Surat <sup class="text-danger">*</sup></div>
                            <div class="col-md-9">
                                <select name="tujuan[]" class="form-control select2" required multiple>
                                    <!-- Perbaikan Eko 09-11-2019 -->
                                    <?php



                                    $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.RoleAtasan = '" . $this->session->userdata('roleatasan') . "' AND p.GroupId = '6' ORDER BY p.PrimaryRoleId ASC")->result();
                                    foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php

                                    }

                                    ?>

                                    <!-- cari uk
                                     <?php
                                        $query = $this->db->query("SELECT * FROM role p WHERE p.RoleParentId NOT IN ('','-') AND p.GroleId = '" . $this->session->userdata('groleid') . "' AND p.gjabatanId = 'XxJyPn38Yh.6' ORDER BY p.RoleStatus ASC")->result();
                                        foreach ($query as $dat_people) {
                                        ?>
                                        <option value="<?= $dat_people->RoleId; ?>"><?= $dat_people->RoleDesc; ?></option>
                                    <?php
                                        }
                                    ?> -->
                                    <!-- cari uk
                                    <?php
                                    $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '6' ORDER BY p.PeopleIsActive ASC")->result();
                                    foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                    }
                                    ?> -->


                                    <!-- batas akhir perbaikan -->
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">Tembusan</div>
                            <div class="col-md-9">
                                <select name="tembusan[]" class="form-control chosen chosen-select" multiple id="tembusan">
                                    <!-- Perbaikan Eko 09-11-2019 -->
                                    <?php
                                    $query = $this->db->query("SELECT PeopleId, PeoplePosition, PeopleName FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND p.PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY p.PrimaryRoleId ASC")->result();
                                    foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                    }
                                    ?>
                                    <!-- batas akhir perbaikan -->
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3">Pesan</div>
                            <div class="col-md-9">
                                <textarea name="pesan" class="form-control mytextarea" id="" cols="30" rows="10"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="title" class="col-sm-3">Unggah Data
                            </label>
                            <div class="col-sm-9">
                                <div class="upload_teruskan"></div>
                                <div class="upload_teruskan_listed"></div>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3"></div>
                            <div class="col-md-9">
                                <button class="btn btn-danger">Simpan dan Kirim</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- TUTUP MODAL TERUSKAN -->

    <div id="k_teruskan_tu" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Teruskan Untuk Dinomeri tu</b></h4>
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/k_teruskan_tu'); ?>" class="form-horizontal" method="POST">

                    <div class="modal-body">
                        <div class="content">

                            <div class="form-group">
                                <input type="hidden" id="NId_Temp5" name="NId_Temp5">
                                <input type="hidden" id="GIR_Id5" name="GIR_Id5">
 
                                <div class="col-6">

                                    <label>PENOMORAN <sup class="text-danger">*</sup></label>
                                    <select name="TtdText" id="TtdText" class="form-control select2 TtdText" required>

                                        <?php
                                        foreach ($this->db->query('SELECT * FROM master_ttd where TtdText = "UArsip"')->result() as $data1) { ?>
                                            <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                                        <?php } ?>

                                        <?php
                                        $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
                                        foreach ($AtasanId as $dataz) {
                                            $xx2 = $dataz->PeopleId;
                                        }
                                        ?>

                                    </select>
                                    <input type="hidden" id="tmpid_tu" name="tmpid_tu" value="<?= $xx2; ?>">
                                </div>
                            </div>
                             <div class="form-group">
                                <div class="col-6">
                                    <select name="Approve_People" class="form-control" id="tu123">
                                      
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-6">
                                    <label> Pesan <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" name="pesan" id="pesan" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan dan Kirim </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

   <div id="k_teruskan_uk" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Teruskan Untuk Dinomeri</b></h4>
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/k_teruskan_uk'); ?>" class="form-horizontal" method="POST">

                    <div class="modal-body">
                        <div class="content">

                            <div class="form-group">
                                <input type="hidden" id="NId_Temp8" name="NId_Temp8">
                                <input type="hidden" id="GIR_Id8" name="GIR_Id8">

                                <div class="col-6">

                                    <label>PENOMORAN <sup class="text-danger">*</sup></label>
                                    <select name="TtdText" id="TtdText" class="form-control select2 TtdText" required>

                                        <?php
                                        foreach ($this->db->query('SELECT * FROM master_ttd where TtdText = "UArsip"')->result() as $data1) { ?>
                                            <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                                        <?php } ?>
                                    </select>

                                    <?php
                                    $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
                                    foreach ($AtasanId as $dataz) {
                                        $xx3 = $dataz->PeopleId;
                                    }
                                    ?>
                                    <input type="hidden" id="tmpid_uk" name="tmpid_uk" value="<?= $xx3; ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-6 Approve_People">
                                 <select name="Approve_People" class="form-control chosen chosen-select" multiple id="Approve_People" required="">
                                        <!-- Perbaikan Eko 09-11-2019 -->
                                        <!-- penambahan dispu -->
                                        $search = array('6,8');
                                        <?php
                                        $age = $this->session->userdata('roleid') == 'uk.1';
                                        $sekdispusipda = $this->session->userdata('roleid') == 'uk.1.19.1.1';
                                        $xx = $this->session->userdata('roleid');

                                 // $konsep123 = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '".$this->session->userdata('roleid')."' AND GroupId IN ('6','8') ORDER BY PrimaryRoleId ASC")->result();
                                        $konsep123 = $this->db->query("SELECT PeopleId, PeoplePosition FROM v_login WHERE GroleId = '" . $this->session->userdata('groleid') . "' AND PeopleIsActive = '1'  AND GroupId IN ('6','8') ORDER BY GroleId ASC")->result();

                                        $konsep1234 = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND PrimaryRoleId  IN ('uk.1.1.1.1.1','uk.1') AND GroupId IN ('6','8') ORDER BY PrimaryRoleId ASC")->result();

                                        $konsepsekdis = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND PrimaryRoleId  IN ('uk.1.19','uk.1.19.1.1') AND GroupId IN ('6','8') ORDER BY PrimaryRoleId ASC")->result();

                                        if ($age == 'uk.1')
                                            foreach ($konsep1234  as $atasan) {
                                                ?>
                                                <option value="<?= $atasan->PeopleId; ?>">
                                                    <?= $atasan->PeoplePosition; ?></option>
                                                    <?php
                                                }

                                                else if ($sekdispusipda == 'uk.1.19.1.1')
                                                    foreach ($konsepsekdis  as $atasan) {
                                                        ?>
                                                        <option value="<?= $atasan->PeopleId; ?>">
                                                            <?= $atasan->PeoplePosition; ?></option>
                                                            <?php
                                                        }

                                                        else foreach ($konsep123  as $atasan) {
                                                            ?>
                                                            <option value="<?= $atasan->PeopleId; ?>">
                                                                <?= $atasan->PeoplePosition; ?></option>
                                                                <?php
                                                            }
                                                            ?>

                                        <!-- akhir penambahan dispu -->
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <label>Pesan <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" name="pesan" id="pesan" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan dan Kirim </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


 <div id="k_tu_ttdnotapimpinan" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <?php 
                    $role_atasan = $this->session->userdata('roleatasan');

                    $cek_ada_tu= $this->db->query("SELECT PeopleId, PeopleName,  PeoplePosition FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '".$this->session->userdata('roleid')."' AND GroupId IN ('8') ORDER BY PrimaryRoleId ASC")->result();

                    if (!$cek_ada_tu){

                     $konsep123 = $this->db->query("SELECT PeopleId, PeopleName, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '". $role_atasan ."' AND GroupId IN ('8') ORDER BY PrimaryRoleId ASC")->result();

                 }else{

                    $konsep123 = $this->db->query("SELECT PeopleId,PeopleName, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '".$this->session->userdata('roleid')."' AND GroupId IN ('8') ORDER BY PrimaryRoleId ASC")->result();
                }

                foreach ($konsep123 as $dat_people) {
                    ?>
                    <h4 class="modal-title">Teruskan Untuk Dinomeri
                        <b><?= $dat_people->PeopleName; ?></b></h4>
                        <?php
                    }
                    ?>
                   <!--  <h4 class="modal-title"><b>Teruskan Untuk Dinomeri</b></h4> -->
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/k_tu_ttdnotapimpinan'); ?>" class="form-horizontal" method="POST">

                    <div class="modal-body">
                        <div class="content">

                            <div class="form-group">
                                <input type="hidden" id="NId_Temp9" name="NId_Temp9">
                                <input type="hidden" id="GIR_Id9" name="GIR_Id9">

                                <div class="col-6">

                                    <label>PENOMORAN <sup class="text-danger">*</sup></label>
                                    <select name="TtdText" id="TtdText" class="form-control select2 TtdText" required>


                                        <?php


                                         foreach ($this->db->query('SELECT * FROM master_ttd where TtdText = "UArsip"')->result() as $data1) { ?>
                                            <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                                        <?php } ?>
                                    </select>

                                    <?php
                                    $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
                                    foreach ($AtasanId as $dataz) {
                                        $xx3 = $dataz->PeopleId;
                                    }
                                    ?>


                                    <input type="hidden" id="tmpid_uk" name="tmpid_uk" value="<?= $xx3; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6 Approve_People">
                                 <select name="Approve_People" class="form-control select2" id="Approve_People" required>
                                        <!-- Perbaikan Eko 09-11-2019 -->
                                        <!-- penambahan dispu -->
                                      
                                        <?php
                                        $age = $this->session->userdata('roleid') == 'uk.1';
                                        $sekdispusipda = $this->session->userdata('roleid') == 'uk.1.19.1.1';
                                        $xx = $this->session->userdata('roleid');
                                        $role_atasan = $this->session->userdata('roleatasan');

                                        $cek_ada_tu= $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '".$this->session->userdata('roleid')."' AND GroupId IN ('8') ORDER BY PrimaryRoleId ASC")->result();

                                        if (!$cek_ada_tu){

                                           $konsep123 = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '". $role_atasan ."' AND GroupId IN ('8') ORDER BY PrimaryRoleId ASC")->result();

                                       }else{

                                        $konsep123 = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '".$this->session->userdata('roleid')."' AND GroupId IN ('8') ORDER BY PrimaryRoleId ASC")->result();
                                       }

                                        $konsep1234 = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND PrimaryRoleId  IN ('uk.1.1.1.1.1','uk.1') AND GroupId IN ('6','8') ORDER BY PrimaryRoleId ASC")->result();

                                        $konsepsekdis = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND PrimaryRoleId  IN ('uk.1.19','uk.1.19.1.1') AND GroupId IN ('6','8') ORDER BY PrimaryRoleId ASC")->result();

                                        if ($age == 'uk.1')
                                            foreach ($konsep1234  as $atasan) {
                                                ?>
                                                <option value="<?= $atasan->PeopleId; ?>">
                                                    <?= $atasan->PeoplePosition; ?></option>
                                                    <?php
                                                }

                                                else if ($sekdispusipda == 'uk.1.19.1.1')
                                                    // foreach ($konsepsekdis  as $atasan) {

                                                        foreach ($konsep123  as $atasan) {
                                                        ?>
                                                        <option value="<?= $atasan->PeopleId; ?>">
                                                            <?= $atasan->PeoplePosition; ?></option>
                                                            <?php
                                                        }

                                                        else foreach ($konsep123  as $atasan) {
                                                            ?>
                                                            <option value="<?= $atasan->PeopleId; ?>">
                                                                <?= $atasan->PeoplePosition; ?></option>
                                                                <?php
                                                            }
                                                            ?>

                                        <!-- akhir penambahan dispu -->
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <label>Pesan <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" name="pesan" id="pesan" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan dan Kirim </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

  <div id="k_teruskan_surat_dinas" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Teruskan Untuk Dinomeri</b></h4>
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/k_teruskan_surat_dinas'); ?>" class="form-horizontal" method="POST">

                    <div class="modal-body">
                        <div class="content">

                            <div class="form-group">
                                <input type="hidden" id="NId_Temp6" name="NId_Temp6">
                                <input type="hidden" id="GIR_Id6" name="GIR_Id6">

                                <div class="col-6">

                                    <label>PENOMORAN <sup class="text-danger">*</sup></label>
                                    <select name="TtdText" id="TtdText" class="form-control select2 TtdText" required>

                                        <?php
                                         foreach ($this->db->query('SELECT * FROM master_ttd where TtdText = "UArsip"')->result() as $data1) { ?>
                                            <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                                        <?php } ?>
                                    </select>

                                    <?php
                                    $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
                                    foreach ($AtasanId as $dataz) {
                                        $xx3 = $dataz->PeopleId;
                                    }
                                    ?>

                                    <input type="hidden" id="tmpid_sudin" name="tmpid_sudin" value="<?= $xx3; ?>">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6 Approve_People">

                                   <select name="Approve_People" id="Approve_People" class="form-control select2" required="">  
                                 <!--  <select name="Approve_People" class="form-control chosen chosen-select" multiple id="Approve_People"> -->
                                        <!-- Perbaikan Eko 09-11-2019 -->
                                        <!-- penambahan dispu -->
                                     
                                        <?php
                                        $age = $this->session->userdata('roleid') == 'uk.1';
                                        $sekdispusipda = $this->session->userdata('roleid') == 'uk.1.19.1.1';
                                        $xx = $this->session->userdata('roleid');

                                 // $konsep123 = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '".$this->session->userdata('roleid')."' AND GroupId IN ('6','8') ORDER BY PrimaryRoleId ASC")->result();


                                       $konsep123 = $this->db->query("SELECT PeopleId, PeoplePosition FROM v_login WHERE GroleId = '" . $this->session->userdata('groleid') . "' AND PeopleIsActive = '1'  AND GroupId ='6' ORDER BY GroleId ASC")->result();

                                        $konsep1234 = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND PrimaryRoleId  IN ('uk.1.1.1.1.1','uk.1') AND GroupId IN ('6','8') ORDER BY PrimaryRoleId ASC")->result();

                                        $konsepsekdis = $this->db->query("SELECT PeopleId, PeoplePosition FROM people WHERE PeopleIsActive = '1' AND PrimaryRoleId  IN ('uk.1.19','uk.1.19.1.1') AND GroupId IN ('6') ORDER BY PrimaryRoleId ASC")->result();

                                        if ($age == 'uk.1')
                                            foreach ($konsep1234  as $atasan) {
                                                ?>
                                                <option value="<?= $atasan->PeopleId; ?>">
                                                    <?= $atasan->PeoplePosition; ?></option>
                                                    <?php
                                                }

                                                else if ($sekdispusipda == 'uk.1.19.1.1')
                                                    foreach ($konsepsekdis  as $atasan) {
                                                        ?>
                                                        <option value="<?= $atasan->PeopleId; ?>">
                                                            <?= $atasan->PeoplePosition; ?></option>
                                                            <?php
                                                        }

                                                        else foreach ($konsep123  as $atasan) {
                                                            ?>
                                                            <option value="<?= $atasan->PeopleId; ?>">
                                                                <?= $atasan->PeoplePosition; ?></option>
                                                                <?php
                                                            }
                                                            ?>

                                        <!-- akhir penambahan dispu -->
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <label>Pesan <sup class="text-danger">*</sup></label>
                                    <input type="text" class="form-control" name="pesan" id="pesan" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-6"></div>
                            </div>

                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan dan Kirim </button>
                                </div>
                            </div>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div> 

</section>
<!-- /.content -->

<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>
<script src="<?= BASE_ASSET; ?>/select3/select3.min.js"></script>
<script type="text/javascript">
    $(' .TtdText').change(function() {
        if ($(this).find(':selected').val() == 'none') {
            $('.Approve_People').addClass('hidden');
        } else {
            $('.Approve_People').removeClass('hidden');
            r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
            $('#Approve_People').select2();
            $('#Approve_People').attr('disabled', false);
            if (this.value == "AL") {
                idatasan = $("#tmpid_atasan").val();
                $('#Approve_People').val(idatasan).change();
                $('#Approve_People').attr('disabled', true);
            }
            if (this.value == "SEKDIS") {
                iidatasan = $("#tmpid_sekdis").val();
                $('#Approve_People').val(iidatasan).change();
                $('#Approve_People').attr('disabled', true);
            }
        }
    });

    $(' .nomortu').change(function() {
        if ($(this).find(':selected').val() == 'none') {
            $('.Approve_tu').addClass('hidden');
        } else {
            $('.Approve_tu').removeClass('hidden');
            r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
            $('#Approve_tu').select3();
            $('#Approve_tu').attr('disabled', false);
            if (this.value == "UArsip") {
                idatasan = $("#tmpid_tu").val();
                $('#Approve_tu').val(idatasan).change();

                $('#Approve_tu').attr('disabled', true);
            }
        }
    });

    $(document).ready(function() {

        var dataTable = $('#myTable').DataTable({

            "processing": true,
            "serverSide": true,
            // "order": [
            //     [1, "desc"]
            // ],

            "ajax": {
                "url": "<?php echo site_url('administrator/anri_list_naskah_belum_approve/get_data_naskah_ba') ?>",
                "type": "POST",
                //deferloading untuk menampung 10 data pertama dulu
                "deferLoading": 10,
            },


            "columnDefs": [{
                "targets": [0, 5, 6],
                "orderable": false,
            }, ],

        });

    });

    function ko_verifikasi(NId, GIR_Id) {
        $("#NId_Temp").val(NId);
        $("#GIR_Id").val(GIR_Id);
        $("#approve_naskah").modal('show');
    }

    function ko_teruskan(NId, GIR_Id) {
        $("#NId_Temp2").val(NId);
        $("#GIR_Id2").val(GIR_Id);
        $("#k_teruskan").modal('show');
    }

     function ko_teruskan_tu(NId, GIR_Id) {
      
        $("#NId_Temp5").val(NId);
        $("#GIR_Id5").val(GIR_Id);
        var saveData = $.ajax({
              type: 'POST',
              url: "<?php echo site_url('administrator/anri_list_naskah_belum_approve/filter_uk_tu') ?>",
              data: {nid: NId,
                gird: GIR_Id

              },
              success: function(resultData) { 
                 
                var stringify = JSON.parse(resultData);
                $('#tu123').html("");
                for (var i = 0; i < stringify.length; i++) {
                    // alert(stringify[i]['PeopleId'] + stringify[i]['PeoplePosition']);
                     $('#tu123')
                     .append($("<option></option>")
                                .attr("value", stringify[i]['PeopleId'])
                                .text(stringify[i]['PeoplePosition'])); 
                }
            
            }
              
        });
        saveData.error(function() { alert("Something went wrong"); });
        $("#k_teruskan_tu").modal('show');
    }

    function ko_nomor(NId, GIR_Id) {
        $("#NId_Temp3").val(NId);
        $("#GIR_Id3").val(GIR_Id);
        $("#nomor").modal('show');
    }

    function ko_teruskan_uk(NId, GIR_Id) {
        $("#NId_Temp8").val(NId);
        $("#GIR_Id8").val(GIR_Id);
        $("#k_teruskan_uk").modal('show');
    }

    function ko_tu_ttdnotapimpinan(NId, GIR_Id) {
        $("#NId_Temp9").val(NId);
        $("#GIR_Id9").val(GIR_Id);
        $("#k_tu_ttdnotapimpinan").modal('show');
    }

    function ko_teruskan_surat_dinas(NId, GIR_Id) {
        $("#NId_Temp6").val(NId);
        $("#GIR_Id6").val(GIR_Id);
        $("#k_teruskan_surat_dinas").modal('show');
    }

</script>