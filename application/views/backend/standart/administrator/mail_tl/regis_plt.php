<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<section class="content-header">
    <h1><?= $title; ?></h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active"><?= $title; ?></li>
    </ol>
</section>
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <div class="box box-widget widget-user-2">
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <h3 class="widget-user-username"><?= $title; ?></h3>
                            <hr>
                        </div>
                        <?= form_open_multipart(BASE_URL('administrator/anri_dashboard/post_naskah_masuk_plt_regis'), ['name'    => 'form_inbox', 'class'   => 'form-horizontal', 'id'      => 'form_inbox', 'method'  => 'POST']); ?>
                        <div class="form-group ">
                            <label for="NTglReg" class="col-sm-2 control-label">Tanggal Registrasi</label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right" disabled name="NTglReg"  id="NTglReg" value="<?= date('d-m-Y') ?>">
                                </div>
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Kepada_Yth" class="col-sm-2 control-label">Kepada Yth </label>
                            <div class="col-sm-8">
                                <select name="Kepada_Yth" class="form-control chosen chosen-select">
                                    <option value="">-- Pilih User --</option>
                                    <?php
                                        foreach ($people as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div> 
                        
                        <br>
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <br>
                            <button class="btn btn-primary">Simpan</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>