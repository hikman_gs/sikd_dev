<?php

$z = $this->db->query("SELECT PrimaryRoleId FROM people WHERE PeopleId = '".$Approve_People."'")->row()->PrimaryRoleId;

$sql_header = $this->db->query("SELECT Header, Lokasi FROM v_kopnaskah WHERE RoleId = '".$z."'")->row();

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>test</title>
</head>
<body> 

<img style="width: 100%" src="<?= base_url('FilesUploaded/kop/'.$sql_header->Header); ?>"> 

<br><br>
<table style="width: 100%!important; font-family: Arial; font-size: 12px;">
	<tr>
		<td style="line-height: 15px;">Nomor</td>
		<td style="line-height: 15px;">:</td>
		<td style="line-height: 15px;"><?= $nosurat; ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?= $lokasi; ?>, <?= get_bulan($TglNaskah) ?></td>
	</tr>
	<tr>
		<td style="line-height: 15px;">Sifat</td>
		<td style="line-height: 15px;">:</td>
		<td style="line-height: 15px;"><?= $SifatId; ?></td>
	</tr>
	<tr>
	  <?php
		if($Jumlah != 0) {
	  ?>
		<td style="line-height: 15px;">Lampiran</td>
		<td style="line-height: 15px;">:</td>
		<td style="line-height: 15px;">
			<?php 
				echo $Jumlah.' '.$MeasureUnitId;
			?>				
		</td>
	  <?php } ?>	
	</tr>	
	<tr>
		<td style="line-height: 15px;">Hal</td>
		<td style="line-height: 15px;">:</td>
		<td style="line-height: 15px;"><?= $Hal; ?></td>
	</tr>
</table>

<br><br>
<table style="width: 100%!important; font-family: Arial; font-size: 12px;">
	<tr>
		<td valign="top" style="line-height: 15px;">Yth</td>
		<td valign="top" style="line-height: 15px;">:</td>
		<td valign="top" style="line-height: 15px;">

			<?php 
				$exp = explode(',', $RoleId_To);
				$z = count($exp);				
				if ($z > 1) {
					$n = 1;
					foreach ($exp as $k => $v) {
						$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
						echo $n.'. '.$xx1.'<br>';
						$n++;
					}
				}else{
					$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$exp[0]."'")->row()->RoleName;
					echo $xx1;
				}
			?>
		</td>
	</tr>
</table>	

<br>

<div style="width: 98%; font-family: Arial; font-size: 12px; text-align: justify; margin-left: 5px; line-height: 15px;">
	<?= $Konten; ?>
</div>

<div style="width: 50%; font-family: Arial; font-size: 14px; line-height: 20px; margin-left: 300px; margin-top: 50px;">

	<?php if($TtdText == 'PLT') { ?>
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?></p>
	<?php } elseif($TtdText == 'PLH') { ?>
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?></p>
	<?php } else { ?>	
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?></p>
	<?php } ?>	

	<?php 
		$xyz  = $this->db->query("SELECT QRCode FROM ttd WHERE NId='".$NId."'")->num_rows();

		if ($xyz > 0) { ?>

			<img style="float: right!important; margin-left: 117px; width: 70px;" src="<?= base_url('FilesUploaded/qrcode/'.$this->db->query("SELECT QRCode FROM ttd WHERE NId='".$NId."'")->row()->QRCode) ?>" alt="">

		<?php } else { ?>

			<br><br><br>

		<?php } ?>

	<p style="float: right!important; text-align: center; font-family: Arial; font-size: 12px;"><?= $Nama_ttd_konsep; ?></p>
</div>

<br><br>

<table width="100%" style="font-family: Arial; font-size: 12px; line-height: 15px;">
	<tr>
		<td width="10%" valign="top">
			<?php
				$exp = explode(',', $RoleId_Cc);
				$z = count($exp);
				if ($z > 1) {
					$n = 1;
					echo "Tembusan : <br>";
					foreach ($exp as $k => $v) {
						$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
						echo $n.'. '.$xx1.'<br>';
						$n++;
					}
				}else{
					if (!empty($RoleId_Cc)) {
						if($RoleId_Cc != '') {
							echo "Tembusan : <br>";
						}					
						$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$exp[0]."'")->row()->RoleName;
						echo $xx1;
					}	
				}
			?>
		</td>
	</tr>
</table>

<br>
<page_footer>
<table width="100%" style="font-family: Arial; font-size: 10px; line-height: 15px;">
	<tr>
		<td width="100%" valign="top">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			Dokumen ini telah ditandatangani secara elektronik yang diterbitkan oleh Balai Sertifikasi Elektronik (BSrE), BSSN
		</td>
	</tr>
</table>
</page_footer>

</body>
</html>