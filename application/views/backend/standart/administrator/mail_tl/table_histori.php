<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title><?= get_option('site_name'); ?></title>
  <link rel="shortcut icon" href="<?= BASE_ASSET; ?>/favicon.ico">

  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/morris/morris.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/sweet-alert/sweetalert.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/toastr/build/toastr.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/chosen/chosen.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/custom.css?timestamp=201803311526">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>datetimepicker/jquery.datetimepicker.css"/>
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>js-scroll/style/jquery.jscrollpane.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>flag-icon/css/flag-icon.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" media="all" />
  <?= $this->cc_html->getCssFileTop(); ?>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/sweet-alert/sweetalert-dev.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <script src="<?= BASE_ASSET; ?>/toastr/toastr.js"></script>
  <script src="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.js?v=2.1.5"></script>
  <script src="<?= BASE_ASSET; ?>/datetimepicker/build/jquery.datetimepicker.full.js"></script>
  <script src="<?= BASE_ASSET; ?>/editor/dist/js/medium-editor.js"></script>
  <script src="<?= BASE_ASSET; ?>js/cc-extension.js"></script>
  <script src="<?= BASE_ASSET; ?>/js/cc-page-element.js"></script>
</head>
<body style="padding: 10px;">
  <h3 class="widget-user-username" align="right"><font color="red"><b>Histori Naskah</b></font></h3>
  <hr>
  <div class="table-responsive"> 
    <table id="example1" class="table table-bordered table-striped dataTable">
       <thead>
          <tr class="">
             <th>No</th>
             <th>Tanggal</th>
             <th>Asal Naskah</th>
             <th>Tujuan Naskah</th>
             <th>Keterangan</th>
             <th>Pesan</th>
          </tr>
       </thead>
       <tbody id="tbody_inbox">
       <?php 
        // $this->db->select('*');
        // $this->db->group_by('GIR_Id'); 
        // $this->db->where('NId', $NId); 
        // $this->db->order_by('ReceiveDate','DESC');
        // $inboxs = $this->db->get('inbox_receiver')->result();

        $inboxs = [];

        // ambil data pertama kali surat masuk
        $surat_awal = $this->db 
            ->order_by('ReceiveDate', 'ASC')
            ->group_by('GIR_Id')
            ->get_where('inbox_receiver', ['NId' => $NId])
            ->row();

        // if($surat_awal != null) {
        //   $inboxs[] = $surat_awal;
        // }

      //penambahan
             if (!$surat_awal->ReceiverAs='to'){
              if($surat_awal != null) {
                $inboxs[] = $surat_awal;
              }
            }
      //akhir penambahan

        // ambil data histori surat yang ditunjukan untuk diri sendiri dan bawahan
        $surat_by_role = $inbox_by_role = $this->db
          ->order_by('ReceiveDate', 'ASC')
          ->group_by('GIR_Id')
          ->like('RoleId_To', $this->session->userdata('roleid'), 'after')
          ->where( ['NId' => $NId,])
          ->get('inbox_receiver')
          ->result();

        foreach ($surat_by_role as $key => $value) {
          $inboxs[] = $value;
        }

        // ambil semua surat yang dikirim dari diri sendiri dan bawahan
        $surat_dari_sendiri = $this->db
          ->order_by('ReceiveDate', 'ASC')
          ->group_by('GIR_Id')
          ->like('RoleId_From', $this->session->userdata('roleid'), 'after')
          ->where( ['NId' => $NId])
          ->get('inbox_receiver')
          ->result();

        foreach ($surat_dari_sendiri as $key => $value) {
          $inboxs[] = $value;
        }
        
        // group by GIR_Id
        $inboxs_group = [];
        foreach ($inboxs as $key => $value) {
          $inboxs_group[$value->GIR_Id] = $value;
        }

        // sort berdasarkan tanggal
        // referensi https://stackoverflow.com/questions/1597736/how-to-sort-an-array-of-associative-arrays-by-value-of-a-given-key-in-php
        usort($inboxs_group, function($item1, $item2) {
          return $item2->ReceiveDate <=> $item1->ReceiveDate;
        });
        
       $i = 1;
       foreach($inboxs_group as $k => $inbox): ?>

       <!-- foreach($inboxs as $k => $inbox): ?> -->
          <tr>
              <td><?= $k+1; ?></td>
              <td><?= date('d-m-Y H:i:s',strtotime($inbox->ReceiveDate)) ?></td> 
              <td>
              <!--   <?php 
                if ($inbox->ReceiverAs == 'to') {
                  echo $this->db->query("SELECT Instansipengirim FROM inbox WHERE NId='".$NId."'")->row()->Instansipengirim;
                }else{
                  echo $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$inbox->RoleId_From."'")->row()->RoleName;
                }
                ?> -->

               <!--  perubahan -->
               
                 <?php 
                if ($inbox->ReceiverAs == 'to') {
                 //penambahan kirim ke dinas lain
                  $cek_pengirim=$this->db->query("SELECT Instansipengirim FROM inbox WHERE NId='".$NId."'")->row()->Instansipengirim;

                  if(!$cek_pengirim){

                    echo $this->db->query("SELECT Namapengirim FROM inbox WHERE NId='".$NId."'")->row()->Namapengirim;

                  }else{

                     echo $this->db->query("SELECT Instansipengirim FROM inbox WHERE NId='".$NId."'")->row()->Instansipengirim;

                  }
                    // akhir penambahan kirim ke dinas lain
                 
                }else{
                  echo $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$inbox->RoleId_From."'")->row()->RoleName;
                }
                ?>
               <!--  akhir perubahan -->

              </td>
              <td>
                <?php
                  $tujuan = $this->db->query("SELECT * FROM inbox_receiver WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs != 'bcc'")->result();
                    echo '<ul>';
                    foreach($tujuan as $key => $dat_tujuan) {
                      if($dat_tujuan->StatusReceive=='unread')
                        { 
                          if($dat_tujuan->RoleId_To == $this->session->userdata('roleid')){
                            echo '<li><font color="red" style="background-color: yellow"><b>'.get_data_people('PeopleName',$dat_tujuan->To_Id).' -- '.get_data_people('RoleName',$dat_tujuan->To_Id).' (Belum Dibaca)</b></font></li>';
                          } else {
                            echo '<li><font color="red">'.get_data_people('PeopleName',$dat_tujuan->To_Id).' -- '.get_data_people('RoleName',$dat_tujuan->To_Id).' (Belum Dibaca)</font></li>';
                          }
                        }
                      else
                        {
                          if($dat_tujuan->RoleId_To == $this->session->userdata('roleid')){
                            echo '<li><font color="brown" style="background-color: yellow"><b>'.get_data_people('PeopleName',$dat_tujuan->To_Id).' -- '.get_data_people('RoleName',$dat_tujuan->To_Id).' (Dibaca)</b></font></li>';
                          } else {
                            echo '<li>'.get_data_people('PeopleName',$dat_tujuan->To_Id).' -- '.get_data_people('RoleName',$dat_tujuan->To_Id).' (Dibaca)</li>';
                          }                                                   
                        }    
                    }
                    echo '</ul>';

                  $tembusan = $this->db->query("SELECT * FROM inbox_receiver WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs ='bcc'")->result();
                  if(count($tembusan)){
                    echo 'Tembusan : <br><ul>';
                      foreach($tembusan as $key => $dat_tembusan) {
                        if($dat_tembusan->StatusReceive=='unread')
                          { 
                            if($dat_tembusan->RoleId_To == $this->session->userdata('roleid')){
                              echo '<li><font color="red" style="background-color: yellow"><b>'.get_data_people('PeopleName',$dat_tembusan->To_Id).' -- '.get_data_people('RoleName',$dat_tembusan->To_Id).' (Belum Dibaca)</b></font></li>';
                            } else {
                              echo '<li><font color="red">'.get_data_people('PeopleName',$dat_tembusan->To_Id).' -- '.get_data_people('RoleName',$dat_tembusan->To_Id).' (Belum Dibaca)</font></li>';
                            }
                          }
                        else
                          {
                            if($dat_tembusan->RoleId_To == $this->session->userdata('roleid')){
                              echo '<li><font color="brown" style="background-color: yellow"><b>'.get_data_people('PeopleName',$dat_tembusan->To_Id).' -- '.get_data_people('RoleName',$dat_tembusan->To_Id).' (Dibaca)</b></font></li>';
                            } else {
                              echo '<li>'.get_data_people('PeopleName',$dat_tembusan->To_Id).' -- '.get_data_people('RoleName',$dat_tembusan->To_Id).' (Dibaca)</li>';
                            }
                          }  
                      }
                    echo '</ul>';
                  }
                ?>
              </td> 
              <td>
                <?php 
                  $t1 = $this->db->query("SELECT ReceiverAs FROM inbox_receiver WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs != 'bcc'")->row()->ReceiverAs;

                  if ($t1 == 'to') {
                    echo "Naskah Masuk";
                  }elseif($t1 == 'to_undangan'){
                    echo "Undangan";
                  }elseif($t1 == 'to_sprint'){
                    echo "Surat Perintah";
                  }elseif($t1 == 'to_notadinas'){
                    echo "Nota Dinas";
                  }elseif($t1 == 'to_reply'){
                    echo "Nota Dinas";
                  }elseif($t1 == 'to_usul'){
                    echo "Jawaban Nota Dinas";
                  }elseif($t1 == 'to_forward'){
                    echo "Teruskan";
                  }elseif($t1 == 'cc1'){
                    echo "Disposisi";
                  }elseif($t1 == 'to_keluar'){
                    echo "Surat Dinas Keluar";
                  }elseif($t1 == 'to_nadin'){
                    echo "Naskah Dinas Lainnya";
                  }elseif($t1 == 'to_konsep'){
                    echo "Konsep Naskah";
                  }elseif($t1 == 'to_memo'){
                    echo "Memo";
                  }elseif ($t1 == 'to_draft_notadinas') {
                    echo "Konsep Nota Dinas";
                  }else if($t1 == 'to_draft_sprint'){ 
                    echo "Konsep Surat Perintah";
                  }else if($t1 == 'to_draft_undangan'){ 
                    echo "Konsep Undangan";
                  }else if($t1 == 'to_draft_keluar'){ 
                    echo "Konsep surat Dinas";
                  }else if($t1 == 'to_draft_sket'){ 
                    echo "Konsep surat Keterangan";
                  }else if($t1 == 'to_draft_pengumuman'){ 
                    echo "Konsep Pengumuman";
                  }else if($t1 == 'to_draft_rekomendasi'){ 
                    echo "Konsep Surat Rekomendasi";
                  }
                  else {
                    echo "Konsep Naskah Dinas Lainnya";
                  }
                ?>
              </td>
              <td>
                  <?php
                
                  if($inbox->ReceiverAs == 'cc1'){

                      $count_disposisi  = $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."'")->num_rows();
                      if($count_disposisi > 0){
                      $disposisi  = $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."'")->result();
                        foreach ($disposisi as $key => $value) {            
                          $exp = explode('|', $value->Disposisi);
                          if($value->Disposisi == '-'){
                            echo '';
                          } else {
                            foreach ($exp as $k => $v) {
                              $this->db->select('DisposisiName');
                              $dispo = $this->db->query("SELECT DisposisiName FROM master_disposisi WHERE DisposisiId = '".$v."'")->result();
                              foreach ($dispo as $a => $x) {
                                  echo '<b>- </b>'.$x->DisposisiName;
                              }
                              echo '</br>';
                            }  
                            echo '<p></p>';
                          }       
                        }
                      }
                  }
                  if($inbox->ReceiverAs == 'cc1') {
                      if($inbox->Msg != '') { ?>
                          <font color="red"><b>Pesan Disposisi :</b></font>
                          <?= $inbox->Msg ?>    
                      <?php } 
                  } elseif($inbox->ReceiverAs == 'to_draft_notadinas') {       
                      if($inbox->Msg != '') { ?>
                          <?= $inbox->Msg ?>    
                      <?php } 
                  } elseif($inbox->ReceiverAs == 'to_draft_sprint') {       
                      if($inbox->Msg != '') { ?>
                          <?= $inbox->Msg ?>    
                      <?php } 
                  } elseif($inbox->ReceiverAs == 'to_draft_undangan') {       
                      if($inbox->Msg != '') { ?>
                          <?= $inbox->Msg ?>    
                      <?php } 
                  } elseif($inbox->ReceiverAs == 'to_draft_suratdinas') {       
                      if($inbox->Msg != '') { ?>
                          <?= $inbox->Msg ?>    
                      <?php }                       
                  } elseif($inbox->ReceiverAs == 'to_draft_rekomendasi') {       
                      if($inbox->Msg != '') { ?>
                          <?= $inbox->Msg ?>    
                      <?php }                       
                  }
                  elseif($inbox->ReceiverAs == 'to_draft_keluar') {       
                      if($inbox->Msg != '') { ?>
                          <?= $inbox->Msg ?>    
                      <?php }  
                  } ?>     
              </td>
          </tr>
        <?php endforeach; ?>
       </tbody>
    </table>
  </div>
<script src="<?= BASE_ASSET; ?>/js/chosen.jquery.min.js" type="text/javascript"></script>
<script src="<?= BASE_ASSET; ?>jquery-switch-button/jquery.switchButton.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.ui.touch-punch.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/bootstrap/js/bootstrap.min.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/fastclick/fastclick.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/dist/js/app.min.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/dist/js/adminlte.js"></script>
<script src="<?= BASE_ASSET; ?>js-scroll/script/jquery.jscrollpane.min.js"></script>
<script src="<?= BASE_ASSET; ?>/js/custom.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
  $(function () {
    $('#example1').DataTable();
  })
</script>
</body>
</html>