<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
	<style>
		p.indent {

			padding-left: 6.8em
		}

		h1 {
			margin: 0;
			padding: 20px 0 20px 0;
			background-color: #77D312;
			color: #fff;
			font-size: 1.7em;
			font-weight: 700;
			text-align: center;
		}

		.container {
			margin-bottom: 30px;
			padding: 0 20px 20px 20px;
			width: 700px;
			padding-bottom: 7px;
		}

		#footer2 {
			position: fixed;
			right: 0px;
			bottom: 10px;
			text-align: center;
			border-top: 1px solid black;
		}

		#footer {
			position: absolute;
			bottom: 0;
			width: 100%;
			height: 60px;
			/* tinggi dari footer */
			vertical-align: bottom;
		}

		@page: first {
			size: 5.5in 8.5in;
			margin-top: -10px;

		}

		.configlampiran {
			size: F4;
			margin-top: -10px;


		}

		#footer .page:after {
			size: 5.5in 8.5in;
			content: counter(page, decimal);
			margin-top: -10px;

		}


		footer {
			margin-top: 20px;
			color: #999999;
			text-align: center;
		}
	</style>
</head>

<body>
	<div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -86px;">
		<img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
	</div>

	<br><br>
	<table style="margin: auto; font-family: Arial; font-size: 12px; line-height: 20px;">
		<tr>
			<td style="text-align: center;">SURAT PERINTAH</td>
		</tr>
		<tr>
			<td style="text-align: center;">NOMOR : <?= $nosurat ?></td>
		</tr>
	</table>
	<br>

	<table style="font-family: Arial; font-size: 12px; line-height: 20px; width: 600;">
		<tr style="overflow: hidden;" valign="top">
			<td style="text-align: justify; width: 100px;">DASAR</td>
			<td style="text-align: justify; width: 5px;">:</td>
			<td style="text-align: justify; width: 480;"><?= $Hal; ?>

			</td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td style="text-align: center;" colspan="10">MEMERINTAHKAN :</td>
		</tr>
	</table>
	<br><br>
	<!-- BARIS INI -->
	<table style="font-family: Arial; font-size: 12px; line-height: 20px; width: 400;">
		<tr>
			<td style="text-align: justify; width: 100px;" rowspan="1000" valign="top">Kepada</td>
			<td style="text-align: justify; width: 5px;" rowspan="1000" valign="top">:</td>
		</tr>
	</table>

		<?php

	$exp = explode(',', $RoleId_To);
	$z = count($exp);
	if ($z > 1) {
		$n = 1;

		foreach ($exp as $k => $v) {
			$xx1 =  $this->db->query("SELECT PeopleName FROM people WHERE PeopleId = '" . $v . "'")->row()->PeopleName;
			$xx2 =  $this->db->query("SELECT Pangkat FROM people WHERE PeopleId = '" . $v . "'")->row()->Pangkat;
			$xx3 =  $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $v . "'")->row()->NIP;
			$xx4 =  $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $v . "'")->row()->PeoplePosition;
				$uc  = ucwords(strtolower($xx4));
						$str = str_replace('Dan', 'dan', $uc);
						$str = str_replace('Uptd', 'UPTD', $uc);	

						echo '<table style="font-family: Arial; font-size: 12px; line-height: 20px;">
							<tr style="text-align: justify; line-height: 30px;">' .
							'<td style="text-align: justify; width: 100px;" rowspan="2000" valign="top"></td>
			<td style="text-align: justify; width: 5px;" rowspan="2000" valign="top"></td>' .
							'<td style="text-align: justify;" valign="top">' . $n . '. ' . '</td>' .
							'<td style="text-align: justify;" valign="top">' . 'Nama' . '</td>' .
							'<td style="text-align: justify;" valign="top">' . ':' . '</td>' .
							'<td style="text-align: justify; width: 400;" valign="top" >' . $xx1 . '</td>' .
							'</tr>' .

							'<tr>' .
							'<td style="text-align: justify;" >' . '</td>' .
							'<td style="text-align: justify;" valign="top">' . 'Pangkat' . '</td>' .
							'<td style="text-align: justify;"valign="top">' . ':' . '</td>' .
							'<td style="text-align: justify; width: 400;" valign="top">' . $xx2 . '</td>' .
							'</tr>' .

							'<tr>' .
							'<td style="text-align: justify;">' . '</td>' .
							'<td style="text-align: justify; ">' . 'NIP' . '</td>' .
							'<td style="text-align: justify; ">' . ':' . '</td>' .
							'<td style="text-align: justify; width: 400;">' . $xx3 . '</td>' .
							'</tr>' .

							'<tr>' .
							'<td style="text-align: justify;">' . '</td>' .
							'<td style="text-align: justify;" valign="top">' . 'Jabatan' . '</td>' .
							'<td style="text-align: justify;" valign="top">' . ':' . '</td>' .
							'<td style="text-align: justify; width: 400;" valign="top">' . $str . '</td>' .
							'</tr></table>';

						$n++;
					}
				} else {
						$xx1 =  $this->db->query("SELECT PeopleName FROM people WHERE PeopleId = '" . $exp[0] . "'")->row()->PeopleName;
		$xx2 =  $this->db->query("SELECT Pangkat FROM people WHERE PeopleId = '" . $exp[0] . "'")->row()->Pangkat;
		$xx3 =  $this->db->query("SELECT NIP FROM people WHERE PeopleId = '" . $exp[0] . "'")->row()->NIP;
		$xx4 =  $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $exp[0] . "'")->row()->PeoplePosition;
			$uc  = ucwords(strtolower($xx4));
						$str = str_replace('Dan', 'dan', $uc);
						$str = str_replace('Uptd', 'UPTD', $uc);

					echo '<table style="font-family: Arial; font-size: 12px; line-height: 20px;">'.

						'<tr>' .
						'<td style="text-align: justify; width: 100px;" rowspan="2000" valign="top"></td>
			<td style="text-align: justify; width: 5px;" rowspan="2000" valign="top"></td>' .
						'<td style="text-align: justify;" valign="top">' . $n . '</td>' .
						'<td style="text-align: justify;" valign="top">' . 'Nama' . '</td>' .
						'<td style="text-align: justify;" valign="top">' . ':' . '</td>' .
						'<td style="text-align: justify; width: 400;" valign="top">' . $xx1 . '</td>' .
						'</tr>'.

						'<tr>' .
						'<td style="text-align: justify;" valign="top">' . '</td>' .
						'<td style="text-align: justify;" valign="top">' . 'Pangkat' . '</td>' .
						'<td style="text-align: justify;" valign="top">' . ':' . '</td>' .
						'<td style="text-align: justify; width: 400;" valign="top" >' . $xx2 . '</td>' .
						'</tr>' .

						'<tr>' .
						'<td style="text-align: justify;" valign="top">' . '</td>' .
						'<td style="text-align: justify;" valign="top">' . 'NIP' . '</td>' .
						'<td style="text-align: justify;" valign="top">' . ':' . '</td>' .
						'<td style="text-align: justify; width: 400;" valign="top">' . $xx3 . '</td>' .
						'</tr>' .

						'<tr>' .
						'<td style="text-align: justify;" valign="top">' . '</td>' .
						'<td style="text-align: justify;" valign="top">' . 'Jabatan' . '</td>' .
						'<td style="text-align: justify;" valign="top">' . ':' . '</td>' .
						'<td style="text-align: justify; width: 400;" valign="top">' . $str . '</td>' .
						'</tr></table>';
				}
				?>


	<!-- Sampai Sini -->
	<br><br>


	<table style="text-align: justify; width: 470px; font-family: Arial; font-size: 12px; line-height: 20px;">
		<tr style="overflow: hidden;" valign="top">
			<td style="text-align: justify; width: 100px;" rowspan="5" valign="top">Untuk</td>
			<td style="text-align: justify; width: 5px;" rowspan="5" valign="top">:</td>
			<td style="text-align: justify; width: 470px;"><?= $Konten; ?></td>
		</tr>
	</table>


	<div style="width: 50%; font-family: Arial; font-size: 12px; margin-left: 300px; margin-top: 15px;">
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px; line-height: 1px;">Ditetapkan di <?= $lokasi; ?></p>
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Pada tanggal <?= get_bulan($TglNaskah) ?></p>

		<?php
$r_atasan =  $this->session->userdata('roleatasan'); 
$r_biro =  $this->session->userdata('groleid');
$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
?>
    <?php if($TtdText == 'PLT') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText == 'PLH') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
            <br><?= get_data_people('RoleName',$Nama_ttd_atas_nama) ?>,</p>
    <?php } else { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } ?>  
		
		<p align="center">PEMERIKSA</p>

		<table style='border: 1; font-family: Arial; font-size: 10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 25px; ' cellspacing="0">
			<tr>
				<td rowspan="5"> <img src="<?php echo base_url(); ?>/uploads/kosong.jpg" widht="55" height="60"></td>
				<td>Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr>
				<td style="overflow: hidden; width: 200px;"><?= get_data_people('RoleName', $Approve_People) ?>,</td>
			</tr>

			<tr>
				<td><br></td>
				<td><br></td>
				<td><br></td>
			</tr>

			<tr>
				<td>
					<p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
				</td>
			</tr>
			<tr>
				<td><?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat; ?>
				</td>
			</tr>

		</table>

		<br>
	</div>
	<page_footer>
        <table style="font-family: Arial; font-size: 9px; line-height: 15px; margin-top: 100px;" align="center">
            <tr>
                <td align="center">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </td>
            </tr>
        </table>
    </page_footer>
</body>

</html>