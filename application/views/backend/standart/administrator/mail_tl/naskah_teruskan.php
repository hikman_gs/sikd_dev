<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet" /> -->
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<style>
    .select2-container--default .select2-selection--single{border:0px;}
    .select2-container{
            border: 1px solid #DEDCDC!important;
            border-radius: 5px!important;
            padding: 2px 0px!important;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?= $title ?>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a  href="<?= site_url('administrator/anri_dashboard/list_naskah_belum_approve'); ?>">Naskah Belum Approve</a></li>
        <li class="active"><?= $title ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <h3 class="widget-user-username"><?= $title ?></h3>
                            <hr>
                        </div>
                        <form action="<?= BASE_URL('administrator/anri_dashboard/post_naskah_teruskan/'.$NId) ?>" class="form-horizontal" method="POST">
                          <div class="form-group">
                              <label class="col-sm-2 control-label">Approval</label>
                              <div class="col-md-8">
                                  <select name="Approve_People" class="form-control select2" id="tujuan">
                                      <option value="">-- Pilih User --</option>
                                    <!-- Perbaikan Eko 09-11-2019 -->
                                    <?php
                                        $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                                        foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName; ?></option>
                                    <?php
                                        }
                                    ?>
                                    <!-- batas akhir perbaikan -->
                                  </select>
                              </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label">Pesan</label>
                            <div class="col-md-8">
                              <textarea name="Konten" class="form-control mytextarea" id="" cols="30" rows="10"></textarea>
                            </div>
                          </div>
                          <div class="form-group">
                            <label class="col-sm-2 control-label"></label>
                            <div class="col-md-8">
                              <a href="<?= $this->agent->referrer(); ?>" class="btn btn-warning">Kembali</a>
                              <button class="btn btn-success">Simpan</button>
                            </div>
                          </div>
                          <br><br>
                        </form>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script> -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>
<script src="<?= BASE_ASSET; ?>/tinymce/tinymce.min.js"></script>
  <script>
  tinymce.init({
    selector: '.mytextarea'
  });
  $('.select2').select2();
</script>
<script>
    var params = {};
    params[csrf] = token;
    $('.upload_teruskan').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/anri_dashboard/upload_naskah_masuk_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/anri_dashboard/delete_naskah_masuk_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                            
          },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('.upload_teruskan').fineUploader('getUuid', id);
                   $('.upload_teruskan_listed').append('<input type="hidden" class="listed_file_uuid" name="upload_teruskan_uuid['+id+']" value="'+uuid+'" /><input type="hidden" class="listed_file_name" name="upload_teruskan_name['+id+']" value="'+xhr.uploadName+'" />');
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('.upload_teruskan_listed').find('.listed_file_uuid[name="upload_teruskan_uuid['+id+']"]').remove();
                  $('.upload_teruskan_listed').find('.listed_file_name[name="upload_teruskan_name['+id+']"]').remove();
                }
              }
          }
      }); /*end title galery*/
</script>