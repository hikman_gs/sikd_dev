<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>


	<!-- <meta charset="utf-8"> -->
    <style>
        p.indent {
            padding-left: 6.8em
        }
    </style>
    <style type="text/css">
    table.page_header {
        width: 1020px;
        border: none;
        background-color: #DDDDFF;
        border-bottom: solid 1mm #AAAADD;
        padding: 2mm
    }

    table.page_footer {
        width: 1020px;
        border: none;
        border-top: white 1mm #;
        padding: 2mm
    }
</style>
<page backtop="2mm" backbottom="14mm" backleft="1mm" backright="4mm">
	<page_footer>
		<table style="font-family: Arial; font-size: 10px; line-height: 15px;" align="center">
			<tr>
				<td align="center">
					Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
					<br>
					Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
				</td>
			</tr>
		</table>
	</page_footer>


	<div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -100px;">
		<img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
	</div>
	<p style="text-align: center;"><b>NOTA DINAS</b></p>
	<br>
	<table style="margin-left:15; width: 500; margin-right:30; font-family: Arial; font-size: 12px; line-height: 10px;">
		<tr>
			<td style="line-height: 15px; vertical-align:top; width : 53;" rowspan="100">
				Kepada
			</td>
			<td style="line-height: 15px; vertical-align:top;" rowspan="100">
				 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
			</td>
		</tr>

		<?php
		$exp = explode(',', $RoleId_To);
		$z = count($exp);

		if ($z > 1) {

			$n = 1;



			foreach ($exp as $k => $v) {
				 $xx1 =  $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $v . "'AND GroupId  IN('3','4','7') ")->row();
				  $str = penulisan_dinas($xx1->PeoplePosition);
			

			  echo  '<tr>' . '<td>' . '</td>' .
                    '<td style="width: 10; line-height: 15px;">' . $n . '. ' . '</td>' .
                    '<td style="width: 500; line-height: 15px;">' . $str . '</td>' . '</tr>';
				$n++;
			}
		} else {
			$xx2 =  $this->db->query("SELECT PeoplePosition FROM people WHERE PeopleId = '" . $exp[0] . "'AND GroupId  IN('3','4','7') ")->row();
			$str2 = penulisan_dinas($xx2->PeoplePosition);


			echo  '<tr>' .
			'<td>' . '</td>' .
			'<td style="width: 500; line-height: 15px;">' . $str2 . '</td>' . '</tr>';
		}
		?>
	</table>

	<table style="margin-left:15; width: 500; margin-right:30; font-family: Arial; font-size: 12px; line-height: 10px;">
		<tr>
			<td style="line-height: 10px;vertical-align:top; width : 53;">
				Dari
			</td>
			<td style="line-height: 15px;vertical-align:top">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
			</td>
			<td></td>
			<td style="line-height: 15px; overflow: hidden; width: 500;">
				<?= $uc  = ucwords(strtolower(get_data_people('RoleName', $Nama_ttd_atas_nama)));
				$str = str_replace('Dan', 'dan', $uc);
				$str = str_replace('Uptd', 'UPTD', $uc);
				?>
			</td>

		</tr>
	</table>
<table style="margin-left:15; width: 500; margin-right:30; font-family: Arial; font-size: 12px; line-height: 10px;">
		<tr>
			<td style="line-height: 10px;vertical-align:top; width : 53;" rowspan="100">
				Tembusan
			</td>
			<td style="line-height: 10px;vertical-align:top;" rowspan="100">
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
			</td>
		</tr>

			<?php
		$exp = explode(',', $RoleId_Cc);
		$z = count($exp);

		if ($z > 1) {
			$n = 1;
			foreach ($exp as $k => $v) {
				$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
				$str = penulisan_dinas($xx1);
				

				echo  '<tr>' . '<td>' . '</td>' .
					'<td style="width: 10; line-height: 15px;">' . $n . '. ' . '</td>' .
					'<td style="width: 500; line-height: 15px;">' . $str . '</td>' . '</tr>';

				$n++;
			}
		} else {
			if (!empty($RoleId_Cc)) {
				if ($RoleId_Cc != '') {
				}
				$xxx =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $exp[0] . "'")->row()->RoleName;
				$str = penulisan_dinas($xxx);
			
				echo  '<tr>' .
					'<td>' . '</td>' .
					'<td style="width: 500; line-height: 15px;">' . $str . '</td>' . '</tr>';
			}else{

				$str9 = '-';
                echo  '<tr>' .
                    '<td>' . '</td>' .
                    '<td style="width: 500;  line-height: 15px; ">' . $str9. '</td>' . '</tr>';

			}
		}
		?>    	

	</table>
	<table style="margin-left:15; width: 500; margin-right:30; font-family: Arial; font-size: 12px; line-height: 10px;">

		<tr>
			<td style="line-height: 10px;vertical-align:top; width : 53;">
				Nomor
			</td>
			<td>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
			</td style="width : 10;>
			<td style=" width: 300; line-height: 10px;" colspan="10">
				<?php

				if (!empty($this->data['nosurat'])) {
					$no = $nosurat;
					echo ' &nbsp;'.$no;
				} else {

					$no = '.........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'];
					echo ' &nbsp;'.$no;
				}
				?>

			</td>

		</tr>
		<tr>
			<td style="line-height: 10px;vertical-align:top; width : 53;">
				Tanggal
			</td>
			<td style="line-height: 15px;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
			</td>
			<td style="width: 300; line-height: 10px;" colspan="10">
				&nbsp;<?= get_bulan($TglNaskah) ?>
			</td>

		</tr>

		<tr>
			<td style="line-height: 15px;">
				Sifat
			</td>
			<td style="line-height: 15px;">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
			</td>
			<td style="width: 300; line-height: 10px;" colspan="10">
				&nbsp;<?= $SifatId; ?>
			</td>
		</tr>
		<tr>
			<?php
			if ($Jumlah != 0) {
			?>
				<td style="line-height: 15px;">Lampiran
				</td>
				<td style="line-height: 15px;">
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:

				</td>
				<td style="width: 300; line-height: 10px;" colspan="10">
					<?php
					echo '&nbsp;' . $Jumlah . ' ' . $MeasureUnitId;
					?>

				</td>
			<?php } ?>
		</tr>
		<tr>
			<td style="line-height: 15px; vertical-align: text-top; ">Hal
			</td>
			<td style="line-height: 15px; vertical-align: text-top; ">
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:

			</td>
			<td style="line-height: 15px;" colspan="10">
				<div style="width: 465; margin-left: 4px; text-align: justify;"><?= $Hal; ?></div>
			</td>
		</tr>

	</table>

	<p style="margin-right:1;margin-left:10;" align="center">
		<hr>
	</p>
	<p style="margin-right:-4px ; margin-left:30; line-height: 15px; margin-top:-12px; font-family: Arial; font-size: 12px; text-align: justify;">

	<p style="margin-right:8px ; margin-left:-13; line-height: 20px; font-family: Arial; font-size: 12px; text-align: justify;"><?= $Konten; ?></p>

	</p>
	<div style="width: 50%; font-family: Arial; font-size: 11px; margin-left:280px; margin-top:-15px; ">

	 <?php
   $r_atasan =  $this->session->userdata('roleatasan'); 
   $r_atasan3 =  $this->session->userdata('PrimaryRoleId'); 
   $r_atasan2 =$this->session->userdata(['RoleAtasan',$r_atasan]); 
   $r_biro =  $this->session->userdata('groleid');
   $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
   $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND GroupId NOT IN ('8') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='".$this->session->userdata('roleatasan')."'")->result();
   $atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );
   $koma= ',';
   ?>
   <?php if($TtdText == 'PLT') { ?>
    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
  <?php } elseif($TtdText == 'PLH') { ?>
    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
  <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
      <br><?= get_data_people('RoleName',$Nama_ttd_atas_nama) ?>,</p>
    <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
      <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
        <br><?= get_data_people('RoleName',$atasanub)?>
        <br>u.b.<br><?= get_data_people('RoleName',$Nama_ttd_atas_nama) ?>,</p>
      <?php } else { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName', $Approve_People) ?>,</p>
      <?php } ?>  
		
		  <p align="center">PEMERIKSA</p>
		<div>
			<table style='border: 1; font-family: Arial; font-size: 11px; table-layout: fixed; overflow: hidden; width: 50%; height: 200; margin-left: 10px;' cellspacing="0">

				<tr>
					<td rowspan="5"> <img src="<?php echo base_url(); ?>/uploads/kosong.jpg" widht="55" height="60"></td>
					<td>Ditandatangani secara elekronik oleh:</td>
				</tr>
				<tr>
					<td style="overflow: hidden; width: 200px;">


 <?php
            $r_atasan =  $this->session->userdata('roleatasan'); 
            $r_biro =  $this->session->userdata('groleid');
            $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
            $atasanub= get_data_people('RoleAtasan',$Nama_ttd_atas_nama );
            ?>
            <?php if($TtdText == 'PLT') { ?>
             Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
           <?php } elseif($TtdText == 'PLH') { ?>
             Plh. <?= get_data_people('RoleName',$Approve_People) ?>,

             <br><?= get_data_people('RoleName',$Approve_People) ?>,
           <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
             a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
             <br><?= get_data_people('RoleName', $atasanub) ?>,
             <br>u.b.<br><?= get_data_people('RoleName',$Nama_ttd_atas_nama) ?>,

           <?php } else { ?>
             <?= get_data_people('RoleName', $Approve_People) ?>,
           <?php } ?>  


					</td>
				</tr>

				<tr>
					<td><br></td>
					<td><br></td>
					<td><br></td>
				</tr>

				<tr>
					<td>
						<p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
					</td>
				</tr>
				<tr>
					<td> <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PeopleName = '" .$Nama_ttd_konsep. "' AND GroupId IN ('3','4','7') ")->row()->Pangkat; ?>
					</td>
				</tr>

			</table>
		</div>
		<p style="float: right!important; text-align: center; font-family: Arial; font-size: 12px;" hidden=""></p>

	</div>
	<br>
	
<?php
    $lampiran_decode = array_filter(json_decode($lampiran));
        if (!empty($lampiran_decode)) {
            $jum_lamp = count($lampiran_decode);
            $no_lamp = 1;
                foreach ($lampiran_decode as $isilamp) {
                switch ($no_lamp) {
                    case 1:
                        if ($jum_lamp > 1) {
                            $lamp_romawi = "I";
                        } else {
                            $lamp_romawi = "";
                        }
                        break;
                    case 2:
                        $lamp_romawi = "II";
                        break;
                    case 3:
                        $lamp_romawi = "III";
                        break;
                    case 4:
                        $lamp_romawi = "IV";
                        break;
                    case 5:
                        $lamp_romawi = "V";
                        break;
                    case 6:
                        $lamp_romawi = "VI";
                        break;
                    case 7:
                        $lamp_romawi = "VII";
                        break;
                    case 8:
                        $lamp_romawi = "VIII";
                        break;
                    case 9:
                        $lamp_romawi = "IX";
                        break;
                    case 10:
                        $lamp_romawi = "X";
                        break;
                    default:
                        $lamp_romawi = "";
                }

          ?>
        
        <page backtop="-10mm" backbottom="17mm" backleft="10mm" backright="5mm">
            <page_footer>
                <div style="width: 100%; font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh 
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </div>
            </page_footer>
            <br>
            <table style="height: 290px; width: 345px; font-family: Arial; margin-left:182px; font-size: 11px; line-height: 15px;  align-content : right;">
                <tbody>
                    <tr>
                        <td style="width: 85px; vertical-align: top; text-align: justify;">
                           <?php
                        echo "LAMPIRAN " . $lamp_romawi;
                         $no_lamp++;
                            ?>
                            &nbsp;&nbsp; :</td>
                        <td style="width: 6px; text-align: justify;">&nbsp;</td>
                        <td style="width: 275.333px; text-align: justify;" colspan="3">
                           
                            <?php if ($TtdText == 'PLT') { ?>
                                Plt. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText == 'PLH') { ?>
                                Plh. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                                <?= get_data_people('RoleName', $Approve_People3) ?>,
                            <?php } else { ?>
                                <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } ?> </td>
                    </tr>
                    <tr>
                        <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 74px; vertical-align: top; text-align: justify;">NOMOR</td>
                        <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                        <td style="width: 182px; vertical-align: top; text-align: justify;">

                        	<?php

                        	if (!empty($this->data['nosurat'])) {
                        		$no = $nosurat;
                        		echo ' &nbsp;'.$no;
                        	} else {

                        		$no = '.........../' . $this->data['ClCode'] . '/' . $this->data['RoleCode'];
                        		echo ' &nbsp;'.$no;
                        	}
                        	?>

                        </td>
                    </tr>
                    <tr>
                        <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 74px; vertical-align: top; text-align: justify;">TANGGAL</td>
                        <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                        <td style="width: 182px; vertical-align: top; text-align: justify;">Tanggal / Bulan / Tahun</td>
                    </tr>
                    <tr>
                        <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 74px; vertical-align: top; text-align: justify;">PERIHAL</td>
                        <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                        <td style="width: 182px; vertical-align: top; text-align: justify;"> <?= $Hal; ?></td>
                    </tr>
                </tbody>
            </table>

            <br><br>

            <p class="configlampiran" style="margin-right:-20px ;margin-left:-30; margin-bottom:35px; line-height: 15px; font-family: Arial; font-size: 12px; text-align: justify;">
                <?php
                echo $isilamp . "<br>";
                ?>

            </p>

            <?php
            $naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();
            $sql_lampiran = $this->db->query("SELECT lampiran FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->lampiran;
            ?>
            <div style="width: 60%; font-family: Arial; font-size: 12px;  margin-left: 240px; margin-top:-22px; ">
                <?php
                $r_atasan =  $this->session->userdata('roleatasan');
                $r_biro =  $this->session->userdata('groleid');
                $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
                ?>
                <?php if ($TtdText == 'PLT') { ?>
                    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
                <?php } elseif ($TtdText == 'PLH') { ?>
                    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
                <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                        <br><?= get_data_people('RoleName', $Approve_People) ?>,
                    </p>

                <?php } elseif ($TtdText2 == 'untuk_beliau') { ?>
                    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                        <br><?= get_data_people('RoleName', $atasanub) ?>,
                        <br>u.b.<br><?= get_data_people('RoleName', $Approve_People) ?>,
                    </p>

                <?php } else { ?>
                    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName', $Approve_People) ?>,</p>
                <?php } ?>
                <p align="center">PEMERIKSA</p>
                <table style='border: 1; font-family: Arial; font-size:11px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 32px;' cellspacing="0">

                    <tr>
                        <td rowspan="5"> <img src="<?php echo 'uploads/kosong.jpg" widht="20" height="50"'; ?>"></td>
                        <td>Ditandatangani secara elekronik oleh:</td>
                    </tr>
                    <tr>
                        <td style="overflow: hidden; width: 200px;"> 
                            <?php
                            $r_atasan =  $this->session->userdata('roleatasan');
                            $r_biro =  $this->session->userdata('groleid');
                            $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
                            ?>
                            <?php if ($TtdText == 'PLT') { ?>
                                Plt. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText == 'PLH') { ?>
                                Plh. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                                a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                                <br><?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText2 == 'untuk_beliau') { ?>
                                a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                                <br><?= get_data_people('RoleName', $atasanub) ?>,
                                <br>u.b.<br><?= get_data_people('RoleName', $Approve_People) ?>,

                            <?php } else { ?>
                                <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } ?>
                        </td>
                    </tr>

                    <tr>
                        <td><br></td>
                        <td><br></td>
                        <td><br></td>
                    </tr>

                    <tr>
                        <td>
                            <p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
                        </td>
                    </tr>
                    <tr>
                        <td> <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PeopleName = '" .$Nama_ttd_konsep. "' AND GroupId IN ('3','4','7') ")->row()->Pangkat; ?>
                    </td>
                </tr>

            </table>
        </div>
        </page>
        
    <?php }
        
  }else {  $no_lamp++; ?>
             <?php } ?>
</page>