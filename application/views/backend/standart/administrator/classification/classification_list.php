<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/ui.dynatree.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/tree.skinklas.css">

<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.form.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.dynatree.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/dynatree.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>

<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Klasifikasi</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Pengaturan Klasifikasi</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <a id="btn_tambah" class="btn btn-success" title="Tambah Data">Tambah</a>
                      <a id="btn_hapus" class="btn btn-danger" title="Hapus Data">Hapus</a>
                      <a id="btn_batal" class="btn btn-primary" title="Batal">Batal</a>
                      <a class="btn btn-flat btn-warning btn-edit hidden" title="Ubah Data">Ubah</a>
                    </div>
                  </div>
                  <br>
                  <div class="row">
                    <div class="col-md-6" id="treeClassfication">
                      <?php  
                          $data = array();
                          foreach ($clasification as $row) {
                            $data[$row->parent_id][] = $row; 
                          }
                          echo showClassification($data,'0');
                      ?>
                    </div>

                    <div class="col-md-6">
                      <?=form_open(BASE_URL('administrator/anri_master_klasifikasi/add_save'));?>
                        <div class="form-group col-md-12 hidden">
                            <label for="ClId" class="col-sm-3 control-label">ClId 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="ClId" id="ClId" placeholder="ClId" value="<?= set_value('ClId'); ?>">
                            </div>
                        </div>

                        <div class="form-group col-md-12 hidden">
                            <label for="ClParentId" class="col-sm-3 control-label">ClParentId 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="ClParentId" id="ClParentId" placeholder="ClParentId" value="<?= set_value('ClParentId'); ?>">
                            </div>
                        </div>

                        <div class="form-group col-md-12">
                            <label for="ClCode" class="col-sm-3 control-label">Kode Klasifikasi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled name="ClCode" id="ClCode" placeholder="Kode Klasifikasi" value="<?= set_value('ClCode'); ?>" required>
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            <label for="ClName" class="col-sm-3 control-label">Nama Klasifikasi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled name="ClName" id="ClName" placeholder="Nama Klasifikasi" value="<?= set_value('ClName'); ?>" required>
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            <label for="ClDesc" class="col-sm-3 control-label">Deskripsi 
                            </label>
                            <div class="col-sm-9">
                                <textarea id="ClDesc" name="ClDesc" rows="2" class="form-control ClDesc" disabled><?= set_value('ClDesc'); ?></textarea>
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            <label for="RetensiThn_Active" class="col-sm-3 control-label">Retensi Aktif 
                            </label>
                            <div class="col-sm-9">
                                <input type="number" min='0' class="form-control" disabled name="RetensiThn_Active" id="RetensiThn_Active" placeholder="Retensi Aktif" value="<?= set_value('RetensiThn_Active'); ?>">
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            <label for="Ket_Active" class="col-sm-3 control-label">Ket Aktif 
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled name="Ket_Active" id="Ket_Active" placeholder="Keterangan Aktif" value="<?= set_value('Ket_Active'); ?>">
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            <label for="RetensiThn_InActive" class="col-sm-3 control-label">Retensi Inaktif 
                            </label>
                            <div class="col-sm-9">
                                <input type="number" min='0' class="form-control" disabled name="RetensiThn_InActive" id="RetensiThn_InActive" placeholder="Retensi Inaktif" value="<?= set_value('RetensiThn_InActive'); ?>">
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            <label for="Ket_InActive" class="col-sm-3 control-label">Ket Inaktif 
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" disabled name="Ket_InActive" id="Ket_InActive" placeholder="Keterangan Inaktif" value="<?= set_value('Ket_InActive'); ?>">
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            <label for="SusutId" class="col-sm-3 control-label">Penyusutan Akhir<i class="required">*</i> 
                            </label>
                            <div class="col-sm-9">
                              <div class="susut">
                               <select class="form-control" disabled name="SusutId" id="SusutId" required data-val="">
                                  <?php
                                  $susut = $this->db->query("SELECT SusutId, SusutName FROM master_penyusutan ORDER BY SusutName")->result();
                                  foreach ($susut as $s):
                                  ?>                                 
                                       <option value="<?= $s->SusutId; ?>"><?=$s->SusutName ;?></option>                                 
                                  <?php endforeach; ?>
                                </select>
                              </div>
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            <label for="CIStatus" class="col-sm-3 control-label">Status Aktif  
                            </label>
                            <div class="col-sm-9">
                               <input type="checkbox" disabled id="CIStatus" name="CIStatus" value="<?= set_value('CIStatus'); ?>">
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                          <div class="col-md-12">
                            <input type="submit" name="submit" class="btn btn-save btn-flat btn-danger hidden" title="Simpan Data" value="Simpan">
                            <a title="Simpan Data" class="btn btn-flat btn-danger btn-saveedit hidden">Simpan</a>
                          </div>
                        </div>

                        <?=form_close()?>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<script>

$('#CIStatus').change(function() {
    if(this.checked) {
        $(this).val('1');
    }else{
      $('#CIStatus').val('0');
    }
  });

  $('#btn_tambah').click(function(){
    var id = $('#ClId').val();
    if (id == '') {
      swal({
            title: "Anda Belum Memilih Klasifikasi Induk",
            icon: "error",
          });
    }else{   
      $('.btn-edit').addClass('hidden').attr('disabled',true);
      $('.btn-save').removeClass('hidden').removeAttr('disabled');
      $('.btn-saveedit').addClass('hidden');
      $("[name=ClCode]").removeAttr('disabled').val('');
      $("[name=ClName]").removeAttr('disabled').val('');
      $("[name=ClDesc]").removeAttr('disabled').val('');
      $("[name=RetensiThn_Active]").removeAttr('disabled').val('');
      $("[name=Ket_Active]").removeAttr('disabled').val('');
      $("[name=RetensiThn_InActive]").removeAttr('disabled').val('');
      $("[name=Ket_InActive]").removeAttr('disabled').val('');
      $("[name=SusutId]").removeAttr('disabled').val('');
      $('#CIStatus').removeAttr('disabled').val('1');
      $('#ClCode').focus();      
      $('#CIStatus').checked();   
    }  
  });

  $('#btn_batal').click(function(){
      window.location.href = "<?=BASE_URL('administrator/anri_master_klasifikasi')?>";
  });

  $('.btn-edit').click(function(){
    $('.btn-save').addClass('hidden').attr('disabled',true);
    $('.btn-saveedit').removeClass('hidden');
    $("[name=ClCode]").removeAttr('disabled');
    $("[name=ClName]").removeAttr('disabled');
    $("[name=ClDesc]").removeAttr('disabled');
    $("[name=RetensiThn_Active]").removeAttr('disabled');
    $("[name=Ket_Active]").removeAttr('disabled');
    $("[name=RetensiThn_InActive]").removeAttr('disabled');
    $("[name=Ket_InActive]").removeAttr('disabled');
    $("[name=SusutId]").removeAttr('disabled');
    $("[name=CIStatus]").removeAttr('disabled');
    $('#ClCode').focus(); 
  });

  $('#btn_hapus').click(function(){
    var id = $('#ClId').val();
    if (id == '') {
      swal({
            title: "Anda Belum Memilih Klasifikasi",
            icon: "error",
          });
    }else{
      swal({
            title: "Apakah Data Akan Dihapus ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $.ajax({
                url: "<?=BASE_URL('administrator/anri_master_klasifikasi/delete')?>",
                    type: "POST",
                    data:{
                        data_id: $('#ClId').val(),
                    },
                    success: function(response){
                      var obj = jQuery.parseJSON(response);
                      if(obj['status'] == 'error_check_parent') {
                        swal({title: "Data Tidak Bisa Dihapus, Karena Terdapat Klasifikasi Bawahan",icon: "error",});  
                      }else if(obj['status'] == 'error_berkas'){
                        swal({title: "Data Tidak Bisa Dihapus",icon: "error",});  
                      }else if(obj['status'] == 'success'){
                        window.location.href = "<?=BASE_URL('administrator/anri_master_klasifikasi')?>";
                      }
                    },
                    error: function(){
                      swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
                    }
              });
            }
          });
    }
  });
  
  $('.btn-saveedit').click(function(){
    var id = $('#ClId').val();
    if (id == '') {
      swal({
            title: "Maaf Klasifikasi Belum anda pilih",
            icon: "error",
          });
    }else{
      $.ajax({
          url: "<?=BASE_URL('administrator/anri_master_klasifikasi/update')?>",
          type: "POST",
          data:{
              ClId                    : $("#ClId").val(),
              ClCode                  : $("#ClCode").val(),
              ClName                  : $("#ClName").val(),
              ClDesc                  : $("#ClDesc").val(),
              RetensiThn_Active       : $("#RetensiThn_Active").val(),
              Ket_Active              : $("#Ket_Active").val(),
              RetensiThn_InActive     : $("#RetensiThn_InActive").val(),
              Ket_InActive            : $("#Ket_InActive").val(),
              SusutId                 : $('#SusutId').find(":selected").val(),
              CIStatus                : ($("#CIStatus").is(":checked") == true ? '1' : '0'),
          },
          success: function(response){
            console.log(response);
            window.location.href = "<?=BASE_URL('administrator/anri_master_klasifikasi')?>";
          },
          error: function(){
            swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
          }
      });
    }
  });
</script>