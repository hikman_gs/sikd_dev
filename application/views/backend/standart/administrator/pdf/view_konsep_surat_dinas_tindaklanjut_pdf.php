<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>
<style type="text/css">
    table.page_header {width: 1020px; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 1020px; border: none;  border-top: white 1mm #; padding: 2mm}

</style>


<page backtop="2mm" backbottom="14mm" backleft="1mm" backright="4mm">
    <!-- <page_header> -->
    <!-- Setting Header -->

    <!-- </page_header> -->
    <!-- Setting Footer -->
    <page_footer>
    <table class="page_footer">
        <tr>
          <td style="width: 5%; text-align: right">
            <!--     Halaman [[page_cu]]/[[page_nb]] -->
        </td>
        <td style="width: 50%; text-align: center">
         <div style="font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
            Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
            <br>
            Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
        </div>
    </td>

</tr>
</table>
</page_footer>
<!-- Setting CSS Tabel data yang akan ditampilkan -->
<style type="text/css">
    .tabel2 {
        border-collapse: collapse;
    }
    .tabel2 th, .tabel2 td {
        padding: 5px 5px;
        border: 1px solid #000;
    }
</style>



    <div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -96px;">
        <img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
    </div>
    <div class="pos" id="_450:199" style="top:199;font-family:Arial; font-size:12px; margin-left:358">
        <p style=" id=" _15.8" font-family:Arial; font-size:12px;color:#000000">
        Tempat / Tanggal / Bulan / Tahun </p>
    </div>
    <div class="pos" id="_450:199" style="top:199; margin-left:359;font-family:Arial; font-size:12px;">
        <p style=" font-family:Arial; id=" _15.8" font-size:12px; color:#000000">
        Kepada</p>
    </div>
<table cellpadding="17" cellspacing="1">
        <tbody>
            <tr><br>
                <td align="normal" valign="top" width="352">
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Nomor &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;.../<?= $ClCode; ?>/<?= $RoleCode; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Sifat
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:35px; color:#000000">
                            :&nbsp;&nbsp;&nbsp;&nbsp;<?= $SifatId; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Lampiran
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:9; color:#000000">
                            :&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Jumlah . ' ' . $MeasureUnitId; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115; text-align: justify">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Hal
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:41; color:#000000">
                            :

                        </span>
                        <div style="width: 60%; font-family: Arial; font-size: 12px; margin-left: 11px;">

                            <?= $Hal; ?>
                        </div>
                    </div>
                </td>

                <td align="normal" valign="top" width="180;font-family:Arial; font-size:12px;">Yth.&nbsp;
                    <div class="pos" id="_118:320" style="top:320;left:200;font-family:Arial; font-size:12px;">
                        <?= $RoleId_To; ?>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;margin-left:22;font-family:Arial;font-size:12px; ">di
                    </div>
                    <div class="pos" id="_118:320" style="top:320;margin-left:35; margin-right:15px; font-family:Arial;font-size:12px;"><?= $Alamat; ?>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<br>
<p style="margin-right:-5px ;margin-left:75; line-height: 17px; font-family: Arial; font-size: 12px; text-align: justify;">
    <p class="indent"><?= $Konten; ?></p>
</p>

<div style="width: 55%; font-family: Arial; font-size: 12px; margin-left: 281px; margin-top:-10px; ">
    <?php
        $r_atasan =  $this->session->userdata('roleatasan'); 
        $r_biro =  $this->session->userdata('groleid');
        $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
    ?>
    <?php if($TtdText == 'PLT') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText == 'PLH') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
            <br><?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
            <br><?= get_data_people('RoleName',$r_atasan) ?>,
            <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } else { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } ?>  

            <p align="center">PEMERIKSA</p>

            <table style='border: 1; font-family: Arial; font-size: 10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 29px;' cellspacing="0" >

                <tr>
                    <td rowspan="5" >  <img src="<?php echo base_url(); ?>/uploads/kosong.jpg" widht="20" height="50"></td> 
                    <td >Ditandatangani secara elekronik oleh:</td>
                </tr>
                <tr>
                    <td style="overflow: hidden; width: 200px;">
                 <?php
                 $r_atasan =  $this->session->userdata('roleatasan'); 
                 $r_biro =  $this->session->userdata('groleid');
                 $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 

                 if($TtdText == 'PLT') { ?>
                   Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
               <?php } elseif($TtdText == 'PLH') { ?>
                 Plh. <?= get_data_people('RoleName',$Approve_People) ?>,
                 <br><?= get_data_people('RoleName',$Approve_People) ?>,

             <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                <br><?= get_data_people('RoleName', $Approve_People) ?>,

                <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                 a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                 <br><?= get_data_people('RoleName',$r_atasan) ?>,
                 <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,

             <?php } else { ?>
               <?= get_data_people('RoleName',$Approve_People) ?>,
           <?php } ?>  
           </td>
       </tr>

       <tr>
        <td><br></td>
        <td><br></td>
        <td><br></td>
    </tr>

    <tr>
        <td><p style="float: right!important;"><?= $Nama_ttd_konsep ?></p></td>
    </tr>
    <tr>
        <td>
          <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE ApprovelName = '" .$Nama_ttd_konsep. "' AND GroupId IN ('3','4','7') ")->row()->Pangkat; ?>
        </td>
    </tr>
</table>

</div>

<table width="100%" style="font-family: Arial; font-size: 12px; line-height: 15px;">
    <tr>
      <td width="10%" valign="top">
        <?php
        if (!empty($RoleId_Cc)) {
          $exp = explode(',', $RoleId_Cc);
          $z = count($exp);
          if ($z > 1) {
            $n = 1;
            $y = 'Yth.';
            echo "Tembusan : <br>";
            foreach ($exp as $k => $v) {
              $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
              $uc  = ucwords(strtolower($xx1));
              $str = str_replace('Dan', 'dan', $uc);
              $str = str_replace('Uptd', 'UPTD', $str);
              $str = str_replace('Dprd', 'DPRD', $str);
              if ($n < $z) {

                if ($n == ($z-1)) {
                    echo
                    '<table border="0" height="20" width="100">
                    <tr style="width:80px" margin-right="50px" height="20">
                    <td style="text-align: justify; width: 10;"valign="top">' . $n . '.' . '</td>
                    <td style="text-align: justify; margin-right:30px;"valign="top">' . $y . '&nbsp;</td>
                    <td style="text-align: justify; width: 545;"valign="top">' . $str . '; dan</td>
                    </tr>
                    </table>';
                }else{
                    echo
                    '<table border="0" height="20" width="100">
                    <tr style="width:80px" margin-right="50px" height="20">
                    <td style="text-align: justify; width: 10;"valign="top">' . $n . '.' . '</td>
                    <td style="text-align: justify; margin-right:30px;"valign="top">' . $y . '&nbsp;</td>
                    <td style="text-align: justify; width: 545;"valign="top">' . $str . ';</td>
                    </tr>
                    </table>';
                }
                
            } else {
                echo
                '<table border="0" height="20" width="100">

                <tr style="width:80px" margin-right="50px" height="20">
                <td style="text-align: justify; width: 10;"valign="top">' . $n . '.' . '</td>
                <td style="text-align: justify; margin-right:30px;"valign="top">' . $y . '&nbsp;</td>
                <td style="text-align: justify; width: 545;"valign="top">' . $str . '.</td>
                </tr>

                </table>';
            }
            $n++;
        }
    } else {
        echo "Tembusan : <br>";
        $y = 'Yth.';
        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $exp[0] . "'")->row()->RoleName;
        $uc  = ucwords(strtolower($xx1));
        $str = str_replace('Dan', 'dan', $uc);
        $str = str_replace('Uptd', 'UPTD', $str);
        $str = str_replace('Dprd', 'DPRD', $str);
        echo
        '<table border="0" height="20" width="100">

        <tr style="width:80px" margin-right="50px" height="20">

        <td style="text-align: justify; margin-right:30px;"valign="top">' . $y . '&nbsp;</td>
        <td style="text-align: justify; width: 545;"valign="top">' . $str . '</td>
        </tr>

        </table>';
    }
}
?>
</td>
</tr>
</table>

<br><br>
<!-- <page_footer>
<table style="font-family: Arial; font-size: 9px; line-height: 15px;" align="center">
    <tr>
        <td align="center">
            Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
            <br>
            Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
        </td>
    </tr>
</table>
</page_footer> -->

<!-- LAMPIRAN 1-->
     <?php
        $jum_lamp = $lampiran1 >= '0';
$no_lamp = 1;

                       if ($lampiran >= '0') { ?>

     

            <page backtop="-8mm" backbottom="17mm" backleft="10mm" backright="5mm">
                <page_footer>
                <div style="width: 100%; font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </div>
                </page_footer>
                <br>
               <table style="height: 290px; width: 345px; font-family: Arial; margin-left:183px; font-size: 11px; line-height: 15px;  align-content : right;">
                <tbody>
                    <tr>
                        <td style="width: 85px; vertical-align: top; text-align: justify;">
                                <?php
                                 
                                if ($jum_lamp >= '1') {
                                    switch ($no_lamp){
                                    case 1: 
                                        $lamp_romawi = "I";
                                       
                                        break;
                                    }
                                    echo "LAMPIRAN ".$lamp_romawi;
                                }else{
                                    echo "LAMPIRAN";
                                }

                                ?>
                            &nbsp;&nbsp; :</td>
                            <td style="width: 6px; text-align: justify;">&nbsp;</td>
                            <td style="width: 275.333px; text-align: justify;" colspan="3">
                                <?php
                                if ($type_naskah == 'XxJyPn38Yh.1') {
                                    echo "SURAT";          
                           }elseif ($type_naskah == 'XxJyPn38Yh.2') {
                                  echo "SURAT UNDANGAN";  
                              }elseif ($type_naskah == 'XxJyPn38Yh.3') {
                                  echo "SURAT PANGGILAN";  
                              }
                            $r_atasan =  $this->session->userdata('roleatasan');
                            $r_biro =  $this->session->userdata('groleid');
                            $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
                            ?>
                            <?php if ($TtdText == 'PLT') { ?>
                                Plt. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText == 'PLH') { ?>
                                Plh. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                                <?= get_data_people('RoleName', $Approve_People3) ?>,

                            <?php } else { ?>
                                <?= get_data_people('RoleName', $Approve_People) ?>,
                                <?php } ?>  </td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">NOMOR</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;">.../<?= $ClCode; ?>/<?= $RoleCode; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">TANGGAL</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;">Tanggal / Bulan / Tahun</td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">PERIHAL</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;"> <?= $Hal; ?></td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <p class="configlampiran" style="margin-right:-20px ;margin-left:-30; margin-bottom:35px; line-height: 15px; font-family: Arial; font-size: 12px; text-align: justify;">
                        <?php
                           echo $lampiran."<br>";
                           ?>
                   </p>
               </page>

    <?php
    $naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();
    $sql_lampiran = $this->db->query("SELECT lampiran FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->lampiran;
    ?>
      <div style="width: 60%; font-family: Arial; font-size: 12px;  margin-left: 240px; margin-top:-10px; ">
          <?php
          $r_atasan =  $this->session->userdata('roleatasan'); 
          $r_biro =  $this->session->userdata('groleid');
          $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
          ?>
          <?php if($TtdText == 'PLT') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText == 'PLH') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                <br><?= get_data_people('RoleName',$Approve_People) ?>,</p>

            <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3)?>,
                 <br><?= get_data_people('RoleName',$atasanub )?>,
                 <br>u.b.<br><?= get_data_people('RoleName',$Approve_People)?>,</p>

             <?php } else { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
            <?php } ?>  
            <p align="center">PEMERIKSA</p>
            <table style='border: 1; font-family: Arial; font-size:10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 32px;' cellspacing="0" >

                <tr>
                 <td rowspan="5" >  <img src="<?php echo 'uploads/kosong.jpg" widht="20" height="50"';?>"></td>
                 <td >Ditandatangani secara elekronik oleh:</td>
             </tr>
             <tr>
                <td style="overflow: hidden; width: 200px;"> 
                <?php
                $r_atasan =  $this->session->userdata('roleatasan'); 
                $r_biro =  $this->session->userdata('groleid');
                $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
                ?>
                <?php if($TtdText == 'PLT') { ?>
                 Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
                 <?php } elseif($TtdText == 'PLH') { ?>
                   Plh. <?= get_data_people('RoleName',$Approve_People) ?>,
               <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                <br><?= get_data_people('RoleName', $Approve_People) ?>,

            <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
               a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
               <br><?= get_data_people('RoleName',$atasanub) ?>,
               <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,

           <?php } else { ?>
             <?= get_data_people('RoleName',$Approve_People) ?>,
         <?php } ?> 
                </td>
         </tr>

         <tr>
            <td><br></td>
            <td><br></td>
            <td><br></td>
        </tr>

        <tr>
            <td><p style="float: right!important;"><?= $Nama_ttd_konsep ?></p></td>
        </tr>
        <tr>
            <td>  <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE ApprovelName = '" .$Nama_ttd_konsep. "' AND GroupId IN ('3','4','7') ")->row()->Pangkat; ?>
        </td>
    </tr>
</table>
        </div>
    <?php } else { ?>
    <?php } ?>

    <!-- LAMPIRAN 2 -->


     
        <?php
            $jum_lamp = $lampiran >= '0';
$no_lamp = 1;

                       if ($lampiran2 >= '0') { ?>

     
            <page backtop="-8mm" backbottom="17mm" backleft="10mm" backright="5mm">
                <page_footer>
                <div style="width: 100%; font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </div>
                </page_footer>
                
                <table style="height: 290px; width: 345px; font-family: Arial; margin-left:183px; font-size: 11px; line-height: 15px;  align-content : right;">
                <tbody>
                    <tr>
                        <td style="width: 85px; vertical-align: top; text-align: justify;">
                                <?php
                                 
                                if ($jum_lamp  >= '0') {
                                
                                    echo "LAMPIRAN ".'II';
                                }else{
                                    echo "LAMPIRAN".$lamp_romawi;
                                }

                                ?>
                            &nbsp;&nbsp;:</td>
                            <td style="width: 6px; text-align: justify;">&nbsp;</td>
                            <td style="width: 275.333px; text-align: justify;" colspan="3">
                            <?php
                                if ($type_naskah == 'XxJyPn38Yh.1') {
                                    echo "SURAT";          
                           }elseif ($type_naskah == 'XxJyPn38Yh.2') {
                                  echo "SURAT UNDANGAN";  
                              }elseif ($type_naskah == 'XxJyPn38Yh.3') {
                                  echo "SURAT PANGGILAN";  
                              }
                            $r_atasan =  $this->session->userdata('roleatasan');
                            $r_biro =  $this->session->userdata('groleid');
                            $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
                            ?>
                            <?php if ($TtdText == 'PLT') { ?>
                                Plt. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText == 'PLH') { ?>
                                Plh. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                                <?= get_data_people('RoleName', $Approve_People3) ?>,

                            <?php } else { ?>
                                <?= get_data_people('RoleName', $Approve_People) ?>,
                                <?php } ?>  </td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">NOMOR</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;">.../<?= $ClCode; ?>/<?= $RoleCode; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">TANGGAL</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;">Tanggal / Bulan / Tahun</td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">PERIHAL</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;"> <?= $Hal; ?></td>
                            </tr>
                        </tbody>
                    </table>

                    <br>

                    <p class="configlampiran" style="margin-right:-20px ;margin-left:-30; margin-bottom:35px; line-height: 15px; font-family: Arial; font-size: 12px; text-align: justify;">
                         <?php
                           echo $lampiran2."<br>";
                           ?>
                   </p>
               </page>

    <?php
    $naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();
    $sql_lampiran = $this->db->query("SELECT lampiran FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->lampiran;
    ?>
      <div style="width: 60%; font-family: Arial; font-size: 12px;  margin-left: 240px; margin-top:7px; ">
          <?php
          $r_atasan =  $this->session->userdata('roleatasan'); 
          $r_biro =  $this->session->userdata('groleid');
          $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
          ?>
          <?php if($TtdText == 'PLT') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText == 'PLH') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                <br><?= get_data_people('RoleName',$Approve_People) ?>,</p>

            <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3)?>,
                 <br><?= get_data_people('RoleName',$atasanub )?>,
                 <br>u.b.<br><?= get_data_people('RoleName',$Approve_People)?>,</p>

             <?php } else { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
            <?php } ?>  
            <p align="center">PEMERIKSA</p>
            <table style='border: 1; font-family: Arial; font-size:10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 32px;' cellspacing="0" >

                <tr>
                 <td rowspan="5" >  <img src="<?php echo 'uploads/kosong.jpg" widht="20" height="50"';?>"></td>
                 <td >Ditandatangani secara elekronik oleh:</td>
             </tr>
             <tr>
                <td style="overflow: hidden; width: 200px;"> 
                    <?php
                    $r_atasan =  $this->session->userdata('roleatasan'); 
                    $r_biro =  $this->session->userdata('groleid');
                    $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
                    ?>
                    <?php if($TtdText == 'PLT') { ?>
                       Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
                   <?php } elseif($TtdText == 'PLH') { ?>
                     Plh. <?= get_data_people('RoleName',$Approve_People) ?>,
                 <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                    a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                    <br><?= get_data_people('RoleName', $Approve_People) ?>,

                <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                 a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                 <br><?= get_data_people('RoleName',$atasanub) ?>,
                 <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,

             <?php } else { ?>
               <?= get_data_people('RoleName',$Approve_People) ?>,
           <?php } ?>
               </td>
         </tr>

         <tr>
            <td><br></td>
            <td><br></td>
            <td><br></td>
        </tr>

        <tr>
            <td><p style="float: right!important;"><?= $Nama_ttd_konsep ?></p></td>
        </tr>
        <tr>
            <td> <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE ApprovelName = '" .$Nama_ttd_konsep. "' AND GroupId IN ('3','4','7') ")->row()->Pangkat; ?>
        </td>
    </tr>

</table>
        </div>
    <?php } else { ?>
    <?php } ?>

    <!-- LAMPIRAN 3 -->

     <?php
        $jum_lamp = $lampiran2 >= '0';
$no_lamp = 1;

                       if ($lampiran3 >= '0') { ?>

     
            <BR>
            <page backtop="-8mm" backbottom="17mm" backleft="10mm" backright="5mm">
                <page_footer>
                <div style="width: 100%; font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </div>
                </page_footer>
                <br>
                <table style="height: 290px; width: 345px; font-family: Arial; margin-left:183px; font-size: 11px; line-height: 15px;  align-content : right;">
                <tbody>
                    <tr>
                        <td style="width: 85px; vertical-align: top; text-align: justify;">
                             <?php
                                 
                                if ($jum_lamp >= '0') {
                                    switch ($no_lamp){
                                    case 1: 
                                        $lamp_romawi = "III";
                                        break;
                                    }
                                    echo "LAMPIRAN ".$lamp_romawi.'&nbsp;&nbsp;:';
                                }else{
                                    echo "LAMPIRAN";
                                }
                                ?>
                           </td>
                            <td style="width: 6px; text-align: justify;">&nbsp;</td>
                            <td style="width: 275.333px; text-align: justify;" colspan="3">
                             <?php
                                if ($type_naskah == 'XxJyPn38Yh.1') {
                                    echo "SURAT";          
                           }elseif ($type_naskah == 'XxJyPn38Yh.2') {
                                  echo "SURAT UNDANGAN";  
                              }elseif ($type_naskah == 'XxJyPn38Yh.3') {
                                  echo "SURAT PANGGILAN";  
                              }
                            $r_atasan =  $this->session->userdata('roleatasan');
                            $r_biro =  $this->session->userdata('groleid');
                            $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
                            ?>
                            <?php if ($TtdText == 'PLT') { ?>
                                Plt. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText == 'PLH') { ?>
                                Plh. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                                <?= get_data_people('RoleName', $Approve_People) ?>,

                            <?php } else { ?>
                                <?= get_data_people('RoleName', $Approve_People) ?>,
                                <?php } ?>  </td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">NOMOR</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;">.../<?= $ClCode; ?>/<?= $RoleCode; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">TANGGAL</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;">Tanggal / Bulan / Tahun</td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">PERIHAL</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;"> <?= $Hal; ?></td>
                            </tr>
                        </tbody>
                    </table>

                    <br><br>

                    <p class="configlampiran" style="margin-right:-20px ;margin-left:-30; margin-bottom:35px; line-height: 15px; font-family: Arial; font-size: 12px; text-align: justify;">
                         <?php
                           echo $lampiran3."<br>";
                           ?>
                   </p>
               </page>

    <?php
    $naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();
    $sql_lampiran = $this->db->query("SELECT lampiran FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->lampiran;
    ?>
      <div style="width: 60%; font-family: Arial; font-size: 12px;  margin-left: 240px; margin-top:7px; ">
          <?php
          $r_atasan =  $this->session->userdata('roleatasan'); 
          $r_biro =  $this->session->userdata('groleid');
          $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
          ?>
          <?php if($TtdText == 'PLT') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText == 'PLH') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                <br><?= get_data_people('RoleName',$Approve_People) ?>,</p>

            <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3)?>,
                 <br><?= get_data_people('RoleName',$atasanub )?>,
                 <br>u.b.<br><?= get_data_people('RoleName',$Approve_People)?>,</p>

             <?php } else { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
            <?php } ?>  
            <p align="center">PEMERIKSA</p>
            <table style='border: 1; font-family: Arial; font-size:10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 32px;' cellspacing="0" >

                <tr>
                 <td rowspan="5" >  <img src="<?php echo 'uploads/kosong.jpg" widht="20" height="50"';?>"></td>
                 <td >Ditandatangani secara elekronik oleh:</td>
             </tr>
             <tr>
                <td style="overflow: hidden; width: 200px;"> 

                    <?php
                    $r_atasan =  $this->session->userdata('roleatasan'); 
                    $r_biro =  $this->session->userdata('groleid');
                    $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
                    ?>
                    <?php if($TtdText == 'PLT') { ?>
                       Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
                   <?php } elseif($TtdText == 'PLH') { ?>
                     Plh. <?= get_data_people('RoleName',$Approve_People) ?>,
                 <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                    a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                    <br><?= get_data_people('RoleName', $Approve_People) ?>,
                <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                 a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                 <br><?= get_data_people('RoleName',$atasanub) ?>,
                 <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,

             <?php } else { ?>
               <?= get_data_people('RoleName',$Approve_People) ?>,
           <?php } ?> 

              </td>
         </tr>

         <tr>
            <td><br></td>
            <td><br></td>
            <td><br></td>
        </tr>

        <tr>
            <td><p style="float: right!important;"><?= $Nama_ttd_konsep ?></p></td>
        </tr>
        <tr>
            <td>  <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE ApprovelName = '" .$Nama_ttd_konsep. "' AND GroupId IN ('3','4','7') ")->row()->Pangkat; ?>
        </td>
    </tr>

</table>
        </div>
    <?php } else { ?>
    <?php } ?>


    <!-- LAMPIRAN 4 -->


    <?php
    $jum_lamp = $lampiran3>= '0';
    $no_lamp = 1;

    if ($lampiran4 >= '0') { ?>

     
            <BR>
            <page backtop="-8mm" backbottom="17mm" backleft="10mm" backright="5mm">
                <page_footer>
                <div style="width: 100%; font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </div>
                </page_footer>
                <br>
                <table style="height: 290px; width: 345px; font-family: Arial; margin-left:183px; font-size: 11px; line-height: 15px;  align-content : right;">
                <tbody>
                    <tr>
                        <td style="width: 85px; vertical-align: top; text-align: justify;">
                                <?php
                                 
                                if ($jum_lamp >= '0') {
                                    switch ($no_lamp){
                                    case 1: 
                                        $lamp_romawi = "IV";
                                        break;
                                  
                                    }
                                    echo "LAMPIRAN ".$lamp_romawi.'&nbsp;&nbsp;:';
                                }else{
                                    echo "LAMPIRAN";
                                }

                                ?>
                          </td>
                            <td style="width: 6px; text-align: justify;">&nbsp;</td>
                            <td style="width: 275.333px; text-align: justify;" colspan="3">
                             <?php
                                if ($type_naskah == 'XxJyPn38Yh.1') {
                                    echo "SURAT";          
                           }elseif ($type_naskah == 'XxJyPn38Yh.2') {
                                  echo "SURAT UNDANGAN";  
                              }elseif ($type_naskah == 'XxJyPn38Yh.3') {
                                  echo "SURAT PANGGILAN";  
                              }
                            $r_atasan =  $this->session->userdata('roleatasan');
                            $r_biro =  $this->session->userdata('groleid');
                            $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
                            ?>
                            <?php if ($TtdText == 'PLT') { ?>
                                Plt. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText == 'PLH') { ?>
                                Plh. <?= get_data_people('RoleName', $Approve_People) ?>,
                            <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                                <?= get_data_people('RoleName', $Approve_People3) ?>,

                            <?php } else { ?>
                                <?= get_data_people('RoleName', $Approve_People) ?>,
                                <?php } ?>  </td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">NOMOR</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;">.../<?= $ClCode; ?>/<?= $RoleCode; ?></td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">TANGGAL</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;">Tanggal / Bulan / Tahun</td>
                            </tr>
                            <tr>
                                <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                                <td style="width: 74px; vertical-align: top; text-align: justify;">PERIHAL</td>
                                <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                                <td style="width: 182px; vertical-align: top; text-align: justify;"> <?= $Hal; ?></td>
                            </tr>
                        </tbody>
                    </table>

                    <br><br>

                    <p class="configlampiran" style="margin-right:-20px ;margin-left:-30; margin-bottom:35px; line-height: 15px; font-family: Arial; font-size: 12px; text-align: justify;">
                         <?php
                           echo $lampiran4."<br>";
                           ?>
                   </p>
               </page>

    <?php
    $naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();
    $sql_lampiran = $this->db->query("SELECT lampiran FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->lampiran;
    ?>
      <div style="width: 60%; font-family: Arial; font-size: 12px;  margin-left: 240px; margin-top:7px; ">
          <?php
          $r_atasan =  $this->session->userdata('roleatasan'); 
          $r_biro =  $this->session->userdata('groleid');
          $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
          ?>
          <?php if($TtdText == 'PLT') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText == 'PLH') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                <br><?= get_data_people('RoleName',$Approve_People) ?>,</p>

            <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3)?>,
                 <br><?= get_data_people('RoleName',$atasanub )?>,
                 <br>u.b.<br><?= get_data_people('RoleName',$Approve_People)?>,</p>

             <?php } else { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
            <?php } ?>  
            <p align="center">PEMERIKSA</p>
            <table style='border: 1; font-family: Arial; font-size:10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 32px;' cellspacing="0" >

                <tr>
                 <td rowspan="5" >  <img src="<?php echo 'uploads/kosong.jpg" widht="20" height="50"';?>"></td>
                 <td >Ditandatangani secara elekronik oleh:</td>
             </tr>
             <tr>
                <td style="overflow: hidden; width: 200px;"> 
                    <?php
                    $r_atasan =  $this->session->userdata('roleatasan'); 
                    $r_biro =  $this->session->userdata('groleid');
                    $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
                    ?>
                    <?php if($TtdText == 'PLT') { ?>
                       Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
                   <?php } elseif($TtdText == 'PLH') { ?>
                     Plh. <?= get_data_people('RoleName',$Approve_People) ?>,
                 <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                    a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                    <br><?= get_data_people('RoleName', $Approve_People) ?>,

                <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                 a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                 <br><?= get_data_people('RoleName',$atasanub) ?>,
                 <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,

             <?php } else { ?>
               <?= get_data_people('RoleName',$Approve_People) ?>,
           <?php } ?>  
         </td>
         </tr>

         <tr>
            <td><br></td>
            <td><br></td>
            <td><br></td>
        </tr>

        <tr>
            <td><p style="float: right!important;"><?= $Nama_ttd_konsep ?></p></td>
        </tr>
        <tr>
            <td>  <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE ApprovelName = '" .$Nama_ttd_konsep. "' AND GroupId IN ('3','4','7') ")->row()->Pangkat; ?>
        </td>
    </tr>

</table>
        </div>
    <?php } else { ?>
    <?php } ?></page>
