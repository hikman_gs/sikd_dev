<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <style>
        p.indent {
            padding-left: 6.8em
        }
    </style>
</head>

<body>

    <div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -86px;">
        <img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
    </div>
    <div class="pos" id="_450:199" style="top:199;font-family:Arial; font-size:12px; margin-left:358">
        <p style=" id=" _15.8" font-family:Arial; font-size:12px;color:#000000">
            Tempat / Tanggal / Bulan / Tahun </p>
    </div>
    <div class="pos" id="_450:199" style="top:199; margin-left:359;font-family:Arial; font-size:12px;">
        <p style=" font-family:Arial; id=" _15.8" font-size:12px; color:#000000">
            Kepada 2</p>
    </div>
    <table cellpadding="17" cellspacing="1">
        <tbody>
            <tr><br>
                <td align="normal" valign="top" width="352">
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Nomor &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp;<?= $id; ?>/<?= $ClCode; ?>/<?= $RoleCode; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Sifat
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:35px; color:#000000">
                            :&nbsp;&nbsp;&nbsp;&nbsp;<?= $SifatId; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Lampiran
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:9; color:#000000">
                            :&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Jumlah . ' ' . $MeasureUnitId; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Hal
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:41; color:#000000">
                            :

                        </span>
                        <div style="width: 70%; font-family: Arial; font-size: 12px; margin-left: 11px;">

                            <?= $Hal; ?>
                        </div>
                    </div>
                </td>

                <td align="normal" valign="top" width="180;font-family:Arial; font-size:12px;">Yth.&nbsp;
                    <div class="pos" id="_118:320" style="top:320;left:200;font-family:Arial; font-size:12px;">
                        <?= $RoleId_To; ?>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;margin-left:22;font-family:Arial;font-size:12px; ">di
                    </div>
                    <div class="pos" id="_118:320" style="top:320;margin-left:35; margin-right:15px; font-family:Arial;font-size:12px;"><?= $Alamat; ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <br><br>




    <p style="margin-right:-5px ;margin-left:72;line-height: 15px; font-family: Arial; font-size: 12px; text-align: justify;">
    <p class="indent"><?= $Konten; ?></p>
    </p>

    <br><br><br>
    <div style="width: 50%; font-family: Arial; font-size: 12px; margin-left: 300px; margin-top:-40px; ">

        <?php

        if ($this->input->post('TtdText2') == 'Atas_Nama') {
            $a = $this->input->post('tmpid_atas_nama');
        } else {
            $a = $this->input->post('Approve_People3');
        }
        ?>
        <?php if ($TtdText == 'PLT') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } elseif ($TtdText == 'PLH') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                <br><?= get_data_people('RoleName', $Approve_People) ?>,
            </p>
        <?php } else { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } ?>




        <p align="center">PEMERIKSA</p>

        <br>
    </div>

    <div>
        <table style='border: 1; font-family: Arial; font-size: 8px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 320px;' cellspacing="0" cellpadding="0">
            <tr>
                <td rowspan="5"><img src="<?php echo base_url(); ?>/uploads/kosong.jpg" width="55" height="60"></td>
                <td>Ditandatangani secara elekronik oleh:</td>
            </tr>
            <tr>
                <td style="overflow: hidden; width: 200px;"><?= get_data_people('RoleName', $Approve_People) ?>,</td>
            </tr>

            <tr>
                <td><br></td>
                <td><br></td>
                <td><br></td>
                <td><br></td>
            </tr>
            <tr>
                <td>
                    <p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
                </td>
            </tr>
            <tr>
                <td> <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "'")->row()->Pangkat; ?>
                </td>
            </tr>
        </table>
    </div>

    <br><br>
    <table width="100%" style="font-family: Arial; font-size: 12px; line-height: 15px;">
        <tr>
            <td width="10%" valign="top">
                <?php
                if (!empty($RoleId_Cc)) {
                    $exp = explode(',', $RoleId_Cc);
                    $z = count($exp);
                    if ($z > 1) {
                        $n = 1;
                        echo "Tembusan : <br>";
                        foreach ($exp as $k => $v) {
                            $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
                            $uc  = ucwords(strtolower($xx1));
                            $str = str_replace('Dan', 'dan', $uc);
                            if ($n < $z) {
                                echo $n . '. ' . $str . ';<br>';
                            } else {
                                echo $n . '. ' . $str . '.<br>';
                            }
                            $n++;
                        }
                    } else {
                        echo "Tembusan : <br>";
                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $exp[0] . "'")->row()->RoleName;
                        $uc  = ucwords(strtolower($xx1));
                        $str = str_replace('Dan', 'dan', $uc);
                        echo $n . '. ' . $str . '.<br>';
                    }
                }
                ?>
            </td>
        </tr>
    </table>
    <br>
    <page_footer>
        <table style="font-family: Arial; font-size: 9px; line-height: 15px;" align="center">
            <tr>
                <td align="center">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </td>
            </tr>
        </table>
    </page_footer>
</body>

</html>