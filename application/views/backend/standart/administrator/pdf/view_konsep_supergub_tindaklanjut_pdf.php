<?php
$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = 'uk.1' ")->row()->Header;
?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
</head>

<body>

	<div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -86px;">
		<img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
	</div>
	<br><br>

	<table style="margin: auto; font-family: Arial; font-size: 12px; line-height: 20px;">
		<tr>
			<td style="text-align: center;">SURAT PERINTAH</td>
		</tr>
		<tr>
			<td style="text-align: center;">NOMOR : .........../<?= $ClCode; ?>/<?= $RoleCode; ?></td>
		</tr>
		<tr>
			<td></td>
		</tr>
		<tr>
			<td></td>
		</tr>
	</table>

	<br><br>

	<table style="font-family: Arial; font-size: 12px; line-height: 20px;">
		<tr style="overflow: hidden;" valign="top">
			<td style="text-align: justify; width: 100px;">NAMA</td>
			<td style="text-align: justify; width: 5px;">:</td>
			<td style="text-align: justify; width: 380;">

				<?php
	$a='uk.1';
	$z = $this->db->query("SELECT PeopleName FROM people WHERE PrimaryRoleId = '".$a."'")->row()->PeopleName;
			
echo "$z"; 
			?></td>
		</tr>

		<tr style="overflow: hidden;" valign="top">
			<td style="text-align: justify; width: 100px;">Jabatan</td>
			<td style="text-align: justify; width: 5px;">:</td>
			<td style="text-align: justify; width: 380;">Gubernur Jawa Barat</td>
		</tr>
	</table>

	<table style="margin:auto; font-family: Arial; font-size: 12px; line-height: 20px;">
		<tr>
			<td style="text-align: center;">MEMERINTAHKAN</td>
			<td style="text-align: center;">:</td>
		</tr>
	</table>
	<br><br>

	<!-- BARIS INI -->
	<table style="font-family: Arial; font-size: 12px; line-height: 20px; width: 400;">
		<tr>
			<td style="text-align: justify; width: 100px;" rowspan="1000" valign="top">Kepada</td>
			<td style="text-align: justify; width: 5px;" rowspan="1000" valign="top">:</td>
		</tr>
	</table>

	<?php

	$exp = explode(',', $RoleId_To);
	$z = count($exp);
	if ($z > 1) {
		$n = 1;

		foreach ($exp as $k => $v) {

			$xx1 =  $this->db->query("SELECT PeopleName FROM people WHERE PrimaryRoleId = '" . $v . "'")->row()->PeopleName;
			$xx2 =  $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $v . "'")->row()->Pangkat;
			$xx3 =  $this->db->query("SELECT NIP FROM people WHERE PrimaryRoleId = '" . $v . "'")->row()->NIP;
			$xx4 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;

			echo '<table style="font-family: Arial; font-size: 12px; line-height: 20px; width: 400;">
			<tr>
			<td style="text-align: justify; width: 100px;"  rowspan="1000" valign="top"></td>
			<td style="text-align: justify; width: 5px;" rowspan="1000" valign="top"></td>

			<tr style="text-align: justify; width: 400; line-height: 30px;">' .
				'<td style="text-align: justify;" valign="top">' . $n . '. ' . '</td>' .
				'<td style="text-align: justify;" valign="top">' . 'Nama' . '</td>' .
				'<td style="text-align: justify;" valign="top">' . ':' . '</td>' .
				'<td style="text-align: justify; width: 400;" valign="top">' . $xx1 . '</td>' .
				'</tr>' .

				'<tr>' .
				'<td style="text-align: justify;" >' . '</td>' .
				'<td style="text-align: justify;" valign="top">' . 'Pangkat' . '</td>' .
				'<td style="text-align: justify;"valign="top">' . ':' . '</td>' .
				'<td style="text-align: justify; width: 400;" valign="top">' . $xx2 . '</td>' .
				'</tr>' .

				'<tr>' .
				'<td style="text-align: justify;">' . '</td>' .
				'<td style="text-align: justify; ">' . 'NIP' . '</td>' .
				'<td style="text-align: justify; ">' . ':' . '</td>' .
				'<td style="text-align: justify; width: 400; ">' . $xx3 . '</td>' .
				'</tr>' .

				'<tr>' .
				'<td style="text-align: justify;">' . '</td>' .
				'<td style="text-align: justify; " "valign="top">' . 'Jabatan' . '</td>' .
				'<td style="text-align: justify; ""valign="top">' . ':' . '</td>' .
				'<td style="text-align: justify; width: 400; line-height: 15px;">' . $xx4 . '</td>' .
				'</tr></tr></table>';

			$n++;
		}
	} else {
		$xx1 =  $this->db->query("SELECT PeopleName FROM people WHERE PrimaryRoleId = '" . $exp[0] . "'")->row()->PeopleName;
		$xx2 =  $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $exp[0] . "'")->row()->Pangkat;
		$xx3 =  $this->db->query("SELECT NIP FROM people WHERE PrimaryRoleId = '" . $exp[0] . "'")->row()->NIP;
		$xx4 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $exp[0] . "'")->row()->RoleName;

		echo '<table><td style="text-align: justify;"valign="top">' . 'Nama' . '</td>' .
			'<tr>' .
			'<td style="text-align: justify;"valign="top">' . ':' . '</td>' .
			'<td style="text-align: justify; "valign="top">' . $xx1 . '</td>' .
			'</tr>' .

			'<tr>' .
			'<td style="text-align: justify; "valign="top">' . 'Pangkat' . '</td>' .
			'<td style="text-align: justify; "valign="top">' . ':' . '</td>' .
			'<td style="text-align: justify;"valign="top" >' . $xx2 . '</td>' .
			'</tr>' .

			'<tr>' .
			'<td style="text-align: justify; "valign="top">' . 'NIP' . '</td>' .
			'<td style="text-align: justify; "valign="top">' . ':' . '</td>' .
			'<td style="text-align: justify; "valign="top">' . $xx3 . '</td>' .
			'</tr>' .

			'<tr>' .
			'<td style="text-align: justify; "valign="top">' . 'Jabatan' . '</td>' .
			'<td style="text-align: justify; "valign="top">' . ':' . '</td>' .
			'<td style="text-align: justify; "valign="top">' . $xx4 . '</td>' .
			'</tr></table>';
	}
	?>

	<!-- Sampai Sini -->




	<br>
	<table>

		<tr>
			<td style="text-align: justify; width: 100px;" rowspan="5" valign="top">Untuk</td>
			<td style="text-align: justify; width: 5px;" rowspan="5" valign="top">:</td>
			<td style="text-align: justify; width: 480px;"><?= $Konten; ?></td>
		</tr>
	</table>
	<div style="width: 50%; font-family: Arial; font-size: 12px; margin-left: 300px; margin-top: 50px;">
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px; ">Ditetapkan di ..............</p>
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Pada tanggal ..................</p>

		<?php if ($TtdText == 'PLT') { ?>
			<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName', $Approve_People) ?></p>
		<?php } elseif ($TtdText == 'PLH') { ?>
			<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName', $Approve_People) ?></p>
		<?php } else { ?>
			<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName', $Approve_People) ?></p>
		<?php } ?>

		<p align="center">PEMERIKSA</p>

		<table style='border: 1; font-family: Arial; font-size: 10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 25px;' cellspacing="0">
			<tr>
				<td rowspan="5"> <img src="<?php echo base_url(); ?>/uploads/kosong.jpg" widht="55" height="60"></td>
				<td>Ditandatangani secara elekronik oleh:</td>
			</tr>
			<tr>
				<td style="overflow: hidden; width: 200px;"><?= get_data_people('RoleName', $Approve_People) ?>,</td>
			</tr>

			<tr>
				<td><br></td>
				<td><br></td>
				<td><br></td>
			</tr>

			<tr>
				<td>
					<p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
				</td>
			</tr>
			<tr>
				<td> <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "'")->row()->Pangkat; ?>
				</td>
			</tr>

		</table>
	</div>

	<br><br>
	
	<page_footer>
		<table style="font-family: Arial; font-size: 9px; line-height: 15px;" align="center">
			<tr>
				<td align="center">
					Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
					<br>
					Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
				</td>
			</tr>
		</table>
	</page_footer>
</body>

</html>