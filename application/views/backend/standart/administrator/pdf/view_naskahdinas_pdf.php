<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="utf-8">
</head>

<body>

	<img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">

	<br><br>

	<table style="margin: auto; font-family: Arial; font-size: 12px; line-height: 20px;">
		<tr>
			<td style="text-align: center;">NASKAH DINAS</td>
		</tr>
		<tr>
			<td style="text-align: center;">NOMOR : ........./<?= $ClCode; ?>/<?= $RoleCode; ?></td>
		</tr>
	</table>

	<br>

	<table style="width: 100%!important; font-family: Arial; font-size: 12px;">
		<tr>
			<td valign="top" style="line-height: 15px;">Yth</td>
			<td valign="top" style="line-height: 15px;">:</td>
			<td valign="top" style="line-height: 15px;">
				<?php
				if (count($RoleId_To) > 1) {
					$n = 1;
					foreach ($RoleId_To as $k => $v) {
						$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
						echo $n . '. ' . $xx1 . '<br>';
						$n++;
					}
				} else {
					$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $RoleId_To[0] . "'")->row()->RoleName;
					echo $xx1;
				}
				?>
			</td>
		</tr>
		<tr>
			<?php
			if ($Jumlah != 0) {
			?>
				<td style="line-height: 15px;">Lampiran</td>
				<td style="line-height: 15px;">:</td>
				<td style="line-height: 15px;">
					<?php
					echo $Jumlah . ' ' . $MeasureUnitId;
					?>
				</td>
			<?php } ?>
		</tr>
		<tr>
			<td style="line-height: 15px;">Hal</td>
			<td style="line-height: 15px;">:</td>
			<td style="line-height: 15px;"><?= $Hal; ?></td>
		</tr>
	</table>

	<br><br>

	<div style="width: 98%; font-family: Arial; font-size: 12px; text-align: justify; margin-left: 5px; line-height: 15px;">
		<?= $Konten; ?>
	</div>

	<div style="width: 50%; font-family: Arial; font-size: 12px; margin-left: 300px; margin-top: 50px;">

		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">-</p>

		<?php if ($TtdText == 'PLT') { ?>
			<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName', $Approve_People) ?></p>
		<?php } elseif ($TtdText == 'PLH') { ?>
			<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName', $Approve_People) ?></p>
		<?php } else { ?>
			<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName', $Approve_People) ?></p>
		<?php } ?>

		<br><br><br><br>
		<p style="float: right!important; text-align: center; font-family: Arial; font-size: 12px;"><?= $Nama_ttd_konsep ?></p>

	</div>
	<br><br>
	<table width="100%" style="font-family: Arial; font-size: 12px; line-height: 15px;">
		<tr>
			<td width="10%" valign="top">
				<?php
				if (!empty($RoleId_Cc)) {
					if (count($RoleId_Cc) > 1) {
						$n = 1;
						echo "Tembusan : <br>";
						foreach ($RoleId_Cc as $k => $v) {
							$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
							echo $n . '. ' . $xx1 . '<br>';
							$n++;
						}
					} else {
						echo "Tembusan : <br>";
						$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $RoleId_Cc[0] . "'")->row()->RoleName;
						echo $xx1;
					}
				}
				?>
			</td>
		</tr>
	</table>
</body>

</html>