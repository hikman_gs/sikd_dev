<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <style>
        p.indent {
            padding-left: 6.8em
        }
    </style>
</head>

<body>
    <div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -90px;">
        <img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
    </div>
    <p style="text-align: center;"><b>NOTA DINAS</b></p>

    <table style="margin-left:15; width: 500; margin-right:30; font-family: Arial; font-size: 12px; line-height: 10px;">
        <tr>
            <td style="line-height: 15px; vertical-align:top; width : 53;" rowspan="100">
                Kepada
            </td>
            <td style="line-height: 15px; vertical-align:top;" rowspan="100">
                :
            </td>
        </tr>

        <?php
        if (count($RoleId_To) > 1) {
            $n = 1;
            $z = 1;

            foreach ($RoleId_To as $k => $v) {
                $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
                $uc  = ucwords(strtolower($xx1));
                $str = str_replace('Dan', 'dan', $uc);
                $str = str_replace('Uptd', 'UPTD', $uc);

                echo  '<tr>' . '<td>' . '</td>' .
                    '<td style="width: 10; line-height: 15px;">' . $n . '. ' . '</td>' .
                    '<td style="width: 500; line-height: 15px;">' . $str . '</td>' . '</tr>';


                $n++;
            }
        } else {
            $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $RoleId_To[0] . "'")->row()->RoleName;
            $uc  = ucwords(strtolower($xx1));
            $str = str_replace('Dan', 'dan', $uc);
            $str = str_replace('Uptd', 'UPTD', $uc);
            echo  '<tr>' .
                '<td>' . '</td>' .
                '<td style="width: 500; line-height: 15px;">' . $str . '</td>' . '</tr>';
        }
        ?>
    </table>

    <table style="margin-left:15; width: 500; margin-right:30; font-family: Arial; font-size: 12px; line-height: 10px;">
        <tr>
            <td style="line-height: 10px;vertical-align:top; width : 53;">
                Dari
            </td>
            <td style="line-height: 15px;vertical-align:top">
                :
            </td>
            <td></td>
            <td style="line-height: 15px; overflow: hidden; width: 500;">
                <?= $uc  = ucwords(strtolower(get_data_people('RoleName', $Approve_People)));
                $str = str_replace('Dan', 'dan', $uc);
                $str = str_replace('Uptd', 'UPTD', $str);
                ?>
            </td>

        </tr>
    </table>

    <table style="margin-left:15; width: 500; margin-right:30; font-family: Arial; font-size: 12px; line-height: 10px;">
        <tr>
            <td style="line-height: 15px;vertical-align:top; width : 53;" rowspan="100">
                Tembusan
            </td>
            <td style="line-height: 15px;vertical-align:top;" rowspan="100">:
            </td>
        </tr>

        <?php
        if (!empty($RoleId_Cc)) {
            if (count($RoleId_Cc) > 1) {
                $n = 1;
                foreach ($RoleId_Cc as $k => $v) {
                    $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
                    $uc  = ucwords(strtolower($xx1));
                    $str = str_replace('Dan', 'dan', $uc);
                    $str = str_replace('Uptd', 'UPTD', $uc);

                    echo  '<tr>' . '<td>' . '</td>' .
                        '<td style="width: 10;  line-height: 15px;">' . $n . '. ' . '</td>' .
                        '<td style="width: 500;  line-height: 15px;">' . $str . '</td>' . '</tr>';

                    $n++;
                }
            } else {
                 $xx2 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$RoleId_Cc[0]."'")->row()->RoleName;
                $uc  = ucwords(strtolower($xx2));
                $str = str_replace('Dan', 'dan', $uc);
                $str = str_replace('Uptd', 'UPTD', $uc);
                echo  '<tr>' .
                    '<td>' . '</td>' .
                    '<td style="width: 500;  line-height: 15px; ">' . $str . '</td>' . '</tr>';
            }
        }
        ?>

    </table>
    <br>
    <table style="margin-left:15; width: 500; margin-right:30; font-family: Arial; font-size: 12px; margin-top:0px;">

        <tr>
            <td style="line-height: 10px;vertical-align:top; width : 53;">
                Nomor
            </td>
            <td>:

            </td style="width : 10;">
            <td style=" width: 300;" colspan="10">
            &nbsp;....../<?= $ClCode; ?>/ <?= set_value('rolecode') ?>

            </td>

        </tr>
        <tr>
            <td style="vertical-align:top; width : 53;">
                Tanggal
            </td>
            <td style="">
                :
            </td>
            <td style="width: 300;" colspan="10">
                &nbsp;<?= get_bulan($TglReg) ?>
            </td>

        </tr>

        <tr>
            <td style="line-height: 15px;">
                Sifat
            </td>
            <td style="line-height: 15px;">
                :
            </td>
            <td style="width: 300; line-height: 10px;" colspan="10">
                &nbsp;<?= $SifatId; ?>
            </td>
        </tr>
        <tr>
            <?php
            if ($Jumlah != 0) {
            ?>
                <td style="line-height: 15px; vertical-align: text-top;">Lampiran
                </td>
                <td style="line-height: 15px; vertical-align: text-top;">
                    :

                </td>
                <td style="line-height: 10px;" colspan="10">
                    <div style="width: 500; text-align: justify;"><?= '&nbsp;' . $Jumlah . ' ' . $MeasureUnitId; ?></div>
                </td>
            <?php } ?>
        </tr>
        <tr>
            <td style="line-height: 15px; vertical-align: text-top; ">Hal
            </td>
            <td style="line-height: 15px; vertical-align: text-top; ">
                :

            </td>
            <td style="line-height: 10px;" colspan="10">
                <div style="width: 500; text-align: justify;"><?= $Hal; ?></div>
            </td>
        </tr>

        

    </table>

    <p style="margin-right:1;margin-left:10;" align="center">
        <hr>
    </p>
    <p style="margin-right:-4px ; margin-left:30; line-height: 15px; margin-top:-12px; font-family: Arial; font-size: 12px; text-align: justify;">

    <p style="margin-right:8px ; margin-left:-13; line-height: 20px; font-family: Arial; font-size: 12px; text-align: justify;"><?= $Konten; ?></p>

    </p>

    <div style="width: 50%; font-family: Arial; font-size: 12px; margin-left:280px; margin-top:-20px; ">

        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"></p>

       <?php
$r_atasan =  $this->session->userdata('roleatasan'); 
$r_atasan3 =  $this->session->userdata('PrimaryRoleId'); 
$r_atasan2 =$this->session->userdata(['RoleAtasan',$r_atasan]); 
$r_biro =  $this->session->userdata('groleid');
$age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
     $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND GroupId NOT IN ('8') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='".$this->session->userdata('roleatasan')."'")->result();
$atasanub= get_data_people('RoleAtasan',$Approve_People );

?>
    <?php if($TtdText == 'PLT') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText == 'PLH') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
            <br><?= get_data_people('RoleName',$Approve_People) ?>,</p>
     <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3)?>,
                        <br><?= get_data_people('RoleName',$atasanub )?>,
                        <br>u.b.<br><?= get_data_people('RoleName',$Approve_People)?>,</p>

    <?php } else { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } ?>  


        <p align="center">PEMERIKSA</p>
        <div>
            <table style='border: 1; font-family: Arial; font-size: 9px; table-layout: fixed; overflow: hidden; width: 50%; height: 200; margin-left: 10px;' cellspacing="0">

                <tr>
                    <td rowspan="5"> <img src="<?php echo base_url(); ?>/uploads/kosong.jpg" widht="55" height="60"></td>
                    <td>Ditandatangani secara elekronik oleh:</td>
                </tr>
                <tr>
                    <td style="overflow: hidden; width: 200px;">
                        
               <?php
               $r_atasan =  $this->session->userdata('roleatasan'); 
               $r_biro =  $this->session->userdata('groleid');
               $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
               ?>
               <?php if($TtdText == 'PLT') { ?>
                 Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
             <?php } elseif($TtdText == 'PLH') { ?>
               Plh. <?= get_data_people('RoleName',$Approve_People) ?>,
               
               <br><?= get_data_people('RoleName',$Approve_People) ?>,
           <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
               a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
               <br><?= get_data_people('RoleName',$atasanub) ?>,
               <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,

           <?php } else { ?>
             <?= get_data_people('RoleName',$Approve_People) ?>,
             <?php } ?>


                    </td>
                </tr>

                <tr>
                    <td><br></td>
                    <td><br></td>
                    <td><br></td>
                </tr>

                <tr>
                    <td>
                        <p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
                    </td>
                </tr>
                <tr>
                    <td> <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat; ?>
                    </td>
                </tr>

            </table>
        </div>
        <p style="float: right!important; text-align: center; font-family: Arial; font-size: 12px;" hidden=""></p>

    </div>
    <br>
    <page_footer>
        <table style="font-family: Arial; font-size: 9px; line-height: 15px;" align="center">
            <tr>
                <td align="center">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </td>
            </tr>
        </table>
    </page_footer>
</body>

</html>