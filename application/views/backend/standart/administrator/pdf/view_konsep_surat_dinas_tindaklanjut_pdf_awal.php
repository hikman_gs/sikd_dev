<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <style>
        p.indent {

            padding-left: 6.8em
        }

        h1 {
            margin: 0;
            padding: 20px 0 20px 0;
            background-color: #77D312;
            color: #fff;
            font-size: 1.7em;
            font-weight: 700;
            text-align: center;
        }

        .container {
            margin-bottom: 30px;
            padding: 0 20px 20px 20px;
            width: 700px;
            padding-bottom: 7px;
        }


        /* #footer2 {
      position: fixed;
      right: 0px;
      bottom: 10px;
      text-align: center;
      border-top: 1px solid black;
    }
    */
    #footer {
        position: absolute;
        bottom: 0;
        width: 100%;
        height: 60px;
        /* tinggi dari footer */
        vertical-align: bottom;
    }

    @page: first {
        size: 5.5in 8.5in;
        margin-top: -10px;

    }

    .configlampiran {
        size: F4;
        margin-top: -10px;


    }

    #footer .page:after {
        size: 5.5in 8.5in;
        content: counter(page, decimal);
        margin-top: -10px;

    }


    footer {
        margin-top: 20px;
        color: #999999;
        text-align: center;
    }
</style>
</head>

<body>

    <div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -86px;">
        <img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
    </div>
    <div class="pos" id="_450:199" style="top:199;font-family:Arial; font-size:12px; margin-left:358">
        <p style=" id=" _15.8" font-family:Arial; font-size:12px;color:#000000">
        Tempat / Tanggal / Bulan / Tahun </p>
    </div>
    <div class="pos" id="_450:199" style="top:199; margin-left:359;font-family:Arial; font-size:12px;">
        <p style=" font-family:Arial; id=" _15.8" font-size:12px; color:#000000">
        Kepada</p>
    </div>
    <table cellpadding="17" cellspacing="1">
        <tbody>
            <tr><br>
                <td align="normal" valign="top" width="352">
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Nomor &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; .../<?= $ClCode; ?>/<?= $RoleCode; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Sifat
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:35px; color:#000000">
                            :&nbsp;&nbsp;&nbsp;&nbsp;<?= $SifatId; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Lampiran
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:9; color:#000000">
                            :&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Jumlah . ' ' . $MeasureUnitId; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:115; text-align: justify">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            Hal
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:41; color:#000000">
                            :

                        </span>
                        <div style="width: 60%; font-family: Arial; font-size: 12px; margin-left: 11px;">

                            <?= $Hal; ?>
                        </div>
                    </div>
                </td>

                <td align="normal" valign="top" width="180;font-family:Arial; font-size:12px;">Yth.&nbsp;
                    <div class="pos" id="_118:320" style="top:320;left:200;font-family:Arial; font-size:12px;">
                        <?= $RoleId_To; ?>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;margin-left:22;font-family:Arial;font-size:12px; ">di
                    </div>
                    <div class="pos" id="_118:320" style="top:320;margin-left:35; margin-right:15px; font-family:Arial;font-size:12px;"><?= $Alamat; ?>
                </div>
            </td>
        </tr>
    </tbody>
</table>
<br><br>


<p style="margin-right:-5px ;margin-left:75; line-height: 17px; font-family: Arial; font-size: 12px; text-align: justify;">
    <p class="indent"><?= $Konten; ?></p>
</p>

<br><br>
<div style="width: 55%; font-family: Arial; font-size: 12px; margin-left: 281px; margin-top:-40px; ">
    <?php
    $r_atasan =  $this->session->userdata('roleatasan'); 
    $r_biro =  $this->session->userdata('groleid');
    $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
    ?>
    <?php if($TtdText == 'PLT') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText == 'PLH') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
    <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
        <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
            <br><?= get_data_people('RoleName',$Nama_ttd_atas_nama) ?>,</p>
        <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                <br><?= get_data_people('RoleName',$r_atasan) ?>,
                <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,</p>
            <?php } else { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
            <?php } ?>  

            <p align="center">PEMERIKSA</p>

            <table style='border: 1; font-family: Arial; font-size: 8px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 29px;' cellspacing="0" >

                <tr>
                    <td rowspan="5" >  <img src="<?php echo base_url(); ?>/uploads/kosong.jpg" widht="20" height="50"></td> 
                    <td >Ditandatangani secara elekronik oleh:</td>
                </tr>
                <tr>
                    <td style="overflow: hidden; width: 200px;">
                       <?php
                       $r_atasan =  $this->session->userdata('roleatasan'); 
                       $r_biro =  $this->session->userdata('groleid');
                       $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 

                       if($TtdText == 'PLT') { ?>
                         Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
                     <?php } elseif($TtdText == 'PLH') { ?>
                       Plh. <?= get_data_people('RoleName',$Approve_People) ?>,
                       <br><?= get_data_people('RoleName',$Approve_People) ?>,
                   <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                       a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                       <br><?= get_data_people('RoleName',$r_atasan) ?>,
                       <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,

                   <?php } else { ?>
                     <?= get_data_people('RoleName',$Approve_People) ?>,
                 <?php } ?>  
             </td>
         </tr>

         <tr>
            <td><br></td>
            <td><br></td>
            <td><br></td>
        </tr>

        <tr>
            <td><p style="float: right!important;"><?= $Nama_ttd_konsep ?></p></td>
        </tr>
        <tr>
            <td>
                <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat; ?>
            </td>
        </tr>

    </table>
    <br>
</div>

<br>
<table width="100%" style="font-family: Arial; font-size: 12px; line-height: 15px;">
    <tr>
      <td width="10%" valign="top">
        <?php
        if (!empty($RoleId_Cc)) {
          $exp = explode(',', $RoleId_Cc);
          $z = count($exp);
          if ($z > 1) {
            $n = 1;
            $y = 'Yth.';
            echo "Tembusan : <br>";
            foreach ($exp as $k => $v) {
              $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
              $uc  = ucwords(strtolower($xx1));
              $str = str_replace('Dan', 'dan', $uc);
              $str = str_replace('Uptd', 'UPTD', $str);
              $str = str_replace('Dprd', 'DPRD', $str);
              if ($n < $z) {

                if ($n == ($z-1)) {
                    echo
                    '<table border="0" height="20" width="100">
                    <tr style="width:80px" margin-right="50px" height="20">
                    <td style="text-align: justify; width: 10;"valign="top">' . $n . '.' . '</td>
                    <td style="text-align: justify; margin-right:30px;"valign="top">' . $y . '&nbsp;</td>
                    <td style="text-align: justify; width: 545;"valign="top">' . $str . '; dan</td>
                    </tr>
                    </table>';
                }else{
                    echo
                    '<table border="0" height="20" width="100">
                    <tr style="width:80px" margin-right="50px" height="20">
                    <td style="text-align: justify; width: 10;"valign="top">' . $n . '.' . '</td>
                    <td style="text-align: justify; margin-right:30px;"valign="top">' . $y . '&nbsp;</td>
                    <td style="text-align: justify; width: 545;"valign="top">' . $str . ';</td>
                    </tr>
                    </table>';
                }
                
            } else {
                echo
                '<table border="0" height="20" width="100">

                <tr style="width:80px" margin-right="50px" height="20">
                <td style="text-align: justify; width: 10;"valign="top">' . $n . '.' . '</td>
                <td style="text-align: justify; margin-right:30px;"valign="top">' . $y . '&nbsp;</td>
                <td style="text-align: justify; width: 545;"valign="top">' . $str . '.</td>
                </tr>

                </table>';
            }
            $n++;
        }
    } else {
        echo "Tembusan : <br>";
        $y = 'Yth.';
        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $exp[0] . "'")->row()->RoleName;
        $uc  = ucwords(strtolower($xx1));
        $str = str_replace('Dan', 'dan', $uc);
        $str = str_replace('Uptd', 'UPTD', $str);
        $str = str_replace('Dprd', 'DPRD', $str);
        echo
        '<table border="0" height="20" width="100">

        <tr style="width:80px" margin-right="50px" height="20">

        <td style="text-align: justify; margin-right:30px;"valign="top">' . $y . '&nbsp;</td>
        <td style="text-align: justify; width: 545;"valign="top">' . $str . '</td>
        </tr>

        </table>';
    }
}
?>
</td>
</tr>
</table>

<br><br>
<page_footer>
<table style="font-family: Arial; font-size: 9px; line-height: 15px;" align="center">
    <tr>
        <td align="center">
            Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
            <br>
            Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
        </td>
    </tr>
</table>
</page_footer>
<?php
if ($lampiran >= '0') { ?>
    <BR>
    <page backtop="-8mm" backbottom="17mm" backleft="10mm" backright="5mm">



        <page_footer>
        <div style="width: 100%; font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
            Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
            <br>
            Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
        </div>
        </page_footer>

        <br>

        <table style="height: 290px; width: 340px; font-family: Arial; margin-left:196px; font-size: 10px; line-height: 15px;  align-content : right;">
            <tbody>
                <tr>
                    <td style="width: 70px; vertical-align: top; text-align: justify;">LAMPIRAN&nbsp;&nbsp; :</td>
                    <td style="width: 6px; text-align: justify;">&nbsp;</td>
                    <td style="width: 275.333px; text-align: justify;" colspan="3">
                       <?php
                       if  ($this->input->post('type_naskah') == 'XxJyPn38Yh.2') {
                        echo "SURAT UNDANGAN ";          
                    } else { 
                        echo "SURAT ";
                    }
                    $r_atasan =  $this->session->userdata('roleatasan');
                    $r_biro =  $this->session->userdata('groleid');
                    $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
                    ?>
                    <?php if ($TtdText == 'PLT') { ?>
                        Plt. <?= get_data_people('RoleName', $Approve_People) ?>,
                    <?php } elseif ($TtdText == 'PLH') { ?>
                        Plh. <?= get_data_people('RoleName', $Approve_People) ?>,
                    <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                        <?= get_data_people('RoleName', $Approve_People) ?>,

                    <?php } else { ?>
                        <?= get_data_people('RoleName', $Approve_People) ?>,
                        <?php } ?>  </td>
                    </tr>
                    <tr>
                        <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 74px; vertical-align: top; text-align: justify;">NOMOR</td>
                        <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                        <td style="width: 182px; vertical-align: top; text-align: justify;">.../<?= $ClCode; ?>/<?= $RoleCode; ?></td>
                    </tr>
                    <tr>
                        <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 74px; vertical-align: top; text-align: justify;">TANGGAL</td>
                        <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                        <td style="width: 182px; vertical-align: top; text-align: justify;">Tanggal / Bulan / Tahun</td>
                    </tr>
                    <tr>
                        <td style="width: 70px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 6px; vertical-align: top; text-align: justify;">&nbsp;</td>
                        <td style="width: 74px; vertical-align: top; text-align: justify;">PERIHAL</td>
                        <td style="width: 7.33333px; vertical-align: top; text-align: justify;">:</td>
                        <td style="width: 182px; vertical-align: top; text-align: justify;"> <?= $Hal; ?></td>
                    </tr>
                </tbody>
            </table>
            
            <br><br>

            <p class="configlampiran" style="margin-right:-20px ;margin-left:-30; margin-bottom:35px; line-height: 15px; font-family: Arial; font-size: 12px; text-align: justify;">
                <p><?= $lampiran; ?></p>
            </p>
        </page>
    <?php } else { ?>
    <?php } ?>
    <br>

    <?php
    $naskah = $this->db->query("SELECT * FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row();
    $sql_lampiran = $this->db->query("SELECT lampiran FROM konsep_naskah WHERE NId_Temp = '" . $NId . "' LIMIT 1")->row()->lampiran;
    if ($lampiran >= '0') { ?>
      <div style="width: 60%; font-family: Arial; font-size: 12px;  margin-left: 240px; margin-top:-40px; ">
          <?php
          $r_atasan =  $this->session->userdata('roleatasan'); 
          $r_biro =  $this->session->userdata('groleid');
          $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
          ?>
          <?php if($TtdText == 'PLT') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText == 'PLH') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?>,</p>
        <?php } elseif($TtdText2 == 'Atas_Nama') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                <br><?= get_data_people('RoleName',$Nama_ttd_atas_nama) ?>,</p>

            <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                    <br><?= get_data_people('RoleName',$r_atasan) ?>,
                    <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,</p>
                <?php } else { ?>
                    <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?>,</p>
                <?php } ?>  
                <p align="center">PEMERIKSA</p>
                <table style='border: 1; font-family: Arial; font-size: 8px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 32px;' cellspacing="0" >

                    <tr>
                     <td rowspan="5" >  <img src="<?php echo 'uploads/kosong.jpg" widht="20" height="50"';?>"></td>
                     <td >Ditandatangani secara elekronik oleh:</td>
                 </tr>
                 <tr>
                    <td style="overflow: hidden; width: 200px;">
                        <?php
                        $r_atasan =  $this->session->userdata('roleatasan'); 
                        $r_biro =  $this->session->userdata('groleid');
                        $age =$this->session->userdata('groleid')=='XxJyPn38Yh.3'; 
                        ?>
                        <?php if($TtdText == 'PLT') { ?>
                         Plt. <?= get_data_people('RoleName',$Approve_People) ?>,
                     <?php } elseif($TtdText == 'PLH') { ?>
                       Plh. <?= get_data_people('RoleName',$Approve_People) ?>,

                   <?php } elseif($TtdText2 == 'untuk_beliau') { ?>
                       a.n.&nbsp;<?= get_data_people('RoleName',$Approve_People3) ?>,
                       <br><?= get_data_people('RoleName',$r_atasan) ?>,
                       <br>u.b.<br><?= get_data_people('RoleName',$Approve_People) ?>,

                   <?php } else { ?>
                     <?= get_data_people('RoleName',$Approve_People) ?>,
                 <?php } ?>  


             </td>
         </tr>

         <tr>
            <td><br></td>
            <td><br></td>
            <td><br></td>
        </tr>

        <tr>
            <td><p style="float: right!important;"><?= $Nama_ttd_konsep ?></p></td>
        </tr>
        <tr>
            <td> <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat; ?>
        </td>
    </tr>

</table>
</div>
<?php } else { ?>
<?php } ?>


</body>

</html>