<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <style>
        body {
            font-family: Arial;
            font-size: 12px;
        }

        #footer {
            position: absolute;
            bottom: 0px;
            width: 100%;
            height: 0px;
            /* tinggi dari footer */
            vertical-align: bottom;
        }

        @page {
            size: 5.5in 8.5in;
            margin-top: -10px;

        }
    </style>
</head>

<body>

    <div style="width: 100%; margin-left: 5px; margin-top: -86px;">
        <img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
    </div>

    <div style="top:199;font-family:Arial; font-size:12px; margin-left:306px">
        <p style="font-family:Arial; font-size:12px;color:#000000">
            Tempat / Tanggal / Bulan / Tahun
        </p>
    </div>
    <div class="pos" id="_450:199" style="top:199; margin-left:306px; font-family:Arial; font-size:12px;">
        <p style=" font-family:Arial;font-size:12px; color:#000000">
            Kepada</p>
    </div>
    <table cellpadding="17" cellspacing="1">
        <tbody>
            <tr><br>
                <td align="normal" valign="top" width="300">
                    <div class="pos" id="_118:320" style="top:320;left:100">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">

                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:100">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:35px; color:#000000">
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:100">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:9; color:#000000">
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:100">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                        </span>
                        <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:41; color:#000000">
                        </span>
                        <div style="width: 70%; font-family: Arial; font-size: 12px; margin-left: 11px;">
                        </div>
                    </div>
                </td>

                <td align="normal" valign="top" width="270; font-family:Arial; margin-left:19px; font-size:12px;">Yth.&nbsp;
                        <div class="pos" id="_118:320" style="top:320;left:200;font-family:Arial; font-size:12px;">
                        <?= $RoleId_To; ?>
                        </div>
                    <div class="pos" id="_118:320" style="top:320; margin-top:-20; margin-left:22;font-family:Arial;font-size:12px; ">
                        <br><br>&nbsp;&nbsp;di<br>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;margin-left:35; margin-right:15px; font-family:Arial;font-size:12px;">
                        <?= $Alamat; ?>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>

    <br><br>
    <table style="font-family: Arial; font-size: 12px;">
        <tbody>
            <tr><br>
                <td align="center" valign="top" width="300">
                    <div class="pos" id="_118:320" style="top:320; left:100">
                        <span id="_16.3" style=" font-family:Arial; line-height: 17px; font-size:12px; color:#000000">
                            SURAT EDARAN
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320; left:100">
                        <span id="_16.3" style=" font-family:Arial; line-height: 17px; font-size:12px; color:#000000">
                            NOMOR : ... / <?= $ClCode; ?>/<?= $RoleCode; ?>
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320; left:100">
                        <span id="_16.3" style=" font-family:Arial; line-height: 17px; font-size:12px; color:#000000">
                            TENTANG
                        </span>
                    </div>
                    <div class="pos" id="_118:320" style="top:320;left:100">
                        <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
                            <?= $Hal; ?>
                        </span>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    <p style="margin-right:-5px ;margin-left:10; line-height: 17px; font-family: Arial; font-size: 12px; text-align: justify;">
        <?= $Konten; ?>
    </p>

    <br><br><br>
   <div style="width: 55%; font-family: Arial; font-size: 12px; margin-left: 281px; margin-top:-40px; ">
        <?php
        $r_atasan =  $this->session->userdata('roleatasan');
        $r_biro =  $this->session->userdata('groleid');
        $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
        ?>
        <?php if ($TtdText == 'PLT') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } elseif ($TtdText == 'PLH') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                <br><?= get_data_people('RoleName', $Approve_People) ?>,
            </p>
        <?php } else { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } ?>

        <p align="center">PEMERIKSA</p>

        <table style='border: 1; font-family: Arial; font-size: 10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 29px;' cellspacing="0">

            <tr>
                <td rowspan="5"> <img src="<?php echo base_url(); ?>/uploads/kosong.jpg" widht="20" height="50"></td>
                <td>Ditandatangani secara elekronik oleh:</td>
            </tr>
            <tr>
                <td style="overflow: hidden; width: 200px;"><?= get_data_people('RoleName', $Approve_People) ?>,</td>
            </tr>

            <tr>
                <td><br></td>
                <td><br></td>
                <td><br></td>
            </tr>

            <tr>
                <td>
                    <p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
                </td>
            </tr>
            <tr>
                <td><?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat; ?>
                </td>
            </tr>

        </table>
        <br>
    </div>

    <br>
   <!--  <div id="footer" style="width: 100%; font-size: 10px; text-align: center;">
        Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
        <br>
        Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
    </div> -->
    <br>

    <table width="100%" style="font-family: Arial; margin-right:50px;  font-size: 12px; line-height: 15px;">
        <tr>
            <td width="50%" valign="top">
                <?php
                if (!empty($RoleId_Cc)) {
                    $hitung = count($RoleId_Cc);
                    // echo $hitung;
                    $n = 1;
                    $y = 'Yth.';
                    echo "Tembusan : <br>";
                    foreach ($RoleId_Cc as $k => $v) {
                        $xx1 =  $this->db->query("SELECT PeoplePosition FROM v_login WHERE RoleId = '" . $v . "'")->row()->PeoplePosition;
                        $uc  = ucwords(strtolower($xx1));
                        $str = str_replace('Dan', 'dan', $uc);
                        $str = str_replace('Uptd', 'UPTD', $uc);
                        if ($n < $hitung) {
                            echo '<table border="0" width="80%" cellspacing = "0">
                               <tr>
                                  <td style="width: 0px;" valign="top" >' . $n . '.' . $y.  '</td>
                                  <td style="text-align: justify; width:400;" valign="top">' . $str . ';</td>
                               </tr>
                             </table>';
                        } else {
                            echo
                            '<table border="0" width="80%">
                             <tr>
                                <td style="width:0px;"valign="top">' . $n . '.'  . $y. '</td>
                                <td style="text-align: justify; width: 400;"valign="top">' . $str . '.</td>
                             </tr>
                           </table>';
                        }
                        // echo $n.'.'.$y.  $xx1 .'<br>';
                        $n++;
                    }
                }
                ?>
            </td>
        </tr>
    </table>
    <br>
    <page_footer>
        <table style="font-family: Arial; font-size: 10px; line-height: 15px;" align="center">
            <tr>
                <td align="center">
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </td>
            </tr>
        </table>
    </page_footer>
</body>

</html>