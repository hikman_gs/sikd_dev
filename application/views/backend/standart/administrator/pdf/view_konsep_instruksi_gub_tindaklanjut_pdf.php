<?php
$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = 'uk.1' ")->row()->Header;
$sql_gub= $this->db->query("SELECT PeopleName FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeopleName;
$sql_namagub= $this->db->query("SELECT PeoplePosition FROM people WHERE primaryroleid = 'uk.1' ")->row()->PeoplePosition;
?>

<style type="text/css">
    table.page_header {width: 1020px; border: none; background-color: #DDDDFF; border-bottom: solid 1mm #AAAADD; padding: 2mm }
    table.page_footer {width: 1020px; border: none; margin-left:60px; border-top: white 1mm #; padding: 0mm}

</style>


<page backtop="-5mm" backbottom="14mm" backleft="1mm" backright="4mm">
    <!-- <page_header> -->
    <!-- Setting Header -->

    <!-- </page_header> -->
    <!-- Setting Footer -->

  <page_footer>
    <table class="page_footer">
        <tr>
         <!--  <td style="width: 5%; text-align: right">
                Halaman [[page_cu]]/[[page_nb]]
        </td> -->

      <tr>
                
            </tr>
            <tr>
                <td  style="font-family: Arial; margin-left: 90px; font-size: 10px;" align="center">
                
                    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
                    <br>
                    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
                </td>
            </tr>
</tr>
</table>
</page_footer>
   
<!-- Setting CSS Tabel data yang akan ditampilkan -->
<style type="text/css">
    .tabel2 {
        border-collapse: collapse;
    }
    .tabel2 th, .tabel2 td {
        padding: 5px 5px;
        border: 1px solid #000;
    }
</style>


 <div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -110px;">
        <img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
    </div>  

    <table style="margin: auto; margin-top: 6px ; margin-left:15px; font-family: Arial; font-size: 12px; line-height: 20px;">
        <tr>
            <td style="text-align: center;">INSTRUKSI GUBERNUR JAWA BARAT</td>
        </tr><br>

        <tr>
            <td style="text-align: center;">NOMOR : <?= $nosurat ?></td>

        </tr>

        <tr>
            <td style="text-align: center;">TENTANG</td>
        </tr>
        <tr>
            <td style="text-align: center; "><?= $Hal; ?></td>
        </tr>



        <tr>

            <td style="text-align: center; "><br>GUBERNUR JAWA BARAT</td>
        </tr>

        
            
    </table>

<p style="margin-right:-30px ;margin-left:5; margin-top: 10px;  line-height: 17px; font-family: Arial; font-size: 12px; text-align: justify;">
    <p class="indent"><?= $Hal_pengantar; ?></p></p>

    <table style="font-family: Arial; font-size: 12px; line-height: 20px;">
        <tr style="overflow: hidden;" valign="top">
            <td style="text-align: justify; width: 150px;"> &nbsp;Dengan ini meninstruksikan</td>
            <td style="text-align: justify; width: 5px;">:</td>
            <td style="text-align: justify; width: 380;">
    <!--  -->

        </td>
        </tr>

        
    </table>

    <!-- BARIS INI -->
    <table style="font-family: Arial; font-size: 12px; line-height: 20px; width: 400;">
        <tr>
            <td style="text-align: justify; width: 100px;" rowspan="1000" valign="top"> &nbsp;Kepada</td>
            <td style="text-align: justify; width: 5px;" rowspan="1000" valign="top">:</td>
        </tr>
    </table>

   


    <?php

    if (!empty($RoleId_To)) {
    $RoleId_To_decode = json_decode($RoleId_To);
    $jum_lamp = count($RoleId_To_decode);
    $no_lamp = 1;

    foreach ($RoleId_To_decode as $lamp=> $v){

 

                // $xx1 =  $this->db->query("SELECT PeopleName FROM people WHERE PeopleId = '" . $v . "'")->row()->PeopleName;
            
    
            echo '<table style="font-family: Arial; font-size: 12px; line-height:10px; width: 30;">
            <tr>
            <td style="text-align: justify; width: 100px;"  rowspan="1000" valign="top"></td>
            <td style="text-align: justify; width: 5px;" rowspan="1000" valign="top"></td>

            <tr style="text-align: justify; width: 10; line-height: 15px;">' .
                '<td style="text-align: justify;" valign="top">' . $no_lamp . '. ' . '</td>' .
                '<td style="text-align: justify; width: 475px;" valign="top">' .$v . '</td>' .
                
                
                '</tr>' .

                
                '</tr></table>';

            $no_lamp++;
        }
    } 
    ?>

    <!-- Sampai Sini -->



    

    <!-- Sampai Sini -->

    <br>
    <table>

        <tr>
            <td style="text-align: justify; width: 100px;" rowspan="5" valign="top"> &nbsp;Untuk</td>
            <td style="text-align: justify; width: 5px;" rowspan="5" valign="top">:</td>
            
        </tr>

    </table><BR>
<?php
 

if (!empty($Konten)) {
    $Konten_decode = json_decode($Konten);
    $jum_lamp = count($Konten_decode);
    $no_lamp = 1;

    foreach ($Konten_decode as $lamp){
    ?>

<table width="640" cellpadding="1" cellspacing="0">
    <col width="97"/>

    <col width="5"/>

    <col width="480" align="justify"/>

    <tr valign="top">
        <td width="3" style="border: none; padding: 0in"><p align="justify">
            <font face="Arial, serif"><font size="3" style="font-size: 9pt">

                   <?php
                                 
                             
                                        switch ($no_lamp){
                                    case 1: 
                                        $lamp_romawi = "KESATU";
                                        break;
                                    case 2:
                                        $lamp_romawi =  "KEDUA";
                                        break;
                                    case 3:
                                        $lamp_romawi =  "KETIGA";
                                        break;
                                    case 4:
                                        $lamp_romawi =  "KEEMPAT";
                                        break;
                                    case 5:
                                        $lamp_romawi =  "KELIMA";
                                        break;
                                    case 6:
                                        $lamp_romawi =  "KEENAM";
                                        break;
                                    case 7:
                                        $lamp_romawi =  "KETUJUH";
                                        break;
                                  
                                    case 8:
                                        return "KEDELAPAN";
                                        break;
                                    }
                                    echo $lamp_romawi;
                            

                                ?>


                            </font></font></p>
        </td>
        <td width="1" style="border: none; padding: 0in"><p align="justify">
            <font face="Arial, serif"><font size="3" style="font-size: 9pt">:</font></font></p>
        </td>
        <td width="6" style="border: none; padding: 0in"><p  align="justify" style="margin-bottom: 0in  ">
            
                
                   <?php
                 
                   
                           echo $lamp."<br>";
                           ?>

        </p>
        
        </td>
    </tr>
</table>

<?php
$no_lamp++;
}
}
?>

<p style="margin-right:-5px ;margin-left:6; margin-top: 10px;  line-height: 17px; font-family: Arial; font-size: 12px; text-align: justify;">
    <p class="indent">Instruksi ini mulai berlaku pada tanggal ditetapkan.</p></p>

   <div style="width: 50%; font-family: Arial; font-size: 12px; margin-left: 250px; margin-top: 3px;">
        <p style='margin-top:1.2pt;margin-right:40.65pt;margin-bottom:.0001pt;margin-left:40pt;line-height:normal;font-size:12px;;text-align:center;'>
            <span style='font-size:12px;'>Ditetapkan di ..............
            </span>
        </p>
        <p style='margin-top:1.2pt;margin-right:40.65pt;margin-bottom:.0001pt;margin-left:40pt;line-height:normal;font-size:12px;;text-align:center;'>
            <span style='font-size:12px;'>Pada tanggal ..................
            </span>
        </p>

        <?php if ($TtdText == 'PLT') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName', $Approve_People) ?></p>
        <?php } elseif ($TtdText == 'PLH') { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName', $Approve_People) ?></p>
        <?php } else { ?>
            <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= $sql_namagub ?></p>
        <?php } ?>

        <p align="center">PEMERIKSA</p>

        <table style='border: 1; font-family: Arial; font-size: 10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 15px;' cellspacing="0">
            <tr>
                <td rowspan="5"><br> <img src="<?php echo base_url(); ?>/uploads/kosong.jpg" widht="55" height="60"><br><br></td>
                <td>Ditandatangani secara elekronik oleh:</td>
            </tr>
            <tr>
                <td style="overflow: hidden; width: 200px;"><?= $sql_namagub ?>,</td>
            </tr>

        <tr>
            <td><br></td>
            <td><br></td>
            <td><br></td>
        </tr>

        <tr>
            <td><p style="float: right!important;"><?=  $sql_gub?></p></td>
        </tr>
        <tr>
            <td>  <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = 'uk.1' AND GroupId IN ('3','4') ")->row()->Pangkat; ?>
        </td>
    </tr>

        </table>
    </div>
</page>