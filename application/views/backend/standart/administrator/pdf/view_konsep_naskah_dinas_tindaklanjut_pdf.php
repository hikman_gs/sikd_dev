<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <style>
    p.indent {
      padding-left: 6.8em
    }

    h1 {
      margin: 0;
      padding: 20px 0 20px 0;
      background-color: #77D312;
      color: #fff;
      font-size: 1.7em;
      font-weight: 700;
      text-align: center;
    }

    .container {
      margin-bottom: 30px;
      padding: 0 20px 20px 20px;
      width: 700px;
      padding-bottom: 7px;
    }

    span {
      display: block;
      padding: 10px;
    }

    #footer2 {
      position: fixed;
      right: 0px;
      bottom: 10px;
      text-align: center;
      border-top: 1px solid black;
    }

    #footer {
      position: absolute;
      bottom: 0;
      width: 100%;
      height: 60px;
      /* tinggi dari footer */
      vertical-align: bottom;
    }

    @page {
      size: 5.5in 8.5in;
      margin-top: -10px;

    }

    .configlampiran {
      size: F4;
      margin-top: -10px;


    }




    footer {
      margin-top: 20px;
      color: #999999;
      text-align: center;
    }
  </style>
</head>

<body>
  <?php

  $sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '" . $this->session->userdata('roleid') . "'")->row()->Header;

  ?>
  <div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -86px;">
    <img style="width: 100%" src="<?= base_url('FilesUploaded/kop/' . $sql_header); ?>">
  </div>
  <div class="pos" id="_450:199" style="top:199;font-family:Arial; font-size:12px; margin-left:358">
    <p style=" id=" _15.8" font-family:Arial; font-size:12px;color:#000000">
      Tempat / Tanggal / Bulan / Tahun </p>
  </div>
  <div class="pos" id="_450:199" style="top:199; margin-left:359;font-family:Arial; font-size:12px;">
    <p style=" font-family:Arial; id=" _15.8" font-size:12px; color:#000000">
      Kepada 2</p>
  </div>
  <table cellpadding="17" cellspacing="1">
    <tbody>
      <tr><br>
        <td align="normal" valign="top" width="352">
          <div class="pos" id="_118:320" style="top:320;left:115">
            <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
              Nomor &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;&nbsp; <?= set_value('id') ?> / <?= $ClCode; ?>/<?= set_value('up') ?>
            </span>
          </div>
          <div class="pos" id="_118:320" style="top:320;left:115">
            <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
              Sifat
            </span>
            <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:35px; color:#000000">
              :&nbsp;&nbsp;&nbsp;&nbsp;<?= $SifatId; ?>
            </span>
          </div>
          <div class="pos" id="_118:320" style="top:320;left:115">
            <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
              Lampiran
            </span>
            <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:9; color:#000000">
              :&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $Jumlah . ' ' . $MeasureUnitId; ?>
            </span>
          </div>
          <div class="pos" id="_118:320" style="top:320;left:115; text-align: justify">
            <span id="_16.3" style=" font-family:Arial; font-size:12px; color:#000000">
              Hal
            </span>
            <span id="_16.3" style=" font-family:Arial; font-size:12px;margin-left:41; color:#000000">
              :

            </span>
            <div style="width: 70%; font-family: Arial; font-size: 12px; margin-left: 11px;">

              <?= $Hal; ?>
            </div>
          </div>
        </td>

        <td align="normal" valign="top" width="180;font-family:Arial; font-size:12px;">Yth.&nbsp;
          <div class="pos" id="_118:320" style="top:320;left:200;font-family:Arial; font-size:12px;">
            <?= $RoleId_To; ?>
          </div>
          <div class="pos" id="_118:320" style="top:320;margin-left:22;font-family:Arial;font-size:12px; ">di
          </div>
          <div class="pos" id="_118:320" style="top:320;margin-left:35; margin-right:15px; font-family:Arial;font-size:12px;"><?= $Alamat; ?>
          </div>
        </td>
      </tr>
    </tbody>
  </table>
  <br><br>


  <p style="margin-right:-5px ;margin-left:72; line-height: 17px; font-family: Arial; font-size: 12px; text-align: justify;">
  <p class="indent"><?= $Konten; ?></p>
  </p>

  <br><br><br>
  <div style="width: 50%; font-family: Arial; font-size: 12px; margin-left: 300px; margin-top:-40px; ">
    <?php
    $r_atasan =  $this->session->userdata('roleatasan');
    $r_biro =  $this->session->userdata('groleid');
    $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
    ?>
    <?php if ($TtdText == 'PLT') { ?>
      <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
    <?php } elseif ($TtdText == 'PLH') { ?>
      <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
    <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
      <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
        <br><?= get_data_people('RoleName', $Approve_People) ?>,
      </p>
    <?php } else { ?>
      <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName', $Approve_People) ?>,</p>
    <?php } ?>





    <p align="center">PEMERIKSA</p>

    <table style='border: 1; font-family: Arial; font-size: 10px; table-layout: fixed; overflow: hidden; width: 300; height: 200; margin-left: 25px;' cellspacing="0">

      <tr>
        <td rowspan="5"> <img src="<?php echo 'uploads/kosong.jpg" widht="20" height="50"'; ?>"></td>
        <td>Ditandatangani secara elekronik oleh:</td>
      </tr>
      <tr>
        <td style="overflow: hidden; width: 200px;"><?= get_data_people('RoleName', $Approve_People) ?>,</td>
      </tr>

      <tr>
        <td><br></td>
        <td><br></td>
        <td><br></td>
      </tr>

      <tr>
        <td>
          <p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
        </td>
      </tr>
      <tr>
        <td> <?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat; ?>
        </td>
      </tr>

    </table>
    <br>
  </div>

  <br>
  <br>

  <table width="100%" style="font-family: Arial; font-size: 12px; line-height: 15px;">
    <tr>
      <td width="10%" valign="top">
        <?php
        $exp = explode(',', $RoleId_Cc);
        $z = count($exp);
        if ($z > 1) {
          $n = 1;
          $y = 'Yth. &nbsp;';
          echo "Tembusan : <br>";
          foreach ($exp as $k => $v) {
            $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $v . "'")->row()->RoleName;
            echo $n . '. ' . $y . $xx1 . '<br>';
            $n++;
          }
        } else {
          if (!empty($RoleId_Cc)) {
            if ($RoleId_Cc != '') {
              echo "Tembusan : <br>";
            }
            $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '" . $exp[0] . "'")->row()->RoleName;

            echo $xx1;
          }
        }
        ?>
      </td>
    </tr>
  </table>
  <br><br>

  <div id="footer" style="width: 100%; font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
    Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
    <br>
    Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
  </div>
  <br>
  <?php
  if ($lampiran >= '0') { ?>
    <BR>
    <page backtop="-8mm" backbottom="17mm" backleft="10mm" backright="5mm">
      <page_header>
        <div style="width: 100%; font-family: Arial; color:purple;  font-weight: solid #e3e3e3; ; font-size: 10px; margin-left: 5px; text-align: center; margin-top: -70px;">
          - Lampiran - <p class="page">. </p>
        </div><br><br>
      </page_header>

      <page_footer>
        <div style="width: 100%; font-family: Arial; font-size: 9px; margin-left: 5px; text-align: center;">
          Dokumen ini telah ditandatangani secara elektronik menggunakan sertifikat elektronik yang diterbitkan oleh
          <br>
          Balai Sertifikasi Elektronik (BSrE) Badan Siber dan Sandi Negara
        </div>
      </page_footer>

      <br />

      <table style="height: 300px; width: 400%; font-family: Arial; margin-left:200px; font-size: 10px; line-height: 15px;  align-content : right;">
        <tbody>
          <tr>
            <td style="width: 74px; vertical-align: top;">LAMPIRAN</td>
            <td style="width: 256.426px; vertical-align: top;" colspan="4"><?php
                                                                            $r_atasan =  $this->session->userdata('roleatasan');
                                                                            $r_biro =  $this->session->userdata('groleid');
                                                                            $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
                                                                            ?>
              <?php if ($TtdText == 'PLT') { ?>
                Plt. <?= get_data_people('RoleName', $Approve_People) ?>,
              <?php } elseif ($TtdText == 'PLH') { ?>
                Plh. <?= get_data_people('RoleName', $Approve_People) ?>,
              <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
                a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>,
                <br><?= get_data_people('RoleName', $Approve_People) ?>,
              <?php } else { ?>
                <?= get_data_people('RoleName', $Approve_People) ?>,
              <?php } ?>
            </td>
          </tr>
          <tr>
            <td style="width: 74px;">&nbsp;</td>
            <td style="width: 68px; vertical-align: top;">NOMOR</td>
            <td style="width: 8px; vertical-align: top;">: </td>
            <td style="width: -5px; vertical-align: top;">... </td>
            <td style="width: 90px; vertical-align: top;"><?= $ClCode; ?></td>
            <td style="width: 180.426px; vertical-align: top;" colspan="2">&nbsp;</td>
          </tr>
          <tr>
            <td style="width: 74px;">&nbsp;</td>
            <td style="width: 68px; vertical-align: top;">TANGGAL</td>
            <td style="width: 8px; vertical-align: top;">:</td>
            <td style="width: 180.426px; vertical-align: top;" colspan="2">Tanggal/Bulan/Tahun</td>
          </tr>
          <tr>
            <td style="width: 74px;">&nbsp;</td>
            <td style="width: 68px; vertical-align: top;">PERIHAL</td>
            <td style="width: 8px; vertical-align: top;">:</td>
            <td style="width: 180.426px; vertical-align: top;" colspan="2"> <?= $Hal; ?></td>
          </tr>
        </tbody>
      </table>
      <br><br>

      <p class="configlampiran" style="margin-right:-20px ;margin-left:-30; margin-bottom:35px; line-height: 15px; font-family: Arial; font-size: 12px; text-align: justify;">
      <p><?= $lampiran; ?></p>
      </p>
    </page>
  <?php } else { ?>
  <?php } ?>

  <span>

    <?php
    if ($lampiran >= '0') { ?>
      <div style="width: 60%; font-family: Arial; font-size: 12px;  margin-left: 230px; margin-top:-40px; ">
        <?php
        $r_atasan =  $this->session->userdata('roleatasan');
        $r_biro =  $this->session->userdata('groleid');
        $age = $this->session->userdata('groleid') == 'XxJyPn38Yh.3';
        ?>
        <?php if ($TtdText == 'PLT') { ?>
          <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } elseif ($TtdText == 'PLH') { ?>
          <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } elseif ($TtdText2 == 'Atas_Nama') { ?>
          <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">a.n.&nbsp;<?= get_data_people('RoleName', $Approve_People3) ?>
            <br><?= get_data_people('RoleName', $Approve_People) ?>,
          </p>
        <?php } else { ?>
          <p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName', $Approve_People) ?>,</p>
        <?php } ?>

        <p align="center">PEMERIKSA</p>

        <table style='border: 1; font-family: Arial; font-size: 7px; table-layout: fixed; overflow: hidden; width: 240; height: 200; margin-left: 26px;' cellspacing="0">

          <tr>
            <td rowspan="5"> <img src="<?php echo 'uploads/kosong.jpg" widht="20" height="50"'; ?>"></td>
            <td>Ditandatangani secara elekronik oleh:</td>
          </tr>
          <tr>
            <td style="overflow: hidden; width: 200px;"><?= get_data_people('RoleName', $Approve_People) ?>,</td>
          </tr>

          <tr>
            <td><br></td>
            <td><br></td>
            <td><br></td>
          </tr>

          <tr>
            <td>
              <p style="float: right!important;"><?= $Nama_ttd_konsep ?></p>
            </td>
          </tr>
          <tr>
            <td><?= $tujuan = $this->db->query("SELECT Pangkat FROM people WHERE PrimaryRoleId = '" . $this->session->userdata('primaryroleid') . "' AND GroupId IN ('3','4') ")->row()->Pangkat; ?></td>
          </tr>

        </table>
        <br>
      </div>
    <?php } else { ?>
    <?php } ?>
  </span>



</body>

</html>