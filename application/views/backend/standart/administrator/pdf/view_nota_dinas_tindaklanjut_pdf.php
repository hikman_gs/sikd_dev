<?php

$sql_header = $this->db->query("SELECT Header FROM v_kopnaskah WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Header;

?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>preview</title>
	<style>p.indent{ padding-left: 6.8em }
	</style>
</head>
<body>
<div style="width: 100%; font-family: Arial; font-size: 12px; margin-left: 5px; margin-top: -90px;">
<img style="width: 100%" src="<?= base_url('FilesUploaded/kop/'.$sql_header); ?>"> </div>

<table style="margin: auto; font-family: Arial; font-size: 12px; line-height: 20px;">
	<tr><td style="text-align: center;">NOTA DINAS</td></tr>
</table>
<br>
<br>
<table style="margin-left:35; width:98%; margin-right:40; font-family: Arial; font-size: 12px; line-height: 40px;">  

        <tr>
		<td style="line-height: 15px;vertical-align:top"> <p style="margin-right:40;margin-left:1"> Kepada </p></td>
		<td style="line-height: 15px;vertical-align:top"><p style="margin-right:40;margin-left:40">:</p></td>
		<td style="line-height: 15px;vertical-align:bottom">
		    <p style="margin-right:40;margin-left:8">
	            	<?php
			    	if(count($RoleId_To) > 1) {
					$n = 1;
					foreach ($RoleId_To as $k => $v) {
						$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
						echo $n.'. '.$xx1.'<br>';
						$n++;
					}
				}else{
					$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$RoleId_To[0]."'")->row()->RoleName;
					echo $xx1;
				}
			?>
	           
            </p>
             </td>
	
	</tr>





<tr>
		<td style="line-height: 15px;vertical-align:top"> <p style="margin-right:40; margin-left:1; line-height: 5x">
	       Dari
            </p></td>
		<td style="line-height: 15px;vertical-align:top"> <p style="margin-left:40px;">:
            </p></td>
	
       
        <td style="line-height: 15px;vertical-align:bottom">
	  	 <p style="margin-right:50px; margin-left:8; line-height: 15px">
	         <?= get_data_people('RoleName',$Approve_People)
	        ?>
	           
            </p>
             </td>
	
	</tr>
	
	
	  <tr>
		<td style="line-height: 15px;vertical-align:top"><p style="margin-right:40; margin-left:1; line-height: 5x">
	       Tembusan
            </p></td>
		<td style="line-height: 15px;vertical-align:top"><p style="margin-left:40px;">:
            </p></td>
		<td style="line-height: 15px;vertical-align:bottom">
	
	  	 <p style="margin-right:50px; margin-left:8; line-height: 15px">
	          <?php
				if(!empty($RoleId_Cc)) {
					if(count($RoleId_Cc) > 1) {
						$n = 1;
						foreach ($RoleId_Cc as $k => $v) {
							$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
							echo $n.'. '.$xx1.'<br>';
							$n++;
						}
					}else{
						
						$xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$RoleId_Cc[0]."'")->row()->RoleName;
						echo $xx1;
					}
				}
		        ?>
	           
            </p>
             </td>
	
	</tr>
	
	
	<tr>
	    <td>
	        <p style="margin-right:40; margin-left:1; line-height: 5px">
	       Nomor
            </p>
        </td>
        <td>
            <p style="margin-left:40px;">:
            </p>
        </td>
        <td>
	  	 <p style="margin-right:40; margin-left:8; line-height: 17px">
	         .........../<?= $ClCode; ?>/...........
	           
            </p>
             </td>
	
	</tr>
	
	
	
	
	<tr>
		<td style="line-height: 15px;"><p style="margin-right:40; margin-left:1; line-height: 5px">
	       Tanggal
            </p></td>
		<td style="line-height: 15px;"><p style="margin-right:40; margin-left:1; line-height: 5px; margin-left:40px;">
	       :
            </p></td>
		<td style="line-height: 15px;">
		    <p style="margin-right:40;margin-left:8">
		        <?= get_bulan($TglReg) ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p></td>
	         
	</tr>       
	
	<tr>
		<td style="line-height: 15px;"><p style="margin-right:40; margin-left:1; line-height: 15px">
	       Sifat
            </p></td>
		<td style="line-height: 15px;"><p style="margin-right:40; margin-left:40px; line-height: 5px">
	       :
            </p></td>
		<td style="line-height: 15px;">
		    <p style="margin-right:40;margin-left:8">
		        <?= $SifatId; ?>
		    </p>
		    </td>
	</tr>
	<tr>
	  <?php
		if($Jumlah != 0) {
	  ?>
		<td style="line-height: 15px;"><p style="margin-right:40;margin-left:1">Lampiran</p></td>
		<td style="line-height: 15px;"><p style="margin-right:40; margin-left:1; line-height: 5px; margin-left:40px;">
	       :
            </p></td>
		<td style="line-height: 15px;">
			<p style="margin-right:40;margin-left:8">
			    <?php 
				    echo $Jumlah.' '.$MeasureUnitId;
			    ?>
			</p>
		</td>
	  <?php } ?>	
	</tr>	
	<tr>
		<td style="line-height: 15px;"><p style="margin-right:40;margin-left:1">Hal</p></td>
		<td style="line-height: 15px;"><p style="margin-right:40; margin-left:1; line-height: 5px; margin-left:40px;">
	       :
            </p></td>
		<td style="line-height: 15px;">
		    <p style="margin-right:40;margin-left:8">
		        <?= $Hal; ?>
		        </p>
		        </td>
	</tr>
	
</table>
       <p style="margin-right:1;margin-left:35">
    <hr>
    </p>
 <p style="margin-right:-4px ;margin-left:35;line-height: 15px; margin-top:-12px; font-family: Arial; font-size: 12px; text-align: justify;">
               <p class="indent"><?= $Konten; ?></p>
                 </p>

<br>

<div style="width: 50%; font-family: Arial; font-size: 12px; margin-left: 300px; margin-top:-20px; ">

	<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"></p>

	<?php if($TtdText == 'PLT') { ?>
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plt. <?= get_data_people('RoleName',$Approve_People) ?></p>
	<?php } elseif($TtdText == 'PLH') { ?>
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;">Plh. <?= get_data_people('RoleName',$Approve_People) ?></p>
	<?php } else { ?>
		<p style="float: right!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;"><?= get_data_people('RoleName',$Approve_People) ?></p>
	<?php } ?>	

	<br><br><br><br>
	<p style="float: right!important; text-align: center; font-family: Arial; font-size: 12px;"><?= $Nama_ttd_konsep ?></p>

</div>
</body>
</html>