<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<!-- File untuk konfigurasi fine-upload -->
<?php $this->load->view('core_template/fine_upload'); ?>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<style>
    .select2-container--default .select2-selection--single{border:0px;}
    .select2-container{
            border: 1px solid #DEDCDC!important;
            border-radius: 5px!important;
            padding: 2px 0px!important;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">Pencatatan Naskah Tanpa Tindaklanjut</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pencatatan Naskah Tanpa Tindaklanjut</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>
                        <?= form_open_multipart(BASE_URL('administrator/anri_reg_naskah_ttl/naskah_masuk_add_save'), ['name'    => 'form_inbox', 'class'   => 'form-horizontal', 'id'      => 'form_inbox', 'method'  => 'POST']); ?>
                        <div class="form-group ">
                            <label for="NTglReg" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right" disabled name="NTglReg" id="NTglReg" value="<?= date('d-m-Y') ?>">
                                </div>
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Naskah<sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-4">
                                    <select name="JenisId" class="form-control select2" required>
                                        <option value="">-- Pilih --</option>
                                        <?php
                                            $query = $this->db->query("SELECT * FROM master_jnaskah ORDER BY JenisName ASC")->result();
                                            foreach ($query as $row) {
                                        ?>
                                            <option value="<?= $row->JenisId; ?>"><?= $row->JenisName; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Tanggal Naskah<sup class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right datepicker" required name="Tgl" id="Tgl" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Nomor" class="col-sm-2 control-label">Nomor Naskah
                            <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Nomor" id="Nomor" placeholder="Nomor Naskah" value="<?= set_value('Nomor'); ?>" required autofocus>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Hal" class="col-sm-2 control-label">Uraian Informasi <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Hal" id="Hal" required placeholder="Uraian Informasi" value="<?= set_value('Hal'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Namapengirim" class="col-sm-2 control-label">Asal Naskah  <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Instansipengirim" required id="Instansipengirim" placeholder="Asal Naskah" value="<?= set_value('Instansipengirim'); ?>">
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="UrgensiId" class="col-sm-2 control-label">Tingkat Urgensi <sup class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <select name="UrgensiId" class="form-control select2" id="UrgensiId" required>
                                    <option value="">-- Pilih --</option>
                                    <?php
                                        $urg = $this->db->query("SELECT * FROM master_urgensi ORDER BY UrgensiName ASC")->result();
                                        foreach ($urg as $data_urg){
                                    ?>
                                        <option value="<?= $data_urg->UrgensiId; ?>"><?= $data_urg->UrgensiName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="SifatId" class="col-sm-2 control-label">Sifat Naskah <sup class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <select name="SifatId" class="form-control select2" required id="SifatId">
                                    <option value="">-- Pilih --</option>
                                    <?php
                                        $sifat = $this->db->query("SELECT * FROM master_sifat ORDER BY SifatName ASC")->result();
                                        foreach ($sifat as $data_sifat){
                                    ?>
                                        <option value="<?= $data_sifat->SifatId; ?>"><?= $data_sifat->SifatName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                        <div class="form-group">
                          <label for="BerkasId" class="col-sm-2 control-label">Pilih Berkas <sup class="text-danger">*</sup></label>
                            <div class="col-md-4">
                              <select name="BerkasId" class="form-control select2" required>
                                  <option value=""> -- Pilih Berkas -- </option>
                                  <?php
                                      $query = $this->db->query("SELECT BerkasId, Klasifikasi, BerkasNumber, BerkasName, YEAR(CreationDate) AS tahun_cipta FROM berkas WHERE RoleId = '".$this->session->userdata('roleid')."' AND BerkasStatus = 'open' ORDER BY Klasifikasi ASC")->result();
                                      foreach ($query as $row) {
                                  ?>
                                      <option value="<?= $row->BerkasId; ?>"><?= $row->Klasifikasi; ?>/<?= $row->BerkasNumber; ?>/<?= $row->tahun_cipta; ?> - <?= $row->BerkasName; ?></option>
                                  <?php
                                      }
                                  ?>
                              </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="title" class="col-sm-2 control-label">Unggah Data</label>
                             <div class="col-sm-8">
                                <div id="test_title_galery"></div>
                                <div id="test_title_galery_listed"></div>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <br>
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <br>
                            <button class="btn btn-danger">Simpan</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>

    $(document).ready(function(){        
    $('.select2').select2();  

    var params = {};
    params[csrf] = token;
    $('#test_title_galery').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/anri_reg_naskah_ttl/upload_naskah_masuk_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/anri_reg_naskah_ttl/delete_naskah_masuk_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                            
          },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('#test_title_galery').fineUploader('getUuid', id);
                   $('#test_title_galery_listed').append('<input type="hidden" class="listed_file_uuid" name="test_title_uuid['+id+']" value="'+uuid+'" /><input type="hidden" class="listed_file_name" name="test_title_name['+id+']" value="'+xhr.uploadName+'" />');
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid['+id+']"]').remove();
                  $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name['+id+']"]').remove();
                }
              }
          }
      }); /*end title galery*/
    }); /*end doc ready*/

</script>