
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
//This page is a result of an autogenerated content made by running test.html with firefox.
function domo(){
 
   // Binding keys
   $('*').bind('keydown', 'Ctrl+e', function assets() {
      $('#btn_edit').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+x', function assets() {
      $('#btn_back').trigger('click');
       return false;
   });
    
}


jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Satuan Unit      <small><?= cclang('detail', ['Satuan Unit']); ?> </small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class=""><a  href="<?= site_url('administrator/master_satuanunit'); ?>">Satuan Unit</a></li>
      <li class="active"><?= cclang('detail'); ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">

               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Satuan Unit</h3>
                     <h5 class="widget-user-desc">Detail Satuan Unit</h5>
                     <hr>
                  </div>

                 
                  <div class="form-horizontal" name="form_master_satuanunit" id="form_master_satuanunit" >
                   
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">MeasureUnitId </label>

                        <div class="col-sm-8">
                           <?= _ent($master_satuanunit->MeasureUnitId); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">MeasureUnitName </label>

                        <div class="col-sm-8">
                           <?= _ent($master_satuanunit->MeasureUnitName); ?>
                        </div>
                    </div>
                                        
                    <br>
                    <br>

                    <div class="view-nav">
                        <?php is_allowed('master_satuanunit_update', function() use ($master_satuanunit){?>
                        <a class="btn btn-flat btn-info btn_edit btn_action" id="btn_edit" data-stype='back' title="edit master_satuanunit (Ctrl+e)" href="<?= site_url('administrator/master_satuanunit/edit/'.$master_satuanunit->MeasureUnitId); ?>"><i class="fa fa-edit" ></i> <?= cclang('update', ['Master Satuanunit']); ?> </a>
                        <?php }) ?>
                        <a class="btn btn-flat btn-default btn_action" id="btn_back" title="back (Ctrl+x)" href="<?= site_url('administrator/master_satuanunit/'); ?>"><i class="fa fa-undo" ></i> <?= cclang('go_list_button', ['Master Satuanunit']); ?></a>
                     </div>
                    
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->

      </div>
   </div>
</section>
<!-- /.content -->
