<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>

<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<link href="<?= BASE_ASSET; ?>/select3/select3.min.css" rel="stylesheet" />
<link href="<?= BASE_ASSET; ?>/select4/select4.min.css" rel="stylesheet" />
<style>

    .select2-container--default,.select2-selection--single{border:1px solid #fff!important;width: 100%!important;}
    .select2-container--default .select2-selection--multiple{border:1px solid #fff!important;}
    .select2-container{
            border: 1px solid #DEDCDC!important;
            border-radius: 5px!important;
            padding: 2px 0px!important;
    }

#k_teruskan .modal-content{
    /* new custom width */
    width: 520px;
}

.chosen-container-single,.chosen-container-multi{width: 100%!important;}


</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active"><?= $title; ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" > 
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Daftar Pencatatan Naskah Untuk DiKoreksi</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
                  
                  <div class="table-responsive"> 
                    
                  <table id="myTable" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%"> 

                     <thead>
                       <tr>
                           <th>No</th>
                           <th>Jenis Surat</th>
                           <th>Tanggal</th>
                           <th width="20%">Hal</th>
                           <th width="30%">Tujuan Naskah</th>
                           <th>Pesan Koreksi</th>
                           <th>Aksi</th>
                      </tr>
                     </thead>

                     <tbody></tbody>

                  </table>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>

   <div id="k_teruskan" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Teruskan Untuk Ditandatangani</b></h4>
                </div>
                <form action="<?= base_url('administrator/Anri_list_naskah_koreksi/k_teruskan'); ?>" class="form-horizontal" method="POST">

                    <div class="modal-body">
                        <div class="content">

                          <div class="form-group">
                            <input type="hidden" id="NId" name="NId">
                            <input type="hidden" id="GIR_Id" name="GIR_Id">

                            <div class="col-6">

                              <label>Penandatangan <sup class="text-danger">*</sup></label>
                              <select name="TtdText" id="TtdText" class="form-control select2 TtdText" required>
                                  <?php foreach ($this->db->query('SELECT * FROM master_ttd')->result() as $data1) { ?>
                                  <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                                  <?php } ?>
                              </select>
                              
                              <?php
                                  $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND GroupId NOT IN ('8') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='".$this->session->userdata('roleatasan')."'")->result();
                                  foreach ($AtasanId as $dataz) {
                                    $xx1 = $dataz->PeopleId;
                                  }
                              ?>
                              <!-- tambahan dispu -->
                                <?php
                                     $search = array('4');
                                   $SekdisId= $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.RoleAtasan = '".$this->session->userdata('roleatasan')."' AND p.GroupId = '4' ORDER BY p.PrimaryRoleId ASC")->result();
                                      
                                foreach ($SekdisId as $dataz) {
                                    $xx2 = $dataz->PeopleId;
                                  }
                              ?>
                               <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $xx1; ?>">  
                              <input type="hidden" id="tmpid_sekdis" name="tmpid_sekdis" value="<?= $xx2; ?>">   
                            </div>                              
                          </div>

                          <div class="form-group"> 
                            <div class="col-6 Approve_People hidden">
                                <select name="Approve_People" id="Approve_People" class="form-control select2">
                                    <!-- Perbaikan Eko 09-11-2019 -->
                                    <?php
                                          $query = $this->db->query("SELECT PeopleId, PeoplePosition, PeopleName FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                                          foreach ($query as $dat_people) {
                                      ?>
                                    <option value="<?= $dat_people->PeopleId; ?>">
                                        <?= $dat_people->PeoplePosition; ?>--<?= $dat_people->PeopleName; ?></option>
                                    <?php
                                          }
                                      ?>
                                    <!-- batas akhir perbaikan -->
                                </select>
                            </div> 
                          </div>      

                          <div class="form-group">
                              <div class="col-6">
                                <label>Pesan <sup class="text-danger">*</sup></label>
                                  <input type="text" class="form-control" name="pesan" id="pesan" required>
                              </div>   
                          </div>   

                          <div class="form-group">
                              <div class="col-6"></div>   
                          </div>  

                          <div class="form-group">
                              <div class="col-6">
                                  <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Simpan dan Kirim </button>
                              </div>   
                          </div>   

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>

<!-- /.content -->
<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>
<script src="<?= BASE_ASSET; ?>/select3/select3.min.js"></script>
<script type="text/javascript">



   $(' .TtdText').change(function() {
      if ($(this).find(':selected').val() == 'none') {
          $('.Approve_People').addClass('hidden');
      } else {
          $('.Approve_People').removeClass('hidden');
          r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
          $('#Approve_People').select2();
          $('#Approve_People').attr('disabled', false);
          if (this.value == "AL") {
              idatasan = $("#tmpid_atasan").val();
              $('#Approve_People').val(idatasan).change();
              $('#Approve_People').attr('disabled', true);
          }
           if (this.value == "SEKDIS") {
              iidatasan = $("#tmpid_sekdis").val();
              $('#Approve_People').val(iidatasan).change();
              $('#Approve_People').attr('disabled', true);
          }
      }
  });

    $(' .nomortu').change(function() {
      if ($(this).find(':selected').val() == 'none') {
          $('.Approve_tu').addClass('hidden');
      } else {
          $('.Approve_tu').removeClass('hidden');
          r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
          $('#Approve_tu').select3();
          $('#Approve_tu').attr('disabled', false);
          if (this.value == "UArsip") {
              idatasan = $("#tmpid_tu").val();
              $('#Approve_tu').val(idatasan).change();

              $('#Approve_tu').attr('disabled', true);
          }
      }
  });

 var params = {};
    params[csrf] = token;
    $('.upload_teruskan').fineUploader({
          template: 'qq-template-gallery',
          request: {
              endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
              params : params
          },
          deleteFile: {
              enabled: true, 
              endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
          },
          thumbnails: {
              placeholders: {
                  waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                  notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
              }
          },
          validation: {
              allowedExtensions: ["*"],
              sizeLimit : 0,
                            
          },
          showMessage: function(msg) {
              toastr['error'](msg);
          },
          callbacks: {
              onComplete : function(id, name, xhr) {
                if (xhr.success) {
                   var uuid = $('.upload_teruskan').fineUploader('getUuid', id);
                   $('.upload_teruskan_listed').append('<input type="hidden" class="listed_file_uuid" name="upload_teruskan_uuid['+id+']" value="'+uuid+'" /><input type="hidden" class="listed_file_name" name="upload_teruskan_name['+id+']" value="'+xhr.uploadName+'" />');
                } else {
                   toastr['error'](xhr.error);
                }
              },
              onDeleteComplete : function(id, xhr, isError) {
                if (isError == false) {
                  $('.upload_teruskan_listed').find('.listed_file_uuid[name="upload_teruskan_uuid['+id+']"]').remove();
                  $('.upload_teruskan_listed').find('.listed_file_name[name="upload_teruskan_name['+id+']"]').remove();
                }
              }
          }
      }); /*end title galery*/

  

function ko_verifikasi(NId, GIR_Id) {
    $("#NId_Temp").val(NId);
    $("#GIR_Id").val(GIR_Id);
    $("#approve_naskah").modal('show');
}

function ko_teruskan(NId, GIR_Id) {
    $("#NId").val(NId);
    $("#GIR_Id").val(GIR_Id);
    $("#k_teruskan").modal('show');
}

function ko_teruskan_tu(NId, GIR_Id) {
    $("#NId_Temp4").val(NId);
    $("#GIR_Id4").val(GIR_Id);
    $("#k_teruskan_tu").modal('show');
}

function ko_nomor(NId, GIR_Id) {
    $("#NId_Temp3").val(NId);
    $("#GIR_Id3").val(GIR_Id);
    $("#nomor").modal('show');
}

</script>
<script src="<?= BASE_ASSET; ?>/tinymce/tinymce.min.js"></script>
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
  $(document).ready(function(){
    $('.select2').select2();
  });

  tinymce.init({
    force_br_newlines : false,
    force_p_newlines : false,
    forced_root_block : '',
    selector: '.mytextarea',
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    theme: "modern",
    plugins: [
    "lists spellchecker",
    "code",
    "save table"
    ],
     
    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table",
     
    image_advtab: true,
     
    style_formats: [
    {title: 'Bold text', format: 'h1'},
    {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
    {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
    {title: 'Example 1', inline: 'span', classes: 'example1'},
    {title: 'Example 2', inline: 'span', classes: 'example2'},
    {title: 'Table styles'},
    {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
    ],
     
  });
   $(document).ready(function() {

        var dataTable =  $('#myTable').DataTable( { 

            "processing": true, 
            "serverSide": true, 
            "order": [[2, "desc" ]],
            
            "ajax": {
                "url": "<?php echo site_url('administrator/anri_list_naskah_koreksi/get_data_naskah_bs')?>",
                "type": "POST",
                //deferloading untuk menampung 10 data pertama dulu
                "deferLoading": 10,
            },

            
            "columnDefs": [
            { 
                "targets": [ 0, 6 ], 
                "orderable": false, 
            },
            ],

        });   

    });

</script>
    
   


</script>
<script src="<?= BASE_ASSET; ?>/tinymce/tinymce.min.js"></script>
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
  
