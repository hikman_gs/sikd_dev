<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a  href="<?= site_url('administrator/anri_master_instansi'); ?>">Pengaturan Instansi</a></li>
        <li class="active">Tambah Data</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Tambah Data</h3>
                            <h5 class="widget-user-desc">Instansi</h5>
                            <hr>
                        </div>
                        <?= form_open(BASE_URL('administrator/anri_master_instansi/add_save'), [
                            'name'    => 'form_master_instansi', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_master_instansi', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                        <div class="form-group ">
                            <label for="kode_instansi" class="col-sm-2 control-label">Kode Instansi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="35" name="kode_instansi" id="kode_instansi" placeholder="Kode Instansi" value="<?= set_value('kode_instansi'); ?>" required autofocus>
                                <small class="info help-block">
                                <b>Input Kode Instansi</b> Max Length : 35.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="nama_instansi" class="col-sm-2 control-label">Nama Instansi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="250" name="nama_instansi" id="nama_instansi" placeholder="Nama Instansi" value="<?= set_value('nama_instansi'); ?>" required>
                                <small class="info help-block">
                                <b>Input Nama Instansi</b> Max Length : 250.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="nama_resmi" class="col-sm-2 control-label">Nama Resmi Lainnya
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="250" name="nama_resmi" id="nama_resmi" placeholder="Nama Resmi Lainnya" value="<?= set_value('nama_resmi'); ?>" required>
                                <small class="info help-block">
                                <b>Input Nama Resmi Lainnya</b> Max Length : 250.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="tipe_instansi" class="col-sm-2 control-label">Tipe Instansi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="250" name="tipe_instansi" id="tipe_instansi" placeholder="Tipe Instansi" value="<?= set_value('tipe_instansi'); ?>" required>
                                <small class="info help-block">
                                <b>Input Tipe Instansi</b> Max Length : 250.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="tgl" class="col-sm-2 control-label">Tanggal Keberadaan Instansi 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                            <div class="input-group date col-sm-12">
                              <input type="text" class="form-control pull-right datepicker" name="tgl"  placeholder="Tanggal Keberadaan Instans" id="tgl" required>
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="fungsi" class="col-sm-2 control-label">Fungsi, Jabatan dan Kegiatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <!-- <input type="text" class="form-control" name="fungsi" id="fungsi" placeholder="Fungsi, Jabatan dan Kegiatan" value="<?= set_value('fungsi'); ?>"> -->
                                <textarea name="fungsi" rows="3" class="form-control" required></textarea>
                                <small class="info help-block">
                                <b>Input Fungsi, Jabatan dan Kegiatan</b> Max Length : 250.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="mandat" class="col-sm-2 control-label">Mandat / Sumber Kewenangan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="250" name="mandat" id="mandat" placeholder="Mandat / Sumber Kewenangan" value="<?= set_value('mandat'); ?>" required>
                                <small class="info help-block">
                                <b>Input Mandat / Sumber Kewenangan</b> Max Length : 250.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="status_instansi" class="col-sm-2 control-label">Status Aktif 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="checkbox" name="status_instansi" id="status_instansi" value="Y" required>
                            </div>
                        </div>
                                                 
                        <div class="form-group hidden">
                            <label for="creation_by" class="col-sm-2 control-label">Creation By 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="15" name="creation_by" id="creation_by" placeholder="Creation By" value="<?= set_value('creation_by'); ?>">
                                <small class="info help-block">
                                <b>Input Creation By</b> Max Length : 15.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group hidden">
                            <label for="created_date" class="col-sm-2 control-label">Created Date 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datetimepicker" name="created_date"  id="created_date">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                           <input type="submit" class="btn btn-flat btn-danger" value="Simpan">
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
