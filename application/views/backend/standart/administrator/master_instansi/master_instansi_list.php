<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Instansi</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                        <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="Tambah Data" href="<?=  site_url('administrator/anri_master_instansi/add'); ?>"><i class="fa fa-plus-square-o" ></i> Tambah Data</a>
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Pengaturan Instansi</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <div class="table-responsive"> 
                  <table id="example1" class="table table-bordered table-striped table-hover">
                     <thead>
                        <tr class="">
                           <th>
                            <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                           </th>
                           <th>Nama Instansi</th>
                           <th>Status Instansi</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody id="tbody_master_instansi">
                     <?php 
                     $master_instansis = $this->db->get('master_instansi')->result();
                     foreach($master_instansis as $master_instansi): ?>
                        <tr>
                           <td width="5">
                              <input type="checkbox" class="flat-red check" name="id[]" value="<?= $master_instansi->id; ?>">
                           </td>
                           
                           <td><?= _ent($master_instansi->nama_instansi); ?></td> 
                           <td><?= _ent($master_instansi->status_instansi); ?></td> 
                           <td width="200">
                              <a href="<?= site_url('administrator/anri_master_instansi/update/' . $master_instansi->id); ?>" class="btn btn-sm btn-primary" title="Ubah Data"><i class="fa fa-edit "></i></a>
                              <a href="javascript:void(0);" data-href="<?= site_url('administrator/anri_master_instansi/delete/' . $master_instansi->id); ?>" class="btn btn-sm btn-danger remove-data" title="Hapus Data"><i class="fa fa-trash"></i></a>
                           </td>
                        </tr>
                      <?php endforeach; ?>
                     </tbody>
                  </table>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
  $(document).ready(function(){
   
  }); /*end doc ready*/
  
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });

</script>