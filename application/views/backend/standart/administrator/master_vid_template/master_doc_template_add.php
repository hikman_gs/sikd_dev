<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a  href="<?= site_url('administrator/anri_master_doc_template'); ?>">Pengaturan Template Dokumen</a></li>
        <li class="active">Tambah Data</li>        
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Tambah Data</h3>
                            <h5 class="widget-user-desc">Template Dokumen</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/anri_master_doc_template/add_save'), [
                            'name'    => 'form_master_doc_template', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_master_doc_template', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                        <div class="form-group ">
                            <label for="doc_desc" class="col-sm-2 control-label">Nama Template 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="250" name="doc_desc" id="doc_desc" placeholder="Nama Template" value="<?= set_value('doc_desc'); ?>" required autofocus>
                                <small class="info help-block">
                                <b>Input Nama Template</b> Max Length : 250.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="Header" class="col-sm-2 control-label" style="text-align:left">Upload Template 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="image_header" id="image_header" placeholder="Upload Template" value="<?= set_value('doc_file'); ?>" required>
                                <small class="info help-block">
                                <b><font color ='red'>Jenis File : doc | docx | pdf | rtf |jpg | jpeg | png</font>. Input Upload Template</b> Max Length : 255.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="upload_date" class="col-sm-2 control-label">Tanggal Upload 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" value="<?= date('Y-m-d'); ?>" readonly name="upload_date"  id="upload_date">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>                       
                           <input type="submit" name="submit" value="Simpan" class="btn btn-flat btn-danger">                       
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
