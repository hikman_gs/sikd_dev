<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Daftar Template Naskah Dinas</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Video Tutorial Penggunaan Aplikasi</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>                

                  <div class="table-responsive"> 
                  <table id="example1" class="table table-bordered table-striped">
                      
                        
                        <tr>
                           <th><iframe width="470" height="315" src="https://www.youtube.com/embed/cj1ncEdZ2mw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></th>
                           <th><iframe width="470" height="315" src="https://www.youtube.com/embed/covsoscH9Tw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></th>
                        </tr>
                        <tr>
                           <th><iframe width="470" height="315" src="https://www.youtube.com/embed/j-uLuHakwos" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></th>
                           <th><iframe width="470" height="315" src="https://www.youtube.com/embed/covsoscH9Tw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></th>
                        </tr>
                        <tr>
                           <th><iframe width="470" height="315" src="https://www.youtube.com/embed/cj1ncEdZ2mw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></th>
                           <th><iframe width="470" height="315" src="https://www.youtube.com/embed/covsoscH9Tw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></th>
                           
                        </tr>
                     
                     <tbody id="tbody_master_vid_template">
                     
                     </tbody>
                  </table>
                  </div>
               </div>
               </form>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->