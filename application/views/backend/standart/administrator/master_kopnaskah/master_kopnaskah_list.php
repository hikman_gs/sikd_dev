<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Kop Naskah Dinas</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                        <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="Tambah Data" href="<?=  site_url('administrator/anri_master_kopnaskah/add'); ?>"><i class="fa fa-plus-square-o" ></i> Tambah Data</a>
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Pengaturan Kop Naskah Dinas</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <div class="table-responsive"> 
                  <table id="example" class="table table-bordered table-striped table-hover dataTable">
                     <thead>
                        <tr class="">
                           <th>
                            <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                           </th>
                           <th>Grup Unit Kerja</th>
                           <th>Digital Kop Naskah</th>
                           <th>Lokasi Penandatangan</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody id="tbody_master_kopnaskah">
                     <?php 
                     $master_kopnaskahs = $this->db->select('*')->from('master_kopnaskah')->join('master_grole','master_grole.GRoleId=master_kopnaskah.GRoleId')->get()->result();
                     $no = 0;
                     foreach($master_kopnaskahs as $master_kopnaskah): 
                      ?>
                        <tr>
                           <td width="5">
                              <input type="checkbox" class="flat-red check" name="id[]" value="<?= $master_kopnaskah->KopId; ?>">
                           </td>
                           
                           <td><?= _ent($master_kopnaskah->GRoleName); ?></td> 
                           <td><i class="fa fa-image"></i> <a href="#" data-toggle="modal" data-target="#modal-default<?=$no;?>"><?= _ent($master_kopnaskah->Header); ?></a></td> 
                           <td><?= _ent($master_kopnaskah->Lokasi); ?></td> 

                           <?php
                              if($this->db->query("SELECT GRoleId FROM role WHERE GRoleId = '".$master_kopnaskah->GRoleId."'")->num_rows() > 0)
                              {
                           ?>

                              <td width="200">
                                <a href="<?= site_url('administrator/anri_master_kopnaskah/update/' . $master_kopnaskah->KopId); ?>" class="btn btn-primary btn-sm" title="Ubah Data"><i class="fa fa-edit"></i></a>
                              </td>

                           <?php } else { ?>


                              <td width="200">
                                <a href="<?= site_url('administrator/anri_master_kopnaskah/update/' . $master_kopnaskah->KopId); ?>" class="btn btn-primary btn-sm" title="Ubah Data"><i class="fa fa-edit"></i></a>

                                <a href="javascript:void(0);" data-href="<?= site_url('administrator/anri_master_kopnaskah/delete/' . $master_kopnaskah->KopId); ?>" class=" btn-danger btn-sm btn remove-data" title="Hapus Data"><i class="fa fa-trash" ></i> </a>
                             </td>

                           <?php } ?>

                        </tr>
                      <?php 
                      $no++;
                    endforeach; ?>
                     </tbody>
                  </table>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->
<?php 
$no = 0;
foreach($master_kopnaskahs as $master_kopnaskah): ?>
<div class="modal fade" id="modal-default<?=$no;?>">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
              </div>
              <div class="modal-body">
                <p><img class="img-responsive" src="<?=BASE_URL('FilesUploaded/kop/')?><?= _ent($master_kopnaskah->Header); ?>"></p>
              </div>
            </div>
          </div>
</div>
<?php
$no++;
 endforeach;?>
<!-- Page script -->
<script>
  $(document).ready(function(){

    $('#example').DataTable( {
        "order": [[ 1, "asc" ]]
    } );
  }); /*end doc ready*/
       
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });

</script>