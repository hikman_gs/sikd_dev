<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a  href="<?= site_url('administrator/anri_master_kopnaskah'); ?>">Pengaturan Kop Naskah Dinas</a></li>
        <li class="active">Tambah Data</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Tambah Data</h3>
                            <h5 class="widget-user-desc">Kop Naskah Dinas</h5>
                            <hr>
                        </div>
                        <?= form_open_multipart(BASE_URL('administrator/anri_master_kopnaskah/add_save'), [
                            'name'    => 'form_master_kopnaskah', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_master_kopnaskah', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                        <div class="form-group ">
                            <label for="GRoleId" class="col-sm-2 control-label" style="text-align:left">Grup Unit Kerja 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="GRoleId" id="GRoleId" data-placeholder="Select Grup Jabatan" required>
                                    <?php foreach (db_get_all_data('master_grole') as $row): ?>
                                    <option value="<?= $row->GRoleId ?>"><?= $row->GRoleName; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Grup Unit Kerja</b> Max Length : 14.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="Header" class="col-sm-2 control-label" style="text-align:left">Upload Kop Naskah 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="image_header" id="image_header" placeholder="Upload Kop" value="<?= set_value('Header'); ?>" required>
                                <small class="info help-block">
                                <b>Input Upload Header Naskah</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="Lokasi" class="col-sm-2 control-label" style="text-align:left">Lokasi Penandatangan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="20" name="Lokasi" id="Lokasi" placeholder="Lokasi Penandatangan" value="<?= set_value('Lokasi'); ?>" required autofocus>
                                <small class="info help-block">
                                <b>Input Lokasi Penandatangan</b> Max Length : 20.</small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>                       
                           <input type="submit" name="submit" value="Simpan" class="btn btn-flat btn-danger">                       
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
