<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a  href="<?= site_url('administrator/anri_master_kopnaskah'); ?>">Pengaturan Kop Naskah Dinas</a></li>
        <li class="active">Ubah Data</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Ubah Data</h3>
                            <h5 class="widget-user-desc">Kop Naskah Dinas</h5>
                            <hr>
                        </div>                                                   
                        <?= form_open(base_url('administrator/anri_master_kopnaskah/update_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_master_kopnaskah', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_master_kopnaskah', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                        <div class="form-group ">
                            <label for="GRoleId" class="col-sm-2 control-label" style="text-align:left">Grup Unit Kerja 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <select  class="form-control chosen chosen-select-deselect" name="GRoleId" id="GRoleId" data-placeholder="Select Grup Unit Kerja" required>
                                    <?php foreach (db_get_all_data('master_grole') as $row): ?>
                                    <option <?=  $row->GRoleId ==  $master_kopnaskah->GRoleId ? 'selected' : ''; ?> value="<?= $row->GRoleId ?>"><?= $row->GRoleName; ?></option>
                                    <?php endforeach; ?>  
                                </select>
                                <small class="info help-block">
                                <b>Input Grup Unit Kerja</b> Max Length : 14.</small>
                            </div>
                        </div>

                        <?php                               
                        if($this->db->query("SELECT Header FROM master_kopnaskah WHERE KopId = '".$master_kopnaskah->KopId."'")->num_rows()> 0)

                            if($master_kopnaskah->Header != '')
                            { ?>
                                                                          
                              <div class="form-group">
                                  <label for="title" class="col-sm-2 control-label" style="text-align:left">Data Digital</label>
                                  <div class="col-sm-2">
                                      <div class="thumbnail" style="overflow: hidden;">
                                        <h6><?= $master_kopnaskah->Header; ?></h6>
                                        <a href="#" data-href="<?= base_url('administrator/anri_master_kopnaskah/hapus_file3/'.$master_kopnaskah->KopId.'/'.$master_kopnaskah->Header) ?>" class="btn btn-danger btn-block remove-data">Hapus Data</a>
                                      </div>
                                  </div>
                              </div>
                            <?php  } else { ?>

                            <?php  } ?>  

                        <div class="form-group ">
                            <label for="Header" class="col-sm-2 control-label" style="text-align:left">Upload Kop Naskah 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="file" class="form-control" name="image_header" id="image_header" placeholder="Upload Kop" value="<?= set_value('Header', $master_kopnaskah->Header); ?>">
                                <small class="info help-block">
                                <b>Input Upload Header Naskah</b> Max Length : 255.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="Lokasi" class="col-sm-2 control-label" style="text-align:left">Lokasi Penandatangan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="20" name="Lokasi" id="Lokasi" placeholder="Lokasi" value="<?= set_value('Lokasi', $master_kopnaskah->Lokasi); ?>" required>
                                <small class="info help-block">
                                <b>Input Lokasi Penandatangan</b> Max Length : 20.</small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                            <input type="submit" name="submit" value="Simpan" class="btn btn-flat btn-danger">
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
      
      
      $('.remove-data').click(function(){
      var url = $(this).attr('data-href');
      swal({
          title: "Anda yakin data akan dihapus?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });       
       
              
    }); /*end doc ready*/
</script>