<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
        &nbsp;&nbsp;&nbsp;&nbsp;Daftar Konsep Naskah
        <small>Semua naskah tersedia disini!</small>
      </h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">List Naskah</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      <div class="col-md-12">
        <div class="box">
          <div class="box-header with-border">

            <h3 class="box-title">Pilih Konsep Naskah</h3>

            <div class="box-tools pull-right">
              <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="" data-original-title="Collapse">
                <i class="fa fa-minus"></i></button>
              <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="" data-original-title="Remove">
                <i class="fa fa-times"></i></button>
            </div>
          </div>
          <div class="box-body" style="">
            <div class="row">
              <div class="col-md-5">
                <div class="callout callout-info">
                <p><i class="icon fa fa-info"></i>&nbsp;&nbsp; Terdapat contoh naskah untuk berbagai keperluan, untuk melihat selebihnya klik disini. <a href="<?= base_url('administrator/anri_list_temp_doc'); ?>"><button class="btn btn-default btn-xs">Template Naskah <i class="fa fa-arrow-right"></i></button></a></p>
                </div>
              </div>
            </div>
                    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/konsep_naskah_menu/create/surat_tugas'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah Word</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                      </a>
                    </div> -->
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/anri_regis_suratdinas_keluar'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Dinas</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                      </a>
                    </div>
                    
                  <div class="col-md-3 col-sm-6 col-xs-12">
                     <!--  <a href="<?= base_url('administrator/surat_nota_dinas/create'); ?>"> -->
                    <a href="<?= base_url('administrator/anri_regis_notadinas'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Nota Dinas</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                      </a>
                    </div>

                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/anri_regis_suratedaran'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Edaran</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                      </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                     
                      <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah</font></span>
                          <br>
                          <ul>
                            <li><a href="<?= base_url('administrator/surat_perintah/create'); ?>">Perangkat Daerah</a></li>
                            <li><a href="<?= base_url('administrator/Anri_regis_supergub '); ?>">Gubernur</a></li>
                          </ul>
                          <!-- <span class="info-box-number">
                            <font size="6px">

                            </font>
                          </span> -->
                        </div>
                      </div>
                    </div>
             <!--         <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="">
                      <div class="info-box">
                        <span class="info-box-icon bg-blue"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah Gubernur</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                      </a>
                    </div> -->
                    
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/anri_regis_suratintruksigub'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Instruksi Gubernur</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                      </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/surat_keterangan/create'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Keterangan</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                      </a>
                    </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/anri_regis_super_m_tugas'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-green"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Pernyataan Melaksanakan Tugas</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/surat_pengumuman/create'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-navy"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Pengumuman</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </a>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/Anri_regis_surat_izin'); ?>">
                        <div class="info-box">
                          <span class="info-box-icon bg-blue"><i class="fa fa-file-text"></i>
                          </span>
                          <div class="info-box-content">
                            <span><font color="black">Surat Izin</font></span>
                            <span class="info-box-number">
                              <font size="6px">

                              </font>
                            </span>
                          </div>
                        </div>
                      </a>
                    </div>
                     <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="<?= base_url('administrator/surat_rekomendasi/create'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-teal"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Rekomendasi</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </a>
                    </div>


                     <div class="col-md-3 col-sm-6 col-xs-12" hidden="">
                      <a href="<?= base_url('administrator/Anri_regis_suratdinastcpdf'); ?>">
                      <div class="info-box">
                        <span class="info-box-icon bg-teal"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Dinas Tcpdf</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </a>
                    </div>

                    <!-- <div class="col-md-3 col-sm-6 col-xs-12">
                      <a href="">
                      <div class="info-box">
                        <span class="info-box-icon bg-yellow"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Nota Dinas TCPDF</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                      </a>
                    </div> -->



                    <!-- 
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-navy"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-teal"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-purple"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-orange"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-maroon"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-12">
                      <div class="info-box">
                        <span class="info-box-icon bg-black"><i class="fa fa-file-text"></i>
                        </span>
                        <div class="info-box-content">
                          <span><font color="black">Surat Perintah</font></span>
                          <span class="info-box-number">
                            <font size="6px">
                              
                            </font>
                          </span>
                        </div>
                      </div>
                    </div> -->
          </div>
          <!-- /.box-body -->
        </div>
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
