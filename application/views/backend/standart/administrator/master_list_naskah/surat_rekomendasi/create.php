<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<script src="<?= BASE_ASSET; ?>/ckeditor4.16/ckeditor.js"></script>
<!-- <script src="https://cdn.ckeditor.com/4.10.0/standard/ckeditor.js"></script> -->

<!-- <script src="//cdn.ckeditor.com/4.16.0/full/ckeditor.js"></script> -->

<?php $this->load->view('core_template/fine_upload'); ?>

<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<style>
  .select2-container--default .select2-selection--single {
    border: 0px;
  }

  .select2-container {
    border: 1px solid #DEDCDC !important;
    border-radius: 5px !important;
    padding: 2px 0px !important;
  }

  .chosen-container-single,
  .chosen-container-multi {
    width: 100% !important;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><small></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active"><?= $title; ?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-body ">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header ">
              <div class="widget-user-image">
                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username"><?= $title; ?></h3>
              <h5 class="widget-user-desc">&nbsp;</h5>
              <hr>
            </div>
             <form id="myfrm" class="form-horizontal" action="<?= base_url('administrator/surat_rekomendasi/store'); ?>" method="POST" enctype="multipart/form-data">
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="<?= date('d-m-Y') ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Pembuat Naskah</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="<?= $this->session->userdata('peoplename'); ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Jenis Naskah</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="Rekomendasi">
                </div>
              </div>
            <!--   <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Add more</label>
                <div class="col-sm-8">
                  <div class="input_fields_wrap">
                    <button class="add_field_button">Add More Fields</button>
                    <div> <textarea class="ckeditor" name="ck[]"></textarea></div>
                  </div>
                </div>
              </div> -->
              <div class="form-group <?php if(form_error('ClId')){ echo 'has-error';}?>">
                <label for="title" class="col-sm-2 control-label">Kode Klasifikasi <sup class="text-danger">*</sup></label>
                <div class="col-sm-6">
                  <select name="ClId" id="ClId" class="form-control select2">
                    <option value="">-- Pilih Kode Klasifikasi --</option>
                    <?php foreach ($ClId as $data) { ?>
                      <option value="<?= $data->ClId; ?>"><?= $data->ClCode; ?> - <?= $data->ClName; ?></option>
                    <?php } ?>
                  </select>
                   <span class="help-block"><b><?= form_error('ClId'); ?></b></span>
                </div>
              </div>
              <div class="form-group <?php if(form_error('rolecode')){ echo 'has-error';}?>">
                <label for="title" class="col-sm-2 control-label">Unit Pengolah<sup class="text-danger">*</sup></label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="rolecode" id="rolecode" value="<?= set_value('rolecode') ?>" placeholder="Unit Pengolah">
                  <span class="help-block"><b><?= form_error('rolecode'); ?></b></span>
                </div>
              </div>
              <div class="form-group <?php if(form_error('RoleId_To[]')){ echo 'has-error';}?>">
                <label for="title" class="col-sm-2 control-label">Rekomendasi Kepada <sup class="text-danger">*</sup></label>
                <div class="col-sm-7">
                  <select name="RoleId_To[]" class="form-control select2" id="RoleId_To">
                    <option value="">-- Pilih ASN --</option>
                    <?php
                    foreach ($RoleId_To as $dat_people) {
                    ?>
                      <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                    <?php
                    }
                    ?>
                  </select>
                  <span class="help-block"><b><?= form_error('RoleId_To[]'); ?></b></span>
                </div>
              </div>
              <div class="form-group <?php if(form_error('Hal')){ echo 'has-error';}?>">
                <label for="title" class="col-sm-2 control-label">Dasar <sup class="text-danger">*</sup></label>
                <div class="col-sm-8">
                  <textarea class="ckeditor form-control" name="Hal" id="Hal"></textarea>
                  <!-- <textarea class="form-control" name="Hal" id="Hal" class="form-control mytextarea" placeholder="Dasar" cols="30" rows="10"></textarea> -->
                  <span class="help-block"><b><?= form_error('Hal'); ?></b></span>
                </div>
              </div>
            <div class="form-group <?php if(form_error('menimbang')){ echo 'has-error';}?>">
                <label for="title" class="col-sm-2 control-label">Menimbang <sup class="text-danger">*</sup></label>
                <div class="col-sm-8">
                  <textarea class="ckeditor form-control" name="menimbang" id="menimbang"></textarea>
                 <!--  <textarea class="form-control" name="Hal" id="Hal" class="form-control mytextarea" placeholder="Dasar" cols="30" rows="10"></textarea> -->
                  <span class="help-block"><b><?= form_error('menimbang'); ?></b></span>
                </div>
            </div>
            <div class="form-group <?php if(form_error('untuk')){ echo 'has-error';}?>">
                <label for="title" class="col-sm-2 control-label">Untuk <sup class="text-danger">*</sup></label>
                <div class="col-sm-8">
                  <textarea class="ckeditor form-control" name="untuk" id="untuk"></textarea>
                 <!--  <textarea class="form-control" name="Hal" id="Hal" class="form-control mytextarea" placeholder="Dasar" cols="30" rows="10"></textarea> -->
                  <span class="help-block"><b><?= form_error('untuk'); ?></b></span>
                </div>
            </div>

             <div class="form-group <?php if(form_error('TtdText')){ echo 'has-error';}?>">
                <label for="title" class="col-sm-2 control-label">Pemeriksa <sup class="text-danger">*</sup></label>
                <div class="col-sm-2">
                  <select name="TtdText" class="form-control select2 TtdText">
                    <?php foreach ($TtdText as $data1) { ?>
                      <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                    <?php } ?>
                  </select>
                  <span class="help-block"><b><?= form_error('TtdText'); ?></b></span>
                </div>
                <?php
                
                foreach ($AtasanId as $dataz) {
                  $xx1 = $dataz->PeopleId;
                }
                ?>
                <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $xx1; ?>">
                <div class="col-md-6 Approve_People hidden">
                  <select name="Approve_People" id="Approve_People" class="form-control">
                    <!-- Perbaikan Eko 09-11-2019 -->
                    <?php
                    foreach ($Approve_People as $dat_people) {
                    ?>
                      <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeoplePosition; ?></option>
                    <?php
                    }
                    ?>
                    <!-- batas akhir perbaikan -->
                  </select>
                </div>
              </div>
                 <!-- penambahan dispu -->
              <div class="form-group <?php if(form_error('TtdText2')){ echo 'has-error';}?>">
                <label for="title" class="col-sm-2 control-label">a.n<sup class="text-danger">*</sup></label>
                <div class="col-sm-2">
                  <select name="TtdText2" class="form-control select2 TtdText2">
                    <?php foreach ($TtdText2 as $data2) { ?>
                      <option value="<?= $data2->TtdText2; ?>"><?= $data2->NamaText2; ?></option>
                    <?php } ?>
                  </select>
                  <span class="help-block"><b><?= form_error('TtdText2'); ?></b></span>

                </div>

                <?php
                foreach ($NamaId as $dataz) {
                  $xx2 = $dataz->PeopleId;
                }
                ?>
                <input type="hidden" id="tmpid_atas_nama" name="tmpid_atas_nama" value="<?= $xx2; ?>">

                <div class="col-md-6 Approve_People3 hidden">
                  <select name="Approve_People3" id="Approve_People3" class="form-control">
                    <!-- Perbaikan DISPU 2020 -->
                   <?php
                    foreach ($Approve_People3 as $dat_people2) {
                    ?>
                      <option value="<?= $dat_people2->PeopleId; ?>"><?= $dat_people2->PeoplePosition; ?></option>
                    <?php
                    }
                    ?>
                    <!-- batas akhir penambahan -->
                  </select>
                </div>
              </div>
              <br>
              <div class="message"></div>
              <div class="row-fluid col-md-7">
                <br>
                <a title="Lihat Naskah" onclick="lihat_naskah();" class="btn btn-flat btn-success btn-lihatnaskah" target="_blank">Lihat Konsep Surat Rekomendasi</a>
                
                <button type="submit" onclick="simpan_naskah();" class="btn btn-danger">Simpan</button>
              </div>
            </form>
          </div>
        </div>
        <!--/box body -->
      </div>
      <!--/box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>
<script>
  // $(document).ready(function() {
  //   var max_fields      = 10; //maximum input boxes allowed
  //   var wrapper         = $(".input_fields_wrap"); //Fields wrapper
  //   var add_button      = $(".add_field_button"); //Add button ID
    
  //   var x = 1; //initlal text box count
  //   $(add_button).click(function(e){ //on add input button click
  //       e.preventDefault();
  //       if(x < max_fields){ //max input box allowed
  //           x++; //text box increment
  //           var editorId = 'editor_' +x;
  //           $(wrapper).append('<div> <textarea id="'+editorId+'" class="ckeditor" name="ck[]"></textarea><a href="#" class="remove_field">Remove</a></div>'); //add input box
  //           CKEDITOR.replace(editorId, { height: 200 });
  //       }
  //   });
    
  //   $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
  //       e.preventDefault(); $(this).parent('div').remove(); x--;
  //   })
  // });
  
  $("#RoleId_To").on("select2:select select2:unselect", function(e) {
    var items = $(this).val();
    var ip = e.params.data.id;
    var tembusan = $("#RoleId_Cc").val();

    $.each(tembusan, function(i, item) {
      $.each(items, function(k, item2) {
        if (item == item2) {
          items.splice(k, 1);
          $("#RoleId_To").val(items).change();
          return false;
        }
      })
    })
  });

  $("#RoleId_Cc").on("select2:select select2:unselect", function(e) {
    var items = $(this).val();
    var ip = e.params.data.id;
    var tujuan = $("#RoleId_To").val();

    $.each(tujuan, function(i, item) {
      $.each(items, function(k, item2) {
        if (item == item2) {
          items.splice(k, 1);
          $("#RoleId_Cc").val(items).change();
          return false;
        }
      })
    })
  });

  $(' .TtdText').change(function() {
    if ($(this).find(':selected').val() == 'none') {
      $('.Approve_People').addClass('hidden');
    } else {
      $('.Approve_People').removeClass('hidden');
      r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
      $('#Approve_People').select2();
      $('#Approve_People').attr('disabled', false);
      if (this.value == "AL") {
        idatasan = $("#tmpid_atasan").val();
        $('#Approve_People').val(idatasan).change();
        $('#Approve_People').attr('disabled', true);
      }
    }
  });

   $(' .TtdText2').change(function() {
    if ($(this).find(':selected').val() == 'none') {
      $('.Approve_People3').addClass('hidden');
    } else {
      $('.Approve_People3').removeClass('hidden');
      r_nama =  "hidden";
      $('#Approve_People3').select2();
      $('#Approve_People3').attr('disabled', false);
      if (this.value == "AL") {
        idnama = $("#tmpid_atas_nama").val();
        $('#Approve_People3').val(idnama).change();
        $('#Approve_People3').attr('disabled', true);
      }
    }
  });

  $(document).ready(function() {

    $('.select2').select2();

    var params = {};
    params[csrf] = token;
    $('#test_title_galery').fineUploader({
      template: 'qq-template-gallery',
      request: {
        endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
        params: params
      },
      deleteFile: {
        enabled: true,
        endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
      },
      thumbnails: {
        placeholders: {
          waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
          notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
        }
      },
      validation: {
        allowedExtensions: ["*"],
        sizeLimit: 0,

      },
      showMessage: function(msg) {
        toastr['error'](msg);
      },
      callbacks: {
        onComplete: function(id, name, xhr) {
          if (xhr.success) {
            var uuid = $('#test_title_galery').fineUploader('getUuid', id);
            $('#test_title_galery_listed').append('<input type="hidden" class="listed_file_uuid" name="test_title_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="test_title_name[' + id + ']" value="' + xhr.uploadName + '" />');
          } else {
            toastr['error'](xhr.error);
          }
        },
        onDeleteComplete: function(id, xhr, isError) {
          if (isError == false) {
            $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid[' + id + ']"]').remove();
            $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name[' + id + ']"]').remove();
          }
        }
      }
    }); /*end title galery*/
  }); /*end doc ready*/
</script>

<script>
  function lihat_naskah() {
    if ($("#ClId").val() == "") {
      alert("Kode Klasifikasi Wajib Diisi");
      myfrm.ClId.focus();
    } else if ($("#rolecode").val() == "") {
      alert("Unit Pengolah Wajib Diisi");
      myfrm.rolecode.focus();
    }else if (CKEDITOR.instances.Hal.getData() == "") {
      alert("Uraian Dasar Wajib Diisi");
      myfrm.Hal.focus();
    } 
    else if (CKEDITOR.instances.menimbang.getData() == "") {
      alert("Uraian Menimbang Wajib Diisi");
      myfrm.menimbang.focus();
    } 
    else if (CKEDITOR.instances.untuk.getData() == "") {
      alert("Uraian Untuk Wajib Diisi");
      myfrm.untuk.focus();
    }  else if ($("#RoleId_To").find('option:selected').length == 0) {
      alert("Yang ditugaskan Wajib Diisi");
      myfrm.RoleId_To.focus();
    } else {
      $("#myfrm").attr('target', '_blank');
      $("#myfrm").attr('action', "<?= BASE_URL('administrator/surat_rekomendasi/detail_pdf') ?>");
      $("#myfrm").submit();
    }
  }

  function simpan_naskah(){
      $("#myfrm").attr('target', '_self');
      $("#myfrm").attr('action', "<?= base_url('administrator/surat_rekomendasi/store'); ?>");
      $("#myfrm").submit();
  }
</script>