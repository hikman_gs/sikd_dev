<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>

<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/ckeditor4.16/ckeditor.js"></script>
<script src="<?= BASE_ASSET; ?>/ckeditor4.16/config.js"></script>
<style>
    textarea {
        height: 350px;
        width: 90px;
    }

    div.card,
    .tox div.card {
        width: 80px;
        background: white;
        border: 1px solid #ccc;
        border-radius: 3px;
        box-shadow: 0 4px 8px 0 rgba(34, 47, 62, .1);
        padding: 8px;
        font-size: 12px;
        font-family: Arial;
    }

    div.card::after,
    .tox div.card::after {
        content: "";
        clear: both;
        display: table;
    }

    div.card h1,
    .tox div.card h1 {
        font-size: 12px;
        font-weight: normal;
        margin: 0 0 8px;
        padding: 0;
        line-height: 15px;
        font-family: Arial;
    }

    div.card p,
    .tox div.card p {
        font-family: Arial;
    }

    div.card img.avatar,
    .tox div.card img.avatar {
        width: 48px;
        height: 48px;
        margin-right: 8px;
        float: left;
    }

    .container {
        width: 100px;
        margin-right: 20px;
    }

    .header img {
        width: 35%;
        float: left;
    }

    .header h2 {
        width: 60%;
        float: left;
        margin-left: 30px;
        font-size: 28px;
        text-align: center;
    }

    .space {
        margin-bottom: 30px;
        margin-top: 30px;
    }

    .select2-container--default .select2-selection--single {
        border: 0px;
    }

    .select2-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 2px 0px !important;
    }

    .chosen-container-single,
    .chosen-container-multi {
        width: 100% !important;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active"><?= $title; ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pencatatan Nota Dinas</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>
                        <form id="myfrm" class="form-horizontal" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                                <div class="col-sm-8">
                                    <input type="text" disabled class="form-control" value="<?= date('d-m-Y') ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Pembuat Naskah</label>
                                <div class="col-sm-8">
                                    <input type="text" disabled class="form-control" value="<?= $this->session->userdata('peoplename'); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Jenis Naskah</label>
                                <div class="col-sm-8">
                                    <input type="text" disabled class="form-control" value="Nota Dinas">
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('TtdText')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Penandatangan <sup class="text-danger">*</sup></label>
                                <div class="col-sm-2">
                                    <select name="TtdText" class="form-control select2 TtdText" required>
                                        <?php foreach ($TtdText as $data1) { ?>
                                            <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-block"><b><?= form_error('TtdText'); ?></b></span>
                                </div>

                                <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $AtasanId->PeopleId; ?>">

                                <div class="col-md-6 Approve_People hidden">
                                    <select name="Approve_People" id="Approve_People" class="form-control">
                                        <!-- Perbaikan DISPU 2020 -->
                                        <?php
                                        foreach ($Approve_People as $dat_people) {
                                        ?>
                                            <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeoplePosition; ?></option>
                                        <?php
                                        }
                                        ?>
                                        <!-- batas akhir perbaikan -->
                                    </select>
                                </div>
                            </div>

                            <div class="form-group <?php if (form_error('MeasureUnitId')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Lampiran</label>
                                <div class="col-md-2">
                                    <select name="MeasureUnitId" class="form-control chosen chosen-select" id="MeasureUnitId">
                                        <option value="">Pilih Unit</option>
                                        <?php foreach ($MeasureUnitId as $measure) { ?>
                                            <option value="<?= $measure->MeasureUnitId; ?>"><?= $measure->MeasureUnitName; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-block"><b><?= form_error('MeasureUnitId'); ?></b></span>
                                </div>
                                <div class="col-md-2">
                                    <input type="number" class="form-control" name="Jumlah" id="Jumlah" min="0" id="Jumlah">
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('ClId')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Kode Klasifikasi <sup class="text-danger">*</sup></label>
                                <div class="col-sm-6">
                                    <select name="ClId" id="ClId" class="form-control select2">
                                        <option value="">-- Pilih Kode Klasifikasi --</option>
                                        <?php foreach ($ClId as $data) { ?>
                                            <option value="<?= $data->ClId; ?>"><?= $data->ClCode; ?> - <?= $data->ClName; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-block"><b><?= form_error('ClId'); ?></b></span>
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('rolecode')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Unit Pengolah<sup class="text-danger">*</sup></label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="rolecode" id="rolecode" value="<?= set_value('rolecode') ?>" placeholder="Unit Pengolah">
                                    <span class="help-block"><b><?= form_error('rolecode'); ?></b></span>
                                </div>
                            </div>

                            <div class="form-group <?php if (form_error('UrgensiId')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="UrgensiId" class="col-sm-2 control-label">Tingkat Urgensi <sup class="text-danger">*</sup></label>
                                <div class="col-sm-2">
                                    <select name="UrgensiId" id="UrgensiId" class="form-control select2">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach ($Urgensi as $urg) { ?>
                                            <option value="<?= $urg->UrgensiId; ?>"><?= $urg->UrgensiName; ?>
                                            <?php } ?>
                                    </select>
                                    <span class="help-block"><b><?= form_error('UrgensiId'); ?></b></span>
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('SifatId')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Sifat Naskah <sup class="text-danger">*</sup></label>
                                <div class="col-sm-2">
                                    <select name="SifatId" id="SifatId" class="form-control select2">
                                        <option value="">-- Pilih --</option>
                                        <?php foreach ($sifat_naskah as $sn) { ?>
                                            <option value="<?= $sn->SifatId; ?>"><?= $sn->SifatName; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-block"><b><?= form_error('SifatId'); ?></b></span>
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('Hal')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Hal <sup class="text-danger">*</sup></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="Hal" id="Hal" placeholder="Hal">
                                    <span class="help-block"><b><?= form_error('Hal'); ?></b></span>
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('Konten')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Konsep Nota Dinas <sup class="text-danger">*</sup></label>
                                <div class="col-sm-8">
                                    <textarea name="Konten" id="Konten" class="form-control ckeditor" cols="30" rows="10" placeholder="Isi"></textarea>
                                    <span class="help-block"><b><?= form_error('Konten'); ?></b></span>
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('RoleId_To[]')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Kepada Yth <sup class="text-danger">*</sup></label>
                                <div class="col-sm-7">
                                    <select name="RoleId_To[]" class="form-control select2" id="RoleId_To" multiple>
                                        <option value="">-- Pilih --</option>
                                        <?php
                                        foreach ($RoleId_To as $dat_people) {
                                        ?>
                                            <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <span class="help-block"><b><?= form_error('RoleId_To[]'); ?></b></span>
                                </div>
                            </div>

                            <div class="form-group <?php if (form_error('RoleId_Cc')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Tembusan</label>
                                <div class="col-sm-7">
                                    <select name="RoleId_Cc[]" class="form-control select2" id="RoleId_Cc" multiple tabi-ndex="5">
                                        <?php
                                        foreach ($tembusan as $dat_people) {
                                        ?>
                                            <option value="<?= $dat_people->PrimaryRoleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                    <span class="help-block"><b><?= form_error('RoleId_Cc'); ?></b></span>
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('isi_lampiran')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Isi Lampiran <sup class="text-danger">*</sup></label>

                                <div class="col-sm-8 input_fields_wrap">
                                    <textarea class="ckeditor form-control" name="isi_lampiran[]" id="isi_lampiran"></textarea>
                                    <!--  <textarea class="form-control" name="Hal" id="Hal" class="form-control mytextarea" placeholder="Dasar" cols="30" rows="10"></textarea> -->
                                    <br>
                                </div>
                                <span class="help-block"><b><?= form_error('Isi Lampiran'); ?></b></span>
                                <button class="add_field_button btn btn-success">Tambah Isi Lampiran</button>

                            </div>

                            <!-- penambahan dispu -->
                            <div class="form-group <?php if (form_error('TtdText2')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">a.n. dan u.b.<sup class="text-danger">*</sup></label>
                                <div class="col-sm-2">
                                    <select name="TtdText2" class="form-control select2 TtdText2" id="TtdText2">
                                        <?php foreach ($TtdText2 as $data2) { ?>
                                            <option value="<?= $data2->TtdText2; ?>"><?= $data2->NamaText2; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="help-block"><b><?= form_error('TtdText2'); ?></b></span>
                                </div>

                                <?php
                                $xx2 = $NamaId->PeopleId;
                                ?>
                                <input type="hidden" id="tmpid_atas_nama" name="tmpid_atas_nama" value="<?= $xx2; ?>">

                                <div class="col-md-6 Approve_People3 hidden">
                                    <select name="Approve_People3" id="Approve_People3" class="form-control">
                                        <!-- Perbaikan DISPU 2020 -->
                                        <?php
                                        foreach ($Approve_People as $dat_people2) {
                                        ?>
                                            <option value="<?= $dat_people2->PeopleId; ?>"><?= $dat_people2->PeoplePosition; ?></option>
                                        <?php
                                        }
                                        ?>
                                        <!-- batas akhir penambahan -->
                                    </select>
                                </div>
                            </div>

                            <div class="form-group ">
                                <label for="title" class="col-sm-2 control-label">Unggah Data </label>
                                <div class="col-sm-8">
                                    <div id="test_title_galery"></div>
                                    <div id="test_title_galery_listed"></div>
                                    <small class="info help-block">
                                    </small>
                                </div>
                            </div>
                            <br>
                            <div class="message"></div>
                            <div class="row-fluid col-md-7">
                                <br>
                                <a title="Lihat Naskah" onclick="lihat_naskah();" class="btn btn-flat btn-success btn-lihatnaskah" target="_blan  k">Lihat Konsep Nota
                                    Dinas</a>
                                <button onclick="simpan_naskah();" class="btn btn-danger">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
    $(document).ready(function() {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                var editorId = 'editor_' + x;
                $(wrapper).append('<div> <textarea id="' + editorId + '" class="ckeditor" name="isi_lampiran[]"></textarea><a href="#" class="remove_field btn btn-danger">Remove </a></div><br>'); //add input box

                CKEDITOR.replace(editorId, {
                    height: 200

                });
            }
        });

        $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });

    function get_num_naskah() {
        var pil_ttd = $(".TtdText").val();

        if (pil_ttd == 'none')
            ttd_approve = "<?= $this->session->userdata('roleid'); ?>";
        else if (pil_ttd == 'AL')
            ttd_approve = "<?= $this->session->userdata('roleatasan'); ?>";
        else
            ttd_approve = ""


        alert(ttd_approve);
        $.ajax({
            url: "<?= BASE_URL('administrator/anri_regis_notadinas/get_num_naskah') ?> ",
            type: "post",
            success: function($num) {
                alert($num);
            }
        })
    }


    $("#RoleId_To").on("select2:select select2:unselect", function(e) {
        var items = $(this).val();
        var ip = e.params.data.id;
        var tembusan = $("#RoleId_Cc").val();

        $.each(tembusan, function(i, item) {
            $.each(items, function(k, item2) {
                if (item == item2) {
                    items.splice(k, 1);
                    $("#RoleId_To").val(items).change();
                    return false;
                }
            })
        })
    });

    $("#RoleId_Cc").on("select2:select select2:unselect", function(e) {
        var items = $(this).val();
        var ip = e.params.data.id;
        var tujuan = $("#RoleId_To").val();

        $.each(tujuan, function(i, item) {
            $.each(items, function(k, item2) {
                if (item == item2) {
                    items.splice(k, 1);
                    $("#RoleId_Cc").val(items).change();
                    return false;
                }
            })
        })
    });



    $(' .TtdText').change(function() {
        if ($(this).find(':selected').val() == 'none') {
            $('.Approve_People').addClass('hidden');
        } else {
            $('.Approve_People').removeClass('hidden');
            r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
            $('#Approve_People').select2();
            $('#Approve_People').attr('disabled', false);
            if (this.value == "AL") {
                idatasan = $("#tmpid_atasan").val();
                $('#Approve_People').val(idatasan).change();
                $('#Approve_People').attr('disabled', true);
            }
        }
    });

    $(' .TtdText2').change(function() {
        if ($(this).find(':selected').val() == 'none') {
            $('.Approve_People3').addClass('hidden');
        } else {
            $('.Approve_People3').removeClass('hidden');
            r_nama = "hidden";
            $('#Approve_People3').select2();
            $('#Approve_People3').attr('disabled', false);
            if (this.value == "AL") {
                idnama = $("#tmpid_atas_nama").val();
                $('#Approve_People3').val(idnama).change();
                $('#Approve_People3').attr('disabled', true);
            }
        }
    });

    $(document).ready(function() {
        $('#MeasureUnitId').change(function() {
            // alert(',' + $('#MeasureUnitId').find('option:selected').text() + ', ');
            if ($(this).find(':selected').val() == 'XxJyPn38Yh.6') {
                $('#Jumlah').prop("disabled", true);
            } else {
                $('#Jumlah').prop("disabled", false);
            }
        });

        $('.select2').select2();

        var params = {};
        params[csrf] = token;
        $('#test_title_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
                params: params
            },
            deleteFile: {
                enabled: true,
                endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: ["*"],
                sizeLimit: 0,

            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#test_title_galery').fineUploader('getUuid', id);
                        $('#test_title_galery_listed').append(
                            '<input type="hidden" class="listed_file_uuid" name="upload_naskahdinas_uuid[' +
                            id + ']" value="' + uuid +
                            '" /><input type="hidden" class="listed_file_name" name="upload_naskahdinas_name[' +
                            id + ']" value="' + xhr.uploadName + '" />');
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#test_title_galery_listed').find(
                                '.listed_file_uuid[name="upload_naskahdinas_uuid[' + id + ']"]')
                            .remove();
                        $('#test_title_galery_listed').find(
                                '.listed_file_name[name="upload_naskahdinas_name[' + id + ']"]')
                            .remove();
                    }
                }
            }
        }); /*end title galery*/
    }); /*end doc ready*/
</script>



<script>
    function lihat_naskah() {

        if ($("#ClId").val() == "") {
            alert("Kode Klasifikasi Wajib Diisi");
            myfrm.ClId.focus();
        } else if ($("#MeasureUnitId").val() == "") {
            alert("Lampiran Wajib Dipilih");
            myfrm.MeasureUnitId.focus();

        } else if ($("#MeasureUnitId").val() != "XxJyPn38Yh.6" && $("#Jumlah").val() == "") {
                alert('Jumlah Wajib diisi');
                myfrm.Jumlah.focus();
        } else if ($("#rolecode").val() == "") {
            alert("Unit Pengolah Wajib Diisi");
            myfrm.rolecode.focus();
        } else if ($("#UrgensiId").val() == "") {
            alert("Tingkat Urgensi Wajib Diisi");
            myfrm.UrgensiId.focus();
        } else if ($("#SifatId").val() == "") {
            alert("Sifat Naskah Wajib Diisi");
            myfrm.SifatId.focus();
        } else if ($("#Hal").val() == "") {
            alert("Uraian Hal Wajib Diisi");
            myfrm.Hal.focus();
        } else if (CKEDITOR.instances.Konten.getData() == "") {
            alert("Konsep Nota Dinas Wajib Diisi");
            myfrm.Konten.focus();
        } else if ($("#RoleId_To").find('option:selected').length == 0) {
            alert("Kepada Yth Wajib Diisi");
            myfrm.RoleId_To.focus();
        } else if ($("#TtdText2").val() == "") {
            alert("a.n. dan u.b.");
            myfrm.TtdText2.focus();
        } else {
            $("#myfrm").attr('target', '_blank');
            $("#myfrm").attr('action', "<?= BASE_URL('administrator/surat_nota_dinas/detail_pdf') ?>");
            $("#myfrm").submit();
        }

    }

    function simpan_naskah() {


        $("#myfrm").attr('target', '_self');
        $("#myfrm").attr('action', "<?= BASE_URL('administrator/surat_nota_dinas/store'); ?>");
        $("#myfrm").submit();

    }
</script>