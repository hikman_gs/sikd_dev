<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active"><?= $title; ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username"><?= $title; ?></h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <form name="form_inbox" id="form_inbox" action="<?= base_url('administrator/inbox/index'); ?>">
                  <div class="table-responsive"> 

                  <table id="myTable" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%">

                     <thead>
                        <tr>
                           <th>No</th>
                           <th>Nomor Pengumuman</th>
                           <th>Tanggal Naskah</th>
                           <th>Asal Naskah</th>
                           <th>Hal</th>
                           <th width="10%">Aksi</th>
                        </tr>
                     </thead>

                     <tbody></tbody>

                  </table>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->

<script>
    


   var table1;

  $(document).ready(function(){
        //datatables
        table1 = $('#myTable').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [[ 2, "desc" ]], 
            "ajax": {
                "url": BASE_URL +"/administrator/surat_pengumuman/get_ajax",
                "type": "POST",
            },
            "columnDefs": [
              // {
              //    "target":[4,5],
              //   "className": 'text-center',
              // },

              {
                 "target":[0,5],
                  "orderable": false,
              },


            ],
 
        });
  }); /*end doc ready*/    


  
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });

</script>