<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<style>

#approve .modal-content{
    /* new custom width */
    height: 220px;
}

#nomor .modal-content{
    /* new custom width */
    height: 220px;
}

#k_teruskan .modal-content{
    /* new custom width */
    width: 520px;
}

.select2-container--default .select2-selection--single{border:0px;}
.select2-container{
      border: 1px solid #DEDCDC!important;
      border-radius: 5px!important;
      padding: 2px 0px!important;
}
.chosen-container-single,.chosen-container-multi{width: 100%!important;}

</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active"><?= $title; ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">

                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                        <h3 class="widget-user-username">Naskah Belum Kasih Nomor</h3>
                        <h5 class="widget-user-desc">&nbsp;</h5>
                        <hr>
                  </div>

                  <div class="table-responsive"> 

                  <table id="myTable" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%"> 

                     <thead>
                        <tr>
                           <th>No</th>
                           <th>Jenis Naskah</th>
                           <th>Nomor</th>
                           <th>Tanggal</th>                          
                           <th>Hal</th>
                           <th>Tujuan</th>
                           <th>Lampiran</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>

                     <tbody></tbody>

                  </table>

                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
 <div id="nomor" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><b>Penomoran Surat Dinas Manual</b></h4>
                </div>
                <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/nomor_dong'); ?>" class="form-horizontal" method="POST">
                    <div class="modal-body">
                        <div class="content">
                            <div class="form-group">
                                <input type="hidden" id="NId_Temp3" name="NId_Temp3">
                                <input type="hidden" id="GIR_Id3" name="GIR_Id3">
                                <div class="col-6">
                                    <label>Masukan Nomor Surat Dinas</label>
                                    <input type="text" class="form-control" name="nonaskah" id="nonaskah">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-6">
                                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-check"></i> Ambil Nomor Surat </button>
                                </div>   
                            </div>                             
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

<!-- Page script -->

<script type="text/javascript">
    
    $(document).ready(function() {

        var dataTable =  $('#myTable').DataTable( { 

            "processing": true, 
            "serverSide": true, 
            "order": [[3, "desc" ]],
            
            "ajax": {
                "url": "<?php echo site_url('administrator/Anri_log_surat_dinas_belum_kasih_nomerdispu/get_data_surat_dinas_keluar')?>",
                "type": "POST",
                //deferloading untuk menampung 10 data pertama dulu
                "deferLoading": 10,
            },

            
            "columnDefs": [
            { 
                "targets": [ 0, 7 ], 
                "orderable": false, 
            },
            ],

        });   

    });

    function ko_nomor(NId, GIR_Id) {
    $("#NId_Temp3").val(NId);
    $("#GIR_Id3").val(GIR_Id);
    $("#nomor").modal('show');
}
</script>