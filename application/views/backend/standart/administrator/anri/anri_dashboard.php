<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<section class="content-header">
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i>Dashboard</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <h3 class="text-center">Selamat Datang Di <?= site_name(); ?></h3>
  </div><br>

  <?php if (($this->session->userdata('groupid') == 3) || ($this->session->userdata('groupid') == 4) || ($this->session->userdata('groupid') == 7)) { ?>

    <!-- mulai row -->

    <div class="row">

      <a href="<?= BASE_URL('administrator/anri_list_naskahmasuksemua_btl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-brown"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Semua Naskah</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_naskahmasuksemua();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>
      <!-- ./col -->

      <a href="<?= BASE_URL('administrator/anri_list_naskahmasuk_btl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Naskah Masuk </font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_naskahmasuk();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>
      <!-- ./col -->

      <a href="<?= BASE_URL('administrator/anri_list_disposisi_btl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa fa-files-o"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Disposisi</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_disposisi();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>
      <!-- ./col -->


      <a href="<?= BASE_URL('administrator/anri_list_teruskan_btl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-mail-forward"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Naskah Teruskan</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_teruskan();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>
      <!-- ./col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <a href="<?= BASE_URL('administrator/Anri_list_notakeluar_btl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-brown"><i class="fa fa-envelope-o"></i></span>
            <div class="info-box-content">
              <span>
                <font color="black">Nota Dinas</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_notakeluar();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
          </div>
        </div>
      </a>
      <!-- ./col -->

      <a href="<?= BASE_URL('administrator/anri_list_undangan_btl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-file-text"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Undangan</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_undangan();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>
      <!-- ./col -->

      <a href="<?= BASE_URL('administrator/anri_list_surattugas_btl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-brown"><i class="fa fa-list-alt"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Surat Perintah</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_tugas();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>
      <!-- /.col -->

      <a href="<?= BASE_URL('administrator/surat_keterangan/list_tbl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-brown"><i class="fa fa-list-alt"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Surat Keterangan</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah_ket = $this->model_jumlah->hitung_keterangan();
                  echo $total_naskah_ket;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>

      <a href="<?= BASE_URL('administrator/surat_rekomendasi/list_tbl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-brown"><i class="fa fa-list-alt"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Surat Rekomendasi</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah_ket = $this->model_jumlah->hitung_rekomendasi();
                  echo $total_naskah_ket;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>

      <a href="<?= BASE_URL('administrator/surat_pengumuman/list_tbl') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-brown"><i class="fa fa-list-alt"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Pengumuman</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah_ket = $this->model_jumlah->hitung_umum();
                  echo $total_naskah_ket;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>


        <a href="<?= BASE_URL('administrator/anri_list_instruksi_btl') ?>">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="glyphicon glyphicon-info-sign"></i></span>

              <div class="info-box-content">
                <span>
                  <font color="black">Surat Instruksi</font>
                </span>
                <span class="info-box-number">
                  <font size="6px">
                    <?php
                    $total_naskah = $this->model_jumlah->hitung_instruksi();
                    echo $total_naskah;
                    ?>
                  </font>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>

          <!-- ./col -->

          <a href="<?= BASE_URL('administrator/anri_list_surat_m_tugas_btl') ?>">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-clipboard" aria-hidden="true"></i></span>

                <div class="info-box-content">
                  <span>
                    <font color="black">Surat Pernyataan Melaksanakan Tugas</font>
                  </span>
                  <span class="info-box-number">
                    <font size="6px">
                      <?php
                      $total_naskah = $this->model_jumlah->hitung_m_tugas();
                      echo $total_naskah;
                      ?>
                    </font>
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
          </a>


        </a>

        <div class="row"></div>

        <a href="<?= BASE_URL('administrator/anri_list_suratdinas_keluar_btl') ?>">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-green"><i class="fa fa-arrow-circle-right "></i></span>

              <div class="info-box-content">
                <span>
                  <font color="black">Surat Dinas Keluar</font>
                </span>
                <span class="info-box-number">
                  <font size="6px">
                    <?php
                    $total_naskah = $this->model_jumlah->hitung_keluar();
                    echo $total_naskah;
                    ?>
                  </font>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
          <!-- /.col -->

          <a href="<?= BASE_URL('administrator/anri_list_naskahdinas_lain_btl') ?>">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-send"></i></span>

                <div class="info-box-content">
                  <span>
                    <font color="black">Naskah Dinas Lainnya</font>
                  </span>
                  <span class="info-box-number">
                    <font size="6px">
                      <?php
                      $total_naskah = $this->model_jumlah->hitung_nadin_lain();
                      echo $total_naskah;
                      ?>
                    </font>
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
          </a>

          <a href="<?= BASE_URL('administrator/Anri_list_suratizin_btl') ?>">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-blue"><i class="fa fa-info"></i></span>

                <div class="info-box-content">
                  <span>
                    <font color="black">Surat Izin</font>
                  </span>
                  <span class="info-box-number">
                    <font size="6px">
                      <?php
                      $total_naskah = $this->model_jumlah->hitung_izin();
                      echo $total_naskah;
                      ?>
                    </font>
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
          </a>
          <!-- ./col -->
    </div>
    <!-- /.row -->

    <div class="row">
      <a href="<?= BASE_URL('administrator/anri_list_naskah_belum_approve') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-gears"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Naskah Belum Disetujui</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_app();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </a>

      <a href="<?= BASE_URL('administrator/anri_list_naskah_sudah_approve') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-yellow"><i class="fa  fa-paste"></i></span>

            <div class="info-box-content">
              <span>
                <font color="black">Naskah Belum Dikirim</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_nadin_kirim();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
        </div>
      </a>
      <!-- ./col -->


      <!-- <a href="<?= BASE_URL('administrator/anri_list_notadinas_btl_ap'); ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-check-square-o"></i></span>
            <div class="info-box-content">
              <span>
                <font color="black">Nota Dinas Belum Setujui</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  $total_naskah = $this->model_jumlah->hitung_bapp();
                  echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
          </div>
        </div>
      </a> -->
      <!-- ./col -->


      <!-- ./col -->
      <!-- <a href="<?= BASE_URL('administrator/anri_list_notadinas_sdh_ap') ?>">
        <div class="col-md-3 col-sm-6 col-xs-12">
          <div class="info-box">
            <span class="info-box-icon bg-blue"><i class="fa fa-send"></i></span>
            <div class="info-box-content">
              <span>
                <font color="black">Nota Dinas Belum Dikirim</font>
              </span>
              <span class="info-box-number">
                <font size="6px">
                  <?php
                  // $total_naskah = $this->model_jumlah->hitung_sapp();
                  // echo $total_naskah;
                  ?>
                </font>
              </span>
            </div>
          </div>
        </div>
      </a> -->
      <!-- ./col -->

    </div>
    <!-- /.row -->



    <?php if (($this->session->userdata('primaryroleid') == 'uk.1')) { ?>
      <div class="row">
        <a href="<?= BASE_URL('administrator/Anri_list_naskahmasuksemua_acaragub_btl') ?>">
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
              <span class="info-box-icon bg-yellow"><i class="fa fa-calendar-check-o"></i></span>

              <div class="info-box-content">
                <span>
                  <font color="black">Naskah Acara Masuk</font>
                </span>
                <span class="info-box-number">
                  <font size="6px">
                    <?php
                    $total_naskah = $this->model_jumlah->hitung_naskahmasuk_acaragub();
                    echo $total_naskah;
                    ?>
                  </font>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
          </div>
        <?php } ?>
        <!-- /.col -->
        </a>


        <?php if (($this->session->userdata('primaryroleid') == 'uk.1')) { ?>
          <a href="<?= BASE_URL('administrator/Anri_list_naskahmasuksemua_audiengub_btl') ?>">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-id-card-o"></i></span>

                <div class="info-box-content">
                  <span>
                    <font color="black">Naskah Masuk Audiensi</font>
                  </span>
                  <span class="info-box-number">
                    <font size="6px">
                      <?php
                      $total_naskah = $this->model_jumlah->hitung_naskahmasuk_audiengub();
                      echo $total_naskah;
                      ?>
                    </font>
                  </span>
                </div>
                <!-- /.info-box-content -->
              </div>
              <!-- /.info-box -->
            </div>
          <?php } ?>
          </a>

          <?php if (($this->session->userdata('primaryroleid') == 'uk.1')) { ?>
            <a href="<?= BASE_URL('administrator/anri_list_naskahmasuksemua_Naskahttd_gub_btl') ?>">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-yellow"><i class="fa fa-text-width"></i></span>

                  <div class="info-box-content">
                    <span>
                      <font color="black">Naskah Masuk Penandatangan Gubernur</font>
                    </span>
                    <span class="info-box-number">
                      <font size="6px">
                        <?php
                        $total_naskah = $this->model_jumlah->hitung_naskahmasuk_ttdgub();
                        echo $total_naskah;
                        ?>
                      </font>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
            <?php } ?>
            </a>

            <div class="row">
              <a href="<?= BASE_URL('administrator/Anri_list_naskah_koreksi') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-blind" aria-hidden="true"></i></span>

                    <div class="info-box-content">
                      <span>
                        <font color="black">Naskah Koreksi Konsep </font>
                      </span>
                      <span class="info-box-number">
                        <font size="6px">
                          <?php
                          $total_naskah = $this->model_jumlah->hitung_naskahkoreksi();
                          echo $total_naskah;
                          ?>
                        </font>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
              </a>

              <a href="<?= BASE_URL('administrator/tandatangan/respons/dokumen') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="fa fa-pencil"></i></span>

                    <div class="info-box-content">
                      <span>
                        <font color="black">TTD Yang Perlu Direspon</font>
                      </span>
                      <span class="info-box-number">
                        <font size="6px">
                          <?php
                          $PeopleID = $this->session->userdata('peopleid');
                          $this->db->where('PeopleIDTujuan', $PeopleID);
                          $this->db->where('status', 0);
                          $this->db->where('next', 1);
                          $this->db->from('m_ttd_kirim');
                          $jumlahfilerespon = $this->db->count_all_results();
                          echo $jumlahfilerespon;
                          ?>
                        </font>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
              </a>

              <a href="<?= BASE_URL('administrator/tandatangan/dokumen/kirim') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa  fa-pencil"></i></span>

                    <div class="info-box-content">
                      <span>
                        <font color="black">TTD Yang Perlu Dikirim</font>
                      </span>
                      <span class="info-box-number">
                        <font size="6px">
                          <?php
                          $PeopleID = $this->session->userdata('peopleid');
                          $this->db->where('PeopleIDTujuan', $PeopleID);
                          $this->db->group_by('ttd_id');
                          $this->db->from('m_ttd_terusankirim');
                          $this->db->where('status', 0);
                          $jumlahfilekirim = $this->db->count_all_results();
                          echo $jumlahfilekirim;
                          ?>
                        </font>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
              </a>
              <!-- ./col -->

            </div>

          <?php } ?>

          <?php if (($this->session->userdata('groupid') == 6)) { ?>

            <!-- ./col -->

            <a href="<?= BASE_URL('administrator/anri_list_naskahmasuksemua_btl') ?>">
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

                  <div class="info-box-content">
                    <span>
                      <font color="black">Naskah Masuk Unit Kearsipan </font>
                    </span>
                    <span class="info-box-number">
                      <font size="6px">
                        <?php
                        $total_naskah = $this->model_jumlah->hitung_naskahmasuksemua();
                        echo $total_naskah;
                        ?>
                      </font>
                    </span>
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>
            </a>
            
            <a href="<?=BASE_URL('administrator/Anri_list_suratdinas_kirim_keluar_btl')?>">        
              <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                  <span class="info-box-icon bg-aqua"><i class="fa fa-envelope-o"></i></span>

                  <div class="info-box-content">
                    <span><font color="black">Naskah Harus dikirim</font></span>
                    <span class="info-box-number">
                      <font size="6px">
                        <?php 
                        $total_naskah = $this->model_jumlah->hitung_naskah_uk_kirim_keluar();
                        echo $total_naskah; 
                        ?>  
                      </font>
                    </span>  
                  </div>
                  <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
              </div>        
            </a>

            <?php if (($this->session->userdata('primaryroleid') == 'uk.1.1.1.1.1')) { ?>
              <a href="<?= BASE_URL('administrator/anri_list_naskahmasuksemua_Naskahttd_gub_btl') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12">
                  <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-text-width"></i></span>

                    <div class="info-box-content">
                      <span>
                        <font color="black">Naskah Masuk Penandatangan Gubernur</font>
                      </span>
                      <span class="info-box-number">
                        <font size="6px">
                          <?php
                          $total_naskah = $this->model_jumlah->hitung_naskahmasuk_ttdgub();
                          echo $total_naskah;
                          ?>
                        </font>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
              <?php } ?>
              </a>
              <!-- ./col -->

              <!-- ./col -->
            <?php } ?>
            <?php if (($this->session->userdata('groupid') == 7)) { ?>
              <a href="<?= BASE_URL('administrator/Anri_list_naskah_koreksi') ?>">
                <div class="col-md-3 col-sm-6 col-xs-12" hidden="">
                  <div class="info-box">
                    <span class="info-box-icon bg-aqua"><i class="fa fa-blind" aria-hidden="true"></i></span>

                    <div class="info-box-content">
                      <span>
                        <font color="black">Naskah Koreksi Konsep </font>
                      </span>
                      <span class="info-box-number">
                        <font size="6px">
                          <?php
                          $total_naskah = $this->model_jumlah->hitung_naskahkoreksi();
                          echo $total_naskah;
                          ?>
                        </font>
                      </span>
                    </div>
                    <!-- /.info-box-content -->
                  </div>
                  <!-- /.info-box -->
                </div>
                <!-- /.col -->
              <?php } ?>
              </a>


              <div class="row">


                <!-- ./col -->

                <!-- ./col -->



              </div>
              <!-- /.row -->
              <style>
                #pengumuman {
                  -webkit-animation: UMUM-YOUR-ANIMATION 1s infinite;
                  /* Safari 4+ */
                  -moz-animation: UMUM-YOUR-ANIMATION 1s infinite;
                  /* Fx 5+ */
                  -o-animation: UMUM-YOUR-ANIMATION 1s infinite;
                  /* Opera 12+ */
                  animation: UMUM-YOUR-ANIMATION 1s infinite;
                  /* IE 10+, Fx 29+ */
                }

                @-webkit-keyframes UMUM-YOUR-ANIMATION {

                  0%,
                  49% {
                    background-color: rgba(9, 132, 227, 1.0);
                    /*border: 3px solid #e50000;*/
                    color: white;
                  }

                  50%,
                  100% {
                    background-color: rgba(116, 185, 255, 1.0);
                    /*border: 3px solid rgb(117,209,63);*/
                    color: white;
                  }
                }
              </style>

              <div id="news" class="modal fade" role="dialog">
                <div class="modal-lg modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header" id="pengumuman">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">INFORMASI TERBARU!!!</h4>
                    </div>
                    <div class="modal-body">
                      <span class="pull-right">Tanggal hari ini : <b><?= date('d/m/Y'); ?></b></span>
                      <!-- Custom Tabs -->
                      <?php
                      if ($this->session->userdata('groupid') == 7) {
                        $notif = $this->db->query("SELECT NId FROM inbox_receiver WHERE RoleId_To='" . $this->session->userdata('roleid') . "' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND StatusReceive = 'unread'")->num_rows();
                      } else {
                        $notif = $this->db->query("SELECT NId FROM inbox_receiver WHERE RoleId_To='" . $this->session->userdata('roleid') . "' AND StatusReceive = 'unread' AND ReceiverAs NOT IN ('to_draft_keluar','to_draft_nadin','to_draft_notadinas')")->num_rows();
                      }
                      ?>
                      <?php
                      if ($notif != 0) {
                        echo 'Anda memiliki <b><font size="2" color="red"> ' . $notif . '</font></b> pesan hari ini!';
                      }
                      ?>


                      <?php
                      if ($this->session->userdata('groupid') == 9) {
                        $hitung_naskahmasuk = $this->db->query("SELECT NId FROM v_suratmasuk WHERE ReceiverAs in('to','bcc') AND NTipe = 'inbox' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                      } else {
                        $hitung_naskahmasuk = $this->db->query("SELECT NId FROM v_suratmasuk WHERE ReceiverAs in('to','bcc') AND NTipe = 'inbox' AND StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                      }

                      //edit tambahan
                      if ($this->session->userdata('groupid') == 9) {
                        $hitung_notadinas = $this->db->query("SELECT NId FROM v_tem_notadinas WHERE Status = '0' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                      } else {
                        $hitung_notadinas = $this->db->query("SELECT NId FROM v_tem_notadinas WHERE Status = '0' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                      }

                      if ($this->session->userdata('groupid') == 9) {
                        $hitung_teruskan = $this->db->query("SELECT NId FROM v_tem_teruskan WHERE ReceiverAs ='to_forward' AND StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                      } else {
                        $hitung_teruskan = $this->db->query("SELECT NId FROM v_tem_teruskan WHERE ReceiverAs ='to_forward' AND StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "' and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                      }

                      if ($this->session->userdata('groupid') == 9) {
                        $hitung_undangan = $this->db->query("SELECT NId FROM v_tem_undangan WHERE StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                      } else {
                        $hitung_undangan = $this->db->query("SELECT NId FROM v_tem_undangan WHERE StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "' and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                      }

                      if ($this->session->userdata('groupid') == 9) {
                        $hitung_tugas = $this->db->query("SELECT NId FROM v_tem_sprint WHERE StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                      } else {
                        $hitung_tugas = $this->db->query("SELECT NId FROM v_tem_sprint WHERE StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                      }

                      // disposisi

                      if ($this->session->userdata('groupid') == 9) {
                        $hitung_disposisi = $this->db->query("SELECT NId FROM v_disposisi WHERE StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "' ")->num_rows();
                      } else {
                        $hitung_disposisi = $this->db->query("SELECT NId FROM v_disposisi WHERE StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                      }


                      //edit tambah nota
                      if ($this->session->userdata('groupid') == 7) {
                        $hitung_nota = $this->db->query("SELECT NId FROM v_suratmasuk WHERE  StatusReceive = 'unread' AND Status = '0' AND NTipe = 'outboxnotadinas' AND ReceiverAs ='to' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                      } else {
                        $hitung_nota = $this->db->query("SELECT NId FROM v_suratmasuk WHERE StatusReceive = 'unread' AND Status = '0' AND NTipe = 'outboxnotadinas' AND ReceiverAs ='to' AND RoleId_To = '" . $this->session->userdata('roleid') . "'")->num_rows();
                      }

                      if ($this->session->userdata('groupid') == 9) {
                        $hitung_keluar = $this->db->query("SELECT NId FROM v_tem_suratdinas WHERE StatusReceive = 'unread' AND To_Id = '" . $this->session->userdata('peopleid') . "' AND RoleId_To = '" . $this->session->userdata('roleid') . "'and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                      } else {
                        $hitung_keluar = $this->db->query("SELECT NId FROM v_tem_suratdinas WHERE StatusReceive = 'unread' AND RoleId_To = '" . $this->session->userdata('roleid') . "' and ReceiverAs not in ('to_draft_keluar')")->num_rows();
                      } ?>

                      <ul>
                        <?php if ($hitung_naskahmasuk > 0) { ?>
                          <li class="header"><a href="<?= BASE_URL('administrator/anri_list_naskahmasuksemua_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_naskahmasuk; ?></b></font> Naskah Masuk </a></li>
                        <?php } ?>

                        <?php if ($hitung_nota > 0) { ?>
                          <li class="header"><a href="<?= BASE_URL('administrator/Anri_list_notakeluar_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_nota; ?></b></font> Nota Masuk</a></li>
                        <?php } ?>

                        <?php if ($hitung_notadinas > 0) { ?>
                          <li class="header"><a href="<?= BASE_URL('administrator/anri_list_notadinas_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_notadinas; ?></b></font> Nota Dinas</a></li>
                        <?php } ?>

                        <?php if ($hitung_disposisi > 0) { ?>
                          <li class="header"><a href="<?= BASE_URL('administrator/anri_list_disposisi_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_disposisi; ?></b></font> Disposisi</a></li>
                        <?php } ?>

                        <?php if ($hitung_teruskan > 0) { ?>
                          <li class="header"><a href="<?= BASE_URL('administrator/anri_list_teruskan_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_teruskan; ?></b></font> Naskah Teruskan</a></li>
                        <?php } ?>

                        <?php if ($hitung_undangan > 0) { ?>
                          <li class="header"><a href="<?= BASE_URL('administrator/anri_list_undangan_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_undangan; ?></b></font> Undangan</a> </li>
                        <?php } ?>

                        <?php if ($hitung_tugas > 0) { ?>
                          <li class="header"><a href="<?= BASE_URL('administrator/anri_list_surattugas_btl') ?>">Anda memiliki <font color="red"><b><?= $hitung_tugas; ?></b></font> Surat Tugas</a></li>
                        <?php } ?>
                      </ul>
                      <hr>
                      <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                          <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="true"><b style="font-size: 16px;">PENGUMUMAN!</b></a></li>
                          <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false"><b style="font-size: 16px;">Fitur Baru!
                                <?php if ($tampil_sofware_update == 1) : ?>
                                  <span class="badge bg-red" style="font-size: 9px;">new</span>
                                <?php endif ?>
                              </b></a></li>
                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab_1">
                            <?php
                            if (!empty($pengumuman)) {
                              foreach ($pengumuman as $ntc => $notice) {
                                if ($notice->priority == 0) {
                                  echo '<b>' . $notice->notice_name . '</b><br>';
                                  echo '<div class="table-responsive"><p>' . $notice->notice_description . '</p></div><hr>';
                                }
                              }
                            }
                            ?>


                          </div>
                          <!-- /.tab-pane -->
                          <div class="tab-pane" id="tab_2">
                            <div></div>
                            <table class="table" style="padding: 0; margin: 0; border-collapse: collapse;">
                              <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Keterangan</th>
                                  <th>Tanggal</th>
                                  <th>Status</th>
                                </tr>
                              </thead>
                              <tbody>

                                <?php
                                if (!empty($pengumuman)) {
                                  $i = 1;
                                  foreach ($pengumuman as $ntc => $notice2) {

                                    if ($notice2->priority == 1) {
                                      echo '<tr>';
                                      echo '<td>' . $i . '</td>';
                                      echo '<td> ' . $notice2->notice_name . '</td>';
                                      echo '<td> ' . date("d/m/Y", strtotime($notice2->tgl_awal)) . ' - ' . date("d/m/Y", strtotime($notice2->tgl_akhir)) . '</td>';
                                      echo '<td>' . '<span class="badge bg-green">New Update</span>' . '</td>';
                                      echo '</tr>';
                                    } elseif ($notice2->priority == 2) {
                                      echo '<tr>';
                                      echo '<td>' . $i . '</td>';
                                      echo '<td> ' . $notice2->notice_name . '</td>';
                                      echo '<td> ' . date("d/m/Y", strtotime($notice2->tgl_awal)) . ' - ' . date("d/m/Y", strtotime($notice2->tgl_akhir)) . '</td>';
                                      echo '<td>' . '<span class="badge bg-yellow">Maintenance</span>' . '</td>';
                                      echo '</tr>';
                                    } elseif ($notice2->priority == 3) {
                                      echo '<tr>';
                                      echo '<td>' . $i . '</td>';
                                      echo '<td> ' . $notice2->notice_name . '</td>';
                                      echo '<td> ' . date("d/m/Y", strtotime($notice2->tgl_awal)) . ' - ' . date("d/m/Y", strtotime($notice2->tgl_akhir)) . '</td>';
                                      echo '<td>' . '<span class="badge bg-blue">Update</span>' . '</td>';
                                      echo '</tr>';
                                    }
                                    $i++;
                                  }
                                }

                                ?>

                              </tbody>
                            </table>
                          </div>
                        </div>
                        <!-- /.tab-content -->
                      </div>
                      <!-- nav-tabs-custom -->

                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-right" data-dismiss="modal">Tutup</button>
                    </div>
                  </div>
                </div>
              </div>
</section>