<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<!-- File untuk konfigurasi fine-upload -->
<?php $this->load->view('core_template/fine_upload'); ?>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<style>
    .select2-container--default .select2-selection--single {
        border: 0px;
    }

    .select2-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 2px 0px !important;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">Pencatatan Naskah Masuk</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pencatatan Naskah Masuk</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>
                        <?= form_open_multipart(BASE_URL('administrator/anri_reg_naskah_masuk/naskah_masuk_add_save'), ['name'    => 'form_inbox', 'class'   => 'form-horizontal', 'id'      => 'form_inbox', 'method'  => 'POST']); ?>
                        <div class="form-group ">
                            <label for="NTglReg" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-8">
                                    <input type="text" class="form-control pull-right" disabled name="NTglReg" id="NTglReg" value="<?= date('d-m-Y') ?>">
                                </div>
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <!-- penambahan -->
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Naskah<sup class="text-danger">*</sup></label>
                            <div class="col-sm-3">
                                <select name="JenisId" class="form-control select2" required>
                                    <option value="">-- Pilih --</option>
                                    <?php
                                    $query = $this->db->query("SELECT * FROM master_jnaskah ORDER BY JenisName ASC")->result();
                                    foreach ($query as $row) {
                                    ?>
                                        <option value="<?= $row->JenisId; ?>"><?= $row->JenisName; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <label class="col-sm-2 control-label">Naskah Lainnya</label>
                            <div class="col-md-3">
                                <select name="JenisIdg" class="form-control select2" required>
                                    <option value="none" required autofocus>-- Pilih --</option>
                                    <?php
                                    $query = $this->db->query("SELECT * FROM master_jnaskah_gub ORDER BY JenisNameg ASC")->result();
                                    foreach ($query as $row) {
                                    ?>
                                        <option value="<?= $row->JenisIdg; ?>"><?= $row->JenisNameg; ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <!-- akhir penambahan -->

                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Tanggal Naskah<sup class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-8">
                                    <input type="text" class="form-control pull-right datepicker" required name="Tgl" id="Tgl" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Nomor" class="col-sm-2 control-label">Nomor Naskah
                                <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Nomor" id="Nomor" placeholder="Nomor Naskah" value="<?= set_value('Nomor'); ?>" required autofocus>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Nomor Agenda</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="NAgenda" id="NAgenda" placeholder="Nomor Agenda" value="<?= set_value('NAgenda'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Hal" class="col-sm-2 control-label">Hal <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Hal" id="Hal" required placeholder="Hal" value="<?= set_value('Hal'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Namapengirim" class="col-sm-2 control-label">Asal Naskah <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Instansipengirim" required id="Instansipengirim" placeholder="Asal Naskah" value="<?= set_value('Instansipengirim'); ?>">
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="UrgensiId" class="col-sm-2 control-label">Tingkat Urgensi <sup class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <select name="UrgensiId" class="form-control select2" id="UrgensiId" required>
                                    <option value="">-- Pilih --</option>
                                    <?php
                                    $urg = $this->db->query("SELECT * FROM master_urgensi ORDER BY UrgensiName ASC")->result();
                                    foreach ($urg as $data_urg) {
                                    ?>
                                        <option value="<?= $data_urg->UrgensiId; ?>"><?= $data_urg->UrgensiName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="SifatId" class="col-sm-2 control-label">Sifat Naskah <sup class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <select name="SifatId" class="form-control select2" required id="SifatId">
                                    <option value="">-- Pilih --</option>
                                    <?php
                                    $sifat = $this->db->query("SELECT * FROM master_sifat ORDER BY SifatName ASC")->result();
                                    foreach ($sifat as $data_sifat) {
                                    ?>
                                        <option value="<?= $data_sifat->SifatId; ?>"><?= $data_sifat->SifatName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Kepada_Yth" class="col-sm-2 control-label">Kepada Yth <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <select name="Kepada_Yth[]" class="form-control select2" required multiple tabi-ndex="5" id="Kepada_Yth">
                                    <option value="">-- Pilih User --</option>
                                    <!-- Perbaikan Eko 09-11-2019 -->
                                    <?php
                                    $query = $this->db->query("SELECT * FROM v_login WHERE RoleCode = " . $this->session->userdata('rolecode') . " AND PeopleId != " . $this->session->userdata('peopleid') . " AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4') ORDER BY PeopleId ASC")->result();
                                    foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                    }
                                    ?>
                                    <!-- batas akhir perbaikan -->
                                </select>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="Tembusan" class="col-sm-2 control-label">Tembusan</label>
                            <div class="col-sm-8">
                                <select name="Tembusan[]" class="form-control select2" multiple tabi-ndex="5" id="Tembusan">
                                    <!-- Perbaikan Eko 09-11-2019 -->
                                    <?php
                                    $query = $this->db->query("SELECT * FROM v_login WHERE RoleCode = " . $this->session->userdata('rolecode') . " AND PeopleId != " . $this->session->userdata('peopleid') . " AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4')  ORDER BY PeopleId ASC")->result();
                                    foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                    }
                                    ?>
                                    <!-- batas akhir perbaikan -->
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="BerkasId" class="col-sm-2 control-label">Pilih Berkas</label>
                            <div class="col-md-4">
                                <select name="BerkasId" id="BerkasId" class="form-control select2">
                                    <option value=""> -- Pilih Berkas -- </option>
                                </select>
                            </div>
                            <div class="col-md-3">
                                <a id="refresh" class="btn btn-default"><i class="fa fa-refresh"></i></a>
                                <a target="blank_" href="<?= site_url('administrator/anri_berkas_unit/berkas_unit_add') ?>" class="btn btn-success"><i class="fa fa-plus"></i> Buat Berkas</a>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="title" class="col-sm-2 control-label">Unggah Data</label>
                            <div class="col-sm-8">
                                <div id="test_title_galery"></div>
                                <div id="test_title_galery_listed"></div>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <br>
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <br>
                            <button class="btn btn-danger">Simpan dan Kirim</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->

<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
    $("#Kepada_Yth").on("select2:select select2:unselect", function(e) {
        var items = $(this).val();
        var ip = e.params.data.id;
        var tembusan = $("#Tembusan").val();

        $.each(tembusan, function(i, item) {
            $.each(items, function(k, item2) {
                if (item == item2) {
                    items.splice(k, 1);
                    $("#Kepada_Yth").val(items).change();
                    return false;
                }
            })
        })
    });

    $("#Tembusan").on("select2:select select2:unselect", function(e) {
        var items = $(this).val();
        var ip = e.params.data.id;
        var tujuan = $("#Kepada_Yth").val();

        $.each(tujuan, function(i, item) {
            $.each(items, function(k, item2) {
                if (item == item2) {
                    items.splice(k, 1);
                    $("#Tembusan").val(items).change();
                    return false;
                }
            })
        })
    });

    $(document).ready(function() {

        $('#form_inbox').on('submit', function(e) {
            e.preventDefault();
            $('#btn-submit-naskah').attr('disabled');

            $.ajax({
                url: '<?php echo site_url('administrator/anri_reg_naskah_masuk/check_nomor'); ?>',
                method: 'POST',
                data: {
                    nomor: $('#Nomor').val()
                },
                success: function(res) {
                    $('#btn-submit-naskah').removeAttr('disabled');
                    if (res == 'true') {
                        alert('Nomor sudah digunakan');
                    } else {
                        $('#form_inbox').unbind('submit');
                        $('#form_inbox').submit();
                    }
                }
            });
        });

        var getajax = $.ajax({
            url: '<?php echo site_url('administrator/anri_reg_naskah_masuk/get_berkas/0'); ?>',
            method: 'GET',
            success: function(res) {
                var BerkasId = $("#BerkasId");
                var json = JSON.parse(res);
                for (let index = 0; index < json.length; index++) {
                    BerkasId.append('<option value=' + json[index].BerkasId + '>' + json[index].Klasifikasi + '/' + json[index].BerkasNumber + '/' + json[index].tahun_cipta + ' - ' + json[index].BerkasName + '</option>');
                }
            },
            error: function(e) {
                console.log(e);
            }
        });

        $('#refresh').on('click', function() {
            $('#BerkasId').empty();
            $.ajax({
                url: '<?php echo site_url('administrator/anri_reg_naskah_masuk/get_berkas/0'); ?>',
                method: 'GET',
                success: function(res) {
                    var BerkasId = $("#BerkasId");
                    var json = JSON.parse(res);
                    for (let index = 0; index < json.length; index++) {
                        BerkasId.append('<option value=' + json[index].BerkasId + '>' + json[index].Klasifikasi + '/' + json[index].BerkasNumber + '/' + json[index].tahun_cipta + ' - ' + json[index].BerkasName + '</option>');
                    }
                },
                error: function(e) {
                    console.log(e);
                }
            });
        })

        $('.select2').select2();

        var params = {};
        params[csrf] = token;
        $('#test_title_galery').fineUploader({
            template: 'qq-template-gallery',
            request: {
                endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
                params: params
            },
            deleteFile: {
                enabled: true,
                endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
            },
            thumbnails: {
                placeholders: {
                    waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                    notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
                }
            },
            validation: {
                allowedExtensions: ["*"],
                sizeLimit: 0,

            },
            showMessage: function(msg) {
                toastr['error'](msg);
            },
            callbacks: {
                onComplete: function(id, name, xhr) {
                    if (xhr.success) {
                        var uuid = $('#test_title_galery').fineUploader('getUuid', id);
                        $('#test_title_galery_listed').append('<input type="hidden" class="listed_file_uuid" name="test_title_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="test_title_name[' + id + ']" value="' + xhr.uploadName + '" />');
                    } else {
                        toastr['error'](xhr.error);
                    }
                },
                onDeleteComplete: function(id, xhr, isError) {
                    if (isError == false) {
                        $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid[' + id + ']"]').remove();
                        $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name[' + id + ']"]').remove();
                    }
                }
            }
        }); /*end title galery*/
    }); /*end doc ready*/
</script>