<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a  href="<?= site_url('administrator/anri_master_tperkembangan'); ?>">Pengaturan Tingkat Perkembangan</a></li>
        <li class="active">Tambah Data</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Tambah Data</h3>
                            <h5 class="widget-user-desc">Tingkat Perkembangan</h5>
                            <hr>
                        </div>
                        <?= form_open(BASE_URL('administrator/anri_master_tperkembangan/add_save')); ?>
                         
                        <div class="form-group ">
                            <label for="TPName" class="col-sm-2 control-label">Tingkat Perkembangan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="TPName" maxlength="50" id="TPName" placeholder="Tingkat Perkembangan" value="<?= set_value('TPName'); ?>" required autofocus>
                                <small class="info help-block">
                                <b>Input Tingkat Perkembangan</b> Max Length : 50.</small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7"><br>
                           <input type="submit" name="submit" value="Simpan" class="btn btn-flat btn-danger">
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
