<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Tingkat Perkembangan</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                  <form name="form_master_tperkembangan" id="form_master_tperkembangan" action="<?= base_url('administrator/anri_master_tperkembangan'); ?>">
                     <div class="row pull-right">
                        <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="Tambah Data" href="<?=  site_url('administrator/anri_master_tperkembangan/add'); ?>"><i class="fa fa-plus-square-o" ></i> Tambah Data</a>
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Pengaturan Tingkat Perkembangan</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <input type="hidden" name="bulk" id="bulk" value="delete">
                  <div class="box-body">
                    <table id="example" class="table table-bordered table-striped table-hover">
                      <thead>
                      <tr>
                        <th>
                          <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                        </th>
                        <th>Tingkat Perkembangan</th>
                        <th>Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      <?php
                      $master_tperkembangan = $this->db->get('master_tperkembangan')->result();
                      foreach ($master_tperkembangan as $tperkembangan):
                      ?>
                      <tr>
                        <td width="5">
                          <input type="checkbox" class="flat-red check" name="id[]" value="<?= $tperkembangan->TPId; ?>">
                        </td>
                        <td><?=$tperkembangan->TPName;?></td>
                        <td width="150">
                              <a href="<?= site_url('administrator/anri_master_tperkembangan/update/' . $tperkembangan->TPId); ?>" class=" btn-primary btn-sm btn" title="Ubah Data"><i class="fa fa-edit "></i></a>
                              <?php
                              if (cek_data('inbox','TPId',$tperkembangan->TPId) == '0') {
                              ?>
                              <a href="javascript:void(0);" data-href="<?= site_url('administrator/anri_master_tperkembangan/delete/' . $tperkembangan->TPId); ?>" class="btn-danger btn-sm btn remove-data"><i  title="Hapus Data"class="fa fa-trash"></i></a>
                              <?php  
                              }
                              ?>
                        </td>
                      </tr>
                      <?php endforeach; ?>
                      </tbody>
                    </table>
                  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
  $(document).ready(function(){

    $('#example').DataTable( {
        "order": [[ 1, "asc" ]]
    } );
       
  }); /*end doc ready*/     
  
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });

</script>