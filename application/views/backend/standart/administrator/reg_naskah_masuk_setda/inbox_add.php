<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<!-- File untuk konfigurasi fine-upload -->
<?php $this->load->view('core_template/fine_upload'); ?>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<style>
    .select2-container--default .select2-selection--single{border:0px;}
    .select2-container{
            border: 1px solid #DEDCDC!important;
            border-radius: 5px!important;
            padding: 2px 0px!important;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">Registrasi Naskah Masuk</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Registrasi Naskah Masuk Setda</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>
                        <?= form_open_multipart(BASE_URL('administrator/anri_reg_naskah_masuk_setda/naskah_masuk_add_save'), ['name'    => 'form_inbox', 'class'   => 'form-horizontal', 'id'      => 'form_inbox', 'method'  => 'POST']); ?>
                        <div class="form-group ">
                            <label for="NTglReg" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right" disabled name="NTglReg" id="NTglReg" value="<?= date('d-m-Y') ?>">
                                </div>
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Naskah<sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-4">
                                    <select name="JenisId" class="form-control select2" required>
                                        <option value="">-- Pilih --</option>
                                        <?php
                                            $query = $this->db->query("SELECT * FROM master_jnaskah ORDER BY JenisName ASC")->result();
                                            foreach ($query as $row) {
                                        ?>
                                            <option value="<?= $row->JenisId; ?>"><?= $row->JenisName; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Tanggal Naskah<sup class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right datepicker" required name="Tgl" id="Tgl" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Nomor" class="col-sm-2 control-label">Nomor Naskah
                            <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Nomor" id="Nomor" placeholder="Nomor Naskah" value="<?= set_value('Nomor'); ?>" required autofocus>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
						
						
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Nomor Urut</label>
                            <div class="col-sm-8">
					<input type="text" class="form-control" name="no_urut" id="no_urut" placeholder="Nomor Urut" value="<?= $no_urut; ?>" readonly>
                                
								<small class="info help-block">
                                </small>
                            </div>
                        </div>
						
                        <div class="form-group ">
                            <label for="Hal" class="col-sm-2 control-label">Hal <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Hal" id="Hal" required placeholder="Hal" value="">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Namapengirim" class="col-sm-2 control-label">Asal Naskah  <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Instansipengirim" required id="Instansipengirim" placeholder="Asal Naskah" value="<?= set_value('Instansipengirim'); ?>">
                                <small class="info help-block"></small>
                            </div>
                        </div>
						
						<div class="form-group ">
                            <label for="SifatId" class="col-sm-2 control-label">Sifat Naskah <sup class="text-danger">*</sup></label>
                            <div class="col-sm-4">
                                <select name="SifatId" class="form-control select2" required id="SifatId">
                                    <option value="">-- Pilih --</option>
                                    <?php
                                        $sifat = $this->db->query("SELECT * FROM master_sifat ORDER BY SifatName ASC")->result();
                                        foreach ($sifat as $data_sifat){
                                    ?>
                                        <option value="<?= $data_sifat->SifatId; ?>"><?= $data_sifat->SifatName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div> 
                     
						<div class="form-group ">
                            <label class="col-sm-2 control-label">Kontak Person</label>
                            <div class="col-sm-8">
   <input type="text" class="form-control" name="cp" id="cp" placeholder="Kontak Person" value="<?= set_value('cp');?>">
                                
								<small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <br>
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <br>
                            <button class="btn btn-danger">Simpan</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>

		$(document).ready(function(){        
		$('.select2').select2();  
    }); /*end doc ready*/

</script>