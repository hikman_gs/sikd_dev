<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<style>
.select2-container--default .select2-selection--single {
    border: 0px;
}

.select2-container {
    border: 1px solid #DEDCDC !important;
    border-radius: 5px !important;
    padding: 2px 0px !important;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li>
            <a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a>
        </li>

        <li>
            <?php 

          $x = $_GET['dt'];
          if($x == '1') { ?>

            <a href="<?= site_url('administrator/anri_list_naskah_masuk'); ?>">Daftar Semua Naskah Masuk</a>

            <?php } elseif($x == '2') { ?>

            <a href="<?= site_url('administrator/anri_list_naskahmasuk_btl'); ?>">Daftar Naskah Masuk Belum
                Tindaklanjut</a>

            <?php } elseif($x == '3') { ?>

            <a href="<?= site_url('administrator/anri_list_disposisi_btl'); ?>">Daftar Disposisi Belum Tindaklanjut</a>

            <?php } elseif($x == '4') { ?>

            <a href="<?= site_url('administrator/anri_list_disposisi'); ?>">Daftar Semua Disposisi</a>

            <?php } elseif($x == '5') { ?>

            <a href="<?= site_url('administrator/anri_list_teruskan_btl'); ?>">Daftar Naskah Teruskan Belum
                Tindaklanjut</a>

            <?php } elseif($x == '6') { ?>

            <a href="<?= site_url('administrator/anri_list_teruskan'); ?>">Daftar Semua Naskah Teruskan</a>

            <?php } elseif($x == '7') { ?>

            <a href="<?= site_url('administrator/anri_list_notadinas_btl_ap'); ?>">Daftar Nota Dinas Belum Disetujui</a>

            <?php } elseif($x == '8') { ?>

            <a href="<?= site_url('administrator/anri_list_notadinas_sdh_ap'); ?>">Daftar Nota Dinas Belum Dikirim</a>

            <?php } elseif($x == '9') { ?>

            <a
                href="<?= site_url('administrator/anri_log_naskah_masuk/view_log_naskah_histori/log/' . $NId .'?dt=9'); ?>">Detail
                Naskah</a>

            <?php } elseif($x == '10') { ?>

            <a href="<?= site_url('administrator/anri_list_notadinas_btl'); ?>">Daftar Nota Dinas Belum Tindaklanjut</a>

            <?php } elseif($x == '11') { ?>

            <a href="<?= site_url('administrator/anri_list_notadinas'); ?>">Daftar Semua Nota Dinas</a>

            <?php } elseif($x == '12') { ?>

            <a href="<?= site_url('administrator/anri_list_undangan_btl'); ?>">Daftar Undangan Belum Tindaklanjut</a>

            <?php } elseif($x == '13') { ?>

            <a href="<?= site_url('administrator/anri_list_undangan'); ?>">Daftar Semua Undangan</a>

            <?php } elseif($x == '14') { ?>

            <a href="<?= site_url('administrator/anri_list_surattugas_btl'); ?>">Daftar Surat Tugas Belum
                Tindaklanjut</a>

            <?php } elseif($x == '15') { ?>

            <a href="<?= site_url('administrator/anri_list_surattugas'); ?>">Daftar Semua Surat Tugas</a>

            <?php } elseif($x == '16') { ?>

            <a href="<?= site_url('administrator/anri_list_suratdinas_keluar_btl'); ?>">Daftar Surat Dinas Belum
                Tindaklanjut</a>

            <?php } elseif($x == '17') { ?>

            <a href="<?= site_url('administrator/anri_list_suratdinas_keluar'); ?>">Daftar Semua Surat Dinas</a>

            <?php } elseif($x == '18') { ?>

            <a href="<?= site_url('administrator/anri_list_naskahdinas_lain_btl'); ?>">Daftar Naskah Dinas Lainnya Belum
                Tindaklanjut</a>

            <?php } elseif($x == '19') { ?>

            <a href="<?= site_url('administrator/anri_list_naskahdinas_lain'); ?>">Daftar Semua Naskah Dinas Lainnya</a>

            <?php } else { ?>

            <a href="<?= site_url('administrator/anri_list_naskah_masuk'); ?>"><?= $x; ?></a>

            <?php } ?>
        </li>

        <li class="active">Ubah Metadata</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Ubah Metadata</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>
                        <?= form_open_multipart(BASE_URL('administrator/anri_reg_naskah_masuk_setda/post_naskah_masuk_edit/'.$tipe.'/'.$data->NId), ['name'    => 'form_inbox', 'class'   => 'form-horizontal', 'id'      => 'form_inbox', 'method'  => 'POST']); ?>
                        <div class="form-group ">
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-12">
                                    <input type="hidden" class="form-control pull-right" name="xdt" id="xdt"
                                        value="<?= $x; ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="NTglReg" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right" disabled name="NTglReg"
                                        id="NTglReg" value="<?= date('d-m-Y',strtotime($data->NTglReg)) ?>">
                                </div>
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Naskah <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-4">
                                    <select name="JenisId" class="form-control select2" required id="">
                                        <?php
                                            $query = $this->db->query("SELECT * FROM master_jnaskah ORDER BY JenisName ASC")->result();
                                            foreach ($query as $row) {
                                        ?>
                                        <option <?= ($data->JenisId == $row->JenisId ? 'selected' : '') ?>
                                            value="<?= $row->JenisId; ?>"><?= $row->JenisName; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Tanggal Naskah <sup
                                    class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-2">
                                    <input type="text" class="form-control pull-right datepicker" name="Tgl" id="Tgl"
                                        value="<?= date('Y-m-d',strtotime($data->Tgl)) ?>" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Nomor" class="col-sm-2 control-label">Nomor Naskah <sup
                                    class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Nomor" id="Nomor" placeholder="Nomor"
                                    value="<?= $data->Nomor; ?>" autofocus required>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Nomor Urut</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="no_urut" id="no_urut"
                                    placeholder="Nomor Urut" value="<?= $data->no_urut; ?>" readonly>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Hal" class="col-sm-2 control-label">Hal <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Hal" id="Hal" placeholder="Hal"
                                    value="<?= $data->Hal; ?>" required>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Instansipengirim" class="col-sm-2 control-label">Asal Naskah <sup
                                    class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Instansipengirim" id="Instansipengirim"
                                    placeholder="Asal Naskah" value="<?= $data->Instansipengirim; ?>" required>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="SifatId" class="col-sm-2 control-label">Sifat Naskah <sup
                                    class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <select name="SifatId" class="form-control select2" id="SifatId" required>
                                    <?php
                                        $sifat = $this->db->query("SELECT * FROM master_sifat ORDER BY SifatName ASC")->result();
                                        foreach ($sifat as $data_sifat){
                                    ?>
                                    <option <?= ($data->SifatId == $data_sifat->SifatId ? 'selected' : '') ?>
                                        value="<?= $data_sifat->SifatId; ?>"><?= $data_sifat->SifatName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <br>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Kontak Person</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="cp" id="cp" placeholder="Kontak Person"
                                    value="<?= $data->cp; ?>">

                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <br>
                            <?php if ($tipe == 'log') { ?>
                            <a href="<?= BASE_URL('administrator/anri_log_naskah_masuk_setda') ?>"
                                class="btn btn-success">Kembali</a>
                            <?php }else{ ?>
                            <a href="<?= BASE_URL('administrator/anri_log_naskah_masuk_setda') ?>"
                                class="btn btn-success">Kembali</a>
                            <?php } ?>
                            <button class="btn btn-danger">Simpan</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>
<script>
$(document).ready(function() {
            $('.select2').select2();
        };
</script>