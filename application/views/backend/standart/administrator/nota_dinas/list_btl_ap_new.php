<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <?php
      $x = '7';
   ?>
   <ol class="breadcrumb">
      <li>
        <a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a>
      </li>
      <li class="active"><?= $title; ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Daftar Nota Dinas Belum Disetujui</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>

                  </div>

                      <div class="table-responsive"> 
                        <table id="example1" class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>No</th>
                                 <th>Jenis Naskah</th>
                                 <th>Nomor</th>
                                 <th>Tanggal</th>
                                 <th>Tujuan</th>
                                 <th>Hal</th>
                                 <th width="5%">Lampiran</th>
                                 <th width="7%">#</th>
                                 <th width="7%">Aksi</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_inbox">
                           <?php 
                           $konsep = $this->db->query("SELECT * FROM konsep_naskah WHERE Approve_People = '".$this->session->userdata('peopleid')."' AND Konsep = '1' AND Ket = 'nadin'")->result();
                           foreach($konsep as $k => $data): ?>
                              <tr>
                                 <td><?= $k+1; ?></td>
                                 <td><?= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$data->JenisId."'")->row()->JenisName ?></td>
                                 <td><?= $data->nosurat ?></td>
                                 <td><?= date('d-m-Y',strtotime($data->TglReg)) ?></td>
                                 <td>

                                  <?php
                                    $exp = explode(',', $data->RoleId_To);
                                    if (count($exp) > 1) {
                                      echo '<ul>';
                                      foreach ($exp as $k => $v) {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
                                        echo '<li>'.$xx1.'</li>';
                                      }
                                      echo '</ul>';
                                    }else{
                                      $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$exp[0]."'")->row()->RoleName;
                                      echo $xx1;
                                    }

                                    $exp = explode(',', $data->RoleId_Cc);
                                    if (count($exp) > 1) {
                                      echo '<br><br><b>Tembusan :</b> <br><ul>';
                                      foreach ($exp as $k => $v) {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
                                        echo '<li>'.$xx1.'</li>';
                                      }
                                      echo '</ul>';
                                    }else{
                                      if($data->RoleId_Cc != '') {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$exp[0]."'")->row()->RoleName;
                                        echo '<br><br><b>Tembusan :</b> <br>'.$xx1;
                                      }  
                                    }

                                  ?>
                               
                                 </td>
                                 <td><?= $data->Hal ?></td>
                                 <td>
                                    <?php
                                        //Untuk nota dinas tindaklanjut nid inbox files ambil data gir id pada konsep naskah dan gir id inbox files ambil data nid temp pada konsep naskah
                                        $query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$data->GIR_Id."' AND GIR_Id = '".$data->NId_Temp."'");

                                        $files = BASE_URL.'/FilesUploaded/naskah/';

                                        foreach($query->result_array() as $row):

                                          echo '<center><a href="'.$files.$row['FileName_fake'].'" target="_new" title="'.$row['FileName_fake'].'" class="btn btn-danger btn-sm"><i class="fa fa-download"></i></a><br><br></center>';
                                        endforeach;   
                                    ?>                                  
                                 </td>                                  
                                 <td>
                                    <?php if ($data->Konsep == 1) { ?>
                                      <a title="Menyetujui" href="#" data-nid="<?= $data->NId_Temp; ?>" data-qrcode="<?= base_url('FilesUploaded/qrcode/'.$data->NId_Temp) ?>" class="btn btn-success btn-sm verifikasi"><i class="fa fa-check"></i></a>
                                      <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/nota_dinas_tindaklanjut_pdf/'.$data->NId_Temp) ?>" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                    <?php  } ?>                                 
                                 </td>
                                 <td>                                 
                                  <?php if ($data->Konsep == 1) { ?>
                                  <a href="<?= base_url('administrator/anri_mail_tl/view_naskah_dinas_tindaklanjut_edit/view/'.$data->NId_Temp.'?dt='.$x) ?>" title="Ubah Data" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                  <a href="#" data-temp="<?= $data->NId_Temp; ?>" data-girid="<?= $data->GIR_Id; ?>" title="Hapus Data" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash"></i></a>
                                  <?php } ?>
                                </td>
                              </tr>
                            <?php endforeach; ?>
                           </tbody>
                        </table>
                      </div>

               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="approve" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Verifikasi Akun</h4>
          </div>
          <div class="modal-body">
            <div class="body-qrcode"></div>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- /.content -->
<script>
  $('.verifikasi').click(function(){
    $('.hapus-form').remove();

    $('#approve').modal('toggle');
    var img = $(this).data('qrcode');
    var nid = $(this).data('nid');
    var append = '<form action="<?= base_url('administrator/anri_mail_tl/verifikasi/'); ?>'+nid+'" class="form-horizontal hapus-form" method="POST">'+
              '<div class="form-group">'+
                '<div class="col-md-12">'+
                  '<input type="password" class="form-control" placeholder="Masukan Kode Verifikasi" name="password">'+
                '</div>'+
              '</div>'+
              '<div class="form-group">'+
                '<div class="col-md-12">'+
                  '<button class="btn btn-success">Setujui</button>'+
                '</div>'+
              '</div>'+
            '</form>';
    $('.body-qrcode').append(append);
  });

  $('.btn-delete').click(function(){
  var nid = $(this).data('temp');
    swal({
          title: "Apakah Data Akan Dihapus ??",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
    .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "<?=BASE_URL('administrator/anri_mail_tl/hapus_naskah_dinas_tindaklanjut2/'); ?>'+nid+'", 
                  type: "POST",
                  data:{
                      temp: $(this).data('temp'),
                      girid: $(this).data('girid'),
                  },
                  success: function(response){
                    swal({
                      title: "Data berhasil dihapus",
                      icon: "success",
                    }).then((result) => {
                      window.location.href = "<?=BASE_URL('administrator/anri_list_notadinas_btl_ap')?>";
                    });
                  },
                  error: function(){
                    swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
                  }
            });
          }
        });
    });
</script>