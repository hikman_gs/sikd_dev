<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <?php
      $x = '8';
   ?>
   <ol class="breadcrumb">
      <li>
        <a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a>
      </li>
      <li class="active"><?= $title; ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Daftar Nota Dinas Belum Dikirim</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>

                  </div>

                      <div class="table-responsive"> 
                        <table id="example1" class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>No</th>
                                 <th>Jenis Naskah</th>
                                 <th>Nomor</th>
                                 <th>Tanggal</th>
                                 <th>Tujuan</th>
                                 <th>Hal</th>
                                 <th width="8%">Nota Dinas</th>
                                 <th width="8%">Aksi</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_inbox">
                           <?php 
                           $konsep = $this->db->query("SELECT * FROM konsep_naskah WHERE RoleId_From = '".$this->session->userdata('roleid')."' AND Konsep = '0'")->result();
                           foreach($konsep as $k => $data): ?>
                              <tr>
                                 <td><?= $k+1; ?></td>
                                 <td><?= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$data->JenisId."'")->row()->JenisName ?></td>
                                 <td><?= $data->nosurat ?></td>
                                 <td><?= date('d-m-Y',strtotime($data->TglReg)) ?></td>
                                 <td>

                                  <?php
                                    $exp = explode(',', $data->RoleId_To);
                                    if (count($exp) > 1) {
                                      echo '<ul>';
                                      foreach ($exp as $k => $v) {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
                                        echo '<li>'.$xx1.'</li>';
                                      }
                                      echo '</ul>';
                                    }else{
                                      $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$exp[0]."'")->row()->RoleName;
                                      echo $xx1;
                                    }

                                    $exp = explode(',', $data->RoleId_Cc);
                                    if (count($exp) > 1) {
                                      echo '<br><br><b>Tembusan :</b> <br><ul>';
                                      foreach ($exp as $k => $v) {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$v."'")->row()->RoleName;
                                        echo '<li>'.$xx1.'</li>';
                                      }
                                      echo '</ul>';
                                    }else{
                                      if($data->RoleId_Cc != '') {
                                        $xx1 =  $this->db->query("SELECT RoleName FROM role WHERE RoleId = '".$exp[0]."'")->row()->RoleName;
                                        echo '<br><br><b>Tembusan :</b> <br>'.$xx1;
                                      }  
                                    }

                                  ?>
                               
                                 </td>
                                 <td><?= $data->Hal ?></td>
                                 <td>
                                    <?php  if ($data->Konsep == 0) { ?>
                                      <a title="Kirim Naskah" href="#" data-nid="<?= $data->NId_Temp; ?>" data-girid="<?= $data->GIR_Id; ?>" class="btn btn-danger btn-sm btn-kirim"><i class="fa fa-send"></i></a>

                                        <?php 
                                        
                                            $xq  = $this->db->query("SELECT Keterangan FROM inbox_files WHERE NId = '".$data->GIR_Id."' AND GIR_Id = '".$data->NId_Temp."' AND Keterangan = 'outbox'")->num_rows();
                                            
                                            //Jika Sumber Naskah Dinas inisiatif yang diterima, maka munculkan file naskah dinas nya, dan jika naskah sudah disposisi atau diteruskan jalankan fungsi ini untuk melihat pesan disposisi yang ditulis pada textfield pesan
                                            if($xq > 0) {

                                              $xyz  = $this->db->query("SELECT FileName_real FROM inbox_files WHERE NId = '".$data->GIR_Id."' AND GIR_Id = '".$data->NId_Temp."' AND Keterangan = 'outbox'")->row();
                                              
                                              $files = BASE_URL.'/FilesUploaded/naskah/';

                                              echo '<a href="'.$files.$xyz->FileName_real.'" target="_new" title="Lihat Nota Dinas" class="btn btn-success btn-sm"><i class="fa fa-envelope-o"></i></a>';

                                            } else { ?>

                                              <a target="_blank" href="<?= base_url('administrator/anri_mail_tl/log_naskah_masuk_pdf/'.$data->NId_Temp) ?>" title="Lihat Naskah" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>

                                    <?php } } ?>                                
                                 </td>
                                 <td>                                 
                                    &nbsp;
                                </td>
                              </tr>
                            <?php endforeach; ?>
                           </tbody>
                        </table>
                      </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
<script>
  $('.btn-kirim').click(function(){
    swal({
          title: "Proses pengiriman nota dinas ??",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
    .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "<?=BASE_URL('administrator/anri_mail_tl/naskah_dinas_kirim')?>",
                  type: "POST",
                  data:{
                      nid: $(this).data('nid'),
                      girid: $(this).data('girid'),
                  },
                  success: function(response){
                    swal({
                      title: "Nota dinas berhasil dikirim",
                      icon: "success",
                    }).then((result) => {
                      window.location.href = "<?=BASE_URL('administrator/anri_list_notadinas_sdh_ap')?>";
                    });
                  },
                  error: function(){
                    swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
                  }
            });
          }
        });
    });


</script>