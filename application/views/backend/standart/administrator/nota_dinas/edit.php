<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>

<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/ckeditor4.16/ckeditor.js"></script>
<style>
    textarea {
        height: 350px;
        width: 90px;
    }

    div.card,
    .tox div.card {
        width: 80px;
        background: white;
        border: 1px solid #ccc;
        border-radius: 3px;
        box-shadow: 0 4px 8px 0 rgba(34, 47, 62, .1);
        padding: 8px;
        font-size: 12px;
        font-family: Arial;
    }

    div.card::after,
    .tox div.card::after {
        content: "";
        clear: both;
        display: table;
    }

    div.card h1,
    .tox div.card h1 {
        font-size: 12px;
        font-weight: normal;
        margin: 0 0 8px;
        padding: 0;
        line-height: 15px;
        font-family: Arial;
    }

    div.card p,
    .tox div.card p {
        font-family: Arial;
    }

    div.card img.avatar,
    .tox div.card img.avatar {
        width: 48px;
        height: 48px;
        margin-right: 8px;
        float: left;
    }

    .container {
        width: 100px;
        margin-right: 20px;
    }

    .header img {
        width: 35%;
        float: left;
    }

    .header h2 {
        width: 60%;
        float: left;
        margin-left: 30px;
        font-size: 28px;
        text-align: center;
    }

    .space {
        margin-bottom: 30px;
        margin-top: 30px;
    }

    .select2-container--default .select2-selection--single {
        border: 0px;
    }

    .select2-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 2px 0px !important;
    }

    .chosen-container-single,
    .chosen-container-multi {
        width: 100% !important;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active"><?= $title; ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pencatatan Nota Dinas</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>

                        </div>
                        <form id="myfrm" class="form-horizontal" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                                <div class="col-sm-8">
                                    <?= $data; ?>
                                    <input type="text" disabled class="form-control" value="<?= date('d-m-Y', strtotime($data->TglReg)) ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Pembuat Naskah</label>
                                <div class="col-sm-8">
                                    <input type="text" disabled class="form-control" value="<?= get_data_people('PeopleName', $data->CreateBy); ?>">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="title" class="col-sm-2 control-label">Jenis Naskah</label>
                                <div class="col-sm-8">
                                    <input type="text" disabled class="form-control" value="<?= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $data->JenisId . "'")->row()->JenisName ?>">
                                </div>
                            </div>
                           
                              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Penandatangan <sup class="text-danger">*</sup></label>

                <div class="col-sm-2">

                  <select name="TtdText" class="form-control select2 TtdText" required>
                    <?php foreach ($this->db->query('SELECT * FROM master_ttd')->result() as $data1) { ?>
                      <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                    <?php } ?>
                  </select>
                </div>

                <?php
                $pid = $this->session->userdata('roleatasan');
                $pid1 = 3;
                $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->row();

                ?>
                <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $AtasanId->PeopleId; ?>">

                <?php
                if ($data->TtdText == 'none') {

                  // var_dump($data);
                  // die();
                  ?>
                  <div class="col-md-5 Approve_People <?= ($data->Approve_People == $this->session->userdata('peopleid') ?  : '') ?>">
                  <?php } else { ?>
                    <div class="col-md-5 Approve_People <?= ($data->Approve_People == $this->session->userdata('peopleid')) ?>">
                      <?php
                    }
                    ?>
                    <select name="Approve_People" id="Approve_People" class="form-control" disabled>
                      <?php
                      $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                      foreach ($query as $dat_people) {
                        ?>
                        <option <?= ($data->Approve_People == $dat_people->PeopleId ? 'selected' : '') ?> value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName; ?>-<?= $dat_people->PeoplePosition; ?></option>
                      <?php } ?>

                    </select>
                  </div>
                </div>

                <div class="form-group <?php if (form_error('MeasureUnitId')) {
                    echo 'has-error';
                } ?>">
                <label for="title" class="col-sm-2 control-label">Lampiran</label>

                <div class="col-md-2">


                     <select name="MeasureUnitId" class="form-control" id="MeasureUnitId" required="">
                      <option value=""></option>
                        <?php foreach ($this->db->query('SELECT * FROM master_satuanunit')->result() as $data_mes) { ?>
                      <option <?= ($data->MeasureUnitId == $data_mes->MeasureUnitId ? 'selected' : '') ?> value="<?= $data_mes->MeasureUnitId; ?>"><?= $data_mes->MeasureUnitName; ?></option>
                    <?php } ?>
                    </select>

                     <span class="help-block"><b><?= form_error('MeasureUnitId'); ?></b></span>
                </div>

                     <div class="col-md-2">
                        <input type="text" class="form-control" name="Jumlah" id="Jumlah" min="0" id="Jumlah"  value="<?= $data->Jumlah; ?>" required>
                     </div>
                </div>

                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">Kode Klasifikasi <sup class="text-danger">*</sup></label>
                    <div class="col-sm-6">
                      <select name="ClId" class="form-control select2" required>
                        <?php foreach ($this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result() as $data_class) { ?>
                          <option <?= ($data->ClId == $data_class->ClId ? 'selected' : '') ?> value="<?= $data_class->ClId; ?>"><?= $data_class->ClCode; ?> - <?= $data_class->ClName; ?></option>
                      <?php } ?>
                  </select>
              </div>
          </div>

               <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Unit Pengolah<sup class="text-danger">*</sup></label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="RoleCode" id="RoleCode" value="<?= $data->RoleCode; ?>" placeholder="Unit Pengolah" required="">
                </div>
              </div>

                   <div class="form-group ">
                  <label for="UrgensiId" class="col-sm-2 control-label">Tingkat Urgensi <sup class="text-danger">*</sup></label>
                  <div class="col-sm-2">
                    <select name="UrgensiId" class="form-control select2" id="UrgensiId" required>
                      <?php
                      $urg = $this->db->query("SELECT * FROM master_urgensi ORDER BY UrgensiName ASC")->result();
                      foreach ($urg as $data_urg) {
                      ?>
                        <option <?= ($data->UrgensiId == $data_urg->UrgensiId ? 'selected' : '') ?> value="<?= $data_urg->UrgensiId; ?>"><?= $data_urg->UrgensiName; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Sifat Naskah <sup class="text-danger">*</sup></label>
                  <div class="col-sm-2">
                    <select name="SifatId" class="form-control select2" required>
                      <?php foreach ($this->db->query('SELECT * FROM master_sifat')->result() as $data_sifat) { ?>
                        <option <?= ($data->SifatId == $data_sifat->SifatId ? 'selected' : '') ?> value="<?= $data_sifat->SifatId; ?>"><?= $data_sifat->SifatName; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

                            <div class="form-group <?php if (form_error('Hal')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Hal <sup class="text-danger">*</sup></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="Hal" id="Hal" placeholder="Hal" value="<?= $data->Hal; ?>" required>
                                    <span class="help-block"><b><?= form_error('Hal'); ?></b></span>
                                </div>
                            </div>
                            <div class="form-group <?php if (form_error('Konten')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Konsep Nota Dinas <sup class="text-danger">*</sup></label>
                                <div class="col-sm-8">
                                    <textarea name="Konten" id="Konten" class="form-control ckeditor" cols="30" rows="10" placeholder="Isi"><?= $data->Konten; ?></textarea>
                                    <span class="help-block"><b><?= form_error('Konten'); ?></b></span>
                                </div>
                            </div>

            <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Kepada Yth<sup class="text-danger">*</sup></label>

            <?php

                  $sel = '';
                  $role_array = explode(',', $data->RoleId_To);
                  $sql =  "SELECT * FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId NOT IN ('','-') AND PeopleId IN (";
                  foreach ($role_array as $key => $value) {
                    $sql .= "'" . $value . "'" . ",";
                    $sel .= "'" . $value . "'" . ",";
                  }
                  $sql = substr($sql, 0, -1);
                  $sel = substr($sel, 0, -1);
                  $sql .=  ") AND PeopleIsActive = '1' ORDER BY GroupId , Golongan DESC, Eselon ASC, PrimaryRoleId ASC";

// $this->db->query("SELECT * FROM v_login WHERE PeopleId != ".$this->session->userdata('peopleid')." AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND GRoleId = '".$this->session->userdata('groleid')."' ORDER BY GroupId , Golongan DESC, Eselon ASC")->result();

                  $sql2 ="SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '".$this->session->userdata('rolecode')."' ORDER BY GroupId , Golongan DESC, Eselon ASC, PrimaryRoleId ASC";
                  ?>

                  <div class="col-sm-8">
                    <select name="RoleId_To[]" class="form-control select2" id="RoleId_To" multiple tabi-ndex="5" required>

                       <?php
                      $query1 = $this->db->query($sql)->result();
                      foreach ($query1 as $ft) {
                      ?>
                        <option value='<?= $ft->PeopleId; ?>' selected><?= $ft->PeopleName; ?> -- <?= $ft->PeoplePosition; ?></option>
                      <?php }
                      $query2 = $this->db->query($sql2)->result();
                      foreach ($query2 as $ft) {
                              echo "<option value = " . $ft->PeopleId . ">" . $ft->PeopleName .' -- '.  $ft->PeoplePosition."</option>";
                      }
                      ?>
                    </select>
                  </div>
                </div>                     

            <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tembusan</label>

                <?php

                $sel1 = '';
                $role_array1 = explode(',', $data->RoleId_Cc);

                $sql1 = "SELECT * FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId NOT IN ('','-') AND PrimaryRoleId IN (";

                foreach ($role_array1 as $key => $value1) {
                  $sql1 .= "'" . $value1 . "'" . ",";
                  $sel1 .= "'" . $value1 . "'" . ",";
                }

                $sql1 = substr($sql1, 0, -1);
                $sel1 = substr($sel1, 0, -1);

                $sql1 .= ") AND PeopleIsActive = '1' AND GroupId = '3' AND PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY PrimaryRoleId ASC";



                  $sql2 ="SELECT * FROM v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') AND RoleCode = '".$this->session->userdata('rolecode')."' ORDER BY GroupId , Golongan DESC, Eselon ASC";

                $sql21 = "SELECT PrimaryRoleId, PeopleName, PeoplePosition FROM people WHERE RoleAtasan NOT IN ('','-')  AND PrimaryRoleId NOT IN (" . $sel1 . ") AND PeopleIsActive = '1'  AND PeopleId != " . $this->session->userdata('peopleid') . " AND GroupId = '3' ORDER BY PrimaryRoleId ASC";
                ?>

                <div class="col-sm-8">
                  <select name="RoleId_Cc[]" class="form-control select2" id="RoleId_Cc" multiple tabi-ndex="5">

                    <?php

                    $query11 = $this->db->query($sql1)->result();
                    foreach ($query11 as $ft1) {

                    ?>

                      <option value='<?= $ft1->PrimaryRoleId; ?>' selected><?= $ft1->PeopleName; ?> - <?= $ft1->PeoplePosition; ?></option>

                    <?php }

                    $query21 = $this->db->query($sql21)->result();

                    foreach ($query21 as $ft1) {
                      echo "<option value = " . $ft1->PrimaryRoleId . ">" . $ft1->PeopleName .' -- '.$ft1->PeoplePosition . "</option>";
                    }

                    ?>

                  </select>
                </div>
              </div>

                            <div class="form-group <?php if (form_error('isi_lampiran')) {
                                                        echo 'has-error';
                                                    } ?>">
                                <label for="title" class="col-sm-2 control-label">Isi Lampiran <sup class="text-danger">*</sup></label>
                                <?php
                                $data_lampiran = json_decode($data->lampiran);

                                ?>
                                <div class="col-sm-8 input_fields_wrap">
                                    <?php
                                    foreach ($data_lampiran as $datlamp) {
                                    ?>
                                        <textarea class="ckeditor form-control" name="isi_lampiran[]" id="isi_lampiran"><?= $datlamp; ?></textarea>
                                        <br>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <span class="help-block"><b><?= form_error('Isi Lampiran'); ?></b></span>
                                <button class="add_field_button btn btn-success">Tambah Isi Lampiran</button>

                            </div>

                            <!-- penambahan dispu -->
                           <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">a.n. dan u.b.<sup class="text-danger">*</sup></label>
                  <div class="col-sm-2">

            <!--         <select name="TtdText2" class="form-control select2 TtdText2" required>
                      <?php foreach ($this->db->query('SELECT * FROM master_atas_nama')->result() as $data2) { ?>
                        <option value="<?= $data2->TtdText2; ?>"><?= $data2->NamaText2; ?></option>
                      <?php } ?>
                    </select> -->

                 <!--  <select name="TtdText2" class="form-control select2 TtdText2" required>
                    <?php foreach ($this->db->query('SELECT * FROM master_atas_nama')->result() as $data2) { ?>
                      <option value="<?= $data2->TtdText2; ?>"><?= $data2->NamaText2; ?></option>
                    <?php } ?>
                  </select> -->

                  <select name="TtdText2" class="form-control select2 TtdText2" id="TtdText2">

                      <?php foreach ($this->db->query('SELECT * FROM master_atas_nama')->result() as $data2) { ?>
                        <option value="<?= $data2->TtdText2; ?>" 
                          <?php
                          if ($data2->TtdText2 == $data->TtdText2) {
                            echo 'selected';
                          }
                        ?>><?= $data2->NamaText2; ?></option>
                      <?php } ?>
                      
                    </select> 


                    <span class="help-block"><b><?= form_error('TtdText2'); ?></b></span>
                  </div>

                  <?php
                  $NamaId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = '3' AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
                  foreach ($NamaId as $dataz) {
                    $xx2 = $dataz->PeopleId;
                  }
                  ?>
                <input type="hidden" id="tmpid_atas_nama" name="tmpid_atas_nama" value="<?= $xx2; ?>">

                        <?php
                if ($data2->TtdText2 == 'none') {

             
                  ?>

                  <div class="col-md-5 Approve_People3 <?= ($data2->Approve_People3 == $this->session->userdata('peopleid') ?  : '') ?>">
                  <?php } else { ?>
                    <div class="col-md-5 Approve_People3 <?= ($data2->Approve_People3 == $this->session->userdata('peopleid')) ?>">
                      <?php
                    }
                    ?>

                  <select name="Approve_People3" id="Approve_People3" class="form-control">
                    <!-- Perbaikan DISPU 2020 -->
                    <?php
                    $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                    foreach ($query as $dat_people2) {
                      ?>



                        <option <?= ($data->Approve_People3 == $dat_people2->PeopleId ? 'selected' : '') ?> value="<?= $dat_people2->PeopleId; ?>"><?= $dat_people2->PeopleName; ?>-<?= $dat_people2->PeoplePosition; ?></option>



                    
                      <?php
                    }
                    ?>
                    <!-- batas akhir penambahan -->
                  </select>
                </div>
              </div>


                 <!-- AKHIR ATAS NAMA DAN UNTUK BELIAU -->

                 <div class="form-group ">
                    <label for="title" class="col-sm-2 control-label">Unggah Data </label>
                    <div class="col-sm-8">
                        <div id="test_title_galery"></div>
                        <div id="test_title_galery_listed"></div>
                        <small class="info help-block">
                        </small>
                    </div>
                </div>

                <?php
                $file = $this->db->query("SELECT * FROM inbox_files WHERE NId = '" . $data->NId_Temp . "'")->result();
                if (count($file) > 0) { ?>
                    <div class="form-group">
                      <label for="title" class="col-sm-2 control-label">Data Digital</label>
                      <div class="col-sm-6">
                        <?php foreach ($file as $key => $value) { ?>
                          <div class="col-sm-8">
                            <div class="thumbnail" style="overflow: hidden;">
                              <h6><?= $value->FileName_fake; ?></h6>
                              <a href="#" data-href="<?= base_url('administrator/anri_list_naskah_belum_approve/hapus_file5/' . $data->NId_Temp . '/' . $value->FileName_fake) ?>" class="btn btn-danger btn-block remove-data">Hapus File</a>
                          </div>
                      </div>
                            <?php  } ?>
                        </div>
                        </div>
                    <?php  } ?>

                            <br>
                            <div class="message"></div>
                            <div class="row-fluid col-md-7">
                                <br>
                                <a title="Lihat Naskah" onclick="lihat_naskah();" class="btn btn-flat btn-success btn-lihatnaskah" target="_blan  k">Lihat Konsep Nota
                                    Dinas</a>
                                <button onclick="simpan_naskah();" class="btn btn-danger">Simpan</button>
                            </div>
                        </form>
                         

                          
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>

<!-- /.content -->

<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
    //lampiran
    $(document).ready(function() {
        var max_fields = 10; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        var x = 1; //initlal text box count
        $(add_button).click(function(e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                var editorId = 'editor_' + x;
                $(wrapper).append('<div> <textarea id="' + editorId + '" class="ckeditor" name="isi_lampiran[]"></textarea><a href="#" class="remove_field btn btn-danger">Remove </a></div><br>'); //add input box

                CKEDITOR.replace(editorId, {
                    height: 200
                });
            }
        });

        $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
            e.preventDefault();
            $(this).parent('div').remove();
            x--;
        })
    });

    function get_num_naskah() {
        var pil_ttd = $(".TtdText").val();

        if (pil_ttd == 'none')
            ttd_approve = "<?= $this->session->userdata('roleid'); ?>";
        else if (pil_ttd == 'AL')
            ttd_approve = "<?= $this->session->userdata('roleatasan'); ?>";
        else
            ttd_approve = ""


        alert(ttd_approve);
        $.ajax({
            url: "<?= BASE_URL('administrator/anri_regis_notadinas/get_num_naskah') ?> ",
            type: "post",
            success: function($num) {
                alert($num);
            }
        })
    }


    $("#RoleId_To").on("select2:select select2:unselect", function(e) {
        var items = $(this).val();
        var ip = e.params.data.id;
        var tembusan = $("#RoleId_Cc").val();

        $.each(tembusan, function(i, item) {
            $.each(items, function(k, item2) {
                if (item == item2) {
                    items.splice(k, 1);
                    $("#RoleId_To").val(items).change();
                    return false;
                }
            })
        })
    });

    $("#RoleId_Cc").on("select2:select select2:unselect", function(e) {
        var items = $(this).val();
        var ip = e.params.data.id;
        var tujuan = $("#RoleId_To").val();

        $.each(tujuan, function(i, item) {
            $.each(items, function(k, item2) {
                if (item == item2) {
                    items.splice(k, 1);
                    $("#RoleId_Cc").val(items).change();
                    return false;
                }
            })
        })
    });


    $(' .TtdText').change(function() {
        if ($(this).find(':selected').val() == 'none') {
            $('.Approve_People').addClass('hidden');
        } else {
            $('.Approve_People').removeClass('hidden');
            r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
            $('#Approve_People').select2();
            $('#Approve_People').attr('disabled', false);
            if (this.value == "AL") {
                idatasan = $("#tmpid_atasan").val();
                $('#Approve_People').val(idatasan).change();
                $('#Approve_People').attr('disabled', true);
            }
        }
    });

    $(' .TtdText2').change(function() {
        if ($(this).find(':selected').val() == 'none') {
            $('.Approve_People3').addClass('hidden');
        } else {
            $('.Approve_People3').removeClass('hidden');
            r_nama = "hidden";
            $('#Approve_People3').select2();
            $('#Approve_People3').attr('disabled', false);
            if (this.value == "AL") {
                idnama = $("#tmpid_atas_nama").val();
                $('#Approve_People3').val(idnama).change();
                $('#Approve_People3').attr('disabled', true);
            }
        }
    });
   

   $(document).ready(function() {

     $('#MeasureUnitId').change(function() {
      // alert(',' + $('#MeasureUnitId').find('option:selected').text() + ', ');
      if ($(this).find(':selected').val() == 'XxJyPn38Yh.6') {
        $('#Jumlah').prop("disabled", true);
      } else {
        $('#Jumlah').prop("disabled", false);
      }
    });

    $('.select2').select2();

    var params = {};
    params[csrf] = token;
    $('#test_title_galery').fineUploader({
      template: 'qq-template-gallery',
      request: {
        endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
        params: params
      },
      deleteFile: {
        enabled: true,
        endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
      },
      thumbnails: {
        placeholders: {
          waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
          notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
        }
      },
      validation: {
        allowedExtensions: ["*"],
        sizeLimit: 0,

      },
      showMessage: function(msg) {
        toastr['error'](msg);
      },
      callbacks: {
        onComplete: function(id, name, xhr) {
          if (xhr.success) {
            var uuid = $('#test_title_galery').fineUploader('getUuid', id);
            $('#test_title_galery_listed').append('<input type="hidden" class="listed_file_uuid" name="test_title_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="test_title_name[' + id + ']" value="' + xhr.uploadName + '" />');
          } else {
            toastr['error'](xhr.error);
          }
        },
        onDeleteComplete: function(id, xhr, isError) {
          if (isError == false) {
            $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid[' + id + ']"]').remove();
            $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name[' + id + ']"]').remove();
          }
        }
      }
    }); /*end title galery*/
  }); /*end doc ready*/

  $('.remove-data').click(function() {
    var url = $(this).attr('data-href');
    swal({
        title: "Anda yakin data akan dihapus?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
          document.location.href = url;
        }
      });

    return false;
  });
</script>

<script>
    function lihat_naskah() {

         if ($("#MeasureUnitId").val() == "") {
          alert("Lampiran Wajib Dipilih, pilih strip (-) jika tidak ada lampiran");
          myfrm.MeasureUnitId.focus();
        } else if ($("#MeasureUnitId").val() != "XxJyPn38Yh.6" && $("#Jumlah").val() == "") {
            alert('Jumlah Wajib diisi');
            myfrm.Jumlah.focus();
        } else if    ($("#ClId").val() == "") {
            alert("Kode Klasifikasi Wajib Diisi");
            myfrm.ClId.focus();
        } else if ($("#RoleCode").val() == "") {
            alert("Unit Pengolah Wajib Diisi");
            myfrm.RoleCode.focus();
        } else if ($("#UrgensiId").val() == "") {
            alert("Tingkat Urgensi Wajib Diisi");
            myfrm.UrgensiId.focus();
        } else if ($("#SifatId").val() == "") {
            alert("Sifat Naskah Wajib Diisi");
            myfrm.SifatId.focus();
        } else if ($("#Hal").val() == "") {
            alert("Uraian Hal Wajib Diisi");
            myfrm.Hal.focus();
        } else if (CKEDITOR.instances.Konten.getData() == "") {
            alert("Konsep Nota Dinas Wajib Diisi");
            myfrm.Konten.focus();
        } else if ($("#RoleId_To").find('option:selected').length == 0) {
            alert("Kepada Yth Wajib Diisi");
            myfrm.RoleId_To.focus();
        } else if ($("#TtdText2").val() == "") {
            alert("a.n. dan u.b.");
            myfrm.TtdText2.focus();
            myfrm.RoleId_To.focus();
        } else {
            $("#myfrm").attr('target', '_blank');
            $("#myfrm").attr('action', "<?= BASE_URL('administrator/anri_regis_notadinas/lihat_naskah_dinas/')?>");
            $("#myfrm").submit();
        }

    }

    function simpan_naskah() {

        if ($("#MeasureUnitId").val() == "") {
          alert("Lampiran Wajib Dipilih, pilih strip (-) jika tidak ada lampiran");
          myfrm.MeasureUnitId.focus();
        } else if ($("#MeasureUnitId").val() != "XxJyPn38Yh.6" && $("#Jumlah").val() == "") {
            alert('Jumlah Wajib diisi');
            myfrm.Jumlah.focus();
        } else if    ($("#ClId").val() == "") {
            alert("Kode Klasifikasi Wajib Diisi");
            myfrm.ClId.focus();
        } else if ($("#RoleCode").val() == "") {
            alert("Unit Pengolah Wajib Diisi");
            myfrm.RoleCode.focus();
        } else if ($("#UrgensiId").val() == "") {
            alert("Tingkat Urgensi Wajib Diisi");
            myfrm.UrgensiId.focus();
        } else if ($("#SifatId").val() == "") {
            alert("Sifat Naskah Wajib Diisi");
            myfrm.SifatId.focus();
        } else if ($("#Hal").val() == "") {
            alert("Uraian Hal Wajib Diisi");
            myfrm.Hal.focus();
        } else if (CKEDITOR.instances.Konten.getData() == "") {
            alert("Konsep Nota Dinas Wajib Diisi");
            myfrm.Konten.focus();
        } else if ($("#RoleId_To").find('option:selected').length == 0) {
            alert("Kepada Yth Wajib Diisi");
            myfrm.RoleId_To.focus();
        } else {
            $("#myfrm").attr('target', '_self');
            $("#myfrm").attr('action', "<?= base_url('administrator/anri_regis_notadinas/postedit_notadinas/' . $data->NId_Temp); ?>");
            $("#myfrm").submit();
        }
    }
</script>