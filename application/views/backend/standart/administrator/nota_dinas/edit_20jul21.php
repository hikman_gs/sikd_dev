<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>

<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<style>
  .select2-container--default .select2-selection--single {
    border: 0px;
  }

  .select2-container {
    border: 1px solid #DEDCDC !important;
    border-radius: 5px !important;
    padding: 2px 0px !important;
  }

  .chosen-container-single,
  .chosen-container-multi {
    width: 100% !important;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>&nbsp;</h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active"><?= $title; ?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-body ">
          <div class="box box-widget widget-user-2">
            <div class="widget-user-header ">
              <div class="widget-user-image">
                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
              </div>
              <h3 class="widget-user-username"><?= $title; ?></h3>
              <h5 class="widget-user-desc">&nbsp;</h5>
              <hr>
            </div>

            <form action="<?= base_url('administrator/anri_list_naskah_belum_approve/postedit_notadinas/' . $data->NId_Temp); ?>" class="form-horizontal" method="POST">
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="<?= date('d-m-Y', strtotime($data->TglReg)) ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Pembuat Naskah</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="<?= get_data_people('PeopleName', $data->CreateBy); ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Jenis Naskah</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="<?= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '" . $data->JenisId . "'")->row()->JenisName ?>">
                </div>
              </div>

              <!-- Jika bukan surat tugas jalankan fungsi ini   -->
              <?php if ($data->Ket != 'outboxsprint') { ?>
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Lampiran</label>
                  <div class="col-md-2">
                    <input type="text" class="form-control" name="Jumlah" value="<?= $data->Jumlah; ?>">
                  </div>
                  <div class="col-md-6">

                    <select name="MeasureUnitId" class="form-control chosen chosen-select">
                      <option value="">----</option>
                      <?php foreach ($this->db->query('SELECT * FROM master_satuanunit')->result() as $data_mes) { ?>
                        <option <?= ($data->MeasureUnitId == $data_mes->MeasureUnitId ? 'selected' : '') ?> value="<?= $data_mes->MeasureUnitId; ?>"><?= $data_mes->MeasureUnitName; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>

               <!--  <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Tembusan</label>

                  <?php

                  $sel1 = '';
                  $role_array1 = explode(',', $data->RoleId_Cc);

                  $sql1 = "SELECT * FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId NOT IN ('','-') AND PrimaryRoleId IN (";

                  foreach ($role_array1 as $key => $value1) {
                    $sql1 .= "'" . $value1 . "'" . ",";
                    $sel1 .= "'" . $value1 . "'" . ",";
                  }

                  $sql1 = substr($sql1, 0, -1);
                  $sel1 = substr($sel1, 0, -1);

                  $sql1 .= ") AND PeopleIsActive = '1' AND GroupId = '3' AND PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY PrimaryRoleId ASC";

                  $sql21 = "SELECT PrimaryRoleId, PeoplePosition FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId NOT IN (" . $sel1 . ") AND PeopleIsActive = '1' AND PeopleId != " . $this->session->userdata('peopleid') . " AND GroupId = '3' ORDER BY PrimaryRoleId ASC";
                  ?>

                  <div class="col-sm-8">
                    <select name="RoleId_Cc[]" class="form-control select2" id="RoleId_Cc" multiple tabi-ndex="5">

                      <?php

                      $query11 = $this->db->query($sql1)->result();
                      foreach ($query11 as $ft1) {

                      ?>

                        <option value='<?= $ft1->PrimaryRoleId; ?>' selected> <?= $ft1->PeoplePosition; ?></option>

                      <?php }

                      $query21 = $this->db->query($sql21)->result();

                      foreach ($query21 as $ft1) {
                        echo "<option value = " . $ft1->PrimaryRoleId . ">" . $ft1->PeoplePosition . "</option>";
                      }

                      ?>

                    </select>
                  </div>
                </div> -->



              <?php } ?>
              <!-- Akhir Jika bukan surat tugas jalankan fungsi ini   -->

              <!-- Tambahan Fazri Nomor Naskah manual 2020 -->
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Unit Pengolah<sup class="text-danger">*</sup></label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="rolecode" id="rolecode" value="<?= $data->RoleCode; ?>" placeholder="Unit Pengolah">
                </div>
              </div>
              <!-- Tambahan Fazri Nomor Naskah manual 2020 -->

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Kode Klasifikasi <sup class="text-danger">*</sup></label>
                <div class="col-sm-6">
                  <select name="ClId" class="form-control select2" required>
                    <?php foreach ($this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result() as $data_class) { ?>
                      <option <?= ($data->ClId == $data_class->ClId ? 'selected' : '') ?> value="<?= $data_class->ClId; ?>"><?= $data_class->ClCode; ?> - <?= $data_class->ClName; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <!-- Jika bukan surat tugas jalankan fungsi ini   -->
              <?php if ($data->Ket != 'outboxsprint') { ?>
                <div class="form-group ">
                  <label for="UrgensiId" class="col-sm-2 control-label">Tingkat Urgensi <sup class="text-danger">*</sup></label>
                  <div class="col-sm-2">
                    <select name="UrgensiId" class="form-control select2" id="UrgensiId" required>
                      <?php
                      $urg = $this->db->query("SELECT * FROM master_urgensi ORDER BY UrgensiName ASC")->result();
                      foreach ($urg as $data_urg) {
                      ?>
                        <option <?= ($data->UrgensiId == $data_urg->UrgensiId ? 'selected' : '') ?> value="<?= $data_urg->UrgensiId; ?>"><?= $data_urg->UrgensiName; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Sifat Naskah <sup class="text-danger">*</sup></label>
                  <div class="col-sm-2">
                    <select name="SifatId" class="form-control select2" required>
                      <?php foreach ($this->db->query('SELECT * FROM master_sifat')->result() as $data_sifat) { ?>
                        <option <?= ($data->SifatId == $data_sifat->SifatId ? 'selected' : '') ?> value="<?= $data_sifat->SifatId; ?>"><?= $data_sifat->SifatName; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Hal <sup class="text-danger">*</sup></label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="Hal" value="<?= $data->Hal ?>" placeholder="Hal" required>
                  </div>
                </div>
              <?php } else { ?>
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Uraian Informasi <sup class="text-danger">*</sup></label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="Hal" value="<?= $data->Hal ?>" placeholder="Uraian Informasi" required>
                  </div>
                </div>
              <?php } ?>
              <!-- Akhir Jika bukan surat tugas jalankan fungsi ini   -->

              <!-- Jika surat tugas jalankan fungsi ini   -->
              <?php if ($data->Ket == 'outboxsprint') { ?>
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Yang Ditugaskan <sup class="text-danger">*</sup></label>

                  <?php

                  $sel = '';
                  $role_array = explode(',', $data->RoleId_To);

                  $sql = "SELECT * FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId IN (";

                  foreach ($role_array as $key => $value) {
                    $sql .= "'" . $value . "'" . ",";
                    $sel .= "'" . $value . "'" . ",";
                  }

                  $sql = substr($sql, 0, -1);
                  $sel = substr($sel, 0, -1);

                  $sql .= ") AND PeopleIsActive = '1' AND PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY PrimaryRoleId ASC";

                  $sql2 = "SELECT PrimaryRoleId, PeoplePosition FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId NOT IN (" . $sel . ") AND PeopleIsActive = '1' AND PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY PrimaryRoleId ASC";
                  ?>

                  <div class="col-sm-8">
                    <select name="RoleId_To[]" class="form-control select2" id="RoleId_To" multiple tabi-ndex="5" required>

                      <?php

                      $query1 = $this->db->query($sql)->result();
                      foreach ($query1 as $ft) {

                      ?>

                        <option value='<?= $ft->PrimaryRoleId; ?>' selected> <?= $ft->PeoplePosition; ?></option>

                      <?php }

                      $query2 = $this->db->query($sql2)->result();

                      foreach ($query2 as $ft) {
                        echo "<option value = " . $ft->PrimaryRoleId . ">" . $ft->PeoplePosition . "</option>";
                      }

                      ?>

                    </select>
                  </div>
                </div>

                <!-- Jika surat dinas jalankan fungsi ini   -->
              <?php } elseif ($data->Ket == 'outboxkeluar') { ?>

                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Tujuan Surat Dinas <sup class="text-danger">*</sup></label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" name="RoleId_To" id="RoleId_To" value="<?= $data->RoleId_To ?>" placeholder="Tujuan Surat Dinas" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Alamat Surat Dinas </label>
                  <div class="col-sm-8">
                    <textarea name="Alamat" class="form-control mytextarea" cols="20" rows="5" placeholder="Alamat Surat Dinas"><?= $data->Alamat ?></textarea>
                  </div>
                </div>

              <?php } elseif ($data->Ket == 'outboxedaran') { ?>

                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Tujuan Surat Edaran <sup class="text-danger">*</sup></label>
                  <div class="col-sm-8">
                    <textarea class="form-control" name="RoleId_To" id="RoleId_To" value="<?= $data->RoleId_To ?>" placeholder="Tujuan Surat Edaran">Tujuan Surat Edaran</textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Alamat Surat Edaran </label>
                  <div class="col-sm-8">
                    <textarea name="Alamat" class="form-control mytextarea" cols="20" rows="5" placeholder="Alamat Surat Edaran"><?= $data->Alamat ?></textarea>
                  </div>
                </div>

              <?php } else { ?>
                <!-- Jika selain surat tugas jalankan fungsi ini   -->
                <div class="form-group">
                  <label for="title" class="col-sm-2 control-label">Kepada Yth <sup class="text-danger">*</sup></label>

                  <?php

                  $sel = '';
                  $role_array = explode(',', $data->RoleId_To);

                  $sql = "SELECT * FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId IN (";

                  foreach ($role_array as $key => $value) {
                    $sql .= "'" . $value . "'" . ",";
                    $sel .= "'" . $value . "'" . ",";
                  }

                  $sql = substr($sql, 0, -1);
                  $sel = substr($sel, 0, -1);

                  $sql .= ") AND PeopleIsActive = '1' AND GroupId IN ('3','4') AND PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY PrimaryRoleId ASC";

                  $sql2 = "SELECT PrimaryRoleId, PeoplePosition FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId NOT IN (" . $sel . ") AND PeopleIsActive = '1' AND PeopleId != " . $this->session->userdata('peopleid') . " AND GroupId IN ('3','4') ORDER BY PrimaryRoleId ASC";
                  ?>

                  <div class="col-sm-8">
                    <select name="RoleId_To[]" class="form-control select2" id="RoleId_To" multiple tabi-ndex="5" required>

                      <?php

                      $query1 = $this->db->query($sql)->result();
                      foreach ($query1 as $ft) {

                      ?>

                        <option value='<?= $ft->PrimaryRoleId; ?>' selected> <?= $ft->PeoplePosition; ?></option>

                      <?php }

                      $query2 = $this->db->query($sql2)->result();

                      foreach ($query2 as $ft) {
                        echo "<option value = " . $ft->PrimaryRoleId . ">" . $ft->PeoplePosition . "</option>";
                      }

                      ?>

                    </select>
                  </div>
                </div>
              <?php } ?>
              <!-- Akhir fungsi ini   -->

              <div class="form-group">
                <!-- Jika surat tugas jalankan fungsi ini   -->
                <?php if ($data->Ket == 'outboxsprint') { ?>
                  <label for="title" class="col-sm-2 control-label">Konsep Surat Tugas</label>
                <?php } elseif ($data->Ket == 'outboxundangan') { ?>
                  <label for="title" class="col-sm-2 control-label">Konsep Undangan</label>
                <?php } elseif ($data->Ket == 'outboxkeluar') { ?>
                  <label for="title" class="col-sm-2 control-label">Konsep Surat Dinas</label>
                <?php } elseif ($data->Ket == 'outboxnotadinas') { ?>
                  <label for="title" class="col-sm-2 control-label">Konsep Nota Dinas</label>
                <?php } else { ?>
                  <label for="title" class="col-sm-2 control-label">Konsep Naskah Dinas Lainnya</label>
                <?php } ?>

                <div class="col-sm-8">
                  <textarea name="Konten" class="form-control mytextarea" cols="30" rows="10" placeholder="Isi" required><p><?= $data->Konten ?></p></textarea>
                </div>


              </div>

         

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tembusan</label>

                <?php

                $sel1 = '';
                $role_array1 = explode(',', $data->RoleId_Cc);

                $sql1 = "SELECT * FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId NOT IN ('','-') AND PrimaryRoleId IN (";

                foreach ($role_array1 as $key => $value1) {
                  $sql1 .= "'" . $value1 . "'" . ",";
                  $sel1 .= "'" . $value1 . "'" . ",";
                }

                $sql1 = substr($sql1, 0, -1);
                $sel1 = substr($sel1, 0, -1);

                $sql1 .= ") AND PeopleIsActive = '1' AND GroupId = '3' AND PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY PrimaryRoleId ASC";

                $sql21 = "SELECT PrimaryRoleId, PeoplePosition FROM people WHERE RoleAtasan NOT IN ('','-') AND PrimaryRoleId NOT IN (" . $sel1 . ") AND PeopleIsActive = '1' AND PeopleId != " . $this->session->userdata('peopleid') . " AND GroupId = '3' ORDER BY PrimaryRoleId ASC";
                ?>

                <div class="col-sm-8">
                  <select name="RoleId_Cc[]" class="form-control select2" id="RoleId_Cc" multiple tabi-ndex="5">

                    <?php

                    $query11 = $this->db->query($sql1)->result();
                    foreach ($query11 as $ft1) {

                    ?>

                      <option value='<?= $ft1->PrimaryRoleId; ?>' selected> <?= $ft1->PeoplePosition; ?></option>

                    <?php }

                    $query21 = $this->db->query($sql21)->result();

                    foreach ($query21 as $ft1) {
                      echo "<option value = " . $ft1->PrimaryRoleId . ">" . $ft1->PeoplePosition . "</option>";
                    }

                    ?>

                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Pemeriksa</label>
                <div class="col-sm-2">
                  <select name="TtdText" class="form-control select2 TtdText" required disabled>
                    <?php foreach ($this->db->query('SELECT * FROM master_ttd')->result() as $data1) { ?>
                      <option <?= ($data->TtdText == $data1->TtdText ? 'selected' : '') ?> value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                    <?php } ?>
                  </select>
                </div>

                <?php
                $pid = $this->session->userdata('roleatasan');
                $AtasanId = $this->db->query('select PeopleId from people where PrimaryRoleId = "' . $pid . '"')->row();
                ?>
                <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $AtasanId->PeopleId; ?>">

                <?php
                if ($data->TtdText == 'none') {
                ?>
                  <div class="col-md-5 Approve_People <?= ($data->Approve_People == $this->session->userdata('peopleid') ? 'hidden' : '') ?>">
                  <?php } else { ?>
                    <div class="col-md-5 Approve_People <?= ($data->Approve_People == $this->session->userdata('peopleid')) ?>">
                    <?php
                  }
                    ?>
                    <select name="Approve_People" id="Approve_People" class="form-control" disabled>
                      <?php
                      $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                      foreach ($query as $dat_people) {
                      ?>
                        <option <?= ($data->Approve_People == $dat_people->PeopleId ? 'selected' : '') ?> value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeoplePosition; ?></option>
                      <?php } ?>

                    </select>
                    </div>
                  </div>
                  <div class="form-group ">
                    <label for="title" class="col-sm-2 control-label">Unggah Data </label>
                    <div class="col-sm-8">
                      <div id="test_title_galery"></div>
                      <div id="test_title_galery_listed"></div>
                      <small class="info help-block"></small>
                    </div>
                  </div>
                  <?php
                  $file = $this->db->query("SELECT * FROM inbox_files WHERE NId = '" . $data->NId_Temp . "'")->result();
                  if (count($file) > 0) { ?>
                    <div class="form-group">
                      <label for="title" class="col-sm-2 control-label">Data Digital</label>
                      <div class="col-sm-6">
                        <?php foreach ($file as $key => $value) { ?>
                          <div class="col-sm-8">
                            <div class="thumbnail" style="overflow: hidden;">
                              <h6><?= $value->FileName_fake; ?></h6>
                              <a href="#" data-href="<?= base_url('administrator/anri_list_naskah_belum_approve/hapus_file5/' . $data->NId_Temp . '/' . $value->FileName_fake) ?>" class="btn btn-danger btn-block remove-data">Hapus File</a>
                            </div>
                          </div>
                        <?php  } ?>
                      </div>
                    </div>
                  <?php  } ?>
                  <div class="form-group">
                    <div class="col-sm-2">
                      <a href="<?= base_url('administrator/anri_list_naskah_belum_approve') ?>" class="btn btn-success">Kembali</a>
                      <button class="btn btn-danger">Simpan</button>
                    </div>

                  </div>
                  <br>
            </form>
          </div>
        </div>
        <!--/box body -->
      </div>
      <!--/box -->
    </div>
  </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
  $("#RoleId_To").on("select2:select select2:unselect", function(e) {
    var items = $(this).val();
    var ip = e.params.data.id;
    var tembusan = $("#RoleId_Cc").val();

    $.each(tembusan, function(i, item) {
      $.each(items, function(k, item2) {
        if (item == item2) {
          items.splice(k, 1);
          $("#RoleId_To").val(items).change();
          return false;
        }
      })
    })
  });

  $("#RoleId_Cc").on("select2:select select2:unselect", function(e) {
    var items = $(this).val();
    var ip = e.params.data.id;
    var tujuan = $("#RoleId_To").val();

    $.each(tujuan, function(i, item) {
      $.each(items, function(k, item2) {
        if (item == item2) {
          items.splice(k, 1);
          $("#RoleId_Cc").val(items).change();
          return false;
        }
      })
    })
  });

  $(' .TtdText').change(function() {
    if ($(this).find(':selected').val() == 'none') {
      $('.Approve_People').addClass('hidden');
    } else {
      $('.Approve_People').removeClass('hidden');
      r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
      $('#Approve_People').select2();
      $('#Approve_People').attr('disabled', false);
      if (this.value == "AL") {
        idatasan = $("#tmpid_atasan").val();
        $('#Approve_People').val(idatasan).change();
        $('#Approve_People').attr('disabled', true);
      }
    }
  });


  $(document).ready(function() {

    $('.select2').select2();

    var params = {};
    params[csrf] = token;
    $('#test_title_galery').fineUploader({
      template: 'qq-template-gallery',
      request: {
        endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
        params: params
      },
      deleteFile: {
        enabled: true,
        endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
      },
      thumbnails: {
        placeholders: {
          waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
          notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
        }
      },
      validation: {
        allowedExtensions: ["*"],
        sizeLimit: 0,

      },
      showMessage: function(msg) {
        toastr['error'](msg);
      },
      callbacks: {
        onComplete: function(id, name, xhr) {
          if (xhr.success) {
            var uuid = $('#test_title_galery').fineUploader('getUuid', id);
            $('#test_title_galery_listed').append('<input type="hidden" class="listed_file_uuid" name="test_title_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="test_title_name[' + id + ']" value="' + xhr.uploadName + '" />');
          } else {
            toastr['error'](xhr.error);
          }
        },
        onDeleteComplete: function(id, xhr, isError) {
          if (isError == false) {
            $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid[' + id + ']"]').remove();
            $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name[' + id + ']"]').remove();
          }
        }
      }
    }); /*end title galery*/
  }); /*end doc ready*/

  $('.remove-data').click(function() {
    var url = $(this).attr('data-href');
    swal({
        title: "Anda yakin data akan dihapus?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        closeOnConfirm: true,
        closeOnCancel: true
      },
      function(isConfirm) {
        if (isConfirm) {
          document.location.href = url;
        }
      });

    return false;
  });
</script>

<script src="<?= BASE_ASSET; ?>/tinymce/tinymce.min.js"></script>

<script>
  tinymce.init({
    force_br_newlines: false,
    force_p_newlines: false,
    forced_root_block: '',
    selector: 'textarea',
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    fontfamily_formats: "Arial",
    marginright_formats: 20,
    theme: "modern",
    indentation: 7,
    indent_use_margin: true,
    nonbreaking_force_tab: true,
    paste_data_images: true,
    nonbreaking_wrap: false,
    powerpaste_keep_unsupported_src: true,
    importcss_append: true,
    tabfocus_elements: "somebutton",
    paste_tab_spaces: 0,
    powerpaste_block_drop: true,
    lists_indent_on_tab: true,
    indentation: '20pt',
    indent_use_margin: true,
    paste_as_text: true,


    smart_paste: true,

    plugins: ["tabfocus"],
    tabfocus_elements: ":prev,:next",
    plugins: [
      "searchreplace",
      "paste",
      "textcolor colorpicker textpattern",
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "forecolor backcolor emoticons",
      "nonbreaking table lists",
      "nonbreaking",
      "textpattern",
      "edit",
      "code",
      "save table"
    ],


    height: 200,
    image_caption: true,

    noneditable_noneditable_class: "mceNonEditable",

    paste_postprocess: function(plugin, args) {
      console.log(args.node);
      args.node.setAttribute('konten', '42');
    },



    a11y_advanced_options: true,
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link image | fontselect",
    toolbar2: "pastetext | pasteword, | print preview media | numlist bullist checklist |forecolor backcolor emoticons | nonbreaking | formatselect | forecolor backcolor",

    image_advtab: true,


    file_picker_callback: function(callback, value, meta) {
      if (meta.filetype == 'image') {
        $('#upload').trigger('click');
        $('#upload').on('change', function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(e) {
            callback(e.target.result, {
              alt: ''
            });
          };
          reader.readAsDataURL(file);
        });
      }
    },
    formats: {
      // Changes the default format for the underline button to produce a span with a class and not merge that underline into parent spans
      underline: {
        inline: 'span',
        styles: {
          'text-decoration': 'underline'
        },
        exact: true
      },
      strikethrough: {
        inline: 'span',
        styles: {
          'text-decoration': 'line-through'
        },
        exact: true
      }
    },

    style_formats: [{
        title: 'Bold text',
        format: 'h1'
      },
      {
        title: 'Red text',
        inline: 'span',
        styles: {
          color: '#ff0000'
        }
      },
      {
        title: 'Red header',
        block: 'h1',
        styles: {
          color: '#ff0000'
        }
      },
      {
        title: 'Example 1',
        inline: 'span',
        classes: 'example1'
      },
      {
        title: 'Example 2',
        inline: 'span',
        classes: 'example2'
      },
      {
        title: 'Table styles'
      },
      {
        title: 'Table row 1',
        selector: 'tr',
        classes: 'tablerow1'
      }
    ],

    content_style: "body{margin: 3px;background: #1ABC9C; align:justify; padding: 5px 15px 20px 20px;border-radius: 3px;}",
  });
</script>

<script>