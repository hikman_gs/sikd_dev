<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
//This page is a result of an autogenerated content made by running test.html with firefox.
function domo(){
 
   // Binding keys
   $('*').bind('keydown', 'Ctrl+e', function assets() {
      $('#btn_edit').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+x', function assets() {
      window.location.href = BASE_URL + '/administrator/user';
       return false;
   });
    
}


jQuery(document).ready(domo);
</script>
<style type="text/css">
  .widget-user-header {
    padding-left: 20px !important; 
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      <small>Data Pengguna</small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Ubah Kata Sandi</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-7">
         <div class="box box-warning">
            <div class="box-body ">

                   <!-- /.col -->
                  <div class="col-md-12">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user" style="box-shadow:none">
                      <!-- Add the bg color to the header using any of the bg-* classes -->
                      <div class="widget-user-header " style="background: url('<?= BASE_ASSET; ?>admin-lte/dist/img/photo1.png') center center;">
                        <h3 class="widget-user-username text-white"><?= $this->session->userdata('peopleusername'); ?></h3>
                      </div>
                      <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_URL.'uploads/user/'.(!empty($user->avatar) ? $user->avatar :'default.png'); ?>" alt="User Avatar" style="height: 80px; width: 80px" >
                      </div>
                      <div class="box-footer">
                       
                        <!-- /.row -->
                      </div>
                    </div>     
                    </div>

                    <div class="col-md-6">
                      <!-- Widget: user widget style 1 -->
                      <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="">
                          
                          <h3 class="">Ubah Kata Sandi</h3>
                        </div>
                        <div class="box-footer no-padding">
                          <ul class="nav nav-stacked">
                           <?=form_open(BASE_URL('administrator/anri_dashboard/change_password_profile'))?>
                           <li>Kata Sandi Lama : </li>
                           <li><input type="password" class="form-control" placeholder="Password Lama" name="old_password" required></li>
                           <li>&nbsp;</li>
                           <li>Kata Sandi Baru :</li>
                           <li><input type="password" class="form-control" placeholder="Password Baru" name="new_password" required></li>
                           <li>&nbsp;</li>
                           <li><input type="submit" name="submit" value="Simpan" class="btn btn-flat btn-info btn-warning"></li>
                           <?=form_close();?>
                          </ul>
                        </div>
                      </div>
                      <!-- /.widget-user -->
                    </div>

                     <div class="col-md-6">
                      <!-- Widget: user widget style 1 -->
                      <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="">
                          <h3 class="">Pengguna</h3>
                        </div>
                        <div class="box-footer no-padding">
                          <ul class="nav nav-stacked">
                            <li>Nama Pengguna : <?= $this->session->userdata('peoplename'); ?>
                            </li>
                            <li>Jabatan : <?= $this->session->userdata('peopleposition'); ?>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <!-- /.widget-user -->
                    </div>

                 
            </div>
            <!--/box body -->
         </div>
         <!--/box -->

      </div>
   </div>
</section>
<!-- /.content -->
