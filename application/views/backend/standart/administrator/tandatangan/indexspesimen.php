<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            Spesimen TTD
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Spesimen</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php include('panel.php');?>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"></h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Cari Dokumen" id="myInputTextField">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body padding">
                         
						<div class="mailbox-controls ">
                            <!-- Check all button --> 
                            <!-- /.pull-right -->
							<a href="<?= base_url('administrator/tandatangan/uploadspesimens'); ?>" class="btn btn-primary"> Tambah Tandatangan </a>
							<br>
                        </div>
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped" id="lookup1">
                                	<thead>
										<tr> 
											<th>Image Spesimen</th> 
											<th>Nama</th> 
											<th>Aksi</th>  
										</tr>
									</thead>
									<tbody>
                                    <?php foreach ($tte as $ttd) { ?>
									<tr> 
                                        <td class="mailbox-name">
											<img src="<?php echo BASE_URL.'/FilesUploaded/ttd/spesimen/'.$ttd->image; ?>" width="150">
										</td> 
                                        <td class="mailbox-name">
											 <?php echo $ttd->nama; ?>
										</td> 
                                         
                                        <td class="mailbox-date pull-right">
											<a href="<?= BASE_URL('administrator/tandatangan/editspesimen/'.$ttd->id) ?>" class="btn btn-warning btn-sm" title="Edit Spesimen" >
												<i class='fa fa-pencil'></i>
											</a>
											<a href="#" class="btn btn-danger btn-sm btn-hapus" title="Hapus Spesimen" data-id="<?=$ttd->id ?>">
												<i class='fa fa-trash'></i>
											</a>
										</td>
                                    </tr>
									<?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages --> 
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <div class="mailbox-controls" style="padding:20px">
                             
                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

 
<script type="text/javascript">

$(document).ready(function(){
 

		// Display the Bootstrap modal
		oTable =  $('#lookup1').DataTable(
			{
				dom: 'Bfrtip',
				"ordering":false,
				"lengthMenu": [[5], [5]],
			}
		);
		 $('#myInputTextField').keyup(function(){
			  oTable.search($(this).val()).draw() ;
		})
});   

$(document).ready(function () {
    $('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        swal("Minimal satu harus dipilih.");
        return false;
      }

    });
});
	$(document).ready(function(){
		$('.btn-hapus').click(function(){
			var id = $(this).data('id');
			swal({
				title: "Apakah Data Akan dihapus ??",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(isConfirm) {
				if (isConfirm) {  
					$.ajax({
						type: "POST",
						url: BASE_URL + "administrator/tandatangan/hapusspesimen",
						dataType: "JSON",
						data: {
							id: id,
						},
						success: function(response, status, xhr) { 
							toastr.success('Data Berhasil dihapus!!');
							setTimeout(function() {
							   location.reload();
							}, 500);
						}
					});
					return false;
				}
			});
		});
	});
</script>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td { padding:12px !important}
.dataTables_filter, .dataTables_info { display: none; }

</style>

 <script type="text/javascript">
  $(document).ready(function(){
   fetchTable();

   $('#uploadForm').submit(function(e){
    e.preventDefault();
    var url = '<?php echo base_url(); ?>';

    var reader = new FileReader();
    reader.readAsDataURL(document.getElementById('file').files[0]);

    var formdata = new FormData();
    formdata.append('file', document.getElementById('file').files[0]);
    $.ajax({
     method: 'POST',
     contentType: false,
     cache: false,
     processData: false,
     data: formdata,
     dataType: 'json',
     url: url + 'index.php/upload/insert',
     success: function(response){
      console.log(response);
      if(response.error){
       $('#responseDiv').removeClass('alert-success').addClass('alert-danger').show();
       $('#message').html(response.message);
      }
      else{
       $('#responseDiv').removeClass('alert-danger').addClass('alert-success').show();
       $('#message').html(response.message);
       fetchTable();
       $('#uploadForm')[0].reset();
      }
     }
    });
   });

   $('#clearMsg').click(function(){
    $('#responseDiv').hide();
   });

  });
  function fetchTable(){
   var url = '<?php echo base_url(); ?>';
   $.ajax({
    method: 'POST',
    url: url + 'index.php/upload/fetch',
    success: function(response){
     $('#tbody').html(response);
    }
   });
  }
 </script>