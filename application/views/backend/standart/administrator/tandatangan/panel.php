<?php $id = $this->uri->segment(4); ?>
<?php $spesimen = $this->uri->segment(3); ?>
<div class="col-md-3">
 
	<a href="<?= BASE_URL('administrator/tandatangan/upload') ?>"  class="btn btn-primary btn-block margin-bottom">
		<i class="fa fa-upload"></i> Upload
	</a>
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title"  style="font-size: 14px !important;">Dokumen Saya </h3>

			<div class="box-tools">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body no-padding">
			<ul class="nav nav-pills nav-stacked" style="font-size: 13px !important;">
				<li class=" <?php echo $id =='0'?'active':''; ?>">
					<a href="<?= BASE_URL('administrator/tandatangan/index/0') ?>"><i class="fa fa-inbox"></i> Semua Dokumen
					<span class="label pull-right" style="background-color:#273c75;"><?php echo $all;?></span></a>
				</li>
				<li class="<?php echo $id =='konsep'?'active':''; ?>"><a href="<?= BASE_URL('administrator/tandatangan/index/konsep') ?>"><i class="fa fa-file-text-o"></i> Draft <span class="label label-info pull-right"><?php echo $konsep;?></span></a></li>
				<li class="<?php echo $id =='selesai'?'active':''; ?>"><a href="<?= BASE_URL('administrator/tandatangan/index/selesai') ?>"><i class="fa fa-check-square-o"></i> Tertandatangani  <span class="label label-success pull-right"><?php echo $tertandatangani;?></span></a></li> 
				<li class="<?php echo $spesimen =='onprogress'?'active':''; ?>"> <a href="<?= BASE_URL('administrator/tandatangan/onprogress') ?>"><i class="fa fa-share-square-o"></i> Teruskan untuk TTD <span class="label bg-blue pull-right"><?php echo $onprogress;?></span></a></a></li>
				<li class="<?php echo $spesimen =='responsterkirim'?'active':''; ?>"> <a href="<?= BASE_URL('administrator/tandatangan/responsterkirim') ?>"><i class="fa fa-share-square-o"></i> Teruskan untuk Dikirim <span class="label pull-right bg-blue"><?php echo $responsterkirim;?></span></a></a></li>
			</ul>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /. box -->
	
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title"  style="font-size: 14px !important;">Dokumen Masuk</h3>
			<div class="box-tools">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body no-padding">
			<ul class="nav nav-pills nav-stacked" style="font-size: 13px !important;">
				<li class=" <?php echo $id =='dokumen'?'active':''; ?>"> <a href="<?= BASE_URL('administrator/tandatangan/respons/dokumen') ?>"><i class="fa fa-hourglass-half"></i> Yang Perlu Direspons <span class="label label-danger pull-right"><?php echo $jumlahfilerespon;?></span></a></li>
				<li class=" <?php echo $id =='kirim'?'active':''; ?>"> <a href="<?= BASE_URL('administrator/tandatangan/dokumen/kirim') ?>"><i class="fa fa-paper-plane-o"></i> Yang Perlu Dikirim  <span class="label label-warning pull-right"><?php echo $jumlahfilekirim;?></span></a></li>
				<!---->
			</ul>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /. box -->
	
	<div class="box box-solid">
		<div class="box-header with-border">
			<h3 class="box-title" style="font-size: 14px !important;">Spesimen Tandatangan </h3>
			<div class="box-tools">
				<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
				</button>
			</div>
		</div>
		<div class="box-body no-padding">
			<ul class="nav nav-pills nav-stacked" style="font-size: 13px !important;">
				<li class=" <?php echo $spesimen =='spesimen'?'active':''; ?>">
					 
					<a href="<?= BASE_URL('administrator/tandatangan/spesimen') ?>">
						<i class="fa fa-pencil-square-o"></i> Spesimen Tandatangan
					</a>
				</li>  
				<!---->
			</ul>
		</div>
		<!-- /.box-body -->
	</div>
	<!-- /. box -->
	 
	 
	<!-- /.box -->
</div>
