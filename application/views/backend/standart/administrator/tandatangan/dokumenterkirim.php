<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            Dokumen Masuk
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dokumen Terkirim</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php include('panel.php');?>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=$judul_menu?></h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Cari Dokumen" id="myInputTextField">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body padding">
                        <form action="#" class="form-horizontal xwan" method="POST">
						<div class="mailbox-controls">

                            <!-- /.pull-right -->
                        </div>
                        <div class=" mailbox-messages">
                            <table class="table table-hover table-striped" id="lookup1">
                                	<thead>
										<tr>  
											<th>Nama Dokumen</th>   
                                            <th>Konseptor</th>
                                            <th>Catatan</th>   
											<th>Status</th>    
											<th>Aksi</th>  
										</tr>
									</thead>
									<tbody> 
                                    <?php foreach ($tte as $ttd) { ?> 
									<tr> 
                                        <td class="mailbox-name">
											<?php echo getfileName1TTD($ttd->ttd_id); ?>
										</td> 
                                        <td class="mailbox-sender">
											<?php echo get_konseptor($ttd->PeopleID); ?>
										</td>
                                        <td class="mailbox-note">
											<?php echo $ttd->catatan ?>
										</td>
                                        <td class="mailbox-name">
											<?php echo ttd_label_dokterkirim($ttd->status); ?>
										</td>   
                                        <td>
											<button type="button" title="Tandai Terkirim" class="btn btn-danger btn-sm btn-update"  data-idx="<?=$ttd->ttd_id ?>" data-id="<?=$ttd->id ?>"> <i class="fa fa-check"></i></button>
											<a href="<?= BASE_URL('administrator/tandatangan/dokumendetail/'.$ttd->ttd_id) ?>" class="btn btn-info btn-sm"  title="Detail Dokumen" ><i class='fa fa-info'></i> </a>
											<a href="<?= BASE_URL('administrator/tandatangan/download/'.$ttd->ttd_id) ?>" class="btn btn-primary btn-sm" title="Download File" >
												<i class='fa fa-download'></i>
											</a>
 										</td>
                                    </tr>  
									<?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages --> 
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <div class="mailbox-controls" style="padding:20px">
                             
                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- iCheck -->
<script src="https://adminlte.io/themes/AdminLTE/plugins/iCheck/icheck.min.js"></script>
<!-- Page Script -->
<script>
$(function() {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function() {
        var clicks = $(this).data('clicks');
        if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }
        $(this).data("clicks", !clicks);
    });

    //Handle starring for glyphicon and font awesome
    $(".mailbox-star").click(function(e) {
        e.preventDefault();
        //detect type
        var $this = $(this).find("a > i");
        var glyph = $this.hasClass("glyphicon");
        var fa = $this.hasClass("fa");

        //Switch states
        if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
        }

        if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
        }
    });

});

</script>
<script type="text/javascript">

  
$(document).ready(function(){
    // Display the Bootstrap modal
    oTable =  $('#lookup1').DataTable(
    { 
        "ordering":false, 
    }
    );
        $('#myInputTextField').keyup(function(){
        oTable.search($(this).val()).draw() ;
    })
});   

 
	$(document).ready(function(){
		$('.btn-update').click(function(){
			var id = $(this).data('id');
            var ttdid = $(this).data('idx');
			swal({
				title: "Ubah Status jadi Terkirim ??",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(isConfirm) {
				if (isConfirm) { 
					console.log(id);
						$.ajax({
							type: "POST",
							url: BASE_URL + "administrator/tandatangan/update",
							dataType: "JSON",
							data: {
								id: id,
                                ttdid:ttdid
							},
							success: function(response, status, xhr) {
								console.log(response);
								toastr.success('Data Berhasil diperbarui!!');
								setTimeout(function() {
									$("#lookup1").load(location.href + " #lookup1"); 
								}, 500);
							}
						});
					return false;
				}
			});
		});
	});

</script>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td { padding:12px !important}
.dataTables_filter, .dataTables_info { display: none; }
.box { 
    border-radius: 3px !important; 
}
</style>


 
