<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Sistem Informasi Kearsipan Dinamis</title>
  
    <link rel="shortcut icon" href="<?= BASE_ASSET; ?>favicon.ico">

  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css'>


<link href="<?= BASE_ASSET; ?>select2/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="<?= BASE_ASSET; ?>css/style.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style>
    #approve .modal-content {
        /* new custom width */
        height: 220px;
    }

    #nomor .modal-content {
        /* new custom width */
        height: 220px;
    }

    #k_teruskan .modal-content {
        /* new custom width */
        width: 520px;
    }

    #k_teruskan_tu .modal-content {
        /* new custom width */
        width: 519px;
    }

    .select2-container--default .select2-selection--single {
        border: 0px;
    }

    .select2-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 2px 0px !important;
    }

    .chosen-container-single,
    .chosen-container-multi {
        width: 100% !important;
    }

    .select3-container--default .select2-selection--single {
        border: 0px;
    }

    .select3-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 3px 0px !important;
    }

    .chosen-container-single,
    .chosen-container-multi {
        width: 100% !important;
    }
</style>
</head>

<body>

  <div class="container"> 
  <div class="row">
      <div class="col-md-12" style="padding:10px">
			<a class="btn btn-primary btn-block" href="<?php echo BASE_URL;?>administrator/tandatangan/index/0">Kembali ke Menu Tandatangan</a>
      </div>

  </div>  <div class="row">
      <div class="col-md-12" style="padding:10px">
			<?php if($this->session->flashdata('error')){  ?>
   <div class="alert alert-danger" role="alert">
 <?php echo $this->session->flashdata('error'); ?>
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
   
<?php } ?>
      </div>

  </div>
 
  <div class="row">
					<div class="col-md-12" id="pdfManager"  style="display:none" >	
						<div class="row" id="selectorContainer">
							<div class="col-fixed-240" style="margin-top: 16px;
    margin-left: 10px;
    margin-bottom: 13px;
    font-weight: bold;">
								Spesimen TTD
							</div>
							<div class="col-fixed-240" id="parametriContainer">
								
							</div>
							<div class="col-fixed-605">
								<div>
  <button id="prev" type="button" class="btn"><i class="fa fa-fw fa-angle-double-left"> </i>Prev</button>
  <button id="next" type="button" class="btn"><i class="fa fa-fw fa-angle-double-right"> </i>Next</button>
  &nbsp; &nbsp;
  <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
   <button type="button" class="btn btn-success pull-right" onClick="reqSignature()" style="margin-bottom:5px;"><i class="fa fa-pencil"></i> Tandatangan</button>
</div>
								<div id="pageContainer" class="pdfViewer singlePageView dropzone nopadding" style="background-color:transparent"> 
                                    <canvas id="the-canvas" style="border: 1px  solid black;" data-mce-style="border: 1px  solid black;"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>

	  </div>
 <!-- parameters showed on the left sidebar -->
<input id="parameters" type="hidden" value='<?php echo $jsonSpesimen; ?>' />

 

<!-- Modal -->
<div class="modal fade" id="approve_naskah1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Konfirmasi</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <form action="<?= base_url('administrator/tandatangan/signttdselfsign'); ?>" class="form-horizontal" method="POST">
		 <div class="container-fluid">
    <div class="row">
	<div class="col-md-12">
		 <div class="content">
                        <div class="form-group"> 
                            <input type="hidden" id="idfile" name="idfile" value="<?php echo $idfile; ?>">
                            <input type="hidden" id="xAxis" name="xAxis">
                            <input type="hidden" id="yAxis" name="yAxis">
                            <input type="hidden" id="urisegment" name="urisegment" value="<?php echo $urisegment; ?>">
                            <input type="hidden" id="pageNum" name="pageNum">
                            <input type="hidden" id="spesimenID" name="spesimenID">
                            <input type="hidden" id="respon" name="respon" value="<?php echo $respon; ?>">
                            <div class="col-6">
                                <label>Masukan Passphrase</label>
                                <input type="password" min="1" class="form-control" name="password" id="password" required>
                            </div>
                        </div> 
                    </div>
		 
      </div>
      </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan </button>
      </div>
    </div>
	</form>
  </div>
</div>



 

<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/interact.js/1.2.9/interact.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js'></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script>
// If absolute URL from the remote server is provided, configure the CORS
// header on that server.
var url = '<?php echo $file; ?>';
/*
 *  costanti per i placaholder 
 */
var maxPDFx = 595;
var maxPDFy = 842;
var offsetY = 7;
// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

var pdfDoc = null,
    pageNum = 1,
    pageRendering = true,
    pageNumPending = null,
    scale = 1.0,
    canvas = document.getElementById('the-canvas'),
    ctx = canvas.getContext('2d');

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
    pageRendering = true;
    // Using promise to fetch the page
    pdfDoc.getPage(num).then(function(page) {
        var viewport = page.getViewport({
            scale: scale
        });
        canvas.height = viewport.height;
        canvas.width = viewport.width;

        // Render PDF page into canvas context
        var renderContext = {
            canvasContext: ctx,
            viewport: viewport
        };
        var renderTask = page.render(renderContext);

        // Wait for rendering to finish
        renderTask.promise.then(function() {
            pageRendering = false;
            if (pageNumPending !== null) {
                // New page rendering is pending
                renderPage(pageNumPending);
                pageNumPending = null;
            }
        });
    });

    // Update page counters
    document.getElementById('page_num').textContent = num;
    $('#pdfManager').show();
    var parametri = JSON.parse($('#parameters').val());
    $('#parametriContainer').empty();
    renderizzaPlaceholder(0, parametri);

}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
    if (pageRendering) {
        pageNumPending = num;
    } else {
        renderPage(num);
    }
}

/**
 * Displays previous page.
 */
function onPrevPage() {
    if (pageNum <= 1) {
        return;
    }
    pageNum--;
    queueRenderPage(pageNum);
}
document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
    if (pageNum >= pdfDoc.numPages) {
        return;
    }
    pageNum++;
    queueRenderPage(pageNum);
}
document.getElementById('next').addEventListener('click', onNextPage);

/**
 * Asynchronously downloads PDF.
 */
pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
    pdfDoc = pdfDoc_;
    document.getElementById('page_count').textContent = pdfDoc.numPages;

    // Initial/first page rendering
    renderPage(pageNum);
});


/* The dragging code for '.draggable' from the demo above
 * applies to this demo as well so it doesn't have to be repeated. */

// enable draggables to be dropped into this
interact('.dropzone').dropzone({
    // only accept elements matching this CSS selector
    accept: '.drag-drop',
    // Require a 100% element overlap for a drop to be possible
    overlap: 1,

    // listen for drop related events:

    ondropactivate: function(event) {
        // add active dropzone feedback
        event.target.classList.add('drop-active');
    },
    ondragenter: function(event) {
        var draggableElement = event.relatedTarget,
            dropzoneElement = event.target;

        // feedback the possibility of a drop
        dropzoneElement.classList.add('drop-target');
        draggableElement.classList.add('can-drop');
        draggableElement.classList.remove('dropped-out');
        //draggableElement.textContent = 'Dragged in';
    },
    ondragleave: function(event) {
        // remove the drop feedback style
        event.target.classList.remove('drop-target');
        event.relatedTarget.classList.remove('can-drop');
        event.relatedTarget.classList.add('dropped-out');
        //event.relatedTarget.textContent = 'Dragged out';
    },
    ondrop: function(event) {
        //event.relatedTarget.textContent = 'Dropped';
    },
    ondropdeactivate: function(event) {
        // remove active dropzone feedback
        event.target.classList.remove('drop-active');
        event.target.classList.remove('drop-target');
    }
});

interact('.drag-drop') 
  .draggable({
        inertia: true,
        restrict: {
            restriction: "#selectorContainer",
            endOnly: true,
            elementRect: {
                top: 0,
                left: 0,
                bottom: 1,
                right: 1
            }
        },
        autoScroll: true,
        // dragMoveListener from the dragging demo above
        onmove: dragMoveListener,
    });


function dragMoveListener(event) {
    var target = event.target,
        // keep the dragged position in the data-x/data-y attributes
        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

    // translate the element
    target.style.webkitTransform =
        target.style.transform = 'translate(' + x + 'px, ' + y + 'px)';

    // update the posiion attributes
    target.setAttribute('data-x', x);
    target.setAttribute('data-y', y);

    //console.log('data-x ' + x);
    //console.log('data-y ' + y);
}

// this is used later in the resizing demo
window.dragMoveListener = dragMoveListener;

function renderizzaPlaceholder(currentPage, parametri) {
    var maxHTMLx = $('#the-canvas').width();
    var maxHTMLy = $('#the-canvas').height();

    var paramContainerWidth = $('#parametriContainer').width();
    var yCounterOfGenerated = 0;
    var numOfMaxItem = 25;
    var notValidHeight = 30;
    var y = 0;
    var x = 6;
    var page = 0;


    var totalPages = Math.ceil(parametri.length / numOfMaxItem);

    for (i = 0; i < parametri.length; i++) {
        var param = parametri[i];
        var page = Math.floor(i / numOfMaxItem);
        var display = currentPage == page ? "block" : "none";

        if (i > 0 && i % numOfMaxItem == 0) {
            yCounterOfGenerated = 0;
        }

        var classStyle = "";
        var valore = param.valore;
        /*il placeholder non è valido: lo incolonna a sinistra*/

        if (i > 0 && i % numOfMaxItem == 0) {
            yCounterOfGenerated = 0;
        }

        var classStyle = "";
        var image = param.image;
        var PeopleID = param.PeopleID;
        var id = param.id;
        /*il placeholder non è valido: lo incolonna a sinistra*/
        y = yCounterOfGenerated;
        yCounterOfGenerated += notValidHeight;
        classStyle = "drag-drop dropped-out";

        console.log(x,y);
        $("#parametriContainer").append('<div class="' + classStyle + '" data-id="-1" data-page="' + page + '" data-toggle="' + id + '"  data-valore="' + id + '" data-x="' + x + '" data-y="' + y + '" style="transform: translate(' + x + 'px, ' + y + 'px); display:' + display + '"><img src="<?php echo BASE_URL; ?>/FilesUploaded/ttd/spesimen/' + image + '" width="200"></div>');
    }


    y = notValidHeight * (numOfMaxItem + 1);
    var prevStyle = "";
    var nextStyle = "";
    var prevDisabled = false;
    var nextDisabled = false;
    if (currentPage == 0) {
        prevStyle = "disabled";
        prevDisabled = true;
    }

    if (currentPage >= totalPages - 1 || totalPages == 1) {
        nextDisabled = true;
        nextStyle = "disabled";
    }
}

function renderizzaInPagina(parametri) {
    var maxHTMLx = $('#the-canvas').width();
    var maxHTMLy = $('#the-canvas').height();

    var paramContainerWidth = $('#parametriContainer').width();
    var yCounterOfGenerated = 0;
    var numOfMaxItem = 26;
    var notValidHeight = 30;
    var y = 0;
    var x = 6;
    for (i = 0; i < parametri.length; i++) {
        var param = parametri[i];

        var classStyle = "drag-drop can-drop";
        var valore = param.valore;
        /*il placeholder non è valido: lo incolonna a sinistra*/

        var pdfY = maxPDFy - param.posizioneY - offsetY;
        y = (pdfY * maxHTMLy) / maxPDFy;
        x = ((param.posizioneX * maxHTMLx) / maxPDFx) + paramContainerWidth;

        $("#parametriContainer").append('<div class="' + classStyle + '" data-id="' + param.idParametroModulo + '" data-toggle="' + valore + '" data-valore="' + valore + '" data-x="' + x + '" data-y="' + y + '" style="transform: translate(' + x + 'px, ' + y + 'px);">  <span class="circle"></span><span class="descrizione">' + param.descrizione + ' </span></div>');

    }
}


function changePage(disabled, currentPage, delta) {
    if (disabled) {
        return;
    }

    /*recupera solo i parametri non posizionati in pagina*/
    var parametri = [];
    $(".drag-drop.dropped-out").each(function() {
        var valore = $(this).data("valore");
        var descrizione = $(this).find(".descrizione").text();
        parametri.push({
            valore: valore,
            descrizione: descrizione,
            posizioneX: -1000,
            posizioneY: -1000
        });
        $(this).remove();
    });

    //svuota il contentitore
    $('#pager').remove();
    currentPage += delta;
    renderizzaPlaceholder(currentPage, parametri);
}


function showCoordinates() {
    var validi = [];
    var nonValidi = [];

    var maxHTMLx = $('#the-canvas').width();
    var maxHTMLy = $('#the-canvas').height();
    var paramContainerWidth = $('#parametriContainer').width();

    //recupera tutti i placholder validi
    $('.drag-drop.can-drop').each(function(index) {
        var x = parseFloat($(this).data("x"));
        var y = parseFloat($(this).data("y"));
        var valore = $(this).data("valore");
        var descrizione = $(this).find(".descrizione").text();

        var pdfY = y * maxPDFy / maxHTMLy;
        var posizioneY = maxPDFy - offsetY - pdfY;
        var posizioneX = (x * maxPDFx / maxHTMLx) - paramContainerWidth;

        var val = {
            "descrizione": descrizione,
            "posisX": posizioneX,
            "posisiY": posizioneY,
            "valore": valore
        };
        validi.push(val);

    });

    if (validi.length == 0) {
        alert('No placeholder dragged into document');
    } else {
        alert(JSON.stringify(validi));
    }
}

function reqSignature() {
    var validi = [];
    var nonValidi = [];
    var maxHTMLx = $('#the-canvas').width();
    var maxHTMLy = $('#the-canvas').height();
    var paramContainerWidth = $('#parametriContainer').width();
 
    $('.drag-drop.can-drop').each(function(index) {
        var x = parseFloat($(this).data("x"));
        var y = parseFloat($(this).data("y"));
        var valore = $(this).data("valore");

        var descrizione = $(this).find(".descrizione").text();

        var pdfY = (y * maxPDFy) / maxHTMLy;
        var posizioneY = maxPDFy - pdfY - offsetY;
        var posizioneX = ((x * maxPDFx) / maxHTMLx) - paramContainerWidth;

        var val = {
            "descrizione": descrizione,
            "posizioneX": posizioneX,
            "posizioneY": posizioneY,
            "valore": valore
        };
        validi.push(val);
        $('#xAxis').val(Math.ceil(posizioneX));
        $('#yAxis').val(Math.ceil(posizioneY));
        $('#pageNum').val(pageNum);
        $('#spesimenID').val(valore);
 
        $("#approve_naskah1").modal('show');
        console.log(x,y)
    });
    if (validi.length == 0) {
        alert('No placeholder dragged into document');
    } else {

        //alert(JSON.stringify(validi));
    }


}

function ko_verifikasi1(NId, GIR_Id) {
    $("#NId_Temp").val(NId);
    $("#GIR_Id").val(GIR_Id);
    $("#approve_naskah1").modal('show');
}
</script>
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>
<script>

$(document).ready(function() {
    $('.select2').select2();
 
});
</script>
</body>
</html>
