<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/ui.dynatree.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/tree.skinklas.css">

<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.form.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.dynatree.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/dynatree_role.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>

<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Unit Kerja</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2" style="box-shadow:none">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Pengaturan Unit Kerja</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <a id="btn_tambah" class="btn btn-success" title="Tambah Data">Tambah</a>
                      <a id="btn_hapus" class="btn btn-danger" title="Hapus Data">Hapus</a>
                      <a id="btn_batal" class="btn btn-primary" title="Batal">Batal</a>
                      <a class="btn btn-flat btn-warning btn-edit hidden" title="Ubah Data">Ubah</a>
                    </div>
                  </div>
                  <br>
                  <div class="table-responsive"> 
                    <div class="col-md-6" id="treeUK">
                      <?php
                      if($this->session->userdata('groupid') == "1"){
                        $parent = 'root';
                        $query = $this->db->query("SELECT RoleParentId as parent_id, 
                                  RoleId as id, 
                                  RoleDesc as name, 
                                  Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId,'|',RoleCode , '|',GRoleId) as allData
                                  FROM role 
                                WHERE RoleId != 'root'
                                ORDER BY RoleId,RoleParentId ASC ")->result();
                      }else{
                        $parent = 'root';
                        $query = $this->db->query("SELECT RoleParentId as parent_id, 
                                  RoleId as id, 
                                  RoleDesc as name, 
                                  Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId,'|',RoleCode , '|',GRoleId) as allData
                                  FROM role 
                                WHERE RoleId != 'root'
                                ORDER BY RoleId,RoleParentId ")->result();
                      }

                      $data = array();
                      foreach ($query as $row) {
                        $data[$row->parent_id][] = $row;
                      }
                      echo showUK($data,$parent); // lakukan looping menu utama
                      ?>
                    </div>
                    <?=form_open(BASE_URL('administrator/anri_master_role/add_save'))?>
                    <div class="col-md-6">
                      <input type="hidden" name="txt1">
                      <input type="hidden" name="txt2">
                      <input type="hidden" name="txt3">
                      <input type="hidden" name="txt4">
                      <input type="hidden" name="txt5">
                      <input type="hidden" name="txt6">
                      <div class="form-group col-md-12 hidden">
                            <label for="RoleId" class="col-sm-3 control-label">RoleId 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="RoleId" id="RoleId" placeholder="RoleId" value="<?= set_value('RoleId'); ?>">
                            </div>
                      </div>
                      <div class="form-group col-md-12 hidden">
                            <label for="RoleParentId" class="col-sm-3 control-label">RoleParentId 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="RoleParentId" id="RoleParentId" placeholder="RoleParentId" value="<?= set_value('RoleParentId'); ?>">
                            </div>
                      </div>
                      <div class="form-group col-md-12">
                            <label for="RoleName" class="col-sm-3 control-label">Jabatan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" disabled class="form-control" name="RoleName" id="RoleName" placeholder="Jabatan" value="<?= set_value('RoleName'); ?>" required>
                            </div>
                      </div>
                      <div class="form-group col-md-12">
                            <label for="RoleDesc" class="col-sm-3 control-label">Unit Kerja 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <textarea name="RoleDesc" id="RoleDesc" class="form-control" disabled placeholder="Unit Kerja" required><?= set_value('RoleDesc'); ?></textarea>
                            </div>
                      </div>
                      <div class="form-group col-md-12">
                            <label for="RoleCode  " class="col-sm-3 control-label">Kode Unit Kerja<i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <input type="text" disabled class="form-control" name="RoleCode" id="RoleCode" placeholder="Kode Unit Kerja" value="<?= set_value('RoleCode'); ?>" required>
                            </div>
                      </div>
                      <div class="form-group col-md-12">
                            <label for="GjabatanId   " class="col-sm-3 control-label">Grup Jabatan    
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control select2" disabled name="gjabatanId" id="gjabatanId" required data-val="">
                                  <option value="">-</option>
                                  <?php
                                  $gjabatan = $this->db->query("select gjabatanId, gjabatanName from master_gjabatan order by gjabatanName")->result();
                                  foreach ($gjabatan as $gj):
                                  ?>
                                      <option value="<?= $gj->gjabatanId; ?>"><?=$gj->gjabatanName ;?></option>

                                  <?php endforeach; ?>
                                </select>
                            </div>
                      </div>
                      <div class="form-group col-md-12">
                            <label for="GRoleId" class="col-sm-3 control-label">Grup Unit Kerja    
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control" disabled name="GRoleId" id="GRoleId" required data-val="">
                                  <option value="">-</option>
                                  <?php
                                  $grole = $this->db->query("SELECT * FROM master_grole")->result();
                                  foreach ($grole as $gr):
                                  ?>

                                      <option value="<?= $gr->GRoleId; ?>"><?=$gr->GRoleName;?></option>

                                  <?php endforeach; ?>
                                </select>
                            </div>
                      </div>
                      <div class="form-group col-md-12">
                            <label for="RoleStatus " class="col-sm-3 control-label">Status  </label>
                            <div class="col-sm-9">
                               <input type="checkbox" disabled id="RoleStatus" name="RoleStatus" value="<?= set_value('RoleStatus'); ?>">
                            </div>
                      </div>
                      <div class="form-group col-md-12">
                        <input type="submit" name="submit" class="btn btn-save btn-flat btn-danger hidden" title="Simpan Data" value="Simpan">
                        <a class="btn btn-flat btn-danger btn-saveedit hidden" title="Simpan Data">Simpan</a>
                      </div>
                    </div>
                    <?=form_close();?>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<script>
  $('#RoleStatus').change(function() {
    if(this.checked) {
        $(this).val('1');
    }else{
      $('#RoleStatus').val('0');
    }
  });

  $('#btn_tambah').click(function(){
    var id = $('#RoleId').val();
    if (id == '') {
      swal({
            title: "Anda Belum Memilih Unit Kerja",
            icon: "error",
          });
    }else{       
      $('.btn-edit').addClass('hidden').attr('disabled',true);
      $('.btn-save').removeClass('hidden').removeAttr('disabled');
      $('.btn-saveedit').addClass('hidden');
      $('#RoleName').removeAttr('disabled').val('');
      $('#RoleDesc').removeAttr('disabled').val('');
      $('#RoleCode').removeAttr('disabled').val('');
      $('#GRoleId').removeAttr('disabled').val('');
      $('#gjabatanId').removeAttr('disabled').val('');
      $('#RoleStatus').removeAttr('disabled').val('1');
      $('#RoleName').focus();      
      $('#RoleStatus').checked();
    }  
  });

  $('#btn_batal').click(function(){
      window.location.href = "<?=BASE_URL('administrator/anri_master_role')?>";
  });

  $('.btn-edit').click(function(){
    $('.btn-save').addClass('hidden').attr('disabled',true);
    $('.btn-saveedit').removeClass('hidden');
    $('#RoleName').removeAttr('disabled');
    $('#RoleDesc').removeAttr('disabled');
    $('#RoleCode').removeAttr('disabled');
    $('#GRoleId').removeAttr('disabled');
    $("[name=RoleStatus]").removeAttr('disabled');
    $("[name=gjabatanId]").removeAttr('disabled');    
  });

  $('#btn_hapus').click(function(){
    var id = $('#RoleId').val();
    if (id == '') {
      swal({
            title: "Anda Belum Memilih Unit Kerja",
            icon: "error",
          });
    }else{
      swal({
            title: "Apakah Data Akan Dihapus ?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $.ajax({
                url: "<?=BASE_URL('administrator/anri_master_role/delete')?>",
                    type: "POST",
                    data:{
                        data_id: $('#RoleId').val(),
                        parent: $('#RoleParentId').val(),
                    },
                    success: function(response){
                      var obj = jQuery.parseJSON(response);
                      if(obj['status'] == 'error_check_parent') {
                        swal({title: "Data Tidak Bisa Dihapus, Karena Terdapat Unit Kerja Bawahan",icon: "error",});  
                      }else if(obj['status'] == 'error_berkas'){
                        swal({title: "Data Tidak Bisa Dihapus, Karena Sudah Terdapat Naskah Pada Unit Kerja",icon: "error",});                        
                      }else if(obj['status'] == 'error_aja'){
                        swal({title: "Data Tidak Bisa Dihapus, Karena Sudah Terdapat Pengguna Pada Unit Kerja",icon: "error",});  
                      }else if(obj['status'] == 'success'){
                        window.location.href = "<?=BASE_URL('administrator/anri_master_role')?>";
                      }
                    },
                    error: function(){
                      swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
                    }
              });
            }
          });
    }
  });

  $('.btn-saveedit').click(function(){
    var id = $('#RoleId').val();
    if (id == '') {
      swal({
            title: "Maaf Unit Kerja Belum anda pilih",
            icon: "error",
          });
    }else{
      $.ajax({
          url: "<?=BASE_URL('administrator/anri_master_role/update_save')?>",
          type: "POST",
          data:{
              RoleId: $('#RoleId').val(),
              RoleParentId:$('#RoleParentId').val(),
              RoleName: $('#RoleName').val(),
              RoleDesc: $('#RoleDesc').val(),
              RoleCode: $('#RoleCode').val(),
              GRoleId: $('#GRoleId').val(),
              RoleStatus : ($("#RoleStatus").is(":checked") == true ? '1' : '0'),
              gjabatanId: $("[name=gjabatanId]").val(),
          },
          success: function(response){
            window.location.href = "<?=BASE_URL('administrator/anri_master_role')?>";
          },
          error: function(){
            swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
          }
      });
    }
  });
</script>