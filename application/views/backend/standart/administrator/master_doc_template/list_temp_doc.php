<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Daftar Template Naskah Dinas</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Daftar Template Naskah Dinas</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>                

                  <div class="table-responsive"> 
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr>
                           <th width="5%">No</th>
                           <th width="50%">Nama Template</th>
                           <th>Data Digital</th>
                        </tr>
                     </thead>
                     <tbody id="tbody_master_doc_template">
                     <?php 
                     $master_doc_templates = $this->db->get('master_doc_template')->result();
                     foreach($master_doc_templates as $k => $master_doc_template): ?>
                        <tr>
                           <td><?= $k+1; ?></td>                           
                           <td><?= _ent($master_doc_template->doc_desc); ?></td> 
                           <td>
                              <?php if (!empty($master_doc_template->doc_file)): ?>
                                <?php if (is_image($master_doc_template->doc_file)): ?>
                                <a class="fancybox" target="_blank" rel="group" href="<?= BASE_URL . 'FilesUploaded/TemplateDoc/' . $master_doc_template->doc_file; ?>">
                                  <img src="<?= BASE_URL . 'FilesUploaded/TemplateDoc/' . $master_doc_template->doc_file; ?>" class="image-responsive" alt="image master_doc_template" title="doc_file master_doc_template" width="40px">
                                </a>
                                <?php else: ?>
                                  <a target="_blank" href="<?= BASE_URL . 'FilesUploaded/TemplateDoc/' . $master_doc_template->doc_file; ?>">
                                   <img src="<?= get_icon_file($master_doc_template->doc_file); ?>" class="image-responsive image-icon" alt="image master_doc_template" title="doc_file <?= $master_doc_template->doc_file; ?>" width="40px"> 
                                 </a>
                                <?php endif; ?>
                              <?php endif; ?>
                           </td>                            
                        </tr>
                      <?php endforeach; ?>
                     </tbody>
                  </table>
                  </div>
               </div>
               </form>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->