<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Template Dokumen</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                        <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="Tambah Data" href="<?=  site_url('administrator/anri_master_doc_template/add'); ?>"><i class="fa fa-plus-square-o" ></i> Tambah Data</a>
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Pengaturan</h3>
                     <h5 class="widget-user-desc">Template Dokumen</h5>
                     <hr>
                  </div>

                  <form name="form_master_doc_template" id="form_master_doc_template" action="<?= base_url('administrator/anri_master_doc_template'); ?>">
                  

                  <div class="table-responsive"> 
                  <table id="example1" class="table table-bordered table-striped">
                     <thead>
                        <tr class="">
                           <th>
                            <input type="checkbox" class="flat-red toltip" id="check_all" name="check_all" title="check all">
                           </th>
                           <th>Nama Template</th>
                           <th>Data Digital</th>
                           <th>Tanggal Unggah</th>
                           <th>Aksi</th>
                        </tr>
                     </thead>
                     <tbody id="tbody_master_doc_template">
                     <?php 
                     $master_doc_templates = $this->db->get('master_doc_template')->result();
                     foreach($master_doc_templates as $master_doc_template): ?>
                        <tr>
                           <td width="5">
                              <input type="checkbox" class="flat-red check" name="id[]" value="<?= $master_doc_template->doc_id; ?>">
                           </td>
                           
                           <td><?= _ent($master_doc_template->doc_desc); ?></td> 
                           <td>
                              <?php if (!empty($master_doc_template->doc_file)): ?>
                                <?php if (is_image($master_doc_template->doc_file)): ?>
                                <a class="fancybox" target="_blank" rel="group" href="<?= BASE_URL . 'FilesUploaded/TemplateDoc/' . $master_doc_template->doc_file; ?>">
                                  <img src="<?= BASE_URL . 'FilesUploaded/TemplateDoc/' . $master_doc_template->doc_file; ?>" class="image-responsive" alt="image master_doc_template" title="doc_file master_doc_template" width="40px">
                                </a>
                                <?php else: ?>
                                  <a target="_blank" href="<?= BASE_URL . 'FilesUploaded/TemplateDoc/' . $master_doc_template->doc_file; ?>">
                                   <img src="<?= get_icon_file($master_doc_template->doc_file); ?>" class="image-responsive image-icon" alt="image master_doc_template" title="doc_file <?= $master_doc_template->doc_file; ?>" width="40px"> 
                                 </a>
                                <?php endif; ?>
                              <?php endif; ?>
                           </td>
                            
                           <td><?= _ent($master_doc_template->upload_date); ?></td> 
                           <td width="200">
                              <a href="<?= site_url('administrator/anri_master_doc_template/update/' . $master_doc_template->doc_id); ?>" class="btn btn-primary btn-sm"><i class="fa fa-edit "></i></a>
                              <a href="javascript:void(0);" data-href="<?= site_url('administrator/anri_master_doc_template/delete/' . $master_doc_template->doc_id); ?>" class="btn btn-danger btn-sm remove-data"><i class="fa fa-trash"></i></a>
                           </td>
                        </tr>
                      <?php endforeach; ?>
                     </tbody>
                  </table>
                  </div>
               </div>
               </form>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
  $(document).ready(function(){
   
  }); /*end doc ready*/
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });

</script>