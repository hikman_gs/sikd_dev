<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
 <h1><small></small></h1>
 <ol class="breadcrumb">
  <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
  <li class="active">Edit Pengaturan</li>
</ol>
</section>
<!-- Main content -->
<section class="content">
 <div class="row" >
  <div class="col-md-12">
   <div class="box box-warning">
    <div class="box-body ">
     <!-- Widget: user widget style 1 -->
     <div class="box box-widget widget-user-2">
      <!-- Add the bg color to the header using any of the bg-* classes -->
      <div class="widget-user-header ">

        <div class="widget-user-image">
          <img class="img-circle" src="<?= BASE_ASSET; ?>img/add2.png" alt="User Avatar">
        </div>
        <!-- /.widget-user-image -->
        <h3 class="widget-user-username">Edit Data</h3>
        <h5 class="widget-user-desc">Pengumuman</h5>
        <hr>
      </div>
      <!-- <input type="hidden" name="bulk" id="bulk" value="delete"> -->
      <div class="box-body">
        <form action="<?= site_url('administrator/notice/update'); ?>" method="POST" accept-charset="utf-8" class="form-horizontal">
          <input type="hidden" value="<?= $notice['notice_id'] ?>" name="id">
          <div class="form-group <?php if(form_error('priority')){ echo 'has-error';}?>">
            <label for="priority" class="col-sm-2 control-label">Prioritas 
              <i class="required">*</i>
            </label>
            <div class="col-sm-3">
              <select name="priority" id="priority" class="form-control">
                <option value="" <?php if (empty($notice['priority'])) { echo "selected";} ?>>-- Pilih Prioritas --</option>
                <option value="0" <?php if ($notice['priority'] == 0) { echo "selected";} ?>>none</option>
                <option value="1" class="bg-green" <?php if ($notice['priority'] == 1) { echo "selected";} ?>>New Feature</option>
                <option value="2" class="bg-yellow" <?php if ($notice['priority'] == 2) { echo "selected";} ?>>Maintenance</option>
                <option value="3" class="bg-blue" <?php if ($notice['priority'] == 3) { echo "selected";} ?>>Update</option>
              </select>
              <span class="help-block"><b><?= form_error('priority'); ?></b></span>
              <small class="info help-block">
              </small>
            </div>
          </div>
          <div class="form-group <?php if(form_error('date_range')){ echo 'has-error';}?>">
            <label for="" class="col-sm-2 control-label">Tanggal Aktif:
              <i class="required">*</i>
            </label>
            <div class="col-sm-3">
             <div class="input-group">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" class="form-control" id="reservation" name="date_range" autofocus="" value="<?= $date_range = $notice['tgl_awal']." - ".$notice['tgl_akhir']; ?>">
            </div>
            <span class="help-block"><b><?= form_error('date_range'); ?></b></span>
            <small class="info help-block">
            </small>
          </div>
        </div>
        <div class="form-group <?php if(form_error('notice_name')){ echo 'has-error';}?>">
          <label for="notice_name" class="col-sm-2 control-label">Nama Pengumuman 
            <i class="required">*</i>
          </label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="notice_name" id="notice_name" placeholder="Nama Pengumuman" value="<?php if(!empty($notice['notice_name'])){ echo $notice['notice_name']; } ?>">
            <span class="help-block"><b><?= form_error('notice_name'); ?></b></span>
            <small>
              <b>Nama/Judul pengumuman</b></small>
            </div>
          </div>
          <div class="form-group <?php if($notice['priority'] == 0){ echo ''; }else{echo 'hidden';} ?> isidesc">
            <label for="notice_description" class="col-sm-2 control-label">Isi Pengumuman 
              <!-- <i class="required">*</i> -->
            </label>
            <div class="col-sm-10">
              <textarea id="konten" name="notice_description" rows="5" class="textarea form-control mytextarea">
                <?php 
                if (!empty($notice['notice_description'])) {
                 echo $notice['notice_description'];
               }
               ?>
             </textarea>
             <small class="info help-block">
             </small>
           </div>
         </div>
         <div class="box-footer text-center">
          <a href="<?= site_url('administrator/notice'); ?>" class="btn btn-default" title="Pemngumuman list">Kembali</a>
          <button type="submit" class="btn btn-success" value="Simpan" style="margin-left: 10px;"><i class="fa fa-fw fa-save"></i> Simpan</button>
        </div>
      </form>
    </div>
  </div>
</div>
<!--/box body -->
</div>
<!--/box -->
</div>
</div>
</section>
<!-- /.content -->

<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/daterangepicker/moment.js"></script>
<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?= BASE_ASSET; ?>/tinymce/tinymce.min.js"></script>

<script>


  $(function () {
    $("#priority").change(function() {
      var val = $(this).val();
      if(val == 0) {
        $('.isidesc').removeClass('hidden');
      }
      else{
        $('.isidesc').addClass('hidden');
      }
    });
  });

  $(document).ready(function($) 
  {
    $('#reservation').daterangepicker({
      // autoUpdateInput: false,
      locale: {
        cancelLabel: 'Clear'
      }
    });
    
  });

  tinymce.init({
    force_br_newlines: false,
    force_p_newlines: false,
    forced_root_block: '',
    selector: 'textarea',
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    fontfamily_formats: "Arial",
    marginright_formats: 20,
    theme: "modern",
    indentation: 7,
    indent_use_margin: true,
    nonbreaking_force_tab: true,
    paste_data_images: true,
    nonbreaking_wrap: false,
    powerpaste_keep_unsupported_src: true,
    importcss_append: true,
    tabfocus_elements: "somebutton",
    paste_tab_spaces: 0,
    powerpaste_block_drop: true,
    lists_indent_on_tab: true,
    indentation: '20pt',
    indent_use_margin: true,
    paste_as_text: true,


    smart_paste: true,

    plugins: ["tabfocus"],
    tabfocus_elements: ":prev,:next",
    plugins: [
    "searchreplace",
    "paste",
    "textcolor colorpicker textpattern",
    "advlist autolink lists link image charmap print preview hr anchor pagebreak",
    "forecolor backcolor emoticons",
    "nonbreaking table lists",
    "nonbreaking",
    "textpattern",
    "edit",
    "code",
    "save table"
    ],


    height: 200,
    image_caption: true,

    noneditable_noneditable_class: "mceNonEditable",

    paste_postprocess: function(plugin, args) {
      console.log(args.node);
      args.node.setAttribute('konten', '42');
      args.node.setAttribute('lampiran', '42');
    },



    a11y_advanced_options: true,
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link image | fontselect",
    toolbar2: "pastetext | pasteword, | print preview media | numlist bullist checklist |forecolor backcolor emoticons | nonbreaking | formatselect | forecolor backcolor",

    image_advtab: true,


    file_picker_callback: function(callback, value, meta) {
      if (meta.filetype == 'image') {
        $('#upload').trigger('click');
        $('#upload').on('change', function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(e) {
            callback(e.target.result, {
              alt: ''
            });
          };
          reader.readAsDataURL(file);
        });
      }
    },
    formats: {
      // Changes the default format for the underline button to produce a span with a class and not merge that underline into parent spans
      underline: {
        inline: 'span',
        styles: {
          'text-decoration': 'underline'
        },
        exact: true
      },
      strikethrough: {
        inline: 'span',
        styles: {
          'text-decoration': 'line-through'
        },
        exact: true
      }
    },

    style_formats: [{
      title: 'Bold text',
      format: 'h1'
    },
    {
      title: 'Red text',
      inline: 'span',
      styles: {
        color: '#ff0000'
      }
    },
    {
      title: 'Red header',
      block: 'h1',
      styles: {
        color: '#ff0000'
      }
    },
    {
      title: 'Example 1',
      inline: 'span',
      classes: 'example1'
    },
    {
      title: 'Example 2',
      inline: 'span',
      classes: 'example2'
    },
    {
      title: 'Table styles'
    },
    {
      title: 'Table row 1',
      selector: 'tr',
      classes: 'tablerow1'
    }
    ],

    content_style: "body{margin: 3px;background: #1ABC9C; align:justify; padding: 5px 15px 20px 20px;border-radius: 3px;}",
  });
</script>
