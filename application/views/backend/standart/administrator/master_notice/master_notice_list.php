<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Pengumuman</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     <div class="row pull-right">
                        <a class="btn btn-flat btn-success btn_add_new" id="btn_add_new" title="Tambah Data" href="<?=  site_url('administrator/notice/create'); ?>"><i class="fa fa-plus-square-o" ></i> Tambah Data</a>
                     </div>
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Pengaturan</h3>
                     <h5 class="widget-user-desc">Pengumuman</h5>
                     <hr>
                  </div>
                  <!-- <input type="hidden" name="bulk" id="bulk" value="delete"> -->
                  <div class="box-body table-responsive">
                    <table id="table1" class="table table-bordered table-striped table-hover">
                      <thead>
                      <tr>
                        <th>#</th>
                        <th>Nama Pengumuman</th>
                        <th>Prioritas</th>
                        <th>Isi Pengumuman</th>
                        <th>Tanggal Awal</th>
                        <th>Tanggal Akhir</th>
                        <th>Aksi</th>
                      </tr>
                      </thead>
                      <tbody>
                      
                      </tbody>
                    </table>
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->

<script>
   var table1;

  $(document).ready(function(){
        //datatables
        table1 = $('#table1').DataTable({ 
            "processing": true, 
            "serverSide": true, 
            "order": [[ 1, "asc" ]], 
            "ajax": {
                "url": BASE_URL +"/administrator/Notice/get_ajax",
                "type": "POST",
            },
            "columnDefs": [
              {
                 "target":[4,5],
                "className": 'text-center',
              },

              {
                 "target":[0,6],
                  "orderable": false,
              },


            ],
 
        });
  }); /*end doc ready*/    


  
    $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });

</script>