<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a  href="<?= site_url('administrator/anri_dashboard/master_btn'); ?>">Pengaturan Text Tombol</a></li>
        <li class="active">Ubah Data</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Ubah Data</h3>
                            <h5 class="widget-user-desc">Text Tombol</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/anri_dashboard/master_btn_update_save/'.$this->uri->segment(4)), [
                            'name'    => 'form_master_btn', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_master_btn', 
                            'method'  => 'POST'
                            ]); ?>
                        <div class="form-group ">
                            <label for="btn_text" class="col-sm-2 control-label">Nama Tombol 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="35" name="btn_text" id="btn_text" placeholder="Nama Tombol" value="<?= set_value('btn_text', $master_btn->btn_text); ?>" required autofocus>
                                <small class="info help-block">
                                <b>Input Nama Tombol</b> Max Length : 35.</small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="btn_text" class="col-sm-2 control-label">Fungsi Tombol
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="10" name="btn_func" id="btn_func" placeholder="Fungsi Tombol" value="<?= set_value('btn_func', $master_btn->btn_func); ?>" required>
                                <small class="info help-block">
                                <b>Input Fungsi Tombol</b> Max Length : 10.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="btn_desc" class="col-sm-2 control-label">Keterangan Tombol 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="100" name="btn_desc" id="btn_desc" placeholder="Text" value="<?= set_value('btn_desc', $master_btn->btn_desc); ?>" required>
                                <small class="info help-block">
                                <b>Keterangan Tombol</b> Max Length : 100.</small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <input type="submit" name="submit" value="Simpan" class="btn btn-flat btn-danger">
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
      
             
      $('#btn_cancel').click(function(){
        swal({
            title: "Are you sure?",
            text: "the data that you have created will be in the exhaust!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/master_btn';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_master_btn = $('#form_master_btn');
        var data_post = form_master_btn.serializeArray();
        var save_type = $(this).attr('data-stype');
        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: form_master_btn.attr('action'),
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            var id = $('#master_btn_image_galery').find('li').attr('qq-file-id');
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            $('.data_file_uuid').val('');
    
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
       
           
    
    }); /*end doc ready*/
</script>