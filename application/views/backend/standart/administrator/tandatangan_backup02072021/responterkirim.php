<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            Dokumen Terkirim
            
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dokumen Terkirim</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php include('panel.php');?>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=$judul_menu; ?></h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Cari Dokumen" id="myInputTextField">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body padding">
                        <form action="<?= base_url('administrator/tandatangan/signttd_multi'); ?>" class="form-horizontal xwan" method="POST">
						<div class="mailbox-controls">
                            <!-- Check all button --> 
							<!--
                            <div class="btn-group">
                                <button type="submit" class="btn btn-info btn-md" id="submit"><i class="fa  fa-check-circle"></i> Tandatangan Multi File</button> 
                            </div>
							-->
                            <!-- /.pull-right -->
                        </div>
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped" id="lookup1">
                                	<thead>
										<tr>  
											<th>Nama Dokumen</th>  
											<th>Tujuan</th>    
											<th>Status</th>    
											<th>Aksi</th>  
										</tr>
									</thead>
									<tbody> 
                                    <?php foreach ($tte as $ttd) { ?> 
									<tr> 
                                        <td class="mailbox-name">
											<?php echo getfileName1TTD($ttd->ttd_id); ?>
										</td> 
                                        <td class="mailbox-name">
											<?php 
												$data =  getNamaTerkirim2($ttd->ttd_id);  
												foreach ($data as $dt) {
													echo getNamePeTTD($dt->PeopleIDTujuan).'<br>';
												}
											?>
										</td>  
                                        <td class="mailbox-attachment">
											<?php 
												$data =  getNamaTerkirim2($ttd->ttd_id); 

												foreach ($data as $dt) {
													echo ttd_label_dokterkirim($dt->status).'<br>';
												}
											?>  
										</td>
                                        <td class="mailbox-date">
										
											<a href="<?= BASE_URL('administrator/tandatangan/download/'.$ttd->ttd_id) ?>" class="btn btn-info btn-sm" title="Download File" >
												<i class='fa fa-download'></i>
											</a>

											 <!---->
										</td>
                                    </tr> 
									<?php //} ?>
									<?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
						</form>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <div class="mailbox-controls" style="padding:20px">
                             
                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div id="approve_naskah" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Tandatangani Dokumen </b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/signttd_multi'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
                        <div class="form-group"> 
                            <input type="hidden" id="file" name="file"> 
                            <input type="hidden" id="nama_lengkap" name="nama_lengkap" value="<?= $this->session->userdata('peoplename'); ?>">
                            <div class="col-6">
                                <label>Masukan Passphrase</label>
                                <input type="password" min="1" class="form-control" name="password" id="password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
 

  <!-- iCheck -->
<script src="https://adminlte.io/themes/AdminLTE/plugins/iCheck/icheck.min.js"></script>
<!-- Page Script -->
<script>
$(function() {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function() {
        var clicks = $(this).data('clicks');
        if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }
        $(this).data("clicks", !clicks);
    });

    //Handle starring for glyphicon and font awesome
    $(".mailbox-star").click(function(e) {
        e.preventDefault();
        //detect type
        var $this = $(this).find("a > i");
        var glyph = $this.hasClass("glyphicon");
        var fa = $this.hasClass("fa");

        //Switch states
        if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
        }

        if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
        }
    });

});

</script>
<script type="text/javascript">

  
$(document).ready(function(){
 

		// Display the Bootstrap modal
		oTable =  $('#lookup1').DataTable(
			{
				dom: 'Bfrtip',
				"ordering":false,
				"lengthMenu": [[5], [5]],
			}
		);
		 $('#myInputTextField').keyup(function(){
			  oTable.search($(this).val()).draw() ;
		})
});   

$(document).ready(function () {
    $('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        swal("Minimal satu harus dipilih.");
        return false;
      }

    });
});

</script>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td { padding:12px !important}
.dataTables_filter, .dataTables_info { display: none; }

</style>


<script>
    function reqSignature() {

        $("#reqSignature").modal('show');
    }
	
</script>