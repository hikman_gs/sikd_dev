<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            Dokumen Masuk
           
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Menunggu Respons Anda</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php include('panel.php');?>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Yang Perlu Direspons</h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Cari Dokumen" id="myInputTextField">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body padding">
                        <form action="<?= base_url('administrator/tandatangan/signttd_multi/respondokumen'); ?>" class="form-horizontal xwan" method="POST">
						<div class="mailbox-controls">
                            <!-- Check all button --> 

                            <div class="btn-group">
                                <button type="submit" class="btn btn-info btn-md" id="submit"><i class="fa  fa-check-circle"></i> Tandatangan Multi File</button> 
                            </div>
 
                            <!-- /.pull-right -->
                        </div>
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped" id="lookup1">
                                	<thead>
										<tr> 
											<th>
												<button type="button" class="btn btn-default btn-sm checkbox-toggle">
                                                    <i class="fa fa-square-o"></i>                           
                                                </button> 
											</th>  
											<th>Nama Dokumen</th>  
                                            <th>Pengirim</th>  
											<th>Status</th>  
											<th>Tanggal</th>  
											<th>Aksi</th>  
										</tr>
									</thead>
									<tbody>
									<input type="hidden" class="form-control" name="respon" id="respon" value="respondokumen">
                                    <?php foreach ($tte as $ttd) { ?>
									<?php 
										$cek = cekstatussebelumnya($ttd->ttd_id, $ttd->urutan-1);   
                                        
                                        $url1 = 'javascript:void();';
                                        $url2 = '#';
                                        $cekbox = '';
                                        $disabled =''; 

                                        if($cek == '1') {
                                            $url1 = BASE_URL('administrator/tandatangan/detailselfsign/'.$ttd->ttd_id.'/respondokumen'); 
                                            $cekbox = '<input type="checkbox" class="filename" name="filename[]" value="'.getfileNameTTD($ttd->ttd_id).'" >';
                                            $disabled = 'onClick="submitDetailsForm('.$ttd->ttd_id.')"';
                                            $disabledTolakTTD = 'onClick="submitTolakForm('.$ttd->id.')"';
                                        } elseif ($ttd->status == '0' AND $ttd->urutan=='1') {
                                            $url1 = BASE_URL('administrator/tandatangan/detailselfsign/'.$ttd->ttd_id.'/respondokumen'); 
                                            $cekbox = '<input type="checkbox" class="filename" name="filename[]" value="'.getfileNameTTD($ttd->ttd_id).'" >';
                                            $disabled = 'onClick="submitDetailsForm('.$ttd->ttd_id.')"';
											$disabledTolakTTD = 'onClick="submitTolakForm('.$ttd->id.')"';
                                        }   elseif ($ttd->status == '1') {
                                            $url1 = ''; 
                                            $cekbox = '';
                                            $disabled = '';
                                        }  
                                    ?> 
									<?php if($cek == '1') { ?>
                                    <tr>
                                        <td> 
										<?php echo ($ttd->status=='0')?$cekbox:''; ?>  
                                        <input type="hidden" class="filename" name="filenamex[]" value="<?php echo getfileNameTTD($ttd->ttd_id); ?>" > 										
										</td>
                                        <td class="mailbox-name">
											<?php echo getfileName1TTD($ttd->ttd_id); ?>
										</td>
                                        <td class="mailbox-sender">
                                            <?php echo get_konseptor($ttd->PeopleID); ?>
                                        </td>
                                        <td class="mailbox-attachment"><?php echo ttd_label($ttd->status); ?></td>
                                        <td class="mailbox-date">
											<?php echo $ttd->tgl; ?>
										</td>
                                        <td class="mailbox-date">
											 
											
											<a href="<?= $url1; ?>" class="btn btn-warning btn-sm" title="Tandatangani Drag and Drop" class="btn btn-warning btn-sm"><i class="fa fa-pencil "></i></a>
											<button type="button" class="btn bg-maroon btn-sm" title="Tandatangani Invisible" data-id="<?=$ttd->ttd_id ?>"  <?php echo $disabled; ?>><i class="fa fa-pencil "></i></button>
											
											<button type="button" class="btn bg-purple btn-sm" title="Tolak Tandatangani" data-id="<?=$ttd->ttd_id ?>"  <?php echo $disabledTolakTTD; ?>><i class="fa fa-fw fa-hand-paper-o"></i></button>
											<a href="<?= BASE_URL('administrator/tandatangan/dokumendetail/'.$ttd->ttd_id) ?>" class="btn btn-info btn-sm"  title="Detail Dokumen" ><i class='fa fa-info'></i> </a>
											<a href="<?= BASE_URL('administrator/tandatangan/download/'.$ttd->ttd_id) ?>" class="btn btn-primary btn-sm" title="Download File" >
												<i class='fa fa-download'></i>
											</a>
										</td>
                                    </tr> 
                                    <?php } elseif ($ttd->status == '0' AND $ttd->urutan=='1') { ?>
                                        <tr>
                                        <td> 
										<?php echo $cekbox; ?>  
                                        <input type="hidden" class="filename" name="filenamex[]" value="<?php echo getfileNameTTD($ttd->ttd_id); ?>" > 										
										</td>
                                        <td class="mailbox-name">
											<?php echo getfileName1TTD($ttd->ttd_id); ?>
										</td>
                                        <td class="mailbox-sender">
											<?php echo get_konseptor($ttd->PeopleID); ?>
										</td>
                                        <td class="mailbox-attachment"><?php echo ttd_label($ttd->status); ?></td>
                                        <td class="mailbox-date">
											<?php echo $ttd->tgl; ?>
										</td>
                                        <td class="mailbox-date">
											
											<a href="<?= $url1; ?>" class="btn btn-warning btn-sm" title="Tandatangani Drag and Drop" class="btn btn-warning btn-sm"><i class="fa fa-pencil "></i></a>
											<button type="button" class="btn bg-maroon btn-sm" title="Tandatangani Invisible" data-id="<?=$ttd->ttd_id ?>"  <?php echo $disabled; ?>><i class="fa fa-pencil "></i></button>
											
											<button type="button" class="btn bg-purple btn-sm" title="Tolak Tandatangani" data-id="<?=$ttd->ttd_id ?>"  <?php echo $disabledTolakTTD; ?>><i class="fa fa-fw fa-hand-paper-o"></i></button>
											<a href="<?= BASE_URL('administrator/tandatangan/dokumendetail/'.$ttd->ttd_id) ?>" class="btn btn-info btn-sm"  title="Detail Dokumen" ><i class='fa fa-info'></i> </a>
											<a href="<?= BASE_URL('administrator/tandatangan/download/'.$ttd->ttd_id) ?>" class="btn btn-primary btn-sm" title="Download File" >
												<i class='fa fa-download'></i>
											</a>
										</td>
                                    </tr> 
                                    <?php  }   elseif ($ttd->status == '1') { ?>
                                        <tr>
                                        <td> 
										<?php echo $cekbox; ?>  
                                        <input type="hidden" class="filename" name="filenamex[]" value="<?php echo getfileNameTTD($ttd->ttd_id); ?>" > 										
										</td>
                                        <td class="mailbox-name">
											<?php echo getfileName1TTD($ttd->ttd_id); ?>
										</td>
                                        <td class="mailbox-sender">
											<?php echo get_konseptor($ttd->PeopleID); ?>
										</td>
                                        <td class="mailbox-attachment"><?php echo ttd_label($ttd->status); ?></td>
                                        <td class="mailbox-date">
											<?php echo $ttd->tgl; ?>
										</td>
                                        <td class="mailbox-date">
											<a href="<?= $url1; ?>" class="btn btn-warning btn-sm" title="Tandatangani Drag and Drop" class="btn btn-warning btn-sm"><i class="fa fa-pencil "></i></a>
											<button type="button" class="btn bg-maroon btn-sm" title="Tandatangani Invisible" data-id="<?=$ttd->ttd_id ?>"  <?php echo $disabled; ?>><i class="fa fa-pencil "></i></button>
											
											<a href="<?= BASE_URL('administrator/tandatangan/dokumendetail/'.$ttd->ttd_id) ?>" class="btn btn-info btn-sm"  title="Detail Dokumen" ><i class='fa fa-info'></i> </a>
											<a href="<?= BASE_URL('administrator/tandatangan/download/'.$ttd->ttd_id) ?>" class="btn btn-primary btn-sm" title="Download File" >
												<i class='fa fa-download'></i>
											</a>
										</td>
                                    </tr> 
                                    <?php }  ?>
									<?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
						</form>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <div class="mailbox-controls" style="padding:20px">
                             
                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div id="approve_naskah" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Tandatangani Dokumen </b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/signttd_multi'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
                        <div class="form-group"> 
                            <input type="hidden" id="file" name="file"> 
                            <input type="hidden" id="nama_lengkap" name="nama_lengkap" value="<?= $this->session->userdata('peoplename'); ?>">
                            <div class="col-6">
                                <label>Masukan Passphrase</label>
                                <input type="password" min="1" class="form-control" name="password" id="password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="signatureInvisible" class="modal fade" role="dialog">

    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Tandatangan Invisible</b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/signttd_/respondokumen'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
 
						
                        <div class="form-group">
                            <div class="col-12"> 
                                <input type="hidden"  class="form-control" name="filename" id="filename" > 
                            </div>
							<div class="col-12">
                                <label>Masukan Passphrase</label>
                                <input type="password" min="1" class="form-control" name="password" id="password" required>
                            </div>
                        </div> 
 
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title" style="font-size:14px;">Detail Dokumen</h3>
            </div>
            <div class="modal-body form">
			<div class="row">
				<div class="col-lg-12">
				<form action="#" id="form" class="form-horizontal"> 
                    <div class="form-body">
						<div class="row">
							<div class="col-xs-12">
								<div id="namaDokumen"></div>
								<div id="PeopleIDTujuan"></div>
									
							</div>
						</div>
					</div> 
                </form>
				</div>
			</div>
			</div> 
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- End Bootstrap modal -->



<div id="reqTolakTTD" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Tolak Untuk Ditandatangani </b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/tolakttd'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
                         <div class="form-group">
                            <div class="col-12">
                                <label>Catatan</label>
                                <input type="hidden"  class="form-control" name="idfile" id="idfile" >  
								<textarea class="form-control" rows="3" placeholder="Catatan ..."  name="catatan" id="catatan" ></textarea>
                            </div>
                        </div> 
 
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
  <!-- iCheck -->
<script src="https://adminlte.io/themes/AdminLTE/plugins/iCheck/icheck.min.js"></script>
<!-- Page Script -->
<script>
$(function() {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function() {
        var clicks = $(this).data('clicks');
        if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }
        $(this).data("clicks", !clicks);
    });

    //Handle starring for glyphicon and font awesome
    $(".mailbox-star").click(function(e) {
        e.preventDefault();
        //detect type
        var $this = $(this).find("a > i");
        var glyph = $this.hasClass("glyphicon");
        var fa = $this.hasClass("fa");

        //Switch states
        if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
        }

        if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
        }
    });

});

</script>
<script type="text/javascript">

  
$(document).ready(function(){
 

		// Display the Bootstrap modal
		oTable =  $('#lookup1').DataTable(
			{
				dom: 'Bfrtip',
				"ordering":false,
				"lengthMenu": [[5], [5]],
			}
		);
		 $('#myInputTextField').keyup(function(){
			  oTable.search($(this).val()).draw() ;
		});
		
		

});   
	 function submitDetailsForm(idx) {
		//var idx = $(this).data('id');
		$('#filename').val(idx);
        $("#signatureInvisible").modal('show');
    } 
	 function submitTolakForm(idx) {
		//var idx = $(this).data('id');
		$('#idfile').val(idx);
        $("#reqTolakTTD").modal('show');
    } 
$(document).ready(function () {
    $('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        swal("Minimal satu harus dipilih.");
        return false;
      }

    });
	
	
	
});
	function edit_m(id) {  
	
		//Ajax Load data from ajax
		$.ajax({
			url : "<?php echo BASE_URL; ?>administrator/tandatangan/detail_dokumen/" + id,
			type: "GET",
			dataType: "JSON",
			success: function(data)
			{
		
				$('#namaDokumen').html(data.PeopleIDTujuan);
				$('#PeopleIDTujuan').html(data.PeopleIDTujuan);
				console.log(data.PeopleIDTujuan); 
				$('#modal_form').modal('show');
			},
			error: function (jqXHR, textStatus, errorThrown)
			{
				alert('Error get data from ajax');
			}
		});
	}
</script>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td { padding:12px !important}
.dataTables_filter, .dataTables_info { display: none; }

</style>


<script>
    function reqSignature() {

        $("#reqSignature").modal('show');
    }
	
</script>
