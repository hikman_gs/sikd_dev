<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>js/jquery.hotkeys.js"></script> 

<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            Dokumen Detail 
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dokumen</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
		<?php include('panel.php');?>
			<div class="col-md-9"> 
				 <div class="box box-primary">
					 <div class="box-header with-border">
						 <h3 class="box-title">
							
						</h3>
					 </div>
					 <!-- /.box-header -->
					 <div class="box-body"> 
			<div class="row">
			<div class="col-md-12">
<table class="table table-bordered">
                <tbody><tr> 
                  <th>Konseptor</th>
                  <th><?php echo getNamePeTTD($PeopleID);?></th> 
                </tr>
				
                <tr>
                  <td>Nama Dokumen</td>
                  <td><?php echo $namadokumen;?></td> 
                </tr>
                <tr>
                  <td>Status Dokumen</td>
                  <td><?php echo $status;?></td> 
                </tr>
                <tr>
                  <td>File </td>
                  <td><a href="<?= BASE_URL('administrator/tandatangan/download/'.$idx) ?>" class="btn btn-info btn-sm" title="Download File" >
												<i class='fa fa-download'></i>  Download File
											</a></td> 
                </tr> 
                
              </tbody></table>
			</div>
		</div>
			<div class="row">
			<div class="col-md-12"> 
				<h3 class="box-title">Penanda Tangan</h3>  
				
				
				<table class="table table-bordered">
                <tbody>
				<tr> 
                  <th style="width:5px">#</th>
                  <th>Penandatangan</th> 
                  <th>Tanggal Tandatangan</th> 
                </tr>
				<?php $no=1; ?>  
				<?php if (isset($results->details)) { ?> 
				<?php foreach ($results->details as $detail) { ?> 
				<?php $d = $detail->info_signer; ?>
				<?php $sd = $detail->signature_document; ?>
				<tr> 
                  <td><?php echo $no++;?>.</td>
                  <td><?php echo $d->signer_name; ?></td> 
                  <td><?php echo ($sd->signed_in); ?></td> 
                </tr>
				<?php } ?> 
								<?php } else { ?>
                 	<tr> 
                  <td colspan="5">Belum ada data</td> 
                </tr>
				<?php } ?>
              </tbody></table>
			</div> 
			
			<div class="col-md-12"> 
				<h3 class="box-title">Diteruskan untuk Ditandatangani</h3>
 
				<table class="table table-bordered">
                <tbody>
				<tr> 
                  <th style="width:5px">#</th>
                  <th>Penerima</th>
                  <th>Status</th> 
                  <th>Tanggal Tandatangan</th>
                  <th>Catatan</th> 
                  <th>Urutan TTD</th> 
                </tr>
				<?php $no=1; ?> 
				<?php if ($result) { ?> 
				<?php foreach ($result as $ttd) { ?> 

				<tr> 
                  <td><?php echo $no++;?></td>
                  <td><?php echo getNamePeTTD($ttd->PeopleIDTujuan);?></td>
                  <td><?php echo ttd_label($ttd->status);?></td>
                  <td><?php echo $ttd->tgl_ttd;?></td>
                  <td><?php echo $ttd->catatan;?></td> 
                  <td><?php echo $ttd->urutan;?></td> 
                </tr>
				<?php } ?>
				<?php } else { ?>
                 	<tr> 
                  <td colspan="5">Belum ada data</td> 
                </tr>
				<?php } ?>
				
              </tbody></table>
			</div> 

			<div class="col-md-12"> 
				<h3 class="box-title">Diteruskan untuk Dikirim</h3>
 
				<table class="table table-bordered">
                <tbody>
				<tr> 
                  <th style="width:5px">#</th>
                  <th>Diteruskan Ke</th>
                  <th>Status</th>  
                </tr>
				<?php $no=1; ?> 
				<?php if ($m_ttd_terusankirim) { ?> 
				<?php foreach ($m_ttd_terusankirim as $ttd) { ?> 

				<tr> 
                  <td><?php echo $no++;?></td>
                  <td><?php echo getNamePeTTD($ttd->PeopleIDTujuan);?></td>
                  <td><?php echo ttd_label_dokterkirim($ttd->status);?></td> 
                </tr>
				<?php } ?>
				<?php } else { ?>
                 	<tr> 
                  <td colspan="5">Belum ada data</td> 
                </tr>
				<?php } ?>
				
              </tbody></table>
			</div> 
			
		</div> 
						
						
						
					</div>
					 <!-- /.box-footer -->
				 </div>
				 <!-- /. box -->
			 </div>
			 <!-- /.col --> 
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

