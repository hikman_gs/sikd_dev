<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<!-- File untuk konfigurasi fine-upload -->
<?php $this->load->view('core_template/fine_upload'); ?>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<style>
    .select2-container--default .select2-selection--single{border:0px;}
    .select2-container{
            border: 1px solid #DEDCDC!important;
            border-radius: 5px!important;
            padding: 2px 0px!important;
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) --> 
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            Dokumen
            <small><?php echo $jumlahfile;?>  Dokumen</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dokumen</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php include('panel.php');?>
			 <div class="col-md-9">
				 <div class="box box-primary">
					 <div class="box-header with-border">
						 <h3 class="box-title">
							Upload Dokumen baru   
						</h3>
					 </div>
					 <!-- /.box-header -->
					 <div class="box-body">
						<?= form_open_multipart(BASE_URL('administrator/tandatangan/upload1'), ['name'    => 'ttd', 'class'   => 'form-horizontal', 'id'=> 'ttd', 'method'  => 'POST']); ?>
                        <div class="form-group "> 
                             <div class="col-sm-12">
                                <div id="test_title_galery"></div>
                                <div id="test_title_galery_listed"></div>
                                <small class="info help-block"></small>
                            </div>
                        </div> 
						<div class="form-group "> 
                             <div class="col-sm-12">
                                <div class="message"></div>
                            </div>
                        </div> 
					 </div>
					 <!-- /.box-body -->
					 <div class="box-footer">
						 <div> 
							 <button class="btn btn-primary btn-flat"> Simpan</button>
						 </div> 
					 </div>
					 <?= form_close(); ?>
					 <!-- /.box-footer -->
				 </div>
				 <!-- /. box -->
			 </div>
			 <!-- /.col -->
  
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<div id="getData" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">
				<b>Cari Dokumen </b>
			</h4>
		</div>
		<form action="
			<?= base_url('administrator/tandatangan/getFile'); ?>" class="form-horizontal" method="POST">
			<div class="modal-body">
				<div class="content">
					<div class="form-group">
						
							<div class="col-6">
								<table id="lookup" class="table table-bordered table-striped table-hover dataTable" cellspacing="0" width="100%">
									<thead>
										<tr> 
											<th>#</th>  
											<th>Hal</th>  
											<th>Nama File</th>  
										</tr>
									</thead>
									<tbody>
									<?php
										
										$query = $this->db->query("SELECT * FROM `inbox` JOIN inbox_files ON inbox.NId = inbox_files.NId WHERE PeopleID = ".$this->session->userdata('peopleid'));
										foreach ($query->result() as $row) {
									?>
											<tr> 
												<td>
													<input type="checkbox" class="filename" name="filename[]" value="<?php echo $row->NFileDir.'_dan_'.$row->FileName_fake.'_dan_'.$row->Hal; ?>">
												</td> 
												<td title="<?php echo $row->Hal; ?>">
													<?php echo word_limiter($row->Hal, 7); ?>
												</td> 
												<td><?php echo $row->FileName_fake; ?></td> 
											</tr>
									<?php
										}
									?></tbody>
								</table>
							</div>
						</div>
						<div class="form-group">
							<div class="col-6">
								<button type="submit" class="btn btn-success" id="submit">Ambil File</button>
								<button type="button" class="btn btn-primary cancel">Batal</button>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

</div>
<script>
    
$(document).ready(function() {
	
    var params = {};
    params[csrf] = token;
    $('#test_title_galery').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: BASE_URL + '/administrator/tandatangan/upload_ttd_file',
            params: params
        },
        deleteFile: {
            enabled: true,
            endpoint: BASE_URL + '/administrator/tandatangan/delete_ttd_file',
        },
        thumbnails: {
            placeholders: {
                waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['pdf'],
            sizeLimit: 0,

        },
        showMessage: function(msg) {
            toastr['error'](msg);
        },
        callbacks: {
            onComplete: function(id, name, xhr) {
                if (xhr.success) {
                    var uuid = $('#test_title_galery').fineUploader('getUuid', id);
					var nama = $('#test_title_galery').fineUploader('getName', parseInt(id, 10));
					var ukuran = $('#test_title_galery').fineUploader('getSize', parseInt(id, 10));
                    $('#test_title_galery_listed').append('<input type="hidden" class="listed_file_uuid" name="test_title_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="test_title_name[' + id + ']" value="' + xhr.uploadName + '" /><input type="hidden" class="nama_file" name="nama_file[' + id + ']" value="' + nama + '" /><input type="hidden" class="ukuran" name="ukuran[' + id + ']" value="' + ukuran + '" />');
					
					console.log(this.getSize(id)); // Works
					console.log(this.getName(id)); // Works
					console.log(xhr.uploadName); // Works
					console.log(uuid); // Works
                } else {
                    toastr['error'](xhr.error);
                }
            },
            onDeleteComplete: function(id, xhr, isError) {
                if (isError == false) {
                    $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid[' + id + ']"]').remove();
                    $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name[' + id + ']"]').remove();
                }
            }
        }
    }); /*end title galery*/
}); /*end doc ready*/

</script>
<script type="text/javascript">

$(document).ready(function(){
 
	$('.view_data').click(function(){
		$('#getData').modal('show');
		$('#lookup').DataTable( {
			destroy: true,
			searching: true,
			ordering:false,
			"lengthMenu": [[5,10], [5,10]],
		} );
		
	});  
        // Close modal on button click
        $(".cancel").click(function(){
            $("#getData").modal('hide');
        });	
});   


</script>

 <script type="text/javascript">
$(document).ready(function () {
    $('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        swal("Minimal satu harus dipilih.");
        return false;
      }

    });
});

</script>
