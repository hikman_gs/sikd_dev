
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
//This page is a result of an autogenerated content made by running test.html with firefox.
function domo(){
 
   // Binding keys
   $('*').bind('keydown', 'Ctrl+e', function assets() {
      $('#btn_edit').trigger('click');
       return false;
   });

   $('*').bind('keydown', 'Ctrl+x', function assets() {
      $('#btn_back').trigger('click');
       return false;
   });
    
}


jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1>
      Tingkat Urgensi      <small><?= cclang('detail', ['Tingkat Urgensi']); ?> </small>
   </h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class=""><a  href="<?= site_url('administrator/master_urgensi'); ?>">Tingkat Urgensi</a></li>
      <li class="active"><?= cclang('detail'); ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">

               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                    
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Tingkat Urgensi</h3>
                     <h5 class="widget-user-desc">Detail Tingkat Urgensi</h5>
                     <hr>
                  </div>

                 
                  <div class="form-horizontal" name="form_master_urgensi" id="form_master_urgensi" >
                   
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">UrgensiId </label>

                        <div class="col-sm-8">
                           <?= _ent($master_urgensi->UrgensiId); ?>
                        </div>
                    </div>
                                         
                    <div class="form-group ">
                        <label for="content" class="col-sm-2 control-label">UrgensiName </label>

                        <div class="col-sm-8">
                           <?= _ent($master_urgensi->UrgensiName); ?>
                        </div>
                    </div>
                                        
                    <br>
                    <br>

                    <div class="view-nav">
                        <?php is_allowed('master_urgensi_update', function() use ($master_urgensi){?>
                        <a class="btn btn-flat btn-info btn_edit btn_action" id="btn_edit" data-stype='back' title="edit master_urgensi (Ctrl+e)" href="<?= site_url('administrator/master_urgensi/edit/'.$master_urgensi->UrgensiId); ?>"><i class="fa fa-edit" ></i> <?= cclang('update', ['Master Urgensi']); ?> </a>
                        <?php }) ?>
                        <a class="btn btn-flat btn-default btn_action" id="btn_back" title="back (Ctrl+x)" href="<?= site_url('administrator/master_urgensi/'); ?>"><i class="fa fa-undo" ></i> <?= cclang('go_list_button', ['Master Urgensi']); ?></a>
                     </div>
                    
                  </div>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->

      </div>
   </div>
</section>
<!-- /.content -->
