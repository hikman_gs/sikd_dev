<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>

<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<style>
  .select2-container--default .select2-selection--single {
    border: 0px;
  }

  .select2-container {
    border: 1px solid #DEDCDC !important;
    border-radius: 5px !important;
    padding: 2px 0px !important;
  }

  .chosen-container-single,
  .chosen-container-multi {
    width: 100% !important;
  }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1><small></small></h1>
  <ol class="breadcrumb">
    <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
    <li class="active"><?= $title; ?></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-warning">
        <div class="box-body ">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header ">
              <div class="widget-user-image">
                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username">Pencatatan Surat Edaran</h3>
              <h5 class="widget-user-desc">&nbsp;</h5>
              <hr>
            </div>
            <form id="myfrm" class="form-horizontal" method="POST">
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="<?= date('d-m-Y') ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Pembuat Naskah</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="<?= $this->session->userdata('peoplename'); ?>">
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Jenis Naskah</label>
                <div class="col-sm-8">
                  <input type="text" disabled class="form-control" value="Surat Edaran">
                </div>
              </div>
             <div class="form-group <?php if (form_error('MeasureUnitId')) {
                                        echo 'has-error';
                                      } ?>">
                <label for="title" class="col-sm-2 control-label">Lampiran</label>
                <div class="col-md-2">
                  <select name="MeasureUnitId" class="form-control" id="MeasureUnitId" required="">
                    <option value=""></option>
                    <?php foreach ($this->db->query('SELECT * FROM master_satuanunit')->result() as $data) { ?>
                      <option value="<?= $data->MeasureUnitId; ?>"><?= $data->MeasureUnitName; ?></option>
                    <?php } ?>

                  </select>
                  <span class="help-block"><b><?= form_error('MeasureUnitId'); ?></b></span>
                </div>
                <div class="col-md-2">
                  <input type="number" class="form-control" name="Jumlah" id="Jumlah" min="0" id="Jumlah">
                </div>
              </div>
            
              <!-- Tambahan Fazri Nomor Naskah manual 2020 -->
              <div class="form-group" >
                <label for="title" class="col-sm-2 control-label">Unit Pengolah<sup class="text-danger">*</sup></label>
                <div class="col-md-8">
                  <input type="text" class="form-control" name="RoleCode" id="RoleCode" value="<?= set_value('RoleCode') ?>" placeholder="Unit Pengolah">
                </div>
              </div>
              <!-- Tambahan Fazri Nomor Naskah manual 2020 -->

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Kode Klasifikasi <sup class="text-danger">*</sup></label>
                <div class="col-sm-8">
                  <select name="ClId" id="ClId" class="form-control select2">
                    <option value="">-- Pilih --</option>
                    <?php foreach ($this->db->query("SELECT * FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result() as $data) { ?>
                      <option value="<?= $data->ClId; ?>"><?= $data->ClCode; ?> - <?= $data->ClName; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>

              <div class="form-group ">
                <label for="UrgensiId" class="col-sm-2 control-label">Tingkat Urgensi <sup class="text-danger">*</sup></label>
                <div class="col-sm-2">
                  <select name="UrgensiId" id="UrgensiId" class="form-control select2">
                    <option value="">-- Pilih --</option>
                    <?php
                    $urg = $this->db->query("SELECT * FROM master_urgensi ORDER BY UrgensiName ASC")->result();
                    foreach ($urg as $data_urg) {
                    ?>
                      <option value="<?= $data_urg->UrgensiId; ?>"><?= $data_urg->UrgensiName; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Sifat Naskah <sup class="text-danger">*</sup></label>
                <div class="col-sm-2">
                  <select name="SifatId" id="SifatId" class="form-control select2">
                    <option value="">-- Pilih --</option>
                    <?php foreach ($this->db->query('SELECT * FROM master_sifat ORDER BY SifatName ASC')->result() as $data) { ?>
                      <option value="<?= $data->SifatId; ?>"><?= $data->SifatName; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Hal <sup class="text-danger">*</sup></label>
                <div class="col-sm-8">
                  <input type="text" class="form-control" name="Hal" id="Hal" placeholder="Hal">
                </div>
              </div>

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tujuan Surat Edaran <sup class="text-danger">*</sup></label>
                <div class="col-sm-8">
                  <textarea type="input" class="form-control" name="RoleId_To" id="RoleId_To" value=" <?= set_value('RoleId_To') ?>" placeholder="Tujuan Surat Dinas">Tujuan Surat Dinas</textarea>

                </div>
              </div>

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Alamat Surat Edaran</label>
                <div class="col-sm-8">
                  <textarea name="alamat" id="alamat" class="form-control value=" <?= set_value('alamat') ?>" placeholder="Alamat" required></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Konsep Surat Edaran <sup class="text-danger">*</sup></label>
                <div class="col-sm-8">
                  <textarea name="Konten" id="Konten" class="form-control mytextarea" cols="50" rows="20" placeholder="Isi"></textarea>
                </div>
              </div>

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Tembusan</label>
                <div class="col-sm-7">
                  <select name="RoleId_Cc[]" class="form-control select2" id="RoleId_Cc" multiple tabi-ndex="5">

                    <!-- Perbaikan Eko 09-11-2019 -->
                    <?php
                    $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId =('3') AND p.PeopleIsActive = '1' AND p.PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY p.PrimaryRoleId ASC")->result();
                    foreach ($query as $dat_people) {
                    ?>
                      <option value="<?= $dat_people->PrimaryRoleId; ?>"><?= $dat_people->PeoplePosition; ?></option>
                    <?php
                    }
                    ?>
                    <!-- batas akhir perbaikan -->

                  </select>
                </div>
              </div>

              <div class="form-group">
                <label for="title" class="col-sm-2 control-label">Pemeriksa <sup class="text-danger">*</sup></label>
                <div class="col-sm-2">
                  <select name="TtdText" class="form-control select2 TtdText" required>
                    <?php foreach ($this->db->query('SELECT * FROM master_ttd')->result() as $data1) { ?>
                      <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                    <?php } ?>
                  </select>
                </div>
                
                <?php
                $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId IN ('3','4') AND p.PeopleIsActive = '1' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
                foreach ($AtasanId as $dataz) {
                  $xx1 = $dataz->PeopleId;
                }
                ?>
                <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $xx1; ?>">

                <?php
                $Sekdis = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.GroupId = 4 AND p.PeopleIsActive = '1'")->result();
                foreach ($Sekdis as $dataz) {
                  $xx2 = $dataz->PeopleId;
                }
                ?>
                <input type="hidden" id="tmpid_sekdis" name="tmpid_sekdis" value="<?= $xx2; ?>">
                

                <div class="col-md-6 Approve_People hidden">
                  <select name="Approve_People" id="Approve_People" class="form-control">
                    <!-- Perbaikan DISPU 2020 -->
                    <?php
                    $query = $this->db->query("SELECT * FROM  v_login WHERE RoleAtasan NOT IN ('','-') AND GroupId IN ('3','4') AND PeopleIsActive = '1' AND GroleId = '" . $this->session->userdata('groleid') . "' ORDER BY GroleId ASC")->result();
                    foreach ($query as $dat_people) {
                    ?>
                      <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeoplePosition; ?></option>
                    <?php
                    }
                    ?>
                    <!-- batas akhir perbaikan -->
                  </select>
                </div>
              </div>

              <div class="form-group ">
                <label for="title" class="col-sm-2 control-label">Unggah Data </label>
                <div class="col-sm-8">
                  <div id="test_title_galery"></div>
                  <div id="test_title_galery_listed"></div>
                  <small class="info help-block">
                  </small>
                </div>
              </div>
              <br>
              <div class="message"></div>
              <div class="row-fluid col-md-7">
                <br>
                <a title="Lihat Naskah" onclick="lihat_naskah();" class="btn btn-flat btn-success btn-lihatnaskah" target="_blank">Lihat Konsep Surat Edaran</a>
                <button onclick="simpan_naskah();" class="btn btn-danger">Simpan</button>
              </div>
            </form>
          </div>
        </div>
        <!--/box body -->
      </div>
      <!--/box -->
    </div>
  </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
  $("#RoleId_Cc").on("select2:select select2:unselect", function(e) {
    var items = $(this).val();
    var ip = e.params.data.id;
    var tujuan = $("#RoleId_To").val();

    $.each(tujuan, function(i, item) {
      $.each(items, function(k, item2) {
        if (item == item2) {
          items.splice(k, 1);
          $("#RoleId_Cc").val(items).change();
          return false;
        }
      })
    })
  });

  $(' .TtdText').change(function() {
    if ($(this).find(':selected').val() == 'none') {
      $('.Approve_People').addClass('hidden');
    } 
    else {
      $('.Approve_People').removeClass('hidden');
      r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
      $('#Approve_People').select2();
      $('#Approve_People').attr('disabled', false);
      
      if (this.value == "AL") {
        idatasan = $("#tmpid_atasan").val();
        $('#Approve_People').val(idatasan).change();
        $('#Approve_People').attr('disabled', true);
      }
    }
  });


  $(document).ready(function() {

      $('#MeasureUnitId').change(function() {
      // alert(',' + $('#MeasureUnitId').find('option:selected').text() + ', ');
      if ($(this).find(':selected').val() == 'XxJyPn38Yh.6') {
        $('#Jumlah').prop("disabled", true);
      } else {
        $('#Jumlah').prop("disabled", false);
      }
    });

    $('.select2').select2();

    var params = {};
    params[csrf] = token;
    $('#test_title_galery').fineUploader({
      template: 'qq-template-gallery',
      request: {
        endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
        params: params
      },
      deleteFile: {
        enabled: true,
        endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
      },
      thumbnails: {
        placeholders: {
          waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
          notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
        }
      },
      validation: {
        allowedExtensions: ["*"],
        sizeLimit: 0,

      },
      showMessage: function(msg) {
        toastr['error'](msg);
      },
      callbacks: {
        onComplete: function(id, name, xhr) {
          if (xhr.success) {
            var uuid = $('#test_title_galery').fineUploader('getUuid', id);
            $('#test_title_galery_listed').append('<input type="hidden" class="listed_file_uuid" name="test_title_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="test_title_name[' + id + ']" value="' + xhr.uploadName + '" />');
          } else {
            toastr['error'](xhr.error);
          }
        },
        onDeleteComplete: function(id, xhr, isError) {
          if (isError == false) {
            $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid[' + id + ']"]').remove();
            $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name[' + id + ']"]').remove();
          }
        }
      }
    }); /*end title galery*/
  }); /*end doc ready*/
</script>

<script src="<?= BASE_ASSET; ?>/tinymce/tinymce.min.js"></script>

<script>
  tinymce.init({
    force_br_newlines: false,
    force_p_newlines: false,
    forced_root_block: '',
    selector: 'textarea',
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    fontfamily_formats: "Arial",
    marginright_formats: 20,
    theme: "modern",
    indentation: 7,
    indent_use_margin: true,
    nonbreaking_force_tab: true,
    paste_data_images: true,
    nonbreaking_wrap: false,
    powerpaste_keep_unsupported_src: true,
    importcss_append: true,
    tabfocus_elements: "somebutton",
    paste_tab_spaces: 0,
    powerpaste_block_drop: true,
    lists_indent_on_tab: true,
    indentation: '20pt',
    indent_use_margin: true,
    paste_as_text: true,


    smart_paste: true,

    plugins: ["tabfocus"],
    tabfocus_elements: ":prev,:next",
    plugins: [
      "searchreplace",
      "paste",
      "textcolor colorpicker textpattern",
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "forecolor backcolor emoticons",
      "nonbreaking table lists",
      "nonbreaking",
      "textpattern",
      "edit",
      "code",
      "save table"
    ],


    height: 200,
    image_caption: true,

    noneditable_noneditable_class: "mceNonEditable",

    paste_postprocess: function(plugin, args) {
      console.log(args.node);
      args.node.setAttribute('konten', '42');
    },



    a11y_advanced_options: true,
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | outdent indent | link image | fontselect",
    toolbar2: "pastetext | pasteword, | print preview media | numlist bullist checklist |forecolor backcolor emoticons | nonbreaking | formatselect | forecolor backcolor",

    image_advtab: true,


    file_picker_callback: function(callback, value, meta) {
      if (meta.filetype == 'image') {
        $('#upload').trigger('click');
        $('#upload').on('change', function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.onload = function(e) {
            callback(e.target.result, {
              alt: ''
            });
          };
          reader.readAsDataURL(file);
        });
      }
    },
    formats: {
      // Changes the default format for the underline button to produce a span with a class and not merge that underline into parent spans
      underline: {
        inline: 'span',
        styles: {
          'text-decoration': 'underline'
        },
        exact: true
      },
      strikethrough: {
        inline: 'span',
        styles: {
          'text-decoration': 'line-through'
        },
        exact: true
      }
    },

    style_formats: [{
        title: 'Bold text',
        format: 'h1'
      },
      {
        title: 'Red text',
        inline: 'span',
        styles: {
          color: '#ff0000'
        }
      },
      {
        title: 'Red header',
        block: 'h1',
        styles: {
          color: '#ff0000'
        }
      },
      {
        title: 'Example 1',
        inline: 'span',
        classes: 'example1'
      },
      {
        title: 'Example 2',
        inline: 'span',
        classes: 'example2'
      },
      {
        title: 'Table styles'
      },
      {
        title: 'Table row 1',
        selector: 'tr',
        classes: 'tablerow1'
      }
    ],

    content_style: "body{margin: 3px;background: #1ABC9C; align:justify; padding: 5px 15px 20px 20px;border-radius: 3px;}",
  });
</script>

<script>
  function lihat_naskah() {

   if ($("#ClId").val() == "") {
      alert("Kode Klasifikasi Wajib Diisi");
      myfrm.ClId.focus();
    } else if ($("#MeasureUnitId").val() == "") {
      alert("Lampiran Wajib Dipilih, pilih strip (-) jika tidak ada lampiran");
      myfrm.MeasureUnitId.focus();
    } else if ($("#MeasureUnitId").val() !== "XxJyPn38Yh.6" && $("#Jumlah").val() == "") {
      alert('Jumlah Lampiran Wajib diisi');
      myfrm.Jumlah.focus();
   } else if ($("#RoleCode").val() == "") {
        alert("Unit Pengolah Wajib Diisi");
        myfrm.RoleCode.focus();
    } else if ($("#ClId").val() == "") {
      alert("Kode Klasifikasi Wajib Diisi");
      myfrm.ClId.focus();
    } else if ($("#UrgensiId").val() == "") {
      alert("Tingkat Urgensi Wajib Diisi");
      myfrm.UrgensiId.focus();
    } else if ($("#SifatId").val() == "") {
      alert("Sifat Naskah Wajib Diisi");
      myfrm.SifatId.focus();
    } else if ($("#Hal").val() == "") {
      alert("Hal Wajib Diisi");
      myfrm.Hal.focus();
    } else if ($("#RoleId_To").val() == "") {
      alert("Tujuan Surat Edaran Wajib Diisi");
      myfrm.RoleId_To.focus();

    } else {
      $("#myfrm").attr('target', '_blank');
      $("#myfrm").attr('action', "<?= BASE_URL('administrator/anri_regis_suratedaran/lihat_naskah_edaran/') ?>");
      $("#myfrm").submit();
    }

  }

  function simpan_naskah() {

    if ($("#ClId").val() == "") {
      alert("Kode Klasifikasi Wajib Diisi");
      myfrm.ClId.focus();
    } else if ($("#MeasureUnitId").val() == "") {
      alert("Lampiran Wajib Dipilih, pilih strip (-) jika tidak ada lampiran");
      myfrm.MeasureUnitId.focus();
    } else if ($("#MeasureUnitId").val() !== "XxJyPn38Yh.6" && $("#Jumlah").val() == "") {
      alert('Jumlah Lampiran Wajib diisi');
      myfrm.Jumlah.focus();
     } else if ($("#RoleCode").val() == "") {
        alert("Unit Pengolah Wajib Diisi");
        myfrm.RoleCode.focus();
        
    } else if ($("#ClId").val() == "") {
      alert("Kode Klasifikasi Wajib Diisi");
      myfrm.ClId.focus();
    } else if ($("#UrgensiId").val() == "") {
      alert("Tingkat Urgensi Wajib Diisi");
      myfrm.UrgensiId.focus();
    } else if ($("#SifatId").val() == "") {
      alert("Sifat Naskah Wajib Diisi");
      myfrm.SifatId.focus();
    } else if ($("#Hal").val() == "") {
      alert("Hal Wajib Diisi");
      myfrm.Hal.focus();
    } else if ($("#RoleId_To").val() == "") {
      alert("Tujuan Surat Edaran Wajib Diisi");
      myfrm.RoleId_To.focus();
    } else {
      $("#myfrm").attr('target', '_self');
      $("#myfrm").attr('action', "<?= base_url('administrator/anri_regis_suratedaran/post_suratedaran/'); ?>");
      $("#myfrm").submit();
    }

  }
</script>