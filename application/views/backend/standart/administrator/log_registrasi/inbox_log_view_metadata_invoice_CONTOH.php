<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <title><?= get_option('site_name'); ?></title>
  <link rel="shortcut icon" href="<?= BASE_ASSET; ?>/favicon.ico">

  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/morris/morris.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/datepicker/datepicker3.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/all.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/sweet-alert/sweetalert.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/toastr/build/toastr.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.css?v=2.1.5" media="screen" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/chosen/chosen.css">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>/css/custom.css?timestamp=201803311526">
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>datetimepicker/jquery.datetimepicker.css"/>
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>js-scroll/style/jquery.jscrollpane.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>flag-icon/css/flag-icon.css" rel="stylesheet" media="all" />
  <link rel="stylesheet" href="<?= BASE_ASSET; ?>admin-lte/plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" media="all" />
  
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/iCheck/icheck.min.js"></script>
  <script src="<?= BASE_ASSET; ?>/sweet-alert/sweetalert-dev.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="<?= BASE_ASSET; ?>/admin-lte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <script src="<?= BASE_ASSET; ?>/toastr/toastr.js"></script>
  <script src="<?= BASE_ASSET; ?>/fancy-box/source/jquery.fancybox.js?v=2.1.5"></script>
  <script src="<?= BASE_ASSET; ?>/datetimepicker/build/jquery.datetimepicker.full.js"></script>
  <script src="<?= BASE_ASSET; ?>/editor/dist/js/medium-editor.js"></script>
  <script src="<?= BASE_ASSET; ?>js/cc-extension.js"></script>
  <script src="<?= BASE_ASSET; ?>/js/cc-page-element.js"></script>
</head>

	<body style="padding: 10px;">
		<h4>&nbsp;&nbsp;&nbsp;&nbsp;BAGIAN RUMAH TANGGA, TU DAN KEPEGAWAIAN</h4>
		<h4>&nbsp;&nbsp;&nbsp;&nbsp;SEKRETARIAT DAERAH PROVINSI JAWA BARAT</h4>
		<h4>&nbsp;&nbsp;&nbsp;&nbsp;JL. DIPONEGORO NO.22 BANDUNG</h4>
  <h3 class="widget-user-username" align="center"><font color="red"><b>Tanda Terima</b></font></h3>
  <hr>
  <div class="table-responsive"> 
    <table id="example1" class="table table-bordered table-striped dataTable">
       <thead class="tbody_inbox">
          <tr class="">
             <th width="30%">Diterima Dari / Asal Surat</th>
             <td width="1%">:</td>
			 <td><?= $inbox->Instansipengirim ?></td>
          </tr>
		   <tr class="">
             <th width="30%">Tgl Registrasi Surat</th>
             <td width="1%">:</td>
			 <td><?= date('d-m-Y',strtotime($inbox->Tgl)) ?></td>
          </tr>
		   <tr class="">
             <th width="30%">Nomor Surat</th>
             <td width="1%">:</td>
			 <td><?= $inbox->Nomor ?></td>
          </tr>
		  <tr class="">
             <th width="30%">Banyaknya Surat</th>
             <td width="1%">:</td>
			 <td></td>
          </tr>
		  <tr class="">
             <th width="30%">Hal</th>
             <td width="1%">:</td>
			 <td><?= $inbox->Hal ?></td>
          </tr>
		  <tr class="">
             <th width="30%">Contact Person</th>
             <td width="1%">:</td>
			 <td></td>
          </tr>
       </thead>
    </table>
	  <br><br>
	  <p ><u>Konfirmasi</u></p>
	  <p>Kategori Surat :</p>
	  <p>Tlp. 022 - 4233347</p>
	  <p><u>Undangan / Acara / Audiensi</u></p>
	  <p>Tlp. 022 - 4219572</p>
	  <p>WA. 08112109850</p>
	  <p><u>Kirim Surat</u></p>
	  <p>Fax. 022 - 4203450</p>
	  <p>Email => kirimsurat@jabarprov.go.id</p>
  </div>

</script>
</body>
</html>