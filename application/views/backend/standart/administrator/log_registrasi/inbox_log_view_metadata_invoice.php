<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<style>

    @media print
    {
    .noprint {display:none;}
    }

  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
    
      <li><a href="<?= site_url('administrator/anri_dashboard'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class=""><a  href="<?= site_url('administrator/anri_log_naskah_masuk'); ?>"><?= $title; ?></a></li>
      <li class="active">Detail</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                     </div>
                     <!-- /.widget-user-image -->
					 <div style="width: 50%; font-family: Arial; font-size: 12px; margin-left: 20px; margin-top:-20px; ">

						<p style="float: left!important; margin-bottom: 0px; text-align: justify; font-family: Arial; font-size: 12px;">KEPALA SUBBAGIAN KEPEGAWAIAN DAN KEARSIPAN TATA USAHA BIRO UMUM SEKRETARIAT DAERAH PROVINSI JAWA BARAT</p>
						<p style="float: left!important; margin-bottom: 0px; text-align: justify; font-family: Arial; font-size: 12px;">
						<?php
						$alamat = $this->db->query("SELECT Alamat FROM v_alamat WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Alamat;
							echo $alamat;
			
					?></p>
						</div>
					 
					 <br>
					  <br><br>
            <br>
					  
         <h4 class="widget-user-username" align="center">Tanda Terima  </h4>
                  </div>
                <br>
				
                  <div class="tab-content container">
                    <div class="tab-pane fade in active">
                      <div class="form-horizontal">
					  <div style=" font-family: Arial; font-size: 12px;">
                           &nbsp;&nbsp; <label >Asal Naskah </label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                          &nbsp;&nbsp;<?= $inbox->Instansipengirim ?>
                        </div>
					   <div  style=" font-family: Arial; font-size: 12px;">
                            &nbsp;&nbsp; <label>Tanggal Naskah</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:
                          &nbsp;&nbsp;<?= date('d-m-Y',strtotime($inbox->Tgl)) ?>
                        </div>
						<div  style=" font-family: Arial; font-size: 12px;">
                          &nbsp;&nbsp; <label>Nomor Naskah</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;<?= $inbox->Nomor ?>							
                        </div>
						<div  style=" font-family: Arial; font-size: 12px;">
                          &nbsp;&nbsp; <label>Banyaknya Surat</label>&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;1					
                        </div>
						<div  style=" font-family: Arial; font-size: 12px;">
                          &nbsp;&nbsp; <label>Perihal Naskah.</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: &nbsp;&nbsp;<?= $inbox->Hal ?>
		                  </div>
						   <div style=" font-family: Arial; font-size: 12px;">
                            &nbsp;&nbsp; <label>Contact Person</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<?= $inbox->cp ?>
                          &nbsp;&nbsp;
                        </div>
<br><br>
<table width="100%" cellpadding="17"cellspacing="1">
    <tbody>
        <tr>
            <td align="normal" valign="top"width="352">
                 <div class="pos" id="_118:320" style="top:320;left:115">
                    <span id="_16.3" style=" font-family:Arial; text-decoration: underline; font-size:12px; color:#000000">
                     Konfirmasi
                    </span>
					
                </div>
				<div style="width:100px;"  class="pos" id="_118:320"  >
                    <span id="_16.3" style="width:60px; font-size:12px; font-family:Arial;  color:#000000">
                      <?php
						$konfirmasi = $this->db->query("SELECT Konfirmasi FROM v_alamat WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Konfirmasi;
							echo $konfirmasi;
			
					?>  
                    </span>
					
                </div>
                  <div class="pos" id="_118:320" style="top:320;left:115">
                    <span id="_16.3" style=" font-family:Arial; text-decoration: underline; font-size:12px; color:#000000">
                     Undangan/Acara/Audiensi
                    </span>
					
                </div>
				<div style="width:150px;"  class="pos" id="_118:320"  >
                    <span id="_16.3" style="width:100px; font-size:12px; font-family:Arial;  color:#000000">
                      <?php
						$undangan = $this->db->query("SELECT Undangan_Acara_Audiensi FROM v_alamat WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Undangan_Acara_Audiensi;
							echo $undangan;
			
					?>  
                    </span>
					
                </div>
                  <div class="pos" id="_118:320" style="top:320;left:115">
                    <span id="_16.3" style=" font-family:Arial; text-decoration: underline; font-size:12px; color:#000000">
                     Kirim Surat
                    </span>
					
                </div>
				<div style="width:130px;"  class="pos" id="_118:320"  >
                    <span id="_16.3" style="width:260px; font-size:12px; font-family:Arial;  color:#000000">
                      <?php
						$kirsur = $this->db->query("SELECT Kirim_Surat FROM v_alamat WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Kirim_Surat;
							echo $kirsur;
			
					?>  
                    </span>
					
                </div>
           </td>
            
            <td align="center"valign="top" width="500px;font-family:Arial; font-size:12px;"><p style="float: center!important; margin-bottom: 0px; text-align: center; font-family: Arial; font-size: 12px;margin-left20px">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php
							$lok = $this->db->query("SELECT Lokasi FROM v_kopnaskah WHERE RoleId = '".$this->session->userdata('roleid')."'")->row()->Lokasi;
							echo $lok;?>,&nbsp;<?= date('d-m-Y',strtotime($inbox->NTglReg)); ?></p>
                <div class="pos" id="_118:320" style="top:320;left:200;font-family:Arial; font-size:12px;">
                </div>
				<br><br><br><br><br>
                <div class="pos" id="_118:320" style="top:320;margin-left:30;font-family:Arial;font-size:12px; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(........................................................)
                </div>
                <div class="pos" id="_118:320" style="top:320;margin-left:35; margin-right:15px; font-family:Arial;font-size:12px;">
                </div>
                </td>
        </tr>
    </tbody>
</table>
			
                      

						
</div>
 		
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div class="noprint">
 <a href="">
    <button onClick="window.print();">Print</button>
    </a>
	</div>
   </div>
   
</section>
