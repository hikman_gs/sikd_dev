        <?php
        $this->db->select('*');
        $this->db->group_by('GIR_Id');
        $this->db->where('NId', $NId);
        $this->db->order_by('ReceiveDate', 'DESC');
        $inboxs = $this->db->get('inbox_receiver')->result();
        $master_jnaskah = $this->db->get('master_jnaskah')->result();
        $classification = $this->db->get('classification')->result();
        foreach ($inboxs as $k => $inbox); ?>

        <script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
        <link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
        <!-- Fine Uploader jQuery JS file
    ====================================================================== -->
        <script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
        <?php $this->load->view('core_template/fine_upload'); ?>
        <script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
        <style>
            @media print {
                .noprint {
                    display: none;
                }
            }

            .menu-baru {
                padding: 0px;
                list-style: none;
                padding-bottom: 10px;
                padding-left: 5px;
            }

            .menu-baru>li {
                display: inline-block;
                margin-right: 20px;
                border-radius: 3px;
                border: 1px solid #ddd;
                padding: 5px;
                transition: ease-in-out .1s;
            }

            .menu-baru>li a {
                color: #666;
                transition: ease-in-out .1s;
            }

            .menu-baru>li:hover {
                border: 1px solid #3c8dbc;
                background: #3c8dbc;
                color: #fff;
            }

            .menu-baru>li:hover a {
                color: #fff;
            }

            .chosen-container {
                width: 100% !important;
            }
        </style>
        <section class="content-header">
            <h1><small></small></h1>
            <ol class="breadcrumb">
                <li><a href="<?= site_url('administrator/anri_dashboard'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
                <li class=""><a href="<?= site_url('administrator/anri_log_naskah_masuk_setda'); ?>"><?= $title; ?></a></li>
                <li class="active">Detail</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-warning">
                        <div class="box-body ">
                            <div class="box box-widget widget-user-2">
                                <div class="widget-user-header ">
                                    <div class="widget-user-image">
                                    </div>
                                    <!-- /.widget-user-image -->
                                </div>



                                <div class=WordSection1 style='margin-top:-1.2cm'>
                                    <table class=MsoTableGrid border=1 cellspacing=0 cellpadding=0 style='border-collapse:collapse;border:none;mso-border-alt:solid windowtext .5pt;
 mso-padding-alt:0cm 5.4pt 0cm 5.4pt'>
                                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:34.95pt'>
                                            <td width=508 colspan=4 style='width:381.05pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:34.95pt'>
                                                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><b><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>PEMERINTAH
                                                            PROVINSI JAWA BARAT<o:p></o:p></span></p>
                                                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>SEKRETARIAT
                                                        DAERAH<o:p></o:p></span></p>
                                                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:0cm;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>KARTU
                                                        KENDALI SURAT MASUK<o:p></o:p></span></b></p>
                                            </td>
                                        </tr>
                                        <tr style='mso-yfti-irow:1;height:28.3pt'>
                                            <td width=17 style='width:12.5pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-top-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0cm 5.4pt 0cm 5.4pt;mso-rotate:90;height:28.3pt'>
                                                <p class=MsoNormal align=center style='margin-top:0cm;margin-right:5.65pt;
  margin-bottom:0cm;margin-left:5.65pt;margin-bottom:.0001pt;text-align:center;
  line-height:normal'><span style='font-size:8.0pt;font-family:"Arial","sans-serif"'>
                                                        <o:p>&nbsp;</o:p>
                                                    </span></p>
                                            </td>
                                            <td width=189 style='width:5.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:28.3pt'><b>
                                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'>INDEXS :<o:p></o:p></span></p>
                                                </b>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'> <?= $indexs = $this->db->query(" Select c.ClName from classification c INNER JOIN inbox i ON c.ClId=i.ClId where i.NId ='" . $NId . "'")->row()->ClName; ?>


                                            </td>
                                            <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:28.3pt'><b>
                                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'>KODE : <o:p></o:p></span></p>
                                                </b>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'> <?= $indexs = $this->db->query(" Select c.ClCode from classification c INNER JOIN inbox i ON c.ClId=i.ClId where i.NId ='" . $NId . "'")->row()->ClCode; ?>



                                                        <o:p></o:p>
                                                    </span></p>
                                            </td>
                                            <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:28.3pt'><b>
                                                    <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'>No. Agenda :<o:p></o:p></span></p>
                                                </b>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'><?= $no_urut = $this->db->query("Select no_urut from inbox where NId='" . $NId . "'")->row()->no_urut; ?><o:p></o:p></span></p>
                                            </td>
                                        </tr>
                                        <tr style='mso-yfti-irow:2;height:18.9pt'>
                                            <td width=17 valign=top style='width:12.5pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-top-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:18.9pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'>
                                                        <o:p>&nbsp;</o:p>
                                                    </span></p>
                                            </td>
                                            <td width=491 colspan=3 style='width:13.0cm;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:18.9pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'><b>HAL:</b>
                                                        <?= $hal = $this->db->query("Select hal from inbox where NId='" . $NId . "'")->row()->hal; ?><o:p></o:p></span></p>
                                            </td>
                                        </tr>

                                        <tr style='mso-yfti-irow:3;height:52.1pt'>
                                            <td width=17 valign=top style='width:12.5pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-top-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:52.1pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'>
                                                        <o:p>&nbsp;</o:p>
                                                    </span></p>
                                            </td>

                                            <td width=491 colspan=3 style='width:13.0cm;border-top:none;border-left:none;
  border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
  mso-border-top-alt:solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;
  mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
  padding:0cm 5.4pt 0cm 5.4pt;height:52.1pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'><b>ISI
                                                            RINGKASAN :</b><span style='mso-spacerun:yes'>  </span><?= $hal = $this->db->query("Select hal from inbox where NId='" . $NId . "'")->row()->hal; ?><o:p></o:p></span></p>
                                            </td>
                                        </tr>

                                        <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;height:34.95pt'>
                                            <td width=508 colspan=4 style='width:381.05pt;border:solid windowtext 1.0pt;
  mso-border-alt:solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:34.95pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>DARI :</b><span style='mso-spacerun:yes'>  </span><?= $Instansipengirim = $this->db->query("Select Instansipengirim from inbox where NId='" . $NId . "'")->row()->Instansipengirim; ?><?= $Namapengirim = $this->db->query("Select Namapengirim from inbox where NId='" . $NId . "'")->row()->Namapengirim; ?><o:p></o:p></span></p>
                                            </td>
                                        </tr>

                                        <tr style='mso-yfti-irow:4;height:26.25pt'>
                                            <td width=17 valign=top style='width:12.5pt;border-top:none;border-left:solid windowtext 1.0pt;
  border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-top-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
  solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:26.25pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
  normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'>
                                                        <o:p>&nbsp;</o:p>
                                                    </span></p>
                                            </td>

                                            <td width=189 style='width:5.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:
  solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
  0cm 5.4pt 0cm 5.4pt;height:26.25pt'>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'><B>TANGGAL NASKAH :</B>
                                                        <o:p></o:p>
                                                    </span></p>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'><?= $tgl = $this->db->query("Select tgl from inbox where NId='" . $NId . "'")->row()->tgl; ?><o:p></o:p></span></p>
                                            </td>

                                            <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:26.25pt'>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'><B>NOMOR NASKAH :</B>
                                                        <o:p></o:p>
                                                    </span></p>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'><?= $nomor = $this->db->query("Select nomor from inbox where NId='" . $NId . "'")->row()->nomor; ?><o:p></o:p></span></p>
                                            </td>

                                            <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
  solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
  solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
  solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:26.25pt'>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'><B>LAMPIRAN :</B>
                                                        <o:p></o:p>
                                                    </span></p>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
  text-align:center;line-height:normal'><span style='font-size:7.0pt;
  font-family:"Arial","sans-serif"'><?= $lampiran = $this->db->query("Select nomor from inbox where NId='" . $NId . "'")->row()->lampiran; ?><o:p></o:p></span></p>
                                            </td>
                                        </tr>

                                        <tr style='mso-yfti-irow:5;height:33.1pt'>
                                            <td width=17 valign=top style='width:12.5pt;border-top:none;border-left:solid windowtext 1.0pt;
            border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-top-alt:
            solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
            solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
            0cm 5.4pt 0cm 5.4pt;height:33.1pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
            normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'>
                                                        <o:p>&nbsp;</o:p>
                                                    </span></p>
                                            </td>

                                            <td width=189 style='width:5.0cm;border-top:none;border-left:none;border-bottom:
            solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
            solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-bottom-alt:
            solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;padding:
            0cm 5.4pt 0cm 5.4pt;height:33.1pt'>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
            text-align:center;line-height:normal'><span style='font-size:7.0pt;
            font-family:"Arial","sans-serif"'><B>PENGOLAH :</B>
                                                        <o:p></o:p>
                                                    </span></p>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
            text-align:center;line-height:normal'><span style='font-size:7.0pt;
            font-family:"Arial","sans-serif"'>
                                                        <o:p></o:p>
                                                    </span></p>
                                            </td>
                                            <!-- <?= $tujuan = $this->db->query("Select To_Id_Desc from inbox_receiver where NId='" . $NId . "'")->row()->To_Id_Desc;
                                                    ?> -->
                                            <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
            solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
            solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
            solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:33.1pt'>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
            text-align:center;line-height:normal'><span style='font-size:7.0pt;
            font-family:"Arial","sans-serif"'><B>TANGGAL DITERUSKAN :</B>
                                                        <o:p></o:p>
                                                    </span></p>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
            text-align:center;line-height:normal'><span style='font-size:7.0pt;
            font-family:"Arial","sans-serif"'><?= date('d-m-Y H:i:s', strtotime($inbox->ReceiveDate)) ?><o:p></o:p></span></p>
                                            </td>

                                            <td width=151 style='width:4.0cm;border-top:none;border-left:none;border-bottom:
            solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;mso-border-top-alt:
            solid windowtext .5pt;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:
            solid windowtext .5pt;padding:0cm 5.4pt 0cm 5.4pt;height:33.1pt'>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
            text-align:center;line-height:normal'><span style='font-size:7.0pt;
            font-family:"Arial","sans-serif"'><B>TANDA TERIMA :</B>
                                                        <o:p></o:p>
                                                    </span></p>
                                                <p class=MsoNormal align=center style='margin-bottom:0cm;margin-bottom:.0001pt;
            text-align:center;line-height:normal'><span style='font-size:7.0pt;
            font-family:"Arial","sans-serif"'>1<o:p></o:p></span></p>
                                            </td>
                                        </tr>

                                        <tr style='mso-yfti-irow:6;mso-yfti-lastrow:yes;height:63.05pt'>

                                            <td width=17 valign=top style='width:12.5pt;border-top:none;border-left:solid windowtext 1.0pt;
            border-bottom:solid windowtext 1.0pt;border-right:none;mso-border-top-alt:
            solid windowtext .5pt;mso-border-top-alt:solid windowtext .5pt;mso-border-left-alt:
            solid windowtext .5pt;mso-border-bottom-alt:solid windowtext .5pt;padding:
            0cm 5.4pt 0cm 5.4pt;height:63.05pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
            normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'>
                                                        <o:p>&nbsp;</o:p>
                                                    </span></p>
                                            </td>

                                            <td width=491 colspan=3 valign=top style='width:13.0cm;border-top:none;
            border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;
            mso-border-bottom-alt:solid windowtext .5pt;mso-border-right-alt:solid windowtext .5pt;
            padding:0cm 5.4pt 0cm 5.4pt;height:30.05pt'>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
            normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'>
                                                        <o:p>&nbsp;</o:p>
                                                    </span></p>
                                                <p class=MsoNormal style='margin-bottom:0cm;margin-bottom:.0001pt;line-height:
            normal'><span style='font-size:7.0pt;font-family:"Arial","sans-serif"'><B>CATATAN
                                                            :</B>
                                                        <o:p></o:p>
                                                    </span></p>
                                            </td>
                                        </tr>
                                    </table>

                                    <p class=MsoNormal><span style='font-size:7.0pt;line-height:115%;font-family:
"Arial","sans-serif"'>
                                            <o:p>&nbsp;</o:p>
                                        </span></p>

                                </div>

                                <p class=MsoNormal><span style='font-size:8.0pt;line-height:115%;font-family:
"Arial","sans-serif"'>
                                        <o:p>&nbsp;</o:p>
                                    </span></p>
                                <div class="noprint">
                                    <a href="">
                                        <button onClick="window.print();">Print</button>
                                    </a>
                                </div>


        </section>

        <script type="text/javascript">
            $(document).ready(function() {

                var dataTable = $('#myTable').DataTable({

                    "processing": true,
                    "serverSide": true,
                    "order": [
                        [5, "asc"]
                    ],

                    "ajax": {
                        "url": "<?php echo site_url('administrator/anri_log_naskah_masuk_pengendali/get_data_log_surat_all') ?>",
                        "type": "POST",
                        //deferloading untuk menampung 10 data pertama dulu
                        "deferLoading": 10,
                    },

                    "columnDefs": [{
                        "targets": [0, 7],
                        "orderable": false,
                    }, ],

                });

            });
        </script>