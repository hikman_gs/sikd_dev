<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<section class="content-header">
   <h1>Detail Log Registrasi Naskah</h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class=""><a  href="<?= site_url('anri_dashboard/log_naskah_masuk'); ?>">Log Registrasi Naskah Masuk</a></li>
      <li class="active"><?= cclang('detail'); ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Detail Log Registrasi Naskah</h3>
                     <h5 class="widget-user-desc">Data</h5>
                     <hr>
                  </div>
                  <ul class="nav nav-tabs">
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_log_naskah_masuk/log/' . $NId); ?>">Lihat Naskah</a></li>
                    <!-- <li><a href="<?= site_url('administrator/anri_dashboard/view_log_naskah_tindaklanjut/' . $NId); ?>">Tindak Lanjut Masuk</a></li> -->
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_log_naskah_histori/' . $NId); ?>">Histori Naskah</a></li>
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_log_naskah_metadata/' . $NId); ?>">Metadata</a></li>
                    <!-- <li class="active"><a href="<?= site_url('administrator/anri_dashboard/view_log_naskah_berkas/' . $NId); ?>">Status Pemberkasan</a></li> -->
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade in active">
                      <br>
                      <div class="table-responsive"> 
                        <table id="example1" class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>No</th>
                                 <th>Nomor Berkas</th>
                                 <th>Nama Berkas</th>
                                 <!-- <th>Retensi Aktif</th> -->
                                 <!-- <th>Retensi InAktif</th> -->
                                 <th>Status</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_inbox">
                           <?php 
                           $files = $this->db->query("SELECT * FROM inbox_files WHERE NId = '".$NId."'")->result();
                           foreach($files as $k => $file): ?>
                              <tr>
                                 <td><?= $k+1; ?></td>
                                 <td><?= $NId ?></td>
                                 <td><?= $file->FileName_fake; ?></td> 
                                 <td><?= $file->FileStatus; ?></td> 
                              </tr>
                            <?php endforeach; ?>
                           </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->
