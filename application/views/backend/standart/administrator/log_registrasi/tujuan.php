<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class=""><a  href="#">Tujuan Naskah</a></li>
      <li class="active">Ubah Data</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Ubah</h3>
                     <h5 class="widget-user-desc">Tujuan Naskah</h5>
                     <hr>
                  </div>
                  <a title="Kembali" href="<?= base_url('administrator/anri_log_naskah_masuk/view_log_naskah_histori/'.$tipe.'/'.$NId.'?dt=9'); ?>" class="btn btn-danger btn-sm"><i class="fa fa-chevron-left"></i> Kembali</a>
                  <a title="Tambah" data-toggle="modal" data-target="#tambah" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Tambah Tujuan</a>
                  <br><br>
                  <div class="table-responsive"> 
                    <table id="example1" class="table table-bordered table-striped dataTable">
                       <thead>
                          <tr class="">
                             <th width="5%">No</th>
                             <th>Tujuan Naskah</th>
                             <th width="5%"></th>
                          </tr>
                       </thead>
                       <tbody id="tbody_inbox">
                       <?php 
                        $this->db->select('*');
                        $this->db->where('NId', $NId); 
                        $this->db->order_by('ReceiveDate','DESC');
                        $inboxs = $this->db->get('inbox_receiver')->result();
                       foreach($inboxs as $k => $inbox): ?>
                          <tr>
                              <td><?= $k+1; ?></td>
                              <td>
                                <?php
                                  if($inbox->ReceiverAs == 'bcc') {
                                    echo get_data_people('PeopleName',$inbox->To_Id).'<font color="red"><b> (Tembusan)</b></font>';
                                  } else {
                                    echo get_data_people('PeopleName',$inbox->To_Id);
                                  }
                                ?>  
                              </td>
                              <td>
                                  <a title="Hapus" href="javascript:void(0);" data-href="<?= site_url('administrator/anri_reg_naskah_masuk/hapus_tujuan/log/'.$NId.'/'.$inbox->RoleId_To.'/'.$inbox->GIR_Id); ?>" class="btn btn-danger btn-sm remove-data"><i class="fa fa-trash"></i></a>
                              </td>
                          </tr>
                        <?php endforeach; ?>
                       </tbody>
                    </table>
                  </div> 
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="tambah" class="modal fade" role="dialog">
    <div class="modal-lg modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Tambah Tujuan Naskah</h4>
        </div>
        <div class="modal-body">
          <form action="<?= base_url('administrator/anri_reg_naskah_masuk/post_tambah_tujuan/'.$tipe.'/'.$NId); ?>" class="form-horizontal" method="POST">
            <div class="form-group">
                <div class="col-md-3">Tujuan Surat</div>
                <div class="col-md-9">
                    <select name="Kepada_Yth[]" class="form-control chosen chosen-select" multiple tabi-ndex="5" id="Kepada_Yth">
                        <option value="">-- Pilih User --</option>
                          <!-- Perbaikan Eko 09-11-2019 -->
                          <?php
                              $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                              foreach ($query as $dat_people) {
                          ?>
                              <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeoplePosition; ?></option>
                          <?php
                              }
                          ?>
                          <!-- batas akhir perbaikan -->                            
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-3">Tembusan</div>
                <div class="col-md-9">
                    <select name="Tembusan[]" class="form-control chosen chosen-select" multiple tabi-ndex="5" id="Tembusan">
                        <option value="">-- Pilih User --</option>
                          <!-- Perbaikan Eko 09-11-2019 -->
                          <?php
                              $query = $this->db->query("SELECT * FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' ORDER BY p.PrimaryRoleId ASC")->result();
                              foreach ($query as $dat_people) {
                          ?>
                              <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeoplePosition; ?></option>
                          <?php
                              }
                          ?>
                          <!-- batas akhir perbaikan -->                            
                    </select>
                </div>
            </div>
            <div class="form-group">
              <div class="col-md-3"></div>
              <div class="col-md-9">
                <button class="btn btn-danger">Simpan</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.content -->
<script>

  $('.remove-data').click(function(){

      var url = $(this).attr('data-href');

      swal({
          title: "Apakah Data Akan Dihapus ??",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
            document.location.href = url;            
          }
        });

      return false;
    });
  
</script>