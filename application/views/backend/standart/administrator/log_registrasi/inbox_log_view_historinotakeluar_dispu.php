<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li>
        <a href="<?= site_url('administrator/anri_dashboard'); ?>"><i class="fa fa-dashboard"></i> Beranda</a>
      </li>
      <li class="">
        <a  href="<?= site_url('administrator/anri_log_nota_keluar'); ?>"><?= $title; ?></a>
      </li>
      <li class="active">Detail</li>
   </ol>   
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Detail Data Pencatatan Nota Dinas Keluar</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
                  <ul class="menu-baru">
                    <li><i class="fa fa-edit"></i> <a href="<?= site_url('administrator/anri_reg_nota_keluar/edit_naskah_masuk/log/' . $NId .'?dt=9'); ?>"> Ubah Metadata</a></li>
                    <?php if($this->db->query("SELECT Status FROM inbox_receiver WHERE NId='".$NId."' AND ReceiverAs = 'to' AND Status = '1'")->num_rows() == 0){ ?>
                        <li><i class="fa fa-edit"></i> <a href="<?= site_url('administrator/anri_reg_nota_keluar/edit_tujuan/log/' . $NId .'?dt=9'); ?>">Ubah Tujuan Naskah</a></li>
                      <?php } ?>
                  </ul>
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="<?= site_url('administrator/anri_log_nota_keluar/view_log_naskah_histori/log/' . $NId .'?dt=9'); ?>">Histori Naskah</a></li>
                    <li><a href="<?= site_url('administrator/anri_log_nota_keluar/view_log_naskah_metadata/log/' . $NId .'?dt=9'); ?>">Metadata</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade in active">
                      <br>
                      <div class="table-responsive"> 
                        <table id="example1" class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>No</th>
                                 <th>Tanggal</th>
                                 <th>Pembuat Nota</th>
                                 <th>Tujuan Naskah</th>
                                 <th>Keterangan</th>
                                 <th>Data Digital</th>
                                 <th>Pesan</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_inbox">
                           <?php 
                            $this->db->select('*');
                            $this->db->group_by('GIR_Id'); 
                            $this->db->where('NId', $NId); 
                            $this->db->order_by('ReceiveDate','DESC');
                            $inboxs = $this->db->get('inbox_receiver')->result();
                           foreach($inboxs as $k => $inbox): ?>
                              <tr>
                                  <td><?= $k+1; ?></td>
                                  <td><?= date('d-m-Y H:i:s',strtotime($inbox->ReceiveDate)) ?></td> 
                                  <td>
                                    <?php 
                                    if ($inbox->ReceiverAs == 'to') {
                                      echo $this->db->query("SELECT Namapengirim FROM inbox WHERE NId='".$NId."'")->row()->Namapengirim;
                                    }else{
                                      echo $this->db->query("SELECT RoleName FROM role WHERE RoleId='".$inbox->RoleId_From."'")->row()->RoleName;

                                    }
                                    ?>
                                  </td>
                                  <td>
                                    <?php
                                      $tujuan = $this->db->query("SELECT * FROM inbox_receiver WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs !='bcc'")->result();
                                        echo '<ul>';
                                        foreach($tujuan as $key => $dat_tujuan) {
                                          if($dat_tujuan->StatusReceive=='unread')
                                            { 
                                              echo '<li><font color=red>'.get_data_people('PeopleName',$dat_tujuan->To_Id).' (Belum Dibaca)</font>';
                                            }
                                          else
                                            {
                                              echo '<li>'.get_data_people('PeopleName',$dat_tujuan->To_Id).' (Dibaca)';
                                            }  
                                        }
                                        echo '</ul>';

                                      $tembusan = $this->db->query("SELECT * FROM inbox_receiver WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs ='bcc'")->result();
                                      if(count($tembusan)){
                                        echo 'Tembusan : <br><ul>';
                                          foreach($tembusan as $key => $dat_tembusan) {
                                            if($dat_tembusan->StatusReceive=='unread')
                                              { 
                                                echo '<li><font color=red>'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Belum Dibaca)</font>';
                                              }
                                            else
                                              {
                                                echo '<li>'.get_data_people('RoleName',$dat_tembusan->To_Id).' (Dibaca)';
                                              }  
                                          }
                                        echo '</ul>';
                                      }
                                    ?>
                                  </td> 
                                  <td>
                                    <?php 
                                      $t1 = $this->db->query("SELECT ReceiverAs FROM inbox_receiver WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."' AND ReceiverAs != 'bcc'")->row()->ReceiverAs;

                                      if ($t1 == 'to') {
                                        echo "Nota Keluar";
                                      }elseif($t1 == 'to_undangan'){
                                        echo "Undangan";
                                      }elseif($t1 == 'to_sprint'){
                                        echo "Surat Tugas";
                                      }elseif($t1 == 'to_notadinas'){
                                        echo "Nota Dinas";
                                      }elseif($t1 == 'to_reply'){
                                        echo "Nota Dinas";
                                      }elseif($t1 == 'to_usul'){
                                        echo "Jawaban Nota Dinas";
                                      }elseif($t1 == 'to_forward'){
                                        echo "Teruskan";
                                      }elseif($t1 == 'cc1'){
                                        echo "Disposisi";
                                      }elseif($t1 == 'to_keluar'){
                                        echo "Surat Dinas Keluar";
                                      }elseif($t1 == 'to_nadin'){
                                        echo "Naskah Dinas Lainnya";
                                      }elseif($t1 == 'to_konsep'){
                                        echo "Konsep Naskah";
                                      }elseif($t1 == 'to_memo'){
                                        echo "Memo";
                                      }
                                    ?>
                                  </td>
                                  <td>
                                    <?php
                                        $query  = $this->db->query("SELECT FileName_fake FROM inbox_files WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."'");

                                        $files = BASE_URL.'/FilesUploaded/'.get_data_inbox('NFileDir',$inbox->NId).'/';

                                        foreach($query->result_array() as $row):

                                          echo '<center><a href="'.$files.$row['FileName_fake'].'" target="_new" title="'.$row['FileName_fake'].'" class="btn btn-danger btn-sm"><i class="fa fa-download"></i></a><br><br></center>';                                     
                                        endforeach;   
                                    ?>                                  
                                  </td>
                                  <td>
                                    <?php
                                    if($inbox->ReceiverAs == 'cc1'){
                                      $count_disposisi  = $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."'")->num_rows();
                                      if($count_disposisi > 0){
                                        $disposisi  = $this->db->query("SELECT Disposisi FROM inbox_disposisi WHERE NId = '".$inbox->NId."' AND GIR_Id = '".$inbox->GIR_Id."'")->result();
                                        
                                        foreach ($disposisi as $key => $value) {
                                          $exp = explode('|', $value->Disposisi);
                                          if($value->Disposisi == '-'){
                                            echo '';
                                          } else {
                                            echo "<ul>";
                                            foreach ($exp as $k => $v) {
                                              echo '<li>'.$this->db->query("SELECT DisposisiName FROM master_disposisi WHERE DisposisiId = '".$v."'")->row()->DisposisiName.'</li>';
                                            }  
                                            echo "</ul>";
                                          }       
                                        }                                       
                                      }
                                    }
                                    ?>
                                  </td>
                              </tr>
                            <?php endforeach; ?>
                           </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- /.content -->