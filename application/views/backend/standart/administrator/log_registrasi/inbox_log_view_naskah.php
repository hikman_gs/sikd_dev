<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script type="text/javascript" src="<?= BASE_ASSET; ?>sweet-alert/sweetalert.min.js"></script>
<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<?php $this->load->view('core_template/fine_upload'); ?>
<style>
  .menu-baru{padding: 0px; list-style: none; padding-bottom: 10px; padding-left: 5px;}
  .menu-baru > li{display: inline-block; margin-right: 20px; border-radius: 3px; border:1px solid #ddd; padding: 5px; transition: ease-in-out .1s;}
  .menu-baru > li a{color: #666; transition: ease-in-out .1s;}
  .menu-baru > li:hover{border:1px solid #3c8dbc; background: #3c8dbc; color: #fff;}
  .menu-baru > li:hover a{color: #fff;}
  .chosen-container{width: 100%!important;}
</style>
<section class="content-header">
   <h1><?= $title; ?></h1>
   <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class=""><a  href="<?= site_url('anri_dashboard/log_naskah_masuk'); ?>">Data Pencatatan Naskah Masuk</a></li>
      <li class="active"><?= cclang('detail'); ?></li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
     
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <div class="box box-widget widget-user-2">
                  <div class="widget-user-header ">
                     <div class="widget-user-image">
                        <img class="img-circle" src="<?= BASE_ASSET; ?>/img/view.png" alt="User Avatar">
                     </div>
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username"><?= $title; ?></h3>
                     <h5 class="widget-user-desc">Data</h5>
                     <hr>
                  </div>
                  <ul class="menu-baru">
                    <li><i class="fa fa-edit"></i> <a href="<?= site_url('administrator/anri_dashboard/edit_naskah_masuk/log/' . $NId); ?>"> Ubah Metadata</a></li>
                  </ul>
                  <ul class="nav nav-tabs">
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_log_naskah_masuk/log/' . $NId); ?>">Lihat Naskah</a></li>
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_log_naskah_histori/log/' . $NId); ?>">Histori Naskah</a></li>
                    <li><a href="<?= site_url('administrator/anri_dashboard/view_log_naskah_metadata/log/' . $NId); ?>">Metadata</a></li>
                  </ul>
                  <div class="tab-content">
                    <div class="tab-pane fade in active">
                      <br>
                      <div class="table-responsive"> 
                        <table id="example1" class="table table-bordered table-striped dataTable">
                           <thead>
                              <tr class="">
                                 <th>No</th>
                                 <th>Jenis Naskah</th>
                                 <th>Tanggal Registrasi</th>
                                 <th>Hal</th>
                                 <th>Aksi</th>
                              </tr>
                           </thead>
                           <tbody id="tbody_inbox">
                           <?php 
                           $konsep = $this->db->query("SELECT * FROM konsep_naskah WHERE CreateBy = '".$this->session->userdata('peopleid')."'")->result();
                           foreach($konsep as $k => $data): ?>
                              <tr>
                                 <td><?= $k+1; ?></td>
                                 <td><?= $this->db->query("SELECT JenisName FROM master_jnaskah WHERE JenisId = '".$data->JenisId."'")->row()->JenisName ?></td>
                                 <td><?= date('d-m-Y H:i:s',strtotime($data->TglReg)) ?></td>
                                 <td><?= $data->Hal ?></td>
                                 <td>
                                  <?php if ($data->Konsep == 1) { ?>
                                      <a href="#" data-nid="<?= $data->NId_Temp; ?>" data-qrcode="<?= base_url('FilesUploaded/qrcode/'.$this->db->query("SELECT QRCode FROM ttd WHERE NId='".$data->NId_Temp."'")->row()->QRCode) ?>" title="Menyetujui" class="btn btn-success btn-sm verifikasi"><i class="fa fa-check"></i></a>
                                  <?php  }elseif($data->Konsep == 0) { ?>
                                    <a href="#" data-temp="<?= $data->NId_Temp; ?>" data-nid="<?= $NId; ?>" title="Kirim Naskah" class="btn btn-warning btn-sm btn-kirim"><i class="fa fa-upload"></i></a>
                                  <?php } ?>
                                  <a href="#" title="Lihat Naskah" class="btn btn-primary btn-sm"><i class="fa fa-eye"></i></a>
                                  <a href="#" title="Edit" class="btn btn-info btn-sm"><i class="fa fa-edit"></i></a>
                                  <a href="#" data-temp="<?= $data->NId_Temp; ?>" title="Hapus" class="btn btn-danger btn-sm btn-delete"><i class="fa fa-trash-o"></i></a>
                                </td>
                              </tr>
                            <?php endforeach; ?>
                           </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div id="approve" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title">Verifikasi Dokumen</h4>
          </div>
          <div class="modal-body">
            <div class="body-qrcode"></div>
          </div>
        </div>
      </div>
    </div>
</section>
<!-- /.content -->
<script>
  $('.verifikasi').click(function(){
    $('.hapus-form').remove();
    $('#approve').modal('toggle');
    var img = $(this).data('qrcode');
    var nid = $(this).data('nid');
    var append = '<form action="<?= base_url('administrator/anri_dashboard/verifikasi/'); ?>'+nid+'" class="form-horizontal hapus-form" method="POST">'+
              '<div class="form-group">'+
                '<div class="col-md-12 text-center">'+
                  '<img src="'+img+'" width="200" alt="" class="img-fluid">'+
                '</div>'+
              '</div>'+
              '<div class="form-group">'+
                '<div class="col-md-12">'+
                  '<input type="password" class="form-control" placeholder="Masukan Kode Verifikasi" name="password">'+
                '</div>'+
              '</div>'+
              '<div class="form-group">'+
                '<div class="col-md-12">'+
                  '<button class="btn btn-success">Simpan</button>'+
                '</div>'+
              '</div>'+
            '</form>';
    $('.body-qrcode').append(append);
  });
  $('.btn-kirim').click(function(){
    swal({
          title: "Apa kamu yakin ingin mengirim data ini ?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
    .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "<?=BASE_URL('administrator/anri_dashboard/naskah_dinas_kirim')?>",
                  type: "POST",
                  data:{
                      temp: $(this).data('temp'),
                  },
                  success: function(response){
                    swal({
                      title: "Data Berhasil Di Kirim",
                      icon: "success",
                    }).then((result) => {
                      window.location.href = "<?=BASE_URL('administrator/anri_dashboard/view_log_naskah_dinas/'.$NId)?>";
                    });
                  },
                  error: function(){
                    swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
                  }
            });
          }
        });
    });
  $('.btn-delete').click(function(){
    swal({
          title: "Apa kamu yakin ingin menghapus data ini ?",
          icon: "warning",
          buttons: true,
          dangerMode: true,
        })
    .then((willDelete) => {
          if (willDelete) {
            $.ajax({
              url: "<?=BASE_URL('administrator/anri_dashboard/hapus_naskah_dinas_tindaklanjut/'.$NId) ?>",
                  type: "POST",
                  data:{
                      temp: $(this).data('temp'),
                  },
                  success: function(response){
                    swal({
                      title: "Data Berhasil Di Hapus",
                      icon: "success",
                    }).then((result) => {
                      window.location.href = "<?=BASE_URL('administrator/anri_dashboard/view_log_naskah_dinas/'.$NId)?>";
                    });
                  },
                  error: function(){
                    swal({title: "Maaf Terjadi Kesalahan",icon: "error",});
                  }
            });
          }
        });
    });
</script>