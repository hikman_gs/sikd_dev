
<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<link href="<?=BASE_ASSET;?>/select2/select2.min.css"rel="stylesheet" />
<script src="<?=BASE_ASSET;?>/select2/select2.min.js"></script>
<style>
.select2-selection {
  -webkit-box-shadow: 0;
  box-shadow: 0;
  background-color: #fff;
  border: 0;
  border-radius: 0;
  color: #555555;
  font-size: 14px;
  outline: 0;
  min-height: 35px;
  text-align: left;
}

.select2-selection__rendered {
  margin: 7px;
}

.select2-selection__arrow {
  margin: 4px;
}

.select2-selection--single {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

.select2-selection__rendered{
  /* word-wrap: break-word !important; */
  white-space: normal !important;
  text-overflow: hidden !important;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Update Data Perdinas</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header "> 
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username" style="margin-left:0px">Update Data Perdinas</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
 
                  <div class="box-body">
				  
				        <div class="form-group col-md-12">
                            <label for="GRoleId" class="col-sm-3 control-label">Grup Unit Kerja    
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control"  name="GRoleId" id="GRoleId"> 
                                  <option value="0">Pilih Perangkat Daerah</option>
								  <?php
                                  $satker = $this->db->query("SELECT * FROM `mapping_pd` WHERE `aktif`='1'")->result();
                                  foreach ($satker as $gr):
                                  ?>
                                      <option value="<?= $gr->satuan_kerja_id; ?>" <?php echo set_select('GRoleId', $gr->satuan_kerja_id); ?>><?=$gr->unit_kerja_nama;?></option>
                                  <?php endforeach; ?>
                                </select>
                            </div>
                      </div>
				  <table id="table" class="table table-striped table-bordered nowrap" style="width:100%">
					    <thead>
					        <tr role="row"> 
					            <th style='width:2%' >NO</th> 
					            <th>NIP</th> 
					            <th>NAMA</th> 
					            <th>JABATAN</th> 
					            <th style='width:10%' class="text-center">ACTION</th> 
					    </thead>
					    <tbody></tbody>
					</table>
				  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
  $(document).ready(function(){
   $('#GRoleId').select2();
		var table;
		
		var pil1  = $('select#GRoleId').val();
		if (localStorage.make) { 
			var pil1  = localStorage.make;
			$("select#GRoleId").val(pil1);
		}
		
		//datatables
		table = $('#table').DataTable({ 
			"processing": true,
			"serverSide": true,
			"ordering"  : false,
			"ajax": {
				"url": BASE_URL+"administrator/integrasi_perdinas/get_data/"+pil1,
				"type": "POST"
			},
			"bStateSave": true
		});



		$('#GRoleId').on('change', function() {
			var pil1  = $('select#GRoleId').val();
			localStorage.make = pil1;
			table.ajax.url(BASE_URL+"administrator/integrasi_perdinas/get_data/"+pil1).load();
		});
		
		
  }); /*end doc ready*/     


</script>