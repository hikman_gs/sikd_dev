
<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Bulk Data</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header "> 
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username" style="margin-left:0px">Bulk Data (Tarik Data dari SIAP)</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
 
                  <div class="box-body">
						
						<button id='pegawai' class="btn btn-info" >Import Pegawai</button>
                  <button id='jabatan' class="btn btn-info" >Import Jabatan</button>
                  <!-- <button id='pd' class="btn btn-info" >Import PD</button> -->
                  <button id='unit_kerja' class="btn btn-info" >Import Unit Kerja</button>
						<div id="div1"></div>
						<div class="modal"><!-- Place at bottom of page --></div>
						<br>
						<div id="progressbar" style="border:1px solid #ccc; border-radius: 5px; "></div>
						<br>
						<div id="information" style="text-align:center; font-weight:bold">Import Data Sedang Berlangsung. Proses membutuhkan waktu beberapa saat</div>
						<iframe id="loadarea" style="display:none;"></iframe><br />
				  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
  $(document).ready(function(){

    $('#example').DataTable( {
        "order": [[ 1, "asc" ]]
    } );
      
  }); /*end doc ready*/     
 
$(document).ready(function(){
	

$("#pegawai").click(function(){
	swal({
          title: "Import Data dari SIAP",
          type: "warning",
          showCancelButton: true,
          //confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
             document.getElementById('loadarea').src = BASE_URL+"administrator/integrasi_bulkdata/tarik_pegawai";
             $('#information').show();
             return false;
          }
        });
});

$("#jabatan").click(function(){
	
	swal({
          title: "Import Data dari SIAP",
          type: "warning",
          showCancelButton: true,
          //confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
			  
	document.getElementById('loadarea').src = BASE_URL+"administrator/integrasi_bulkdata/tarik_jabatan";
	$('#information').show();
		return false;
          }
        });
});

// $("#pd").click(function(){
	
// 	swal({
//           title: "Import Data dari SIAP",
//           type: "warning",
//           showCancelButton: true,
//           //confirmButtonColor: "#DD6B55",
//           confirmButtonText: "Ya",
//           cancelButtonText: "Tidak",
//           closeOnConfirm: true,
//           closeOnCancel: true
//         },
//         function(isConfirm){
//           if (isConfirm) {
			  
// 	document.getElementById('loadarea').src = BASE_URL+"administrator/integrasi_bulkdata/tarik_opd";
// 	$('#information').show();
// 		return false;
//           }
//         });
// });

$("#unit_kerja").click(function(){
	
	swal({
          title: "Import Data dari SIAP",
          type: "warning",
          showCancelButton: true,
          //confirmButtonColor: "#DD6B55",
          confirmButtonText: "Ya",
          cancelButtonText: "Tidak",
          closeOnConfirm: true,
          closeOnCancel: true
        },
        function(isConfirm){
          if (isConfirm) {
			  
	document.getElementById('loadarea').src = BASE_URL+"administrator/integrasi_bulkdata/tarik_opd";
	$('#information').show();
		return false;
          }
        });
});

});	
</script>
<style>
#information {
	display:none;
}
</style>
