<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<link href="<?=BASE_ASSET;?>/select2/select2.min.css"rel="stylesheet" />
<script src="<?=BASE_ASSET;?>/select2/select2.min.js"></script>
<style>
.select2-selection {
  -webkit-box-shadow: 0;
  box-shadow: 0;
  background-color: #fff;
  border: 0;
  border-radius: 0;
  color: #555555;
  font-size: 14px;
  outline: 0;
  min-height: 35px;
  text-align: left;
}

.select2-selection__rendered {
  margin: 7px;
}

.select2-selection__arrow {
  margin: 4px;
}

.select2-selection--single {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

.select2-selection__rendered{
  /* word-wrap: break-word !important; */
  white-space: normal !important;
  text-overflow: hidden !important;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Perangkat Daerah</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header "> 
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Master Data</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
 
                  <div class="box-body">
				        <form method="POST" action="" class="form-horizontal">
						<div class="form-group">
                            <label for="mapping_pd_id" class="col-sm-3 control-label">Perangkat Daerah    
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8 justify-content-center">
                                <select class="form-control select2" style="text-align: center;" name="mapping_pd_id" id="mapping_pd_id"> 
                                  <option value="0">Pilih Perangkat Daerah</option>
								  <?php
                                        foreach ($perangkat as $list) {
                                    ?>
                                    <option value="<?=$list->satuan_kerja_id?>"><?=$list->unit_kerja_nama?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
						<div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button type="submit" id="btn-filter" class="btn btn-primary">Filter</button> 
                        </div>
                    </div>
						</form>
				<?php 
					$items_ = $this->session->userdata('items_');
					message_flash($items_, 'success');
				?>
                 <div class="box-body table-responsive">
					<table id="mapping" class="stripe table table-striped table-bordered">
						<thead>
							<tr>
								<th>NO</th>
								<!-- <th>KETERANGAN</th> -->
								<th>SIAP (UNIT KERJA NAMA)</th>
                                <th>UPDATE</th> 
								<th>SIKD (ROLE DESC)</th> 
							</tr>
						</thead>
                        <tbody>
                        </tbody>
						<tfoot>
                            <th>NO</th>
							<!-- <th>KETERANGAN</th> -->
							<th>SIAP (UNIT KERJA, JABATAN)</th>
                            <th>UPDATE</th> 
							<th>SIKD (ROLE)</th> 
                        </tfoot>
						<!-- <tbody></tbody>  -->
					</table>
					</div>

                  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>

<div class="modal fade" id="mapping_form">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Mapping Unit Kerja</h4>
            </div>
            <form id="form" class="form-horizontal">
                <div class="modal-body">    
                    <div class="box-body">
                    <input type="hidden" value="" name="mapping_per_pd_id"/>
                    <!-- <input type="hidden" value="" name="input_pangkat"/> -->
                    <div class="form-group">
                            <label for="input_satuan_kerja" class="col-sm-3 control-label">Satuan Kerja</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_satuan_kerja" class="form-control" id="input_satuan_kerja"
                                    placeholder="satuan kerja id" readonly>
                                    <input type="hidden" value="" name="form_satuan_kerja"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_role_code" class="col-sm-3 control-label">Role code</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_role_code" class="form-control" id="input_role_code"
                                    placeholder="Role Code (Dinas)" readonly>
                                    <input type="hidden" value="" name="form_role_code"/>
                                    <input type="hidden" value="" name="form_grole_id"/>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_unit_kerja" class="col-sm-3 control-label">Unit Kerja</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_unit_kerja" class="form-control" id="input_unit_kerja"
                                    placeholder="unit kerja id" readonly>
                                <input type="hidden" value="" name="form_unit_kerja"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_eselon" class="col-sm-3 control-label">Eselon</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_eselon" class="form-control" id="input_eselon"
                                    placeholder="II.a/III.a/dll" readonly>
                                    <input type="hidden" value="" name="form_eselon"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_role_desc" class="col-sm-3 control-label">Role Desc</label>
                            <div class="col-sm-9">
                                <textarea type="text" name="input_role_desc" class="form-control" style="min-height:80px;" id="input_role_desc"
                                    placeholder="-" readonly></textarea>
                                    <input type="hidden" value="" name="form_role_desc"/>
                                    <input type="hidden" value="" name="form_role_name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_gjabatan" class="col-sm-3 control-label">Group Jabatan</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_gjabatan" class="form-control" id="input_gjabatan"
                                    placeholder="Struktural/Staff" readonly>
                                    <input type="hidden" value="" name="form_gjabatan"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_role_id" class="col-sm-3 control-label">Unit Role</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_role_id" class="form-control" id="input_role_id" placeholder="Role Id (UK)" readonly>
                                <span class="help-block" id="status_uk" style="font-size:12px; color:red;"></span>
                                <input type="hidden" value="" name="form_role_id"/>
                                <input type="hidden" value="" name="form_parentrole_id"/>
                            </div>
                        </div>
                        
                        
                        
                    </div>
                    <!-- /.box-body -->
                    <!-- /.box-footer -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btnSave" type="button" onclick="" class="btn btn-primary">Tambah Mapping</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="tabel_role">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Update Data Role</h4>
            </div>
            <form id="form_role" class="form-horizontal">
                <div class="modal-body">    
                    <div class="box-body">
                    <h5><b>Role Lama</b></h5>
                    <div class="form-group">
                            <label for="role_id_update" class="col-sm-3 control-label">Role Id</label>
                            <div class="col-sm-9">
                                <input type="text" name="role_id_update" class="form-control"
                                    placeholder="role id" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="role_lama" class="col-sm-3 control-label">Role Name</label>
                            <div class="col-sm-9">
                                <textarea name="role_lama" class="form-control"
                                    placeholder="role name lama" readonly></textarea>

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc_lama" class="col-sm-3 control-label">Role Desc</label>
                            <div class="col-sm-9">
                                <textarea  name="desc_lama" class="form-control"
                                    placeholder="role description lama" readonly></textarea>
                                <!-- <input type="hidden" value="" name="form_unit_kerja"/> -->
                            </div>
                        </div>
                        <hr style="border-top: 1px solid #8c8b8b;"/>
                        <h5><b>Role Baru</b></h5>
                        <div class="form-group">
                            <label for="role_baru" class="col-sm-3 control-label">Role Name</label>
                            <div class="col-sm-9">
                                <textarea name="role_baru" class="form-control"
                                    placeholder="role name baru" readonly></textarea>
                                    <!-- <input type="hidden" value="" name="form_role_code"/> -->
                                    <!-- <input type="hidden" value="" name="form_grole_id"/> -->

                            </div>
                        </div>
                        <div class="form-group">
                            <label for="desc_baru" class="col-sm-3 control-label">Role Desc</label>
                            <div class="col-sm-9">
                                <textarea name="desc_baru" class="form-control"
                                    placeholder="role description baru" readonly></textarea>
                                <!-- <input type="hidden" value="" name="form_unit_kerja"/> -->
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <!-- /.box-footer -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btn_save_role" type="button" onclick="" class="btn btn-primary">Update Role</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.content -->
<!-- Page script -->
<script>
var tabel;
$(document).ready(function(){
    $('textarea').css({'min-height': '70px'});
    $('#mapping_pd_id').select2();
    $('#btn-filter').on('click', function(e){
        e.preventDefault();
        var map = $('#mapping_pd_id').val()
        tabel = $('#mapping').DataTable({
        "processing": true,
        "autoWidth": true,
        "serverSide": true,
        'destroy': true,
        "bStateSave": true,
        "order": [],
        // "initComplete": function() {
        //     $('.select_grole').select2();
        // },
        "drawCallback": function(){
            $('.select_grole').select2({ width: '90%' });
        },
        "ajax": {
            "data": {mapping_pd : map,},
            "url": "<?php echo site_url("administrator/integrasi_master/get_datatable_pd") ?>",
            "type": "POST",
        },

        "columnDefs": [
            { "targets": 0, "width": "3%", "orderable": false},
            { "targets": 1, "width": "40%"},
            { "targets": 2, "width": "17%","orderable": false},
            { "targets": 3, "width": "40%","orderable": false},
            ],

    });
    });
});

function mapping(id, mark){
        
        var role = $('[name="select_'+id+'"]').val();
        if(role==0){
            swal("Informasi", "Pilih Mapping terlebih dahulu", "error");
            return false;
        }
       
        var roledesc;
     
        // console.log(role);
        $.ajax({
            url: "<?php echo site_url('administrator/integrasi_master/get_detail_mapping/') ?>"+id+'/'+role,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                console.log(data);
                if(mark==1){
                    $('#mapping_form').modal();
                    $('#form')[0].reset();
                    $('#btnSave').text('Update Mapping');
                    $('#btnSave').attr('class', 'btn btn-primary');
                    $('#btnSave').attr('onclick','save(1)');

                    $('[name="mapping_per_pd_id"]').val(data.mapping.mapping_id);
                        $('[name="input_satuan_kerja"]').val(data.mapping.satker+' ('+data.mapping.dinas+')');
                        $('[name="form_satuan_kerja"]').val(data.mapping.satker);
                        $('[name="input_unit_kerja"]').val(data.mapping.uker+' ('+data.mapping.nama+')');
                        $('[name="form_unit_kerja"]').val(data.mapping.uker);
                        $('[name="input_eselon"]').val(data.mapping.eselon);
                        $('[name="form_eselon"]').val(data.mapping.eselon);
                        $('[name="input_role_desc"]').text(data.role.role_desc);
                        $('[name="form_role_desc"]').val(data.role.role_desc);
                        $('[name="form_role_name"]').val(data.nama_role);
                        $('[name="input_gjabatan"]').val(data.role.g_jabatan);
                        $('[name="form_gjabatan"]').val(data.role.g_jabatan);
                        $('[name="input_role_id"]').val(data.role.role_id);
                        $('[name="form_role_id"]').val(data.role.role_id);
                        $('[name="form_parentrole_id"]').val(data.role.role_parent);
                        $('[name="input_role_code"]').val(data.role.rolecode+' ('+data.role.rolecode_name+')');
                        $('[name="form_role_code"]').val(data.role.rolecode);
                        $('[name="form_grole_id"]').val(data.role.grole_id);
                        $('#status_uk').text(data.status_uk);
                        if(data.color_uk==1){
                            $('#status_uk').css('color', 'red');
                        }else{
                            $('#status_uk').css('color', 'green');
                        }

                }else if(mark==2){
                    $('#tabel_role').modal();
                    $('#form_role')[0].reset();
                    $('#btn_save_role').text('Update Role');
                    $('#btn_save_role').attr('class', 'btn btn-danger');
                    $('#btn_save_role').attr('onclick','save(2)');
                    $('[name="role_id_update"]').val(data.role.role_id);
                    $('[name="role_lama"]').text(data.role.rolename);
                    $('[name="desc_lama"]').text(data.role.role_desc);
                    $('[name="role_baru"]').text(data.role_baru.name);
                    $('[name="desc_baru"]').text(data.role_baru.desc);


                }
                       
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error . . .');
                $('#btnSave').text('Tambah Mapping');
            }
        });
    }

    function save(mark){
        var url;
        var dataform;
        if(mark==1){
            url = "<?php echo site_url('administrator/integrasi_master/update_mapping') ?>/"+1;
            dataform = $('#form').serialize();
        }else if(mark==2){
            url = "<?php echo site_url('administrator/integrasi_master/update_mapping') ?>/"+2;
            dataform = $('#form_role').serialize();
            // swal({
            //         title: "Apakah Anda Yakin?",
            //         text: "Anda akan mengubah nama role_desc dan mapping untuk role_id : !",
            //         icon: "warning",
            //         buttons: true,
            //         dangerMode: true,
            //         }).then((willDelete) => {
            //         if (willDelete) {
            //             return true;     
            //         } else {
            //             $('#mapping_form').modal('hide');
            //         }
            //     });
        }
        // console.log("lanjut");
        $.ajax({
            url: url,
            type: "POST",
            data: dataform,
            dataType: "JSON",
            success: function(data) {
                if (data.status) {
                    // $('#btnSave').text('OK');
                    // $('#btnSave').attr('disabled', false);
                    swal("Update Berhasil", "Update data berhasil", "success");
                    $('#mapping_form').modal('hide');
                    $('#tabel_role').modal('hide');
                    tabel.ajax.reload( null, false);
                } else {
                    // for (var i = 0; i < data.inputerror.length; i++) {
                    //     $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                    //     $('[name="' + data.inputerror[i] + '"]').attr('placeholder', data.pesanerror[i]);
                    // }
                }
                // $('#btnSave').text('OK');
                $('#btnSave').attr('disabled', false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error . . .');
                $('#btnSave').text('OK');
                $('#btnSave').attr('disabled', false);
            }
        });
    }
</script>
