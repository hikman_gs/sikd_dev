<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Data Webhook Pegawai</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header "> 
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Data Webhook Pegawai </h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
 
                  <div class="box-body">
                   	<table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
					    <thead>
					        <tr role="row"> 
					            <th style='width:3%' >NO</th> 
					            <th>NIP</th> 
					            <th>NAMA</th> 
					            <th>STATUS</th> 
					            <th>TANGGAL</th> 
					            <th>ADA DI SIKD ?</th> 
					            <th>PD</th> 
					            <th style='width:15%' class="text-center">ACTION</th> 
					    </thead>
					    <tbody> 
						<?php 
							
							//$this->db->distinct('peg_nip');
							$this->db->where("status",0);
							$this->db->group_by("peg_nip");
							$this->db->order_by('tanggal', 'DESC');
							$list_anggota = $this->db->get('siap_notif');
							$lists = $list_anggota->result();
							$no=1;
							foreach ($lists as $r):
							$sql1 = $this->db->query("SELECT PeopleUsername FROM people WHERE PeopleUsername='".$r->peg_nip."'");
							$result = $sql1->num_rows();
							
							$sql123s = $this->db->query("SELECT satuan_kerja_nama FROM siap_pegawai WHERE peg_nip='".$r->peg_nip."'");
							$sql123 = $sql123s->row();
                      ?>
                      <tr>  
                        <td  class="text-right"><?=$no;?>.</td>
                        <td><?=$r->peg_nip;?></td>
                        <td><?=$r->peg_nama;?></td>
                        <td><?=($r->peg_status)?'<span class="label label-primary">Aktif</span>
':'<span class="label label-default">Non Aktif</span>
';?></td>
						<td><?=$r->tanggal;?></td>
						<td><?=($result == 1)?'<span class="label label-success">Ada</span>
':'<span class="label label-danger">Tidak Ada</span>
';?></td>
                        
                        <td><?=isset($sql123->satuan_kerja_nama)?$sql123->satuan_kerja_nama:'-';?></td>
						<td>
							<div class="text-center">
								<?= '<a title="Detail" href="'.site_url("administrator/integrasi_notifikasi/detail/".$r->peg_nip).'" class="btn btn-success  btn-sm" role="button"><i class="fa fa-info" aria-hidden="true"></i> Detail</a>';?>

								<a title="Done" href="<?= site_url('administrator/integrasi_notifikasi/updatestatus/'.$r->peg_nip); ?>" class="btn btn-info  btn-sm" role="button"> <i class="fa fa-check" aria-hidden="true"></i> Done</a>
							</div>
						</td>
                         
                      </tr>
                      <?php $no++; ?>
                      <?php endforeach; ?>
						</tbody>
					</table>
                  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
  $(document).ready(function(){

    $('#example').DataTable( {
  "ordering": false,
     "bStateSave": true
});
      
  }); /*end doc ready*/     


</script>