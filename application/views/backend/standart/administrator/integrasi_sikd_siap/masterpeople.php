<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<link href="<?=BASE_ASSET;?>/select2/select2.min.css"rel="stylesheet" />
<script src="<?=BASE_ASSET;?>/select2/select2.min.js"></script>
<style>
.select2-selection {
  -webkit-box-shadow: 0;
  box-shadow: 0;
  background-color: #fff;
  border: 0;
  border-radius: 0;
  color: #555555;
  font-size: 14px;
  outline: 0;
  min-height: 35px;
  text-align: left;
}

.select2-selection__rendered {
  margin: 7px;
}

.select2-selection__arrow {
  margin: 4px;
}

.select2-selection--single {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

.select2-selection__rendered{
  /* word-wrap: break-word !important; */
  white-space: normal !important;
  text-overflow: hidden !important;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Tambah Pegawai PD</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header "> 
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Master Data Pegawai</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
                  <div class="box-body">
				        <form method="POST" action="" class="form-horizontal">
						<div class="form-group">
                            <label for="mapping_pd_id" class="col-sm-3 control-label">Perangkat Daerah    
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control"  name="satuan_kerja_id" id="satuan_kerja_id"> 
                                  <option value="0">Pilih Perangkat Daerah</option>
                                    <?php
                                        foreach ($perangkat as $list) {
                                    ?>
                                    <option value="<?=$list->satuan_kerja_id?>"><?=$list->unit_kerja_nama?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
						<div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button type="submit" id="btn-filter" class="btn btn-primary">Filter</button> 
                        </div>
                    </div>
						</form>
                    <div class="box-body table-responsive">
					<table id="tabel_people" class="stripe table table-striped table-bordered">
						<thead>
							<tr>
								<th class="text-center">NO</th>
								<th class="text-center">Nama Lengkap</th>
								<th class="text-center">Unit Kerja</th>
								<th class="text-center">Jabatan</th> 
                                <th class="text-center">Status di SIKD</th> 
                                <th class="text-center">Aksi</th>
							</tr>
						</thead>
						<tbody></tbody> 
                        <tfoot>
                        <tr>
								<th class="text-center">NO</th>
								<th class="text-center">Nama Lengkap</th>
								<th class="text-center">Unit Kerja</th>
								<th class="text-center">Jabatan</th> 
                                <th class="text-center">Status di SIKD</th> 
                                <th class="text-center">Aksi</th>
							</tr>
                        </tfoot>
					</table>
                    </div>
					 

                  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>

<div class="modal fade" id="detail_pegawai">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Pegawai SIKD</h4>
            </div>
            <form id="form" class="form-horizontal">
                <div class="modal-body">    
                    <div class="box-body">
                    <input type="hidden" value="" name="input_eselon"/>
                    <input type="hidden" value="" name="input_pangkat"/>
                        <div class="form-group">
                            <label for="input_nip" class="col-sm-3 control-label">NIP</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_nip" class="form-control" id="input_nip"
                                    placeholder="NIP" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_nama" class="col-sm-3 control-label">Nama</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_nama" class="form-control" id="input_nama"
                                    placeholder="Fulan" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_nik" class="col-sm-3 control-label">NIK</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_nik" class="form-control" id="input_nik"
                                    placeholder="-" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_pd" class="col-sm-3 control-label">PD</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_pd" class="form-control" id="input_pd"
                                    placeholder="Dinas Komunikasi dan Informatika" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_jabatan" class="col-sm-3 control-label">Jabatan</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_jabatan" class="form-control" id="input_jabatan" placeholder="Jabatan" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_atasan" class="col-sm-3 control-label">Atasan</label>
                            <div class="col-sm-9">
                                <input type="text" name="input_atasan" class="form-control" id="input_atasan"
                                    placeholder="Fulan" readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" name="input_email" id="input_email" readonly>
                                <!-- <input type="" name="input_jeniskelamin" class="form-control" id="input_jeniskelamin" placeholder="Email"> -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input_role" class="col-sm-3 control-label">Role Name</label>
                            <div class="col-sm-9">
                            <select class="form-control" name="input_role" id="input_role">
                                        <option value="" selected>-pilih salah satu-</option>
                            </select>
                                <!-- <input type="text" class="form-control" name="input_role" id="input_role"> -->
                                <!-- <input type="" name="input_jeniskelamin" class="form-control" id="input_jeniskelamin" placeholder="Email"> -->
                            </div>
                        </div>
                        <div class="form-group" >
                            <label for="input_group" class="col-sm-3 control-label">Group</label>
                            <div class="col-sm-9">
                            <select class="form-control" name="input_group" id="input_group">
                                        <option value="">-pilih salah satu-</option>
                                        <?php
                                            foreach($group as $list){
                                            echo "<option value='$list[GroupId]'>$list[GroupName]</option>";
                                            }
                                        ?>
                            </select>
                            </div>
                        </div>
                        
                    </div>
                    <!-- /.box-body -->
                    <!-- /.box-footer -->

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                    <button id="btnSave" type="button" onclick="save()" class="btn btn-primary">Tambahkan ke SIKD</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.content -->
<!-- Page script -->
<script>
var tabel;
$(document).ready(function(){
    // $('#satuan_kerja_id').on('change', function(){
    //     localStorage.setItem('satker', this.value);
    // });
    $('#satuan_kerja_id').select2();
    // if(localStorage.getItem('satker') != ''){
    //     $('#satuan_kerja_id').val(localStorage.getItem('satker'));
    //     // $('#btn-filter').click();
    // }
    $('#btn-filter').on('click', function(e){
        e.preventDefault();
        tabel = $('#tabel_people').DataTable({
        "processing": true,
        "autoWidth": true,
        "serverSide": true,
        'destroy': true,
        "bStateSave": true,
        "order": [],
        "ajax": {
            "data": {satuan_kerja : $('#satuan_kerja_id').val(),},
            "url": "<?php echo site_url("administrator/integrasi_people/get_pegawai_pd") ?>",
            "type": "POST",
        },

        "columnDefs": [
            { "targets": [4], "className": 'text-center'},
            { "targets": [0], "orderable": false},
            ],

    });
    });
});


function filter_on(){
    $('#btn-filter').click();
}

function tambah_people(nip){
    // nip = String(nip);
    swal({
                title: "Tambah data SIKD",
                text: nip+" akan ditambahkan ke SIKD?",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) { 
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_people/tambah_people",
                        dataType: "JSON",
                        data: {
                            nip: nip,
                        },
                        success: function(response, status, xhr) {
                            // alert("sukses");
                            setTimeout(function() {
                                tabel.ajax.reload();
                            }, 500);
                        }
                    });
                    return false;
                }
            });
}

function detail_pegawai(nip) {
        save_method = 'tambah';
        $('#detail_pegawai').modal({
            backdrop: 'static',
            keyboard: false
        })
        $('#form')[0].reset();
        $('.form-group').removeClass('has-error');
        $.ajax({
            url: "<?php echo site_url('administrator/integrasi_people/get_detail_pegawai/') ?>" + nip,
            type: "GET",
            dataType: "JSON",
            success: function(data) {
                        $('#input_role').empty();
                        $.each(data.role, function (key, item) {
                            $('#input_role').append($('<option>', { 
                                value: item.RoleId,
                                text : item.RoleId +'-'+ item.RoleName 
                            }));
                        });
                        $('[name="input_role"]').next().css('max-width','30%');
                        $('[name="input_pangkat"]').val(data.people.pangkat);
                        $('[name="input_eselon"]').val(data.people.eselon);
                        $('[name="input_nik"]').val(data.people.nik);
                        $('[name="input_nip"]').val(data.people.nip);
                        $('[name="input_nama"]').val(data.people.nama);
                        $('[name="input_pd"]').val(data.people.pd);
                        $('[name="input_jabatan"]').val(data.people.jabatan);
                        $('[name="input_atasan"]').val(data.people.atasan);
                        $('[name="input_email"]').val(data.people.email);
                        $('[name="input_email"]').val(data.people.email);
                        if(data.mapping_kosong=='TRUE'){
                            $('#input_role').empty().append('<option value="" selected>-Mapping Per PD masih kosong-</option>');
                        }else{
                            $('#input_role').val(data.role_pegawai);
                        }
                        $('[name="input_group"]').val(data.group);
                        $('#modal_add').modal('show'); // show bootstrap modal when complete loaded
                        $('.modal-title').text('Tambah Pegawai SIKD'); // Set title to Bootstrap modal title
                        $('#btnSave').text('Tambahkan ke SIKD');
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error . . .');
                $('#btnSave').text('Tambahkan ke SIKD');
            }
        });
    }

    function save(){
        $.ajax({
            url: "<?php echo site_url('administrator/integrasi_people/tambah_people') ?>",
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data) {
                if (data.status) {
                    $('#btnSave').text('OK');
                    $('#btnSave').attr('disabled', false);
                    $('#detail_pegawai').modal('hide');
                    tabel.ajax.reload( null, false );
                } else {
                    for (var i = 0; i < data.inputerror.length; i++) {
                        $('[name="' + data.inputerror[i] + '"]').parent().parent().addClass('has-error');
                        $('[name="' + data.inputerror[i] + '"]').attr('placeholder', data.pesanerror[i]);
                    }
                }
                $('#btnSave').text('OK');
                $('#btnSave').attr('disabled', false);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert('Error . . .');
                $('#btnSave').text('OK');
                $('#btnSave').attr('disabled', false);
            }
        });
    }

</script>
