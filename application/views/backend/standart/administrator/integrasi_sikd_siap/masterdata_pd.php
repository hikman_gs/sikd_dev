
<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<link href="<?=BASE_ASSET;?>/select2/select2.min.css"rel="stylesheet" />
<script src="<?=BASE_ASSET;?>/select2/select2.min.js"></script>
<style>
.select2-selection {
  -webkit-box-shadow: 0;
  box-shadow: 0;
  background-color: #fff;
  border: 0;
  border-radius: 0;
  color: #555555;
  font-size: 14px;
  outline: 0;
  min-height: 35px;
  text-align: left;
}

.select2-selection__rendered {
  margin: 7px;
}

.select2-selection__arrow {
  margin: 4px;
}

.select2-selection--single {
  overflow: hidden;
  text-overflow: ellipsis;
  white-space: nowrap;
}

.select2-selection__rendered{
  /* word-wrap: break-word !important; */
  white-space: normal !important;
  text-overflow: hidden !important;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">mapping PD</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header "> 
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Master Data Perangkat Daerah</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
 
                  <div class="box-body">
 
					<table class="table table-bordered" id="tabel11">
						<tbody>
							<tr> 
								<th style="width:3%;" class="text-center">NO</th>
								<th style="width:10%;" class="text-center">KETERANGAN</th>
								<th style="width:30%;" class="text-center">SIAP (UNIT KERJA, JABATAN)</th>
								<th style="width:5%;" class="text-center">Aksi</th>
								<th style="width:30%;" class="text-center">SIKD (ROLE)</th> 
							</tr> 
							<?php
							$no=1;
								foreach ($map as $list) {
								?>
								<tr>
									<td><?=$no++?></td>
									<td>Unit Kerja OPD<input type='hidden' id="<?='unit_kerja_'.$list->mapping_pd_id?>" value='<?=$list->satuan_kerja_id?>'></td>
									<td><?=$list->unit_kerja_nama?></td>
									<td><button type="button" class="btn btn-danger  btn-update" data-id="<?=$list->mapping_pd_id ?>"> Update</button></td>
									<td class="text-center1">
                    <?php 
                    echo $list->rolecode_name;

           //          if(in_array($list->rolecode_id, array_column($grole, 'rolecode_id'))){
										 //  echo(($grole[array_search($list->rolecode_id, array_column($grole, 'rolecode_id'), false)]['GRoleName']));

									  // }else{
           //            echo "-------=====--------";
           //          }
                    ?>
									</br>
									<select name="roleCode" id="roleCode_<?=$list->mapping_pd_id?>" class="form-control" style="width:100%;">
									<option value=''>----- Pilih PD -----</option>
										<?php
											foreach ($roleCode as $key) {?>
											<option value=<?=$key['rolecode_id']?> <?php if($key['rolecode_id'] == $list->rolecode_id){echo "selected";}?>><?=$key['rolecode_name']?></option>
											<?php
											}
										?>
									</select>
									</td>
									
								</tr>
									<?php
								}
							?>
						</tbody>
					</table>
                  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
	$(document).ready(function(){
        $('select').select2();
		$('.btn-update').click(function(){
			var id = $(this).data('id');
            var role = $('#roleCode_'+id).val(); 
            var unit_kerja = $('#unit_kerja_'+id).val(); 
			swal({
                title: "Apakah Data Akan diubah ??",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) { 
					console.log(id+ " dan "+role+" id unitkerja "+unit_kerja);
					$.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_masterpd/update",
                        dataType: "JSON",
                        data: {
                            role: role,
                            id: id,
                            unit_kerja : unit_kerja, 
                        },
                        success: function(response, status, xhr) {
							console.log(response);
							toastr.success('Data Berhasil diperbarui!!');
							setTimeout(function() {
							   location.reload();
                            }, 500);
                        }
                    });
                    return false;
                }
            });
		});
		
		$('.btn-insert').click(function(){
			var id = $(this).data('id');
			var role = $('.roleName-'+id).val(); 
			swal({
                title: "Apakah Data Akan ditambahkan ??",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) { 
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_masterpd/insert",
                        dataType: "JSON",
                        data: {
                            role: role,
                            id: id,  
                        },
                        success: function(response, status, xhr) {
							console.log(id); 
							console.log(response);
							toastr.success('Data Berhasil diperbarui!!');
                            setTimeout(function() {
                              location.reload();
                            }, 500);
                        }
                    });
                    return false;
                }
            });
		});
	}); /*end doc ready*/
</script>