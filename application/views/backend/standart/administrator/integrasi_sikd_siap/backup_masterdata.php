
<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Pengaturan Bahasa</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header "> 
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Master Data</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>
 
                  <div class="box-body">
				        <form method="POST" action="" class="form-horizontal">
						<div class="form-group">
                            <label for="GRoleId" class="col-sm-3 control-label">Perangkat Daerah    
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-9">
                                <select class="form-control"  name="GRoleId" id="GRoleId">
                                  <!--<option value="">-</option>-->
                                  <?php
                                  $grole = $this->db->query("SELECT * FROM `siap_unit_kerja` GROUP BY satuan_kerja_id")->result();
                                  foreach ($grole as $gr):
                                  ?>
                                      <option value="<?= $gr->unit_kerja_id; ?>" <?php echo set_select('GRoleId', $gr->unit_kerja_id, TRUE); ?>><?=$gr->unit_kerja_nama;?></option>
                                  <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
						<div class="form-group">
                        <label for="LastName" class="col-sm-2 control-label"></label>
                        <div class="col-sm-4">
                            <button type="submit" id="btn-filter" class="btn btn-primary">Filter</button> 
                        </div>
                    </div>
						</form>
					<table class="table table-bordered" id="tabel11">
						<tbody>
							<tr> 
								<th style="width:10%;" class="text-center">KETERANGAN</th>
								<th style="width:30%;" class="text-center">SIAP (UNIT KERJA, JABATAN)</th>
								<th style="width:5%;" class="text-center">Aksi</th>
								<th style="width:30%;" class="text-center">SIKD (ROLE)</th> 
							</tr>
							<?php $x=1; ?>
							<?php foreach ($SukRole as $s) { ?>
							<tr>
								<td>Unit Kerja</td>
								<td><?php echo $s->unit_kerja_nama; ?></td>
								<td class="text-center">-</td> 
								<td>
									
									<?php
										
										$pr  = $this->db->query("SELECT * FROM siap_uk_role_  WHERE unit_kerja_id = '".$GRoleId."'");
										$qs1 = $pr->row();
										
										
										$pr  = $this->db->query("SELECT * FROM role  WHERE GRoleId = (SELECT GRoleId FROM role  WHERE RoleId = '".$qs1->RoleId."' LIMIT 1)");
										$qs = $pr->result();
										
									?>
									
									<?php //echo $qs1->RoleDesc; ?>
									<br>
									<select name="RoleId" style="" class="form-control">
										<?php 
										
										foreach ($qs as $q) { ?>
											<option value="<?php echo $q->RoleId; ?>" <?php echo ($q->RoleId==$qs1->RoleId)?'selected':''; ?>><?php echo $q->RoleDesc; ?></option>
										<?php } ?>
										
									</select>
								
								</td>
							</tr>
							<tr>
								<td colspan="3"></td>
								<td> <button type="button" id="btn-filter-<?php echo $x;?>" class="btn btn-danger">Update</button></td>
							</tr> 
							<tr>
								<td colspan="4" style="background-color:#de3"></td>
							</tr> 
							<?php } ?>
						</tbody>
					</table>


                  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
  $(document).ready(function(){

  
      
  }); /*end doc ready*/     
 

</script>
<?php 
	function getJabatanAtasan($jns,$nip_atasan) {
		//jns 'sikd', 'siap'
		$ci=& get_instance();
		
		$tabel = '';
		$q     = '';
		if ($jns == 'sikd') {
			$p = $ci->db->query("SELECT PeopleName, PeoplePosition FROM people WHERE PrimaryRoleId = '".$nip_atasan."'");
			$qs = $p->row();
			$qs = (isset($qs->PeopleName)?$qs->PeopleName:"-").' - '.(isset($qs->PeoplePosition)?$qs->PeoplePosition:"-");
 
		} elseif ($jns == 'siap') { 
			$p = $ci->db->query("SELECT peg_nama, jabatan_nama FROM siap_pegawai WHERE peg_nip = '".$nip_atasan."'");
			$qs = $p->row();
			$qs = $qs->peg_nama.' - '.$qs->jabatan_nama;
		}
		
		return $qs;
	}
	
	function getJabatan($e) {
		//jns 'sikd', 'siap'
		$ci=& get_instance();
		
		$tabel = '';
		$q     = '';
		$p = $ci->db->query("SELECT * FROM siap_jabatan WHERE unit_kerja_id = '".$e."'");
		$qs = $p->row();
		$qs = isset($qs->jabatan_nama)?$qs->jabatan_nama:"-";
		
		return $qs;
	}
	
?>