<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Data Detail Pegawai</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header ">
                     
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Data Detail Pegawai</h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <input type="hidden" name="bulk" id="bulk" value="delete">
                  <div class="box-body">
				<?php
					
					// SIAP
					$pegawai = $this->db->query("SELECT * FROM siap_pegawai WHERE peg_nip = '".$nip."'");
					$pg = $pegawai->row();

					$jabatanjenis = $this->db->query("SELECT jabatan_jenis FROM `siap_jabatan` WHERE jabatan_id = '".$pg->jabatan_jenis."'");
					$jab_jenis = $jabatanjenis->row();
					
					// SIKD
					$people = $this->db->query("SELECT * FROM people WHERE PeopleUsername = '".$nip."'");
					$q = $people->row();
				
					$PrimaryRoleId = '';
					$RoleAtasan = '';
					$GroupId = '';
					$PeoplePosition = '';
					$RoleDesc = '';
					$RoleId = '';
					$PeopleIsActive = '';
					$pesan = '';
					$disable = '';
					if ($people->num_rows() > 0) {
						$role = $this->db->query("SELECT * FROM role WHERE RoleId = '".$q->PrimaryRoleId."'");
						$r = $role->row();
						
						$PrimaryRoleId = $q->PrimaryRoleId;
						$RoleAtasan = $q->RoleAtasan;
						$GroupId = $q->GroupId;
						$PeoplePosition = $q->PeoplePosition;
						$PeopleIsActive = $q->PeopleIsActive;
						$PeopleIsActive = ($PeopleIsActive == '1')?'Aktif':'Non Aktif';
						//
						$RoleDesc = $r->RoleDesc;
						$RoleId = $r->RoleId;
						$GRoleId = $r->GRoleId;
						$RoleParentId = $r->RoleParentId; //add 04-dec-2020 And
						$disable = '';
						
						$grole = $this->db->query("SELECT * FROM master_grole WHERE GRoleId = '".$GRoleId."'");
						$groleN = $grole->row();
					} else {
						$pesan = '<div class="callout callout-danger"><h4>Peringatannn!</h4><p>Data Pegawai di tabel SIKD tidak ditemukan</p></div>';
						$PeopleIsActive = '-';
						$disable = 'disabled';
					}
					// Nonaktifkan dari Sundulan
					// Tugas Belajar Non Aktif
					// Diberhentikan 
					if($pg->kedudukan_pns == 'Diberhentikan') { 
						$kedudukanpns = 'Non Aktif';
					} elseif($pg->kedudukan_pns == 'Non Aktif') {
						$kedudukanpns = 'Non Aktif';
					} else {
						$kedudukanpns = $pg->kedudukan_pns;
					}
				?>
				<form class="form-horizontal">
				<?php echo $pesan; ?>
				<?php 
					$items_ = $this->session->userdata('items_');
					message_flash($items_, 'success');
				?>
			<table class="table table-bordered" >
		<tbody>
			<tr> 
				<th style="width:10%;" class=" text-center">KETERANGAN</th>
				<th style="width:30%;" class=" text-center">TABEL SIAP</th> 
				<th style="width:20%;" class=" text-center">Aksi</th>
				<th style="width:30%;" class=" text-center">TABEL SIKD</th>
				
			</tr>
			<tr>
				<td>NIP</td>
				<td><?php echo $pg->peg_nip;?></td>
				<td class="text-center"> </td> 
				<td><?=isset($q->NIP)?$q->NIP:'-';;?></td>
				
			</tr> 
			<tr>
				<td>NAMA</td>
				<td><?php echo $pg->peg_nama; ?></td>
				<td class="text-center">
					<button <?php echo $disable;?> type="button" class="btn btn-success  btn-sm btn-block" id="btn_nama">Update <i class="fa fa-fw fa-arrow-right"></i></button><!-- -->
				<td><?=isset($q->PeopleName)?$q->PeopleName:'-';?></td>
				
			</tr>
			<tr>
				<td>PERANGKAT DAERAH</td>
				<td><?php echo $pg->satuan_kerja_nama; ?></td>
				<td class="text-center"> </td>
				<td><?php echo $groleN->GRoleName;?></td>
				
			</tr>
			<tr>
				<td>JABATAN - UNIT KERJA</td>
				<td>
					<?php echo $pg->jabatan_nama;?> - <?php echo $pg->unit_kerja_nama;?>
					<?php 
						
						$people2 = $this->db->query("SELECT RoleId,gjabatanId,GRoleId,eselon FROM mapping_per_pd WHERE unit_kerja_id = '".$pg->unit_kerja_id."'");
						$qry2 = $people2->row();
						if ($pg->jabatan_jenis != 'Struktural') { 
							if (($pg->jabatan_jenis == 'Fungsional Umum') OR (($pg->jabatan_jenis == 'Fungsional Tertentu') and ($qry2->gjabatanId != 'XxJyPn38Yh.11') and ($qry2->gjabatanId != 'XxJyPn38Yh.7'))){
								
								if (($pg->jabatan_jenis == 'Fungsional Tertentu') and ($qry2->eselon != 'IV.a') and ($qry2->eselon != 'IV.b')) {
									$people = $this->db->query("SELECT * FROM role WHERE GRoleId = '".$qry2->GRoleId."' and gjabatanId in ('XxJyPn38Yh.7','XxJyPn38Yh.11') ");
									//var_dump('IEU '.$people.'TSET');exit();
								}else{
									
									$people = $this->db->query("SELECT * FROM role WHERE RoleParentId = '".$qry2->RoleId."'");
									//var_dump('IEU '.$people.'TSET');exit();
								}
								
								$qry1 = $people->result();
								if($people->num_rows() > 0) {
							?>
							<br>
							<?php if($qry1) { ?>
								<select name="RoleId" id="RoleId" style="width:300px;"  <?php echo $disable;?> >
								<?php foreach ($qry1 as $qr) { 

									?>
								<?php if($qr->RoleName != 'Administrator') { ?>
									<option value="<?php echo $qr->RoleId; ?>"><?php echo $qr->RoleName; ?></option>
								<?php } ?>
								<?php } ?>
								</select>
							<?php } ?>
							<?php } ?>

						<?php } ?>
					<?php } ?>
				</td>
				<td class="text-center">
					<button  <?php echo $disable;?>  type="button" class="btn btn-success  btn-sm btn-block" id="btn_jabatan">Update  <i class="fa fa-fw fa-arrow-right"></i></button>
				</td> 
				<td> <?=isset($q->PeoplePosition)?$q->PeoplePosition:'-';?></td>
			</tr>
			<tr>
				<td>EMAIL</td>
				<td><?php echo $pg->peg_email;?></td>
				<td class="text-center"> 
					<button  <?php echo $disable;?>  type="button" id="btn_emails" class="btn btn-success  btn-sm btn-block">Update  <i class="fa fa-fw fa-arrow-right"></i></button>
				</td> 
				<td><?=isset($q->Email)?$q->Email:'-';?></td>
			</tr>
			<tr>
				<td>KEDUDUKAN PNS</td>
				<td><?php echo $kedudukanpns;?></td>
				<td class="text-center"> 
					<button  <?php echo $disable;?>  type="button" id="btn_pensiuns" class="btn btn-success  btn-sm btn-block">Update  <i class="fa fa-fw fa-arrow-right"></i></button>
				</td> 
				<td><?=$PeopleIsActive;?></td>
			</tr>
			
			<tr>
				<td>UNIT KERJA / JABATAN ATASAN</td>
				<td><?php echo getJabatanAtasan('siap', $pg->nip_atasan);?></td>
				<td class="text-center"> 
					<button  <?php echo $disable;?>  type="button" id="btn_atasan" class="btn btn-success  btn-sm btn-block">Update  <i class="fa fa-fw fa-arrow-right"></i></button>
				</td> 
				<td><?php echo getJabatanAtasan('sikd', isset($q->RoleAtasan)?$q->RoleAtasan:'0');?></td>
			</tr>
					<br>
					<input type="hidden" name="PrimaryRoleId" id="PrimaryRoleId" value="<?php echo $PrimaryRoleId;?>">
					<input type="hidden" name="RoleAtasan" id="RoleAtasan" value="<?php echo $RoleAtasan;?>">
					<input type="hidden" name="GroupId" id="GroupId" value="<?php echo $GroupId;?>">
					<input type="hidden" name="PeoplePosition" id="PeoplePosition" value="<?php echo $PeoplePosition;?>">
					<input type="hidden" name="RoleId" id="RoleId" value="<?php echo $RoleId;?>">
					<input type="hidden" name="kedudukan_pns" id="kedudukan_pns" value="<?php echo $pg->kedudukan_pns;?>">
					<br>
					<input type="hidden" name="unit_kerja_id" id="unit_kerja_id" value="<?php echo $pg->unit_kerja_id;?>">
					<input type="hidden" name="jabatan_id" id="jabatan_id" value="<?php echo $pg->jabatan_id;?>">
					<input type="hidden" name="email" id="email" value="<?php echo $pg->peg_email;?>">
					<input type="hidden" name="nip" id="nip" value="<?php echo $pg->peg_nip;?>">
					<input type="hidden" name="peg_nama" id="peg_nama" value="<?php echo $pg->peg_nama;?>">


			</tr> 
		</tbody>
	</table>
	</form>
	
                  </div> 
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->
<?php 
	function getJabatanAtasan($jns,$nip_atasan) {
		//jns 'sikd', 'siap'
		$ci=& get_instance();
		
		$tabel = '';
		$q     = '';
		if ($jns == 'sikd') {
			$p = $ci->db->query("SELECT RoleName FROM role WHERE RoleId = '".$nip_atasan."'");
			$qs = $p->row();
			$qs = (isset($qs->RoleName)?$qs->RoleName:"-");
 
		} elseif ($jns == 'siap') { 
			$p = $ci->db->query("SELECT * FROM siap_pegawai JOIN siap_unit_kerja ON siap_pegawai.unit_kerja_id = siap_unit_kerja.unit_kerja_id  WHERE peg_nip = '".$nip_atasan."'");
			$qr = $p->row();
			$qnum = $p->num_rows();
			if( $qnum > 0 ) {
				$qs = $qr->unit_kerja_nama.' ('.$qr->peg_nama.')';
			} else {
				$qs = '';
			}
		}
		
		return $qs;
	}
	
	function getGeROLE($jns) {
		//jns 'sikd', 'siap'
		$ci=& get_instance();
		
		$tabel = '';
		$q     = '';
		$qs = '';
		  if ($jns == 'sikd') {
			$p = $ci->db->query("SELECT RoleName FROM role WHERE RoleId = '".$nip_atasan."'");
			$qs = $p->row();
			$qs = (isset($qs->RoleName)?$qs->RoleName:"-");
 
		} elseif ($jns == 'siap') { 
			$p = $ci->db->query("SELECT * FROM siap_pegawai JOIN siap_unit_kerja ON siap_pegawai.unit_kerja_id = siap_unit_kerja.unit_kerja_id  WHERE peg_nip = '".$nip_atasan."'");
			$qr = $p->row();
			$qnum = $p->num_rows();
			if( $qnum > 0 ) {
				$qs = $qr->unit_kerja_nama.' ('.$qr->peg_nama.')';
			} else {
				$qs = '';
			}
		}
		
		return $qs;
	}
	
?>
<!-- Page script -->
<script>
	$(document).ready(function() {
    $('#btn_pensiuns').on('click', function() {
        swal({
                title: "Apakah Data Akan diubah ??",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    var kedudukan_pns = $('#kedudukan_pns').val();
                    var nip = $('#nip').val();
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_notifikasi/updatepensiun",
                        dataType: "JSON",
                        data: {
                            kedudukan_pns: kedudukan_pns,
                            nip: nip
                        },
                        success: function(response, status, xhr) {
                            console.log(response);
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        }
                    });
                    return false;
                }
            });
    }); 
	
	$('#btn_emails').on('click', function() {
        swal({
                title: "Apakah Data Akan diubah ??",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    var email = $('#email').val();
                    var nip = $('#nip').val();
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_notifikasi/updateemail",
                        dataType: "JSON",
                        data: {
                            email: email,
                            nip: nip
                        },
                        success: function(response, status, xhr) {
                            console.log(response);
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        }
                    });
                    return false;
                }
            });
    });

    $('#btn_unitKerja').on('click', function() {
        swal({
                title: "Apakah Data Akan diubah ??",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {

                    var PrimaryRoleId = $('#PrimaryRoleId').val();
                    var PeoplePosition = $('#PeoplePosition').val();
                   
                    var nip = $('#nip').val();
                    var RoleAtasan = $('#RoleAtasan').val();
                    var GroupId = $('#GroupId').val();
                    var RoleId = $('#RoleId').val();
                    var unit_kerja_id = $('#unit_kerja_id').val();
                    var peg_nama = $('#peg_nama').val();
                    var RoleIds = $('#RoleId').val();

                    //console.log(BASE_URL);
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_notifikasi/updateUnitKerja",
                        dataType: "JSON",
                        data: {
                            PrimaryRoleId: PrimaryRoleId,
                            nip: nip,
                            PeoplePosition: PeoplePosition,
                            RoleAtasan: RoleAtasan,
                            GroupId: GroupId,
                            RoleIds: RoleIds,
                            unit_kerja_id: unit_kerja_id,
                            peg_nama: peg_nama
                        },
                        success: function(response, status, xhr) {
                            console.log(response);
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        }
                    });
                    return false;
                }
            });
    });

    $('#btn_atasan').on('click', function() {
        swal({
                title: "Apakah Data Akan diubah ??",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {
                    var nip = $('#nip').val();
                    var RoleAtasan = $('#RoleAtasan').val();
                    var unit_kerja_id = $('#unit_kerja_id').val();

                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_notifikasi/updateAtasan",
                        dataType: "JSON",
                        data: {
                            nip: nip,
                            unit_kerja_id: unit_kerja_id,
                            RoleAtasan: RoleAtasan
                        },
                        success: function(response, status, xhr) {
                            console.log(response);
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        }
                    });
                    return false;

                }
            });
    });

    //Update Jabatan
    $('#btn_jabatan').on('click', function() {
        swal({
                title: "Apakah Data Akan diubah ??",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {


                    var nip = $('#nip').val();
                    var RoleAtasan = $('#RoleAtasan').val();
                    var unit_kerja_id = $('#unit_kerja_id').val();
                    var PrimaryRoleId = $('#PrimaryRoleId').val();
                    var RoleIds = $('#RoleId').val();
                    //alert(RoleIds);
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_notifikasi/updateJabatan",
                        dataType: "JSON",
                        data: {
                            nip: nip,
                            unit_kerja_id: unit_kerja_id,
                            PrimaryRoleId: PrimaryRoleId,
                            RoleAtasan: RoleAtasan,
                            RoleIds : RoleIds
                        },
                        success: function(response, status, xhr) {
                            console.log(response);
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        }
                    });
                    return false;
                }
            });
    });

    $('#btn_nip').on('click', function() {
        swal({
                title: "Apakah Data Akan diubah ??",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {

                    var nip = $('#nip').val();
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_notifikasi/updateNip",
                        dataType: "JSON",
                        data: {
                            nip: nip
                        },
                        success: function(response, status, xhr) {
                            console.log(response);
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        }
                    });
                    return false;

                }
            });
    });

    $('#btn_nama').on('click', function() {
        swal({
                title: "Apakah Data Akan diubah ??",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ya",
                cancelButtonText: "Tidak",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function(isConfirm) {
                if (isConfirm) {

                    var nip = $('#nip').val();
                    var peg_nama = $('#peg_nama').val();
                    $.ajax({
                        type: "POST",
                        url: BASE_URL + "administrator/integrasi_notifikasi/updateNama",
                        dataType: "JSON",
                        data: {
                            nip: nip,
                            peg_nama: peg_nama
                        },
                        success: function(response, status, xhr) {
                            console.log(response);
                            setTimeout(function() {
                                location.reload();
                            }, 500);
                        }
                    });
                    return false;
                }
            });
    });
});
</script>