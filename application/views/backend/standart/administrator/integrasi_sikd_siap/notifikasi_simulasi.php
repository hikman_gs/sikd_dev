<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<!-- Content Header (Page header) -->
<section class="content-header">
   <h1><small></small></h1>
   <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
      <li class="active">Data Webhook Pegawai</li>
   </ol>
</section>
<!-- Main content -->
<section class="content">
   <div class="row" >
      
      <div class="col-md-12">
         <div class="box box-warning">
            <div class="box-body ">
               <!-- Widget: user widget style 1 -->
               <div class="box box-widget widget-user-2">
                  <!-- Add the bg color to the header using any of the bg-* classes -->
                  <div class="widget-user-header "> 
                     <!-- /.widget-user-image -->
                     <h3 class="widget-user-username">Data Webhook Pegawai </h3>
                     <h5 class="widget-user-desc">&nbsp;</h5>
                     <hr>
                  </div>

                  <input type="hidden" name="bulk" id="bulk" value="delete">
                  <div class="box-body">
                   	<table id="example" class="table table-striped table-bordered nowrap" style="width:100%">
					    <thead>
					        <tr role="row"> 
					            <th>NIP</th> 
					            <th>NAMA</th> 
					            <th>STATUS</th> 
					            <th style='width:15%' class="text-center">ACTION</th> 
					    </thead>
					    <tbody> 
						<?php 
							$list_anggota = $this->db->query("SELECT peg_nip, peg_nama, peg_status FROM siap_notif_simulasi WHERE status=0");
							$lists = $list_anggota->result();
							foreach ($lists as $r):
                      ?>
                      <tr>  
                        <td><?=$r->peg_nip;?></td>
                        <td><?=$r->peg_nama;?></td>
                        <td><?=$r->peg_status;?></td>
                        <td>
							<div class="text-center">
								<a title="Detail" href="<?= site_url('administrator/integrasi_notifikasi_simulasi/detail/'.$r->peg_nip); ?>" class="btn btn-success  btn-sm" role="button"><i class="fa fa-info" aria-hidden="true"></i> Detail</a>
								<a title="Done" href="<?= site_url('administrator/integrasi_notifikasi_simulasi/updatestatus/'.$r->peg_nip); ?>" class="btn btn-info  btn-sm" role="button"> <i class="fa fa-check" aria-hidden="true"></i> Done</a>
							</div>
						</td>
                         
                      </tr>
                      <?php endforeach; ?>
						</tbody>
					</table>
                  </div>
                  </form>
               </div>
            </div>
            <!--/box body -->
         </div>
         <!--/box -->
      </div>
   </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
  $(document).ready(function(){

    $('#example').DataTable( {
        "order": [[ 1, "asc" ]]
    } );
      
  }); /*end doc ready*/     


</script>