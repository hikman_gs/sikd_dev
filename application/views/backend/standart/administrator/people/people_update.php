<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/ui.dynatree.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/tree.skinklas.css">

<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.form.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.dynatree.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/dynatree_unitdetail.js"></script>
<style>
ul.dynatree-container {
    width: 100% !important;
    max-width: 100% !important;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a href="<?= site_url('administrator/anri_master_pengguna'); ?>">Pengaturan Pengguna</a></li>
        <li class="active">Ubah Data</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2" style="box-shadow:none">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Ubah Data</h3>
                            <h5 class="widget-user-desc">Pengguna</h5>
                            <hr>
                        </div>
                        <?= form_open(base_url('administrator/anri_master_pengguna/update_save'), [
                            'name'    => 'form_people', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_people', 
                            'method'  => 'POST'
                            ]); ?>
                        <?php 

                        $this->db->where('PeopleId',$this->uri->segment(4));
                        $this->db->join('role','role.RoleId = people.PrimaryRoleId');
                        $peoples = $this->db->get('people')->result();
                        foreach($peoples as $people):
                        ?>

                        <div class="form-group row hidden">
                            <label for="PeopleKey" class="col-sm-2 control-label">PeopleKey
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleKey" id="PeopleKey"
                                    placeholder="PeopleKey" value="<?= set_value('PeopleKey', $people->PeopleKey); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group hidden">
                            <label for="PeopleId" class="col-sm-2 control-label">PeopleId
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleId" id="PeopleId"
                                    placeholder="PeopleId" value="<?= set_value('PeopleId', $people->PeopleId); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group row" hidden>
                            <label for="PeopleId" class="col-sm-2 control-label">RoleId
                            </label>
                            <div class="col-sm-8">
                                <input type="text" name="RoleId" id="RoleId" value="<?= $people->PrimaryRoleId; ?>">
                                <input type="text" name="RoleIdA" id="RoleIdA" value="<?= $people->PrimaryRoleId; ?>">
                                <small class="info help-block">
                                </small>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="PrimaryRoleId" class="col-sm-2 control-label">Unit Kerja Asal
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <label for="PrimaryRoleId" class="control-label"><?= $people->RoleDesc; ?>
                                </label> <button title="Ubah Unit Kerja Baru" type="button" onclick="openuk();"
                                    class="btn btn-info btn-xs"><i class="fa fa-folder-open"></i></button>
                            </div>
                        </div>

                        <div id="unitkerja" class="form-group hidden">
                            <label for="PrimaryRoleId" class="col-sm-2 control-label">Unit Kerja Baru
                                <i class="required">*</i>
                            </label>
                            <button title="Batalkan" type="button" onclick="openuk_batal();"
                                class="btn btn-info btn-xs"> X
                            </button>
                            <div class="col-sm-8" id="treeUK">
                                <?php
                            if($this->session->userdata('groupid') == "1"){
                              $parent = 'root';
                              $query = $this->db->query("SELECT RoleParentId as parent_id, 
                                        RoleId as id, 
                                        RoleDesc as name, 
                                        Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId,'|',RoleCode) as allData
                                        FROM role 
                                      WHERE RoleId != 'root'
                                      ORDER BY RoleId,RoleParentId ")->result();
                            } else {
                              $parent = 'root';
                              $query = $this->db->query("SELECT RoleParentId as parent_id, 
                                        RoleId as id, 
                                        RoleDesc as name, 
                                        Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId,'|',RoleCode) as allData
                                        FROM role 
                                      WHERE RoleId != 'root'
                                      ORDER BY RoleId,RoleParentId ")->result();
                            }

                            $data = array();
                            foreach ($query as $row) {
                              $data[$row->parent_id][] = $row;
                            }
                            echo showUK($data,$parent); // lakukan looping menu utama
                            ?>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="PeoplePosition" class="col-sm-2 control-label">Tipe Pengguna
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="GroupId" id="GroupId"
                                    data-placeholder="Select Grup Jabatan">
                                    <option value="">Pilih</option>
                                    <?php 
                                    if($this->session->userdata('groupid') != "1"){
                                      $groups = $this->db->query("select GroupId, GroupName from groups where GroupKey = '" . tb_key() . "' and GroupId > 1")->result();
                                    } else {
                                      $groups = $this->db->query("select GroupId, GroupName from groups where GroupKey = '" . tb_key() . "' ")->result();
                                    }
                                    foreach ($groups as $row): ?>
                                    <option <?=  $row->GroupId ==  $people->GroupId ? 'selected' : ''; ?>
                                        value="<?= $row->GroupId ?>"><?= $row->GroupName; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="PrimaryRoleId" class="col-sm-2 control-label">Jabatan Atasan Langsung
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control chosen chosen-select-deselect" name="RoleAtasan"
                                    id="RoleAtasan" placeholder="Select Jabatan Atasan Langsung">
                                    <option value="">Pilih</option>
                                    <?php 
                                    $role = $this->db->query("select RoleId, RoleParentId, RoleName from role where RoleId not in ('root','uk') and RoleStatus = 1 and gjabatanId != '' order by gjabatanId ASC")->result();
                                    foreach ($role as $row): ?>
                                    <option <?=  $row->RoleId ==  $people->RoleAtasan ? 'selected' : ''; ?>
                                        value="<?= $row->RoleId ?>"><?= $row->RoleName; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="PeopleName" class="col-sm-2 control-label">Nama Lengkap
                                <i class="required">*</i>
                            </label>

                            <?php
                                $jab = $this->db->query("select RoleId_From from inbox_receiver where RoleId_To = '" . $people->PrimaryRoleId . "' ")->num_rows();
                                if($jab > 0) {
                            ?>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleName" id="PeopleName" placeholder="Nama Lengkap" value="<?= set_value('PeopleName', $people->PeopleName); ?>" required readonly>
                            </div>
                            <?php
                                } else {
                            ?>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleName" id="PeopleName" placeholder="Nama Lengkap" value="<?= set_value('PeopleName', $people->PeopleName); ?>" required>
                            </div>
                            <?php
                                }
                            ?>

                        </div>

                        <div class="form-group row">
                            <label for="PeoplePosition" class="col-sm-2 control-label">Nama Jabatan
                                <i class="required">*</i>
                            </label>

                            <?php
                                $jab = $this->db->query("select RoleId_From from inbox_receiver where RoleId_To = '" . $people->PrimaryRoleId . "' ")->num_rows();
                                if($jab > 0) {
                            ?>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="PeoplePosition" id="RoleDesc" placeholder="Nama Jabatan" value="<?= set_value('PeoplePosition', $people->PeoplePosition); ?>" required readonly>
                                </div>
                            <?php
                                } else {
                            ?>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" name="PeoplePosition" id="RoleDesc" placeholder="Nama Jabatan" value="<?= set_value('PeoplePosition', $people->PeoplePosition); ?>" required>
                                </div>
                            <?php
                                }
                            ?>
                        </div>

                        <div class="form-group row">
                            <label for="NIP" class="col-sm-2 control-label">NIP
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="NIP" id="NIP" placeholder="NIP"
                                    value="<?= set_value('NIP', $people->NIP); ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ApprovelName" class="col-sm-2 control-label">Nama Penandatangan
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="ApprovelName" id="ApprovelName"
                                    placeholder="Nama Penandatangan"
                                    value="<?= set_value('ApprovelName', $people->ApprovelName); ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="NIK" class="col-sm-2 control-label">N I K
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" maxlength="16" name="NIK" id="NIK" placeholder="Nomor Induk kependudukan"
                                    value="<?= set_value('NIK', $people->NIK); ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="Email" class="col-sm-2 control-label">Email
                            </label>
                            <div class="col-sm-8">
                                <input type="Email" class="form-control" name="Email" id="Email" placeholder="Email"
                                    value="<?= set_value('Email', $people->Email); ?>">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="PeopleIsActive" class="col-sm-2 control-label">Status Aktif
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="checkbox"
                                    <?= in_array('1', explode(',', $people->PeopleIsActive)) ? 'checked' : ''; ?>
                                    name="PeopleIsActive" id="PeopleIsActive" value="1">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="PeopleUsername" class="col-sm-2 control-label">Login Pengguna
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleUsername" id="PeopleUsername"
                                    placeholder="Login Pengguna"
                                    value="<?= set_value('PeopleUsername', $people->PeopleUsername); ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="PeoplePassword" class="col-sm-2 control-label">Kata Sandi
                                <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="password" class="form-control" name="PeoplePassword" id="PeoplePassword" placeholder="Kata Sandi">
                            </div>
                        </div>
                        <?php endforeach; ?>

                        <div class="message"></div>
                        <div class="row-fluid col-md-7"><br>
                            <input type="submit" name="submit" name="submit" value="Simpan"
                                class="btn btn-flat btn-danger">
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->

<!-- Page script -->
<script>
function openuk() {
    $("#unitkerja").prop('class', 'form-group row');
}

function openuk_batal() {
    $("#RoleId").val($("#RoleIdA").val());
    $("#unitkerja").prop('class', 'form-group hidden');
}
</script>