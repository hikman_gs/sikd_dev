
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Tambah Data   <!--     <small><?= cclang('new', ['Pengguna']); ?> </small>  -->
    </h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class=""><a  href="<?= site_url('administrator/anri_dashboard/people'); ?>">Pengguna</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Tambah Data</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Pengguna']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open('', [
                            'name'    => 'form_people', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_people', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                         
                        <div class="form-group ">
                            <label for="PeopleKey" class="col-sm-2 control-label">PeopleKey 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleKey" id="PeopleKey" placeholder="PeopleKey" value="<?= set_value('PeopleKey'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="PeopleId" class="col-sm-2 control-label">PeopleId 
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="PeopleId" id="PeopleId" placeholder="PeopleId" value="<?= set_value('PeopleId'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="PeopleName" class="col-sm-2 control-label">PeopleName 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleName" id="PeopleName" placeholder="PeopleName" value="<?= set_value('PeopleName'); ?>">
                                <small class="info help-block">
                                <b>Input PeopleName</b> Max Length : 150.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="PeoplePosition" class="col-sm-2 control-label">PeoplePosition 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeoplePosition" id="PeoplePosition" placeholder="PeoplePosition" value="<?= set_value('PeoplePosition'); ?>">
                                <small class="info help-block">
                                <b>Input PeoplePosition</b> Max Length : 150.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="PeopleUsername" class="col-sm-2 control-label">PeopleUsername 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleUsername" id="PeopleUsername" placeholder="PeopleUsername" value="<?= set_value('PeopleUsername'); ?>">
                                <small class="info help-block">
                                <b>Input PeopleUsername</b> Max Length : 40.</small>
                            </div>
                        </div>
                                                 
                        <div class="form-group ">
                            <label for="PeoplePassword" class="col-sm-2 control-label">PeoplePassword 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeoplePassword" id="PeoplePassword" placeholder="PeoplePassword" value="<?= set_value('PeoplePassword'); ?>">
                                <small class="info help-block">
                                <b>Input PeoplePassword</b> Max Length : 250.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="PeopleActiveStartDate" class="col-sm-2 control-label">PeopleActiveStartDate 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="PeopleActiveStartDate"  placeholder="PeopleActiveStartDate" id="PeopleActiveStartDate">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="PeopleActiveEndDate" class="col-sm-2 control-label">PeopleActiveEndDate 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-6">
                            <div class="input-group date col-sm-8">
                              <input type="text" class="form-control pull-right datepicker" name="PeopleActiveEndDate"  placeholder="PeopleActiveEndDate" id="PeopleActiveEndDate">
                            </div>
                            <small class="info help-block">
                            </small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="PeopleIsActive" class="col-sm-2 control-label">PeopleIsActive 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PeopleIsActive" id="PeopleIsActive" placeholder="PeopleIsActive" value="<?= set_value('PeopleIsActive'); ?>">
                                <small class="info help-block">
                                <b>Input PeopleIsActive</b> Max Length : 1.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="PrimaryRoleId" class="col-sm-2 control-label">PrimaryRoleId 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="PrimaryRoleId" id="PrimaryRoleId" placeholder="PrimaryRoleId" value="<?= set_value('PrimaryRoleId'); ?>">
                                <small class="info help-block">
                                <b>Input PrimaryRoleId</b> Max Length : 250.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="GroupId" class="col-sm-2 control-label">GroupId 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="number" class="form-control" name="GroupId" id="GroupId" placeholder="GroupId" value="<?= set_value('GroupId'); ?>">
                                <small class="info help-block">
                                <b>Input GroupId</b> Max Length : 11.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="RoleAtasan" class="col-sm-2 control-label">RoleAtasan 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="RoleAtasan" id="RoleAtasan" placeholder="RoleAtasan" value="<?= set_value('RoleAtasan'); ?>">
                                <small class="info help-block">
                                <b>Input RoleAtasan</b> Max Length : 250.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="NIP" class="col-sm-2 control-label">NIP 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="NIP" id="NIP" placeholder="NIP" value="<?= set_value('NIP'); ?>">
                                <small class="info help-block">
                                <b>Input NIP</b> Max Length : 50.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="ApprovelName" class="col-sm-2 control-label">ApprovelName 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="ApprovelName" id="ApprovelName" placeholder="ApprovelName" value="<?= set_value('ApprovelName'); ?>">
                                <small class="info help-block">
                                <b>Input ApprovelName</b> Max Length : 150.</small>
                            </div>
                        </div>
                                                 
                                                <div class="form-group ">
                            <label for="Email" class="col-sm-2 control-label">Email 
                            <i class="required">*</i>
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Email" id="Email" placeholder="Email" value="<?= set_value('Email'); ?>">
                                <small class="info help-block">
                                <b>Input Email</b> Max Length : 100.</small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                           <button class="btn btn-flat btn-primary btn_save btn_action" id="btn_save" data-stype='stay' title="<?= cclang('save_button'); ?> (Ctrl+s)">
                            <i class="fa fa-save" ></i> <?= cclang('save_button'); ?>
                            </button>
                            <a class="btn btn-flat btn-info btn_save btn_action btn_save_back" id="btn_save" data-stype='back' title="<?= cclang('save_and_go_the_list_button'); ?> (Ctrl+d)">
                            <i class="ion ion-ios-list-outline" ></i> <?= cclang('save_and_go_the_list_button'); ?>
                            </a>
                            <a class="btn btn-flat btn-default btn_action" id="btn_cancel" title="<?= cclang('cancel_button'); ?> (Ctrl+x)">
                            <i class="fa fa-undo" ></i> <?= cclang('cancel_button'); ?>
                            </a>
                            <span class="loading loading-hide">
                            <img src="<?= BASE_ASSET; ?>/img/loading-spin-primary.svg"> 
                            <i><?= cclang('loading_saving_data'); ?></i>
                            </span>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
                   
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/people';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_people = $('#form_people');
        var data_post = form_people.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/people/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
 
       
    
    
    }); /*end doc ready*/
</script>