<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/ui.dynatree.css">
<link rel="stylesheet" type="text/css" href="<?= BASE_ASSET; ?>dynatree/tree.skinklas.css">

<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.form.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.cookie.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.dynatree.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/dynatree_unit.js"></script>
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery.dynatree_role.js"></script>
<style>
.chosen-container {
    width: 100% !important;
}

ul.dynatree-container {
    width: 100% !important;
    max-width: 100% !important;
}
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">Pengaturan Pengguna</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">

        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="row pull-right">
                                <button title="Tambah Data" type="button" class="btn btn-success" data-toggle="modal"
                                    data-target="#modal-default"><i class="fa fa-plus-square-o"></i> Tambah Data
                                </button>
                            </div>
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/list.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Pengaturan Pengguna</h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="example1">
                                <thead>
                                    <tr class="">
                                        <th width="5%">
                                            <input type="checkbox" class="flat-red toltip" id="check_all"
                                                name="check_all" title="check all">
                                        </th>
                                        <th>Nama Lengkap</th>
                                        <th>Unit Kerja</th>
                                        <th>Jabatan</th>
                                        <th width="20%">Nama Pengguna</th>
                                        <th>Status</th>
                                        <th width="15%">Aksi</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>

                        <div class="table-responsive">

                        </div>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<div class="modal fade" id="modal-default">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Data Pengguna</h4>
            </div>
            <div class="modal-body">
                <?= form_open(BASE_URL('administrator/anri_master_pengguna/add_save'))?>
                <div class="form-group row hidden">
                    <label for="PeopleKey" class="col-sm-4 control-label">PeopleKey
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="PeopleKey" id="PeopleKey" placeholder="PeopleKey"
                            value="<?= tb_key(); ?>">
                        <small class="info help-block">
                        </small>
                    </div>
                </div>
                <div class="form-group row hidden">
                    <label for="PeopleId" class="col-sm-4 control-label">PeopleId
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="PeopleId" id="PeopleId" placeholder="PeopleId"
                            value="<?= increment_id('people'); ?>">
                        <small class="info help-block">
                        </small>
                    </div>
                </div>
                <div class="form-group row hidden ">
                    <label for="PeopleId" class="col-sm-2 control-label">RoleId
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="RoleId" id="RoleId" placeholder="RoleId">
                        <small class="info help-block">
                        </small>
                    </div>
                </div>

                <script type="text/javascript"
                    src="https://arsipdinamis.com:443/sikd/asset/dynatree/jquery.dynatree_role.js"></script>
                <div class="form-group row">
                    <label for="PrimaryRoleId" class="col-sm-4 control-label">Unit Kerja
                        <i class="required">*</i>
                    </label>
                    <div class="col-sm-8" id="treeUK">
                        <?php
                            if($this->session->userdata('groupid') == "1"){
                              $parent = 'root';
                              $query = $this->db->query("SELECT RoleParentId as parent_id, 
                                        RoleId as id, 
                                        RoleDesc as name, 
                                        Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId,'|',RoleCode) as allData
                                        FROM role 
                                      WHERE RoleId != 'root' AND RoleStatus ='1'
                                      ORDER BY RoleId,RoleParentId ")->result();
                            } else {
                              $parent = 'root';
                              $query = $this->db->query("SELECT RoleParentId as parent_id, 
                                        RoleId as id, 
                                        RoleDesc as name, 
                                        Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId,'|',RoleCode) as allData
                                        FROM role 
                                      WHERE RoleId != 'root' AND RoleStatus ='1'
                                      ORDER BY RoleId,RoleParentId ")->result();
                            }

                            $data = array();
                            foreach ($query as $row) {
                              $data[$row->parent_id][] = $row;
                            }
                            echo showUK($data,$parent); // lakukan looping menu utama
                              ?>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="PeoplePosition" class="col-sm-4 control-label">Tipe Pengguna
                    </label>
                    <div class="col-sm-8">
                        <select class="form-control chosen chosen-select-deselect" name="GroupId" id="GroupId"
                            data-placeholder="Select Grup Jabatan">
                            <option value="">Pilih</option>
                            <?php 
                                    if($this->session->userdata('groupid') != "1"){
                                      $groups = $this->db->query("select GroupId, GroupName from groups where GroupKey = '" . tb_key() . "' and GroupId > 1")->result();
                                    } else {
                                      $groups = $this->db->query("select GroupId, GroupName from groups where GroupKey = '" . tb_key() . "' ")->result();
                                    }
                                    foreach ($groups as $row): ?>
                            <option value="<?= $row->GroupId ?>"><?= $row->GroupName; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>


                <div class="form-group row">
                    <label for="PrimaryRoleId" class="col-sm-4 control-label">Jabatan Atasan Langsung
                    </label>
                    <div class="col-sm-8">
                        <select class="form-control chosen chosen-select-deselect" name="RoleAtasan" id="RoleAtasan"
                            placeholder="Select Jabatan Atasan Langsung">
                            <option value="">Pilih</option>
                            <?php 
                                    $role = $this->db->query("select RoleId, RoleParentId, RoleName from role where RoleId not in ('root','uk') and RoleStatus = 1 and gjabatanId != '' order by gjabatanId ASC")->result();
                                    foreach ($role as $row): ?>
                            <option value="<?= $row->RoleId ?>"><?= $row->RoleName; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="PeopleName" class="col-sm-4 control-label">Nama Lengkap
                        <i class="required">*</i>
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="PeopleName" id="PeopleName"
                            placeholder="Nama Lengkap" value="<?= set_value('PeopleName'); ?>" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="PeoplePosition" class="col-sm-4 control-label">Nama Jabatan
                        <i class="required">*</i>
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="PeoplePosition" id="RoleDesc"
                            placeholder="Nama Jabatan" value="<?= set_value('PeoplePosition'); ?>" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="NIP" class="col-sm-4 control-label">NIP
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="NIP" id="NIP" placeholder="NIP"
                            value="<?= set_value('NIP'); ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="ApprovelName" class="col-sm-4 control-label">Nama Penandatangan
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="ApprovelName" id="ApprovelName"
                            placeholder="Nama Penandatangan" value="<?= set_value('ApprovelName'); ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="NIK" class="col-sm-4 control-label">N I K
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" maxlength="16" name="NIK" id="NIK"
                            placeholder="Nomor Induk Kependudukan" value="<?= set_value('NIK'); ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="Email" class="col-sm-4 control-label">Email
                    </label>
                    <div class="col-sm-8">
                        <input type="Email" class="form-control" name="Email" id="Email" placeholder="Email"
                            value="<?= set_value('Email'); ?>">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="PeopleIsActive" class="col-sm-4 control-label">Status Aktif
                        <i class="required">*</i>
                    </label>
                    <div class="col-sm-8">
                        <input type="checkbox" name="PeopleIsActive" id="PeopleIsActive" value="1" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="PeopleUsername" class="col-sm-4 control-label">Login Pengguna
                        <i class="required">*</i>
                    </label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="PeopleUsername" id="PeopleUsername"
                            placeholder="Login Pengguna" value="<?= set_value('PeopleUsername'); ?>" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="PeoplePassword" class="col-sm-4 control-label">Kata Sandi
                        <i class="required">*</i>
                    </label>
                    <div class="col-sm-8">
                        <input type="password" class="form-control" name="PeoplePassword" id="PeoplePassword"
                            placeholder="Kata Sandi" value="<?= set_value('PeoplePassword'); ?>" required>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Kembali</button>
                <input type="submit" name="submit" name="submit" value="Simpan" class="btn btn-flat btn-danger">
            </div>
            <?=form_close();?>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<!-- Page script -->
<script>
$(document).ready(function() {
    $('#example1').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?= site_url('administrator/anri_master_pengguna/ajax_list_people'); ?>",
            "type": "POST",
            "deferLoading": 10
        },

        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [0, 6], //first column / numbering column
            "orderable": false, //set not orderable
        }, ],
    });

}); /*end doc ready*/
</script>