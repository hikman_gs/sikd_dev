<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<!-- File untuk konfigurasi fine-upload -->
<?php $this->load->view('core_template/fine_upload'); ?>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<style>
.select2-container--default .select2-selection--single {
    border: 0px;
}

.select2-container {
    border: 1px solid #DEDCDC !important;
    border-radius: 5px !important;
    padding: 2px 0px !important;
}
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1><small></small></h1>
    <ol class="breadcrumb">
        <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class="active">Registrasi Nota Dinas Keluar</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Registrasi Nota Dinas </h3>
                            <h5 class="widget-user-desc">&nbsp;</h5>
                            <hr>
                        </div>
                        <?= form_open_multipart(BASE_URL('administrator/anri_reg_nota_dinas/naskah_masuk_add_save'), ['name'    => 'form_inbox', 'class'   => 'form-horizontal', 'id'      => 'form_inbox', 'method'  => 'POST']); ?>
                        <div class="form-group ">
                            <label for="NTglReg" class="col-sm-2 control-label">Tanggal Pencatatan</label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right" disabled name="NTglReg"
                                        id="NTglReg" value="<?= date('d-m-Y') ?>">
                                </div>
                                <small class="info help-block"></small>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jenis Naskah<sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <div class="input-group date col-sm-4">
                                    <select name="JenisId" class="form-control select2" required>
                                        <option value="">-- Pilih --</option>
                                        <?php
                                            $query = $this->db->query("SELECT * FROM `master_jnaskah` WHERE JenisId='XxJyPn38Yh.17'")->result();
                                            foreach ($query as $row) {
                                        ?>
                                        <option value="<?= $row->JenisId; ?>"><?= $row->JenisName; ?></option>
                                        <?php
                                            }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Tanggal Naskah<sup class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <div class="input-group date col-sm-12">
                                    <input type="text" class="form-control pull-right datepicker" required name="Tgl"
                                        id="Tgl" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Nomor" class="col-sm-2 control-label">Nomor Naskah
                                <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Nomor" id="Nomor"
                                    placeholder="Nomor Naskah" value="<?= set_value('Nomor'); ?>" required autofocus>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-2 control-label">Nomor Agenda</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="NAgenda" id="NAgenda"
                                    placeholder="Nomor Agenda" value="<?= set_value('NAgenda'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Hal" class="col-sm-2 control-label">Hal <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="Hal" id="Hal" required placeholder="Hal"
                                    value="<?= set_value('Hal'); ?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                       

                        <div class="form-group" hidden="">
                            <label for="Namapengirim" class="col-sm-2 control-label">Pembuat Naskah <sup
                                    class="text-danger">*</sup></label>
                            <div class="col-sm-8">

                                <input type="text" disabled class="form-control" name="Namapengirim" id="Namapengirim"
                                    class="form-control" value="<?= $this->session->userdata('peoplename'); ?>">

                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="UrgensiId" class="col-sm-2 control-label">Tingkat Urgensi <sup
                                    class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <select name="UrgensiId" class="form-control select2" id="UrgensiId" required>
                                    <option value="">-- Pilih --</option>
                                    <?php
                                        $urg = $this->db->query("SELECT * FROM master_urgensi ORDER BY UrgensiName ASC")->result();
                                        foreach ($urg as $data_urg){
                                    ?>
                                    <option value="<?= $data_urg->UrgensiId; ?>"><?= $data_urg->UrgensiName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="SifatId" class="col-sm-2 control-label">Sifat Naskah <sup
                                    class="text-danger">*</sup></label>
                            <div class="col-sm-2">
                                <select name="SifatId" class="form-control select2" required id="SifatId">
                                    <option value="">-- Pilih --</option>
                                    <?php
                                        $sifat = $this->db->query("SELECT * FROM master_sifat ORDER BY SifatName ASC")->result();
                                        foreach ($sifat as $data_sifat){
                                    ?>
                                    <option value="<?= $data_sifat->SifatId; ?>"><?= $data_sifat->SifatName; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Kepada_Yth" class="col-sm-2 control-label">Kepada Yth </label>
                            <div class="col-sm-8">
                                <select name="Kepada_Yth[]" class="form-control select2" required multiple tabi-ndex="5"
                                    id="Kepada_Yth">
                                    <option value="">-- Pilih User --</option>
                                    <!-- Perbaikan Dispu 2020 -->
                                    <?php
                                        $query = $this->db->query("SELECT * FROM v_login WHERE PeopleId != ".$this->session->userdata('peopleid')." AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4') AND RoleCode = '".$this->session->userdata('rolecode')."' ORDER BY PeopleId ASC")->result();
                                        foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                        }
                                    ?>
                                    <!-- batas akhir perbaikan -->
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="Tembusan" class="col-sm-2 control-label">Tembusan</label>
                            <div class="col-sm-8">
                                <select name="Tembusan[]" class="form-control select2" multiple tabi-ndex="5"
                                    id="Tembusan">
                                     <!-- Perbaikan Dispu 2020 -->
                                    <?php
                                        $query = $this->db->query("SELECT * FROM v_login WHERE PeopleId != ".$this->session->userdata('peopleid')." AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4') AND RoleCode = '".$this->session->userdata('rolecode')."' ORDER BY PeopleId ASC")->result();
                                        foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                        }
                                    ?>
                                    <!-- batas akhir perbaikan -->
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="BerkasId" class="col-sm-2 control-label">Pilih Berkas</label>
                            <div class="col-md-4">
                                <select name="BerkasId" class="form-control select2">
                                    <option value=""> -- Pilih Berkas -- </option>
                                    <?php
                                      $query = $this->db->query("SELECT BerkasId, Klasifikasi, BerkasNumber, BerkasName, YEAR(CreationDate) AS tahun_cipta FROM berkas WHERE RoleId = '".$this->session->userdata('roleid')."' AND BerkasStatus = 'open' ORDER BY Klasifikasi ASC")->result();
                                      foreach ($query as $row) {
                                  ?>
                                    <option value="<?= $row->BerkasId; ?>">
                                        <?= $row->Klasifikasi; ?>/<?= $row->BerkasNumber; ?>/<?= $row->tahun_cipta; ?> -
                                        <?= $row->BerkasName; ?></option>
                                    <?php
                                      }
                                  ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="title" class="col-sm-2 control-label">Unggah Data</label>
                            <div class="col-sm-8">
                                <div id="test_title_galery"></div>
                                <div id="test_title_galery_listed"></div>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
						<div class="form-group ">
                            <label for="title" class="col-sm-2 control-label">Unggah Data dari Menu TTE</label>
                             <div class="col-sm-8">
                                <div class="btnx btn-primary view_data" style="padding:10px;">Browse File</div>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label for="title" class="col-sm-2 control-label">  </label>
                             <div class="col-sm-8">
                                <div id="fileAddons"></div>
                            </div>
                        </div>
                        <br>
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                            <br>
                            <button class="btn btn-danger">Simpan dan Kirim</button>
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->

<div id="getData" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg" role="document">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">&times;</button>
			<h4 class="modal-title">
				<b>Cari Dokumen </b>
			</h4>
		</div>
		 
			<div class="modal-body">
				<div class="content">
 <div class="row">
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Yang perlu dikirim</a></li>
              <li><a href="#tab_2" data-toggle="tab">Tertandatangani</a></li> 
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
	<div class="form-group">
						
							<div class="col-6">
							
							
								<table id="lookup" class="table table-bordered table-striped table-hover dataTable attrTable" cellspacing="0" width="100%">
									<thead>
										<tr> 
											<th style="width:5px;">#</th> 
											<th>Nama Dokumen</th>  
											<th>Tanggal</th>  
											<th>Status</th>  
										</tr>
									</thead>
									<tbody>
									<?php
										
										$query = $this->db->query("SELECT * FROM `m_ttd_terusankirim`  WHERE PeopleIDTujuan = ".$this->session->userdata('peopleid')."   ORDER BY tgl DESC");
										foreach ($query->result() as $row) {
									?>
											<tr> 
												<td>
													<input type="checkbox" class="filename" name="filename[]" value="<?php echo $row->ttd_id; ?>">
													<input type="hidden" class="filename" name="filename" id="namafile_<?php echo $row->ttd_id; ?>" value="<?php echo getfileNameTTD($row->ttd_id); ?>">
													<input type="hidden" class="ukuranfile" name="ukuranfile" id="ukuranfile_<?php echo $row->ttd_id; ?>" value="<?php echo getUkuranFile($row->ttd_id); ?>">
												</td> 
												<td title="<?php echo $row->ttd_id; ?>" class="namafile" data-id="<?php echo getfileNameTTD($row->ttd_id); ?>">
													<?php echo getfileName1TTD($row->ttd_id); ?>
												</td>
												<td>
													<?php echo $row->tgl; ?>
												</td>
												<td>
													<?php echo ttd_label_dokterkirim($row->status); ?>
												</td>  
											</tr>
									<?php
										}
									?></tbody>
								</table>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-6">
								<button type="button" class="btn btn-success" id="ambildata">Ambil File</button>
								<button type="button" class="btn btn-primary cancel">Batal</button>
							</div>
						</div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
               <div class="form-group">
						
							<div class="col-6">
							
							
								<table id="lookup" class="table table-bordered table-striped table-hover dataTable attrTable" cellspacing="0" width="100%">
									<thead>
										<tr> 
											<th style="width:5px;">#</th> 
											<th>Nama Dokumen</th>  
											<th>Tanggal</th>  
											<th>Penandatangan</th>  
										</tr>
									</thead>
									<tbody>
									<?php
										$this->load->helper('ttd');
										$PeopleID = $this->session->userdata('peopleid');
										$this->db->order_by('tanggal','DESC');
										$this->db->where('status', 1);
										$this->db->where('PeopleID', $PeopleID);
										$this->db->from('m_ttd'); 
										$tte = $this->db->get();
										$tte = $tte->result();
										
										foreach ($tte as $row) {
									?>
											<tr> 
												<td>
													<input type="checkbox" class="filename" name="filename[]" value="<?php echo $row->id; ?>">
													<input type="hidden" class="filename" name="filename" id="namafile_<?php echo $row->id; ?>" value="<?php echo getfileNameTTD($row->id); ?>">
													<input type="hidden" class="ukuranfile" name="ukuranfile" id="ukuranfile_<?php echo $row->id; ?>" value="<?php echo getUkuranFile($row->id); ?>">
												</td> 
												<td title="<?php echo $row->id; ?>" class="namafile" data-id="<?php echo getfileNameTTD($row->id); ?>">
													<?php echo getfileName1TTD($row->id); ?>
												</td>
												<td>
													<?php echo $row->tanggal; ?>
												</td>
												<td>
													<?php echo verifikasifile(getfileNameTTD($row->id)); ?>
												</td>  
											</tr>
									<?php
										}
									?></tbody>
								</table>
								
							</div>
						</div>
						<div class="form-group">
							<div class="col-6">
								<button type="button" class="btn btn-success" id="ambildata2">Ambil File</button>
								<button type="button" class="btn btn-primary cancel">Batal</button>
							</div>
						</div>
              </div>
              <!-- /.tab-pane -->
              
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->

        
        <!-- /.col -->
      </div>
      <!-- /.row -->
					</div>
				</div>
			 
		</div>
	</div>

</div>

<!-- Page script -->
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
$("#Kepada_Yth").on("select2:select select2:unselect", function(e) {
    var items = $(this).val();
    var ip = e.params.data.id;
    var tembusan = $("#Tembusan").val();

    $.each(tembusan, function(i, item) {
        $.each(items, function(k, item2) {
            if (item == item2) {
                items.splice(k, 1);
                $("#Kepada_Yth").val(items).change();
                return false;
            }
        })
    })
});

$("#Tembusan").on("select2:select select2:unselect", function(e) {
    var items = $(this).val();
    var ip = e.params.data.id;
    var tujuan = $("#Kepada_Yth").val();

    $.each(tujuan, function(i, item) {
        $.each(items, function(k, item2) {
            if (item == item2) {
                items.splice(k, 1);
                $("#Tembusan").val(items).change();
                return false;
            }
        })
    })
});


$(document).ready(function() {
    $('.select2').select2();

    var params = {};
    params[csrf] = token;
    $('#test_title_galery').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: BASE_URL + '/administrator/anri_reg_nota_dinas/upload_naskah_masuk_file',
            params: params
        },
        deleteFile: {
            enabled: true,
            endpoint: BASE_URL + '/administrator/anri_reg_nota_dinas/delete_naskah_masuk_file',
        },
        thumbnails: {
            placeholders: {
                waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ["*"],
            sizeLimit: 0,

        },
        showMessage: function(msg) {
            toastr['error'](msg);
        },
        callbacks: {
            onComplete: function(id, name, xhr) {
                if (xhr.success) {
                    var uuid = $('#test_title_galery').fineUploader('getUuid', id);
                    $('#test_title_galery_listed').append(
                        '<input type="hidden" class="listed_file_uuid" name="test_title_uuid[' +
                        id + ']" value="' + uuid +
                        '" /><input type="hidden" class="listed_file_name" name="test_title_name[' +
                        id + ']" value="' + xhr.uploadName + '" />');
                } else {
                    toastr['error'](xhr.error);
                }
            },
            onDeleteComplete: function(id, xhr, isError) {
                if (isError == false) {
                    $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid[' +
                        id + ']"]').remove();
                    $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name[' +
                        id + ']"]').remove();
                }
            }
        }
    }); /*end title galery*/
}); /*end doc ready*/
</script>

<script type="text/javascript">

$(document).ready(function(){
 
	$('.view_data').click(function(){
		$('#getData').modal('show');
		
		$('#lookup').DataTable( {
			destroy: true,
			searching: true,
			ordering:false,
			"lengthMenu": [[5,10], [5,10]],
		} );
		
	});  
        // Close modal on button click
        $(".cancel").click(function(){
            $("#getData").modal('hide');
        });	
});  



$(document).ready(function(){
	
	$('#ambildata').click(function(){
		var values = [];
		i = 1;
		$("#fileAddons").empty();
		var html = '';
		$.each($("input[name='filename[]']:checked"), function() {
			var data = $(this).val(); 
			var namafile = $('#namafile_'+data).val(); 
			var ukuranfile = $('#ukuranfile_'+data).val(); 
			 
			
			html += '<input type="hidden" class="form-control" name="filename[]" id="filename_'+i+'" value="'+data+'"><div class="filename">'+namafile+'<br>'+ukuranfile+'</div>';
			i++;
		});  
		$("#fileAddons").append(html);
		$("#getData").modal('hide');
	});  
$('#ambildata2').click(function(){
		var values = [];
		i = 1;
		$("#fileAddons").empty();
		var html = '';
		$.each($("input[name='filename[]']:checked"), function() {
			var data = $(this).val(); 
			var namafile = $('#namafile_'+data).val(); 
			var ukuranfile = $('#ukuranfile_'+data).val(); 
			 
			
			html += '<input type="hidden" class="form-control" name="filename[]" id="filename_'+i+'" value="'+data+'"><div class="filename">'+namafile+'<br>'+ukuranfile+'</div>';
			i++;
		});  
		$("#fileAddons").append(html);
		$("#getData").modal('hide');
	});   	
});   


</script>