<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Verifikasi Dokumen</title>
    <link rel="shortcut icon" href="<?= BASE_ASSET; ?>/favicon.ico">
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>font-awesome/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= BASE_ASSET; ?>/admin-lte/dist/css/AdminLTE.min.css">
</head>

<style type="text/css">
.captcha-box {
    padding: 5px 0;
}

.captcha-box input {
    width: 30%;
    border: 1px solid #E5E2E2;
    padding: 5px;
}

.captcha-box img {
    width: 55%;
    float: left;
}

.required {
    color: #D02727
}
</style>

<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-box-body">
     <?php
       if (!empty($this->session->flashdata('error'))) {
       ?>
       <div class="alert alert-danger alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>Perhatian!</strong> Kode Dokumen yang dicari tidak terdaftar.
       </div>
       <?php
       }
     ?>

     <?=form_open(BASE_URL('verifikasi_dokumen/cek_dong'),['autocomplete'=>'off'])?>
      <div class="form-group text-center" style="font-size: 20px">
         <img src="<?=BASE_URL('uploads/depan.png')?>" height="80px">
        <br>
        <hr>Verifikasi Keaslian Dokumen
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Masukkan Kode Dokumen" name="id_dok" autofocus required maxlength="10">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>

      <div class="row">
          <div class="col-xs-4">
              <input type="submit" class="btn btn-primary btn-block btn-flat" value="Cari">
          </div>
      </div>
      <br>
    <?= form_close(); ?>

    <div class="social-auth-links text-center">
      <hr>Copyright &copy; 2016 - 2019 <br>Arsip Nasional Republik Indonesia.<br>
    </div>
  </div>
</div>
</body>
</html>

<script src="<?= BASE_ASSET; ?>/admin-lte/plugins/jQuery/jquery-2.2.3.min.js"></script>
