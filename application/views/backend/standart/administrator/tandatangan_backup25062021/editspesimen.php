<link href="<?= BASE_ASSET; ?>/fine-upload/fine-uploader-gallery.min.css" rel="stylesheet">
<!-- Fine Uploader jQuery JS file
    ====================================================================== -->
<script src="<?= BASE_ASSET; ?>/fine-upload/jquery.fine-uploader.js"></script>
<!-- File untuk konfigurasi fine-upload -->
<?php $this->load->view('core_template/fine_upload'); ?>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<script type="text/javascript" src="<?= BASE_ASSET; ?>dynatree/jquery-ui.custom.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>

<style>
    .select2-container--default .select2-selection--single{border:0px;}
    .select2-container{
            border: 1px solid #DEDCDC!important;
            border-radius: 5px!important;
            padding: 2px 0px!important;
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) --> 
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            &nbsp;
            <small>   &nbsp;</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">&nbsp;</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php //include('panel.php');?>
			 <div class="col-md-12">
				 <div class="box box-primary">
					 <div class="box-header with-border">
						 <h3 class="box-title">
							Upload Spesimen TTD  
						</h3>
					 </div>
					 <!-- /.box-header -->
					 <div class="box-body">
						<?= form_open_multipart(BASE_URL('administrator/tandatangan/uploadspesimen'), ['name'    => 'ttd', 'class'   => 'form-horizontal', 'id'=> 'ttd', 'method'  => 'POST']); ?>
                        <div class="form-group ">
                            <label for="Hal" class="col-sm-2 control-label">Nama <sup class="text-danger">*</sup></label>
                            <div class="col-sm-8">
                                <input type="hidden" class="form-control" name="mode" id="mode" value="update">
                                <input type="hidden" class="form-control" name="id" id="id" value="<?php echo $id; ?>">
                                <input type="hidden" class="form-control" name="image" id="image" value="<?php echo $image; ?>">
                                <input type="text" class="form-control" name="nama" id="nama" required="" placeholder="Nama" value="<?php echo $nama;?>">
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
						<div class="form-group "> 
                             <label for="Hal" class="col-sm-2 control-label">Image <sup class="text-danger">*</sup></label>
							 <div class="col-sm-8">
                                 <img src="<?php echo BASE_URL.'/FilesUploaded/ttd/spesimen/'.$image;?>" width="300px">
                            </div>
                        </div> 
						<div class="form-group "> 
                             <label for="Hal" class="col-sm-2 control-label">Image <sup class="text-danger">*</sup></label>
							 <div class="col-sm-8">
                                <div id="test_title_galery"></div>
                                <div id="test_title_galery_listed"></div>
                                <small class="info help-block"></small>
                            </div>
                        </div> 
						<div class="form-group "> 
                             <div class="col-sm-12">
                                <div class="message"></div>
                            </div>
                        </div> 
					 </div>
					 <!-- /.box-body -->
					 <div class="box-footer">
						 <div> 
							 <button class="btn btn-primary"> Simpan</button>
						 </div> 
					 </div>
					 <?= form_close(); ?>
					 <!-- /.box-footer -->
				 </div>
				 <!-- /. box -->
			 </div>
			 <!-- /.col -->
  
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
 

 
<script>
    
$(document).ready(function() {
	
    var params = {};
    params[csrf] = token;
    $('#test_title_galery').fineUploader({
        template: 'qq-template-gallery',
        request: {
            endpoint: BASE_URL + '/administrator/tandatangan/upload_ttd_file',
            params: params
        },
        deleteFile: {
            enabled: true,
            endpoint: BASE_URL + '/administrator/tandatangan/delete_ttd_file',
        },
        thumbnails: {
            placeholders: {
                waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
                notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
            }
        },
        validation: {
            allowedExtensions: ['jpg','png','jpeg'],
            sizeLimit: 0,

        },
        showMessage: function(msg) {
            toastr['error'](msg);
        },
        callbacks: {
            onComplete: function(id, name, xhr) {
                if (xhr.success) {
                    var uuid = $('#test_title_galery').fineUploader('getUuid', id);
					var nama = $('#test_title_galery').fineUploader('getName', parseInt(id, 10));
					var ukuran = $('#test_title_galery').fineUploader('getSize', parseInt(id, 10));
                    $('#test_title_galery_listed').append('<input type="hidden" class="listed_file_uuid" name="test_title_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="test_title_name[' + id + ']" value="' + xhr.uploadName + '" /><input type="hidden" class="nama_file" name="nama_file[' + id + ']" value="' + nama + '" /><input type="hidden" class="ukuran" name="ukuran[' + id + ']" value="' + ukuran + '" />');

                } else {
                    toastr['error'](xhr.error);
                }
            },
            onDeleteComplete: function(id, xhr, isError) {
                if (isError == false) {
                    $('#test_title_galery_listed').find('.listed_file_uuid[name="test_title_uuid[' + id + ']"]').remove();
                    $('#test_title_galery_listed').find('.listed_file_name[name="test_title_name[' + id + ']"]').remove();
                }
            }
        }
    }); /*end title galery*/
}); /*end doc ready*/

</script>
<script type="text/javascript">

$(document).ready(function(){
 
	$('.view_data').click(function(){
		$('#getData').modal('show');
		$('#lookup').DataTable( {
			destroy: true,
			searching: true,
			ordering:false,
			"lengthMenu": [[5,10], [5,10]],
		} );
		
	});  
        // Close modal on button click
        $(".cancel").click(function(){
            $("#getData").modal('hide');
        });	
});   


</script>

 <script type="text/javascript">
$(document).ready(function () {
    $('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        swal("Minimal satu harus dipilih.");
        return false;
      }

    });
});

</script>