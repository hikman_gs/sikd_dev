<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Sistem Informasi Kearsipan Dinamis</title>
  
  
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/css/bootstrap.min.css'>


<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
      <link rel="stylesheet" href="<?= BASE_ASSET; ?>css/style.css">

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style>
    #approve .modal-content {
        /* new custom width */
        height: 220px;
    }

    #nomor .modal-content {
        /* new custom width */
        height: 220px;
    }

    #k_teruskan .modal-content {
        /* new custom width */
        width: 520px;
    }

    #k_teruskan_tu .modal-content {
        /* new custom width */
        width: 519px;
    }

    .select2-container--default .select2-selection--single {
        border: 0px;
    }

    .select2-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 2px 0px !important;
    }

    .chosen-container-single,
    .chosen-container-multi {
        width: 100% !important;
    }

    .select3-container--default .select2-selection--single {
        border: 0px;
    }

    .select3-container {
        border: 1px solid #DEDCDC !important;
        border-radius: 5px !important;
        padding: 3px 0px !important;
    }

    .chosen-container-single,
    .chosen-container-multi {
        width: 100% !important;
    }
</style>
</head>

<body>

  <div class="container"> 
  <div class="row">
      <div class="col-md-12" style="padding:10px">
			<a class="btn btn-primary btn-block" href="<?php echo BASE_URL;?>administrator/tandatangan/index/0">Kembali ke Menu Tandatangan</a>
      </div>

  </div>

  <div class="row">
					<div class="col-md-12" id="pdfManager"  style="display:none" >	
						<div class="row" id="selectorContainer">
							<div class="col-fixed-240" style="margin-top: 16px;
    margin-left: 10px;
    margin-bottom: 13px;
    font-weight: bold;">
								Specimen TTD
							</div>
							<div class="col-fixed-240" id="parametriContainer">
							</div>
							<div class="col-fixed-605">
								<div>
  <button id="prev" type="button" class="btn"><i class="fa fa-fw fa-angle-double-left"> </i>Prev</button>
  <button id="next" type="button" class="btn"><i class="fa fa-fw fa-angle-double-right"> </i>Next</button>
  &nbsp; &nbsp;
  <span>Page: <span id="page_num"></span> / <span id="page_count"></span></span>
   <button type="button" class="btn btn-success pull-right" onClick="reqSignature()" style="margin-bottom:5px;"><i class="fa fa-pencil"></i> Simpan</button>
</div>
								<div id="pageContainer" class="pdfViewer singlePageView dropzone nopadding" style="background-color:transparent">
									<canvas id="the-canvas" style="border:1px  solid black"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>

	  
</div>
 <!-- parameters showed on the left sidebar -->
<input id="parameters" type="hidden" value='[{"idParametro":480,"descrizione":"RAPINA","valore":"x","nota":null}]' />

 


<div id="approve_naskah1" class="modal fade" role="dialog">

    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Request Signature </b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/savesignandshare'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
 
						<div class="form-group">
                            <input type="hidden" id="idfile" name="idfile" value="<?php echo $idfile; ?>">
                            <input type="hidden" id="xAxis" name="xAxis">
                            <input type="hidden" id="yAxis" name="yAxis">
                            <input type="hidden" id="pageNum" name="pageNum">
                        </div>
						<div class="form-group">
							<label>Penandatangan</label>
							<select class="form-control select2" multiple="multiple" data-placeholder="Pilih User"
							style="width: 100%;" name="userpenandatangan[]">
                                    <option value="">-- Pilih User --</option>
                                    <!-- Perbaikan Dispu 2020 -->
                                    <?php
                                        $query = $this->db->query("SELECT * FROM v_login WHERE PeopleId != ".$this->session->userdata('peopleid')." AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4') AND GRoleId = '".$this->session->userdata('groleid')."' ORDER BY GRoleId ASC")->result();
                                        foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                        }
                                    ?>
							</select>
						</div>
                        <div class="form-group">
                            <div class="col-12">
                                <label>Subject and Message</label>
                                <input type="text"  class="form-control" name="pesan" id="pesan" placeholder="Pesan ..."  >
								<textarea class="form-control" rows="3" placeholder="Catatan ..."  name="catatan" id="catatan" ></textarea>
                            </div>
                        </div> 
 
						<div class="form-group"> 
                            <div class="col-6">
                                <label>Masukan Passphrase</label>
                                <input type="password" min="1" class="form-control" name="password" id="password" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<div id="approve_naskah12" class="modal fade" role="dialog">

    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Request Signature </b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/signttd'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
                        <div class="form-group">
                            <input type="hidden" id="idfile" name="idfile" value="<?php echo $idfile;?>">
                            <input type="hidden" id="file" name="file" value="<?php echo $filen;?>">
                        </div>
						<div class="form-group">
							<label>Penandatangan</label>
							<select class="form-control select2" multiple="multiple" data-placeholder="Pilih User"
							style="width: 100%;">
                                    <option value="">-- Pilih User --</option>
                                    <!-- Perbaikan Dispu 2020 -->
                                    <?php
                                        $query = $this->db->query("SELECT * FROM v_login WHERE PeopleId != ".$this->session->userdata('peopleid')." AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4') AND GRoleId = '".$this->session->userdata('groleid')."' ORDER BY GRoleId ASC")->result();
                                        foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                        }
                                    ?>
							</select>
						</div>
                        <div class="form-group">
                            <div class="col-12">
                                <label>Subject and Message</label>
                                <input type="hidden"  class="form-control" name="namafile" id="namafile" value="<?php //echo $nama_file;?>">
                                <input type="hidden"  class="form-control" name="xAxis" id="xAxis">
                                <input type="hidden"  class="form-control" name="yAxis" id="yAxis">
								<textarea class="form-control" rows="3" placeholder="Catatan ..."></textarea>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="col-12">
                                <label>Files</label>
                                <!--<label class="pull-right"><a href="#">Add Files</a></label>-->
                            </div>
							<div class="col-12">
                                <?php echo $filen;?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<script src="//mozilla.github.io/pdf.js/build/pdf.js"></script> 
<script src='https://cdnjs.cloudflare.com/ajax/libs/interact.js/1.2.9/interact.min.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.5/js/bootstrap.min.js'></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script>
// If absolute URL from the remote server is provided, configure the CORS
// header on that server.
var url = '<?php echo $file; ?>';
 /*
 *  costanti per i placaholder 
 */
var maxPDFx = 595;
var maxPDFy = 842;
var offsetY = 7;
// Loaded via <script> tag, create shortcut to access PDF.js exports.
var pdfjsLib = window['pdfjs-dist/build/pdf'];

// The workerSrc property shall be specified.
pdfjsLib.GlobalWorkerOptions.workerSrc = '//mozilla.github.io/pdf.js/build/pdf.worker.js';

var pdfDoc = null,
    pageNum = 1,
    pageRendering = false,
    pageNumPending = null,
    scale = 2,
    canvas = document.getElementById('the-canvas'),
    ctx = canvas.getContext('2d');

/**
 * Get page info from document, resize canvas accordingly, and render page.
 * @param num Page number.
 */
function renderPage(num) {
  pageRendering = true;
  // Using promise to fetch the page
  pdfDoc.getPage(num).then(function(page) {
    var viewport = page.getViewport({scale: scale});
    canvas.height = viewport.height;
    canvas.width = viewport.width;

    // Render PDF page into canvas context
    var renderContext = {
      canvasContext: ctx,
      viewport: viewport
    };
    var renderTask = page.render(renderContext);

    // Wait for rendering to finish
    renderTask.promise.then(function() {
      pageRendering = false;
      if (pageNumPending !== null) {
        // New page rendering is pending
        renderPage(pageNumPending);
        pageNumPending = null;
      }
    });
  });

  // Update page counters
  document.getElementById('page_num').textContent = num;
  	   $('#pdfManager').show();
	   var parametri = JSON.parse($('#parameters').val());
		 $('#parametriContainer').empty();
	   renderizzaPlaceholder(0, parametri);	
}

/**
 * If another page rendering in progress, waits until the rendering is
 * finised. Otherwise, executes rendering immediately.
 */
function queueRenderPage(num) {
  if (pageRendering) {
    pageNumPending = num;
  } else {
    renderPage(num);
  }
}

/**
 * Displays previous page.
 */
function onPrevPage() {
  if (pageNum <= 1) {
    return;
  }
  pageNum--;
  queueRenderPage(pageNum);
}
document.getElementById('prev').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
  if (pageNum >= pdfDoc.numPages) {
    return;
  }
  pageNum++;
  queueRenderPage(pageNum);
}
document.getElementById('next').addEventListener('click', onNextPage);

/**
 * Asynchronously downloads PDF.
 */
pdfjsLib.getDocument(url).promise.then(function(pdfDoc_) {
  pdfDoc = pdfDoc_;
  document.getElementById('page_count').textContent = pdfDoc.numPages;

  // Initial/first page rendering
  renderPage(pageNum);
});

  
  /* The dragging code for '.draggable' from the demo above
   * applies to this demo as well so it doesn't have to be repeated. */

  // enable draggables to be dropped into this
  interact('.dropzone').dropzone({
    // only accept elements matching this CSS selector
    accept: '.drag-drop',
    // Require a 100% element overlap for a drop to be possible
    overlap: 1,

    // listen for drop related events:

    ondropactivate: function (event) {
      // add active dropzone feedback
      event.target.classList.add('drop-active');
    },
    ondragenter: function (event) {
      var draggableElement = event.relatedTarget,
          dropzoneElement = event.target;

      // feedback the possibility of a drop
      dropzoneElement.classList.add('drop-target');
      draggableElement.classList.add('can-drop');
      draggableElement.classList.remove('dropped-out');
      //draggableElement.textContent = 'Dragged in';
    },
    ondragleave: function (event) {
      // remove the drop feedback style
      event.target.classList.remove('drop-target');
      event.relatedTarget.classList.remove('can-drop');
      event.relatedTarget.classList.add('dropped-out');
      //event.relatedTarget.textContent = 'Dragged out';
    },
    ondrop: function (event) {
      //event.relatedTarget.textContent = 'Dropped';
    },
    ondropdeactivate: function (event) {
      // remove active dropzone feedback
      event.target.classList.remove('drop-active');
      event.target.classList.remove('drop-target');
    }
  });

  interact('.drag-drop')
    .draggable({
      inertia: true,
      restrict: {
        restriction: "#selectorContainer",
        endOnly: true,
        elementRect: { top: 0, left: 0, bottom: 1, right: 1 }
      },
      autoScroll: true,
      // dragMoveListener from the dragging demo above
      onmove: dragMoveListener,
    });
  
  
  function dragMoveListener (event) {
	    var target = event.target,
	        // keep the dragged position in the data-x/data-y attributes
	        x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx,
	        y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy;

	    // translate the element
	    target.style.webkitTransform =
	    target.style.transform ='translate(' + x + 'px, ' + y + 'px)';

	    // update the posiion attributes
	    target.setAttribute('data-x', x);
	    target.setAttribute('data-y', y);
		
				console.log('data-x '+x);
				console.log('data-y '+y);
	  }

	  // this is used later in the resizing demo
	  window.dragMoveListener = dragMoveListener;

// $(document).bind('pagerendered', function (e) {
//	   $('#pdfManager').show();
//	   var parametri = JSON.parse($('#parameters').val());
//		 $('#parametriContainer').empty();
//	   renderizzaPlaceholder(0, parametri);	
//	});
  
  function renderizzaPlaceholder(currentPage, parametri){
		  var maxHTMLx = $('#the-canvas').width();
			var maxHTMLy = $('#the-canvas').height();
		
			var paramContainerWidth = $('#parametriContainer').width();
			var yCounterOfGenerated = 0;
			var numOfMaxItem = 25;
			var notValidHeight = 30;
			var y = 0;
			var x = 6;
			var page=0;
			

			var totalPages=Math.ceil(parametri.length/numOfMaxItem);
			
			for (i = 0; i < parametri.length; i++) {
				var param = parametri[i];
				var page = Math.floor(i/numOfMaxItem);
				var display= currentPage == page ? "block" : "none";
				
				if(i > 0 && i%numOfMaxItem == 0){
					yCounterOfGenerated = 0;
				}

				var classStyle = "";
				var valore = param.valore;
				/*il placeholder non è valido: lo incolonna a sinistra*/
		
				if(i > 0 && i%numOfMaxItem == 0){
					yCounterOfGenerated = 0;
				}

				var classStyle = "";
				var valore = param.valore;
				/*il placeholder non è valido: lo incolonna a sinistra*/
				y = yCounterOfGenerated;
				yCounterOfGenerated += notValidHeight;
				classStyle = "drag-drop dropped-out";

				
				$("#parametriContainer").append('<div class="'+classStyle+'" data-id="-1" data-page="'+page+'" data-toggle="'+valore+'" data-valore="'+valore+'" data-x="'+x+'" data-y="'+y+'" style="transform: translate('+x+'px, '+y+'px); display:'+display+'"><img src="http://localhost/sikd20032021/web/FilesUploaded/ttd/ttd.jpg" width="200" height="50"></div>');
			}
			
			y = notValidHeight * (numOfMaxItem+1);
			var prevStyle = "";
			var nextStyle = "";
			var prevDisabled = false;
			var nextDisabled = false;
			if(currentPage == 0){
				prevStyle = "disabled";
				prevDisabled = true;
			}
			
			if(currentPage >= totalPages-1 || totalPages == 1){
				nextDisabled=true;
				nextStyle="disabled";
			} 
			
			//Aggiunge la paginazione
			$("#parametriContainer").append('<ul id="pager" class="pager" style="transform: translate('+x+'px, '+y+'px); width:200px;"><li onclick="changePage('+prevDisabled+','+currentPage+',-1)" class="page-item '+prevStyle+'"><span>«</span></li><li onclick="changePage('+nextDisabled+','+currentPage+',1)" class="page-item '+nextStyle+'" style="margin-left:10px;"><span>&raquo;</span></li></ul>');
			
	 }
  
  	function renderizzaInPagina(parametri){
		var maxHTMLx = $('#the-canvas').width();
		var maxHTMLy = $('#the-canvas').height();
	
		var paramContainerWidth = $('#parametriContainer').width();
		var yCounterOfGenerated = 0;
		var numOfMaxItem = 26;
		var notValidHeight = 30;
		var y = 0;
		var x = 6;
  		for (i = 0; i < parametri.length; i++) {
			var param = parametri[i];
			
			var classStyle = "drag-drop can-drop";
			var valore = param.valore;
			/*il placeholder non è valido: lo incolonna a sinistra*/
			
			var pdfY = maxPDFy - param.posizioneY - offsetY;
			y = (pdfY * maxHTMLy) / maxPDFy;
			x = ((param.posizioneX * maxHTMLx) / maxPDFx) + paramContainerWidth;
			
			$("#parametriContainer").append('<div class="'+classStyle+'" data-id="'+param.idParametroModulo+'" data-toggle="'+valore+'" data-valore="'+valore+'" data-x="'+x+'" data-y="'+y+'" style="transform: translate('+x+'px, '+y+'px);">  <span class="circle"></span><span class="descrizione">'+param.descrizione+' </span></div>');
		}
  	}
	 
	 
	 function changePage(disabled, currentPage, delta){
		 if(disabled){
			return;	 
		 }

		 /*recupera solo i parametri non posizionati in pagina*/
		 var parametri = [];
		 $(".drag-drop.dropped-out").each(function() {
			var valore = $(this).data("valore");
			var descrizione = $(this).find(".descrizione").text();
			parametri.push({valore:valore, descrizione:descrizione, posizioneX:-1000, posizioneY:-1000});
			$(this).remove();
		 });
		 
		 //svuota il contentitore
		 $('#pager').remove();
		 currentPage += delta;
		 renderizzaPlaceholder(currentPage, parametri);
		 //aggiorna lo stato dei pulsanti
		 //aggiorna gli elementi visualizzati
	 }

  
  function showCoordinates(){
      var validi = [];
  	  var nonValidi = [];
  	  
  	  var maxHTMLx = $('#the-canvas').width();
  	  var maxHTMLy = $('#the-canvas').height();
      var paramContainerWidth = $('#parametriContainer').width();
  	  
  	  //recupera tutti i placholder validi
  	  $('.drag-drop.can-drop').each(function( index ) {
  		  var x = parseFloat($(this).data("x"));
  		  var y = parseFloat($(this).data("y"));
  		  var valore = $(this).data("valore");
  		  var descrizione = $(this).find(".descrizione").text();
  		    
  		  var pdfY = y * maxPDFy / maxHTMLy;
  		  var posizioneY = maxPDFy - offsetY - pdfY;	  
  		  var posizioneX =  (x * maxPDFx / maxHTMLx)  - paramContainerWidth;
  		  
  		  var val = {"descrizione": descrizione, "posizioneX":posizioneX,   "posizioneY":posizioneY, "valore":valore};
  		  validi.push(val);
      
  	  });
    
      if(validi.length == 0){
         alert('No placeholder dragged into document');
      }
     else{
      alert(JSON.stringify(validi));
     }
  }
</script>
 

<script>
    function reqSignature() {
		var validi = [];
		var nonValidi = [];
		var maxHTMLx = $('#the-canvas').width();
		var maxHTMLy = $('#the-canvas').height();
		var paramContainerWidth = $('#parametriContainer').width();
  	  
  	  $('.drag-drop.can-drop').each(function( index ) {
  		  var x = parseFloat($(this).data("x"));
  		  var y = parseFloat($(this).data("y"));
  		  var valore = $(this).data("valore");
  		  var descrizione = $(this).find(".descrizione").text();
  		    
  		  var pdfY = y * maxPDFy / maxHTMLy;
  		  var posizioneY = maxPDFy - offsetY - pdfY;	  
  		  var posizioneX =  (x * maxPDFx / maxHTMLx)  - paramContainerWidth;
  		  
  		  var val = {"descrizione": descrizione, "posizioneX":posizioneX,   "posizioneY":posizioneY, "valore":valore};
  		  validi.push(val);
		  	$('#xAxis').val(posizioneX);
			$('#yAxis').val(posizioneY);
			$('#pageNum').val(pageNum);
			$("#approve_naskah1").modal('show');
  	  });
  	    if(validi.length == 0){
			alert('Tidak ada spesimen yang masukan');
		}  else {

			//alert(JSON.stringify(validi));
		}
		
        
    }
    function ko_verifikasi1(NId, GIR_Id) {
        $("#NId_Temp").val(NId);
        $("#GIR_Id").val(GIR_Id);
        $("#approve_naskah1").modal('show');
    }
	
</script>
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>
<script>

$(document).ready(function() {
    $('.select2').select2();
 
});
</script>
</body>
</html>
