<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            Dokumen Saya
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dokumen</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php include('panel.php');?>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?=$judul_menu;?></h3>

                        <div class="box-tools pull-right">
                            <div class="has-feedback">
                                <input type="text" class="form-control input-sm" placeholder="Cari Dokumen" id="myInputTextField">
                                <span class="glyphicon glyphicon-search form-control-feedback"></span>
                            </div>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body padding">
                        <form action="<?= base_url('administrator/tandatangan/signttd_multi/norespon'); ?>" class="form-horizontal xwan" id="formId" method="POST">
						<div class="mailbox-controls">
                            <!-- Check all button --> 

                            <div class="btn-group">
                                <button type="submit" class="btn btn-info btn-md" id="submit"><i class="fa  fa-check-circle"></i> Tandatangan Multi File</button> 
                            </div>
 
                            <!-- /.pull-right -->
                        </div>
                        <div class="table-responsive mailbox-messages">
                            <table class="table table-hover table-striped" id="lookup1">
                                	<thead>
										<tr> 
											<th><button type="button" class="btn btn-default btn-sm checkbox-toggle"><i class="fa fa-square-o">
											</i>                           </button> 
</th>  
											<th>Nama Dokumen</th>  
											<th>Status</th> 
                                            <th>Penandatangan</th>
											<th>Tanggal</th>  
											<th>Aksi</th>  
										</tr>
									</thead>
									<tbody>
									<input type="hidden" class="form-control" name="respon" id="respon" value="norespon">
                                    <?php foreach ($tte as $ttd) { ?>
									<tr>
                                        <td>
                                            <input type="checkbox" class="filename" name="filename[]" value="<?php echo $ttd->file; ?>" >
                                            <input type="hidden" class="filename" name="filenamex[]" value="<?php echo $ttd->file; ?>" >
                                        </td>
                                        <!--<td class="mailbox-star"><a href="#"><i class="fa fa-star text-yellow"></i></a></td>-->
                                        <td class="mailbox-name">
											<!--<a href="<?= BASE_URL('administrator/tandatangan/detail/'.$ttd->id) ?>" title="<?php echo $ttd->nama_file; ?>"></a>-->
											<?php echo word_limiter($ttd->nama_file, 3); ?>
										</td> 
                                        <td class="mailbox-attachment"><?php echo ttd_label($ttd->status); ?></td>
                                        <td class="mailbox-attachment"><?php echo verifikasifile($ttd->file); ?></td>
                                        <td class="mailbox-date">
											<?php echo $ttd->tanggal; ?>
										</td>
                                        <td class="mailbox-date">
											<a href="<?= BASE_URL('administrator/tandatangan/detailselfsign/'.$ttd->id.'/norespon') ?>" class="btn btn-warning btn-sm" title="Tandatangani Drag and Drop" class="btn btn-warning btn-sm"><i class="fa fa-pencil "></i></a>
											
											<button type="button" class="btn bg-maroon btn-sm" title="Tandatangani Invisible" data-id="<?=$ttd->file ?>" onClick='submitDetailsForm(<?=$ttd->id ?>)' ><i class="fa fa-pencil "></i></button>
                                            <?php if(cekttdkirim($ttd->id)) {  ?>
											<a href="#" onClick="reqSignature(<?php echo $ttd->id; ?>)" class="btn btn-success btn-sm"  title="Teruskan Untuk Ditandatangani" ><i class='fa fa-arrow-right'></i> </a>
											<?php } else {  ?> 
                                                <button type="button" class="btn btn-success btn-sm" title="Teruskan Untuk Ditandatangani"  disabled ><i class='fa fa-arrow-right'></i> </button>
                                            <?php } ?> 
                                            <?php if($menu_index!='index'){?>
											<a href="#" onClick="teruskandankirim(<?php echo $ttd->id; ?>)" class="btn btn-info btn-sm" title="Teruskan untuk dikirim" class="btn btn-warning btn-sm"><i class="fa fa-arrow-right" data-id="<?php echo $ttd->id; ?>"></i></a>
                                            <?php }?>
											<!-- --> 
                                           
											
											<a href="<?= BASE_URL('administrator/tandatangan/dokumendetail/'.$ttd->id) ?>" class="btn btn-info btn-sm"  title="Detail Dokumen" ><i class='fa fa-info'></i> </a>
											
											<a href="<?= BASE_URL('administrator/tandatangan/download/'.$ttd->id) ?>" class="btn btn-primary btn-sm" title="Download File" >
												<i class='fa fa-download'></i>
											</a>
											<a class="btn btn-danger btn-sm btn-hapus" data-id="<?=$ttd->id ?>" title="Hapus Dokumen" >
												<i class='fa fa-trash'></i>
											</a>
											 <!---->
										</td>
                                    </tr>
									<?php } ?>
                                </tbody>
                            </table>
                            <!-- /.table -->
                        </div>
                        <!-- /.mail-box-messages -->
						</form>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer no-padding">
                        <div class="mailbox-controls" style="padding:20px">
                             
                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->


<div id="reqSignature" class="modal fade" role="dialog">

    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Teruskan Untuk Ditandatangani </b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/savesignandshare'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
 
						<div class="form-group">
							<label>Penandatangan</label>
							<select class="form-control select2" multiple="multiple" data-placeholder="Pilih User"
							style="width: 100%;" name="userpenandatangan[]">
                                    <option value="">-- Pilih User --</option>
                                    <!-- Perbaikan Dispu 2020 -->
                                    <?php
                                        $query = $this->db->query("SELECT * FROM v_login WHERE PeopleId != ".$this->session->userdata('peopleid')." AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4') AND GRoleId = '".$this->session->userdata('groleid')."' ORDER BY GRoleId ASC")->result();
                                        foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                        }
                                    ?>
							</select>
						</div>
                        <div class="form-group">
                            <div class="col-12">
                                <label>Catatan</label>
                                <input type="hidden"  class="form-control" name="idfile" id="idfile" > 
								<textarea class="form-control" rows="3" placeholder="Catatan ..."  name="catatan" id="catatan" ></textarea>
                            </div>
                        </div> 
 
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>

<div id="teruskandankirim" class="modal fade" role="dialog">

    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Teruskan Untuk Dikirim </b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/teruskandankirim'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
 
						<div class="form-group">
							<label>Teruskan ke</label>
							<select class="form-control select2" multiple="multiple" data-placeholder="Pilih User"
							style="width: 100%;" name="userpenandatangan[]">
                                    <option value="">-- Pilih User --</option>
                                    <!-- Perbaikan Dispu 2020 -->
                                    <?php
                                        $query = $this->db->query("SELECT * FROM v_login WHERE PeopleId != ".$this->session->userdata('peopleid')." AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7','8','6') AND GRoleId = '".$this->session->userdata('groleid')."' ORDER BY GRoleId ASC")->result();
										
                                        foreach ($query as $dat_people) {
                                    ?>
                                        <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                                    <?php
                                        }
                                    ?>
							</select>
						</div>
                        <div class="form-group">
                            <div class="col-12">
                                <label>Catatan</label>
                                <input type="hidden" class="form-control" name="fileID" id="fileID" > 
								<textarea class="form-control" rows="3" placeholder="Catatan ..."  name="catatan" id="catatan" ></textarea>
                            </div>
                        </div> 
 
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<div id="signatureInvisible" class="modal fade" role="dialog">

    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><b>Tandatangan Invisible</b></h4>
            </div>
            <form action="<?= base_url('administrator/tandatangan/signttd_/norespon'); ?>" class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
 
						
                        <div class="form-group">
                            <div class="col-12"> 
                                <input type="hidden"  class="form-control" name="filename" id="filename" > 
                            </div>
							<div class="col-12">
                                <label>Masukan Passphrase</label>
                                <input type="password" min="1" class="form-control" name="password" id="password" required>
                            </div>
                        </div> 
 
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
  <!-- iCheck -->
<script src="https://adminlte.io/themes/AdminLTE/plugins/iCheck/icheck.min.js"></script>
<!-- Page Script -->
<script>
$(function() {
    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function() {
        var clicks = $(this).data('clicks');
        if (clicks) {
            //Uncheck all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
            $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
        } else {
            //Check all checkboxes
            $(".mailbox-messages input[type='checkbox']").iCheck("check");
            $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
        }
        $(this).data("clicks", !clicks);
    });

    //Handle starring for glyphicon and font awesome
    $(".mailbox-star").click(function(e) {
        e.preventDefault();
        //detect type
        var $this = $(this).find("a > i");
        var glyph = $this.hasClass("glyphicon");
        var fa = $this.hasClass("fa");

        //Switch states
        if (glyph) {
            $this.toggleClass("glyphicon-star");
            $this.toggleClass("glyphicon-star-empty");
        }

        if (fa) {
            $this.toggleClass("fa-star");
            $this.toggleClass("fa-star-o");
        }
    });

});

</script>
<script type="text/javascript">

$(document).ready(function(){
 

		// Display the Bootstrap modal
		oTable =  $('#lookup1').DataTable(
			{
				dom: 'Bfrtip',
				"ordering":false,
				"lengthMenu": [[5], [5]],
			}
		);
		 $('#myInputTextField').keyup(function(){
			  oTable.search($(this).val()).draw() ;
		})
});   

$(document).ready(function () {
    $('#submit').click(function() {
      checked = $("input[type=checkbox]:checked").length;

      if(!checked) {
        swal("Minimal satu harus dipilih.");
        return false;
      }

    });    
	
	$('.submit').click(function() {
		//checked = $("input[type=checkbox]:checked").length;
		var id = $(this).data('id');
		console.log(id);
		
    });

	
});
	 function submitDetailsForm(idx) {
		//var idx = $(this).data('id');
		$('#filename').val(idx);
        $("#signatureInvisible").modal('show');
    } 
	
	$(document).ready(function(){
		$('.btn-hapus').click(function(){
			var id = $(this).data('id');
			swal({
				title: "Apakah Data Akan dihapus ??",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Ya",
				cancelButtonText: "Tidak",
				closeOnConfirm: true,
				closeOnCancel: true
			},
			function(isConfirm) {
				if (isConfirm) { 
					console.log(id);
						$.ajax({
							type: "POST",
							url: BASE_URL + "administrator/tandatangan/hapusdokumen",
							dataType: "JSON",
							data: {
								id: id,
							},
							success: function(response, status, xhr) {
								console.log(response);
								toastr.success('Data Berhasil dihapus!!');
								setTimeout(function() {
								   location.reload();
								}, 500);
							}
						});
					return false;
				}
			});
		});
	});
</script>
<style>
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td { padding:12px !important}
.dataTables_filter, .dataTables_info { display: none; }

</style>


<script>
    function reqSignature(idfile) {
		$('#idfile').val(idfile);
        $("#reqSignature").modal('show');
    }
	
</script>
<script>
	function teruskandankirim(idfile) {
		$('#fileID').val(idfile); 
		console.log(idfile); 
		$("#teruskandankirim").modal('show');
	}
	

</script>

<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>
<script>

$(document).ready(function() {
    $('.select2').select2();
 
});
</script>
