<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
 
<!-- Content Wrapper. Contains page content -->
<div style="padding:10px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1 style="padding-left: 20px;">
            Dokumen Detail
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dokumen</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row"> 
            <?php include('panel.php');?>
            <!-- /.col -->
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Posisi & Passphrase</h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
 
            <form action=" " class="form-horizontal" method="POST">
                <div class="modal-body">
                    <div class="content">
						<div class="form-group">
							
							<div class="radio">
								<label style="font-weight:bold;padding-left: 0px;">Posisi Tandatangan</label>
								<br>
							</div>
							
								<div class="radio">
									<label>
										<input type="radio" name="posisiTtd" id="optionsRadios1" value="invisible" checked> Invisible
									</label>
								</div>
								
						</div>							
						
						<div class="form-group"> 
                            <?php if(is_array($files)) { ?>
                            <?php foreach ($files as $key => $val) { ?>
								<input type="hidden" id="file" name="file[]" value="<?php echo $val;?>">
							<?php } ?> 
							<?php } else {  ?> 
							<input type="text" id="file" name="file[]" value="<?php echo $files;?>">
							<?php } ?> 
                            <div class="col-12">
                                <label>Masukan Passphrase</label>
                                <input type="password" min="1" class="form-control" name="password" id="password" required>
                            </div>
                        </div> 

                        
                        <div class="form-group">
                            <div class="col-6">
                                <button type="submit" class="btn btn-success">Submit </button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
                    </div>
                    <!-- /.box-body -->
 
                    <!-- /.box-footer -->
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
