<script src="<?= BASE_ASSET; ?>jquery-ui/jquery-ui.js"></script>
<script src="<?= BASE_ASSET; ?>/js/jquery.hotkeys.js"></script>
<script type="text/javascript">
    function domo(){
     
       // Binding keys
       $('*').bind('keydown', 'Ctrl+s', function assets() {
          $('#btn_save').trigger('click');
           return false;
       });
    
       $('*').bind('keydown', 'Ctrl+x', function assets() {
          $('#btn_cancel').trigger('click');
           return false;
       });
    
      $('*').bind('keydown', 'Ctrl+d', function assets() {
          $('.btn_save_back').trigger('click');
           return false;
       });
        
    }
    
    jQuery(document).ready(domo);
</script>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Table Modul        <small><?= cclang('new', ['Table Modul']); ?> </small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Beranda</a></li>
        <li class=""><a  href="<?= site_url('administrator/table_modul'); ?>">Table Modul</a></li>
        <li class="active"><?= cclang('new'); ?></li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row" >
        <div class="col-md-12">
            <div class="box box-warning">
                <div class="box-body ">
                    <!-- Widget: user widget style 1 -->
                    <div class="box box-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header ">
                            <div class="widget-user-image">
                                <img class="img-circle" src="<?= BASE_ASSET; ?>/img/add2.png" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">Table Modul</h3>
                            <h5 class="widget-user-desc"><?= cclang('new', ['Table Modul']); ?></h5>
                            <hr>
                        </div>
                        <?= form_open(BASE_URL('administrator/anri_dashboard/table_modul_add_save'), [
                            'name'    => 'form_table_modul', 
                            'class'   => 'form-horizontal', 
                            'id'      => 'form_table_modul', 
                            'enctype' => 'multipart/form-data', 
                            'method'  => 'POST'
                            ]); ?>
                        <script>
                        	$(function() {
							  $('#level_modul').change(function(){
							    $('.level_modul').hide();
							    $('#' + $(this).val()).show();
							  });
							});
                        </script>

                        <div class="form-group ">
                            <label for="nama_table" class="col-sm-2 control-label">Level Menu
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control" id="level_modul" name="level_modul">
                                	<option value="">--Pilih--</option>
                                	<option value="1">Menu Utama</option>
                                	<option value="2">Sub Menu</option>
                                </select>
                            </div>
                        </div>

                        <div id="2" class="form-group level_modul">
                            <label for="nama_table" class="col-sm-2 control-label">Menu Utama
                            </label>
                            <div class="col-sm-8">
                                <select class="form-control" id="id_child" name="id_child">
                                	<option value="0">--Pilih--</option>
                                	<?php $level_modul = $this->db->order_by('nama_menu','asc')->get_where('table_modul',array('level_modul'=>'1'))->result();
                                	foreach ($level_modul as $lm) {
                                	?>
                                	<option value="<?=$lm->id_tm;?>"><?=$lm->nama_menu;?></option>
                                	<?php
                                	}
                                	?>
                                </select>
                            </div>
                        </div>
                         
                        <div class="form-group ">
                            <label for="nama_menu" class="col-sm-2 control-label">Menu Name 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_menu" id="nama_menu" placeholder="Nama Menu" value="<?= set_value('nama_menu'); ?>" required>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="nama_table" class="col-sm-2 control-label">Id Navigation 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_table" id="nama_table" placeholder="Id Navigation" value="<?= set_value('nama_table'); ?>" required>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>

                        <div class="form-group ">
                            <label for="nama_controller" class="col-sm-2 control-label">Nama Controller 
                            </label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="nama_controller" id="nama_controller" placeholder="Nama Controller" value="<?= set_value('nama_controller'); ?>" required>
                                <small class="info help-block">
                                </small>
                            </div>
                        </div>
                                                
                        <div class="message"></div>
                        <div class="row-fluid col-md-7">
                           <input type="submit" name="submit" value="<?= cclang('save_button'); ?>" class="btn btn-flat btn-primary">
                        </div>
                        <?= form_close(); ?>
                    </div>
                </div>
                <!--/box body -->
            </div>
            <!--/box -->
        </div>
    </div>
</section>
<!-- /.content -->
<!-- Page script -->
<script>
    $(document).ready(function(){
                   
      $('#btn_cancel').click(function(){
        swal({
            title: "<?= cclang('are_you_sure'); ?>",
            text: "<?= cclang('data_to_be_deleted_can_not_be_restored'); ?>",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes!",
            cancelButtonText: "No!",
            closeOnConfirm: true,
            closeOnCancel: true
          },
          function(isConfirm){
            if (isConfirm) {
              window.location.href = BASE_URL + 'administrator/table_modul';
            }
          });
    
        return false;
      }); /*end btn cancel*/
    
      $('.btn_save').click(function(){
        $('.message').fadeOut();
            
        var form_table_modul = $('#form_table_modul');
        var data_post = form_table_modul.serializeArray();
        var save_type = $(this).attr('data-stype');

        data_post.push({name: 'save_type', value: save_type});
    
        $('.loading').show();
    
        $.ajax({
          url: BASE_URL + '/administrator/table_modul/add_save',
          type: 'POST',
          dataType: 'json',
          data: data_post,
        })
        .done(function(res) {
          if(res.success) {
            
            if (save_type == 'back') {
              window.location.href = res.redirect;
              return;
            }
    
            $('.message').printMessage({message : res.message});
            $('.message').fadeIn();
            resetForm();
            $('.chosen option').prop('selected', false).trigger('chosen:updated');
                
          } else {
            $('.message').printMessage({message : res.message, type : 'warning'});
          }
    
        })
        .fail(function() {
          $('.message').printMessage({message : 'Error save data', type : 'warning'});
        })
        .always(function() {
          $('.loading').hide();
          $('html, body').animate({ scrollTop: $(document).height() }, 2000);
        });
    
        return false;
      }); /*end btn save*/
      
       
 
       
    
    
    }); /*end doc ready*/
</script>