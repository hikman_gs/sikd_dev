<ul class="sidebar-menu  sidebar-admin tree" data-widget="tree">
  <?php if(($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4 ) || ($this->session->userdata('groupid')==7)) { ?>

    <li class="header treeview"><small></small></li>
    <!-- Menu Registrasi -->
    <li id="konsep_naskah">
        <a href="<?= BASE_URL('administrator/konsep_naskah_menu') ?>">
            <i class="fa fa-pencil-square-o"></i>
            <span>KONSEP NASKAH</span>
        </a>
    </li>

<?php } ?>
    <li id="knsp1" hidden="">

        <!-- Menu Registrasi -->
    <li id="registrasi_naskah_m">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-edit default"></i>
            <span>PENCATATAN NASKAH</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree Menu Registrasi -->
        <ul class="treeview-menu">
    <?php if(($this->session->userdata('groupid')==6) ) { ?>
            <li id="naskah_masuk">
                <a href="<?= BASE_URL('administrator/anri_reg_naskah_masuk') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Registrasi Naskah Masuk</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
    <?php } ?>
            <li id="reg_naskah_masuk_setda">
                <a href="<?= BASE_URL('administrator/anri_reg_naskah_masuk_setda') ?>"><i class="fa fa-circle-o default"></i> <span>Penerima Naskah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
 <?php if(($this->session->userdata('groupid')==6) ) { ?>

            <li id="reg_naskah_keluar_manual">
                <a href="<?= BASE_URL('administrator/anri_reg_naskah_keluar') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Registrasi Naskah Keluar</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
   <?php } ?>
    <?php if(($this->session->userdata('groupid')==8) || ($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4)) { ?>
            <!-- Sub Menu Nota Dinas Manual !-->
            <li id="reg_nota_dinas_manual">
                <a href="<?= BASE_URL('administrator/anri_reg_nota_dinas') ?>">
                    <i class="fa fa-circle-o default"></i> <span>Registrasi Nota Dinas</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
    <?php } ?>

            <!-- Akhir Sub menu Nota Dinas !-->

            <!-- Sub Menu Registrasi Naskah Tanpa TL -->
            <li id="naskah_keluar">
                <a href="<?= BASE_URL('administrator/anri_reg_naskah_ttl') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Naskah Tanpa Tindaklanjut</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Registrasi Naskah Tanpa TL -->
        </ul>
        <!-- Akhir Tree Menu Registrasi -->
    </li>
    <!-- Akhir Menu Registrasi Naskah -->



    <!-- Menu Kotak Masuk -->
    <li id="naskah_masuk_nm">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-envelope default"></i>
            <span>KOTAK MASUK</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Kotak Masuk -->
        <ul class="treeview-menu">
            <!-- Menu Logout -->
            <li id="km_pengendali_naskah_setda">
                <a href="<?= BASE_URL('administrator/anri_log_naskah_masuk_pengendali') ?>">
                    <i class="fa fa-sign-out default"></i>
                    <span>Pengendali Naskah</span>
                </a>
            </li>
            <!-- Akhir Menu Logout -->

            <!-- Menu Logout -->
            <li id="km_pengarah_naskah_setda">
                <a href="<?= BASE_URL('administrator/anri_log_naskah_masuk_pengarah') ?>">
                    <i class="fa fa-sign-out default"></i>
                    <span>Pengarah Naskah</span>
                </a>
            </li>
            <!-- Akhir Menu Logout -->
            <!-- Sub Menu Naskah Masuk -->
            <li id="naskah_masuk_nm2">
                <a href="<?= BASE_URL('administrator/anri_naskah_masuk_nm2') ?>">
                    <i class="fa fa-envelope-o default"></i>

                    <span>Semua Naskah</span><i class="fa fa-angle-left pull-right"></i>

                </a>

                <!-- Tree View Sub Menu Naskah Masuk -->
                <ul class="treeview-menu">
                    <!-- Sub Sub Menu Semua Naskah -->
                    <li id="list_naskah_masuk">
                        <a href="<?= BASE_URL('administrator/anri_list_naskah_masuk') ?>"><i class="fa fa-circle-o default"></i> <span>Semua Naskah</span>
                        </a>
                    </li>
                    <!-- Akhir Sub Sub Menu Semua Naskah -->

                    <!-- Sub Sub Menu Naskah Masuk Belum TL -->
                    <li id="list_naskahmasuk_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_naskahmasuksemua_btl') ?>"><i class="fa fa-circle-o default"></i> <span>
                                <font color='red'>Naskah Belum TL</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Sub Menu Naskah Masuk Belum TL -->
                </ul>
                <!-- Akhir Tree View Sub Menu Naskah Masuk -->
            </li>
            <!-- Akhir Sub Menu Naskah Masuk -->
  <?php if(($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4 ) || ($this->session->userdata('groupid')==7) || ($this->session->userdata('groupid')==8) ) { ?>
            <!-- Sub Menu Nota Dinas -->
            <li id="nodin2">
                <a href="<?= BASE_URL('administrator/anri_naskah_masuk_nm2') ?>">
                    <i class="fa fa-envelope-o default"></i>
                    <span>Nota Dinas</span>

                </a>

                <!-- Tree View Sub Menu Naskah Masuk -->
                <ul class="treeview-menu">
                    <!-- Sub Sub Menu Semua Naskah -->
                    <li id="list_naskah_masuk">
                        <a href="<?= BASE_URL('administrator/Anri_list_nota_all_dispu') ?>"><i class="fa fa-circle-o default"></i> <span>Nota Dinas</span>
                        </a>
                    </li>
                    <!-- Akhir Sub Sub Menu Semua Naskah -->

                    <!-- Sub Sub Menu Naskah Masuk Belum TL -->
                    <li id="list_naskahmasuk_btl">
                        <a href="<?= BASE_URL('administrator/Anri_list_notakeluar_btl') ?>"><i class="fa fa-circle-o default"></i> <span>
                                <font color='red'>Nota Dinas Belum TL</font>
                            </span>
                        </a>
                    </li>
                    <!-- Akhir Sub Sub Menu Naskah Masuk Belum TL -->
                </ul>
                <!-- Akhir Tree View Sub Menu Naskah Masuk -->
            </li>
            <!-- Akhir Sub Menu Naskah Masuk -->
<?php } ?>
            <!-- Sub Menu Nota Dinas -->
            <li id="nodin2" hidden>
                <a href="<?= BASE_URL('administrator/anri_dashboard/nodin2') ?>">
                    <i class="fa fa-sticky-note-o default"></i>

                    <span>Nota Dinas</span><i class="fa fa-angle-left pull-right"></i>
                </a>

                <!-- Tree View Sub Menu Nota Dinas -->
                <ul class="treeview-menu">



                    <!-- Sub Sub  Menu Nota Dinas Belum Approve -->
                    <li id="list_notadinas_btl_ap">
                        <a href="<?= BASE_URL('administrator/anri_list_notadinas_btl_ap') ?>"><i class="fa fa-circle-o default"></i> <span>
                                <font color='red'>Nodin Belum Disetujui</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Sub  Menu Nota Dinas Belum Approve -->

                    <!-- Sub Sub  Menu Nota Dinas Belum Dikirim -->
                    <li id="list_notadinas_sdh_ap">
                        <a href="<?= BASE_URL('administrator/anri_list_notadinas_sdh_ap') ?>"><i class="fa fa-circle-o default"></i> <span>
                                <font color='red'>Nodin Belum Dikirim</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Sub  Menu Nota Dinas Belum Dikirim -->

                    <!-- Sub Sub  Menu Nota Dinas Sudah Dikirim -->
                    <li id="list_notadinas_sdh_kirim">
                        <a href="<?= BASE_URL('administrator/anri_list_notadinas_sdh_kirim') ?>"><i class="fa fa-circle-o default"></i> <span>Nodin Sudah Dikirim</span>
                        </a>
                    </li>
                    <!-- Akhir Sub Sub  Menu Nota Dinas Sudah Dikirim -->
                </ul>
                <!-- Akhir Tree View Sub Menu Naskah Masuk -->
            </li>
            <!-- Akhir Sub Menu Naskah Masuk -->

 <?php if(($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4 ) || ($this->session->userdata('groupid')==7)) { ?>
            <!-- Sub Menu Disposisi -->
            <li id="dispo1">
                <a href="<?= BASE_URL('administrator/anri_dashboard/dispo1') ?>">
                    <i class="fa fa-credit-card default"></i>

                    <span>Disposisi</span><i class="fa fa-angle-left pull-right"></i>

                </a>

                <!-- Tree View Sub Menu Disposisi -->
                <ul class="treeview-menu">

                    <!-- Sub Menu Disposisi -->
                    <li id="list_disposisi">
                        <a href="<?= BASE_URL('administrator/anri_list_disposisi') ?>"><i class="fa fa-circle-o default"></i> <span>Semua Disposisi</span></a>
                    </li>
                    <!-- Akhir Sub Menu Disposisi -->

                    <!-- Sub Menu Disposisi Belum TL-->
                    <li id="list_disposisi_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_disposisi_btl') ?>">

                            <i class="fa fa-circle-o default"></i>

                            <span>
                                <font color='red'>Disposisi Belum TL</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Menu Disposisi Belum TL-->
                </ul>
                <!-- Akhir Tree View Sub Menu Disposisi -->
            </li>
            <!-- Akhir Sub Menu Disposisi -->

<?php } ?>
            <!-- Sub Menu Teruskan -->
            <li id="teruskan1">
                <a href="<?= BASE_URL('administrator/anri_dashboard/teruskan1') ?>">
                    <i class="fa fa-mail-forward default"></i>

                    <span>Teruskan</span><i class="fa fa-angle-left pull-right"></i>

                </a>

                <!-- Tree View Sub Menu Teruskan -->
                <ul class="treeview-menu">

                    <!-- Sub Menu Teruskan -->
                    <li id="list_teruskan">
                        <a href="<?= BASE_URL('administrator/anri_list_teruskan') ?>"><i class="fa fa-circle-o default"></i> <span>Semua Teruskan</span></a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan -->

                    <!-- Sub Menu Teruskan Belum TL-->
                    <li id="list_teruskan_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_teruskan_btl'); ?>">

                            <i class="fa fa-circle-o default"></i>

                            <span>
                                <font color='red'>Teruskan Belum TL</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan Belum TL-->
                </ul>
                <!-- Akhir Tree View Sub Menu Teruskan -->
            </li>
<?php if(($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4 ) || ($this->session->userdata('groupid')==7)) { ?>
             <li id="surat_tugas">
                <a href="<?= BASE_URL('administrator/anri_dashboard/surat_tugas') ?>">
                   <i class="fa fa-hand-o-right" aria-hidden="true"></i>

                    <span>Surat Perintah</span><i class="fa fa-angle-left pull-right"></i>

                </a>

                <!-- Tree View Sub Menu Teruskan -->
                <ul class="treeview-menu">

                    <!-- Sub Menu Teruskan -->
                    <li id="list_surattugas">
                        <a href="<?= BASE_URL('administrator/anri_list_surattugas') ?>"><i class="fa fa-circle-o default"></i> <span>Semua Surat Perintah</span></a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan -->

                    <!-- Sub Menu Teruskan Belum TL-->
                    <li id="list_surattugas_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_surattugas_btl'); ?>">

                            <i class="fa fa-circle-o default"></i>

                            <span>
                                <font color='red'>Surat Perintah Belum TL</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan Belum TL-->
                </ul>
                <!-- Akhir Tree View Sub Menu Teruskan -->
            </li>
            <!-- Akhir Sub Menu Teruskan -->
            <li id="surat_keterangan">
                <a href="<?= BASE_URL('administrator/surat_keterangan/list_tbl') ?>">
                   <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                    <span>Surat Keterangan</span>
                </a>
            </li>
            <li id="surat_rekomendasi">
                <a href="<?= BASE_URL('administrator/surat_rekomendasi/list_tbl') ?>">
                   <i class="fa fa-hand-o-right" aria-hidden="true"></i>

                    <span>Surat Rekomendasi</span>

                </a>
            </li>
            <li id="surat_pengumuman">
                <a href="<?= BASE_URL('administrator/surat_pengumuman/list_tbl') ?>">
                   <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                    <span>Pengumuman</span>
                </a>
                <!-- Akhir Tree View Sub Menu Teruskan -->
            </li>
               <li id="surat_tugas">
                <a href="<?= BASE_URL('administrator/anri_dashboard/surat_tugas') ?>">
                   <i class="glyphicon glyphicon-info-sign" aria-hidden="true"></i>
                    <span>Surat Instruksi</span>
                </a>

                <!-- Tree View Sub Menu Teruskan -->
                <ul class="treeview-menu">

                    <!-- Sub Menu Teruskan -->
                    <li id="list_surattugas">
                        <a href="<?= BASE_URL('administrator/anri_list_instruksi_all') ?>"><i class="glyphicon glyphicon-info-sign"></i> <span>Semua Surat Instruksi</span></a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan -->

                    <!-- Sub Menu Teruskan Belum TL-->
                    <li id="list_surattugas_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_instruksi_btl'); ?>">

                            <i class="glyphicon glyphicon-info-sign"></i>

                            <span>
                                <font color='red'>Surat Instruksi Belum TL</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan Belum TL-->
                </ul>
                <!-- Akhir Tree View Sub Menu Teruskan -->
            </li>
            <!-- Akhir Sub Menu Teruskan -->
            <li id="surat_tugas">
                <a href="<?= BASE_URL('administrator/anri_dashboard/surat_tugas') ?>">
                   <span class="glyphicon glyphicon-pencil"></span></i>

                    <span>Super Tugas</span><i class="fa fa-angle-left pull-right"></i>

                </a>

                <!-- Tree View Sub Menu Teruskan -->
                <ul class="treeview-menu">

                    <!-- Sub Menu Teruskan -->
                    <li id="list_surattugas">
                        <a href="<?= BASE_URL('administrator/anri_list_surat_m_tugas_all') ?>"><i class="fa fa-circle-o default"></i> <span>Semua Super Tugas </span></a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan -->

                    <!-- Sub Menu Teruskan Belum TL-->
                    <li id="list_surattugas_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_surat_m_tugas_btl'); ?>">

                            <i class="fa fa-circle-o default"></i>

                            <span>
                                <font color='red'>Super Tugas Belum TL</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan Belum TL-->
                </ul>
                <!-- Akhir Tree View Sub Menu Teruskan -->
            </li>
              <li id="surat_izin">
                <a href="<?= BASE_URL('administrator/anri_dashboard/surat_izin') ?>">
                  <span class="glyphicon glyphicon-book"></span>
                    <span>Surat Izin</span>

                </a>
                <!-- Tree View Sub Menu Teruskan -->
                <ul class="treeview-menu">

                    <!-- Sub Menu Teruskan -->
                    <li id="list_surattugas">
                        <a href="<?= BASE_URL('administrator/Anri_list_suratizin_all') ?>"><i class="glyphicon glyphicon-book"></i><span>Semua Surat Izin </span></a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan -->

                    <!-- Sub Menu Teruskan Belum TL-->
                    <li id="list_surattugas_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_suratizin_btl'); ?>">

                            <i class="glyphicon glyphicon-book"></i>

                            <span>
                                <font color='red'>Surat Izin Belum TL</font>
                            </span>

                        </a>
                    </li>
                    <!-- Akhir Sub Menu Teruskan Belum TL-->
                </ul>
                <!-- Akhir Tree View Sub Menu Teruskan -->
            </li>
<?php } ?>
            <!-- Sub Menu Undangan -->
            <li id="undangan1" hidden>
                <a href="<?= BASE_URL('administrator/anri_dashboard/undangan1') ?>">
                    <i class="fa fa-pencil-square-o default"></i>
                    <span>Undangan</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <!-- Tree View Sub Menu Undangan -->
                <ul class="treeview-menu">

                    <!-- Sub Menu Undangan -->
                    <li id="list_undangan">
                        <a href="<?= BASE_URL('administrator/anri_list_undangan') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>Semua Undangan</span></a>
                    </li>
                    <!-- Akhir Sub Menu Undangan -->

                    <!-- Sub Menu Undangan Belum TL-->
                    <li id="list_undangan_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_undangan_btl') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>
                                <font color='red'>Undangan Belum TL</font>
                            </span>
                        </a>
                    </li>
                    <!-- Akhir Sub Menu Undangan Belum TL-->
                </ul>
                <!-- Akhir Tree View Sub Menu Undangan -->
            </li>
            <!-- Akhir Sub Menu Undangan -->


            <!-- Sub Menu Naskah Dinas Lainnya -->
            <li id="nd11" hidden>
                <a href="<?= BASE_URL('administrator/anri_dashboard/nd1') ?>">
                    <i class="fa fa-file-pdf-o default"></i>
                    <span>Naskah Dinas Lainnya</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <!-- Tree View Sub Menu Naskah Dinas Lainnya -->
                <ul class="treeview-menu">

                    <!-- Sub Menu Naskah Dinas Lainnya -->
                    <li id="list_naskahdinas_lain" hidden>
                        <a href="<?= BASE_URL('administrator/anri_list_naskahdinas_lain') ?>"><i class="fa fa-circle-o default"></i> <span>Semua Naskah Dinas</span></a>
                    </li>
                    <!-- Akhir Sub Menu Naskah Dinas Lainnya -->

                    <!-- Sub Menu Naskah Dinas Lainnya Belum TL-->
                    <li id="list_naskahdinas_lain_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_naskahdinas_lain_btl') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>
                                <font color='red'>Naskah Dinas Belum TL</font>
                            </span>
                        </a>
                    </li>
                    <!-- Akhir Sub Menu Naskah Dinas Lainnya Belum TL-->
                </ul>
                <!-- Akhir Tree View Sub Menu Naskah Dinas Lainnya -->
            </li>
            <!-- Akhir Sub Menu Naskah Dinas Lainnya -->
        </ul>
        <!-- Tree View Kotak Masuk -->
    </li>
    <!-- Akhir Menu Kotak Masuk -->
    <li id="knsp1" hidden>
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-edit default"></i>
            <span>KONSEP SURAT</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>
        <ul class="treeview-menu">
            <li id="sd1">
                <a href="<?= BASE_URL('administrator/anri_dashboard/sd1') ?>">
                    <i class="fa fa-file-word-o default"></i>
                    <span>Konsep Surat Dinas</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li id="regis_suratdinas_keluar">
                        <a href="<?= BASE_URL('administrator/anri_regis_suratdinas_keluar') ?>"><i class="fa fa-circle-o default"></i> <span>Surat Dinas Baru</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>

                    <li id="log_suratdinas_keluar">
                        <a href="<?= BASE_URL('administrator/anri_log_suratdinas_keluar') ?>"><i class="fa fa-circle-o default"></i> <span>Surat Dinas Terkirim</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>
                    <li id="list_suratdinas_keluar">
                        <a href="<?= BASE_URL('administrator/anri_list_suratdinas_keluar') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>Semua Surat Dinas</span></a>
                    </li>
                  
                    <li id="list_suratdinas_keluar_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_suratdinas_keluar_btl') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>
                                <font color='red'>Surat Dinas Belum TL</font>
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>


        <ul class="treeview-menu">
            <li id="nodin2">
                <a href="<?= BASE_URL('administrator/anri_dashboard/nodin2') ?>">
                    <i class="fa fa-sticky-note-o default"></i>
                    <span>Konsep Nota Dinas</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>

                <ul class="treeview-menu">
                   
                    <li id="regis_notadinas">
                        <a href="<?= BASE_URL('administrator/anri_regis_notadinas') ?>"><i class="fa fa-circle-o default"></i> <span>Nota Dinas Baru</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>
                    
                    <li id="log_notadinas">
                        <a href="<?= BASE_URL('administrator/anri_log_notadinas') ?>"><i class="fa fa-circle-o default"></i> <span>Nota Dinas Terkirim</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>
                   
                    <li id="list_notadinas">
                        <a href="<?= BASE_URL('administrator/anri_list_notadinas') ?>"><i class="fa fa-circle-o default"></i> <span>Semua Nodin</span></a>
                    </li>
                  
                    <li id="list_notadinas_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_notadinas_btl') ?>"><i class="fa fa-circle-o default"></i> <span>
                                <font color='red'>Nodin Belum TL</font>
                            </span>

                        </a>
                    </li>
                    
                    <li id="list_notadinas_btl_ap">
                        <a href="<?= BASE_URL('administrator/anri_list_notadinas_btl_ap') ?>"><i class="fa fa-circle-o default"></i> <span>
                                <font color='red'>Nodin Belum Disetujui</font>
                            </span>

                        </a>
                    </li>
                   
                    <li id="list_notadinas_sdh_ap">
                        <a href="<?= BASE_URL('administrator/anri_list_notadinas_sdh_ap') ?>"><i class="fa fa-circle-o default"></i> <span>
                                <font color='red'>Nodin Belum Dikirim</font>
                            </span>

                        </a>
                    </li>
                  
                    <li id="list_notadinas_sdh_kirim">
                        <a href="<?= BASE_URL('administrator/anri_list_notadinas_sdh_kirim') ?>"><i class="fa fa-circle-o default"></i> <span>Nodin Sudah Dikirim</span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="treeview-menu">
            <li id="sd1">
                <a href="<?= BASE_URL('administrator/anri_dashboard/sd1') ?>">
                    <i class="fa fa-file-word-o default"></i>
                    <span>Konsep Surat Edaran</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li id="regis_suratedaran">
                        <a href="<?= BASE_URL('administrator/anri_regis_suratedaran') ?>"><i class="fa fa-circle-o default"></i> <span>Surat Edaran Baru</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>

                    <li id="log_suratdinas_keluar">
                        <a href="<?= BASE_URL('administrator/anri_log_suratedaran') ?>"><i class="fa fa-circle-o default"></i> <span>Surat Edaran Terkirim</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>
                    <li id="list_suratedaran">
                        <a href="<?= BASE_URL('administrator/anri_list_suratedaran') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>Semua Surat Edaran</span></a>
                    </li>

                    <li id="list_suratedaran_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_suratedaran_btl') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>
                                <font color='red'>Surat Edaran Belum TL</font>
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>

        <ul class="treeview-menu">
            <li id="st1">
                <a href="<?= BASE_URL('administrator/anri_dashboard/st1') ?>">
                    <i class="fa fa-file-image-o default"></i>
                    <span>Konsep Surat Perintah</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>

                <ul class="treeview-menu">
                    <li id="regis_surattugas">
                        <a href="<?= BASE_URL('administrator/anri_regis_surattugas') ?>"><i class="fa fa-circle-o default"></i>
                            <span>Surat Perintah Baru</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>

                    <li id="regis_supergub">
                        <a href="<?= BASE_URL('administrator/anri_regis_supergub') ?>"><i class="fa fa-circle-o default"></i>
                            <span>Surat Perintah Gubernur</span>
                            <span class="pull-right-container"></span>
                        </a>
                    </li>

                    <li id="list_surattugas">
                        <a href="<?= BASE_URL('administrator/anri_list_surattugas') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>Semua Surat Perintah</span></a>
                    </li>

                    <li id="list_surattugas_btl">
                        <a href="<?= BASE_URL('administrator/anri_list_surattugas_btl') ?>">
                            <i class="fa fa-circle-o default"></i>
                            <span>
                                <font color='red'>Surat Perintah Belum TL</font>
                            </span>
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </li> 
</li>

<?php if(($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4 ) || ($this->session->userdata('groupid')==14 ) || ($this->session->userdata('groupid')==7)) { ?>
    <!-- Menu Log Registrasi  -->
    <li id="belum_tl">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-file-zip-o default"></i>
            <span>KONSEP SURAT BELUM TL</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Log Registrasi -->
        <ul class="treeview-menu">
            <!-- Sub Menu Belum Approve -->
            <li id="list_naskah_belum_approve">
                <a href="<?= BASE_URL('administrator/anri_list_naskah_belum_approve'); ?>"><i class="fa fa-circle-o default"></i>

                    <span>
                        <font color="red">Naskah Belum Disejutui</font>
                    </span>

                </a>
            </li>
            <!-- Akhir Sub Menu Belum Approve -->

            <!-- Sub Menu Belum Dikirim -->
            <li id="list_naskah_sudah_approve">
                <a href="<?= BASE_URL('administrator/anri_list_naskah_sudah_approve') ?>"><i class="fa fa-circle-o default"></i>
                    <span>
                        <font color="red">Naskah Belum Dikirim</font>
                    </span>

                </a>
            </li>
            <!-- Akhir Sub Menu Belum Dikirim -->

            <!-- Sub Menu Sudah Kirim -->
            <li id="list_naskah_sudah_kirim">
                <a href="<?= BASE_URL('administrator/anri_list_naskah_sudah_kirim') ?>"><i class="fa fa-circle-o default"></i>

                    <span>Naskah Sudah Dikirim</span>

                </a>
            </li>
            <!-- Akhir Sub Menu Sudah Kirim -->

            <!-- Sub Menu Batal Kirim -->
            <li id="list_naskah_batal_kirim">
                <a href="<?= BASE_URL('administrator/anri_list_naskah_batal_kirim') ?>"><i class="fa fa-circle-o default"></i>

                    <span>
                        <font color="blue">Naskah Batal Dikirim</font>
                    </span>

                </a>
            </li>
            <!-- Akhir Sub Menu Batal Kirim -->

            <!-- Sub Menu Log Naskah Teruskan Setujui -->
            <li id="list_naskah_teruskan_approve">
                <a href="<?= BASE_URL('administrator/anri_list_naskah_teruskan_approve') ?>"><i class="fa fa-circle-o default"></i>

                    <span>
                        <font color="brown">Teruskan Naskah Setujui</font>
                    </span>

                </a>
            </li>
        </ul>
    </li>
<?php } ?>

    <!-- Menu Log Penomoran  -->
    <li id="penomoran_nodin">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-file-zip-o default"></i>
            <span>PENOMORAN NOTA DINAS</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Log Registrasi -->
        <ul class="treeview-menu">
            <!-- baru dispusipda -->
            <li id="anri_log_nota_belum_kasih_nomerdispu">
                <a href="<?= BASE_URL('administrator/Anri_log_nota_belum_kasih_nomerdispu') ?>"><i class="fa fa-circle-o default"></i>

                    <span>
                        <font color="brown">Nota Dinas Belum Dinomori</font>
                    </span>

                </a>
            </li>

            <li id="anri_log_nota_sudah_kasih_nomerdispu">
                <a href="<?= BASE_URL('administrator/Anri_log_nota_sudah_kasih_nomerdispu') ?>"><i class="fa fa-circle-o default"></i>

                    <span>
                        <font color="brown">Nota Dinas Sudah Dinomori</font>
                    </span>

                </a>
            </li>
        </ul>
    </li>


    <!-- Menu Log Penomoran  -->
    <li id="penomoran_surat_dinas">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-file-zip-o default"></i>
            <span>PENOMORAN SURAT</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Log Registrasi -->
        <ul class="treeview-menu">

            <li id="anri_list_naskah_uk_belum_nomerdispu">
                <a href="<?= BASE_URL('administrator/anri_list_naskah_uk_belum_nomerdispu') ?>"><i class="fa fa-circle-o default"></i>

                    <span>
                        <font color="brown">Daftar Penomoran Naskah</font>
                    </span>

                </a>
            </li>
            <li id="anri_log_surat_dinas_sudah_kasih_nomerdispu">
                <a href="<?= BASE_URL('administrator/Anri_log_surat_dinas_sudah_kasih_nomerdispu') ?>"><i class="fa fa-circle-o default"></i>

                    <span>
                        <font color="brown">Naskah Sudah Dinomori</font>
                    </span>

                </a>
            </li>
            <!-- akhir baru dispusipda -->
        </ul>
    </li>


    <!-- Menu Log Registrasi  -->
    <li id="log_registrasi">

        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-file-zip-o default"></i>
            <span>KOTAK KELUAR</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Log Registrasi -->
        <ul class="treeview-menu">
<?php if(($this->session->userdata('groupid')==6) ) { ?>
            <!-- Sub Menu Log Registrasi Naskah Masuk -->

            <li id="log_naskah_masuk">
                <a href="<?= BASE_URL('administrator/anri_list_drafting_naskah_keluar') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Naskah Terkirim</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            
            <li id="log_naskah_masuk">
                <a href="<?= BASE_URL('administrator/anri_log_naskah_masuk') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Reg Naskah Masuk</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
<?php } ?>
            <!-- Sub Menu Log Registrasi Naskah Masuk Setda-->
            <li id="log_penerima_naskah_setda">
                <a href="<?= BASE_URL('administrator/anri_log_naskah_masuk_setda') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Penerima Naskah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Sub Menu Log Registrasi Naskah Keluar -->
<?php if(($this->session->userdata('groupid')==6) ) { ?>
            <li id="log_surat_keluar_manual">
                <a href="<?= BASE_URL('administrator/anri_log_naskah_keluar') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Reg Naskah Keluar</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
<?php } ?>   

            <!-- Akhir Sub Menu Log Registrasi Naskah Keluar -->
<?php if(($this->session->userdata('groupid')==8) || ($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4)) { ?>
            <!-- Sub Menu Log Registrasi Nota Dinas -->
            <li id="log_nodin_manual">
                <a href="<?= BASE_URL('administrator/anri_log_nota_keluar') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Reg Nota Dinas</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
<?php }?>



<?php if(($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4 ) || ($this->session->userdata('groupid')==7)) { ?>

            <!-- Akhir Sub Menu Log Registrasi Nota Dinas -->
            <li id="log_surat_perintah">
                <a href="<?= BASE_URL('administrator/anri_log_surattugas') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Surat Perintah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <li id="log_surat_keterangan">
                <a href="<?= BASE_URL('administrator/surat_keterangan/list_log') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Surat Keterangan</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <li id="log_surat_rekomendasi">
                <a href="<?= BASE_URL('administrator/surat_rekomendasi/list_log') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Surat Rekomendasi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <li id="log_surat_pengumuman">
                <a href="<?= BASE_URL('administrator/surat_pengumuman/list_log') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Surat Pengumuman</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
             <li id="log_surat_m_tugas">
                <a href="<?= BASE_URL('administrator/anri_log_surat_m_tugas') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Super Tugas</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
             <li id="log_surat_m_tugas">
                <a href="<?= BASE_URL('administrator/anri_log_surat_izin') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Surat Izin</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
               <li id="log_surat_m_tugas">
                <a href="<?= BASE_URL('administrator/anri_log_surat_instruksi') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Surat Instruksi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
              <li id="log_nodin_draft">
                <a href="<?= BASE_URL('administrator/anri_log_notadinas') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Nota Dinas</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
             <li id="log_surat_dinas_draft">
                <a href="<?= BASE_URL('administrator/anri_log_suratdinas_keluar') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Log Draft Surat Dinas</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>

<?php } ?> 
            <!-- Sub Menu Log Registrasi Naskah Tanpa TL -->
            <li id="naskah_keluar_ttl">
                <a href="<?=BASE_URL('administrator/anri_log_naskah_ttl')?>"><i
                        class="fa fa-circle-o default"></i> <span> Log Naskah Tanpa Tindaklanjut</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Log Registrasi Naskah Tanpa TL -->
        </ul>
        <!-- Akhir Tree View Log Registrasi -->
    </li>
    <!-- Menu Log Registrasi  -->
    <?php if(($this->session->userdata('groupid')==3) || ($this->session->userdata('groupid')==4 ) || ($this->session->userdata('groupid')==7)) { ?>
      <li id="konsep_naskah">
        <a href="<?= BASE_URL('administrator/Anri_log_pernah_koreksi') ?>">
           <i class="fa fa-pencil" aria-hidden="true"></i>
           <span>RIWAYAT EDIT NASKAH</span>
       </a>
   </li>  
   <!-- Menu Log Registrasi  -->
<?php } ?> 

<li>
    <a href="<?= BASE_URL('administrator/tandatangan/respons/dokumen') ?>">
       <?php 
       $PeopleID = $this->session->userdata('peopleid');
       $this->db->where('PeopleIDTujuan', $PeopleID);
       $this->db->where('status', 0);
       $this->db->where('next',1);
       $this->db->from('m_ttd_kirim');
       $jumlahfilerespon = $this->db->count_all_results();
       
       
       $this->db->where('PeopleIDTujuan', $PeopleID);
       $this->db->group_by('ttd_id');
       $this->db->from('m_ttd_terusankirim');
       $this->db->where('status', 0);
       $jumlahfilekirim = $this->db->count_all_results();  
       ?> 
       <i class="fa fa-key default"></i>
       <span>TANDA TANGAN</span>
       
       <span class="label label-warning pull-right"><?php echo $jumlahfilekirim;?></span>
       <span class="label label-danger pull-right"><?php echo $jumlahfilerespon;?></span>
   </a> 
</li>

    <!-- Menu Berkas -->
    <li id="berkas">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-file default"></i>
            <span>BERKAS</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Berkas -->
        <ul class="treeview-menu">

            <!-- Sub Menu Berkas Unit Kerja -->
            <li id="berkas_unit">
                <a href="<?= BASE_URL('administrator/anri_berkas_unit') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Berkas Unit Kerja</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Berkas Unit Kerja -->

            <!-- Sub Menu Naskah Belum Diberkaskan -->
            <li id="naskah_belum_berkas">
                <a href="<?= BASE_URL('administrator/anri_naskah_belum_berkas') ?>"><i class="fa fa-circle-o default"></i>
                    <span>
                        <font color=red>Naskah Belum Diberkaskan</font>
                    </span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Naskah Belum Diberkaskan -->

            <!-- Sub Menu Naskah Sudah Diberkaskan -->
            <li id="naskah_sudah_berkas">
                <a href="<?= BASE_URL('administrator/anri_naskah_sudah_berkas') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Naskah Sudah Diberkaskan</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Naskah Sudah Diberkaskan -->

            <!-- Sub Menu Daftar Arsip Aktif -->
            <li id="daftar_arsip_aktif">
                <a href="<?= BASE_URL('administrator/anri_daftar_arsip_aktif') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Daftar Arsip Aktif</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Daftar Arsip Aktif -->

            <!-- Sub Menu Daftar Arsip Inaktif -->
            <li id="daftar_arsip_inaktif">
                <a href="<?= BASE_URL('administrator/anri_daftar_arsip_inaktif') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Daftar Arsip Inaktif</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Daftar Arsip Inaktif -->

            <!-- Sub Menu Berkas Lewat Aktif -->
            <li id="daftar_arsip_lewat_aktif">
                <a href="<?= BASE_URL('administrator/anri_daftar_arsip_lewat_aktif') ?>"><i class="fa fa-circle-o default"></i> <span>Arsip Melewati Aktif</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Berkas Lewat Aktif -->

            <!-- Sub Menu Berkas Lewat Inaktif -->
            <li id="daftar_arsip_lewat_inaktif">
                <a href="<?= BASE_URL('administrator/anri_daftar_arsip_lewat_inaktif') ?>"><i class="fa fa-circle-o default"></i> <span>Arsip Melewati Inaktif</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Berkas Lewat Inaktif -->

            <!-- Sub Menu Usul Musnah -->
            <li id="usul_musnah">
                <a href="<?= BASE_URL('administrator/anri_usul_musnah') ?>"><i class="fa fa-circle-o default"></i>
                <span>Usul Musnah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Usul Musnah -->

            <!-- Sub Menu Musnah -->
            <li id="musnah">
                <a href="<?= BASE_URL('administrator/anri_musnah') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Musnah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Musnah -->

            <!-- Sub Menu Usul Serah -->
            <li id="usul_serah">
                <a href="<?= BASE_URL('administrator/anri_usul_serah') ?>"><i class="fa fa-circle-o default"></i> <span>Usul
                        Serah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Usul Serah -->

            <!-- Sub Menu Serah -->
            <li id="serah">
                <a href="<?= BASE_URL('administrator/anri_serah') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Serah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Serah -->
        </ul>
        <!-- Akhir Tree View Berkas -->
    </li>
    <!-- Akhir Menu Berkas -->

    <!-- Menu Klasifikasi -->
    <li id="klasifikasi">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-sitemap default"></i>
            <span>KLASIFIKASI</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- tree view klasifikasi -->
        <ul class="treeview-menu">

            <!-- Sub Menu Klasifikasi -->
            <li id="classification">
                <a href="<?= BASE_URL('administrator/anri_master_klasifikasi') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Pengaturan Klasifikasi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- AkhirSub Menu Klasifikasi -->
        </ul>
        <!-- Akhir tree view klasifikasi -->
    </li>
    <!-- Akhir Menu Klasifikasi -->


    <!-- Menu Pengaturan Umum -->
    <li id="pengaturan_umum">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-cogs  default"></i>
            <span>PENGATURAN UMUM</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Pengaturan Umum -->
        <ul class="treeview-menu">

            <!-- Sub Menu Pengaturan Bahasa -->
            <li id="master_bahasa">
                <a href="<?= BASE_URL('administrator/anri_master_bahasa') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Bahasa</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Bahasa -->

            <!-- Sub Menu Pengaturan Jenis Naskah -->
            <li id="master_jnaskah">
                <a href="<?= BASE_URL('administrator/anri_master_jnaskah') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Jenis Naskah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Jenis Naskah -->

            <!-- Sub Menu Pengaturan Media Arsip -->
            <li id="master_media">
                <a href="<?= BASE_URL('administrator/anri_master_media') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Media Arsip</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Media Arsip -->

            <!-- Sub Menu Pengaturan Sifat Naskah -->
            <li id="master_sifat">
                <a href="<?= BASE_URL('administrator/anri_master_sifat') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Sifat Naskah</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Sifat Naskah -->

            <!-- Sub Menu Pengaturan Tingkat Perkembangan -->
            <li id="master_tperkembangan">
                <a href="<?= BASE_URL('administrator/anri_master_tperkembangan') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Tingkat Perkembangan</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Tingkat Perkembangan -->

            <!-- Sub Menu Pengaturan Tingkat Urgensi -->
            <li id="master_urgensi">
                <a href="<?= BASE_URL('administrator/anri_master_urgensi') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Tingkat Urgensi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Tingkat Urgensi -->

            <!-- Sub Menu Pengaturan Satuan Unit -->
            <li id="master_satuanunit">
                <a href="<?= BASE_URL('administrator/anri_master_satuanunit') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Satuan Unit</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Satuan Unit -->

            <!-- Sub Menu Pengaturan Grup Unit Kerja -->
            <li id="master_grole">
                <a href="<?= BASE_URL('administrator/anri_master_grole') ?>"><i class="fa fa-circle-o default"></i> <span>Grup
                        Unit Kerja</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Grup Unit Kerja -->

            <!-- Sub Menu Pengaturan Grup Jabatan -->
            <li id="master_gjabatan">
                <a href="<?= BASE_URL('administrator/anri_master_gjabatan') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Grup Jabatan</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Sub Menu Pengaturan Grup Jabatan -->

            <!-- Sub Menu Pengaturan Isi Disposisi -->
            <li id="master_disposisi">
                <a href="<?= BASE_URL('administrator/anri_master_disposisi') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Isi Disposisi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Isi Disposisi -->

            <!-- Sub Menu Pengaturan Kop Naskah Dinas -->
            <li id="master_kopnaskah">
                <a href="<?= BASE_URL('administrator/anri_master_kopnaskah') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Kop Naskah Dinas</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Kop Naskah Dinas -->

            <!-- Sub Menu Pengaturan Instansi -->
            <li id="master_instansi">
                <a href="<?= BASE_URL('administrator/anri_master_instansi') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Instansi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Instansi -->

            <!-- Sub Menu Pengaturan Template Dokumen -->
            <li id="master_doc_template">
                <a href="<?= BASE_URL('administrator/anri_master_doc_template') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Template Dokumen</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Pengaturan Template Dokumen -->

            <li id="Pengumuman">
                <a href="<?= BASE_URL('administrator/notice') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Pengumuman<span>
                            <span class="pull-right-container"></span>
                </a>
            </li>
        </ul>
        <!-- Akhir Tree View Pengaturan Umum -->
    </li>
    <!-- Akhir Menu Pengaturan Umum -->

    <!-- Menu Unit Kerja dan Pengguna -->
    <li id="unit_kerja_pengguna">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-users  default"></i>
            <span>UNIT KERJA & PENGGUNA</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Unit Kerja dan Pengguna -->
        <ul class="treeview-menu">

            <!-- Sub Menu Unit Kerja -->
            <li id="role">
                <a href="<?= BASE_URL('administrator/anri_master_role') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Unit Kerja</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Unit Kerja -->

            <!-- Sub Menu Pengguna -->
            <li id="people">
                <a href="<?= BASE_URL('administrator/anri_master_pengguna') ?>"><i class="fa fa-circle-o default"></i>
                    <span>Pengguna</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Sub Menu Pengguna -->
        </ul>
        <!-- Akhir Tree View Unit Kerja dan Pengguna -->
    </li>
    <!-- Akhir Menu Unit Kerja dan Pengguna -->

    <!-- Menu Akses Modul Group -->
    <li id="modul_group">
        <a href="<?= BASE_URL('administrator/anri_dashboard/modul_group') ?>">
            <i class="fa fa-calendar"></i>
            <span>AKSES PENGGUNA</span>
        </a>
    </li>
    <!-- Akhir Menu Akses Modul Group -->

    <!-- Menu Template Dokumen -->
    <li id="template">
        <a href="<?= BASE_URL('administrator/anri_list_temp_doc') ?>" style="Font-size:11px ">
            <i class="fa fa-cloud-download default"></i>
            <span>UNDUH TEMPLATE NASKAH</span>
        </a>
    </li>
    <!-- Akhir Menu Template Dokumen -->

    <!-- Menu User Manual -->
    <li>
        <a href="">
            <i class="fa fa-list-alt default"></i>
            <span>USER MANUAL</span>
        </a>
        <!-- Tree Menu Registrasi -->
        <ul class="treeview-menu">
            <li id="naskah_masuk">
                <a href="<?= BASE_URL('upejabat.pdf') ?>">
                    <i class="fa fa-circle-o default"></i> <span>Pejabat Struktural / Staf </span>
                    <span class="pull-right-container"></span>
                </a>
            </li>


            <li id="reg_naskah_keluar">
                <a href="<?= BASE_URL('up.pdf') ?>"><i class="fa fa-circle-o default"></i> <span>TU / UP</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>

            <!-- Sub Menu Nota Dinas Manual !-->
            <li id="nota_dinas_manual">
                <a href="<?= BASE_URL('uk.pdf') ?>">
                    <i class="fa fa-circle-o default"></i> <span>Unit Kearsipan</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <li id="manual_tte">
                <a href="<?= BASE_URL('MANUAL%20BOOK%20FITUR%20TTE%20SIKD.pdf') ?>">
                    <i class="fa fa-circle-o default"></i> <span>Tanda Tangan</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <?php if($this->session->userdata('groupid')==1){ ?>
            <li id="panduan_admin">
                <a href="<?= BASE_URL('MANUAL%20BOOK%20FITUR%20INTEGRASI%20SIKD%20v4.pdf') ?>">
                    <i class="fa fa-circle-o default"></i> <span>Integrasi SIAP SIKD</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <?php } ?>
            <!-- Akhir Sub menu Nota Dinas !-->
            <!-- Akhir Sub Menu Registrasi Naskah Tanpa TL -->
        </ul>
        <!-- Akhir Tree Menu Registrasi -->
    </li>

    <!-- Menu Logout -->
    <li id="video">
        <a href="<?= BASE_URL('administrator/anri_list_temp_vid') ?>">
            <i class="fa fa-file-video-o default"></i>
            <span>VIDEO TUTORIAL</span>
        </a>

    </li>
    <!-- Akhir Menu User Manual -->

    <!-- Menu Logout -->

    <!-- Akhir Menu User Manual -->
    <!-- Menu Pengaturan Integrasi -->
    <li id="integrasi_siap">
        <a href="<?= BASE_URL('administrator/anri_dashboard/') ?>">
            <i class="fa fa-cogs  default"></i>
            <span>Integrasi SIAP SIKD</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Pengaturan Umum -->
        <ul class="treeview-menu" id="integrasi_master">

            <!-- Sub Menu Pengaturan Bahasa -->
            <li id="integrasi_bulkdata">
                <a href="<?= BASE_URL('administrator/integrasi_bulkdata') ?>"><i class="fa fa-circle-o default"></i> <span>Bulk Data (Import Data SIAP)</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <li id="integrasi_master">
                <a href="<?= BASE_URL('administrator/integrasi_masterpd') ?>"><i class="fa fa-circle-o default"></i> <span>Master Data PD</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <li id="integrasi_master">
                <a href="<?= BASE_URL('administrator/integrasi_master') ?>"><i class="fa fa-circle-o default"></i> <span>Master Data Per PD</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <li id="integrasi_people">
                <a href="<?= BASE_URL('administrator/integrasi_people') ?>"><i class="fa fa-circle-o default"></i> <span>Tambah Pengguna SIKD</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!--
			<li id="integrasi_notifikasi_simulasi">
                <a href="<?= BASE_URL('administrator/integrasi_notifikasi_simulasi') ?>"><i
                        class="fa fa-circle-o default"></i> <span>Notifikasi Simulasi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
			-->
            <!-- <li id="integrasi_notifikasi">
                <a href="<?= BASE_URL('administrator/integrasi_notifikasi') ?>"><i class="fa fa-circle-o default"></i> <span>Notifikasi</span>
                    <span class="pull-right-container"></span>
                </a>
            </li> -->
            <li id="integrasi_perdinas">
                <a href="<?= BASE_URL('administrator/integrasi_perdinas') ?>"><i class="fa fa-circle-o default"></i> <span>Update Data Per Dinas</span>
                    <span class="pull-right-container"></span>
                </a>
            </li>
            <!-- Akhir Sub Menu Integrasi-->

        </ul>
        <!-- Akhir Tree View Pengaturan Umum -->
    </li>
    <!-- Akhir Menu Pengaturan Umum -->


    <li id="Log_passphrase">
        <a href="<?= BASE_URL('administrator/User') ?>">
            <i class="fa fa-calendar"></i>
            <span>LOG_PASSPHRASE</span>
            <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
            </span>
        </a>

        <!-- Tree View Pengaturan Umum -->
        <ul class="treeview-menu">
            <!-- Sub Sub Menu Semua Naskah -->
            <li id="list_naskah_masuk">
                <a href="<?= BASE_URL('administrator/User') ?>"><i class="fa fa-circle-o default"></i> <span>Log_Passphrase</span>
                </a>
            </li>
            <!-- Akhir Sub Sub Menu Semua Naskah -->

            <!-- Sub Sub Menu Naskah Masuk Belum TL -->
            <li id="list_log_passphrase">

            </li>
            <!-- Akhir Sub Sub Menu Naskah Masuk Belum TL -->
        </ul>


 
        <!-- Akhir Tree View Pengaturan Umum -->
    </li>

     
    <!-- Akhir Menu TTD -->

    <!-- Menu Logout -->
    <li>
        <a href="<?= BASE_URL('administrator/anri_logout') ?>">
            <i class="fa fa-sign-out default"></i>
            <span>KELUAR</span>
        </a>
    </li>
    <!-- Akhir Menu Logout -->

    <br>
    <!-- Menu Logout -->
    <li>
        <img src="<?= BASE_URL('uploads') ?>/bsre.jpeg?>" Width="200px" height="80px" align="center">
    </li>
    <!-- Akhir Menu Logout -->

</ul>
<!-- Akhir Navigation -->

<?php
$this->db->select("modul_name");
$this->db->where('group_id', $this->session->userdata('groupid'));
$menu = $this->db->get('modul_group')->result();

foreach ($menu as $a) {
    echo '<script>$("#' . $a->modul_name . '").hide();</script>';
}

?>