<link href="<?= BASE_ASSET; ?>/select2/select2.min.css" rel="stylesheet" />
<style>
  .select2-container--default,
  .select2-selection--single {
    border: 1px solid #fff !important;
    width: 100% !important;
  }

  .select2-container--default .select2-selection--multiple {
    border: 1px solid #fff !important;
  }

  .select2-container {
    border: 1px solid #DEDCDC !important;
    border-radius: 5px !important;
    padding: 2px 0px !important;
  }
</style>

<!-- MODAL TERUSKAN -->
<div id="teruskan" class="modal fade" role="dialog">
  <div class="modal-lg modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Teruskan</h4>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('administrator/anri_mail_tl/inbox_teruskan/' . $tipe . '/' . $NId); ?>" class="form-horizontal" method="POST">
          <div class="form-group">
            <div class="col-md-9">
              <?php $x = $_GET['dt']; ?>
              <input type="hidden" name="xdt" class="form-control" value="<?= $x; ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Tujuan Surat <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <select name="tujuan[]" class="form-control select2" required multiple>
                <!-- Perbaikan Eko 09-11-2019 -->
                <?php
                $query = $this->db->query("SELECT PeopleId, PeoplePosition, PeopleName FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId IN ('3','4','6') AND p.PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY p.PrimaryRoleId ASC")->result();
                foreach ($query as $dat_people) {
                ?>
                  <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                <?php
                }
                ?>
                <!-- batas akhir perbaikan -->
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Tembusan</div>
            <div class="col-md-9">
              <select name="tembusan[]" class="form-control chosen chosen-select" multiple id="tembusan">
                <!-- Perbaikan Eko 09-11-2019 -->
                <?php
                $query = $this->db->query("SELECT PeopleId, PeoplePosition, PeopleName  FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY p.PrimaryRoleId ASC")->result();
                foreach ($query as $dat_people) {
                ?>
                  <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                <?php
                }
                ?>
                <!-- batas akhir perbaikan -->
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Pesan</div>
            <div class="col-md-9">
              <textarea name="pesan" class="form-control mytextarea" id="" cols="30" rows="10"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="title" class="col-sm-3">Unggah Data
            </label>
            <div class="col-sm-9">
              <div class="upload_teruskan"></div>
              <div class="upload_teruskan_listed"></div>
              <small class="info help-block">
              </small>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <button class="btn btn-danger">Simpan dan Kirim</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- TUTUP MODAL TERUSKAN -->

<!-- KIRIM KELUAR DINAS -->
<div id="kirim_keluar_dinas" class="modal fade" role="dialog">
  <div class="modal-lg modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">KIRIM KE UNIT KEARSIPAN TUJUAN</h4>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('administrator/anri_mail_tl/inbox_kirim_keluar_dinas/' . $tipe . '/' . $NId); ?>" class="form-horizontal" method="POST">
          <div class="form-group">
            <div class="col-md-9">
              <?php $x = $_GET['dt']; ?>
              <input type="hidden" name="xdt" class="form-control" value="<?= $x; ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Tujuan Surat <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <select name="tujuan[]" class="form-control select2" required multiple>
                <!-- Perbaikan Eko 09-11-2019 -->
                <?php
                $query = $this->db->query("SELECT PeopleId, PeoplePosition, PeopleName FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId IN ('6') AND p.PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY p.PrimaryRoleId ASC")->result();
                foreach ($query as $dat_people) {
                ?>
                  <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                <?php
                }
                ?>
                <!-- batas akhir perbaikan -->
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <button class="btn btn-danger">Simpan dan Kirim</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- AKIR KIRIM KELUAR DINAS -->



<!-- MODAL NOTA DINAS -->
<div id="notadinas" class="modal fade" role="dialog">
  <div class="modal-lg modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nota Dinas</h4>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('administrator/anri_mail_tl/inbox_nota_dinas/' . $tipe . '/' . $NId); ?>" class="form-horizontal" method="POST">
          <div class="form-group">
            <div class="col-md-3">Tujuan Surat <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <select name="tujuan[]" class="form-control select2" multiple required>
                <!-- Perbaikan Eko 09-11-2019 -->
                <?php
                $query = $this->db->query("SELECT * FROM v_login WHERE GRoleId = '" . $this->session->userdata('groleid') . "' AND PeopleId != " . $this->session->userdata('peopleid') . " AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4') ORDER BY GRoleId ASC")->result();
                foreach ($query as $dat_people) {
                ?>
                  <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                <?php
                }
                ?>
                <!-- batas akhir perbaikan -->
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Tembusan</div>
            <div class="col-md-9">
              <select name="tembusan[]" class="form-control chosen chosen-select" multiple id="tembusan">
                <!-- Perbaikan Eko 09-11-2019 -->
                <?php
                $query = $this->db->query("SELECT * FROM v_login WHERE GRoleId = '" . $this->session->userdata('groleid') . "' AND PeopleId != " . $this->session->userdata('peopleid') . " AND GroupId BETWEEN '3' AND '4' AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4') ORDER BY GRoleId ASC")->result();
                foreach ($query as $dat_people) {
                ?>
                  <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                <?php
                }
                ?>
                <!-- batas akhir perbaikan -->
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Pesan</div>
            <div class="col-md-9">
              <textarea name="pesan" class="form-control mytextarea" id="" cols="30" rows="10"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="title" class="col-sm-3">Unggah Data
            </label>
            <div class="col-sm-9">
              <div class="upload_notadinas"></div>
              <div class="upload_notadinas_listed"></div>
              <small class="info help-block">
              </small>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <button class="btn btn-danger">Simpan dan Kirim</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- TUTUP MODAL NOTA DINAS -->

<!-- MODAL NASKAH DINAS -->
<div id="naskahdinas" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Nota Dinas Tindaklanjut</h4>
      </div>
      <div class="modal-body">
        <form id="myfrm" class="form-horizontal" method="POST">
          <div class="form-group">
            <div class="col-md-9">
              <?php $x = $_GET['dt']; ?>
              <input type="hidden" name="xdt" class="form-control" value="<?= $x; ?>">
              <input type="hidden" name="xdt1" id="xdt1" class="form-control" value="<?= $NId; ?>">
              <input type="hidden" name="xdt2" id="xdt2" class="form-control" value="<?= $tipe; ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Tanggal Registrasi</div>
            <div class="col-md-9">
              <input type="text" disabled class="form-control" value="<?= date('d-m-Y') ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Pembuat Naskah</div>
            <div class="col-md-9">
              <input type="text" disabled class="form-control" value="<?= $this->session->userdata('peoplename'); ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Jenis Naskah</div>
            <div class="col-md-9">
              <input type="text" disabled class="form-control" value="Nota Dinas">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Lampiran</div>
            <div class="col-md-2">
              <input type="number" class="form-control" name="Jumlah" id="Jumlah" min="0" required>
            </div>
            <div class="col-md-6">
              <select name="MeasureUnitId" class="form-control select2">
                <?php foreach ($this->db->query('SELECT * FROM master_satuanunit')->result() as $data) { ?>
                  <option value="<?= $data->MeasureUnitId; ?>"><?= $data->MeasureUnitName; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Kode Klasifikasi <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <select name="ClId" id="ClId" class="form-control select2" required>
                <option value="">-- Pilih --</option>
                <?php foreach ($this->db->query("SELECT ClId, ClCode, ClName FROM classification WHERE CIStatus = '1' AND ClId != '1' ORDER BY ClCode ASC")->result() as $data) { ?>
                  <option value="<?= $data->ClId; ?>"><?= $data->ClCode; ?> - <?= $data->ClName; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Sifat Naskah <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <select name="SifatId" id="SifatId" class="form-control select2" required>
                <option value="">-- Pilih --</option>
                <?php foreach ($this->db->query('SELECT * FROM master_sifat')->result() as $data) { ?>
                  <option value="<?= $data->SifatId; ?>"><?= $data->SifatName; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Hal <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <input type="text" class="form-control" name="Hal" id="Hal" placeholder="Hal" required>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Kepada Yth <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <select name="RoleId_To[]" id="RoleId_To" class="form-control select2" required multiple>
                <!-- Perbaikan Eko 09-11-2019 -->
                <?php
                $query = $this->db->query("SELECT PrimaryRoleId, PeoplePosition, PeopleName  FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY p.PrimaryRoleId ASC")->result();
                foreach ($query as $dat_people) {
                ?>
                  <option value="<?= $dat_people->PrimaryRoleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                <?php
                }
                ?>
                <!-- batas akhir perbaikan -->
              </select>
            </div>
          </div>
          <!--           <div class="form-group">
            <div class="col-md-3">Hal Pengantar Nota Dinas<sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <input type="text" class="form-control" name="Hal_pengantar" placeholder="Hal Pengantar Nota Dinas" required>
            </div>
          </div>          
          <div class="form-group">
            <div class="col-md-3">Pengantar Nota Dinas<sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <textarea name="Pesan" class="form-control mytextarea" cols="30" rows="100" placeholder="Pengantar Nota Dinas"></textarea>
            </div> -->
          <!-- </div>           -->
          <div class="form-group">
            <div class="col-md-3">Konsep Nota Dinas<sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <textarea name="Konten" class="form-control mytextarea" cols="30" rows="100" placeholder="Isi Draft Nota Dinas"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Tembusan</div>
            <div class="col-md-9">
              <select name="RoleId_Cc[]" class="form-control chosen chosen-select" multiple>
                <!-- Perbaikan Eko 09-11-2019 -->
                <?php
                $query = $this->db->query("SELECT PrimaryRoleId, PeoplePosition, PeopleName  FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND GroupId BETWEEN '3' AND '4' AND p.PeopleIsActive = '1' AND p.PeopleId != " . $this->session->userdata('peopleid') . " ORDER BY p.PrimaryRoleId ASC")->result();
                foreach ($query as $dat_people) {
                ?>
                  <option value="<?= $dat_people->PrimaryRoleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                <?php
                }
                ?>
                <!-- batas akhir perbaikan -->
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Penandatangan <sup class="text-danger">*</sup></div>

            <div class="col-md-3">
              <select name="TtdText" class="form-control select2 TtdText" required>
                <?php foreach ($this->db->query('SELECT * FROM master_ttd')->result() as $data1) { ?>
                  <option value="<?= $data1->TtdText; ?>"><?= $data1->NamaText; ?></option>
                <?php } ?>
              </select>
            </div>

            <?php
            $AtasanId = $this->db->query("SELECT PeopleId FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' AND p.PrimaryRoleId ='" . $this->session->userdata('roleatasan') . "'")->result();
            foreach ($AtasanId as $dataz) {
              $xx1 = $dataz->PeopleId;
            }
            ?>
            <input type="hidden" id="tmpid_atasan" name="tmpid_atasan" value="<?= $xx1; ?>">

            <div class="col-md-6 Approve_People hidden">
              <select name="Approve_People" id="Approve_People" class="form-control">
                <!-- Perbaikan Eko 09-11-2019 -->
                <?php
                $query = $this->db->query("SELECT PeopleId, PeoplePosition, PeopleName  FROM people p WHERE p.RoleAtasan NOT IN ('','-') AND p.PeopleIsActive = '1' AND GroupId BETWEEN '3' AND '4' ORDER BY p.PrimaryRoleId ASC")->result();
                foreach ($query as $dat_people) {
                ?>
                  <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                <?php
                }
                ?>
                <!-- batas akhir perbaikan -->
              </select>
            </div>

          </div>

          <div class="form-group">
            <label for="title" class="col-sm-3">Unggah Data
            </label>
            <div class="col-sm-9">
              <div class="upload_naskahdinas"></div>
              <div class="upload_naskahdinas_listed"></div>
              <small class="info help-block">
              </small>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <a title="Lihat Naskah" onclick="lihat_naskah();" class="btn btn-flat btn-success btn-lihatnaskah" target="_blank">Lihat Konsep Nota Dinas</a>
              <button onclick="simpan_naskah();" class="btn btn-danger">Simpan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- TUTUP MODAL NASKAH DINAS -->

<!-- MODAL DISPOSISI -->
<div id="disposisi" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Disposisi</h4>
      </div>
      <div class="modal-body">
        <form action="<?= base_url('administrator/anri_mail_tl/post_inbox_disposisi/' . $tipe . '/' . $NId); ?>" class="form-horizontal" method="POST">
          <div class="form-group" hidden>
            <div class="col-md-3">No Indeks</div>
            <div class="col-md-9">
              <?php $x = $_GET['dt']; ?>
              <input type="text" name="NoIndex" class="form-control">
              <input type="hidden" name="xdt" class="form-control" value="<?= $x; ?>">
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Sifat <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <select name="Sifat" class="form-control chosen chosen-select">
                <option value="">-- Pilih Sifat --</option>
                <?php foreach ($this->db->query('SELECT * FROM master_sifat ORDER BY SifatName ASC')->result() as $sifat) { ?>
                  <option value="<?= $sifat->SifatId; ?>"><?= $sifat->SifatName; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Isi Disposisi</div>
            <div class="col-md-9">
              <?php foreach ($this->db->query("SELECT DisposisiId, DisposisiName FROM master_disposisi WHERE gjabatanId ='" . $this->session->userdata('gjabatanid') . "'")->result() as $isi) { ?>
                <input type="checkbox" name="Disposisi[]" value="<?= $isi->DisposisiId ?>"> <?= $isi->DisposisiName ?> <br>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Tujuan Disposisi <sup class="text-danger">*</sup></div>
            <div class="col-md-9">
              <?php foreach ($this->db->query("SELECT PeopleId, PeoplePosition,PeopleName FROM people WHERE PeopleIsActive = '1' AND RoleAtasan = '" . $this->session->userdata('roleid') . "' AND PeopleId != '" . $this->session->userdata('peopleid') . "' AND GroupId IN ('3','4','7') ORDER BY PrimaryRoleId ASC")->result() as $atasan) { ?>
                <input type="checkbox" name="RoleId[]" value="<?= $atasan->PeopleId ?>"> <?= $atasan->PeopleName ?> -- <?= $atasan->PeoplePosition ?> <br>
              <?php } ?>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Tujuan Disposisi Lainnya</div>
            <div class="col-md-9">
              <select name="tujuan_lainnya[]" class="form-control chosen chosen-select" multiple>
                
              <?php
                $query = $this->db->query("SELECT * FROM v_login WHERE PeopleId != ".$this->session->userdata('peopleid')." AND RoleAtasan NOT IN ('','-') AND GroupId  IN('3','4','7') ORDER BY GroupId, Golongan DESC, Eselon ASC, PrimaryRoleId ASC")->result();
                foreach ($query as $dat_people) {
                    ?>

                    <option value="<?= $dat_people->PeopleId; ?>"><?= $dat_people->PeopleName ?> -- <?= $dat_people->PeoplePosition; ?></option>
                    <?php
                }
                ?>                                         
                <!-- batas akhir perbaikan -->


                <!-- batas akhir perbaikan -->
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3">Pesan</div>
            <div class="col-md-9">
              <textarea name="pesan" class="form-control mytextarea" id="" cols="30" rows="10"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label for="title" class="col-sm-3">Unggah Data
            </label>
            <div class="col-sm-9">
              <div class="upload_disposisi"></div>
              <div class="upload_disposisi_listed"></div>
              <small class="info help-block">
              </small>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <button class="btn btn-danger">Simpan dan Kirim</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- TUTUP MODAL DISPOSISI -->

<div id="seleseitindaklanjut" class="modal fade" role="dialog">
  <div class="modal-dialog modal-dm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" align="center">Anda akan menyeleseikan naskah</h4>
      </div>
      <div class="modal-body">




        <div class="form-group" hidden>
          <div class="col-md-3">No </div>

        </div>

        <div class="form-group">
          <div class="col-md-3" align=""></div>

          <button class="btn btn-warning btn-md" class="btn btn-warning btn-md"></a>
            <li><i class="fa fa-check" class="btn btn-warning btn-md"></i> <a href="<?= site_url('administrator/anri_mail_tl/selesai_tindaklanjut/view/' . $NId . '?dt=' . $x); ?>"> Selesai Tindak Lanjut</a></li>
          </button>

        </div>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="<?= BASE_ASSET; ?>/tinymce/tinymce.min.js"></script>
<script src="<?= BASE_ASSET; ?>/select2/select2.min.js"></script>

<script>
  $(' .TtdText').change(function() {
    if ($(this).find(':selected').val() == 'none') {
      $('.Approve_People').addClass('hidden');
    } else {
      $('.Approve_People').removeClass('hidden');
      r_atasan = "<?= $this->session->userdata('roleatasan'); ?>";
      $('#Approve_People').select2();
      $('#Approve_People').attr('disabled', false);
      if (this.value == "AL") {
        idatasan = $("#tmpid_atasan").val();
        $('#Approve_People').val(idatasan).change();
        $('#Approve_People').attr('disabled', true);
      }
    }
  });
</script>

<script>
  $(document).ready(function() {
    $('.select2').select2();
  });

  tinymce.init({
    force_br_newlines: false,
    force_p_newlines: false,
    forced_root_block: '',
    selector: '.mytextarea',
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    theme: "modern",
    plugins: [
      "lists spellchecker",
      "code",
      "save table"
    ],

    toolbar: "bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table",

    image_advtab: true,

    style_formats: [{
        title: 'Bold text',
        format: 'h1'
      },
      {
        title: 'Red text',
        inline: 'span',
        styles: {
          color: '#ff0000'
        }
      },
      {
        title: 'Red header',
        block: 'h1',
        styles: {
          color: '#ff0000'
        }
      },
      {
        title: 'Example 1',
        inline: 'span',
        classes: 'example1'
      },
      {
        title: 'Example 2',
        inline: 'span',
        classes: 'example2'
      },
      {
        title: 'Table styles'
      },
      {
        title: 'Table row 1',
        selector: 'tr',
        classes: 'tablerow1'
      }
    ],

  });
</script>

<script>
  function pengantar_naskah() {
    $("#myfrm").attr('target', '_blank');
    $("#myfrm").attr('action', "<?= BASE_URL('administrator/anri_mail_tl/lihat_pengantar_naskah_dinas_tindaklanjut/') ?>");
    $("#myfrm").submit();
  }

  function lihat_naskah() {

    if ($("#Jumlah").val() == "") {
      alert("Jumlah Lampiran Wajib Diisi");
      myfrm.Jumlah.focus();
    } else if ($("#ClId").val() == "") {
      alert("Kode Klasifikasi Wajib Diisi");
      myfrm.ClId.focus();
    } else if ($("#SifatId").val() == "") {
      alert("Sifat Naskah Wajib Diisi");
      myfrm.SifatId.focus();
    } else if ($("#Hal").val() == "") {
      alert("Hal Wajib Diisi");
      myfrm.Hal.focus();
    } else if ($("#RoleId_To").val() == null) {
      alert("Tujuan Naskah Wajib Diisi");
      myfrm.RoleId_To.focus();
    } else {
      var tipe = $("#xdt2").val();
      var nid = $("#xdt1").val();
      $("#myfrm").attr('target', '_blank');
      $("#myfrm").attr('action', "<?= BASE_URL('administrator/anri_mail_tl/lihat_naskah_dinas_tindaklanjut/') ?>" + tipe + '/' + nid + '/');
      $("#myfrm").submit();
    }

  }

  function simpan_naskah() {

    if ($("#Jumlah").val() == "") {
      alert("Jumlah Lampiran Wajib Diisi");
      myfrm.Jumlah.focus();
    } else if ($("#ClId").val() == "") {
      alert("Kode Klasifikasi Wajib Diisi");
      myfrm.ClId.focus();
    } else if ($("#SifatId").val() == "") {
      alert("Sifat Naskah Wajib Diisi");
      myfrm.SifatId.focus();
    } else if ($("#Hal").val() == "") {
      alert("Hal Wajib Diisi");
      myfrm.Hal.focus();
    } else if ($("#RoleId_To").val() == null) {
      alert("Tujuan Naskah Wajib Diisi");
      myfrm.RoleId_To.focus();
    } else {
      $("#myfrm").attr('target', '_self');
      $("#myfrm").attr('action', "<?= base_url('administrator/anri_mail_tl/post_naskah_dinas_tindaklanjut/'); ?>");
      $("#myfrm").submit();
    }

  }


  $('.berkas').change(function() {
    if ($(this).val() != '') {
      var path = $(this).find(':selected').data('folder');
      if ($(this).find(':selected').data('ext') == 'pdf') {
        $('.hapus').remove();
        $('.body-berkas').append('<iframe src="<?= site_url('FilesUploaded/'); ?>' + $(this).find(':selected').val() + '" width="100%" height="800px" class="hapus">');
        $('.body-button').append('<a href="<?= site_url('FilesUploaded/'); ?>' + $(this).find(':selected').val() + '" class="btn btn-info hapus" download>DOWNLOAD</a>');
      } else {
        $('.hapus').remove();
        $('.body-button').append('<a href="<?= site_url('FilesUploaded/'); ?>' + $(this).find(':selected').val() + '" class="btn btn-info hapus" download>DOWNLOAD</a>');
      }
    }
  });

  var params = {};
  params[csrf] = token;
  $('.upload_teruskan').fineUploader({
    template: 'qq-template-gallery',
    request: {
      endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
      params: params
    },
    deleteFile: {
      enabled: true,
      endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
    },
    thumbnails: {
      placeholders: {
        waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
        notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
      }
    },
    validation: {
      allowedExtensions: ["*"],
      sizeLimit: 0,

    },
    showMessage: function(msg) {
      toastr['error'](msg);
    },
    callbacks: {
      onComplete: function(id, name, xhr) {
        if (xhr.success) {
          var uuid = $('.upload_teruskan').fineUploader('getUuid', id);
          $('.upload_teruskan_listed').append('<input type="hidden" class="listed_file_uuid" name="upload_teruskan_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="upload_teruskan_name[' + id + ']" value="' + xhr.uploadName + '" />');
        } else {
          toastr['error'](xhr.error);
        }
      },
      onDeleteComplete: function(id, xhr, isError) {
        if (isError == false) {
          $('.upload_teruskan_listed').find('.listed_file_uuid[name="upload_teruskan_uuid[' + id + ']"]').remove();
          $('.upload_teruskan_listed').find('.listed_file_name[name="upload_teruskan_name[' + id + ']"]').remove();
        }
      }
    }
  }); /*end title galery*/
  $('.upload_notadinas').fineUploader({
    template: 'qq-template-gallery',
    request: {
      endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
      params: params
    },
    deleteFile: {
      enabled: true,
      endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
    },
    thumbnails: {
      placeholders: {
        waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
        notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
      }
    },
    validation: {
      allowedExtensions: ["*"],
      sizeLimit: 0,

    },
    showMessage: function(msg) {
      toastr['error'](msg);
    },
    callbacks: {
      onComplete: function(id, name, xhr) {
        if (xhr.success) {
          var uuid = $('.upload_notadinas').fineUploader('getUuid', id);
          $('.upload_notadinas_listed').append('<input type="hidden" class="listed_file_uuid" name="upload_notadinas_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="upload_notadinas_name[' + id + ']" value="' + xhr.uploadName + '" />');
        } else {
          toastr['error'](xhr.error);
        }
      },
      onDeleteComplete: function(id, xhr, isError) {
        if (isError == false) {
          $('.upload_notadinas_listed').find('.listed_file_uuid[name="upload_notadinas_uuid[' + id + ']"]').remove();
          $('.upload_notadinas_listed').find('.listed_file_name[name="upload_notadinas_name[' + id + ']"]').remove();
        }
      }
    }
  }); /*end title galery*/
  $('.upload_naskahdinas').fineUploader({
    template: 'qq-template-gallery',
    request: {
      endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
      params: params
    },
    deleteFile: {
      enabled: true,
      endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
    },
    thumbnails: {
      placeholders: {
        waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
        notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
      }
    },
    validation: {
      allowedExtensions: ["*"],
      sizeLimit: 0,

    },
    showMessage: function(msg) {
      toastr['error'](msg);
    },
    callbacks: {
      onComplete: function(id, name, xhr) {
        if (xhr.success) {
          var uuid = $('.upload_naskahdinas').fineUploader('getUuid', id);
          $('.upload_naskahdinas_listed').append('<input type="hidden" class="listed_file_uuid" name="upload_naskahdinas_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="upload_naskahdinas_name[' + id + ']" value="' + xhr.uploadName + '" />');
        } else {
          toastr['error'](xhr.error);
        }
      },
      onDeleteComplete: function(id, xhr, isError) {
        if (isError == false) {
          $('.upload_naskahdinas_listed').find('.listed_file_uuid[name="upload_naskahdinas_uuid[' + id + ']"]').remove();
          $('.upload_naskahdinas_listed').find('.listed_file_name[name="upload_naskahdinas_name[' + id + ']"]').remove();
        }
      }
    }
  }); /*end title galery*/
  $('.upload_disposisi').fineUploader({
    template: 'qq-template-gallery',
    request: {
      endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/upload_naskah_masuk_file',
      params: params
    },
    deleteFile: {
      enabled: true,
      endpoint: BASE_URL + '/administrator/anri_reg_naskah_masuk/delete_naskah_masuk_file',
    },
    thumbnails: {
      placeholders: {
        waitingPath: BASE_URL + '/asset/fine-upload/placeholders/waiting-generic.png',
        notAvailablePath: BASE_URL + '/asset/fine-upload/placeholders/not_available-generic.png'
      }
    },
    validation: {
      allowedExtensions: ["*"],
      sizeLimit: 0,

    },
    showMessage: function(msg) {
      toastr['error'](msg);
    },
    callbacks: {
      onComplete: function(id, name, xhr) {
        if (xhr.success) {
          var uuid = $('.upload_disposisi').fineUploader('getUuid', id);
          $('.upload_disposisi_listed').append('<input type="hidden" class="listed_file_uuid" name="upload_disposisi_uuid[' + id + ']" value="' + uuid + '" /><input type="hidden" class="listed_file_name" name="upload_disposisi_name[' + id + ']" value="' + xhr.uploadName + '" />');
        } else {
          toastr['error'](xhr.error);
        }
      },
      onDeleteComplete: function(id, xhr, isError) {
        if (isError == false) {
          $('.upload_disposisi_listed').find('.listed_file_uuid[name="upload_disposisi_uuid[' + id + ']"]').remove();
          $('.upload_disposisi_listed').find('.listed_file_name[name="upload_disposisi_name[' + id + ']"]').remove();
        }
      }
    }
  }); /*end title galery*/
</script>