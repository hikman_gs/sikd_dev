<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Log Passphrase</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  </head>
  <body>
  <section class="section">
    <div class="container">
    	 <ol class="breadcrumb">
      <li><a href="<?= site_url('administrator/anri_dashboard/'); ?>"><i class="fa fa-dashboard"></i> Beranda    /</a>Log Passphrase</li>
      <li class="active"><?= $title; ?></li>
   </ol>
    	<h2>Data Log Passphrase</h2>
        <p></p>
      <table class="table is-narrow" id="tabeluser">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama_Lengkap</th>
                    <th>Jam_Akses</th>
                    <th>Keterangan</th>
                    <th>Log_Desc</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $no = 1;
                foreach ($sesi_pasphrase as $u) :
                    ?>
                    <tr>
                        <td><?= $no++; ?></td>
                        <td><?= $u['nama_lengkap'] ?></td>
                        <td><?= $u['jam_akses'] ?></td>
                         <td><?= $u['keterangan'] ?></td>
                        <td><?= $u['log_desc'] ?></td>
                        
                      
                    </tr>
                <?php
                endforeach;
                ?>
          </tbody>
      </table>
      <p class="subtitle">
        Log Passphrase <strong>sikd.jabarprov.go.id</strong>!
      </p>
    </div>
  </section>
  </body>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function() {
      $('#tabeluser').DataTable();
  });
  </script>
</html>