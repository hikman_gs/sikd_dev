<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Master_model extends CI_Model {
    // var $table = 'siap_unit_kerja';
    var $column_order = array('satuan_kerja_id', 'unit_kerja_id', 'unit_kerja_nama');
    var $column_search = array('satuan_kerja_id', 'unit_kerja_id', 'unit_kerja_nama');
    var $order = array('unit_kerja_id' => 'asc');
 
    public function __construct() {

        parent::__construct();
        // $this->load->database();
    }
 
    private function get_mappingperpd_query($where) {
		$this->db->from('mapping_per_pd');
        $this->db->where('satuan_kerja_id', $where);
        $this->db->where('aktif', '1');
        $this->db->where('unit_kerja_parent IS NOT NULL');
		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_POST['search']['value']) {
			if ($i === 0) {
				$this->db->group_start();
				$this->db->like($item, $_POST['search']['value']);
			} else {
				$this->db->or_like($item, $_POST['search']['value']);
			}
			if (count($this->column_search) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		}


    function get_mappingpd($where) {
		$this->get_mappingperpd_query($where);
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

    function mapping_filtered($where) {
		$this->get_mappingperpd_query($where);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function mapping_all($where) {
        $this->db->from('mapping_per_pd');
        $this->db->where('satuan_kerja_id', $where); 
        $this->db->where('aktif', '1');
        $this->db->where('unit_kerja_parent IS NOT NULL');                                                                                                                                                                                            
		return $this->db->count_all_results();
	}

    public function get_mapping($unit_kerja)
    {
        $this->db->where('unit_kerja_id', $unit_kerja);
        $query = $this->db->get('mapping_per_pd');
        return $query->row();
    }

    public function get_nama_unit($unit_kerja)
    {
        $this->db->where('unit_kerja_id', $unit_kerja);
        $query = $this->db->get('mapping_per_pd');
        return $query->row()->unit_kerja_nama;
    }

    public function get_role_uk($role_id)
    {
        $this->db->from('role');
        $this->db->where('role.RoleId', $role_id);
        $this->db->join('rolecode', 'role.RoleCode=rolecode.rolecode_id');
        $this->db->join('master_grole', 'role.GRoleId=master_grole.GRoleId');
        $query = $this->db->get();
        $data =  $query->row();
        $nama_dinas;
        if(isset($data)){
            
            $toReturn = array(
                'role_desc' => $data->RoleDesc,
                'role_id' => $data->RoleId,
                'role_parent'=> $data->RoleParentId,
                'g_jabatan' => $data->gjabatanId,
                'grole_id' => $data->GRoleId,
                'rolecode' => $data->RoleCode,
                'rolename' => $data->RoleName,
                'rolecode_name' => $data->rolecode_name,
                'grolename' => $data->GRoleName,

            );
        }
        return $toReturn;
    }

    public function get_list_grole_by_rolecode($grole_id)
    {
        // $this->db->where('GRoleId', $grole_id);
        $this->db->where('RoleCode', $grole_id);
        $gjabatan = array('XxJyPn38Yh.2', 'XxJyPn38Yh.3', 'XxJyPn38Yh.4','XxJyPn38Yh.5','XxJyPn38Yh.11');
        $this->db->where_in('gjabatanId',$gjabatan);
        $query = $this->db->get('role');
        return $query->result();
        
    }

    public function get_list_grole_by_grole_id($grole_id)
    {
        $this->db->where('GRoleId', $grole_id);
        // $this->db->where('RoleCode', $grole_id);
        $gjabatan = array('XxJyPn38Yh.2', 'XxJyPn38Yh.3', 'XxJyPn38Yh.4','XxJyPn38Yh.5','XxJyPn38Yh.11');
        $this->db->where_in('gjabatanId',$gjabatan);
        $query = $this->db->get('role');
        return $query->result();
        
    }

    public function get_detail_mapping($map)
    {
        $this->db->select('mapping_per_pd.*, satker.unit_kerja_nama as dinas');
        $this->db->from('mapping_per_pd');
        $this->db->join('mapping_per_pd satker', 'mapping_per_pd.satuan_kerja_id=satker.unit_kerja_id');
		$this->db->where('mapping_per_pd.mapping_per_pd_id', $map);
		$query = $this->db->get();
		$data = $query->row();
		if(isset($data)){
            $rolecode;
            $grole_name;
			$toReturn = array(
				'mapping_id' => $data->mapping_per_pd_id,
				'satker' => $data->satuan_kerja_id,
                'parent' => $data->unit_kerja_parent,
                'dinas' => $data->dinas,
				'uker' => $data->unit_kerja_id,
				'nama' => $data->unit_kerja_nama,
				'eselon' => $data->eselon,
                'grole_name'=> $this->get_grole_name($data->dinas_grole),
				
			);
		}
		return $toReturn;
    }

    public function get_rolecode_name($role)
    {
        $this->db->where('rolecode_id', $role);
        $query = $this->db->get('rolecode');
        return $query->row()->rolecode_name;
        // return "asdaf";
    }

    public function get_grole_name($role)
    {
        $this->db->where('GRoleId', $role);
        $query = $this->db->get('master_grole');
        return $query->row()->GRoleName;
        // return "asdaf";
    }

    public function get_role_code($map)
    {
        $this->db->where('satuan_kerja_id', $map);
        $query = $this->db->get('mapping_pd');
        return $query->row()->rolecode_id;
    }

    public function get_grole_id($map)
    {
        $this->db->where('unit_kerja_id', $map);
        $query = $this->db->get('mapping_per_pd');
        return $query->row()->GRoleId;
    }

    public function update_mapping($data, $id)
    {
        $this->db->where('mapping_per_pd_id', $id);
        $this->db->update('mapping_per_pd', $data);
        return $this->db->affected_rows();
    }

    public function update_role($data, $id)
    {
        $this->db->where('RoleId', $id);
        $this->db->update('role', $data);
        return $this->db->affected_rows();
    }

    public function cek_status_uk($role)
    {
        $this->db->where('RoleId', $role);
        $query = $this->db->get('mapping_per_pd');
        return $query;
    }

   public function get_role_desc($uk)
   {
       $this->db->where('RoleId', $uk);
       $query = $this->db->get('role');
       return $query->row()->RoleDesc;
   }

}