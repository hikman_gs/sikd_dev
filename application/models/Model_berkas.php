<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Model_berkas extends MY_Model {
	var $table = 'berkas'; //nama tabel dari database
	var $column_order = array(null, 'BerkasStatus','BerkasNumber','BerkasName','BerkasDesc','PeopleName','RetensiValue_Active',null); //field yang ada di table user
	var $column_search = array('BerkasStatus','BerkasNumber','BerkasName','BerkasDesc','PeopleName','RetensiValue_Active'); //field yang diizin untuk pencarian 
	var $order = array('BerkasNumber' => 'DESC'); // default order 


	private $primary_key 	= 'BerkasId';

	private $table_name 	= 'berkas';

	private $field_search 	= ['BerkasKey','BerkasId','RoleId','ClId','Klasifikasi','BerkasNumber','BerkasName','RetensiTipe_Active','RetensiValue_Active','RetensiTipe_InActive','RetensiValue_InActive','SusutId','BerkasLokasi','BerkasDesc','CreatedBy','CreationDate','BerkasStatus','BerkasCountSince','KetAkhir'];



	public function __construct()
	{

		$config = array(

			'primary_key' 	=> $this->primary_key,

		 	'table_name' 	=> $this->table_name,

		 	'field_search' 	=> $this->field_search,

		 );



		parent::__construct($config);
		$this->load->database();

	}

    public function get_data_naskah_belum_berkas()
    {
        //$xx = get_field_people('RoleId');
        /*$this->datatables->select('UrgensiName,Nomor,Hal,Instansipengirim,Tgl,NTglReg');
        $this->datatables->from('v_log_masuk');
        //$this->datatables->where('CreationRoleId', $xx);
        $this->datatables->where('NTipe', 'inbox');*/
        $this->datatables->select('NId,UrgensiName,Nomor,Hal,Instansipengirim,Tgl,NTglReg');
        $this->datatables->from('inbox');
        $this->datatables->join('master_urgensi','master_urgensi.UrgensiId=inbox.UrgensiId');
        $this->datatables->where('BerkasId', '0');
        $this->datatables->add_column('view', '<a href="'.site_url('administrator/anri_dashboard/view_naskah_berkas/view/$1').'" title="Lihat Detail Naskah" class="btn btn-primary"><i class="fa fa-edit"></i></a> <a href="'.site_url('administrator/anri_dashboard/iframe_histori/$1').'" target="_new" title="Lihat Histori Naskah" class="btn btn-warning"><i class="fa fa-list"></i></a>','NId');
        //$this->datatables->add_column('btn_berkas','<button type="button" class="btn btn-default open-NId" data-toggle="modal" data-NId="$1" data-target="#berkas"><i class="fa fa-plus"></i></button>','NId');
        //$this->datatables->add_column('btn_berkas','<button type="button" class="btn btn-default open-NId" data-toggle="modal" data-NId="$1" data-target="#berkas"><i class="fa fa-plus"></i></button>','NId');
        return $this->datatables->generate();
    }

	public function get_data()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('BerkasNumber !='=>'0'));
        $this->datatables->add_column('view', '<a class="btn-primary btn-sm btn" href="'.site_url("administrator/anri_dashboard/berkas_unit_update/$1").'"><i class="fa fa-edit" ></i></a> <a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_belum_diberkaskan()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('BerkasNumber !='=>'0'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_arsip_aktif()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('BerkasNumber !='=>'0'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_arsip_inaktif()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('BerkasNumber !='=>'0'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_melewati_aktif()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('RetensiValue_Active <=' => date('Y-m-d'), 'BerkasNumber !=' => '0'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_melewati_inaktif()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('RetensiValue_InActive <=' => date('Y-m-d'), 'BerkasNumber !=' => '0'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_usul_musnah()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('RetensiValue_InActive <='=>date('Y-m-d'), 'BerkasStatus'=>'close', 'BerkasNumber !=','0', 'SusutId' => 'XxJyPn38Yh.1'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_musnah()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('BerkasNumber !='=>'0'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_usul_serah()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('RetensiValue_InActive <='=>date('Y-m-d'), 'BerkasStatus'=>'close', 'BerkasNumber !=','0', 'SusutId !=' => 'XxJyPn38Yh.1'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

    public function get_data_serah()
    {
    	$tb_key = tb_key();
    	$this->datatables->select('BerkasId,BerkasStatus,BerkasNumber,BerkasName,BerkasDesc,PeopleName,RetensiValue_Active');
        $this->datatables->from('berkas');
        $this->datatables->join('people','people.PeopleId=berkas.CreatedBy');
        $this->datatables->where('BerkasKey',$tb_key);
        $this->datatables->where(array('BerkasNumber !='=>'0'));
        $this->datatables->add_column('view', '<a href="javascript:void(0);" class="btn-danger btn-sm btn remove-data" data-href="'.site_url("administrator/anri_dashboard/berkas_unit_delete/$1").'"><i class="fa fa-trash" ></i></a>','BerkasId');
        $this->datatables->add_column('status_berkas','<a href="'.BASE_URL('administrator/anri_dashboard/berkas_unit_update_status?id=$1&st=closed').'"><span class="label label-primary">$2</span></a>', 'BerkasId,BerkasStatus');
        return $this->datatables->generate();
    }

	public function count_all($q = null, $field = null)

	{

		$iterasi = 1;

        $num = count($this->field_search);

        $where = NULL;

        $q = $this->scurity($q);

		$field = $this->scurity($field);



        if (empty($field)) {

	        foreach ($this->field_search as $field) {

	            if ($iterasi == 1) {

	                $where .= "berkas.".$field . " LIKE '%" . $q . "%' ";

	            } else {

	                $where .= "OR " . "berkas.".$field . " LIKE '%" . $q . "%' ";

	            }

	            $iterasi++;

	        }



	        $where = '('.$where.')';

        } else {

        	$where .= "(" . "berkas.".$field . " LIKE '%" . $q . "%' )";

        }



		$this->join_avaiable()->filter_avaiable();

        $this->db->where($where);

		$query = $this->db->get($this->table_name);



		return $query->num_rows();

	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	{

		$iterasi = 1;

        $num = count($this->field_search);

        $where = NULL;

        $q = $this->scurity($q);

		$field = $this->scurity($field);



        if (empty($field)) {

	        foreach ($this->field_search as $field) {

	            if ($iterasi == 1) {

	                $where .= "berkas.".$field . " LIKE '%" . $q . "%' ";

	            } else {

	                $where .= "OR " . "berkas.".$field . " LIKE '%" . $q . "%' ";

	            }

	            $iterasi++;

	        }



	        $where = '('.$where.')';

        } else {

        	$where .= "(" . "berkas.".$field . " LIKE '%" . $q . "%' )";

        }



        if (is_array($select_field) AND count($select_field)) {

        	$this->db->select($select_field);

        }

		

		$this->join_avaiable()->filter_avaiable();

        $this->db->where($where);

        $this->db->limit($limit, $offset);

        $this->db->order_by('berkas.'.$this->primary_key, "DESC");

		$query = $this->db->get($this->table_name);



		return $query->result();

	}


    public function join_avaiable() {
        return $this;
    }
    public function filter_avaiable() {
    	return $this;
    }

    

    


}



/* End of file Model_berkas.php */

/* Location: ./application/models/Model_berkas.php */