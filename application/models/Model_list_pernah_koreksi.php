<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_list_pernah_koreksi extends CI_Model {

	var $table = 'inbox_receiver_koreksi'; //nama tabel dari database
	var $column_order = array(null, 'ReceiverAs','NId','GIR_Id','id_koreksi','JenisId',null); //field yang ada di table user
	var $column_search = array('ReceiverAs','NId','GIR_Id','id_koreksi'); //field yang diizin untuk pencarian 
	var $order = array('StatusReceive' => 'DESC'); // default order 
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{


		 if($this->session->userdata('groupid')==7) {

		 	$xx1= $this->session->userdata('peopleid');
			$xx= $this->session->userdata('primaryroleid');
			$wheres = ['RoleId_From' => $xx, 'From_Id'=>$xx1];
			$names = array('to_koreksi');	


		}else{
			

		 	$xx= $this->session->userdata('primaryroleid');
			$wheres = ['RoleId_From' => $xx];
			$names = array('to_koreksi');		



		}
			
			

		
		$this->db->where_in('ReceiverAs', $names);
		$this->db->where($wheres);	
		$this->db->group_by('NId');// add group_by
		$this->db->from($this->table);
		
		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				
				if(count($this->column_search) - 1 == $i) //last loop
				$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}

		$this->db->order_by('StatusReceive', 'DESC');
	}

	public function get_datatables($limit = -1, $start = 0)
	{
		$this->_get_datatables_query();

		if($limit != -1) {
			$this->db->limit($limit, $start);
		}
		
		$query = $this->db->get();
	
		return $query->result();
	}

	public function count_filtered()
	{
		$this->db->select('NId');
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{

		// $xx= $this->session->userdata('primaryroleid');
		// $wheres = ['RoleId_From' => $xx];
		// $names = array('to_koreksi');	



		 if($this->session->userdata('groupid')==7) {

		 	$xx1= $this->session->userdata('peopleid');
			$xx= $this->session->userdata('primaryroleid');
			$wheres = ['RoleId_From' => $xx, 'From_Id'=>$xx1];
			$names = array('to_koreksi');	


		}else{
			
			
		 	$xx= $this->session->userdata('primaryroleid');
			$wheres = ['RoleId_From' => $xx];
			$names = array('to_koreksi');		

				

		}	

		$this->db->distinct('Nid');
		$this->db->where_in('ReceiverAs', $names);
		$this->db->where($wheres);	
		$this->db->from($this->table);

	$query=	$this->db->select('NId');
		return $query->count_all_results();
	}

}

/* End of file Model_list_naskah_teruskan_approve.php */
/* Location: ./application/models/Model_list_naskah_teruskan_approve.php */