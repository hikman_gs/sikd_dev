<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_list_suratedaran_btl extends CI_Model {

	var $table = 'v_tem_suratedaran'; //nama tabel dari database
	var $column_order = array(null, 'GIR_Id','Tgl','RoleId_From','Hal', null); //field yang ada di table user
	var $column_search = array('GIR_Id','Tgl','RoleId_From','Hal'); //field yang diizin untuk pencarian 
	var $order = array('Tgl' => 'DESC'); // default order 
	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	private function _get_datatables_query()
	{

		$xx = $this->session->userdata('roleid');
		$wheres = ['Status' => '0', 'RoleId_To' => $xx];

		$this->db->where($wheres);	
		$this->db->from($this->table);
		
		$i = 0;

		foreach ($this->column_search as $item) // loop column 
		{
			if($_POST['search']['value']) // if datatable send POST for search
			{
				if($i===0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				}
				else
				{
					$this->db->or_like($item, $_POST['search']['value']);
				}
				
				if(count($this->column_search) - 1 == $i) //last loop
				$this->db->group_end(); //close bracket
			}
			$i++;
		}

		if(isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} 
		else if(isset($this->order))
		{
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}

		$this->db->order_by('Tgl', 'DESC');
	}

	public function get_datatables($limit = -1, $start = 0)
	{
		$this->_get_datatables_query();

		if($limit != -1) {
			$this->db->limit($limit, $start);
		}
		
		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered()
	{
		$this->db->select('NId');
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all()
	{
		$xx = $this->session->userdata('roleid');
		$wheres = ['Status' => '0', 'RoleId_To' => $xx];

		$this->db->where($wheres);	
		$this->db->from($this->table);

		$this->db->select('NId');
		return $this->db->count_all_results();
	}
}

/* End of file Model_list_suratedaran_btl.php */
/* Location: ./application/models/Model_list_suratedaran_btl.php */