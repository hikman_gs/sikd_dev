<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_list_keterangan extends CI_Model {
	 // start datatables
    // var $column_order = array(null, 'notice_name', 'notice_description', 'tgl_awal', 'tgl_akhir'); //set column field database for datatable orderable
    // var $column_search = array('notice_name', 'notice_description', 'tgl_awal', 'tgl_akhir'); //set column field database for datatable searchable
    // var $order = array('notice_id' => 'desc'); // default order 

    var $column_order = array(null, 'GIR_Id','Tgl','RoleId_From','Hal', null); //field yang ada di table user
	var $column_search = array('GIR_Id','Tgl','RoleId_From','Hal'); //field yang diizin untuk pencarian 
	var $order = array('Tgl' => 'DESC'); // default order 

    private $primary_key 	= 'NId';
	private $table_name 	= 'v_tem_sket';
	private $field_search 	= ['GIR_Id','Tgl','RoleId_From','Hal'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}
 
    private function _get_datatables_query() {
    	// $xx = $this->session->userdata('peopleid');
		// $wheres = ['Status' => '0', 'RoleId_To' => $xx];
		if($this->session->userdata('groupid')==7){
            $xx = $this->session->userdata('roleid');
            $xx1 = $this->session->userdata('peopleid');
            $wheres = ['Status' => '0', 'To_Id'=> $xx1, 'RoleId_To' => $xx];
        }else{
            $xx     = $this->session->userdata('roleid');
            $wheres = ['Status' => '0', 'RoleId_To' => $xx];
        }
		
		$this->db->where($wheres);	
        $this->db->select('v_tem_sket.*');
        $this->db->from('v_tem_sket');
        // $this->db->join('p_category', 'p_item.category_id = p_category.category_id');
        // $this->db->join('p_unit', 'p_item.unit_id = p_unit.unit_id');
        $i = 0;
        foreach ($this->column_search as $item) { // loop column 
            if(@$_POST['search']['value']) { // if datatable send POST for search
                if($i===0) { // first loop
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
        
        if(isset($_POST['order'])) { // here order processing
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }  else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
    function get_datatables() {
        $this->_get_datatables_query();
        if(@$_POST['length'] != -1)
        $this->db->limit(@$_POST['length'], @$_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
    function count_filtered() {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
    function count_all() {
        // $this->db->from('v_tem_sket');
        // return $this->db->count_all_results();
		if($this->session->userdata('groupid')==7){
            $xx = $this->session->userdata('roleid');
            $xx1 = $this->session->userdata('peopleid');
            $wheres = ['Status' => '0', 'To_Id'=>  $xx1, 'RoleId_To' => $xx];
            $names = array('to_sket');

        }else{

            $xx     = $this->session->userdata('roleid');
            $wheres = ['Status' => '0', 'RoleId_To' => $xx];
            $names = array('to_sket');

        }
        
        $this->db->where_in('ReceiverAs', $names);
        $this->db->where($wheres);  
        $this->db->from('v_tem_sket');
        //akhir penambahan

        $this->db->select('NId');
        return $this->db->count_all_results();


    }
    // end datatables


	// public function count_all($q = null, $field = null)
	// {
	// 	$iterasi = 1;
 //        $num = count($this->field_search);
 //        $where = NULL;
 //        $q = $this->scurity($q);
	// 	$field = $this->scurity($field);

 //        if (empty($field)) {
	//         foreach ($this->field_search as $field) {
	//             if ($iterasi == 1) {
	//                 $where .= "master_notice.".$field . " LIKE '%" . $q . "%' ";
	//             } else {
	//                 $where .= "OR " . "master_notice.".$field . " LIKE '%" . $q . "%' ";
	//             }
	//             $iterasi++;
	//         }

	//         $where = '('.$where.')';
 //        } else {
 //        	$where .= "(" . "master_notice.".$field . " LIKE '%" . $q . "%' )";
 //        }

	// 	$this->join_avaiable()->filter_avaiable();
 //        $this->db->where($where);
	// 	$query = $this->db->get($this->table_name);

	// 	return $query->num_rows();
	// }

	// public function get($q = null, $field = null, $limit = 0, $offset = 0, $select_field = [])
	// {
	// 	$iterasi = 1;
 //        $num = count($this->field_search);
 //        $where = NULL;
 //        $q = $this->scurity($q);
	// 	$field = $this->scurity($field);

 //        if (empty($field)) {
	//         foreach ($this->field_search as $field) {
	//             if ($iterasi == 1) {
	//                 $where .= "master_notice.".$field . " LIKE '%" . $q . "%' ";
	//             } else {
	//                 $where .= "OR " . "master_notice.".$field . " LIKE '%" . $q . "%' ";
	//             }
	//             $iterasi++;
	//         }

	//         $where = '('.$where.')';
 //        } else {
 //        	$where .= "(" . "master_notice.".$field . " LIKE '%" . $q . "%' )";
 //        }

 //        if (is_array($select_field) AND count($select_field)) {
 //        	$this->db->select($select_field);
 //        }
		
	// 	$this->join_avaiable()->filter_avaiable();
 //        $this->db->where($where);
 //        $this->db->limit($limit, $offset);
 //        $this->db->order_by('master_notice.'.$this->primary_key, "DESC");
	// 	$query = $this->db->get($this->table_name);

	// 	return $query->result();
	// }

 //    public function join_avaiable() {
        
 //        return $this;
 //    }

 //    public function filter_avaiable() {
        
 //        return $this;
 //    }

}

/* End of file Model_master_notice.php */
/* Location: ./application/models/Model_master_notice.php */