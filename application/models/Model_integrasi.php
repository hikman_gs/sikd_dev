<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_integrasi extends MY_Model {

    var $column_order = array('peg_nama', 'unit_kerja_nama', 'pangkat', 'kedudukan_pns');
    var $column_search = array('peg_nama', 'unit_kerja_nama', 'pangkat', 'kedudukan_pns');
    var $order = array('peg_nama' => 'asc');

    public function get_total_siap_pegawai(){
        $query = $this->db->get('siap_pegawai');
        return $query->num_rows();
        // echo "asdf";
    }

    public function cek_nip($nip){
        $this->db->where('peg_nip', $nip);
        $query = $this->db->get('siap_pegawai');
        return $query->num_rows();
    }

    public function get_all_nip()
    {
        $this->db->select('peg_nip');
        $query = $this->db->get('siap_pegawai');
        return $query->result_array();
    }

    public function cek_jabatan($jabatan_id){
        $query = $this->db->get_where('siap_jabatan', array('jabatan_id'=>$jabatan_id));
        return $query->num_rows();
    }

    public function cek_unit_kerja($unit_kerja_id)
    {
        $query = $this->db->get_where('siap_unit_kerja', array('unit_kerja_id'=>$unit_kerja_id));
        return $query->num_rows();
    }  
    
    public function cek_id_jabatan($jabatan_id)
    {
        $query = $this->db->get_where('siap_jabatan', array('jabatan_id'=> $jabatan_id));
        return $query->num_rows();
    }

    public function get_satker_pd()
    {
        $this->db->select('satuan_kerja_id');
        $query = $this->db->get('mapping_pd');
        return $query->result();
    }

    public function get_unitkerja_perpd()
    {
        $this->db->select('unit_kerja_id');
        $query = $this->db->get('mapping_per_pd');
        return $query->result();
    }

    public function cek_pd($satuan_kerja_id)
    {
        $query = $this->db->get_where('mapping_pd', array('satuan_kerja_id'=>$satuan_kerja_id));
        return $query->num_rows();
    }

    public function cek_per_pd($unit_kerja_id)
    {
        $query = $this->db->get_where('mapping_per_pd', array('unit_kerja_id'=>$unit_kerja_id));
        return $query->num_rows();
    }

    public function cek_perpd($unit_kerja_id)
    {
        $query = $this->db->get_where('mapping_per_pd', array('satuan_kerja_id'=>$unit_kerja_id));
        return $query->num_rows();
    }
    
    public function replace_pegawai($data)
    {
        $this->db->replace('siap_pegawai', $data);
        return $this->db->affected_rows();
    }

    public function tambah_jabatan($data)
    {
        $this->db->insert('siap_jabatan', $data);
        return $this->db->affected_rows();
    }

    public function tambah_pegawai_batch($temp)
    {
        // echo 'here';
        $this->db->insert_batch('siap_pegawai', $temp);
        return $this->db->affected_rows();
    }

    public function update_pegawai_batch($temp)
    {
        // echo 'here';
        $this->db->update_batch('siap_pegawai', $temp, 'peg_nip');
        return $this->db->affected_rows();
    }

    public function update_jabatan_batch($temp)
    {
        // echo 'here';
        $this->db->update_batch('siap_jabatan', $temp, 'jabatan_id');
        return $this->db->affected_rows();
    }

    public function update_jabatan_single($temp, $id_jabatan)
    {
        // echo 'here';
        $this->db->where('jabatan_id',$id_jabatan);
        $this->db->update('siap_jabatan', $temp);
        return $this->db->affected_rows();
    }

    public function tambah_mapping_pd($data)
    {
        $this->db->insert('mapping_pd', $data);
        return $this->db->affected_rows();
    }

    public function update_mapping_pd($where, $data)
    {
        $this->db->where('mapping_pd_id', $where);
        $this->db->update('mapping_pd', $data);
        $this->db->affected_rows();
    }

    public function update_status_pd($where, $data)
    {
        $this->db->where_in('satuan_kerja_id', $where);
        $this->db->update('mapping_pd', $data);

    }

    public function update_status_perpd($where, $data)
    {
        $this->db->where_in('unit_kerja_id', $where);
        $this->db->update('mapping_per_pd', $data);
    }

    public function tambah_mapping_perpd($data)
    {
        $this->db->insert('mapping_per_pd', $data);
        return $this->db->affected_rows();
    }

    public function get_id_bysatker($satuan_kerja)
    {
        $this->db->where('satuan_kerja_id', $satuan_kerja);
        $query = $this->db->get('mapping_pd');
        return $query->row()->mapping_pd_id;
    }

    public function get_total_unit_kerja()
    {
        $query = $this->db->get('siap_unit_kerja');
        return $query->num_rows();
    }

    public function get_total_opd()
    {
        $query = $this->db->get('mapping_pd');
        return $query->num_rows();
    }

    public function update_unit_batch($data)
    {
        $this->db->update_batch('siap_unit_kerja', $data, 'unit_kerja_id');
        return $this->db->affected_rows();
    }

    public function tambah_unit_kerja($data)
    {
        $this->db->insert('siap_unit_kerja', $data);
        return $this->db->affected_rows();
    }

    public function update_opd_batch($data)
    {
        $this->db->update_batch('mapping_pd', $data, 'satuan_kerja_id');
        return $this->db->affected_rows();
    }
    
    public function update_perpd_batch($data)
    {
        $this->db->update_batch('mapping_per_pd', $data, 'unit_kerja_id');
        return $this->db->affected_rows();
    }

    public function update_perpd($data, $id)
    {
        $this->db->where('unit_kerja_id', $id);
        $this->db->update_batch('mapping_per_pd', $data);
        return $this->db->affected_rows();
    }

    public function get_mappd()
    {
        $this->db->where('aktif',1);
        $this->db->join('rolecode','mapping_pd.rolecode_id = rolecode.rolecode_id');
        $query = $this->db->get('mapping_pd');
        return $query->result();
    }

    public function get_grole()
    {
        $query = $this->db->get('master_grole');
        return $query->result_array();
    }

    public function get_rolecode()
    {
        $query = $this->db->get('rolecode');
        return $query->result_array();
    }

    public function get_grolename($id)
    {
        $this->db->where('GRoleId', $id);
        $query = $this->db->get('master_grole');
        return $query->row()->GRoleName;
    }

    public function get_eselon($unit_kerja_id)
    {
        $this->db->where('unit_kerja_id', $unit_kerja_id);
        $query = $this->db->get('siap_unit_kerja');
        return $query->row()->eselon;
    }

    private function get_siap_pegawai_query($where) {
		$this->db->from('siap_pegawai');
        $this->db->where('satuan_kerja_id', $where);
		$i = 0;
		foreach ($this->column_search as $item) {
			if ($_POST['search']['value']) {
			if ($i === 0) {
				$this->db->group_start();
				$this->db->like($item, $_POST['search']['value']);
			} else {
				$this->db->or_like($item, $_POST['search']['value']);
			}
			if (count($this->column_search) - 1 == $i)
				$this->db->group_end();
			}
			$i++;
		}
		if (isset($_POST['order'])) {
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
		}


    function get_siap_pegawai($where) {
		$this->get_siap_pegawai_query($where);
		if ($_POST['length'] != -1)
			$this->db->limit($_POST['length'], $_POST['start']);
		$query = $this->db->get();
		return $query->result();
	}

    function siap_pegawai_filtered($where) {
		$this->get_siap_pegawai_query($where);
		$query = $this->db->get();
		return $query->num_rows();
	}
	
	public function siap_pegawai_all($where) {
		$this->db->from('siap_pegawai');
		$this->db->where('satuan_kerja_id', $where);                                                                                                                                                                                               
		return $this->db->count_all_results();
	}

    public function cek_username($nip)
    {
        $this->db->where('PeopleUsername', $nip);
        $query = $this->db->get('people');
        return $query->num_rows();
    }

    public function get_pegawai_bynip($where)
    {
        $this->db->where('peg_nip', $where);
        $query = $this->db->get('siap_pegawai');
        return $query->row();
    }

    public function people_last_id()
    {
        $this->db->order_by('PeopleId', 'desc');
        $this->db->limit(1);
        $query = $this->db->get('people');
        return $query->row()->PeopleId;
    }

    public function get_role_pd($satuan_kerja_id, $unit_kerja_id)
    {   
        $unit_kerja_id==''? $unit_kerja_id=$satuan_kerja_id:$unit_kerja_id;
        $this->db->where(array('satuan_kerja_id'=> $satuan_kerja_id, 'unit_kerja_id'=> $unit_kerja_id));
        $query = $this->db->get('mapping_per_pd');
        return $query->row()->RoleId;
    }

    public function tambah_people($data)
    {
        $this->db->insert('people', $data);
        return $this->db->affected_rows();
    }

    public function get_detail_pegawai($nip)
	{
		$this->db->from('siap_pegawai');
        $this->db->join('mapping_per_pd', 'siap_pegawai.unit_kerja_id=mapping_per_pd.unit_kerja_id');
		$this->db->where('peg_nip', $nip);
		$query = $this->db->get();
		$data = $query->row();
		if(isset($data)){
			$toReturn = array(
				'nip' => $data->peg_nip,
				'nama' => $data->peg_nama,
				'pd' => $data->satuan_kerja_nama,
				'jabatan' => $data->jabatan_nama,
				'atasan' => $data->nip_atasan,
				'email' => $data->peg_email,
                'role_id' => $data->RoleId,
                'role_parent_id'=> $data->RoleParentId,
                'jabatan_jenis' => $data->jabatan_jenis,
                'nik' => $data->peg_ktp,
                'pangkat' => $data->pangkat,
                'eselon' => $data->eselon_nm,
			);
		}
		return $toReturn;
	}

    public function get_group()
    {
        $this->db->order_by('GroupId', 'asc');
        $query = $this->db->get('groups');
        return $query->result_array();
    }

    public function get_role($where)
    {
        $this->db->where('RoleParentId', $where);
        $this->db->order_by('RoleParentId', 'asc');
        $query = $this->db->get('role');
        return $query->result_array();
    }

    public function get_role_pegawai($where)
    {
        $this->db->where('RoleParentId', $where);
        // $this->db->order_by('RoleParentId', 'asc');
        $query = $this->db->get('role');
        if($query->num_rows()<1){
            $this->db->where('RoleId', $where);
            $query = $this->db->get('role');
            return $query->row()->RoleId;
        }else{
            return $query->row()->RoleId;
        }
        
    }

    public function get_role_atasan($role)
    {
        $this->db->where('RoleId', $role);
        $query = $this->db->get('role');
        return $query->row()->RoleParentId;
    }

    public function pegawai_non_aktif($data)
    {
        // $this->db->where_in($where);
        $this->db->update('siap_pegawai', $data);
        return $this->db->affected_rows();
    }

}

/* End of file Model_integrasi.php */
/* Location: ./application/models/Model_integrasi.php */