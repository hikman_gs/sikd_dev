<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Perdinas_model extends CI_Model {
    var $table = 'siap_pegawai';
    var $column_order  = array(null, null,null,null,null,null,null);
    var $column_search = array('peg_nama', 'peg_nip', 'jabatan_nama'); 
    var $order = array('peg_nip' => 'ASC');
 
    public function __construct() {

        parent::__construct();
        $this->load->database();
    }
 
    private function _get_datatables_query() {
        $this->db->from($this->table);
        $i = 0;
     
        foreach ($this->column_search as $item) {
            if($_POST['search']['value']) {
                 
                if($i===0) {
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                } else {
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i)
                    $this->db->group_end();
            }
            $i++;
        }
         
        if(isset($_POST['order'])) {
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        } else if(isset($this->order)) {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
 
    function get_datatables($GRoleId) {
		
		$this->db->where('satuan_kerja_id', $GRoleId);
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
 
    function count_filtered($GRoleId) {
		$this->db->where('satuan_kerja_id', $GRoleId);
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all($GRoleId) {
		$this->db->where('satuan_kerja_id', $GRoleId);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

}