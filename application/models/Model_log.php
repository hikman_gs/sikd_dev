<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_log extends CI_Model{

    function simpan_user()
    {
        $simpan_data=array(
            'nama_lengkap'  => $this->session->userdata('peoplename'),
            'jam_akses' 	=> date('Y-m-d H:i:s'), 
            'Keterangan' 	=> 'Insert Passphrase Berhasil, Data disimpan',
            'log_desc' 	    => 'sukses',
           
	   );
        $simpan = $this->db->insert('sesi_pasphrase', $simpan_data);
        return $simpan;
    }

       function gagal_user()
    {
        $simpan_data=array(
            'nama_lengkap'  => $this->session->userdata('peoplename'),
            'jam_akses' 	=> date('Y-m-d H:i:s'),
            'Keterangan' 	=> 'Insert Passphrase Gagal, Data failed',
            'log_desc' 	    => 'gagal',
           
	   );
        $simpan = $this->db->insert('sesi_pasphrase', $simpan_data);
        return $simpan;
    }
}