<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_statistik extends CI_Model {

		    public function statistik_naskahmasuk_pd(){

				        $sql = "SELECT Nid from v_penerima_pd WHERE NTipe = 'inbox' AND pengirim = 'eksternal' and ReceiverAs IN('to','bcc','to_forward') AND RoleId_To = '" . $this->session->userdata('roleid') . "'";
				            $hasil = $this->db->query($sql);
				    
				            if($hasil->num_rows()>0)
				            {
				              return $hasil->num_rows();
				            }
				            else
				            {
				              return 0;
				            }  
		        }

	        public function statistik_naskahkeluar_pd(){

		            $sql = "SELECT Nid from v_penerima_pd WHERE NTipe = 'outbox' AND pengirim = 'eksternal' and ReceiverAs IN('to','bcc','to_forward') AND RoleId_To = '" . $this->session->userdata('roleid') ."'";
		                $hasil = $this->db->query($sql);
		        
		                if($hasil->num_rows()>0)
		                {
		                  return $hasil->num_rows();
		                }
		                else
		                {
		                  return 0;
		                }  
	            }

	        public function statistik_naskah_per_pd(){

		            $sql = "SELECT Nid from v_penerima_pd WHERE NTipe = 'outbox' AND pengirim = 'eksternal' and ReceiverAs IN('to','bcc','to_forward') AND RoleCode = '" . $this->session->userdata('rolecode') ."'";
		                $hasil = $this->db->query($sql);
		        
		                if($hasil->num_rows()>0)
		                {
		                  return $hasil->num_rows();
		                }
		                else
		                {
		                  return 0;
		                }  
	            }

	        public function statistik_naskah_per_uptd(){

		            $sql = "SELECT Nid from v_penerima_pd WHERE NTipe = 'outbox' AND pengirim = 'eksternal' and ReceiverAs IN('to','bcc','to_forward') AND GroleId = '" . $this->session->userdata('groleid') ."'";
		                $hasil = $this->db->query($sql);
		        
		                if($hasil->num_rows()>0)
		                {
		                  return $hasil->num_rows();
		                }
		                else
		                {
		                  return 0;
		                }  
	            }

	        // SURAT YG DI REGIS DARI SIKD
	        public function statistik_suratbysikd(){

	                        $sql = "SELECT Nid_Temp FROM konsep_naskah where Konsep = 2";
	                            $hasil = $this->db->query($sql);
	                    
	                            if($hasil->num_rows()>0)
	                            {
	                              return $hasil->num_rows();
	                            }
	                            else
	                            {
	                              return 0;
	                            }  
	        }            
                        
	        // SURAT DARI HASIL REGUS MANUAL
		    public function statistik_suratoutsikd(){

			                $sql = "SELECT * from NTipe = 'outbox' AND pengirim = 'eksternal' and ReceiverAs IN('to','bcc','to_forward') AND RoleId_To = '" . $this->session->userdata('roleid') ."'" ;
			                $hasil = $this->db->query($sql);
			                        
			                if($hasil->num_rows()>0)
			                                {
			                  return $hasil->num_rows();
			                                }
			                                else
			                                {
			                                  return 0;
			                                }  
			}                   		


            // STATISTIK PER BULANAN / MINGGUAN / HARIAN
            public function statistik_ratarata_harian(){

                $sql = ;
                    $hasil = $this->db->query($sql);
            
                    if($hasil->num_rows()>0)
                    {
                      return $hasil->num_rows();
                    }
                    else
                    {
                      return 0;
                    }  
                }
            
                
            public function statistik_ratarata_mingguan(){

	              $sql = ;
	                  $hasil = $this->db->query($sql);
	          
	                  if($hasil->num_rows()>0)
	                  {
	                    return $hasil->num_rows();
	                  }
	                  else
	                  {
	                    return 0;
	                  }  
              }

            
              public function statistik_ratarata_bulanan(){

	                $sql = ;
	                    $hasil = $this->db->query($sql);
	            
	                    if($hasil->num_rows()>0)
	                    {
	                      return $hasil->num_rows();
	                    }
	                    else
	                    {
	                      return 0;
	                    }  
                }

            // STATISTIK PEOPLE
            public function statistik_people(){

	                $sql = "SELECT PeopleId FROM v_login";
	                    $hasil = $this->db->query($sql);
	            
	                    if($hasil->num_rows()>0)
	                    {
	                      return $hasil->num_rows();
	                    }
	                    else
	                    {
	                      return 0;
	                    }  
                }

            public function statistik_jumlah_people_struktural(){

	                $sql = "SELECT PeopleId FROM v_login where GroupId = 3 ";
	                    $hasil = $this->db->query($sql);
	            
	                    if($hasil->num_rows()>0)
	                    {
	                      return $hasil->num_rows();
	                    }
	                    else
	                    {
	                      return 0;
	                    }  
                }

            public function statistik_jumlah_people_staf(){

	                $sql = "SELECT PeopleId FROM v_login where GroupId = 7";
	                    $hasil = $this->db->query($sql);
	            
	                    if($hasil->num_rows()>0)
	                    {
	                      return $hasil->num_rows();
	                    }
	                    else
	                    {
	                      return 0;
	                    }  
                }

            // STATISTIK JUMLAH DISPOSISI PADA SUATU DINAS
            public function statistik_disposisi_all(){

                    $sql = "SELECT * FROM v_penerima_pd where ReceiverAs = 'cc1' AND RoleCode = '" . $this->session->userdata('rolecode') ."'";
                        $hasil = $this->db->query($sql);
                
                        if($hasil->num_rows()>0)
                        {
                          return $hasil->num_rows();
                        }
                        else
                        {
                          return 0;
                        }  
                    }
                    
                    public function statistik_disposisi_bidang(){
                      // ini belum bisa di jalankan
                      $sql = "SELECT * FROM v_penerima_pd where ReceiverAs = 'cc1' AND Code_Tu = '" . $this->session->userdata('codetu') ."'";
                          $hasil = $this->db->query($sql);
                  
                          if($hasil->num_rows()>0)
                          {
                            return $hasil->num_rows();
                          }
                          else
                          {
                            return 0;
                          }  
                      }
                    



                    public function statistik_berkas_pd(){

	                    $sql = ;
	                    $hasil = $this->db->query($sql);
	                            
	                    if($hasil->num_rows()>0)
	                    {
	                    return $hasil->num_rows();
	                    }
	                    else
	                    {
	                    return 0;
	                    }  
                    }

                    public function statistik_berkas_bidang(){
                                  
                                    $sql = ;
                                        $hasil = $this->db->query($sql);
                                
                                        if($hasil->num_rows()>0)
                                        {
                                          return $hasil->num_rows();
                                        }
                                        else
                                        {
                                          return 0;
                                        }  
					}
                                
                    public function statistik_disposisi_all(){

                                  $sql = ;
                                      $hasil = $this->db->query($sql);
                              
                                      if($hasil->num_rows()>0)
                                      {
                                        return $hasil->num_rows();
                                      }
                                      else
                                      {
                                        return 0;
                                      }  
					}
                                
                    public function statistik_susut(){

                                          $sql = ;
                                              $hasil = $this->db->query($sql);
                                      
                                              if($hasil->num_rows()>0)
                                              {
                                                return $hasil->num_rows();
                                              }
                                              else
                                              {
                                                return 0;
                                              }  
                    }             

                    public function statistik_musnah(){

                                        $sql = ;
                                            $hasil = $this->db->query($sql);
                                    
                                            if($hasil->num_rows()>0)
                                            {
                                              return $hasil->num_rows();
                                            }
                                            else
                                            {
                                              return 0;
                                            }  
                                        }
                                        
}