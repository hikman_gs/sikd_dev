<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_keys extends MY_Model {

	private $primary_key 	= 'id';
	private $table_name 	= 'keys';
	private $field_search 	= ['key', 'level', 'ignore_limits', 'is_private_key', 'ip_addresses', 'date_created'];

	public function __construct()
	{
		$config = array(
			'primary_key' 	=> $this->primary_key,
		 	'table_name' 	=> $this->table_name,
		 	'field_search' 	=> $this->field_search,
		 );

		parent::__construct($config);
	}

	public function count_all($q = null, $field = null)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "(" . $field . " LIKE '%" . $q . "%' ";
	            } else if ($iterasi == $num) {
	                $where .= "OR " . $field . " LIKE '%" . $q . "%') ";
	            } else {
	                $where .= "OR " . $field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }
        } else {
        	$where .= "(" . $field . " LIKE '%" . $q . "%' )";
        }

        $this->db->where($where);
		$query = $this->db->get($this->table_name);

		return $query->num_rows();
	}

	public function get($q = null, $field = null, $limit = 0, $offset = 0)
	{
		$iterasi = 1;
        $num = count($this->field_search);
        $where = NULL;
        $q = $this->scurity($q);
		$field = $this->scurity($field);

        if (empty($field)) {
	        foreach ($this->field_search as $field) {
	            if ($iterasi == 1) {
	                $where .= "(" . $field . " LIKE '%" . $q . "%' ";
	            } else if ($iterasi == $num) {
	                $where .= "OR " . $field . " LIKE '%" . $q . "%') ";
	            } else {
	                $where .= "OR " . $field . " LIKE '%" . $q . "%' ";
	            }
	            $iterasi++;
	        }
        } else {
        	$where .= "(" . $field . " LIKE '%" . $q . "%' )";
        }

        $this->db->where($where);
        $this->db->limit($limit, $offset);
        $this->db->order_by($this->primary_key, "DESC");
		$query = $this->db->get($this->table_name);

		return $query->result();
	}
	
	function create($id,$key){
        $data = array('user_id' => $id,
        'key'=>$key,
        'level'=>0,//isi terserah ynag penting angka karena tipe data int
        'date_created'=>date('Ymd'));
        $query = $this->db->insert('keys', $data);
        return $query;
    }
    function verifyCreate($id,$key){
        $this->db->where('user_id', $id);
        $this->db->where('key', $key);
        $query = $this->db->get('keys');
        return $query;
    }
    function getByUser($user){
        $this->db->where('user_id', $user);
        $this->db->limit('1');
        $query = $this->db->get('keys');
        return $query;
    }

}

/* End of file Model_keys.php */
/* Location: ./application/models/Model_keys.php */