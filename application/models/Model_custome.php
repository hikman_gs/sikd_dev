<?php

defined('BASEPATH') OR exit('No direct script access allowed');



class Model_custome extends MY_Model {



  public function __construct()

  {

    $config = array();
    parent::__construct($config);

  }

  public function get_people()
  {
    $query = $this->db->query("SELECT * FROM people WHERE PeopleUsername != 'admin' ORDER BY PrimaryRoleId ASC");
    return $query->result();
  }

  public function peopleid($username)
  {

    $query = $this->db->query("SELECT * FROM people WHERE PeopleUsername='".$username."'")->row()->PeopleId;

    return $query;

  }
  
  public function classification()
  {
    $query = $this->db->query("SELECT ClId as id,
        '0' as parent_id,
        Concat(ClCode, ' - ', ClName) as name,
        Concat('0||',ClId,'|',ClCode,'|',ClName,'|',ClDesc,'|',RetensiThn_Active,'|',RetensiThn_InActive,'|', CIStatus,'|', SusutId,'|',
            (case when (select count(c2.ClCode) from classification c2 where c2.ClParentId like c.ClId ) > 0 then 'true' else 'false' end),'|',1) as allData
        FROM classification c
        where ClId='1'
        union
        (SELECT c.ClId as id, c.ClParentId as parent_id, Concat(c.ClCode, ' - ', c.ClName) as name,
        Concat(c.ClParentId,'|',c2.ClCode,'|',c.ClId,'|',c.ClCode,'|',c.ClName,'|',c.ClDesc,'|',c.RetensiThn_Active,'|',c.RetensiThn_InActive,'|', c.CIStatus,'|', c.SusutId,'|', c.Ket_InActive,'|', c.Ket_Active,'|',
          (case when (select count(c3.ClCode) from classification c3 where c3.ClParentId like c.ClId ) > 0 then 'true' else 'false' end), '|', c.ClStatusParent ) as allData
                    FROM classification c join classification c2 on c.ClParentId = c2.ClId order by c.ClCode)");
    return $query->result();
  }

  public function classification_berkas()
  {
    $query = $this->db->query("SELECT ClId as id,
        '0' as parent_id,
        Concat(ClCode, ' - ', ClName) as name,
        Concat(ClId,'|',ClCode) as allData
        FROM classification c
        where ClId='1'
        union
        (SELECT c.ClId as id, c.ClParentId as parent_id, Concat(c.ClCode, ' - ', c.ClName) as name,
        Concat(c.ClId,'|',c.ClCode) as allData
                    FROM classification c join classification c2 on c.ClParentId = c2.ClId order by c.ClCode)");
    return $query->result();
  }

    public function role_access()

    {

                      $_SESSION["GroupId"] = '1';

                      if($_SESSION["GroupId"] == "1"){

                        $parent = 'root';

                        $query = $this->db->query("SELECT RoleParentId as parent_id, 

                                  RoleId as id, 

                                  RoleDesc as name, 

                                  Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId) as allData

                                  FROM role 

                                WHERE RoleId != 'root'

                                ORDER BY RoleId,RoleParentId ");

                      }else{

                        $sql = $this->db->query("SELECT RoleParentId 

                            FROM role 

                            WHERE RoleKey = '" . $_SESSION["AppKey"] . "' 

                              and RoleId = '" . $_SESSION["PrimaryRoleId"] . "'");

                        foreach ($sql as $row) {

                          $parent =  $row->RoleParentId;

                        }

                        

                        $query = $this->db->query("SELECT RoleParentId as parent_id, 

                            RoleId as id, RoleDesc as name,

                            Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId) as allData 

                          FROM role 

                          WHERE RoleKey = '" . $_SESSION["AppKey"] . "' 

                            and RoleId = (select RoleParentId from role r2 where r2.RoleId = '" . $_SESSION["PrimaryRoleId"] . "')

                          UNION

                          SELECT RoleParentId as parent_id, 

                            RoleId as id, 

                            RoleDesc as name,

                            Concat(RoleParentId,'|',RoleId,'|',RoleName,'|',RoleDesc,'|',RoleStatus,'|',gjabatanId) as allData

                          FROM role 

                          WHERE RoleKey = '" . $_SESSION["AppKey"] . "'

                            and RoleId like '" . $_SESSION["PrimaryRoleId"] . "%' ");

                      }

        return $query->result();

    }

}