<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route_path = APPPATH . 'routes/';
require_once $route_path . 'routes_landing.php';

$route['404_override'] = 'not_found';
$route['translate_uri_dashes'] = FALSE;

$route['administrator/adminback'] = 'administrator/auth/login';
$route['administrator/register'] = 'administrator/auth/register';
$route['administrator/forgot-password'] = 'administrator/auth/forgot_password';

$route['administrator/masuk'] = 'administrator/anri';
$route['administrator/anri_check'] = 'administrator/anri/anri_check';
$route['administrator/anri_logout'] = 'administrator/anri/anri_logout';

$route['page/(:any)'] = 'page/detail/$1';
$route['blog/index'] = 'blog/index';
$route['blog/(:any)'] = 'blog/detail/$1';
$route['administrator/web-page'] = 'administrator/page/admin';

//anri
/*$route['anri/login'] = 'app/anri';
$route['anri/login_check'] = 'app/anri/anri_check';
$route['anri/logout'] = 'app/anri/anri_logout';
$route['anri/dashboard'] = 'app/anri_dashboard';
$route['anri/dashboard/(:any)'] = 'app/anri_dashboard/$1';*/
